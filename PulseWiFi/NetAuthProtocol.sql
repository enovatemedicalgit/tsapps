/*----------------------------------------------------------------------------*/
/*--  SQLExtract - Copyright 2012, Micro Tech, A JourneyTEAM Company  --------*/
/*--  NetAuthProtocol extracted from StingerCAST  ----------------------------*/
/*--  5 Records targeted for StingerCAST  ------------------------------------*/
/*----------------------------------------------------------------------------*/


USE StingerCAST
GO

CREATE TABLE [dbo].[NetAuthProtocol] (
        [Description] [varchar] (50) NOT NULL ,
        [Sequence] [varchar] (3) NULL ,
        [ROW_ID] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]
GO


SET IDENTITY_INSERT [dbo].[NetAuthProtocol] ON
GO


CREATE  UNIQUE  CLUSTERED  INDEX [PK_NetAuthProtocol] ON [dbo].[NetAuthProtocol]([ROW_ID]
        )  ON [PRIMARY] 
GO

CREATE  INDEX [IX_NetAuthProtocol] ON [dbo].[NetAuthProtocol]([Description], [Sequence], [ROW_ID]) ON [PRIMARY]
GO



INSERT INTO NetAuthProtocol ( 
Description, [Sequence], ROW_ID ) VALUES ( 
'Mixed WPA-PSK & WPA2-PSK', '4', 4 )
GO


INSERT INTO NetAuthProtocol ( 
Description, [Sequence], ROW_ID ) VALUES ( 
'Open', '1', 1 )
GO


INSERT INTO NetAuthProtocol ( 
Description, [Sequence], ROW_ID ) VALUES ( 
'WEP-128', '2', 2 )
GO


INSERT INTO NetAuthProtocol ( 
Description, [Sequence], ROW_ID ) VALUES ( 
'WPA-PSK', '3', 3 )
GO


INSERT INTO NetAuthProtocol ( 
Description, [Sequence], ROW_ID ) VALUES ( 
'WPA2-PSK', '5', 5 )
GO


SET IDENTITY_INSERT [dbo].[NetAuthProtocol] OFF
GO


