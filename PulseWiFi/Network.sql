/*----------------------------------------------------------------------------*/
/*--  SQLExtract - Copyright 2012, Micro Tech, A JourneyTEAM Company  --------*/
/*--  Network extracted from StingerCAST  ------------------------------------*/
/*--  8 Records targeted for StingerCAST  ------------------------------------*/
/*----------------------------------------------------------------------------*/


USE StingerCAST
GO

CREATE TABLE [dbo].[Network] (
        [BusinessUnitID] [int] NOT NULL ,
        [SSID] [varchar] (32) NOT NULL ,
        [Channel] [int] NOT NULL ,
        [NetAuthProtocolID] [int] NOT NULL ,
        [WEPKey] [varchar] (26) NULL ,
        [WPAPassphrase] [varchar] (64) NULL ,
        [DHCP] [bit] NOT NULL ,
        [IPAddress] [varchar] (20) NULL ,
        [SubnetMask] [varchar] (20) NULL ,
        [DNSServer] [varchar] (20) NULL ,
        [DefaultGateway] [varchar] (20) NULL ,
        [CreatedDateUTC] [datetime] NULL ,
        [CreatedUserID] [int] NULL ,
        [ModifiedDateUTC] [datetime] NULL ,
        [ModifiedUserID] [int] NULL ,
        [ROW_ID] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]
GO


SET IDENTITY_INSERT [dbo].[Network] ON
GO


CREATE  UNIQUE  CLUSTERED  INDEX [PK_Network] ON [dbo].[Network]([ROW_ID]
        )  ON [PRIMARY] 
GO

CREATE  INDEX [IX_Network] ON [dbo].[Network]([BusinessUnitID], [NetAuthProtocolID], [SSID], [ROW_ID]) ON [PRIMARY]
GO

CREATE  INDEX [IX_Network_Unique_SSID_Within_BusinessUnit] ON [dbo].[Network]([BusinessUnitID], [SSID]) ON [PRIMARY]
GO



ALTER TABLE [dbo].[Network] ADD 
   CONSTRAINT [DF_Network_DHCP] DEFAULT ((0)) FOR [DHCP],
   CONSTRAINT [DF_Network_CreatedDateUTC] DEFAULT (getutcdate()) FOR [CreatedDateUTC],
   CONSTRAINT [DF_Network_ModifiedDateUTC] DEFAULT (getutcdate()) FOR [ModifiedDateUTC]
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
553, 'RCH_Guest', 0, 1, '', '', 1
, '', '', '', '', '8/1/2012 5:17:28 PM'
, 588, '8/1/2012 5:17:28 PM', 588, 29 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
587, 'InfoSys', 0, 2, 'C7001563CE08FF974EA502BD62', '', 1
, '', '', '', '', '8/2/2012 3:46:32 PM'
, 659, '8/2/2012 3:46:32 PM', 659, 30 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
396, 'STINGER', 0, 5, '', 'St1ng3rs4r34b33s!', 1
, '', '', '', '', '10/17/2012 6:45:52 PM'
, 48, '10/17/2012 6:45:52 PM', 48, 31 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
395, 'STINGER', 0, 5, '', 'St1ng3rs4r34b33s!', 1
, '', '', '', '', '10/17/2012 6:46:13 PM'
, 48, '10/17/2012 6:46:13 PM', 48, 32 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
68, 'STINGER', 0, 5, '', 'St1ng3rs4r34b33s!', 1
, '', '', '', '', '10/17/2012 6:46:39 PM'
, 48, '10/17/2012 6:46:39 PM', 48, 33 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
355, 'wifi2', 0, 5, '', 'EsfikjnGbtcJnPFouKmi', 1
, '', '', '', '', '10/17/2012 6:51:50 PM'
, 48, '10/17/2012 6:51:50 PM', 48, 34 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
454, 'Atomic', 0, 5, '', 'NtP2theRescue', 1
, '', '', '', '', '10/17/2012 6:52:46 PM'
, 48, '10/17/2012 6:52:46 PM', 48, 35 )
GO


INSERT INTO Network ( 
BusinessUnitID, SSID, Channel, NetAuthProtocolID, WEPKey
, WPAPassphrase, DHCP, IPAddress, SubnetMask, DNSServer
, DefaultGateway, CreatedDateUTC, CreatedUserID, ModifiedDateUTC, ModifiedUserID
, ROW_ID ) VALUES ( 
224, 'Atomic', 0, 5, '', 'NtP2theRescue', 1
, '', '', '', '', '10/17/2012 6:53:25 PM'
, 48, '10/17/2012 6:53:25 PM', 48, 36 )
GO


SET IDENTITY_INSERT [dbo].[Network] OFF
GO


