﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HospWifi
{
    public partial class frmAdmin : Telerik.WinControls.UI.ShapedForm
    {
        public frmAdmin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@Username", txtUsernameAdd.Text },
                { "@FirstName", txtFirstNameAdd.Text},
                { "@LastName", txtLastNameAdd.Text},
                { "@SiteID", ddlSites1.SelectedValue.ToString()},
            };
            ApiLayer api = new ApiLayer();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcAddUser", parameters);
        }

        private void frmAdmin_Load(object sender, EventArgs e)
        {
            ApiLayer api = new ApiLayer();
            DataTable dt = new DataTable();
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@IDSite", "4" } };

            dt = api.ExecSpApi("prcSitesSelect", parameters);
            if (dt.Rows.Count > 1) //Found Site for User
            {

                ddlSites1.DataSource = dt;
                ddlSites2.DataSource = dt;
                ddlSites3.DataSource = dt;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@Username", txtUsernameDelete.Text }
               
            };
            ApiLayer api = new ApiLayer();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcDeleteUser", parameters);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            txtSerialRecycle.Text = "'" + txtSerialRecycle.Text + "'";
            txtSerialRecycle.Text.Replace(",","','");

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SerialList",  txtSerialRecycle.Text }
               
            };

            ApiLayer api = new ApiLayer();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcDeleteUser", parameters);
        }
    }
}
