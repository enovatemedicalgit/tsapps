﻿namespace HospWifi
{
    partial class frmCheckingPulse
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.radWaitingBar1 = new Telerik.WinControls.UI.RadWaitingBar();
            this.dotsRingWaitingBarIndicatorElement1 = new Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement();
            this.roundRectShape1 = new Telerik.WinControls.RoundRectShape(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).BeginInit();
            this.SuspendLayout();
            // 
            // radWaitingBar1
            // 
            this.radWaitingBar1.Location = new System.Drawing.Point(46, 23);
            this.radWaitingBar1.Name = "radWaitingBar1";
            this.radWaitingBar1.Size = new System.Drawing.Size(70, 70);
            this.radWaitingBar1.TabIndex = 0;
            this.radWaitingBar1.Text = "radWaitingBar1";
            this.radWaitingBar1.WaitingIndicators.Add(this.dotsRingWaitingBarIndicatorElement1);
            this.radWaitingBar1.WaitingSpeed = 50;
            this.radWaitingBar1.WaitingStyle = Telerik.WinControls.Enumerations.WaitingBarStyles.DotsRing;
            // 
            // dotsRingWaitingBarIndicatorElement1
            // 
            this.dotsRingWaitingBarIndicatorElement1.AutoSize = false;
            this.dotsRingWaitingBarIndicatorElement1.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren;
            this.dotsRingWaitingBarIndicatorElement1.Bounds = new System.Drawing.Rectangle(-1, -1, 73, 70);
            this.dotsRingWaitingBarIndicatorElement1.DotRadius = 5;
            this.dotsRingWaitingBarIndicatorElement1.DrawImage = false;
            this.dotsRingWaitingBarIndicatorElement1.InnerRadius = 5;
            this.dotsRingWaitingBarIndicatorElement1.LastDotRadius = 7;
            this.dotsRingWaitingBarIndicatorElement1.Name = "dotsRingWaitingBarIndicatorElement1";
            this.dotsRingWaitingBarIndicatorElement1.NumberOfColors = 4;
            this.dotsRingWaitingBarIndicatorElement1.Radius = 30;
            this.dotsRingWaitingBarIndicatorElement1.StretchHorizontally = true;
            this.dotsRingWaitingBarIndicatorElement1.Text = "Checking Pulse";
            this.dotsRingWaitingBarIndicatorElement1.TextWrap = true;
            this.dotsRingWaitingBarIndicatorElement1.Click += new System.EventHandler(this.dotsRingWaitingBarIndicatorElement1_Click);
            // 
            // frmCheckingPulse
            // 
            this.AllowResize = false;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(254)))));
            this.ClientSize = new System.Drawing.Size(167, 119);
            this.ControlBox = false;
            this.Controls.Add(this.radWaitingBar1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCheckingPulse";
            this.Shape = this.roundRectShape1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmCheckingPulse";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmCheckingPulse_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radWaitingBar1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadWaitingBar radWaitingBar1;
        private Telerik.WinControls.UI.DotsRingWaitingBarIndicatorElement dotsRingWaitingBarIndicatorElement1;
        private Telerik.WinControls.RoundRectShape roundRectShape1;
    }
}
