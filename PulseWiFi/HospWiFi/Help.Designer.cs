﻿namespace HospWifi
{
   partial class Help
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage8 = new System.Windows.Forms.TabPage();
         this.txtGeneral = new System.Windows.Forms.TextBox();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.txtUSBHelp = new System.Windows.Forms.TextBox();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.txtCommandModeHelp = new System.Windows.Forms.TextBox();
         this.tabPage3 = new System.Windows.Forms.TabPage();
         this.txtSSIDHelp = new System.Windows.Forms.TextBox();
         this.tabPage4 = new System.Windows.Forms.TabPage();
         this.txtChannelHelp = new System.Windows.Forms.TextBox();
         this.tabPage5 = new System.Windows.Forms.TabPage();
         this.txtAuthenticationHelp = new System.Windows.Forms.TextBox();
         this.tabPage6 = new System.Windows.Forms.TabPage();
         this.txtDHCPHelp = new System.Windows.Forms.TextBox();
         this.tabPage7 = new System.Windows.Forms.TabPage();
         this.txtSaveRebootHelp = new System.Windows.Forms.TextBox();
         this.btnClose = new System.Windows.Forms.Button();
         this.tabControl1.SuspendLayout();
         this.tabPage8.SuspendLayout();
         this.tabPage1.SuspendLayout();
         this.tabPage2.SuspendLayout();
         this.tabPage3.SuspendLayout();
         this.tabPage4.SuspendLayout();
         this.tabPage5.SuspendLayout();
         this.tabPage6.SuspendLayout();
         this.tabPage7.SuspendLayout();
         this.SuspendLayout();
         // 
         // tabControl1
         // 
         this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.tabControl1.Controls.Add(this.tabPage8);
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Controls.Add(this.tabPage3);
         this.tabControl1.Controls.Add(this.tabPage4);
         this.tabControl1.Controls.Add(this.tabPage5);
         this.tabControl1.Controls.Add(this.tabPage6);
         this.tabControl1.Controls.Add(this.tabPage7);
         this.tabControl1.Location = new System.Drawing.Point(12, 12);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(579, 364);
         this.tabControl1.TabIndex = 0;
         // 
         // tabPage8
         // 
         this.tabPage8.Controls.Add(this.txtGeneral);
         this.tabPage8.Location = new System.Drawing.Point(4, 22);
         this.tabPage8.Name = "tabPage8";
         this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage8.Size = new System.Drawing.Size(571, 338);
         this.tabPage8.TabIndex = 7;
         this.tabPage8.Text = "General";
         this.tabPage8.UseVisualStyleBackColor = true;
         // 
         // txtGeneral
         // 
         this.txtGeneral.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtGeneral.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtGeneral.Location = new System.Drawing.Point(21, 21);
         this.txtGeneral.Multiline = true;
         this.txtGeneral.Name = "txtGeneral";
         this.txtGeneral.ReadOnly = true;
         this.txtGeneral.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtGeneral.Size = new System.Drawing.Size(526, 296);
         this.txtGeneral.TabIndex = 2;
         // 
         // tabPage1
         // 
         this.tabPage1.Controls.Add(this.txtUSBHelp);
         this.tabPage1.Location = new System.Drawing.Point(4, 22);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage1.Size = new System.Drawing.Size(571, 338);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Text = "USB Connection";
         this.tabPage1.UseVisualStyleBackColor = true;
         // 
         // txtUSBHelp
         // 
         this.txtUSBHelp.AcceptsReturn = true;
         this.txtUSBHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtUSBHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtUSBHelp.Location = new System.Drawing.Point(21, 21);
         this.txtUSBHelp.Multiline = true;
         this.txtUSBHelp.Name = "txtUSBHelp";
         this.txtUSBHelp.ReadOnly = true;
         this.txtUSBHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtUSBHelp.Size = new System.Drawing.Size(526, 296);
         this.txtUSBHelp.TabIndex = 0;
         // 
         // tabPage2
         // 
         this.tabPage2.Controls.Add(this.txtCommandModeHelp);
         this.tabPage2.Location = new System.Drawing.Point(4, 22);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage2.Size = new System.Drawing.Size(571, 338);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Text = "Command Mode";
         this.tabPage2.UseVisualStyleBackColor = true;
         // 
         // txtCommandModeHelp
         // 
         this.txtCommandModeHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtCommandModeHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtCommandModeHelp.Location = new System.Drawing.Point(21, 21);
         this.txtCommandModeHelp.Multiline = true;
         this.txtCommandModeHelp.Name = "txtCommandModeHelp";
         this.txtCommandModeHelp.ReadOnly = true;
         this.txtCommandModeHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtCommandModeHelp.Size = new System.Drawing.Size(526, 296);
         this.txtCommandModeHelp.TabIndex = 0;
         // 
         // tabPage3
         // 
         this.tabPage3.Controls.Add(this.txtSSIDHelp);
         this.tabPage3.Location = new System.Drawing.Point(4, 22);
         this.tabPage3.Name = "tabPage3";
         this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage3.Size = new System.Drawing.Size(571, 338);
         this.tabPage3.TabIndex = 2;
         this.tabPage3.Text = "SSID";
         this.tabPage3.UseVisualStyleBackColor = true;
         // 
         // txtSSIDHelp
         // 
         this.txtSSIDHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSSIDHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtSSIDHelp.Location = new System.Drawing.Point(21, 21);
         this.txtSSIDHelp.Multiline = true;
         this.txtSSIDHelp.Name = "txtSSIDHelp";
         this.txtSSIDHelp.ReadOnly = true;
         this.txtSSIDHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtSSIDHelp.Size = new System.Drawing.Size(526, 296);
         this.txtSSIDHelp.TabIndex = 1;
         // 
         // tabPage4
         // 
         this.tabPage4.Controls.Add(this.txtChannelHelp);
         this.tabPage4.Location = new System.Drawing.Point(4, 22);
         this.tabPage4.Name = "tabPage4";
         this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage4.Size = new System.Drawing.Size(571, 338);
         this.tabPage4.TabIndex = 3;
         this.tabPage4.Text = "Channel";
         this.tabPage4.UseVisualStyleBackColor = true;
         // 
         // txtChannelHelp
         // 
         this.txtChannelHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtChannelHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtChannelHelp.Location = new System.Drawing.Point(21, 21);
         this.txtChannelHelp.Multiline = true;
         this.txtChannelHelp.Name = "txtChannelHelp";
         this.txtChannelHelp.ReadOnly = true;
         this.txtChannelHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtChannelHelp.Size = new System.Drawing.Size(526, 296);
         this.txtChannelHelp.TabIndex = 1;
         // 
         // tabPage5
         // 
         this.tabPage5.Controls.Add(this.txtAuthenticationHelp);
         this.tabPage5.Location = new System.Drawing.Point(4, 22);
         this.tabPage5.Name = "tabPage5";
         this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage5.Size = new System.Drawing.Size(571, 338);
         this.tabPage5.TabIndex = 4;
         this.tabPage5.Text = "Authentication";
         this.tabPage5.UseVisualStyleBackColor = true;
         // 
         // txtAuthenticationHelp
         // 
         this.txtAuthenticationHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtAuthenticationHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtAuthenticationHelp.Location = new System.Drawing.Point(21, 21);
         this.txtAuthenticationHelp.Multiline = true;
         this.txtAuthenticationHelp.Name = "txtAuthenticationHelp";
         this.txtAuthenticationHelp.ReadOnly = true;
         this.txtAuthenticationHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtAuthenticationHelp.Size = new System.Drawing.Size(526, 296);
         this.txtAuthenticationHelp.TabIndex = 1;
         // 
         // tabPage6
         // 
         this.tabPage6.Controls.Add(this.txtDHCPHelp);
         this.tabPage6.Location = new System.Drawing.Point(4, 22);
         this.tabPage6.Name = "tabPage6";
         this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage6.Size = new System.Drawing.Size(571, 338);
         this.tabPage6.TabIndex = 5;
         this.tabPage6.Text = "DHCP";
         this.tabPage6.UseVisualStyleBackColor = true;
         // 
         // txtDHCPHelp
         // 
         this.txtDHCPHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtDHCPHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtDHCPHelp.Location = new System.Drawing.Point(21, 21);
         this.txtDHCPHelp.Multiline = true;
         this.txtDHCPHelp.Name = "txtDHCPHelp";
         this.txtDHCPHelp.ReadOnly = true;
         this.txtDHCPHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtDHCPHelp.Size = new System.Drawing.Size(526, 296);
         this.txtDHCPHelp.TabIndex = 1;
         // 
         // tabPage7
         // 
         this.tabPage7.Controls.Add(this.txtSaveRebootHelp);
         this.tabPage7.Location = new System.Drawing.Point(4, 22);
         this.tabPage7.Name = "tabPage7";
         this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage7.Size = new System.Drawing.Size(571, 338);
         this.tabPage7.TabIndex = 6;
         this.tabPage7.Text = "Save & Reboot";
         this.tabPage7.UseVisualStyleBackColor = true;
         // 
         // txtSaveRebootHelp
         // 
         this.txtSaveRebootHelp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtSaveRebootHelp.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.txtSaveRebootHelp.Location = new System.Drawing.Point(21, 21);
         this.txtSaveRebootHelp.Multiline = true;
         this.txtSaveRebootHelp.Name = "txtSaveRebootHelp";
         this.txtSaveRebootHelp.ReadOnly = true;
         this.txtSaveRebootHelp.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
         this.txtSaveRebootHelp.Size = new System.Drawing.Size(526, 296);
         this.txtSaveRebootHelp.TabIndex = 1;
         // 
         // btnClose
         // 
         this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnClose.Location = new System.Drawing.Point(512, 386);
         this.btnClose.Name = "btnClose";
         this.btnClose.Size = new System.Drawing.Size(75, 23);
         this.btnClose.TabIndex = 1;
         this.btnClose.Text = "Close";
         this.btnClose.UseVisualStyleBackColor = true;
         this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
         // 
         // Help
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(603, 421);
         this.Controls.Add(this.btnClose);
         this.Controls.Add(this.tabControl1);
         this.Name = "Help";
         this.Text = "Enovate Medical - Hospital WiFi Setup Utility Help";
         this.tabControl1.ResumeLayout(false);
         this.tabPage8.ResumeLayout(false);
         this.tabPage8.PerformLayout();
         this.tabPage1.ResumeLayout(false);
         this.tabPage1.PerformLayout();
         this.tabPage2.ResumeLayout(false);
         this.tabPage2.PerformLayout();
         this.tabPage3.ResumeLayout(false);
         this.tabPage3.PerformLayout();
         this.tabPage4.ResumeLayout(false);
         this.tabPage4.PerformLayout();
         this.tabPage5.ResumeLayout(false);
         this.tabPage5.PerformLayout();
         this.tabPage6.ResumeLayout(false);
         this.tabPage6.PerformLayout();
         this.tabPage7.ResumeLayout(false);
         this.tabPage7.PerformLayout();
         this.ResumeLayout(false);

      }

      #endregion

      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.Button btnClose;
      private System.Windows.Forms.TextBox txtUSBHelp;
      private System.Windows.Forms.TextBox txtCommandModeHelp;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.TextBox txtSSIDHelp;
      private System.Windows.Forms.TabPage tabPage4;
      private System.Windows.Forms.TextBox txtChannelHelp;
      private System.Windows.Forms.TabPage tabPage5;
      private System.Windows.Forms.TextBox txtAuthenticationHelp;
      private System.Windows.Forms.TabPage tabPage6;
      private System.Windows.Forms.TextBox txtDHCPHelp;
      private System.Windows.Forms.TabPage tabPage7;
      private System.Windows.Forms.TextBox txtSaveRebootHelp;
      private System.Windows.Forms.TabPage tabPage8;
      private System.Windows.Forms.TextBox txtGeneral;
   }
}