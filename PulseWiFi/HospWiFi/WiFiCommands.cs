﻿/**********************************************************************************
 Program Name: WiFiCommands.cs
 Author: G.T. Sanford III
 Copyright (c) 2010 by Stinger Medical
 Created: 11/6/2010 at 10:06
-----------------------------------------------------------------------------------
 Description: 



-----------------------------------------------------------------------------------

                     M O D I F I C A T I O N    H I S T O R Y 

  Date    By   Vers   Modification
--------  ---  ----  --------------------------------------------------------------




------------------------------ ALL RIGHTS RESERVED --------------------------------
**********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HospWifi
{
   public partial class WiFiCommands : Form
   {
      private string _text = "";


      public string CText
      {
         get
         {
            return _text;
         }
      }


      public WiFiCommands()
      {
         InitializeComponent();
      }

      private void btnClose_Click( object sender, EventArgs e )
      {
         this.Close();
      }

      private void textBox1_DoubleClick( object sender, EventArgs e )
      {
         TextBox tb = (TextBox)sender;
         _text = tb.Text;
         this.Close();
      }
   }
}
/*--- end of WiFiCommands.cs ----------------------------------------------------*/

