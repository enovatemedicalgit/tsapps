﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HospWifi
{
   public partial class Substitute : Form
   {
      private string _NewChar;



      public string NewCharacter
      {
         get
         {
            return _NewChar;
         }
         set
         {
            _NewChar = value;
         }
      }




      public Substitute()
      {
         InitializeComponent();
      }




      private void btnCancel_Click( object sender, EventArgs e )
      {
         _NewChar = "Cancel";
         this.Close();
      }

      
      
      
      private void btnSet_Click( object sender, EventArgs e )
      {
         _NewChar = txtNewCharacter.Text;

         this.Close();
      }




      private void btnLaunchCharMap_Click( object sender, EventArgs e )
      {
         ProcessStartInfo startInfo = new ProcessStartInfo( "charmap.exe" );
         Process.Start( startInfo );
         txtNewCharacter.Select();
      }

  



   }
}
