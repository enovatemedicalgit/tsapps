﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace HospWifi 
{
    public class ApiLayer
    {
        
        public static string URL =  PACU.APIUrl;
        

        public  DataTable ExecSpApi(string storedProc, Dictionary<string,string> spParms )
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(PACU.APIUrl);
            try
            {
                client.Timeout = TimeSpan.FromSeconds(10);
            }
            catch (Exception ex) 
            {
                client = null;
            client = new HttpClient();
            client.BaseAddress = new Uri(PACU.APIUrl);
            }

            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            // not using get method - very limited
            // post is requiring multiple parameters to be sent as 1, so add in stored proc name
            spParms.Add("storedProcName", storedProc);

            HttpResponseMessage response = client.PostAsJsonAsync("api/StoredProc/", spParms).Result;
            if (response.IsSuccessStatusCode)
            {
                var dt = response.Content.ReadAsAsync<DataTable>().Result;

                return dt;
            }
            else
            {
                return new DataTable();
            }
        }


        public bool AuthUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                if (identity == null) return false;
                userName = identity.Name;
            }
            HttpClient client = new HttpClient {BaseAddress = new Uri( PACU.APIUrl)};

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/UserAuth/?userId="+userName).Result;
            if (response.IsSuccessStatusCode)
            {
                bool auth = response.Content.ReadAsAsync<bool>().Result;
                return auth;
            }
            else
            {
                //MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof (T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo colProp in temp.GetProperties())
                {
                    if (colProp.Name == column.ColumnName) colProp.SetValue(obj, dr[column.ColumnName], null);
                }
            }
            return obj;
        }

       
    }
}

