﻿namespace HospWifi
{
    partial class PACU
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PACU));
            this.label67 = new System.Windows.Forms.Label();
            this.rdo13ASCII = new System.Windows.Forms.RadioButton();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtChannel = new System.Windows.Forms.TextBox();
            this.rdo26HEX = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdoHex = new System.Windows.Forms.RadioButton();
            this.rdoASCII = new System.Windows.Forms.RadioButton();
            this.lblSSID2 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rdoMixedWPA = new System.Windows.Forms.RadioButton();
            this.txtDeviceIP = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassPhrase = new System.Windows.Forms.TextBox();
            this.txtWEPKey = new System.Windows.Forms.TextBox();
            this.rdoWPA2 = new System.Windows.Forms.RadioButton();
            this.pbxCommunicating = new System.Windows.Forms.PictureBox();
            this.rdoWPA1 = new System.Windows.Forms.RadioButton();
            this.label59 = new System.Windows.Forms.Label();
            this.txtDNSServer = new System.Windows.Forms.TextBox();
            this.txtGateway = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtSubnetMask = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdoWEP128 = new System.Windows.Forms.RadioButton();
            this.rdoOpen = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoDHCPOn = new System.Windows.Forms.RadioButton();
            this.rdoDHCPOff = new System.Windows.Forms.RadioButton();
            this.gboxPulseStatus = new System.Windows.Forms.GroupBox();
            this.txtAssignedSite = new System.Windows.Forms.Label();
            this.txtCommunicatingToPulse = new System.Windows.Forms.Label();
            this.txtLastSuccessfulPacket = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSiteIP = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtLastPostDate = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.gbPulseAssetSetup = new System.Windows.Forms.GroupBox();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.btnSaveAsset = new Telerik.WinControls.UI.RadButton();
            this.label6 = new System.Windows.Forms.Label();
            this.ddlSites = new Telerik.WinControls.UI.RadDropDownList();
            this.ddlDepartments = new Telerik.WinControls.UI.RadDropDownList();
            this.txtFloor = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txtWing = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.txtAssetNumber = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.btnSendPacketNow = new Telerik.WinControls.UI.RadButton();
            this.rdoBaycharger = new System.Windows.Forms.RadioButton();
            this.rdoWorkstation = new System.Windows.Forms.RadioButton();
            this.txtWSSerialNumber = new System.Windows.Forms.TextBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.txtSignalQuality = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.txtSSID = new Telerik.WinControls.UI.RadTextBox();
            this.cmbSSIDs = new System.Windows.Forms.ComboBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnWSSNSet = new Telerik.WinControls.UI.RadButton();
            this.btnWSSNGet = new Telerik.WinControls.UI.RadButton();
            this.pbxSaveConfiguration = new System.Windows.Forms.PictureBox();
            this.pbxRebootWiFi = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ilsLED = new System.Windows.Forms.ImageList(this.components);
            this.txtResponse = new Telerik.WinControls.UI.RadTextBoxControl();
            this.txtCommand = new Telerik.WinControls.UI.RadTextBoxControl();
            this.btnUpdateRovingFirmware = new Telerik.WinControls.UI.RadButton();
            this.btnLoginToPulse = new Telerik.WinControls.UI.RadButton();
            this.cbUSBConnected = new System.Windows.Forms.CheckBox();
            this.btnSaveChanges = new Telerik.WinControls.UI.RadButton();
            this.btnConnectUSB = new Telerik.WinControls.UI.RadButton();
            this.pbConnectedToUSB = new System.Windows.Forms.PictureBox();
            this.pbChangesSaved = new System.Windows.Forms.PictureBox();
            this.pbConnectedToPulse = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radButton1 = new Telerik.WinControls.UI.RadButton();
            this.tmrCommCheck = new System.Windows.Forms.Timer(this.components);
            this.btnSaveNetwork = new Telerik.WinControls.UI.RadButton();
            this.radButton3 = new Telerik.WinControls.UI.RadButton();
            this.groupBox14.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gboxPulseStatus.SuspendLayout();
            this.gbPulseAssetSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDepartments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendPacketNow)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignalQuality)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSID)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnWSSNSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnWSSNGet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtResponse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpdateRovingFirmware)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoginToPulse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConnectUSB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnectedToUSB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChangesSaved)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnectedToPulse)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveNetwork)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(14, 86);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(129, 13);
            this.label67.TabIndex = 284;
            this.label67.Text = "Internal IP of Device:";
            // 
            // rdo13ASCII
            // 
            this.rdo13ASCII.AutoSize = true;
            this.rdo13ASCII.Checked = true;
            this.rdo13ASCII.Location = new System.Drawing.Point(10, 15);
            this.rdo13ASCII.Name = "rdo13ASCII";
            this.rdo13ASCII.Size = new System.Drawing.Size(69, 17);
            this.rdo13ASCII.TabIndex = 0;
            this.rdo13ASCII.TabStop = true;
            this.rdo13ASCII.Text = "13 ASCII ";
            this.rdo13ASCII.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label2);
            this.groupBox14.Controls.Add(this.txtChannel);
            this.groupBox14.Location = new System.Drawing.Point(390, 15);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(71, 65);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Channel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "(1-13, 0)";
            // 
            // txtChannel
            // 
            this.txtChannel.Location = new System.Drawing.Point(12, 19);
            this.txtChannel.MaxLength = 2;
            this.txtChannel.Name = "txtChannel";
            this.txtChannel.Size = new System.Drawing.Size(43, 20);
            this.txtChannel.TabIndex = 13;
            this.txtChannel.Text = "0";
            this.toolTip1.SetToolTip(this.txtChannel, "1 thru 13, 0 for scan");
            // 
            // rdo26HEX
            // 
            this.rdo26HEX.AutoSize = true;
            this.rdo26HEX.Location = new System.Drawing.Point(10, 36);
            this.rdo26HEX.Name = "rdo26HEX";
            this.rdo26HEX.Size = new System.Drawing.Size(60, 17);
            this.rdo26HEX.TabIndex = 1;
            this.rdo26HEX.Text = "26 HEX";
            this.rdo26HEX.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdoHex);
            this.panel1.Controls.Add(this.rdoASCII);
            this.panel1.Location = new System.Drawing.Point(183, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 26);
            this.panel1.TabIndex = 21;
            // 
            // rdoHex
            // 
            this.rdoHex.AutoSize = true;
            this.rdoHex.Location = new System.Drawing.Point(81, 3);
            this.rdoHex.Name = "rdoHex";
            this.rdoHex.Size = new System.Drawing.Size(45, 17);
            this.rdoHex.TabIndex = 20;
            this.rdoHex.Text = "HEX";
            this.rdoHex.UseVisualStyleBackColor = true;
            this.rdoHex.CheckedChanged += new System.EventHandler(this.rdoHex_CheckedChanged);
            // 
            // rdoASCII
            // 
            this.rdoASCII.AutoSize = true;
            this.rdoASCII.Checked = true;
            this.rdoASCII.Location = new System.Drawing.Point(9, 3);
            this.rdoASCII.Name = "rdoASCII";
            this.rdoASCII.Size = new System.Drawing.Size(54, 17);
            this.rdoASCII.TabIndex = 19;
            this.rdoASCII.TabStop = true;
            this.rdoASCII.Text = "ASCII ";
            this.rdoASCII.UseVisualStyleBackColor = true;
            this.rdoASCII.CheckedChanged += new System.EventHandler(this.rdo13ASCII_CheckedChanged);
            // 
            // lblSSID2
            // 
            this.lblSSID2.AutoSize = true;
            this.lblSSID2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Underline);
            this.lblSSID2.Location = new System.Drawing.Point(335, 112);
            this.lblSSID2.Name = "lblSSID2";
            this.lblSSID2.Size = new System.Drawing.Size(134, 13);
            this.lblSSID2.TabIndex = 18;
            this.lblSSID2.Text = "(Will convert spaces to $)";
            this.lblSSID2.Click += new System.EventHandler(this.lblSSID2_Click);
            this.lblSSID2.DoubleClick += new System.EventHandler(this.lblSSID2_DoubleClick);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.rdo26HEX);
            this.groupBox8.Controls.Add(this.rdo13ASCII);
            this.groupBox8.Location = new System.Drawing.Point(474, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(80, 62);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "# Chars";
            // 
            // rdoMixedWPA
            // 
            this.rdoMixedWPA.AutoSize = true;
            this.rdoMixedWPA.Location = new System.Drawing.Point(17, 89);
            this.rdoMixedWPA.Name = "rdoMixedWPA";
            this.rdoMixedWPA.Size = new System.Drawing.Size(156, 17);
            this.rdoMixedWPA.TabIndex = 3;
            this.rdoMixedWPA.Text = "Mixed WPA1 && WPA2-PSK";
            this.rdoMixedWPA.UseVisualStyleBackColor = true;
            this.rdoMixedWPA.CheckedChanged += new System.EventHandler(this.rdoMixedWPA_CheckedChanged);
            // 
            // txtDeviceIP
            // 
            this.txtDeviceIP.Enabled = false;
            this.txtDeviceIP.Location = new System.Drawing.Point(181, 84);
            this.txtDeviceIP.Name = "txtDeviceIP";
            this.txtDeviceIP.Size = new System.Drawing.Size(200, 20);
            this.txtDeviceIP.TabIndex = 283;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(186, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "WPA Passphrase (1-64 chars)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(186, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "WEP Key (13/26 chars)";
            // 
            // txtPassPhrase
            // 
            this.txtPassPhrase.Enabled = false;
            this.txtPassPhrase.Location = new System.Drawing.Point(183, 86);
            this.txtPassPhrase.MaxLength = 64;
            this.txtPassPhrase.Name = "txtPassPhrase";
            this.txtPassPhrase.Size = new System.Drawing.Size(278, 20);
            this.txtPassPhrase.TabIndex = 52;
            this.txtPassPhrase.MouseClick += new System.Windows.Forms.MouseEventHandler(this.txtPassPhrase_MouseClick);
            this.txtPassPhrase.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPassPhrase_KeyDown);
            // 
            // txtWEPKey
            // 
            this.txtWEPKey.Location = new System.Drawing.Point(183, 40);
            this.txtWEPKey.MaxLength = 13;
            this.txtWEPKey.Name = "txtWEPKey";
            this.txtWEPKey.Size = new System.Drawing.Size(180, 20);
            this.txtWEPKey.TabIndex = 51;
            // 
            // rdoWPA2
            // 
            this.rdoWPA2.AutoSize = true;
            this.rdoWPA2.Checked = true;
            this.rdoWPA2.Location = new System.Drawing.Point(17, 112);
            this.rdoWPA2.Name = "rdoWPA2";
            this.rdoWPA2.Size = new System.Drawing.Size(77, 17);
            this.rdoWPA2.TabIndex = 4;
            this.rdoWPA2.TabStop = true;
            this.rdoWPA2.Text = "WPA2-PSK";
            this.rdoWPA2.UseVisualStyleBackColor = true;
            this.rdoWPA2.CheckedChanged += new System.EventHandler(this.rdoWPA2_CheckedChanged);
            // 
            // pbxCommunicating
            // 
            this.pbxCommunicating.Location = new System.Drawing.Point(382, 16);
            this.pbxCommunicating.Name = "pbxCommunicating";
            this.pbxCommunicating.Size = new System.Drawing.Size(19, 15);
            this.pbxCommunicating.TabIndex = 279;
            this.pbxCommunicating.TabStop = false;
            // 
            // rdoWPA1
            // 
            this.rdoWPA1.AutoSize = true;
            this.rdoWPA1.Location = new System.Drawing.Point(17, 66);
            this.rdoWPA1.Name = "rdoWPA1";
            this.rdoWPA1.Size = new System.Drawing.Size(55, 17);
            this.rdoWPA1.TabIndex = 2;
            this.rdoWPA1.Text = "WPA1";
            this.rdoWPA1.UseVisualStyleBackColor = true;
            this.rdoWPA1.CheckedChanged += new System.EventHandler(this.rdoWPA1_CheckedChanged);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(308, 46);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(74, 13);
            this.label59.TabIndex = 13;
            this.label59.Text = "DNS Server";
            // 
            // txtDNSServer
            // 
            this.txtDNSServer.Enabled = false;
            this.txtDNSServer.Location = new System.Drawing.Point(408, 43);
            this.txtDNSServer.Name = "txtDNSServer";
            this.txtDNSServer.Size = new System.Drawing.Size(134, 20);
            this.txtDNSServer.TabIndex = 11;
            // 
            // txtGateway
            // 
            this.txtGateway.Enabled = false;
            this.txtGateway.Location = new System.Drawing.Point(408, 16);
            this.txtGateway.Name = "txtGateway";
            this.txtGateway.Size = new System.Drawing.Size(134, 20);
            this.txtGateway.TabIndex = 10;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(308, 19);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(101, 13);
            this.label60.TabIndex = 12;
            this.label60.Text = "Default Gateway";
            // 
            // txtSubnetMask
            // 
            this.txtSubnetMask.Enabled = false;
            this.txtSubnetMask.Location = new System.Drawing.Point(190, 42);
            this.txtSubnetMask.Name = "txtSubnetMask";
            this.txtSubnetMask.Size = new System.Drawing.Size(100, 20);
            this.txtSubnetMask.TabIndex = 8;
            this.txtSubnetMask.Text = "255.255.255.255";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(102, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(81, 13);
            this.label58.TabIndex = 9;
            this.label58.Text = "Subnet Mask";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Enabled = false;
            this.txtIPAddress.Location = new System.Drawing.Point(190, 16);
            this.txtIPAddress.MaxLength = 15;
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(100, 20);
            this.txtIPAddress.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(103, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "IP Address";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox14);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.lblSSID2);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtPassPhrase);
            this.groupBox2.Controls.Add(this.txtWEPKey);
            this.groupBox2.Controls.Add(this.rdoWPA2);
            this.groupBox2.Controls.Add(this.rdoMixedWPA);
            this.groupBox2.Controls.Add(this.rdoWPA1);
            this.groupBox2.Controls.Add(this.rdoWEP128);
            this.groupBox2.Controls.Add(this.rdoOpen);
            this.groupBox2.Location = new System.Drawing.Point(168, 228);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(564, 145);
            this.groupBox2.TabIndex = 309;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Authentication";
            this.toolTip1.SetToolTip(this.groupBox2, "Choose the Authentication method");
            // 
            // rdoWEP128
            // 
            this.rdoWEP128.AutoSize = true;
            this.rdoWEP128.Location = new System.Drawing.Point(17, 43);
            this.rdoWEP128.Name = "rdoWEP128";
            this.rdoWEP128.Size = new System.Drawing.Size(70, 17);
            this.rdoWEP128.TabIndex = 1;
            this.rdoWEP128.Text = "WEP-128";
            this.rdoWEP128.UseVisualStyleBackColor = true;
            this.rdoWEP128.CheckedChanged += new System.EventHandler(this.rdoWEP128_CheckedChanged);
            // 
            // rdoOpen
            // 
            this.rdoOpen.AutoSize = true;
            this.rdoOpen.Location = new System.Drawing.Point(17, 20);
            this.rdoOpen.Name = "rdoOpen";
            this.rdoOpen.Size = new System.Drawing.Size(54, 17);
            this.rdoOpen.TabIndex = 0;
            this.rdoOpen.Text = "Open";
            this.rdoOpen.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.txtDNSServer);
            this.groupBox1.Controls.Add(this.txtGateway);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Controls.Add(this.txtSubnetMask);
            this.groupBox1.Controls.Add(this.label58);
            this.groupBox1.Controls.Add(this.txtIPAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rdoDHCPOn);
            this.groupBox1.Controls.Add(this.rdoDHCPOff);
            this.groupBox1.Location = new System.Drawing.Point(168, 378);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(566, 74);
            this.groupBox1.TabIndex = 310;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DHCP";
            this.toolTip1.SetToolTip(this.groupBox1, "Choose the DHCP method");
            // 
            // rdoDHCPOn
            // 
            this.rdoDHCPOn.AutoSize = true;
            this.rdoDHCPOn.Checked = true;
            this.rdoDHCPOn.Location = new System.Drawing.Point(14, 42);
            this.rdoDHCPOn.Name = "rdoDHCPOn";
            this.rdoDHCPOn.Size = new System.Drawing.Size(73, 17);
            this.rdoDHCPOn.TabIndex = 1;
            this.rdoDHCPOn.TabStop = true;
            this.rdoDHCPOn.Text = "DHCP On";
            this.rdoDHCPOn.UseVisualStyleBackColor = true;
            this.rdoDHCPOn.CheckedChanged += new System.EventHandler(this.rdoDHCPOn_CheckedChanged);
            // 
            // rdoDHCPOff
            // 
            this.rdoDHCPOff.AutoSize = true;
            this.rdoDHCPOff.Location = new System.Drawing.Point(14, 19);
            this.rdoDHCPOff.Name = "rdoDHCPOff";
            this.rdoDHCPOff.Size = new System.Drawing.Size(74, 17);
            this.rdoDHCPOff.TabIndex = 0;
            this.rdoDHCPOff.Text = "DHCP Off";
            this.rdoDHCPOff.UseVisualStyleBackColor = true;
            this.rdoDHCPOff.CheckedChanged += new System.EventHandler(this.rdoDHCPOff_CheckedChanged_1);
            // 
            // gboxPulseStatus
            // 
            this.gboxPulseStatus.Controls.Add(this.txtAssignedSite);
            this.gboxPulseStatus.Controls.Add(this.txtCommunicatingToPulse);
            this.gboxPulseStatus.Controls.Add(this.txtLastSuccessfulPacket);
            this.gboxPulseStatus.Controls.Add(this.label5);
            this.gboxPulseStatus.Controls.Add(this.txtDeviceIP);
            this.gboxPulseStatus.Controls.Add(this.label67);
            this.gboxPulseStatus.Controls.Add(this.txtSiteIP);
            this.gboxPulseStatus.Controls.Add(this.label23);
            this.gboxPulseStatus.Controls.Add(this.txtLastPostDate);
            this.gboxPulseStatus.Controls.Add(this.pbxCommunicating);
            this.gboxPulseStatus.Controls.Add(this.label29);
            this.gboxPulseStatus.Enabled = false;
            this.gboxPulseStatus.Location = new System.Drawing.Point(168, 458);
            this.gboxPulseStatus.Name = "gboxPulseStatus";
            this.gboxPulseStatus.Size = new System.Drawing.Size(566, 109);
            this.gboxPulseStatus.TabIndex = 311;
            this.gboxPulseStatus.TabStop = false;
            this.gboxPulseStatus.Text = "Pulse Status";
            this.toolTip1.SetToolTip(this.gboxPulseStatus, "Choose the DHCP method");
            // 
            // txtAssignedSite
            // 
            this.txtAssignedSite.AutoEllipsis = true;
            this.txtAssignedSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 5F);
            this.txtAssignedSite.Location = new System.Drawing.Point(407, 40);
            this.txtAssignedSite.Name = "txtAssignedSite";
            this.txtAssignedSite.Size = new System.Drawing.Size(147, 12);
            this.txtAssignedSite.TabIndex = 288;
            // 
            // txtCommunicatingToPulse
            // 
            this.txtCommunicatingToPulse.AutoSize = true;
            this.txtCommunicatingToPulse.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.25F, System.Drawing.FontStyle.Bold);
            this.txtCommunicatingToPulse.Location = new System.Drawing.Point(404, 17);
            this.txtCommunicatingToPulse.Name = "txtCommunicatingToPulse";
            this.txtCommunicatingToPulse.Size = new System.Drawing.Size(150, 12);
            this.txtCommunicatingToPulse.TabIndex = 287;
            this.txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
            // 
            // txtLastSuccessfulPacket
            // 
            this.txtLastSuccessfulPacket.Enabled = false;
            this.txtLastSuccessfulPacket.Location = new System.Drawing.Point(181, 12);
            this.txtLastSuccessfulPacket.MaxLength = 15;
            this.txtLastSuccessfulPacket.Name = "txtLastSuccessfulPacket";
            this.txtLastSuccessfulPacket.Size = new System.Drawing.Size(200, 20);
            this.txtLastSuccessfulPacket.TabIndex = 285;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 13);
            this.label5.TabIndex = 286;
            this.label5.Text = "Last Successful Packet:";
            // 
            // txtSiteIP
            // 
            this.txtSiteIP.Enabled = false;
            this.txtSiteIP.Location = new System.Drawing.Point(181, 60);
            this.txtSiteIP.Name = "txtSiteIP";
            this.txtSiteIP.Size = new System.Drawing.Size(200, 20);
            this.txtSiteIP.TabIndex = 8;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(14, 63);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(82, 13);
            this.label23.TabIndex = 9;
            this.label23.Text = "DMZ/Site IP:";
            // 
            // txtLastPostDate
            // 
            this.txtLastPostDate.Enabled = false;
            this.txtLastPostDate.Location = new System.Drawing.Point(181, 36);
            this.txtLastPostDate.MaxLength = 15;
            this.txtLastPostDate.Name = "txtLastPostDate";
            this.txtLastPostDate.Size = new System.Drawing.Size(200, 20);
            this.txtLastPostDate.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(14, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(145, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Last Post Date In Pulse:";
            // 
            // gbPulseAssetSetup
            // 
            this.gbPulseAssetSetup.Controls.Add(this.radButton2);
            this.gbPulseAssetSetup.Controls.Add(this.btnSaveAsset);
            this.gbPulseAssetSetup.Controls.Add(this.label6);
            this.gbPulseAssetSetup.Controls.Add(this.ddlSites);
            this.gbPulseAssetSetup.Controls.Add(this.ddlDepartments);
            this.gbPulseAssetSetup.Controls.Add(this.txtFloor);
            this.gbPulseAssetSetup.Controls.Add(this.label71);
            this.gbPulseAssetSetup.Controls.Add(this.txtWing);
            this.gbPulseAssetSetup.Controls.Add(this.label68);
            this.gbPulseAssetSetup.Controls.Add(this.label69);
            this.gbPulseAssetSetup.Controls.Add(this.txtAssetNumber);
            this.gbPulseAssetSetup.Controls.Add(this.label70);
            this.gbPulseAssetSetup.Enabled = false;
            this.gbPulseAssetSetup.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gbPulseAssetSetup.Location = new System.Drawing.Point(289, 17);
            this.gbPulseAssetSetup.Name = "gbPulseAssetSetup";
            this.gbPulseAssetSetup.Size = new System.Drawing.Size(443, 132);
            this.gbPulseAssetSetup.TabIndex = 316;
            this.gbPulseAssetSetup.TabStop = false;
            this.gbPulseAssetSetup.Text = "Pulse Asset Setup";
            this.toolTip1.SetToolTip(this.gbPulseAssetSetup, "Click Login to Enable Asset Setup");
            // 
            // radButton2
            // 
            this.radButton2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radButton2.Location = new System.Drawing.Point(346, 47);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(91, 26);
            this.radButton2.TabIndex = 18;
            this.radButton2.Text = "Add New";
            this.radButton2.TextWrap = true;
            this.radButton2.Click += new System.EventHandler(this.radButton2_Click);
            // 
            // btnSaveAsset
            // 
            this.btnSaveAsset.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSaveAsset.Location = new System.Drawing.Point(346, 79);
            this.btnSaveAsset.Name = "btnSaveAsset";
            this.btnSaveAsset.Size = new System.Drawing.Size(91, 43);
            this.btnSaveAsset.TabIndex = 17;
            this.btnSaveAsset.Text = "Save changes to Pulse";
            this.btnSaveAsset.TextWrap = true;
            this.btnSaveAsset.Click += new System.EventHandler(this.btnSaveAsset_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Facility:";
            // 
            // ddlSites
            // 
            this.ddlSites.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlSites.DefaultItemsCountInDropDown = 24;
            this.ddlSites.DisplayMember = "SiteName";
            this.ddlSites.Location = new System.Drawing.Point(82, 17);
            this.ddlSites.Name = "ddlSites";
            this.ddlSites.Size = new System.Drawing.Size(355, 20);
            this.ddlSites.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.ddlSites.TabIndex = 5;
            this.ddlSites.Text = "Select Facility";
            this.ddlSites.ValueMember = "IDSite";
            this.ddlSites.SelectedValueChanged += new System.EventHandler(this.ddlSites_SelectedValueChanged);
            // 
            // ddlDepartments
            // 
            this.ddlDepartments.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlDepartments.DefaultItemsCountInDropDown = 12;
            this.ddlDepartments.DisplayMember = "Description";
            this.ddlDepartments.Location = new System.Drawing.Point(82, 47);
            this.ddlDepartments.Name = "ddlDepartments";
            this.ddlDepartments.Size = new System.Drawing.Size(258, 20);
            this.ddlDepartments.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.ddlDepartments.TabIndex = 6;
            this.ddlDepartments.Text = "Select Department";
            this.ddlDepartments.ValueMember = "IDDepartment";
            // 
            // txtFloor
            // 
            this.txtFloor.Location = new System.Drawing.Point(250, 79);
            this.txtFloor.Name = "txtFloor";
            this.txtFloor.Size = new System.Drawing.Size(90, 20);
            this.txtFloor.TabIndex = 12;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(202, 82);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(39, 13);
            this.label71.TabIndex = 13;
            this.label71.Text = "Floor:";
            // 
            // txtWing
            // 
            this.txtWing.Location = new System.Drawing.Point(61, 79);
            this.txtWing.Name = "txtWing";
            this.txtWing.Size = new System.Drawing.Size(127, 20);
            this.txtWing.TabIndex = 10;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(6, 82);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(40, 13);
            this.label68.TabIndex = 11;
            this.label68.Text = "Wing:";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(6, 52);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(80, 13);
            this.label69.TabIndex = 9;
            this.label69.Text = "Department: ";
            // 
            // txtAssetNumber
            // 
            this.txtAssetNumber.Location = new System.Drawing.Point(62, 106);
            this.txtAssetNumber.MaxLength = 15;
            this.txtAssetNumber.Name = "txtAssetNumber";
            this.txtAssetNumber.Size = new System.Drawing.Size(126, 20);
            this.txtAssetNumber.TabIndex = 2;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(6, 109);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(54, 13);
            this.label70.TabIndex = 3;
            this.label70.Text = "Asset #:";
            // 
            // btnSendPacketNow
            // 
            this.btnSendPacketNow.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSendPacketNow.Location = new System.Drawing.Point(13, 199);
            this.btnSendPacketNow.Name = "btnSendPacketNow";
            this.btnSendPacketNow.Size = new System.Drawing.Size(112, 38);
            this.btnSendPacketNow.TabIndex = 3;
            this.btnSendPacketNow.Text = "3: Check Pulse";
            this.btnSendPacketNow.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSendPacketNow.TextWrap = true;
            this.btnSendPacketNow.Click += new System.EventHandler(this.btnSendPacketNow_Click_1);
            // 
            // rdoBaycharger
            // 
            this.rdoBaycharger.AutoSize = true;
            this.rdoBaycharger.Location = new System.Drawing.Point(13, 43);
            this.rdoBaycharger.Name = "rdoBaycharger";
            this.rdoBaycharger.Size = new System.Drawing.Size(66, 17);
            this.rdoBaycharger.TabIndex = 1;
            this.rdoBaycharger.Text = "Charger";
            this.rdoBaycharger.UseVisualStyleBackColor = true;
            // 
            // rdoWorkstation
            // 
            this.rdoWorkstation.AutoSize = true;
            this.rdoWorkstation.Checked = true;
            this.rdoWorkstation.Location = new System.Drawing.Point(13, 20);
            this.rdoWorkstation.Name = "rdoWorkstation";
            this.rdoWorkstation.Size = new System.Drawing.Size(90, 17);
            this.rdoWorkstation.TabIndex = 0;
            this.rdoWorkstation.TabStop = true;
            this.rdoWorkstation.Text = "WorkStation";
            this.rdoWorkstation.UseVisualStyleBackColor = true;
            // 
            // txtWSSerialNumber
            // 
            this.txtWSSerialNumber.Location = new System.Drawing.Point(17, 17);
            this.txtWSSerialNumber.Name = "txtWSSerialNumber";
            this.txtWSSerialNumber.Size = new System.Drawing.Size(141, 20);
            this.txtWSSerialNumber.TabIndex = 0;
            this.txtWSSerialNumber.TextChanged += new System.EventHandler(this.txtWSSerialNumber_TextChanged);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.txtSignalQuality);
            this.groupBox15.Controls.Add(this.radLabel2);
            this.groupBox15.Controls.Add(this.radLabel1);
            this.groupBox15.Controls.Add(this.txtSSID);
            this.groupBox15.Controls.Add(this.cmbSSIDs);
            this.groupBox15.Location = new System.Drawing.Point(168, 155);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(381, 68);
            this.groupBox15.TabIndex = 317;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "SSID";
            // 
            // txtSignalQuality
            // 
            this.txtSignalQuality.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.txtSignalQuality.Location = new System.Drawing.Point(86, 22);
            this.txtSignalQuality.Name = "txtSignalQuality";
            this.txtSignalQuality.Size = new System.Drawing.Size(13, 18);
            this.txtSignalQuality.TabIndex = 22;
            this.txtSignalQuality.Text = "0";
            this.txtSignalQuality.Visible = false;
            // 
            // radLabel2
            // 
            this.radLabel2.Location = new System.Drawing.Point(9, 21);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(78, 18);
            this.radLabel2.TabIndex = 21;
            this.radLabel2.Text = "Signal Quality:";
            this.radLabel2.Visible = false;
            // 
            // radLabel1
            // 
            this.radLabel1.Location = new System.Drawing.Point(6, 44);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(32, 18);
            this.radLabel1.TabIndex = 20;
            this.radLabel1.Text = "SSID:";
            // 
            // txtSSID
            // 
            this.txtSSID.Font = new System.Drawing.Font("Segoe UI", 7F);
            this.txtSSID.Location = new System.Drawing.Point(121, 43);
            this.txtSSID.Name = "txtSSID";
            this.txtSSID.NullText = "Manually Enter SSID Here (Spaces will be converted to $)";
            this.txtSSID.Size = new System.Drawing.Size(254, 18);
            this.txtSSID.TabIndex = 19;
            // 
            // cmbSSIDs
            // 
            this.cmbSSIDs.DisplayMember = "name";
            this.cmbSSIDs.FormattingEnabled = true;
            this.cmbSSIDs.Location = new System.Drawing.Point(121, 17);
            this.cmbSSIDs.Name = "cmbSSIDs";
            this.cmbSSIDs.Size = new System.Drawing.Size(254, 21);
            this.cmbSSIDs.TabIndex = 50;
            this.cmbSSIDs.ValueMember = "name";
            this.cmbSSIDs.Visible = false;
            this.cmbSSIDs.SelectedValueChanged += new System.EventHandler(this.cmbSSIDs_SelectedValueChanged);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btnWSSNSet);
            this.groupBox10.Controls.Add(this.btnWSSNGet);
            this.groupBox10.Controls.Add(this.txtWSSerialNumber);
            this.groupBox10.Location = new System.Drawing.Point(555, 155);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(177, 68);
            this.groupBox10.TabIndex = 315;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Device Serial Number";
            // 
            // btnWSSNSet
            // 
            this.btnWSSNSet.Location = new System.Drawing.Point(94, 43);
            this.btnWSSNSet.Name = "btnWSSNSet";
            this.btnWSSNSet.Size = new System.Drawing.Size(64, 19);
            this.btnWSSNSet.TabIndex = 326;
            this.btnWSSNSet.Text = "Set Serial";
            this.btnWSSNSet.TextWrap = true;
            this.btnWSSNSet.Click += new System.EventHandler(this.btnWSSNSet_Click);
            // 
            // btnWSSNGet
            // 
            this.btnWSSNGet.Location = new System.Drawing.Point(17, 43);
            this.btnWSSNGet.Name = "btnWSSNGet";
            this.btnWSSNGet.Size = new System.Drawing.Size(64, 19);
            this.btnWSSNGet.TabIndex = 325;
            this.btnWSSNGet.Text = "Get Serial";
            this.btnWSSNGet.TextWrap = true;
            this.btnWSSNGet.Click += new System.EventHandler(this.btnWSSNGet_Click);
            // 
            // pbxSaveConfiguration
            // 
            this.pbxSaveConfiguration.Location = new System.Drawing.Point(394, 574);
            this.pbxSaveConfiguration.Name = "pbxSaveConfiguration";
            this.pbxSaveConfiguration.Size = new System.Drawing.Size(17, 17);
            this.pbxSaveConfiguration.TabIndex = 312;
            this.pbxSaveConfiguration.TabStop = false;
            // 
            // pbxRebootWiFi
            // 
            this.pbxRebootWiFi.Location = new System.Drawing.Point(417, 574);
            this.pbxRebootWiFi.Name = "pbxRebootWiFi";
            this.pbxRebootWiFi.Size = new System.Drawing.Size(17, 17);
            this.pbxRebootWiFi.TabIndex = 313;
            this.pbxRebootWiFi.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoBaycharger);
            this.groupBox3.Controls.Add(this.rdoWorkstation);
            this.groupBox3.Location = new System.Drawing.Point(168, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(115, 72);
            this.groupBox3.TabIndex = 300;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Device Type";
            // 
            // ilsLED
            // 
            this.ilsLED.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilsLED.ImageStream")));
            this.ilsLED.TransparentColor = System.Drawing.Color.Transparent;
            this.ilsLED.Images.SetKeyName(0, "LedWhite.ico");
            this.ilsLED.Images.SetKeyName(1, "LedBlue.ico");
            this.ilsLED.Images.SetKeyName(2, "LedRed.ico");
            this.ilsLED.Images.SetKeyName(3, "LedGreen.ico");
            // 
            // txtResponse
            // 
            this.txtResponse.BackColor = System.Drawing.Color.Transparent;
            this.txtResponse.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Italic);
            this.txtResponse.Location = new System.Drawing.Point(440, 573);
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(294, 20);
            this.txtResponse.TabIndex = 321;
            // 
            // txtCommand
            // 
            this.txtCommand.BackColor = System.Drawing.Color.Transparent;
            this.txtCommand.Location = new System.Drawing.Point(13, 297);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(63, 20);
            this.txtCommand.TabIndex = 322;
            this.txtCommand.Text = "Command";
            this.txtCommand.Visible = false;
            // 
            // btnUpdateRovingFirmware
            // 
            this.btnUpdateRovingFirmware.Location = new System.Drawing.Point(13, 323);
            this.btnUpdateRovingFirmware.Name = "btnUpdateRovingFirmware";
            this.btnUpdateRovingFirmware.Size = new System.Drawing.Size(114, 38);
            this.btnUpdateRovingFirmware.TabIndex = 324;
            this.btnUpdateRovingFirmware.Text = "Update WIFI Module";
            this.btnUpdateRovingFirmware.TextWrap = true;
            this.btnUpdateRovingFirmware.Click += new System.EventHandler(this.btnUpdateRovingFirmware_Click);
            // 
            // btnLoginToPulse
            // 
            this.btnLoginToPulse.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnLoginToPulse.Location = new System.Drawing.Point(168, 100);
            this.btnLoginToPulse.Name = "btnLoginToPulse";
            this.btnLoginToPulse.Size = new System.Drawing.Size(115, 49);
            this.btnLoginToPulse.TabIndex = 4;
            this.btnLoginToPulse.Text = "Login To Enable Asset Setup";
            this.btnLoginToPulse.TextWrap = true;
            this.btnLoginToPulse.Click += new System.EventHandler(this.btnLoginToPulse_Click_1);
            // 
            // cbUSBConnected
            // 
            this.cbUSBConnected.AutoCheck = false;
            this.cbUSBConnected.AutoSize = true;
            this.cbUSBConnected.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.cbUSBConnected.Font = new System.Drawing.Font("Segoe UI", 7.25F, System.Drawing.FontStyle.Bold);
            this.cbUSBConnected.Location = new System.Drawing.Point(177, 573);
            this.cbUSBConnected.Name = "cbUSBConnected";
            this.cbUSBConnected.Size = new System.Drawing.Size(141, 16);
            this.cbUSBConnected.TabIndex = 325;
            this.cbUSBConnected.Text = "USB Connected To Device";
            this.cbUSBConnected.UseVisualStyleBackColor = true;
            // 
            // btnSaveChanges
            // 
            this.btnSaveChanges.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSaveChanges.Location = new System.Drawing.Point(13, 150);
            this.btnSaveChanges.Name = "btnSaveChanges";
            this.btnSaveChanges.Size = new System.Drawing.Size(112, 38);
            this.btnSaveChanges.TabIndex = 2;
            this.btnSaveChanges.Text = "2: Save Changes";
            this.btnSaveChanges.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveChanges.TextWrap = true;
            this.btnSaveChanges.Click += new System.EventHandler(this.btnSaveChanges_Click);
            // 
            // btnConnectUSB
            // 
            this.btnConnectUSB.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.btnConnectUSB.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnConnectUSB.Image = ((System.Drawing.Image)(resources.GetObject("btnConnectUSB.Image")));
            this.btnConnectUSB.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConnectUSB.Location = new System.Drawing.Point(13, 101);
            this.btnConnectUSB.Name = "btnConnectUSB";
            this.btnConnectUSB.Size = new System.Drawing.Size(112, 38);
            this.btnConnectUSB.TabIndex = 1;
            this.btnConnectUSB.Text = "1: Connect Device";
            this.btnConnectUSB.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConnectUSB.TextWrap = true;
            this.btnConnectUSB.Click += new System.EventHandler(this.radButton1_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnConnectUSB.GetChildAt(0))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image")));
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnConnectUSB.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnConnectUSB.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnConnectUSB.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadButtonElement)(this.btnConnectUSB.GetChildAt(0))).Text = "1: Connect Device";
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnConnectUSB.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.btnConnectUSB.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ScaleTransform = new System.Drawing.SizeF(0.17F, 0.17F);
            // 
            // pbConnectedToUSB
            // 
            this.pbConnectedToUSB.Image = ((System.Drawing.Image)(resources.GetObject("pbConnectedToUSB.Image")));
            this.pbConnectedToUSB.Location = new System.Drawing.Point(130, 106);
            this.pbConnectedToUSB.Name = "pbConnectedToUSB";
            this.pbConnectedToUSB.Size = new System.Drawing.Size(30, 29);
            this.pbConnectedToUSB.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbConnectedToUSB.TabIndex = 326;
            this.pbConnectedToUSB.TabStop = false;
            // 
            // pbChangesSaved
            // 
            this.pbChangesSaved.Image = ((System.Drawing.Image)(resources.GetObject("pbChangesSaved.Image")));
            this.pbChangesSaved.Location = new System.Drawing.Point(130, 154);
            this.pbChangesSaved.Name = "pbChangesSaved";
            this.pbChangesSaved.Size = new System.Drawing.Size(30, 29);
            this.pbChangesSaved.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbChangesSaved.TabIndex = 327;
            this.pbChangesSaved.TabStop = false;
            this.pbChangesSaved.Visible = false;
            // 
            // pbConnectedToPulse
            // 
            this.pbConnectedToPulse.Image = ((System.Drawing.Image)(resources.GetObject("pbConnectedToPulse.Image")));
            this.pbConnectedToPulse.Location = new System.Drawing.Point(130, 203);
            this.pbConnectedToPulse.Name = "pbConnectedToPulse";
            this.pbConnectedToPulse.Size = new System.Drawing.Size(30, 29);
            this.pbConnectedToPulse.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbConnectedToPulse.TabIndex = 328;
            this.pbConnectedToPulse.TabStop = false;
            this.pbConnectedToPulse.Visible = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(-157, -165);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(475, 388);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 329;
            this.pictureBox1.TabStop = false;
            // 
            // radButton1
            // 
            this.radButton1.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radButton1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radButton1.Image = ((System.Drawing.Image)(resources.GetObject("radButton1.Image")));
            this.radButton1.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton1.Location = new System.Drawing.Point(13, 254);
            this.radButton1.Name = "radButton1";
            this.radButton1.Size = new System.Drawing.Size(112, 38);
            this.radButton1.TabIndex = 330;
            this.radButton1.Text = "4: Done";
            this.radButton1.TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            this.radButton1.TextWrap = true;
            this.radButton1.Click += new System.EventHandler(this.radButton1_Click_1);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton1.GetChildAt(0))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image1")));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton1.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton1.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleLeft;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton1.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton1.GetChildAt(0))).Text = "4: Done";
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton1.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ScaleTransform = new System.Drawing.SizeF(0.17F, 0.17F);
            // 
            // tmrCommCheck
            // 
            this.tmrCommCheck.Interval = 2000;
            this.tmrCommCheck.Tick += new System.EventHandler(this.tmrCommCheck_Tick);
            // 
            // btnSaveNetwork
            // 
            this.btnSaveNetwork.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.btnSaveNetwork.Location = new System.Drawing.Point(13, 411);
            this.btnSaveNetwork.Name = "btnSaveNetwork";
            this.btnSaveNetwork.Size = new System.Drawing.Size(114, 52);
            this.btnSaveNetwork.TabIndex = 53;
            this.btnSaveNetwork.Text = "Save Facility SSID/Passkey to Pulse";
            this.btnSaveNetwork.TextWrap = true;
            this.btnSaveNetwork.Visible = false;
            this.btnSaveNetwork.Click += new System.EventHandler(this.btnSaveNetwork_Click);
            // 
            // radButton3
            // 
            this.radButton3.DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            this.radButton3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold);
            this.radButton3.Image = ((System.Drawing.Image)(resources.GetObject("radButton3.Image")));
            this.radButton3.ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            this.radButton3.Location = new System.Drawing.Point(13, 367);
            this.radButton3.Name = "radButton3";
            this.radButton3.Size = new System.Drawing.Size(112, 38);
            this.radButton3.TabIndex = 331;
            this.radButton3.Text = "Test Network";
            this.radButton3.TextWrap = true;
            this.radButton3.Click += new System.EventHandler(this.radButton3_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton3.GetChildAt(0))).Image = ((System.Drawing.Image)(resources.GetObject("resource.Image2")));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton3.GetChildAt(0))).ImageAlignment = System.Drawing.ContentAlignment.MiddleRight;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton3.GetChildAt(0))).TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton3.GetChildAt(0))).DisplayStyle = Telerik.WinControls.DisplayStyle.Text;
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton3.GetChildAt(0))).Text = "Test Network";
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton3.GetChildAt(0).GetChildAt(1).GetChildAt(0))).Visibility = Telerik.WinControls.ElementVisibility.Collapsed;
            ((Telerik.WinControls.Primitives.ImagePrimitive)(this.radButton3.GetChildAt(0).GetChildAt(1).GetChildAt(0))).ScaleTransform = new System.Drawing.SizeF(0.17F, 0.17F);
            // 
            // PACU
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(255)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(751, 610);
            this.Controls.Add(this.radButton3);
            this.Controls.Add(this.btnSaveNetwork);
            this.Controls.Add(this.radButton1);
            this.Controls.Add(this.pbConnectedToPulse);
            this.Controls.Add(this.pbxSaveConfiguration);
            this.Controls.Add(this.pbChangesSaved);
            this.Controls.Add(this.pbxRebootWiFi);
            this.Controls.Add(this.pbConnectedToUSB);
            this.Controls.Add(this.btnSendPacketNow);
            this.Controls.Add(this.btnConnectUSB);
            this.Controls.Add(this.btnSaveChanges);
            this.Controls.Add(this.cbUSBConnected);
            this.Controls.Add(this.btnLoginToPulse);
            this.Controls.Add(this.btnUpdateRovingFirmware);
            this.Controls.Add(this.txtCommand);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.groupBox15);
            this.Controls.Add(this.gbPulseAssetSetup);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.gboxPulseStatus);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "PACU";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.ShowIcon = false;
            this.Text = "PACU";
            this.ThemeName = "ControlDefault";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PACU_FormClosing);
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gboxPulseStatus.ResumeLayout(false);
            this.gboxPulseStatus.PerformLayout();
            this.gbPulseAssetSetup.ResumeLayout(false);
            this.gbPulseAssetSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlDepartments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSendPacketNow)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSignalQuality)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSSID)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnWSSNSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnWSSNGet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtResponse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnUpdateRovingFirmware)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnLoginToPulse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnConnectUSB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnectedToUSB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbChangesSaved)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbConnectedToPulse)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnSaveNetwork)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.RadioButton rdo13ASCII;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtChannel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.RadioButton rdo26HEX;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rdoHex;
        private System.Windows.Forms.RadioButton rdoASCII;
        private System.Windows.Forms.Label lblSSID2;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.RadioButton rdoMixedWPA;
        private System.Windows.Forms.TextBox txtDeviceIP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassPhrase;
        private System.Windows.Forms.TextBox txtWEPKey;
        private System.Windows.Forms.RadioButton rdoWPA2;
        private System.Windows.Forms.PictureBox pbxCommunicating;
        private System.Windows.Forms.RadioButton rdoWPA1;
     
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtDNSServer;
        private System.Windows.Forms.TextBox txtGateway;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.TextBox txtSubnetMask;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.Label label1;

        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdoWEP128;
        private System.Windows.Forms.RadioButton rdoOpen;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoDHCPOn;
        private System.Windows.Forms.RadioButton rdoDHCPOff;
        private System.Windows.Forms.GroupBox gboxPulseStatus;
        private System.Windows.Forms.TextBox txtSiteIP;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtLastPostDate;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.RadioButton rdoBaycharger;
        private System.Windows.Forms.RadioButton rdoWorkstation;
        private System.Windows.Forms.TextBox txtWSSerialNumber;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.ComboBox cmbSSIDs;
        private System.Windows.Forms.GroupBox gbPulseAssetSetup;
        private System.Windows.Forms.TextBox txtFloor;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtWing;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtAssetNumber;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.PictureBox pbxSaveConfiguration;
        private System.Windows.Forms.PictureBox pbxRebootWiFi;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ImageList ilsLED;
 
        private Telerik.WinControls.UI.RadTextBoxControl txtResponse;
        private Telerik.WinControls.UI.RadTextBoxControl txtCommand;
        private Telerik.WinControls.UI.RadButton btnSendPacketNow;
        private Telerik.WinControls.UI.RadButton btnUpdateRovingFirmware;
        private Telerik.WinControls.UI.RadTextBox txtSSID;
        private System.Windows.Forms.Label label6;
        private Telerik.WinControls.UI.RadDropDownList ddlSites;
        private Telerik.WinControls.UI.RadDropDownList ddlDepartments;
        private Telerik.WinControls.UI.RadButton btnWSSNGet;
        private Telerik.WinControls.UI.RadButton btnWSSNSet;
        private Telerik.WinControls.UI.RadButton btnLoginToPulse;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadButton btnSaveAsset;
        private System.Windows.Forms.CheckBox cbUSBConnected;
        private Telerik.WinControls.UI.RadButton btnSaveChanges;
        private Telerik.WinControls.UI.RadButton btnConnectUSB;
        private System.Windows.Forms.PictureBox pbConnectedToPulse;
        private System.Windows.Forms.PictureBox pbConnectedToUSB;
        private System.Windows.Forms.PictureBox pbChangesSaved;
        private System.Windows.Forms.Label txtCommunicatingToPulse;
        private System.Windows.Forms.TextBox txtLastSuccessfulPacket;
        private System.Windows.Forms.Label label5;
        private Telerik.WinControls.UI.RadLabel txtSignalQuality;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadButton radButton1;
        private System.Windows.Forms.Timer tmrCommCheck;
        private Telerik.WinControls.UI.RadButton radButton2;
        private System.Windows.Forms.Label txtAssignedSite;
        private Telerik.WinControls.UI.RadButton btnSaveNetwork;
        private Telerik.WinControls.UI.RadButton radButton3;

    }
}
