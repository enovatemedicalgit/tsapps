﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using System.IO;
using System.Threading;
using at_usb_api;
using VistaButton;
//using HospWifi.Win32;
using NativeWifi;
using System.Net;
using System.Reflection;

namespace HospWifi
{
    public partial class PACU : Telerik.WinControls.UI.RadForm
    {
        string TVersion = "1.41";
        string VID = "03EB";
        string PID = "204F";
        string sReply = "";
        bool DataReceived = false;
        bool USBConnected = false;
        int bformClosed = 0;
        at_usb_api.at_usb_api atusb = new at_usb_api.at_usb_api();
        private static Wifi wifi;

        string WEP_KEY = "";
        string PASS_KEY = "";
        int ExperienceLevel = 2;
        bool WDebug = false;
        public string PulseURL = "https://Pulse.myenovate.com";
        public string PulseLAUrl = "https://software.myenovate.com/sla.pdf";
        string SubstituteChar = "$";
        string ProgName = "PulseWifi";
        string sSuccessfulNetworkCredentials = "0";
        public string sUsername { get; set; }
             public string sUserID { get; set; }
        public string sPassword { get; set; }

        public string sSiteID { get; set; }

        public static int iAuthProtocol = 3;
        public static string sSSID = "";
        public static string DSN = "";
        bool _continue = false;
        string USBData = "";
        Boolean bLoggedIn = false;

        delegate void SetTextCallback(string text);

        System.Threading.Thread readThread;

        public static string APIUrl { get; set; }

        private AppDomain _currentDomain = AppDomain.CurrentDomain;
        Wlan.WlanAvailableNetwork[] networks = null;
        List<Wlan.WlanAvailableNetwork> NetworksFiltered = new List<Wlan.WlanAvailableNetwork>();
        ApiLayer api = new ApiLayer();
        int itmrtick = 0;
        Boolean bRememberMe = true;

        public PACU()
        {
            try
            {
                InitializeComponent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
            this.StartPosition = FormStartPosition.CenterScreen;
            LoadExt();
        }

        public void LoadExt()
        {
            toolTip1.SetToolTip(txtPassPhrase, SubstituteChar + " character will be replaced with a space.");

            //ThemeResolutionService.ApplicationThemeName = "Windows8";
            ScanNetworks();

            cmbSSIDs.DataSource = List();

            APIUrl = Properties.Settings.Default.APIURL;
            pbxSaveConfiguration.Image = ilsLED.Images[0];
            pbxRebootWiFi.Image = ilsLED.Images[0];
            pbxCommunicating.Image = ilsLED.Images[2];
            _currentDomain.UnhandledException += MYExnHandler;

            USBConnected = false;
            cbUSBConnected.Checked = false;
            btnConnectUSB.Enabled = true;
            pbConnectedToUSB.Visible = false;
            txtResponse.Text = "Not Connected: Connect USB to Device and press Connect";
            btnConnectUSB.Enabled = true;
            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            string strVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                strVersion = ad.CurrentVersion.ToString();
            }
            else
            {
                strVersion = assemName.Version.ToString();
            }

            this.Text = "Enovate Medical - Pulse Asset Configuration Utility " + System.String.Format("{0} ({1})", assemName.Name, strVersion);

            btnLoginToPulse.Enabled = true;

            if (!atusb.FoundDLL)
                this.Close();

            LoadParams();

            if (SubstituteChar.Length < 1)
                SubstituteChar = "$";

            if (USBConnected)
            {
                txtCommand.Text = "6X";
                atusb.WriteBytes("6X");
                WaitForData();
                if (sReply.Contains("W"))
                {
                    rdoWorkstation.Checked = true;
                }
                if (sReply.Contains("B"))
                {
                    rdoBaycharger.Checked = true;
                }
            }
        }

        static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength).TrimStart().TrimEnd();
        }

        private static IEnumerable<AccessPoint> List()
        {
            try
            {
                IEnumerable<AccessPoint> accessPoints = wifi.GetAccessPoints();
                return accessPoints;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void ScanNetworks()
        {
            try
            {
                WlanClient client = new WlanClient();
                List<String> SSIDS = new List<String>();

                cmbSSIDs.DataSource = List();
                //foreach (AccessPoint ap in List()){
                //          SSIDS.Add(ap.Name);
                //          NetworksFiltered.Add(ap.Network);
                //      }
                foreach (NativeWifi.WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    // Lists all available networks
                    networks = wlanIface.GetAvailableNetworkList(0);

                    //List<String> SSIDS = new List<String>();

                    wlanIface.GetAvailableNetworkList(0);
                    foreach (Wlan.WlanAvailableNetwork network in networks)
                    {
                        if (GetStringForSSID(network.dot11Ssid).Length > 3 && GetStringForSSID(network.dot11Ssid) != "" && GetStringForSSID(network.dot11Ssid) != null)
                        {
                            if (GetStringForSSID(network.dot11Ssid).Trim().Length > 4)
                            {
                                SSIDS.Add(GetStringForSSID(network.dot11Ssid));
                                NetworksFiltered.Add(network);
                            }
                        }
                        else
                        {
                        }
                    }
                    cmbSSIDs.DataSource = SSIDS;
                }
            }
            catch (Exception ex)
            {
                //   MessageBox.Show("Cannot detect any Wifi Networks: " + ex.Message.ToString()); //Wifi Not Available
            }
        }

        #region btnClose_Click

        /****************************************************************************
        btnClose_Click                                         10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void btnClose_Click(object sender, EventArgs e)
        {
            _continue = false;

            if (USBConnected)
                atusb.CloseDevice();

            this.Close();
        }

        /*--- end of btnClose_Click() ---------------------------------------------*/

        #endregion

        #region WriteCommand

        /****************************************************************************
        WriteCommand                                             11/08/06
        Description: Writes debug info to a text file.
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void WriteCommand(string command)
        {
            if (!WDebug)
                return;

            try
            {
                StreamWriter textOut =
                    new StreamWriter(new FileStream("Debug.log",
                        FileMode.Append,
                        FileAccess.Write));

                textOut.WriteLine("HospWiFi: " + DateTime.Now.ToString());
                textOut.WriteLine(" Command: " + command);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
            }

            return;
        }

        /*--- end of WriteCommand() -----------------------------------------------*/

        #endregion

        #region WriteResponse

        /****************************************************************************
        WriteResponse                                            11/08/06
        Description: Writes debug info to a text file.
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void WriteResponse(string response)
        {
            if (!WDebug)
                return;

            try
            {
                StreamWriter textOut =
                    new StreamWriter(new FileStream("Debug.log",
                        FileMode.Append,
                        FileAccess.Write));

                textOut.WriteLine("HospWiFi: " + DateTime.Now.ToString());
                textOut.WriteLine("Response: " + response);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
            }

            return;
        }

        /*--- end of WriteResponse() ----------------------------------------------*/

        #endregion

        #region GetParam

        /****************************************************************************
        GetParam                                               08/02/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private string GetParam(StreamReader textIn, string key)
        {
            string sline = "";
            string value = "";

            while (true)
            {
                try
                {
                    sline = textIn.ReadLine();
                }
                catch
                {
                    break;
                }

                if (sline == null)
                    break;

                if (sline.Contains(key))
                {
                    int ipos = sline.IndexOf('=');
                    if (ipos > 0)
                    {
                        try
                        {
                            value = sline.Substring(ipos + 1);
                        }
                        catch
                        {
                            value = "";
                        }
                        break;
                    }
                }
            }

            return value;
        }

        /*--- end of GetParam() ---------------------------------------------------*/

        #endregion

        #region LoadParams

        /****************************************************************************
        LoadParams                                             08/02/10
        Description: Read the saved params and load the textboxes. 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        public void LoadParams()
        {
            StreamReader textIn;
            string sTemp = "";

            try
            {
                textIn = new StreamReader(new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\PulseWifi.cfg", FileMode.Open, FileAccess.Read));
            }
            catch
            {
                return;
            }

            sTemp = GetParam(textIn, "DHCP");
            sUsername = GetParam(textIn, "USERNAME");
            sPassword = GetParam(textIn, "PASSWORD");
            sTemp = GetParam(textIn, "FIRSTLAUNCH");
            if (sTemp != "0" && sTemp != "")
            {
                MessageBox.Show("BY DOWNLOADING, INSTALLING, LOGGING INTO, OR USING THE PULSEWIFI APPLICATION OR PULSE SYSTEM, YOU ARE AGREEING TO ALL THE TERMS AND CONDITIONS OF THE AGREEMENT AT http://download.myenovate.com/SLA.pdf, INCLUDING THE LIMITATIONS ON LIABILITY SET FORTH HEREIN.  IF YOU DO NOT AGREE WITH ALL OF THE TERMS AND CONDITIONS OF THIS AGREEMENT, YOU ARE NOT PERMITTED TO DOWNLOAD, INSTALL OR USE ANY ENOVATE MEDICAL APPLICATION. ");
                System.Diagnostics.Process process1 = new System.Diagnostics.Process();
                process1.StartInfo.Arguments = PulseLAUrl;
                process1.StartInfo.FileName = "IExplore.exe";
                process1.Start();
            }
            if (sUsername.Length > 1)
            {
                bRememberMe = true;
            }
            sTemp = GetParam(textIn, "DHCP");
            if (sTemp == "0")
            {
                rdoDHCPOff.Checked = true;
            }
            if (sTemp == "1")
            {
                rdoDHCPOn.Checked = true;
            }

            txtIPAddress.Text = GetParam(textIn, "IPADDRESS");

            sTemp = GetParam(textIn, "SSID");
            //sTemp = sTemp.Replace( '$', ' ' );
            txtSSID.Text = sTemp;

            //txtUserID.Text = GetParam( textIn, "USERID" );
            //string unc = GetParam( textIn, "PASSWORD" );
            //txtPassword.Text = ADeCode( unc );
            //sTemp = GetParam( textIn, "REMEMBER" );
            //if ( sTemp == "Yes" )
            //   ckbRememberMe.Checked = true;
            //else
            //   ckbRememberMe.Checked = false;

            sTemp = GetParam(textIn, "AUTHEN");
            txtPassPhrase.Enabled = true;
            txtWEPKey.Enabled = false;
            if (sTemp == "0")
            {
                rdoOpen.Checked = true;

                txtWEPKey.Enabled = false;

                txtPassPhrase.Enabled = false;
            }
            if (sTemp == "1")
            {
                rdoWEP128.Checked = true;

                txtWEPKey.Enabled = true;

                txtPassPhrase.Enabled = false;
            }
            if (sTemp == "2")
            {
                rdoWPA1.Checked = true;

                txtWEPKey.Enabled = false;

                txtPassPhrase.Enabled = true;
            }
            if (sTemp == "3")
            {
                rdoMixedWPA.Checked = true;

                txtWEPKey.Enabled = false;

                txtPassPhrase.Enabled = true;
            }
            if (sTemp == "4")
            {
                rdoWPA2.Checked = true;

                txtWEPKey.Enabled = false;

                txtPassPhrase.Enabled = true;
            }

            sTemp = GetParam(textIn, "WEPKEY");
            string uenc = "";
            try
            {
                uenc = HospWifi.Crypto.DecryptStringAES(sTemp, ProgName);
            }
            catch
            {
                uenc = "";
            }
            txtWEPKey.Text = uenc;

            if (txtWEPKey.Text.Length == 26)
                rdo26HEX.Checked = true;

            sTemp = GetParam(textIn, "PASSPHRASE");
            try
            {
                uenc = HospWifi.Crypto.DecryptStringAES(sTemp, ProgName);
            }
            catch
            {
                uenc = "";
            }
            //uenc = uenc.Replace( '$', ' ' );
            txtPassPhrase.Text = uenc;

            bool isHex = true;
            // is the WEP key currently in ASCII or Hex? 
            foreach (char c in txtPassPhrase.Text.Trim())
            {
                if (c < 0x30)
                {
                    isHex = false;
                    break;
                }
                if (c > 0x39 && c < 0x41)
                {
                    isHex = false;
                    break;
                }
                if (c > 0x46 && c < 0x61)
                {
                    isHex = false;
                    break;
                }
                if (c > 0x66)
                {
                    isHex = false;
                    break;
                }
            }

            if (isHex)
                rdoHex.Checked = true;

            txtChannel.Text = GetParam(textIn, "CHANNEL");
            if (txtChannel.Text.Length < 1)
            {
                txtChannel.Text = "0";
            }

            sTemp = GetParam(textIn, "LEVEL");
            // ExperienceLevel = int.Parse(sTemp);

            // Set menu check 
            //if ( ExperienceLevel == 1 )    // beginner 
            //{
            //   beginnerToolStripMenuItem1.Checked = true;
            //   expertToolStripMenuItem1.Checked = false;
            //   ExpertPanel.Visible = false;
            //   BeginnerPanel.Visible = true;
            //   //webBrowserToolStripMenuItem.Visible = false;
            //}

            sTemp = GetParam(textIn, "DEVICE");
            if (sTemp == "BayCharger")
            {
                rdoBaycharger.Checked = true;

                rdoWorkstation.Checked = false;
            }
            else
            {
                rdoBaycharger.Checked = false;

                rdoWorkstation.Checked = true;
            }

            PulseURL = GetParam(textIn, "URL");

            if (PulseURL.Contains("stinger"))
                PulseURL = "https://pulse.myenovate.com";

            txtGateway.Text = GetParam(textIn, "GATEWAY");

            PulseURL = "https://pulse.myenovate.com";
            txtSubnetMask.Text = GetParam(textIn, "SUBNETMASK");

            txtDNSServer.Text = GetParam(textIn, "DNSSERVER");

            rdo13ASCII.Checked = true;
            rdoASCII.Checked = true;
            try
            {
                SubstituteChar = GetParam(textIn, "SUBCHAR");
            }
            catch (Exception ex)
            {

            }
            //webBrowser1.Url = new Uri( PulseURL );

            textIn.Close();

            return;
        }

        /*--- end of LoadParams() -------------------------------------------------*/

        #endregion

        #region SaveParams

        /****************************************************************************
        SaveParams                                             08/02/10
        Description: Save the data in the textboxes.
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        public void SaveParams()
        {
            StreamWriter textOut;

            try
            {
                textOut = new StreamWriter(new FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).ToString() + "\\PulseWifi.cfg", FileMode.Create, FileAccess.ReadWrite));
            }
            catch
            {
                MessageBox.Show("Cannot save to C:\\PulseWifi.cfg file; please make sure you have permission to write to this file.");
                return;
            }

            if (rdoDHCPOff.Checked)
            {
                textOut.WriteLine("DHCP=0");
            }
            else
            {
                textOut.WriteLine("DHCP=1");
            }

            textOut.WriteLine("IPADDRESS=" + txtIPAddress.Text);
            textOut.WriteLine("SSID=" + txtSSID.Text);

            //if ( ckbRememberMe.Checked )
            //{
            //   textOut.WriteLine( "USERID=" + txtUserID.Text );
            //   string enc = AEnCode( txtPassword.Text );
            //   textOut.WriteLine( "PASSWORD=" + enc );
            //   textOut.WriteLine( "REMEMBER=Yes" );
            //}
            //else
            //{
            //   textOut.WriteLine( "USERID=" );
            //   textOut.WriteLine( "PASSWORD=" );
            //   textOut.WriteLine( "REMEMBER=No" );
            //}

            if (rdoOpen.Checked)
                textOut.WriteLine("AUTHEN=0");
            if (rdoWEP128.Checked)
                textOut.WriteLine("AUTHEN=1");
            if (rdoWPA1.Checked)
                textOut.WriteLine("AUTHEN=2");
            if (rdoMixedWPA.Checked)
                textOut.WriteLine("AUTHEN=3");
            if (rdoWPA2.Checked)
                textOut.WriteLine("AUTHEN=4");

            string enc = "";

            try
            {
                enc = HospWifi.Crypto.EncryptStringAES(txtWEPKey.Text, ProgName);
            }
            catch
            {
                enc = "";
            }
            textOut.WriteLine("WEPKEY=" + enc);
            try
            {
                enc = HospWifi.Crypto.EncryptStringAES(txtPassPhrase.Text, ProgName);
            }
            catch
            {
                enc = "";
            }
            textOut.WriteLine("PASSPHRASE=" + enc);
            textOut.WriteLine("CHANNEL=" + txtChannel.Text);

            textOut.WriteLine("LEVEL=" + ExperienceLevel.ToString());

            if (rdoBaycharger.Checked)
                textOut.WriteLine("DEVICE=BayCharger");
            if (rdoWorkstation.Checked)
                textOut.WriteLine("DEVICE=WorkStation");

            textOut.WriteLine("URL=" + PulseURL);

            textOut.WriteLine("GATEWAY=" + txtGateway.Text);
            textOut.WriteLine("SUBNETMASK=" + txtSubnetMask.Text);
            textOut.WriteLine("DNSSERVER=" + txtDNSServer.Text);
            textOut.WriteLine("FIRSTLAUNCH=0");
            try
            {
                textOut.WriteLine("SUBCHAR=" + SubstituteChar);
            }
            catch
            {

            }
            if (bRememberMe == true)
            {
                textOut.WriteLine("USERNAME=" + sUsername);
                textOut.WriteLine("PASSWORD=" + sPassword);
            }
            textOut.Close();
            return;
        }

        /*--- end of SaveParams() -------------------------------------------------*/

        #endregion

        #region btnResetLED_Click

        /****************************************************************************
        btnResetLED_Click                                      10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void btnResetLED_Click(object sender, EventArgs e)
        {
            pbxSaveConfiguration.Image = ilsLED.Images[0];
            pbxRebootWiFi.Image = ilsLED.Images[0];
        }

        /*--- end of btnResetLED_Click() ------------------------------------------*/

        #endregion

        #region rdoOpen_CheckedChanged

        /****************************************************************************
        rdoOpen_CheckedChanged                                 10/30/10
        Description: Open Authentication on the Experienced screen,
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void rdoOpen_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoOpen.Checked)
            {
                txtWEPKey.Enabled = false;
                txtPassPhrase.Enabled = false;
                groupBox8.Enabled = false;
                rdoASCII.Enabled = false;
                rdoHex.Enabled = false;
            }
            if (rdoWEP128.Checked)
            {
                txtWEPKey.Enabled = true;
                txtPassPhrase.Enabled = false;
                groupBox8.Enabled = true;
                rdoASCII.Enabled = false;
                rdoHex.Enabled = false;
                txtWEPKey.Focus();
            }
            if (rdoWPA1.Checked)
            {
                txtWEPKey.Enabled = false;
                txtPassPhrase.Enabled = true;
                groupBox8.Enabled = false;
                rdoASCII.Enabled = true;
                rdoHex.Enabled = true;
                txtPassPhrase.Focus();
            }
            if (rdoMixedWPA.Checked)
            {
                txtWEPKey.Enabled = false;
                txtPassPhrase.Enabled = true;
                groupBox8.Enabled = false;
                rdoASCII.Enabled = true;
                rdoHex.Enabled = true;
                txtPassPhrase.Focus();
            }
            if (rdoWPA2.Checked)
            {
                txtWEPKey.Enabled = false;
                txtPassPhrase.Enabled = true;
                groupBox8.Enabled = false;
                rdoASCII.Enabled = true;
                rdoHex.Enabled = true;
                txtPassPhrase.Focus();
            }
        }

        /*--- end of rdoOpen_CheckedChanged() -------------------------------------*/

        #endregion

        #region rdoDHCPOff_CheckedChanged

        /****************************************************************************
        rdoDHCPOff_CheckedChanged                              10/30/10
        Description: DHCP on the Experienced screen.
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void rdoDHCPOff_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoDHCPOff.Checked)
            {
                txtIPAddress.Enabled = true;
                txtGateway.Enabled = true;
                txtSubnetMask.Enabled = true;
                txtDNSServer.Enabled = true;
                txtIPAddress.Focus();
            }
            else
            {
                rdoDHCPOn.Checked = true;

                txtIPAddress.Enabled = false;
                txtGateway.Enabled = false;
                txtSubnetMask.Enabled = false;
                txtDNSServer.Enabled = false;
            }
        }

        /*--- end of rdoDHCPOff_CheckedChanged() ----------------------------------*/

        #endregion

        #region ReadUSB

        /****************************************************************************
        ReadUSB                                                05/10/12
        Description: USB thread
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: Runs continuously while program is running
        1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
        012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
        "FF 0B 20 55 02 04 7E 02 01 7E D1 03 56 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
        ****************************************************************************/

        public void ReadUSB()
        {
            while (_continue)
            {
                Thread.Sleep(1);
                try
                {
                    USBData = atusb.ReadBytes();
                    if (USBData.Length > 2)
                    {
                        //PacketsReceived++;
                        SetText(USBData);
                    }
                }
                catch
                {
                }
            }
        }

        /*--- end of ReadUSB() ----------------------------------------------------*/

        #endregion

        #region SetText

        /****************************************************************************
        SetText                                                01/27/09
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: This handles displaying the contents of SerialIn as a string
        if needed.
        ****************************************************************************/

        private void SetText(string text)
        {
            // InvokeRequired compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (this.InvokeRequired)
            {
                SetTextCallback stc = new SetTextCallback(SetText);
                this.Invoke(stc, new object[] { text });
            }
            else
            {
                ProcessCommands(text);
                DataReceived = true;
            }
        }

        /*--- end of SetText() ----------------------------------------------------*/

        #endregion

        #region DeviceSendCommand

        /****************************************************************************
        DeviceSendCommand                                       08/02/10
        Description: Wrapper function for atusb.WriteBytes().
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void DeviceSendCommand(string Command)
        {
            string nCommand = Command;

            if (rdoBaycharger.Checked)
            {
                // insert a 0     6E$$$ -> 6E0$$$ 
                nCommand = Command.Substring(0, 2) + "0" + Command.Substring(2);
            }

            txtResponse.Text = "";
            txtResponse.Refresh();

            txtCommand.Text = nCommand;
            txtCommand.Refresh();

            DataReceived = false;

            if (USBConnected)
            {
                WriteCommand(nCommand);
                atusb.WriteBytes(nCommand);
            }

            return;
        }

        /*--- end of DeviceSendCommand() ------------------------------------------*/

        #endregion

        #region DeviceSendCommand

        /****************************************************************************
        DeviceSendCommand                                       08/02/10
        Description: Wrapper function for atusb.WriteBytes().
        Parameters: string Command: command to execute
        string resp: expected response
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private bool DeviceSendCommand(string Command, string resp)
        {
            string nCommand = Command;
            int tries = 0;
            bool result = false;

            if (rdoBaycharger.Checked)
            {
                // insert a 0     6E$$$ -> 6E0$$$ 
                nCommand = Command.Substring(0, 2) + "0" + Command.Substring(2);
            }

            txtResponse.Text = "";
            txtResponse.BackColor = Color.White;
            txtResponse.Refresh();

            txtCommand.Text = nCommand;
            txtCommand.Refresh();

            DataReceived = false;

            if (USBConnected)
            {
                WriteCommand(nCommand);
                atusb.WriteBytes(nCommand);
            }

            tries = 0;
            while (!DataReceived && USBConnected)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(10);
                tries++;
                if (tries > 300)   // 3 seconds 
                {
                    result = false;
                    break;
                }
            }

            System.Threading.Thread.Sleep(100);

            if (sReply.Contains(resp))
            {
                //ProcessCommands();
                //sReply = "";
                result = true;
            }

            return result;
        }

        /*--- end of DeviceSendCommand() ------------------------------------------*/

        #endregion

        #region WaitForData

        /****************************************************************************
        WaitForData                                            09/08/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private bool WaitForData()
        {
            int tries = 0;

            while (!DataReceived && USBConnected)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(10);
                tries++;
                if (tries > 300)   // 3 seconds 
                    return false;
            }

            return true;
        }

        /*--- end of WaitForData() -----------------------------------------------*/

        #endregion

        #region WaitForThisData

        /****************************************************************************
        WaitForThisData                                        09/08/10
        Description: Waits until the expected response is received from the device.
        Tries 4 times to perform the command.
        Parameters: string resp: the expected response 
        Return Value: true if found, false otherwise 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private bool WaitForThisData(string resp)
        {
            int tries = 0;
            bool result = false;

            while (!DataReceived && USBConnected)
            {
                Application.DoEvents();
                System.Threading.Thread.Sleep(10);
                tries++;
                if (tries > 300)   // 3 seconds 
                {
                    result = false;
                    break;
                }
            }

            System.Threading.Thread.Sleep(100);

            if (sReply.Contains(resp))
                result = true;

            return result;
        }

        /*--- end of WaitForThisData() -------------------------------------------*/

        #endregion

        #region ProcessCommands

        /****************************************************************************
        ProcessCommands                                        02/25/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void ProcessCommands(string Reply)
        {
            sReply = Reply;

            char Device = sReply[0];
            char Command = sReply[1];

            WriteResponse(sReply);

            if (rdoBaycharger.Checked)
                txtResponse.Text = sReply.Substring(3);    // bay number is 3rd 
            else
                txtResponse.Text = sReply.Substring(2);

            if (Command == 'O')
            {
                if (rdoBaycharger.Checked)
                    txtWSSerialNumber.Text = sReply.Substring(3);
                else
                    txtWSSerialNumber.Text = sReply.Substring(2);
            }

            txtResponse.Refresh();

            return;
        }

        /*--- end of ProcessCommands() --------------------------------------------*/

        #endregion

        #region CommandMode

        private void CommandMode()
        {
            string command = "6E$$$";
            int tries = 0;
            bool result = false;

            while (!result)
            {
                result = DeviceSendCommand(command, "<");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot set Command Mode";
                    txtResponse.BackColor = Color.Pink;

                    return;
                }
            }
        }

        /*--- end of btnCommandMode ---------------------------------------*/

        #endregion

        #region SetSSID

        /****************************************************************************
        btnSetSSID_Click                                       10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void SetSSID()
        {
            string command = "6Eset w s ";

            if (cmbSSIDs.SelectedValue != null)
            {
                sSSID = cmbSSIDs.SelectedValue.ToString().Replace(" ", "$");
                if (sSSID.Length > 2)
                {
                    command = "6Eset w s " + cmbSSIDs.SelectedValue.ToString().Replace(" ", "$");
                }
                else
                {
                    sSSID = txtSSID.Text.Replace(" ", "$");
                    command = "6Eset w s " + txtSSID.Text.Replace(" ", "$");
                }
            }
            else
            {
                sSSID = txtSSID.Text.Replace(" ", "$"); ;
                command = "6Eset w s " + txtSSID.Text.Replace(" ", "$"); 
            }

            int tries = 0;
            bool result = false;

            //btnSetSSID.Enabled = false;

            while (!result)
            {
                result = DeviceSendCommand(command, "AOK");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot set SSID";
                    txtResponse.BackColor = Color.Pink;
                    //    pbxSetSSID.Image = ilsLED.Images[ 2 ];

                    //    btnSetSSID.Enabled = true;
                    return;
                }
            }
            //DeviceSendCommand( command );
            //  pbxSetSSID.Image = ilsLED.Images[ 3 ];
            //  btnSetSSID.Enabled = true;
        }

        /*--- end of btnSetSSID  -------------------------------------------*/

        #endregion

        #region  SetChannel

        /****************************************************************************
        btnSetChannel_Click                                    10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void SetChannel()
        {
            string command = "6Eset w c " + txtChannel.Text;
            int tries = 0;
            bool result = false;

            //btnSetChannel.Enabled = false;

            while (!result)
            {
                result = DeviceSendCommand(command, "AOK");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot set Channel Number";
                    txtResponse.BackColor = Color.Pink;
                    //    pbxSetChannel.Image = ilsLED.Images[ 2 ];

                    //   btnSetChannel.Enabled = true;
                    return;
                }
            }
        }

        /*--- end of  SetChannel ----------------------------------------*/

        #endregion

        #region  SetAuth

        /****************************************************************************
        btnSetAuth_Click                                       10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        SSID : spbat254
        password : e34b5ca8df172690293ba5c186d7e40fb1f948325ac7ed606bdfc59174e3082a
        agbpV+/,"{!EWLr'?+HGcF0u~,o<iL>\L'o,=.A\5Ab&u=.r5RO#()_;>qL&Y}B
        *   1...|....1....|....2....|....3....|....4....|....5....|....6....|
        ****************************************************************************/

        private void SetAuth()
        {
            string command = "";
            int tries = 0;
            bool result = false;
            iAuthProtocol = 0;

            if (rdoOpen.Checked)
                command = "6Eset w a 0";   // default 

            if (rdoWEP128.Checked)
            {
                if (rdo13ASCII.Checked)
                {
                    if (txtWEPKey.Text.Trim().Length != 13)
                    {
                        MessageBox.Show("The WEP Key must be exactly 13 characters long!", "Length Error");
                        txtWEPKey.Focus();

                        return;
                    }
                }
                if (rdo26HEX.Checked)
                {
                    if (txtWEPKey.Text.Trim().Length != 26)
                    {
                        MessageBox.Show("The WEP Key must be exactly 26 characters long!", "Length Error");
                        //txtWEPKey.Focus();

                        return;
                    }
                }

                command = "6Eset w a 1";   // WEP-128 
                iAuthProtocol = 2;
            }
            if (rdoWPA1.Checked)
            {
                command = "6Eset w a 2";   // 
                iAuthProtocol = 3;
            }
            if (rdoMixedWPA.Checked)
            {
                command = "6Eset w a 3";   // 
                iAuthProtocol = 4;
            }
            if (rdoWPA2.Checked)
            {
                command = "6Eset w a 4";   // 
                iAuthProtocol = 5;
            }
            while (!result)
            {
                result = DeviceSendCommand(command, "AOK");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot set Authentication";
                    txtResponse.BackColor = Color.Pink;
                    return;
                }
            }

            if (rdoWEP128.Checked)
            {
                if (WEP_KEY.Length != 26)
                    txtWEPKey_Leave();

                command = "6Eset w k " + WEP_KEY;

                tries = 0;
                result = false;
                while (!result)
                {
                    result = DeviceSendCommand(command, "AOK");
                    if (result)
                        break;
                    tries++;
                    if (tries > 4)
                    {
                        txtResponse.Text = "Cannot set WEP Key";
                        txtResponse.BackColor = Color.Pink;

                        return;
                    }
                }
            }

            if (rdoWPA1.Checked || rdoWPA2.Checked || rdoMixedWPA.Checked)
            {
                //"set w p e34b5ca8df172690293ba5c186d7e40fb1f948325ac7ed606bdfc59174e3082a"
                txtPassPhrase_Leave();

                if (rdoHex.Checked)
                {
                    // convert back to ASCII 
                    bool isHex = true;
                    // is the PASS key currently in ASCII or Hex? 
                    foreach (char c in txtPassPhrase.Text.Trim())
                    {
                        if (c < 0x30)
                        {
                            isHex = false;
                            break;
                        }
                        if (c > 0x39 && c < 0x41)
                        {
                            isHex = false;
                            break;
                        }
                        if (c > 0x46 && c < 0x61)
                        {
                            isHex = false;
                            break;
                        }
                        if (c > 0x66)
                        {
                            isHex = false;
                            break;
                        }
                    }

                    if (!isHex)
                    {
                        // need to convert it to hex 
                        PASS_KEY = "";
                        foreach (char c in txtPassPhrase.Text.Trim())
                            PASS_KEY += String.Format("{0:X}", (int)c);
                        txtPassPhrase.Text = PASS_KEY;
                    }
                }

                command = "6Eset w p " + txtPassPhrase.Text;

                if (command.Length <= 64)
                {
                    tries = 0;
                    result = false;
                    while (!result)
                    {
                        result = DeviceSendCommand(command, "AOK");
                        if (result)
                            break;
                        tries++;
                        if (tries > 4)
                        {
                            txtResponse.Text = "Cannot set Pass Phrase";
                            txtResponse.BackColor = Color.Pink;

                            return;
                        }
                    }
                }
                else // must split in two 
                {
                    int ilen = txtPassPhrase.Text.Length;

                    string pc1 = txtPassPhrase.Text.Substring(0, 32);   // 1st half
                    string pc2 = txtPassPhrase.Text.Substring(32) + Environment.NewLine;      // 2nd half 

                    command = "6{" + pc1;

                    tries = 0;
                    result = false;
                    while (!result)
                    {
                        result = DeviceSendCommand(command, "AOK");
                        if (result)
                            break;
                        tries++;
                        if (tries > 4)
                        {
                            txtResponse.Text = "Cannot set Pass Phrase";
                            txtResponse.BackColor = Color.Pink;

                            return;
                        }
                    }

                    command = "6}" + pc2;

                    tries = 0;
                    result = false;
                    while (!result)
                    {
                        result = DeviceSendCommand(command, "AOK");
                        if (result)
                            break;
                        tries++;
                        if (tries > 4)
                        {
                            txtResponse.Text = "Cannot set Pass Phrase";
                            txtResponse.BackColor = Color.Pink;

                            return;
                        }
                    }
                }
            }
        }

        /*--- end of  SetAuth  -------------------------------------------*/

        #endregion

        #region  SetDHCP

        /****************************************************************************
        btnSetDHCP_Click                                       10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void SetDHCP()
        {
            string command = "";
            int tries = 0;
            bool result = false;

            //btnSetDHCP.Enabled = false;

            command = "6Eset i d 1";   // default 

            if (rdoDHCPOff.Checked)
                command = "6Eset i d 0";
            if (rdoDHCPOn.Checked)
                command = "6Eset i d 1";   // default 

            while (!result)
            {
                result = DeviceSendCommand(command, "AOK");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot set DHCP";
                    txtResponse.BackColor = Color.Pink;
                    //    pbxSetDHCP.Image = ilsLED.Images[ 2 ];

                    return;
                }
            }

            if (rdoDHCPOff.Checked)
            {
                command = "6Eset i a " + txtIPAddress.Text;
                tries = 0;
                result = false;
                while (!result)
                {
                    result = DeviceSendCommand(command, "AOK");
                    if (result)
                        break;
                    tries++;
                    if (tries > 4)
                    {
                        txtResponse.Text = "Cannot set IP Address";
                        txtResponse.BackColor = Color.Pink;

                        return;
                    }
                }

                command = "6Eset i g " + txtGateway.Text;
                tries = 0;
                result = false;
                while (!result)
                {
                    result = DeviceSendCommand(command, "AOK");
                    if (result)
                        break;
                    tries++;
                    if (tries > 4)
                    {
                        txtResponse.Text = "Cannot set Gateway";
                        txtResponse.BackColor = Color.Pink;

                        return;
                    }
                }

                command = "6Eset i n " + txtSubnetMask.Text;
                tries = 0;
                result = false;
                while (!result)
                {
                    result = DeviceSendCommand(command, "AOK");
                    if (result)
                        break;
                    tries++;
                    if (tries > 4)
                    {
                        txtResponse.Text = "Cannot set Subnet Mask";
                        txtResponse.BackColor = Color.Pink;

                        return;
                    }
                }

                command = "6Eset d a " + txtDNSServer.Text;
                tries = 0;
                result = false;
                while (!result)
                {
                    result = DeviceSendCommand(command, "AOK");
                    if (result)
                        break;
                    tries++;
                    if (tries > 4)
                    {
                        txtResponse.Text = "Cannot set DNS Server";
                        txtResponse.BackColor = Color.Pink;

                        return;
                    }
                }
            }
        }

        /*--- end of btnSetDHCP_Click() -------------------------------------------*/

        #endregion

        #region  Save

        /****************************************************************************
        btnSave_Click                                          10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void Save()
        {
            int tries = 0;
            bool result = false;

            //btnSave.Enabled = false;

            while (!result)
            {
                result = DeviceSendCommand("6Esave", "Storing");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot save config";
                    txtResponse.BackColor = Color.Pink;
                    pbxSaveConfiguration.Image = ilsLED.Images[2];

                    return;
                }
            }
            pbxSaveConfiguration.Image = ilsLED.Images[3];
        }

        /*--- end of  Save ----------------------------------------------*/

        #endregion

        #region txtWEPKey_Leave

        /****************************************************************************
        txtWEPKey_Leave                                        10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void txtWEPKey_Leave()
        {
            string stmp;
            bool FndSpace = false;
            bool FndSub = false;
            bool Conflict = false;
            string sSub = String.Format("{0:X}", Convert.ToInt32(SubstituteChar[0]));

            // check for presence of spaces and substitution character 

            if (rdo13ASCII.Checked)
            {
                if (txtWEPKey.Text.Contains(" ") && txtWEPKey.Text.Contains(SubstituteChar))
                {
                    Conflict = true;
                }
            }
            else
            {
                for (int i = 0; i < txtWEPKey.Text.Length; i += 2)
                {
                    stmp = txtWEPKey.Text.Substring(i, 2);
                    if (stmp == "20")
                        FndSpace = true;
                    if (stmp == sSub)
                        FndSub = true;
                }
                if (FndSpace && FndSub)
                    Conflict = true;
            }

            if (Conflict)
            {
                MessageBox.Show("This WEP Key contains both spaces and\n" +
                                "your current Substitution Character\n\n" +
                                "Please select a new Substitution Character\n\n" +
                                "The New Substitution Character selector will\n" +
                                "open when you close this window.", "WEP Key Error");

                return;
            }

            if (txtWEPKey.Text.Trim().Length > 0)
            {
                if (rdo13ASCII.Checked)
                {
                    if (txtWEPKey.Text.Trim().Length != 13)
                    {
                        MessageBox.Show("The WEP Key must be exactly 13 characters long!", "Length Error");
                        txtWEPKey.Focus();
                        return;
                    }

                    WEP_KEY = "";              // Start fresh 
                    // Convert ASCII WEP Key to Hex digits: 
                    // WirelessLAN4u = 576972656C6573734C414E3475 
                    foreach (char c in txtWEPKey.Text.Trim())
                        WEP_KEY += String.Format("{0:X}", (int)c);
                }
                if (rdo26HEX.Checked)
                {
                    if (txtWEPKey.Text.Trim().Length != 26 )
                    {
                        MessageBox.Show("The WEP Key must be exactly 26 HEX characters long!", "Length Error");
                        //txtWEPKey.Focus();
                        return;
                    }

                    WEP_KEY = txtWEPKey.Text.Trim();
                }
            }
            return;
        }

        /*--- end of txtWEPKey_Leave() --------------------------------------------*/

        #endregion

        #region txtPassPhrase_Leave

        /****************************************************************************
        txtPassPhrase_Leave                                    10/30/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments:                                                                                                             
        ****************************************************************************/

        private void txtPassPhrase_Leave()
        {
            string stmp;
            bool FndSpace = false;
            bool FndSub = false;
            bool Conflict = false;
            string sSub = String.Format("{0:X}", Convert.ToInt32(SubstituteChar[0]));

            // check for presence of spaces and substitution character 

            if (rdoASCII.Checked)
            {
                if (txtPassPhrase.Text.Contains(" ") && txtPassPhrase.Text.Contains(SubstituteChar))
                {
                    Conflict = true;
                }
            }
            else
            {
                for (int i = 0; i < txtPassPhrase.Text.Length; i += 2)
                {
                    stmp = txtPassPhrase.Text.Substring(i, 2);
                    if (stmp == "20")
                        FndSpace = true;
                    if (stmp == sSub)
                        FndSub = true;
                }
                if (FndSpace && FndSub)
                    Conflict = true;
            }

            if (Conflict)
            {
                MessageBox.Show("This passphrase contains both spaces and\n" +
                                "your current Substitution Character\n\n" +
                                "Please select a new Substitution Character\n\n" +
                                "The New Substitution Character selector will\n" +
                                "open when you close this window.", "PassPhrase Error");

                return;
            }

            // replace spaces with dollar signs 
            if (rdoASCII.Checked)
            {
                char nch = SubstituteChar[0];
                txtPassPhrase.Text = txtPassPhrase.Text.Replace(' ', nch);
            }
            else
            {
                try
                {
                    PASS_KEY = "";
                    for (int i = 0; i < txtPassPhrase.Text.Length; i += 2)
                    {
                        stmp = txtPassPhrase.Text.Substring(i, 2);
                        if (stmp == "20")
                            stmp = String.Format("{0:X}", Convert.ToInt32(SubstituteChar[0]));

                        PASS_KEY = PASS_KEY + stmp;
                    }
                    txtPassPhrase.Text = PASS_KEY;
                }
                catch
                {
                }
            }
             if (txtPassPhrase.Text.Trim().Length > 0)
            {
                if (rdo13ASCII.Checked)
                {
                    if (txtPassPhrase.Text.Trim().Length != 13)
                    {
                        MessageBox.Show("The WPA Key must be exactly 13 characters long!", "Length Error");
                        txtPassPhrase.Focus();
                        return;
                    }

                    txtPassPhrase.Text = "";              // Start fresh 
                    // Convert ASCII WEP Key to Hex digits: 
                    // WirelessLAN4u = 576972656C6573734C414E3475 
                    foreach (char c in txtPassPhrase.Text.Trim())
                        PASS_KEY += String.Format("{0:X}", (int)c);
                }
                if (rdo26HEX.Checked)
                {
                    if (txtPassPhrase.Text.Trim().Length < 26 )
                    {
                        MessageBox.Show("The WPA Key must be at least 26 HEX characters long!", "Length Error");
                        txtPassPhrase.Focus();
                        //txtWEPKey.Focus();
                        return;
                    }

                    PASS_KEY = txtPassPhrase.Text.Trim();
                }
            }
        }

        /*--- end of txtPassPhrase_Leave() ----------------------------------------*/

        #endregion

        #region btnCommandSend_Click

        /****************************************************************************
        btnCommandSend_Click                                   08/02/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void btnCommandSend_Click(object sender, EventArgs e)
        {
            string command = "";// txtCommandSend.Text.Trim();

            DeviceSendCommand(command);
        }

        /*--- end of btnCommandSend_Click() ---------------------------------------*/

        #endregion

        #region AEnCode

        /****************************************************************************
        AENCODE                                                09/16/07
        Description: Encrypt a string
        Parameters: string to encode
        Return Value: encoded string
        Processing: 
        Called By: 
        Comments: encoded string will 2 times + 1 the original
        ****************************************************************************/

        private string AEnCode(string cpw)
        {
            if (cpw.Trim().Length == 0)
                return "";

            string enstring = "";

            for (int x = 0; x < cpw.Length; x++)
            {
                string ens = cpw.Substring(x, 1);
                string nl = "";

                switch (ens)
                {
                    case "a":
                        nl = "01";
                        break;
                    case "b":
                        nl = "02";
                        break;
                    case "c":
                        nl = "03";
                        break;
                    case "d":
                        nl = "04";
                        break;
                    case "e":
                        nl = "05";
                        break;
                    case "f":
                        nl = "06";
                        break;
                    case "g":
                        nl = "07";
                        break;
                    case "h":
                        nl = "08";
                        break;
                    case "i":
                        nl = "09";
                        break;
                    case "j":
                        nl = "10";
                        break;
                    case "k":
                        nl = "11";
                        break;
                    case "l":
                        nl = "12";
                        break;
                    case "m":
                        nl = "13";
                        break;
                    case "n":
                        nl = "14";
                        break;
                    case "o":
                        nl = "15";
                        break;
                    case "p":
                        nl = "16";
                        break;
                    case "q":
                        nl = "17";
                        break;
                    case "r":
                        nl = "18";
                        break;
                    case "s":
                        nl = "19";
                        break;
                    case "t":
                        nl = "20";
                        break;
                    case "u":
                        nl = "21";
                        break;
                    case "v":
                        nl = "22";
                        break;
                    case "w":
                        nl = "23";
                        break;
                    case "x":
                        nl = "24";
                        break;
                    case "y":
                        nl = "25";
                        break;
                    case "z":
                        nl = "26";
                        break;
                    case "1":
                        nl = "27";
                        break;
                    case "2":
                        nl = "28";
                        break;
                    case "3":
                        nl = "29";
                        break;
                    case "4":
                        nl = "30";
                        break;
                    case "5":
                        nl = "31";
                        break;
                    case "6":
                        nl = "32";
                        break;
                    case "7":
                        nl = "33";
                        break;
                    case "8":
                        nl = "34";
                        break;
                    case "9":
                        nl = "35";
                        break;
                    case "0":
                        nl = "36";
                        break;
                    case "A":
                        nl = "37";
                        break;
                    case "B":
                        nl = "38";
                        break;
                    case "C":
                        nl = "39";
                        break;
                    case "D":
                        nl = "40";
                        break;
                    case "E":
                        nl = "41";
                        break;
                    case "F":
                        nl = "42";
                        break;
                    case "G":
                        nl = "43";
                        break;
                    case "H":
                        nl = "44";
                        break;
                    case "I":
                        nl = "45";
                        break;
                    case "J":
                        nl = "46";
                        break;
                    case "K":
                        nl = "47";
                        break;
                    case "L":
                        nl = "48";
                        break;
                    case "M":
                        nl = "49";
                        break;
                    case "N":
                        nl = "50";
                        break;
                    case "O":
                        nl = "51";
                        break;
                    case "P":
                        nl = "52";
                        break;
                    case "Q":
                        nl = "53";
                        break;
                    case "R":
                        nl = "54";
                        break;
                    case "S":
                        nl = "55";
                        break;
                    case "T":
                        nl = "56";
                        break;
                    case "U":
                        nl = "57";
                        break;
                    case "V":
                        nl = "58";
                        break;
                    case "W":
                        nl = "59";
                        break;
                    case "X":
                        nl = "60";
                        break;
                    case "Y":
                        nl = "61";
                        break;
                    case "Z":
                        nl = "62";
                        break;
                    case "`":
                        nl = "63";
                        break;
                    case "~":
                        nl = "64";
                        break;
                    case "!":
                        nl = "65";
                        break;
                    case "@":
                        nl = "66";
                        break;
                    case "#":
                        nl = "67";
                        break;
                    case "$":
                        nl = "68";
                        break;
                    case "%":
                        nl = "69";
                        break;
                    case "^":
                        nl = "70";
                        break;
                    case "&":
                        nl = "71";
                        break;
                    case "*":
                        nl = "72";
                        break;
                    case "(":
                        nl = "73";
                        break;
                    case ")":
                        nl = "74";
                        break;
                    case "-":
                        nl = "75";
                        break;
                    case "_":
                        nl = "76";
                        break;
                    case "=":
                        nl = "77";
                        break;
                    case "+":
                        nl = "78";
                        break;
                    case "[":
                        nl = "79";
                        break;
                    case "]":
                        nl = "80";
                        break;
                    case "{":
                        nl = "81";
                        break;
                    case "}":
                        nl = "82";
                        break;
                    case "\\":
                        nl = "83";
                        break;
                    case "|":
                        nl = "84";
                        break;
                    case ";":
                        nl = "85";
                        break;
                    case ":":
                        nl = "86";
                        break;
                    case "\"":
                        nl = "87";
                        break;
                    case "'":
                        nl = "88";
                        break;
                    case ",":
                        nl = "89";
                        break;
                    case "<":
                        nl = "90";
                        break;
                    case ".":
                        nl = "91";
                        break;
                    case ">":
                        nl = "92";
                        break;
                    case "/":
                        nl = "93";
                        break;
                    case "?":
                        nl = "94";
                        break;
                    default:
                        return ("invalid character");
                    //break;
                }

                enstring = enstring + nl;
            }

            int noc = cpw.Length + 50;
            string cksm = noc.ToString();
            enstring = cksm + enstring;

            return (enstring);
        }

        /*--- end of AEnCode() ----------------------------------------------------*/

        #endregion

        #region ADeCode

        /****************************************************************************
        ADECODE                                                09/16/07
        Description: Decrypt a string encrypted with AEnCode()
        Parameters: encoded string
        Return Value: normal string
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private string ADeCode(string cpw)
        {
            if (cpw.Trim().Length == 0)
                return "";

            string unstring = "";
            int noc = cpw.Trim().Length;
            string uns = cpw.Substring(0, 2);
            int chksm = Convert.ToInt32(uns);
            chksm -= 50;

            if (chksm != ((noc / 2) - 1))
            {
                return ("");
            }

            for (int x = 2; x < noc; x += 2)
            {
                string nc = "";
                uns = cpw.Substring(x, 2);

                switch (uns)
                {
                    case "01":
                        nc = "a";
                        break;
                    case "02":
                        nc = "b";
                        break;
                    case "03":
                        nc = "c";
                        break;
                    case "04":
                        nc = "d";
                        break;
                    case "05":
                        nc = "e";
                        break;
                    case "06":
                        nc = "f";
                        break;
                    case "07":
                        nc = "g";
                        break;
                    case "08":
                        nc = "h";
                        break;
                    case "09":
                        nc = "i";
                        break;
                    case "10":
                        nc = "j";
                        break;
                    case "11":
                        nc = "k";
                        break;
                    case "12":
                        nc = "l";
                        break;
                    case "13":
                        nc = "m";
                        break;
                    case "14":
                        nc = "n";
                        break;
                    case "15":
                        nc = "o";
                        break;
                    case "16":
                        nc = "p";
                        break;
                    case "17":
                        nc = "q";
                        break;
                    case "18":
                        nc = "r";
                        break;
                    case "19":
                        nc = "s";
                        break;
                    case "20":
                        nc = "t";
                        break;
                    case "21":
                        nc = "u";
                        break;
                    case "22":
                        nc = "v";
                        break;
                    case "23":
                        nc = "w";
                        break;
                    case "24":
                        nc = "x";
                        break;
                    case "25":
                        nc = "y";
                        break;
                    case "26":
                        nc = "z";
                        break;
                    case "27":
                        nc = "1";
                        break;
                    case "28":
                        nc = "2";
                        break;
                    case "29":
                        nc = "3";
                        break;
                    case "30":
                        nc = "4";
                        break;
                    case "31":
                        nc = "5";
                        break;
                    case "32":
                        nc = "6";
                        break;
                    case "33":
                        nc = "7";
                        break;
                    case "34":
                        nc = "8";
                        break;
                    case "35":
                        nc = "9";
                        break;
                    case "36":
                        nc = "0";
                        break;
                    case "37":
                        nc = "A";
                        break;
                    case "38":
                        nc = "B";
                        break;
                    case "39":
                        nc = "C";
                        break;
                    case "40":
                        nc = "D";
                        break;
                    case "41":
                        nc = "E";
                        break;
                    case "42":
                        nc = "F";
                        break;
                    case "43":
                        nc = "G";
                        break;
                    case "44":
                        nc = "H";
                        break;
                    case "45":
                        nc = "I";
                        break;
                    case "46":
                        nc = "J";
                        break;
                    case "47":
                        nc = "K";
                        break;
                    case "48":
                        nc = "L";
                        break;
                    case "49":
                        nc = "M";
                        break;
                    case "50":
                        nc = "N";
                        break;
                    case "51":
                        nc = "O";
                        break;
                    case "52":
                        nc = "P";
                        break;
                    case "53":
                        nc = "Q";
                        break;
                    case "54":
                        nc = "R";
                        break;
                    case "55":
                        nc = "S";
                        break;
                    case "56":
                        nc = "T";
                        break;
                    case "57":
                        nc = "U";
                        break;
                    case "58":
                        nc = "V";
                        break;
                    case "59":
                        nc = "W";
                        break;
                    case "60":
                        nc = "X";
                        break;
                    case "61":
                        nc = "Y";
                        break;
                    case "62":
                        nc = "Z";
                        break;
                    case "63":
                        nc = "`";
                        break;
                    case "64":
                        nc = "~";
                        break;
                    case "65":
                        nc = "!";
                        break;
                    case "66":
                        nc = "@";
                        break;
                    case "67":
                        nc = "#";
                        break;
                    case "68":
                        nc = "$";
                        break;
                    case "69":
                        nc = "%";
                        break;
                    case "70":
                        nc = "^";
                        break;
                    case "71":
                        nc = "&";
                        break;
                    case "72":
                        nc = "*";
                        break;
                    case "73":
                        nc = "(";
                        break;
                    case "74":
                        nc = ")";
                        break;
                    case "75":
                        nc = "-";
                        break;
                    case "76":
                        nc = "_";
                        break;
                    case "77":
                        nc = "=";
                        break;
                    case "78":
                        nc = "+";
                        break;
                    case "79":
                        nc = "[";
                        break;
                    case "80":
                        nc = "]";
                        break;
                    case "81":
                        nc = "{";
                        break;
                    case "82":
                        nc = "}";
                        break;
                    case "83":
                        nc = "\\";
                        break;
                    case "84":
                        nc = "|";
                        break;
                    case "85":
                        nc = ";";
                        break;
                    case "86":
                        nc = ":";
                        break;
                    case "87":
                        nc = "\"";
                        break;
                    case "88":
                        nc = "'";
                        break;
                    case "89":
                        nc = ",";
                        break;
                    case "90":
                        nc = "<";
                        break;
                    case "91":
                        nc = ".";
                        break;
                    case "92":
                        nc = ">";
                        break;
                    case "93":
                        nc = "/";
                        break;
                    case "94":
                        nc = "?";
                        break;
                }
                unstring = unstring + nc;
            }

            return (unstring);
        }

        /*--- end of ADeCode() ----------------------------------------------------*/

        #endregion

        #region btnCommands_Click

        /****************************************************************************
        btnCommands_Click                                      11/23/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void btnCommands_Click(object sender, EventArgs e)
        {
            HospWifi.WiFiCommands wif = new HospWifi.WiFiCommands();
            wif.ShowDialog();
            //  txtCommandSend.Text = wif.CText;
        }

        /*--- end of btnCommands_Click() ------------------------------------------*/

        #endregion

        #region btnSendDataPacket_Click

        /****************************************************************************
        btnSendDataPacket_Click                                11/23/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void btnSendDataPacket_Click(object sender, EventArgs e)
        {
            DeviceSendCommand("0S");
        }

        /*--- end of btnSendDataPacket_Click() ------------------------------------*/

        #endregion

        #region helpToolStripMenuItem_Click

        /****************************************************************************
        helpToolStripMenuItem_Click                            11/24/10
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Help h = new Help();
            h.Show();
        }

        /*--- end of helpToolStripMenuItem_Click() --------------------------------*/

        #endregion

        #region webBrowserToolStripMenuItem_Click

        /****************************************************************************
        webBrowserToolStripMenuItem_Click                      05/19/11
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void webBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // launch IE 
            System.Diagnostics.Process process1 = new System.Diagnostics.Process();
            //process1.StartInfo.Arguments = "http://cast-dev.stingermedical.com";
            process1.StartInfo.Arguments = PulseURL;
            process1.StartInfo.FileName = "IExplore.exe";
            process1.Start();
        }

        /*--- end of webBrowserToolStripMenuItem_Click() --------------------------*/

        #endregion

        #region txtSSID_Leave

        /****************************************************************************
        txtSSID_Leave                                          05/19/11
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void txtSSID_Leave(object sender, EventArgs e)
        {
            char nch = SubstituteChar[0];
            txtSSID.Text = txtSSID.Text.Replace(' ', nch);
        }

        /*--- end of txtSSID_Leave() ----------------------------------------------*/

        #endregion

        #region HexToDecimal

        /****************************************************************************
        HexToDecimal                                           04/07/10
        Description: String hex value to integer value
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 8 hex characters max: 4,294,967,295 (0xFFFFFFFF) max.
        ****************************************************************************/

        /// <summary>
        /// Convert a hex string to an integer value.
        /// </summary>
        /// <param name="sHex">String to convert</param>
        /// <returns>unsigned integer value</returns>

        private uint HexToDecimal(string sHex)
        {
            uint value = 0;
            uint Multiplier = 1;
            int len = sHex.Length;  // 8 max 

            if (len > 8)
            {
                // throw new string length exception 
                return (value);
            }

            for (int i = len - 1; i >= 0; i--)
            {
                switch (sHex[i])
                {
                    case '0':
                        value += 0 * Multiplier;
                        break;
                    case '1':
                        value += 1 * Multiplier;
                        break;
                    case '2':
                        value += 2 * Multiplier;
                        break;
                    case '3':
                        value += 3 * Multiplier;
                        break;
                    case '4':
                        value += 4 * Multiplier;
                        break;
                    case '5':
                        value += 5 * Multiplier;
                        break;
                    case '6':
                        value += 6 * Multiplier;
                        break;
                    case '7':
                        value += 7 * Multiplier;
                        break;
                    case '8':
                        value += 8 * Multiplier;
                        break;
                    case '9':
                        value += 9 * Multiplier;
                        break;
                    case 'a':
                        value += 10 * Multiplier;
                        break;
                    case 'A':
                        value += 10 * Multiplier;
                        break;
                    case 'b':
                        value += 11 * Multiplier;
                        break;
                    case 'B':
                        value += 11 * Multiplier;
                        break;
                    case 'c':
                        value += 12 * Multiplier;
                        break;
                    case 'C':
                        value += 12 * Multiplier;
                        break;
                    case 'd':
                        value += 13 * Multiplier;
                        break;
                    case 'D':
                        value += 13 * Multiplier;
                        break;
                    case 'e':
                        value += 14 * Multiplier;
                        break;
                    case 'E':
                        value += 14 * Multiplier;
                        break;
                    case 'f':
                        value += 15 * Multiplier;
                        break;
                    case 'F':
                        value += 15 * Multiplier;
                        break;
                }
                Multiplier = (Multiplier * 16);
            }

            return (value);
        }

        /*--- end of HexToDecimal() -----------------------------------------------*/

        #endregion

        #region rdo13ASCII_CheckedChanged

        /****************************************************************************
        rdo13ASCII_CheckedChanged                              05/19/11
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void rdo13ASCII_CheckedChanged(object sender, EventArgs e)
        {
            bool isHex = true;
            string stmp;

            if (rdoASCII.Checked)
            {
                //txtWEPKey.MaxLength = 13;
                txtWEPKey.MaxLength = 13;
                rdoASCII.Checked = true;

                isHex = true;
                // is the WEP key currently in ASCII or Hex? 
                foreach (char c in txtWEPKey.Text.Trim())
                {
                    if (c < 0x30)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x39 && c < 0x41)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x46 && c < 0x61)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x66)
                    {
                        isHex = false;
                        break;
                    }
                }

                if (isHex)
                {
                    // need to convert it to ASCII 
                    try
                    {
                        WEP_KEY = "";
                        for (int i = 0; i < txtWEPKey.Text.Length; i += 2)
                        {
                            stmp = txtWEPKey.Text.Substring(i, 2);

                            WEP_KEY = WEP_KEY + Convert.ToChar(HexToDecimal(stmp));
                        }
                        txtWEPKey.Text = WEP_KEY;
                    }
                    catch
                    {
                        txtWEPKey.Text = "Error";
                    }
                }
            }
            if (rdo26HEX.Checked)
            {
                txtWEPKey.MaxLength = 26;

                isHex = true;
                // is the WEP key currently in ASCII or Hex? 
                foreach (char c in txtWEPKey.Text.Trim())
                {
                    if (c < 0x30)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x39 && c < 0x41)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x46 && c < 0x61)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x66)
                    {
                        isHex = false;
                        break;
                    }
                }

                if (!isHex)
                {
                    // need to convert it to hex 
                    WEP_KEY = "";
                    foreach (char c in txtWEPKey.Text.Trim())
                        WEP_KEY += String.Format("{0:X}", (int)c);
                    txtWEPKey.Text = WEP_KEY;
                }
            }
        }

        /*--- end of rdo13ASCII_CheckedChanged() ----------------------------------*/

        #endregion

        #region newSubstituteCharacterToolStripMenuItem_Click

        /****************************************************************************
        newSubstituteCharacterToolStripMenuItem_Click          04/14/12
        Description: 
        Parameters: 
        Return Value: 
        Processing: 
        Called By: 
        Comments: 
        ****************************************************************************/

        private void newSubstituteCharacterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Substitute sub = new Substitute();
            sub.ShowDialog();

            if (sub.NewCharacter != "Cancel" && sub.NewCharacter.Length == 1)
            {
                // check for valid character 
                byte nch = (byte)sub.NewCharacter[0];

                if (nch < 0x21 || nch > 0x7E)
                {
                    MessageBox.Show("Invalid Replacement Character Chosen\nNo Change.");
                    return;
                }

                SubstituteChar = sub.NewCharacter;
                string snch = String.Format("0x{0:X2}", nch);

                string command = "6E$$$";
                DeviceSendCommand(command);

                WaitForData();

                command = "6Eset o r " + snch;    // opt replace  

                DeviceSendCommand(command);

                WaitForData();
            }
        }

        /*--- end of newSubstituteCharacterToolStripMenuItem_Click() --------------*/

        #endregion

        private void rdoASCII_CheckedChanged(object sender, EventArgs e)
        {
            bool isHex = true;
            string stmp;

            if (rdoASCII.Checked)
            {
                isHex = true;
                // is the WEP key currently in ASCII or Hex? 
                foreach (char c in txtPassPhrase.Text.Trim())
                {
                    if (c < 0x30)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x39 && c < 0x41)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x46 && c < 0x61)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x66)
                    {
                        isHex = false;
                        break;
                    }
                }

                if (isHex)
                {
                    // need to convert it to ASCII 
                    try
                    {
                        PASS_KEY = "";
                        for (int i = 0; i < txtPassPhrase.Text.Length; i += 2)
                        {
                            stmp = txtPassPhrase.Text.Substring(i, 2);

                            PASS_KEY = PASS_KEY + Convert.ToChar(HexToDecimal(stmp));
                        }
                        txtPassPhrase.Text = PASS_KEY;
                    }
                    catch
                    {
                        txtPassPhrase.Text = "Error";
                    }
                }
            }
            else // hex is checked 
            {
                isHex = true;
                // is the PASS key currently in ASCII or Hex? 
                foreach (char c in txtPassPhrase.Text.Trim())
                {
                    if (c < 0x30)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x39 && c < 0x41)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x46 && c < 0x61)
                    {
                        isHex = false;
                        break;
                    }
                    if (c > 0x66)
                    {
                        isHex = false;
                        break;
                    }
                }

                if (!isHex)
                {
                    // need to convert it to hex 
                    PASS_KEY = "";
                    foreach (char c in txtPassPhrase.Text.Trim())
                        PASS_KEY += String.Format("{0:X}", (int)c);
                    txtPassPhrase.Text = PASS_KEY;
                }
            }
        }

        // this is for the field techs 
        private void lblMobius3_DoubleClick(object sender, EventArgs e)
        {
        }

        private void WSSNGet()
        {
            string command = "0O";
            DeviceSendCommand(command);
        }

        private void WSSNSet()
        {
            string command = "0N" + txtWSSerialNumber.Text.Trim();
            DeviceSendCommand(command);
        }

        private void btnSendPacketNow_Click_1(object sender, EventArgs e)
        {
            try
            {

                Application.DoEvents();


                if (txtWSSerialNumber.Text.Length < 10)
                {
                    if (USBConnected == false)
                    {
                        MessageBox.Show("Please connect a usb cable to a Device that is powered on to check its communication to Pulse (button 1). You can also check a device without connecting by manually entering a device serial number in the Device Serial box");
                        return;
                    }
                    WSSNGet();
                }
                txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
                DeviceSendCommand("0S");
                gboxPulseStatus.Enabled = true;
                GetAssetInfo();

                btnSendPacketNow.Text = "Checking...";
                Application.DoEvents();
                tmrCommCheck.Interval = 2000;
                tmrCommCheck.Enabled = true;
            }
            catch (Exception ex)
            {
                tmrCommCheck.Enabled = false;
                btnSendPacketNow.Enabled = true;
                btnSendPacketNow.Visible = true;
                btnSendPacketNow.Text = "3: Check Pulse";
                if (txtSSID.Text.Contains("Manually") == false)
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
            }
        }


        private void btnUpdateRovingFirmware_Click(object sender, EventArgs e)
        {
            string command = "6E$$$";
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eset u f 0";                      // hardware flow control off 
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eset ftp address 198.175.253.161";  // Roving default 
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eset ftp dir public";             // ftp directory 
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eset ftp user roving";            // ftp user 
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eset ftp pass Pass123";           // ftp password 
            DeviceSendCommand(command);

            WaitForData();

            command = "6Eftp u";
            DeviceSendCommand(command);

            MessageBox.Show("Roving Firmware Update\n\nPlease allow 1 minute for the download,\nThen do a SAVE and REBOOT.", "HospWiFi Message");
        }

        private void btnWSSNSet_Click(object sender, EventArgs e)
        {
            WSSNSet();
        }

        private void btnWSSNGet_Click(object sender, EventArgs e)
        {
            WSSNGet();
        }
        
        private void btnLoginToPulse_Click_1(object sender, EventArgs e)
        {
            LoginToPulse();
        }

        private void LoginToPulse()
        {
            using (var form = new frmLoginToPulse())
            {
                form.StartPosition = FormStartPosition.CenterParent;
                form.Username = sUsername;
                form.Password = sPassword;
                if (bRememberMe == true)
                {
                    form.RememberMe = true;
                }
                var result = form.ShowDialog();
                sUsername = form.Username;
                sPassword = form.Password;
                if (result == DialogResult.OK)
                {
                    string val = form.siteID;
                    DataTable _arrSites = form.arrSites;
                    ddlSites.DataSource = _arrSites;
                    bRememberMe = form.RememberMe;
                    sUsername = form.Username;
                    sPassword = form.Password;
                    sSiteID = form.siteID;
                    sUserID = form.UserID;
                    bLoggedIn = true;
                    SaveParams();
                    WSSNGet();
                    DSN = txtWSSerialNumber.Text;
                    gbPulseAssetSetup.Enabled = true;
                    GetAssetInfo();
                    if (sSiteID == "4" || sSiteID == "2")
                    {
                        btnSaveNetwork.Visible = true;
                    }
                }
                else
                {
                    MessageBox.Show("Login Failed");
                }
            }
        }

        private void ddlSites_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dt2 = new DataTable();
                Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    {
                            "@SiteID",
                            ddlSites.SelectedValue.ToString()
                    }
                };
                Dictionary<string, string> parameters2 = new Dictionary<string, string>()
                {
                    {
                            "@SiteID",
                            ddlSites.SelectedValue.ToString()
                    }
                };
                dt = api.ExecSpApi("prcDepartmentsSelectBySite", parameters);
                if (dt.Rows.Count > 0)
                {
                    ddlDepartments.DataSource = dt;
                }

                dt2 = api.ExecSpApi("prcGetNetwork", parameters2);
                txtSSID.Text = "";
                txtPassPhrase.Text = "";
                txtPassPhrase.UseSystemPasswordChar = false;

                if (dt2.Rows.Count > 0)
                {
                    txtPassPhrase.UseSystemPasswordChar = true;
                    txtSSID.Text = dt2.Rows[0]["SSID"].ToString();
                    txtPassPhrase.Text = dt2.Rows[0]["Pass"].ToString();
                    if (Convert.ToInt32(dt2.Rows[0]["NetAuthProtocolID"].ToString()) > 3)
                    {
                        rdoMixedWPA.Checked = true;
                    }
                    else
                    {
                        rdoWEP128.Checked = true;
                    }
                    txtChannel.Text = dt2.Rows[0]["Channel"].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot get list of your facilities or stored network information: " + Environment.NewLine + ex.Message.ToString());
            }
        }

        private void btnSaveAsset_Click(object sender, EventArgs e)
        {
            if (txtFloor.Text.Length < 1 || txtWing.Text.Length < 1)
            {
                MessageBox.Show("Wing and Floor are required fields. Department is not required.");
                return;
            }
            try
            {
                DataTable dt = new DataTable();
                Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    {
                            "@SiteID",
                            ddlSites.SelectedValue.ToString()
                    },
                    {
                            "@UpdateOrSelect",
                            "1"
                    },
                    {
                            "@SerialNo",
                            DSN
                    },
                    {
                            "@AssetNumber",
                            txtAssetNumber.Text
                    },
                    {
                            "@Floor",
                            txtFloor.Text
                    },
                    {
                            "@ManualAllocation",
                            "1"
                    },
                    {
                            "@Wing",
                            txtWing.Text
                    },
                    {
                            "@DepartmentID",
                            ddlDepartments.SelectedValue.ToString()
                    },
                };
                dt = api.ExecSpApi("prcAssetsUpdateDeploy", parameters);
                MessageBox.Show("Asset Serial " + txtWSSerialNumber.Text + " successfully updated in Pulse.");
            }
            catch (Exception Ex)
            {
                MessageBox.Show("An error occured and the asset was not updated.");
            }
        }

        private void txtWSSerialNumber_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
                pbConnectedToPulse.Enabled = false;
                pbConnectedToPulse.Visible = false;
                gboxPulseStatus.Enabled = false;
                txtLastPostDate.Text = "";
                txtDeviceIP.Text = "";
                txtSiteIP.Text = "";
                txtAssignedSite.Text = "";
                if (txtWSSerialNumber.Text.Length > 11)
                {
                    GetAssetInfo();
                }
            }
            catch (Exception ex)
            {
                tmrCommCheck.Enabled = false;
                btnSendPacketNow.Enabled = true;
                btnSendPacketNow.Visible = true;

                if (txtSSID.Text.Contains("Manually") == false)
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
            }
        }

        private void GetAssetInfo()
        {
            btnSendPacketNow.Enabled = false;

            if (txtWSSerialNumber.Text.Length > 11)
            {
                DSN = txtWSSerialNumber.Text;

                DataTable dt = new DataTable();
                Dictionary<string, string> parameters = new Dictionary<string, string>()
                {
                    {
                            "@SerialNo",
                            DSN
                    }
                };
                dt = api.ExecSpApi("prcGetQSPacket", parameters);
                if (dt.Rows.Count > 0)
                {
                    pbConnectedToPulse.Enabled = true;
                    pbConnectedToPulse.Visible = true;
                    gboxPulseStatus.Enabled = true;
                    txtLastSuccessfulPacket.Text = DateTime.Parse(dt.Rows[0]["CreatedDateUTC"].ToString()).ToLocalTime().ToString() + " Local Time";
                    DateTime dt2;
                    if (DateTime.TryParse(dt.Rows[0]["CreatedDateUTC"].ToString(), out dt2) && dt2.Date > DateTime.Today.AddMinutes(-20))
                    {
                        pbxCommunicating.Enabled = true;
                        pbxCommunicating.Visible = true;
                        pbxCommunicating.Image = ilsLED.Images[3];
                        txtCommunicatingToPulse.Text = "Communicating To Pulse";
                    }
                    else
                    {
                        txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
                        pbConnectedToPulse.Enabled = false;
                        pbConnectedToPulse.Visible = false;
                        pbxCommunicating.Image = ilsLED.Images[2];
                    }
                }
                else
                {
                    txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
                    Application.DoEvents();
                    pbConnectedToPulse.Enabled = false;
                    pbConnectedToPulse.Visible = false;
                    gboxPulseStatus.Enabled = false;
                    txtLastSuccessfulPacket.Text = "";
                    pbxCommunicating.Image = ilsLED.Images[2];
                }
                parameters = new Dictionary<string, string>()
                {
                    {
                            "@UpdateOrSelect",
                            "0"
                    },
                    {
                            "@SerialNo",
                            DSN
                    },
                };
                dt = api.ExecSpApi("prcAssetsUpdateDeploy", parameters);
                if (dt.Rows.Count > 0)
                {
                    gboxPulseStatus.Enabled = true;
                    txtLastPostDate.Text = DateTime.Parse(dt.Rows[0]["LastPostDateUTC"].ToString()).ToLocalTime().ToString() + " Local Time";
                    txtDeviceIP.Text = dt.Rows[0]["IP"].ToString();
                    txtSiteIP.Text = dt.Rows[0]["SourceIPAddress"].ToString();
                    txtAssignedSite.Text = "Site: " + dt.Rows[0]["SiteName"].ToString();
                    if (bLoggedIn == true)
                    {
                        txtWing.Text = dt.Rows[0]["Wing"].ToString();
                        txtFloor.Text = dt.Rows[0]["Floor"].ToString();
                        txtAssetNumber.Text = dt.Rows[0]["AssetNumber"].ToString();
                        ddlSites.SelectedValue = dt.Rows[0]["SiteID"];
                        if (Convert.ToInt32(dt.Rows[0]["DepartmentID"]) != 0 && dt.Rows[0]["DepartmentID"] != null)
                        {
                            ddlDepartments.SelectedValue = dt.Rows[0]["DepartmentID"];
                        }
                        else
                        {
                            ddlDepartments.SelectedIndex = 0;
                            ddlDepartments.Text = "Unallocated";
                        }
                    }
                }
                else
                {
                    pbConnectedToPulse.Enabled = false;
                    pbConnectedToPulse.Visible = false;
                    gboxPulseStatus.Enabled = false;
                    txtLastPostDate.Text = "";
                    txtDeviceIP.Text = "";
                    txtSiteIP.Text = "";
                }
            }
            btnSendPacketNow.Enabled = true;
        }

        private void AttemptConnect()
        {
            try
            {
                bool OK = atusb.ConnectDevice(VID, PID);

                if (OK)
                {
                    txtCommand.Text = "USB Connected";

                    USBConnected = true;

                    if (!_continue)
                    {
                        readThread = new Thread(ReadUSB);
                        _continue = true;
                        readThread.Start();
                    }
                }
                else
                {
                    USBConnected = false;
                }

                System.Threading.Thread.Sleep(50);   /// time to see LED on 
            }
            catch
            {
                USBConnected = false;
            }
            return;
        }

        private void btnSaveAllParams()
        {
            try
            {
                btnSaveChanges.Enabled = false;
                txtLastSuccessfulPacket.Text = "";
                txtResponse.BackColor = Color.White;

                // set command mode 
                CommandMode();
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                Application.DoEvents();

                // set ssid 
                SetSSID();
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                Application.DoEvents();

                // set Channel 
                SetChannel();
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                Application.DoEvents();

                // set Auth 
                SetAuth();
                Application.DoEvents();
                System.Threading.Thread.Sleep(500);
                Application.DoEvents();

                // set DHCP 
                SetDHCP();

                System.Threading.Thread.Sleep(500);
                Application.DoEvents();
                // Save 
                Save();
                System.Threading.Thread.Sleep(500);
                Application.DoEvents();

                pbConnectedToPulse.Visible = false;
                pbxCommunicating.Visible = false;
                Application.DoEvents();
                btnReboot();
                btnSaveChanges.Enabled = true;
            }
            catch (Exception ex)
            {
                txtCommand.Text = ex.Message.ToString();
                //  btnReboot();
                System.Threading.Thread.Sleep(500);
                btnSaveChanges.Enabled = true;
                txtResponse.BackColor = Color.White;
            }
        }

        private void btnReboot()
        {
            int tries = 0;
            bool result = false;

            //   btnReboot.Enabled = false;

            while (!result)
            {
                result = DeviceSendCommand("6Ereboot", "Auto");
                if (result)
                    break;
                tries++;
                if (tries > 4)
                {
                    txtResponse.Text = "Cannot reboot device";
                    txtResponse.BackColor = Color.Pink;
                    pbxRebootWiFi.Image = ilsLED.Images[2];

                    //   btnReboot.Enabled = true;
                    return;
                }
            }
            pbxRebootWiFi.Image = ilsLED.Images[3];
            //   btnReboot.Enabled = true;
        }

        private void btnSaveChanges_Click(object sender, EventArgs e)
        {
            try
            {
                txtCommunicatingToPulse.Text = "Not Communicating To Pulse";
                pbxCommunicating.Image = ilsLED.Images[2];
                Application.DoEvents();
                btnSaveAllParams();
                // btnReboot();
                pbChangesSaved.Enabled = true;
                pbChangesSaved.Visible = true;
                Application.DoEvents();

                tmrCommCheck.Interval = 2000;
                tmrCommCheck.Enabled = true;
                btnSendPacketNow.Text = "Checking...";
                Application.DoEvents();

                DeviceSendCommand("0S");
                Application.DoEvents();
            }
            catch (Exception ex)
            {
            }
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            AttemptConnect();
            if (USBConnected == true)
            {
                cbUSBConnected.Checked = true;
                cbUSBConnected.Text = "Successfully Connected To Device";
                btnConnectUSB.Enabled = false;
                pbConnectedToUSB.Visible = true;
                txtCommand.Text = "6X";
                atusb.WriteBytes("6X");
                WaitForData();
                if (sReply.Contains("W"))
                {
                    rdoWorkstation.Checked = true;
                    cbUSBConnected.Text = "Successfully Connected To Workstation";
                }
                if (sReply.Contains("B"))
                {
                    rdoBaycharger.Checked = true;
                    cbUSBConnected.Text = "Successfully Connected To Charger";
                }
                WSSNGet();
            }
            else
            {
                cbUSBConnected.Checked = false;
                cbUSBConnected.Text = "Not Connected To Device";
                btnConnectUSB.Enabled = true;
                pbConnectedToUSB.Visible = false;
                btnSendPacketNow.Enabled = true;
            }
        }

        private void PACU_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (bformClosed != 1)
                {

                    bformClosed = 1;
                    _continue = false;
                    try
                    {
                        if (readThread != null)
                        {
                            readThread.Abort();
                        }
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                    };
                    try
                    {
                        //    SaveParams();
                        Thread.Sleep(100);
                        if (USBConnected)
                            atusb.CloseDevice();

                        this.Close();
                    }
                    catch (Exception ex)
                    {

                        Thread.Sleep(100);
                        this.Close();
                    };
                }
            }
            catch (Exception ex)
            {

            }
        }

        private void MYExnHandler(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                SaveParams();
                _continue = false;

                if (USBConnected)
                {
                    btnReboot();
                    atusb.CloseDevice();
                }

                readThread.Abort();
                Application.DoEvents();
                this.Close();
            }
            catch (Exception ex)
            {
                Application.DoEvents();
                this.Close();
            };
        }

        private void cmbSSIDs_SelectedValueChanged(object sender, EventArgs e)
        {
            txtPassPhrase.Enabled = true;
            txtWEPKey.Enabled = false;
            if (cmbSSIDs.Items.Count < 2)
            {
                return;
            }
            try
            {
                if (NetworksFiltered[cmbSSIDs.SelectedIndex].dot11DefaultCipherAlgorithm == Wlan.Dot11CipherAlgorithm.WEP)
                {
                    rdoWEP128.Select();
                    txtPassPhrase.Enabled = false;
                    txtWEPKey.Enabled = true;
                }
                else if (NetworksFiltered[cmbSSIDs.SelectedIndex].dot11DefaultCipherAlgorithm == Wlan.Dot11CipherAlgorithm.WPA_UseGroup)
                {
                    rdoMixedWPA.Select();
                    txtPassPhrase.Enabled = true;
                    txtWEPKey.Enabled = true;
                }
                else
                {
                    rdoWPA2.Select();
                    txtPassPhrase.Enabled = true;
                    txtWEPKey.Enabled = false;
                }
                txtSignalQuality.Text = NetworksFiltered[cmbSSIDs.SelectedIndex].wlanSignalQuality.ToString();
            }
            catch (Exception ex)
            {
                //  MessageBox.Show(ex.Message.ToString());
            }
        }

        private void rdoMixedWPA_CheckedChanged(object sender, EventArgs e)
        {
               if (rdoMixedWPA.Checked == true)
            {
                txtPassPhrase.Enabled = true;
                txtWEPKey.Enabled = false;
            }
            else
            {
                
              txtPassPhrase.Enabled = false;
            txtWEPKey.Enabled = true;
            }
        }

        private void rdoWPA2_CheckedChanged(object sender, EventArgs e)
        {
       
            if (rdoWPA2.Checked == true)
            {
                txtPassPhrase.Enabled = true;
                txtWEPKey.Enabled = false;
            }
            else
            {
                
              txtPassPhrase.Enabled = false;
            txtWEPKey.Enabled = true;
            }
        }

        private void rdoWPA1_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoWPA1.Checked == true)
            {
                txtPassPhrase.Enabled = true;
                txtWEPKey.Enabled = false;
            }
            else
            {
                
              txtPassPhrase.Enabled = false;
            txtWEPKey.Enabled = true;
            }
        }

        private void radButton1_Click_1(object sender, EventArgs e)
        {
            btnConnectUSB.Enabled = false;
            try
            {
                //btnReboot();
                SaveParams();
                tmrCommCheck.Enabled = false;
                _continue = false;

                if (USBConnected)
                    atusb.CloseDevice();
                try
                {
                    readThread.Abort();
                }
                catch (Exception ex)
                {
                };
                try
                {
                    Application.DoEvents();

                    tmrCommCheck.Enabled = false;
                    btnConnectUSB.Enabled = true;
                    btnSaveChanges.Enabled = true;
                    cbUSBConnected.Checked = false;
                    cbUSBConnected.Text = "Not Connected To Device";
                    btnConnectUSB.Enabled = true;
                    pbConnectedToUSB.Visible = false;
                    pbChangesSaved.Visible = false;
                    pbConnectedToPulse.Visible = false;
                    txtWSSerialNumber.Text = "";
                    pbConnectedToPulse.Enabled = false;
                    pbConnectedToPulse.Visible = false;
                    gboxPulseStatus.Enabled = false;
                    txtLastSuccessfulPacket.Text = "";
                    txtLastPostDate.Text = "";
                    txtDeviceIP.Text = "";
                    txtSiteIP.Text = "";
                    pbxSaveConfiguration.Image = ilsLED.Images[0];
                    pbxRebootWiFi.Image = ilsLED.Images[0];
                    pbxCommunicating.Image = ilsLED.Images[2];
                }
                catch
                {
                }
            }
            catch (Exception ex)
            {
                btnConnectUSB.Enabled = true;
            };
            btnConnectUSB.Enabled = true;
        }


        private void tmrCommCheck_Tick(object sender, EventArgs e)
        {
            try
            {
                sSuccessfulNetworkCredentials = "0";
                itmrtick += 1;

                Application.DoEvents();

                GetAssetInfo();
                Application.DoEvents();
                if (itmrtick > 6 || txtLastSuccessfulPacket.Text.Length > 2)
                {
                    tmrCommCheck.Enabled = false;
                    itmrtick = 0;
                    if (txtLastSuccessfulPacket.Text.Length < 2)
                    {
                        btnSendPacketNow.Text = "3: Check Pulse";
                        btnSaveChanges.Text = "2: Save Changes";
                        Application.DoEvents();
                           sSuccessfulNetworkCredentials = "0";
                        MessageBox.Show("Mobius Device cannot communicate to Pulse. Please check your network settings, and verify with your network administrator that this wireless network allows outbound http (80) traffic to 40.122.135.195 through your firewall. A quick test of communication with Pulse can be done by first connecting your laptop to the network you are connecting the Mobius device to and then hitting the Test Network button in this application. ", "Cannot Connect To Pulse");
                   
                    }
                    else
                    {
                         sSuccessfulNetworkCredentials = "1";
                        btnSendPacketNow.Text = "3: Check Pulse";
                        btnSaveChanges.Text = "2: Save Changes";
                        if (sUsername.ToUpper().Contains("ENOVATE") == true)
                        {
                            Dictionary<string, string> parameters = new Dictionary<string, string>()
                        {
                            {
                                    "@SSID",
                                    sSSID
                            },
                            {
                                    "@Password",
                                    txtPassPhrase.Text
                            },
                            {
                                    "@SerialNo",
                                    txtWSSerialNumber.Text
                            },
                            {
                                    "@SiteID",
                                    sSiteID
                            },
                            {
                                    "@Channel",
                                    txtChannel.Text
                            },
                              {
                                    "@Successful",
                                    sSuccessfulNetworkCredentials
                            },
                            {
                                    "@NetAuthProtocol",
                                    iAuthProtocol.ToString()
                            }
                        };
                            ApiLayer api = new ApiLayer();
                            DataTable dt = new DataTable();

                            dt = api.ExecSpApi("prcSaveNetwork", parameters);
                        }
                    }
                    btnSaveChanges.Text = "2: Save Changes";
                    Application.DoEvents();
                    itmrtick = 0;
                }

                Application.DoEvents();
            }
            catch (Exception ex)
            {
                tmrCommCheck.Enabled = false;
                btnSendPacketNow.Enabled = true;
                btnSendPacketNow.Visible = true;
                btnSaveChanges.Text = "2: Save Changes";
                if (txtSSID.Text.Contains("Manually") == false)
                {
                       sSuccessfulNetworkCredentials = "0";
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                       sSuccessfulNetworkCredentials = "0";
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
            }
        }


        private void radButton2_Click(object sender, EventArgs e)
        {
            string sNewDepartment;
            InputBox testDialog = new InputBox();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            if (testDialog.ShowDialog(this) == DialogResult.OK)
            {
                // Read the contents of testDialog's TextBox.
                sNewDepartment = testDialog.txtDepartment.Text;
            }
            else
            {
                sNewDepartment = "Cancelled";
            }
            testDialog.Dispose();
            try
            {
                if (sNewDepartment != "Cancelled")
                {
                    DataTable dt = new DataTable();
                    Dictionary<string, string> parameters = new Dictionary<string, string>()
                    {
                        {
                                "@SiteID",
                                ddlSites.SelectedValue.ToString()
                        },
                        {
                                "@Description",
                                sNewDepartment
                        }
                    };
                    dt = api.ExecSpApi("prcDepartmentsInsert", parameters);
                    MessageBox.Show("Department " + sNewDepartment + " successfully added to Pulse.");
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show("An error occured and the Department was not added.");
            }
        }

        private void txtPassPhrase_MouseClick(object sender, MouseEventArgs e)
        {
            if (txtPassPhrase.UseSystemPasswordChar == true)
            {
                txtPassPhrase.UseSystemPasswordChar = false;
                txtPassPhrase.Text = "";
            }
        }

        public void saveNetwork()
        {
            if (sUsername.ToUpper().Contains("ENOVATE") == true)
            {
                try
                {

                    Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                {
                        "@SSID",
                        sSSID
                },
                {
                        "@Password",
                        txtPassPhrase.Text
                },
                {
                        "@SerialNo",
                        txtWSSerialNumber.Text
                },
                    {
                                    "@Successful",
                                    sSuccessfulNetworkCredentials
                            },
                {
                        "@SiteID",
                        sSiteID
                },
                  {
                        "@IDUser",
                        sUserID
                },
                {
                        "@Channel",
                        txtChannel.Text
                },
                {
                        "@NetAuthProtocol",
                        iAuthProtocol.ToString()
                }
            };
                    ApiLayer api = new ApiLayer();
                    DataTable dt = new DataTable();

                    dt = api.ExecSpApi("prcSaveNetwork", parameters);
                }
                catch (Exception ex)
                {
                    if (txtSSID.Text.Contains("Manually") == false)
                    {
                        MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                    }
                    else
                    {
                           sSuccessfulNetworkCredentials = "0";
                        MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                    }
                }
            }
        }

        private void btnSaveNetwork_Click(object sender, EventArgs e)
        {
            SaveNetworkInput testDialog = new SaveNetworkInput();
            testDialog.StartPosition = FormStartPosition.CenterParent;
            testDialog.Show();
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
            bool btest = false;
            try
            {
                btest = AttemptTest1();
                Application.DoEvents();
                if (btest == false)
                {
                    AttemptTest2();
                }
            }
            catch (Exception ex)
            {
            }
            saveNetwork();
        }

        private bool AttemptTest1()
        {
            try
            {
                System.Net.HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://capture.myenovate.com/CaptureData.aspx");
                request.Proxy = System.Net.HttpWebRequest.GetSystemWebProxy();
                request.Proxy.Credentials = CredentialCache.DefaultCredentials;

                request.Timeout = 3000;
                request.Method = "HEAD";
                try
                {
                    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                    {
                        if (txtSSID.Text.Contains("Manually") == false)
                        {
                             sSuccessfulNetworkCredentials = "1";
                            MessageBox.Show("Network " + txtSSID.Text + "  has port 80 open and can reach Pulse."); //response.StatusCode == HttpStatusCode.OK;
                        }
                        else
                        {
                             sSuccessfulNetworkCredentials = "1";
                            MessageBox.Show("This laptop's network has port 80 open and can reach Pulse."); //response.StatusCode == HttpStatusCode.OK;
                        }
                        return true;
                    }
                }
                catch (WebException ex)
                {
                    if (txtSSID.Text.Contains("Manually") == false)
                    {
                        MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                    }
                    else
                    {
                        MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                if (txtSSID.Text.Contains("Manually") == false)
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                return false;
            }
        }

        private bool AttemptTest2()
        {
            System.Net.HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://capture.myenovate.com/CaptureData.aspx");
            request.Proxy = GlobalProxySelection.GetEmptyWebProxy();
            request.Proxy.Credentials = CredentialCache.DefaultCredentials;

            request.Timeout = 3000;
            request.Method = "HEAD";
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (txtSSID.Text.Contains("Manually") == false)
                    {
                         sSuccessfulNetworkCredentials = "1";
                        MessageBox.Show("Network " + txtSSID.Text + "  has port 80 open and can reach Pulse."); //response.StatusCode == HttpStatusCode.OK;
                    }
                    else
                    {
                         sSuccessfulNetworkCredentials = "1";
                        MessageBox.Show("This laptop's network has port 80 open and can reach Pulse."); //response.StatusCode == HttpStatusCode.OK;
                    }
                    return true;
                }
            }
            catch (WebException ex)
            {
                if (txtSSID.Text.Contains("Manually") == false)
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                return false;
            }
            catch (Exception ex)
            {
                if (txtSSID.Text.Contains("Manually") == false)
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + txtSSID.Text + " - This network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }
                else
                {
                    MessageBox.Show(ex.Message.ToString() + Environment.NewLine + "This laptop's network cannot reach Pulse. Please ensure port 80 is open and the appropriate firewall rules are in place, and that your laptop is connected to the same network you are setting the Mobius device to.");
                }

                return false;
            }
        }

        private void txtPassPhrase_KeyDown(object sender, KeyEventArgs e)
        {
            if (txtPassPhrase.UseSystemPasswordChar == true)
            {
                txtPassPhrase.UseSystemPasswordChar = false;
                txtPassPhrase.Text = "";
            }

        }

        private void rdoDHCPOff_CheckedChanged_1(object sender, EventArgs e)
        {
            txtIPAddress.Enabled = true;
            txtGateway.Enabled = true;
            txtSubnetMask.Enabled = true;
            txtDNSServer.Enabled = true;
        }

        private void rdoDHCPOn_CheckedChanged(object sender, EventArgs e)
        {
            txtIPAddress.Enabled = false;
            txtGateway.Enabled = false;
            txtSubnetMask.Enabled = false;
            txtDNSServer.Enabled = false;
        }

        private void lblSSID2_DoubleClick(object sender, EventArgs e)
        {


            using (Substitute testDialog = new Substitute())
            {

                testDialog.StartPosition = FormStartPosition.CenterParent;
                var result = testDialog.ShowDialog();
                if (result == DialogResult.OK)
                {
                    SubstituteChar = testDialog.NewCharacter;

                }
                if (SubstituteChar == null)
                {
                    lblSSID2.Text = "$ will be replaced with space.";
                }
                else
                {
                    lblSSID2.Text = SubstituteChar + "  will be replaced with space.";
                }
            }
        }

        private void lblSSID2_Click(object sender, EventArgs e)
        {
            try
            {
                using (Substitute testDialog = new Substitute())
                {

                    testDialog.StartPosition = FormStartPosition.CenterParent;
                    var result = testDialog.ShowDialog();
                    if (result == DialogResult.OK)
                    {
                        SubstituteChar = testDialog.NewCharacter;

                    }
                    if (SubstituteChar == null)
                    {
                        lblSSID2.Text = "$ will be replaced with space.";
                        toolTip1.SetToolTip(txtPassPhrase, SubstituteChar + " character will be replaced with a space.");
                    }
                    else
                    {
                        lblSSID2.Text = SubstituteChar + "  will be replaced with space.";
                        toolTip1.SetToolTip(txtPassPhrase, SubstituteChar + " character will be replaced with a space.");
                    }
                }
            }
            catch (Exception ex)
            {
                var x = ex.Message;
            }
        }

        private void rdoHex_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoHex.Checked == true){
               rdo26HEX.Checked = true;
            }
            else
            {
                 rdo26HEX.Checked = false;
            }
        }

        private void rdoWEP128_CheckedChanged(object sender, EventArgs e)
        {
       if ( rdoWEP128.Checked == true)
            {
                txtPassPhrase.Enabled = true;
                txtWEPKey.Enabled = false;
            }
            else
            {
                
              txtPassPhrase.Enabled = false;
            txtWEPKey.Enabled = true;
            }
        }
    }
}