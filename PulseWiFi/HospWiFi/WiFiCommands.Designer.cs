﻿namespace HospWifi
{
   partial class WiFiCommands
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.btnClose = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.textBox1 = new System.Windows.Forms.TextBox();
         this.textBox2 = new System.Windows.Forms.TextBox();
         this.textBox3 = new System.Windows.Forms.TextBox();
         this.textBox4 = new System.Windows.Forms.TextBox();
         this.textBox5 = new System.Windows.Forms.TextBox();
         this.textBox6 = new System.Windows.Forms.TextBox();
         this.textBox7 = new System.Windows.Forms.TextBox();
         this.textBox8 = new System.Windows.Forms.TextBox();
         this.textBox9 = new System.Windows.Forms.TextBox();
         this.textBox10 = new System.Windows.Forms.TextBox();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // btnClose
         // 
         this.btnClose.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
         this.btnClose.Location = new System.Drawing.Point( 315, 221 );
         this.btnClose.Name = "btnClose";
         this.btnClose.Size = new System.Drawing.Size( 75, 23 );
         this.btnClose.TabIndex = 10;
         this.btnClose.Text = "Close";
         this.btnClose.UseVisualStyleBackColor = true;
         this.btnClose.Click += new System.EventHandler( this.btnClose_Click );
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Location = new System.Drawing.Point( 20, 29 );
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size( 200, 13 );
         this.label1.TabIndex = 12;
         this.label1.Text = "Set Board Communications to 9600 Baud";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point( 14, 49 );
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size( 206, 13 );
         this.label2.TabIndex = 13;
         this.label2.Text = "Set Board Communications to 38400 Baud";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point( 91, 9 );
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size( 129, 13 );
         this.label3.TabIndex = 11;
         this.label3.Text = "Establish Command Mode";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point( 67, 69 );
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size( 153, 13 );
         this.label4.TabIndex = 14;
         this.label4.Text = "Set Roving Baud Rate to 38400";
         // 
         // textBox1
         // 
         this.textBox1.Location = new System.Drawing.Point( 222, 4 );
         this.textBox1.Name = "textBox1";
         this.textBox1.Size = new System.Drawing.Size( 100, 20 );
         this.textBox1.TabIndex = 0;
         this.textBox1.Text = "6E$$$";
         this.textBox1.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox2
         // 
         this.textBox2.Location = new System.Drawing.Point( 222, 24 );
         this.textBox2.Name = "textBox2";
         this.textBox2.Size = new System.Drawing.Size( 100, 20 );
         this.textBox2.TabIndex = 1;
         this.textBox2.Text = "6G";
         this.textBox2.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox3
         // 
         this.textBox3.Location = new System.Drawing.Point( 222, 44 );
         this.textBox3.Name = "textBox3";
         this.textBox3.Size = new System.Drawing.Size( 100, 20 );
         this.textBox3.TabIndex = 2;
         this.textBox3.Text = "6H";
         this.textBox3.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox4
         // 
         this.textBox4.Location = new System.Drawing.Point( 222, 64 );
         this.textBox4.Name = "textBox4";
         this.textBox4.Size = new System.Drawing.Size( 100, 20 );
         this.textBox4.TabIndex = 3;
         this.textBox4.Text = "6Eset u b 38400";
         this.textBox4.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox5
         // 
         this.textBox5.Location = new System.Drawing.Point( 222, 84 );
         this.textBox5.Name = "textBox5";
         this.textBox5.Size = new System.Drawing.Size( 100, 20 );
         this.textBox5.TabIndex = 4;
         this.textBox5.Text = "6Eset c s 768";
         this.textBox5.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox6
         // 
         this.textBox6.Location = new System.Drawing.Point( 222, 104 );
         this.textBox6.Name = "textBox6";
         this.textBox6.Size = new System.Drawing.Size( 100, 20 );
         this.textBox6.TabIndex = 5;
         this.textBox6.Text = "6Eset c t 400";
         this.textBox6.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox7
         // 
         this.textBox7.Location = new System.Drawing.Point( 222, 124 );
         this.textBox7.Name = "textBox7";
         this.textBox7.Size = new System.Drawing.Size( 100, 20 );
         this.textBox7.TabIndex = 6;
         this.textBox7.Text = "6Eset w e 0";
         this.textBox7.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox8
         // 
         this.textBox8.Location = new System.Drawing.Point( 222, 144 );
         this.textBox8.Name = "textBox8";
         this.textBox8.Size = new System.Drawing.Size( 138, 20 );
         this.textBox8.TabIndex = 7;
         this.textBox8.Text = "6Eset i h 208.62.45.106";
         this.textBox8.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox9
         // 
         this.textBox9.Location = new System.Drawing.Point( 222, 164 );
         this.textBox9.Name = "textBox9";
         this.textBox9.Size = new System.Drawing.Size( 100, 20 );
         this.textBox9.TabIndex = 8;
         this.textBox9.Text = "6Eset i r 80";
         this.textBox9.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // textBox10
         // 
         this.textBox10.Location = new System.Drawing.Point( 222, 184 );
         this.textBox10.Name = "textBox10";
         this.textBox10.Size = new System.Drawing.Size( 100, 20 );
         this.textBox10.TabIndex = 9;
         this.textBox10.Text = "6Eset c r 0";
         this.textBox10.DoubleClick += new System.EventHandler( this.textBox1_DoubleClick );
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Location = new System.Drawing.Point( 66, 89 );
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size( 154, 13 );
         this.label5.TabIndex = 15;
         this.label5.Text = "Set Communications Buffer Size";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Location = new System.Drawing.Point( 76, 109 );
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size( 144, 13 );
         this.label6.TabIndex = 16;
         this.label6.Text = "Set Communications Timeout";
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point( 116, 129 );
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size( 104, 13 );
         this.label7.TabIndex = 17;
         this.label7.Text = "Set Internal Antenna";
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point( 118, 149 );
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size( 102, 13 );
         this.label8.TabIndex = 18;
         this.label8.Text = "Set Host IP Address";
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point( 135, 169 );
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size( 85, 13 );
         this.label9.TabIndex = 19;
         this.label9.Text = "Set Remote Port";
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point( 106, 189 );
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size( 114, 13 );
         this.label10.TabIndex = 20;
         this.label10.Text = "Set Remote Response";
         // 
         // label16
         // 
         this.label16.AutoSize = true;
         this.label16.Location = new System.Drawing.Point( 324, 129 );
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size( 63, 13 );
         this.label16.TabIndex = 21;
         this.label16.Text = "External = 1";
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point( 12, 234 );
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size( 147, 13 );
         this.label11.TabIndex = 22;
         this.label11.Text = "Double-Click textbox to select";
         // 
         // WiFiCommands
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size( 402, 256 );
         this.Controls.Add( this.label11 );
         this.Controls.Add( this.label16 );
         this.Controls.Add( this.label10 );
         this.Controls.Add( this.label9 );
         this.Controls.Add( this.label8 );
         this.Controls.Add( this.label7 );
         this.Controls.Add( this.label6 );
         this.Controls.Add( this.label5 );
         this.Controls.Add( this.textBox10 );
         this.Controls.Add( this.textBox9 );
         this.Controls.Add( this.textBox8 );
         this.Controls.Add( this.textBox7 );
         this.Controls.Add( this.textBox6 );
         this.Controls.Add( this.textBox5 );
         this.Controls.Add( this.textBox4 );
         this.Controls.Add( this.textBox3 );
         this.Controls.Add( this.textBox2 );
         this.Controls.Add( this.textBox1 );
         this.Controls.Add( this.label4 );
         this.Controls.Add( this.label3 );
         this.Controls.Add( this.label2 );
         this.Controls.Add( this.label1 );
         this.Controls.Add( this.btnClose );
         this.Name = "WiFiCommands";
         this.Text = "WiFi Commands";
         this.ResumeLayout( false );
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btnClose;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.TextBox textBox2;
      private System.Windows.Forms.TextBox textBox3;
      private System.Windows.Forms.TextBox textBox4;
      private System.Windows.Forms.TextBox textBox5;
      private System.Windows.Forms.TextBox textBox6;
      private System.Windows.Forms.TextBox textBox7;
      private System.Windows.Forms.TextBox textBox8;
      private System.Windows.Forms.TextBox textBox9;
      private System.Windows.Forms.TextBox textBox10;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label11;
   }
}
