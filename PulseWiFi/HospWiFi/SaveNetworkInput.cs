﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HospWifi
{
    public partial class SaveNetworkInput : Telerik.WinControls.UI.ShapedForm
    {
        public SaveNetworkInput()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SSID", txtSSID.Text },
                { "@Password", txtPassKey.Text},
                { "@SerialNo", ""},
                { "@SiteID", ddlSites2.SelectedValue.ToString()},
                { "@Channel", "0"},
			   { "@NetAuthProtocol",  cmbType.SelectedItem.ToString()}
            };
                ApiLayer api = new ApiLayer();
                DataTable dt = new DataTable();

                dt = api.ExecSpApi("prcSaveNetwork", parameters);
                       MessageBox.Show("Network information saved for " + ddlSites2.SelectedItem.ToString());
                  this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Network information failed to save! Is your computer connected to the internet? " );

            }
          
        }

        private void SaveNetworkInput_Load(object sender, EventArgs e)
        {
            ApiLayer api = new ApiLayer();
            DataTable dt = new DataTable();
             Dictionary <string,string>   parameters = new Dictionary<string, string>() {
                { "@IDSite", "4" } };

                dt = api.ExecSpApi("prcSitesSelect", parameters);
                if (dt.Rows.Count > 1) //Found Site for User
                {
                    
                    ddlSites2.DataSource = dt;

                }
            }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        }
    }
    
