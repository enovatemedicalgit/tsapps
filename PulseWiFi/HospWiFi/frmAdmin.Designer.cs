﻿namespace HospWifi
{
    partial class frmAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.roundRectShapeForm = new Telerik.WinControls.RoundRectShape(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.AddUser = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLastNameAdd = new Telerik.WinControls.UI.RadTextBox();
            this.txtFirstNameAdd = new Telerik.WinControls.UI.RadTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.ddlSites1 = new Telerik.WinControls.UI.RadDropDownList();
            this.txtUsernameAdd = new Telerik.WinControls.UI.RadTextBox();
            this.txtUsernameDelete = new Telerik.WinControls.UI.RadTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radTextBoxControl1 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtSerialsMove = new Telerik.WinControls.UI.RadTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlSites2 = new Telerik.WinControls.UI.RadDropDownList();
            this.txtSerialRecycle = new Telerik.WinControls.UI.RadTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.rbIDN = new Telerik.WinControls.UI.RadRadioButton();
            this.rbFacility = new Telerik.WinControls.UI.RadRadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSFCNum = new Telerik.WinControls.UI.RadTextBox();
            this.txtState = new Telerik.WinControls.UI.RadTextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.txtFacilityIDNName = new Telerik.WinControls.UI.RadTextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.ddlCustomers = new Telerik.WinControls.UI.RadDropDownList();
            this.radTextBoxControl2 = new Telerik.WinControls.UI.RadTextBoxControl();
            this.button5 = new System.Windows.Forms.Button();
            this.ddlSites3 = new Telerik.WinControls.UI.RadDropDownList();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.AddUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastNameAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstNameAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsernameAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsernameDelete)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialsMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialRecycle)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbIDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbFacility)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFCNum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacilityIDNName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCustomers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites3)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Username";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Delete";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Pulse Administrator";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(271, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(46, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "Delete";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.AddUser);
            this.groupBox1.Controls.Add(this.txtUsernameDelete);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Location = new System.Drawing.Point(11, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(343, 372);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Users";
            // 
            // AddUser
            // 
            this.AddUser.Controls.Add(this.label6);
            this.AddUser.Controls.Add(this.label5);
            this.AddUser.Controls.Add(this.label4);
            this.AddUser.Controls.Add(this.txtLastNameAdd);
            this.AddUser.Controls.Add(this.txtFirstNameAdd);
            this.AddUser.Controls.Add(this.button2);
            this.AddUser.Controls.Add(this.label3);
            this.AddUser.Controls.Add(this.ddlSites1);
            this.AddUser.Controls.Add(this.txtUsernameAdd);
            this.AddUser.Location = new System.Drawing.Point(7, 142);
            this.AddUser.Name = "AddUser";
            this.AddUser.Size = new System.Drawing.Size(322, 193);
            this.AddUser.TabIndex = 18;
            this.AddUser.TabStop = false;
            this.AddUser.Text = "Add User";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(6, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 24;
            this.label6.Text = "Facility Or IDN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 22;
            this.label5.Text = "Last";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "First";
            // 
            // txtLastNameAdd
            // 
            this.txtLastNameAdd.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtLastNameAdd.Location = new System.Drawing.Point(92, 79);
            this.txtLastNameAdd.Name = "txtLastNameAdd";
            this.txtLastNameAdd.NullText = "Last Name";
            // 
            // 
            // 
            this.txtLastNameAdd.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 79, 100, 20);
            this.txtLastNameAdd.RootElement.StretchVertically = true;
            this.txtLastNameAdd.Size = new System.Drawing.Size(218, 20);
            this.txtLastNameAdd.TabIndex = 23;
            // 
            // txtFirstNameAdd
            // 
            this.txtFirstNameAdd.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFirstNameAdd.Location = new System.Drawing.Point(92, 43);
            this.txtFirstNameAdd.Name = "txtFirstNameAdd";
            this.txtFirstNameAdd.NullText = "First Name";
            // 
            // 
            // 
            this.txtFirstNameAdd.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 43, 100, 20);
            this.txtFirstNameAdd.RootElement.StretchVertically = true;
            this.txtFirstNameAdd.Size = new System.Drawing.Size(218, 20);
            this.txtFirstNameAdd.TabIndex = 21;
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button2.Location = new System.Drawing.Point(264, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(46, 23);
            this.button2.TabIndex = 19;
            this.button2.Text = "Add";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // ddlSites1
            // 
            this.ddlSites1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlSites1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ddlSites1.DefaultItemsCountInDropDown = 24;
            this.ddlSites1.DisplayMember = "SiteName";
            this.ddlSites1.Location = new System.Drawing.Point(9, 132);
            this.ddlSites1.Name = "ddlSites1";
            // 
            // 
            // 
            this.ddlSites1.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 132, 125, 20);
            this.ddlSites1.RootElement.StretchVertically = true;
            this.ddlSites1.Size = new System.Drawing.Size(301, 20);
            this.ddlSites1.TabIndex = 17;
            this.ddlSites1.Text = "Select Facility";
            this.ddlSites1.ValueMember = "IDSite";
            // 
            // txtUsernameAdd
            // 
            this.txtUsernameAdd.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUsernameAdd.Location = new System.Drawing.Point(92, 14);
            this.txtUsernameAdd.Name = "txtUsernameAdd";
            this.txtUsernameAdd.NullText = "Full Username/Email";
            // 
            // 
            // 
            this.txtUsernameAdd.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 14, 100, 20);
            this.txtUsernameAdd.RootElement.StretchVertically = true;
            this.txtUsernameAdd.Size = new System.Drawing.Size(218, 20);
            this.txtUsernameAdd.TabIndex = 16;
            // 
            // txtUsernameDelete
            // 
            this.txtUsernameDelete.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtUsernameDelete.Location = new System.Drawing.Point(66, 31);
            this.txtUsernameDelete.Name = "txtUsernameDelete";
            this.txtUsernameDelete.NullText = "Full Username";
            // 
            // 
            // 
            this.txtUsernameDelete.RootElement.ControlBounds = new System.Drawing.Rectangle(66, 31, 100, 20);
            this.txtUsernameDelete.RootElement.StretchVertically = true;
            this.txtUsernameDelete.Size = new System.Drawing.Size(251, 20);
            this.txtUsernameDelete.TabIndex = 15;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radTextBoxControl1);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.txtSerialRecycle);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Location = new System.Drawing.Point(361, 40);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(349, 372);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Assets";
            // 
            // radTextBoxControl1
            // 
            this.radTextBoxControl1.BackColor = System.Drawing.Color.Transparent;
            this.radTextBoxControl1.Location = new System.Drawing.Point(15, 300);
            this.radTextBoxControl1.Multiline = true;
            this.radTextBoxControl1.Name = "radTextBoxControl1";
            // 
            // 
            // 
            this.radTextBoxControl1.RootElement.ControlBounds = new System.Drawing.Rectangle(15, 300, 125, 20);
            this.radTextBoxControl1.SelectionOpacity = 90;
            this.radTextBoxControl1.Size = new System.Drawing.Size(322, 63);
            this.radTextBoxControl1.TabIndex = 26;
            this.radTextBoxControl1.Text = "*Incorrect Subnet or AP associations will cause assets to move back regardless of" +
    " manual associations. Always check subnet mappings and AP mapping report before " +
    "manually moving assets. ";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtSerialsMove);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.button4);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.ddlSites2);
            this.groupBox3.Location = new System.Drawing.Point(15, 101);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(322, 193);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Move Asset";
            // 
            // txtSerialsMove
            // 
            this.txtSerialsMove.AutoSize = false;
            this.txtSerialsMove.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtSerialsMove.Location = new System.Drawing.Point(65, 21);
            this.txtSerialsMove.Multiline = true;
            this.txtSerialsMove.Name = "txtSerialsMove";
            this.txtSerialsMove.NullText = "1234, 1235, 1236, 123...";
            // 
            // 
            // 
            this.txtSerialsMove.RootElement.ControlBounds = new System.Drawing.Rectangle(65, 21, 100, 20);
            this.txtSerialsMove.Size = new System.Drawing.Size(245, 83);
            this.txtSerialsMove.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 111);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 24;
            this.label8.Text = "To Facility:";
            // 
            // button4
            // 
            this.button4.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button4.Location = new System.Drawing.Point(264, 157);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(46, 23);
            this.button4.TabIndex = 19;
            this.button4.Text = "Move";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Serial(s)";
            // 
            // ddlSites2
            // 
            this.ddlSites2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlSites2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ddlSites2.DefaultItemsCountInDropDown = 24;
            this.ddlSites2.DisplayMember = "SiteName";
            this.ddlSites2.Location = new System.Drawing.Point(9, 131);
            this.ddlSites2.Name = "ddlSites2";
            // 
            // 
            // 
            this.ddlSites2.RootElement.ControlBounds = new System.Drawing.Rectangle(9, 131, 125, 20);
            this.ddlSites2.RootElement.StretchVertically = true;
            this.ddlSites2.Size = new System.Drawing.Size(301, 20);
            this.ddlSites2.TabIndex = 17;
            this.ddlSites2.Text = "Select Facility";
            this.ddlSites2.ValueMember = "IDSite";
            // 
            // txtSerialRecycle
            // 
            this.txtSerialRecycle.AutoSize = false;
            this.txtSerialRecycle.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtSerialRecycle.Location = new System.Drawing.Point(80, 26);
            this.txtSerialRecycle.Multiline = true;
            this.txtSerialRecycle.Name = "txtSerialRecycle";
            this.txtSerialRecycle.NullText = "1234, 1235, 1236, 123...";
            // 
            // 
            // 
            this.txtSerialRecycle.RootElement.ControlBounds = new System.Drawing.Rectangle(80, 26, 100, 20);
            this.txtSerialRecycle.Size = new System.Drawing.Size(256, 45);
            this.txtSerialRecycle.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Serial(s)";
            // 
            // button3
            // 
            this.button3.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button3.Location = new System.Drawing.Point(279, 77);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(57, 23);
            this.button3.TabIndex = 16;
            this.button3.Text = "Recycle ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.button7);
            this.groupBox4.Controls.Add(this.ddlCustomers);
            this.groupBox4.Controls.Add(this.radTextBoxControl2);
            this.groupBox4.Controls.Add(this.button5);
            this.groupBox4.Controls.Add(this.ddlSites3);
            this.groupBox4.Controls.Add(this.button6);
            this.groupBox4.Location = new System.Drawing.Point(716, 40);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(349, 372);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Customers";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.rbIDN);
            this.groupBox5.Controls.Add(this.rbFacility);
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.txtSFCNum);
            this.groupBox5.Controls.Add(this.txtState);
            this.groupBox5.Controls.Add(this.button8);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Controls.Add(this.txtFacilityIDNName);
            this.groupBox5.Location = new System.Drawing.Point(15, 142);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(322, 174);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Add Facility or IDN";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(35, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "Type";
            // 
            // rbIDN
            // 
            this.rbIDN.BackColor = System.Drawing.Color.Transparent;
            this.rbIDN.Location = new System.Drawing.Point(244, 111);
            this.rbIDN.Name = "rbIDN";
            // 
            // 
            // 
            this.rbIDN.RootElement.ControlBounds = new System.Drawing.Rectangle(244, 111, 110, 18);
            this.rbIDN.RootElement.StretchHorizontally = true;
            this.rbIDN.RootElement.StretchVertically = true;
            this.rbIDN.Size = new System.Drawing.Size(66, 18);
            this.rbIDN.TabIndex = 25;
            this.rbIDN.Text = "IDN (HQ)";
            // 
            // rbFacility
            // 
            this.rbFacility.BackColor = System.Drawing.Color.Transparent;
            this.rbFacility.Location = new System.Drawing.Point(93, 111);
            this.rbFacility.Name = "rbFacility";
            // 
            // 
            // 
            this.rbFacility.RootElement.ControlBounds = new System.Drawing.Rectangle(93, 111, 110, 18);
            this.rbFacility.RootElement.StretchHorizontally = true;
            this.rbFacility.RootElement.StretchVertically = true;
            this.rbFacility.Size = new System.Drawing.Size(54, 18);
            this.rbFacility.TabIndex = 24;
            this.rbFacility.Text = "Facility";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 81);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "SF C Number";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 45);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(37, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "State";
            // 
            // txtSFCNum
            // 
            this.txtSFCNum.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtSFCNum.Location = new System.Drawing.Point(92, 79);
            this.txtSFCNum.Name = "txtSFCNum";
            this.txtSFCNum.NullText = "C000361";
            // 
            // 
            // 
            this.txtSFCNum.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 79, 100, 20);
            this.txtSFCNum.RootElement.StretchVertically = true;
            this.txtSFCNum.Size = new System.Drawing.Size(218, 20);
            this.txtSFCNum.TabIndex = 23;
            // 
            // txtState
            // 
            this.txtState.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtState.Location = new System.Drawing.Point(92, 43);
            this.txtState.Name = "txtState";
            this.txtState.NullText = "TN, TX, etc. Two Letters";
            // 
            // 
            // 
            this.txtState.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 43, 100, 20);
            this.txtState.RootElement.StretchVertically = true;
            this.txtState.Size = new System.Drawing.Size(218, 20);
            this.txtState.TabIndex = 21;
            // 
            // button8
            // 
            this.button8.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button8.Location = new System.Drawing.Point(264, 145);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(46, 23);
            this.button8.TabIndex = 19;
            this.button8.Text = "Add";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(6, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(39, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Name";
            // 
            // txtFacilityIDNName
            // 
            this.txtFacilityIDNName.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtFacilityIDNName.Location = new System.Drawing.Point(92, 14);
            this.txtFacilityIDNName.Name = "txtFacilityIDNName";
            this.txtFacilityIDNName.NullText = "Name of IDN or Facility";
            // 
            // 
            // 
            this.txtFacilityIDNName.RootElement.ControlBounds = new System.Drawing.Rectangle(92, 14, 100, 20);
            this.txtFacilityIDNName.RootElement.StretchVertically = true;
            this.txtFacilityIDNName.Size = new System.Drawing.Size(218, 20);
            this.txtFacilityIDNName.TabIndex = 16;
            // 
            // button7
            // 
            this.button7.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button7.Location = new System.Drawing.Point(280, 107);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(57, 23);
            this.button7.TabIndex = 28;
            this.button7.Text = "Delete";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // ddlCustomers
            // 
            this.ddlCustomers.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlCustomers.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ddlCustomers.DefaultItemsCountInDropDown = 24;
            this.ddlCustomers.DisplayMember = "SiteName";
            this.ddlCustomers.Location = new System.Drawing.Point(15, 81);
            this.ddlCustomers.Name = "ddlCustomers";
            // 
            // 
            // 
            this.ddlCustomers.RootElement.ControlBounds = new System.Drawing.Rectangle(15, 81, 125, 20);
            this.ddlCustomers.RootElement.StretchVertically = true;
            this.ddlCustomers.Size = new System.Drawing.Size(322, 20);
            this.ddlCustomers.TabIndex = 27;
            this.ddlCustomers.Text = "Select Customer";
            this.ddlCustomers.ValueMember = "IDSite";
            // 
            // radTextBoxControl2
            // 
            this.radTextBoxControl2.BackColor = System.Drawing.Color.Transparent;
            this.radTextBoxControl2.Location = new System.Drawing.Point(15, 322);
            this.radTextBoxControl2.Multiline = true;
            this.radTextBoxControl2.Name = "radTextBoxControl2";
            // 
            // 
            // 
            this.radTextBoxControl2.RootElement.ControlBounds = new System.Drawing.Rectangle(15, 322, 125, 20);
            this.radTextBoxControl2.SelectionOpacity = 90;
            this.radTextBoxControl2.Size = new System.Drawing.Size(322, 41);
            this.radTextBoxControl2.TabIndex = 26;
            this.radTextBoxControl2.Text = "*Please check asset associations before removing a site or IDN. ";
            // 
            // button5
            // 
            this.button5.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button5.Location = new System.Drawing.Point(217, 52);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(57, 23);
            this.button5.TabIndex = 19;
            this.button5.Text = "Move";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // ddlSites3
            // 
            this.ddlSites3.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ddlSites3.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.ddlSites3.DefaultItemsCountInDropDown = 24;
            this.ddlSites3.DisplayMember = "SiteName";
            this.ddlSites3.Location = new System.Drawing.Point(15, 26);
            this.ddlSites3.Name = "ddlSites3";
            // 
            // 
            // 
            this.ddlSites3.RootElement.ControlBounds = new System.Drawing.Rectangle(15, 26, 125, 20);
            this.ddlSites3.RootElement.StretchVertically = true;
            this.ddlSites3.Size = new System.Drawing.Size(322, 20);
            this.ddlSites3.TabIndex = 17;
            this.ddlSites3.Text = "Select Facility";
            this.ddlSites3.ValueMember = "IDSite";
            // 
            // button6
            // 
            this.button6.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button6.Location = new System.Drawing.Point(280, 52);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(57, 23);
            this.button6.TabIndex = 16;
            this.button6.Text = "Delete";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // frmAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(219)))), ((int)(((byte)(254)))));
            this.ClientSize = new System.Drawing.Size(1079, 427);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Name = "frmAdmin";
            this.Shape = this.roundRectShapeForm;
            this.Text = "frmAdmin";
            this.Load += new System.EventHandler(this.frmAdmin_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.AddUser.ResumeLayout(false);
            this.AddUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtLastNameAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstNameAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsernameAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtUsernameDelete)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl1)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialsMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSerialRecycle)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rbIDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbFacility)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSFCNum)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtState)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacilityIDNName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlCustomers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBoxControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ddlSites3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.RoundRectShape roundRectShapeForm;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Telerik.WinControls.UI.RadTextBox txtUsernameDelete;
        private Telerik.WinControls.UI.RadTextBox txtUsernameAdd;
        private System.Windows.Forms.GroupBox AddUser;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Telerik.WinControls.UI.RadTextBox txtLastNameAdd;
        private Telerik.WinControls.UI.RadTextBox txtFirstNameAdd;
        private System.Windows.Forms.Button button2;
        private Telerik.WinControls.UI.RadDropDownList ddlSites1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl1;
        private System.Windows.Forms.GroupBox groupBox3;
        private Telerik.WinControls.UI.RadTextBox txtSerialsMove;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label11;
        private Telerik.WinControls.UI.RadDropDownList ddlSites2;
        private Telerik.WinControls.UI.RadTextBox txtSerialRecycle;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label9;
        private Telerik.WinControls.UI.RadRadioButton rbIDN;
        private Telerik.WinControls.UI.RadRadioButton rbFacility;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private Telerik.WinControls.UI.RadTextBox txtSFCNum;
        private Telerik.WinControls.UI.RadTextBox txtState;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label13;
        private Telerik.WinControls.UI.RadTextBox txtFacilityIDNName;
        private System.Windows.Forms.Button button7;
        private Telerik.WinControls.UI.RadDropDownList ddlCustomers;
        private Telerik.WinControls.UI.RadTextBoxControl radTextBoxControl2;
        private System.Windows.Forms.Button button5;
        private Telerik.WinControls.UI.RadDropDownList ddlSites3;
        private System.Windows.Forms.Button button6;
    }
}
