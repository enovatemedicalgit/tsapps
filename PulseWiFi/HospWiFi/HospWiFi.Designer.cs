﻿﻿namespace HospWiFi
{
   partial class HospWiFi
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HospWiFi));
            this.pbxRebootWiFi = new System.Windows.Forms.PictureBox();
            this.pbxSaveConfiguration = new System.Windows.Forms.PictureBox();
            this.lblSSID1 = new System.Windows.Forms.Label();
            this.txtSSID = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rdoHex = new System.Windows.Forms.RadioButton();
            this.rdoASCII = new System.Windows.Forms.RadioButton();
            this.lblSSID2 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rdo26HEX = new System.Windows.Forms.RadioButton();
            this.rdo13ASCII = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassPhrase = new System.Windows.Forms.TextBox();
            this.txtWEPKey = new System.Windows.Forms.TextBox();
            this.rdoWPA2 = new System.Windows.Forms.RadioButton();
            this.rdoMixedWPA = new System.Windows.Forms.RadioButton();
            this.rdoWPA1 = new System.Windows.Forms.RadioButton();
            this.rdoWEP128 = new System.Windows.Forms.RadioButton();
            this.rdoOpen = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.txtChannel = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtDNSServer = new System.Windows.Forms.TextBox();
            this.txtGateway = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.txtSubnetMask = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rdoDHCPOn = new System.Windows.Forms.RadioButton();
            this.rdoDHCPOff = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoBaycharger = new System.Windows.Forms.RadioButton();
            this.rdoWorkstation = new System.Windows.Forms.RadioButton();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnCommandSend = new System.Windows.Forms.Button();
            this.txtCommandSend = new System.Windows.Forms.TextBox();
            this.txtCommand = new System.Windows.Forms.TextBox();
            this.btnCommands = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rdoBayChargerB = new System.Windows.Forms.RadioButton();
            this.rdoWorkstationB = new System.Windows.Forms.RadioButton();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSSIDB = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblSSID4 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rdo26HEXB = new System.Windows.Forms.RadioButton();
            this.rdo13ASCIIB = new System.Windows.Forms.RadioButton();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPassPhraseB = new System.Windows.Forms.TextBox();
            this.txtWEPKeyB = new System.Windows.Forms.TextBox();
            this.rdoWPA2B = new System.Windows.Forms.RadioButton();
            this.rdoMixedWPAB = new System.Windows.Forms.RadioButton();
            this.rdoWPA1B = new System.Windows.Forms.RadioButton();
            this.rdoWEP128B = new System.Windows.Forms.RadioButton();
            this.rdoOpenB = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtDNSServerB = new System.Windows.Forms.TextBox();
            this.txtGatewayB = new System.Windows.Forms.TextBox();
            this.txtSubnetMaskB = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtIPAddressB = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.rdoDHCPOnB = new System.Windows.Forms.RadioButton();
            this.rdoDHCPOffB = new System.Windows.Forms.RadioButton();
            this.txtResponse = new System.Windows.Forms.TextBox();
            this.txtChannelB = new System.Windows.Forms.TextBox();
            this.btnClose = new VistaButton.VistaButton();
            this.btnConnectB = new VistaButton.VistaButton();
            this.btnSetChannelB = new VistaButton.VistaButton();
            this.btnSetSSIDB = new VistaButton.VistaButton();
            this.btnCommandModeB = new VistaButton.VistaButton();
            this.btnSetDHCPB = new VistaButton.VistaButton();
            this.btnSetAuthB = new VistaButton.VistaButton();
            this.btnSendDataPacket = new VistaButton.VistaButton();
            this.btnLEDReset = new VistaButton.VistaButton();
            this.btnRebootB = new VistaButton.VistaButton();
            this.btnSaveB = new VistaButton.VistaButton();
            this.btnUpdateRoving = new VistaButton.VistaButton();
            this.btnSendPacketNow = new VistaButton.VistaButton();
            this.btnSetAllParams = new VistaButton.VistaButton();
            this.btnReboot = new VistaButton.VistaButton();
            this.btnConnect = new VistaButton.VistaButton();
            this.gboxPulseStatus = new System.Windows.Forms.GroupBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.pbxCommunicating = new System.Windows.Forms.PictureBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.pbxCommunicating2 = new System.Windows.Forms.PictureBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gbPulseAssetSetup = new System.Windows.Forms.GroupBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label70 = new System.Windows.Forms.Label();
            this.ilsLED = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.functionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSubstituteCharacterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.webBrowserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ExpertPanel = new System.Windows.Forms.Panel();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.btnWSSNSet = new VistaButton.VistaButton();
            this.btnWSSNGet = new VistaButton.VistaButton();
            this.txtWSSerialNumber = new System.Windows.Forms.TextBox();
            this.BeginnerPanel = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.ckbEnableCommandLogging = new System.Windows.Forms.CheckBox();
            this.label51 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label49 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label10 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.lblSSID3 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pbxSetSSIDB = new System.Windows.Forms.PictureBox();
            this.pbxSetChannelB = new System.Windows.Forms.PictureBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.pbxCommandModeB = new System.Windows.Forms.PictureBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.pbxSetAuthB = new System.Windows.Forms.PictureBox();
            this.pbxSetDHCPB = new System.Windows.Forms.PictureBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.pbxSaveConfigurationB = new System.Windows.Forms.PictureBox();
            this.pbxRebootWiFiB = new System.Windows.Forms.PictureBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.gboxPulseStatus.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating2)).BeginInit();
            this.gbPulseAssetSetup.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.ExpertPanel.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.BeginnerPanel.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetSSIDB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetChannelB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommandModeB)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetAuthB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetDHCPB)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfigurationB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFiB)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbxRebootWiFi
            // 
            this.pbxRebootWiFi.Location = new System.Drawing.Point(132, 143);
            this.pbxRebootWiFi.Name = "pbxRebootWiFi";
            this.pbxRebootWiFi.Size = new System.Drawing.Size(17, 17);
            this.pbxRebootWiFi.TabIndex = 263;
            this.pbxRebootWiFi.TabStop = false;
            // 
            // pbxSaveConfiguration
            // 
            this.pbxSaveConfiguration.Location = new System.Drawing.Point(132, 88);
            this.pbxSaveConfiguration.Name = "pbxSaveConfiguration";
            this.pbxSaveConfiguration.Size = new System.Drawing.Size(17, 17);
            this.pbxSaveConfiguration.TabIndex = 262;
            this.pbxSaveConfiguration.TabStop = false;
            // 
            // lblSSID1
            // 
            this.lblSSID1.AutoSize = true;
            this.lblSSID1.Location = new System.Drawing.Point(235, 68);
            this.lblSSID1.Name = "lblSSID1";
            this.lblSSID1.Size = new System.Drawing.Size(127, 13);
            this.lblSSID1.TabIndex = 17;
            this.lblSSID1.Text = "(Will convert spaces to $)";
            // 
            // txtSSID
            // 
            this.txtSSID.Location = new System.Drawing.Point(129, 44);
            this.txtSSID.MaxLength = 32;
            this.txtSSID.Name = "txtSSID";
            this.txtSSID.Size = new System.Drawing.Size(233, 20);
            this.txtSSID.TabIndex = 12;
            this.txtSSID.Text = "Enter Manual SSID Here";
            this.txtSSID.Leave += new System.EventHandler(this.txtSSID_Leave);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox14);
            this.groupBox2.Controls.Add(this.panel1);
            this.groupBox2.Controls.Add(this.lblSSID2);
            this.groupBox2.Controls.Add(this.groupBox8);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtPassPhrase);
            this.groupBox2.Controls.Add(this.txtWEPKey);
            this.groupBox2.Controls.Add(this.rdoWPA2);
            this.groupBox2.Controls.Add(this.rdoMixedWPA);
            this.groupBox2.Controls.Add(this.rdoWPA1);
            this.groupBox2.Controls.Add(this.rdoWEP128);
            this.groupBox2.Controls.Add(this.rdoOpen);
            this.groupBox2.Location = new System.Drawing.Point(161, 211);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(558, 145);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Authentication";
            this.toolTip1.SetToolTip(this.groupBox2, "Choose the Authentication method");
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rdoHex);
            this.panel1.Controls.Add(this.rdoASCII);
            this.panel1.Location = new System.Drawing.Point(183, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 26);
            this.panel1.TabIndex = 21;
            // 
            // rdoHex
            // 
            this.rdoHex.AutoSize = true;
            this.rdoHex.Enabled = false;
            this.rdoHex.Location = new System.Drawing.Point(81, 3);
            this.rdoHex.Name = "rdoHex";
            this.rdoHex.Size = new System.Drawing.Size(47, 17);
            this.rdoHex.TabIndex = 20;
            this.rdoHex.Text = "HEX";
            this.rdoHex.UseVisualStyleBackColor = true;
            // 
            // rdoASCII
            // 
            this.rdoASCII.AutoSize = true;
            this.rdoASCII.Checked = true;
            this.rdoASCII.Enabled = false;
            this.rdoASCII.Location = new System.Drawing.Point(9, 3);
            this.rdoASCII.Name = "rdoASCII";
            this.rdoASCII.Size = new System.Drawing.Size(55, 17);
            this.rdoASCII.TabIndex = 19;
            this.rdoASCII.TabStop = true;
            this.rdoASCII.Text = "ASCII ";
            this.rdoASCII.UseVisualStyleBackColor = true;
            this.rdoASCII.CheckedChanged += new System.EventHandler(this.rdoASCII_CheckedChanged);
            // 
            // lblSSID2
            // 
            this.lblSSID2.AutoSize = true;
            this.lblSSID2.Location = new System.Drawing.Point(341, 112);
            this.lblSSID2.Name = "lblSSID2";
            this.lblSSID2.Size = new System.Drawing.Size(127, 13);
            this.lblSSID2.TabIndex = 18;
            this.lblSSID2.Text = "(Will convert spaces to $)";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.rdo26HEX);
            this.groupBox8.Controls.Add(this.rdo13ASCII);
            this.groupBox8.Enabled = false;
            this.groupBox8.Location = new System.Drawing.Point(382, 15);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(86, 65);
            this.groupBox8.TabIndex = 10;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "# chars";
            // 
            // rdo26HEX
            // 
            this.rdo26HEX.AutoSize = true;
            this.rdo26HEX.Location = new System.Drawing.Point(10, 36);
            this.rdo26HEX.Name = "rdo26HEX";
            this.rdo26HEX.Size = new System.Drawing.Size(62, 17);
            this.rdo26HEX.TabIndex = 1;
            this.rdo26HEX.Text = "26 HEX";
            this.rdo26HEX.UseVisualStyleBackColor = true;
            this.rdo26HEX.CheckedChanged += new System.EventHandler(this.rdo13ASCII_CheckedChanged);
            // 
            // rdo13ASCII
            // 
            this.rdo13ASCII.AutoSize = true;
            this.rdo13ASCII.Checked = true;
            this.rdo13ASCII.Location = new System.Drawing.Point(10, 15);
            this.rdo13ASCII.Name = "rdo13ASCII";
            this.rdo13ASCII.Size = new System.Drawing.Size(70, 17);
            this.rdo13ASCII.TabIndex = 0;
            this.rdo13ASCII.TabStop = true;
            this.rdo13ASCII.Text = "13 ASCII ";
            this.rdo13ASCII.UseVisualStyleBackColor = true;
            this.rdo13ASCII.CheckedChanged += new System.EventHandler(this.rdo13ASCII_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(186, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(176, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "WPA Passphrase (1-64 chars)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(186, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "WEP Key (13/26 chars)";
            // 
            // txtPassPhrase
            // 
            this.txtPassPhrase.Enabled = false;
            this.txtPassPhrase.Location = new System.Drawing.Point(183, 86);
            this.txtPassPhrase.MaxLength = 64;
            this.txtPassPhrase.Name = "txtPassPhrase";
            this.txtPassPhrase.PasswordChar = '*';
            this.txtPassPhrase.Size = new System.Drawing.Size(285, 20);
            this.txtPassPhrase.TabIndex = 6;
            this.txtPassPhrase.Leave += new System.EventHandler(this.txtPassPhrase_Leave);
            // 
            // txtWEPKey
            // 
            this.txtWEPKey.Location = new System.Drawing.Point(183, 40);
            this.txtWEPKey.MaxLength = 13;
            this.txtWEPKey.Name = "txtWEPKey";
            this.txtWEPKey.PasswordChar = '*';
            this.txtWEPKey.Size = new System.Drawing.Size(180, 20);
            this.txtWEPKey.TabIndex = 5;
            this.txtWEPKey.Leave += new System.EventHandler(this.txtWEPKey_Leave);
            // 
            // rdoWPA2
            // 
            this.rdoWPA2.AutoSize = true;
            this.rdoWPA2.Location = new System.Drawing.Point(17, 112);
            this.rdoWPA2.Name = "rdoWPA2";
            this.rdoWPA2.Size = new System.Drawing.Size(80, 17);
            this.rdoWPA2.TabIndex = 4;
            this.rdoWPA2.Text = "WPA2-PSK";
            this.rdoWPA2.UseVisualStyleBackColor = true;
            this.rdoWPA2.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
            // 
            // rdoMixedWPA
            // 
            this.rdoMixedWPA.AutoSize = true;
            this.rdoMixedWPA.Location = new System.Drawing.Point(17, 89);
            this.rdoMixedWPA.Name = "rdoMixedWPA";
            this.rdoMixedWPA.Size = new System.Drawing.Size(154, 17);
            this.rdoMixedWPA.TabIndex = 3;
            this.rdoMixedWPA.Text = "Mixed WPA1 && WPA2-PSK";
            this.rdoMixedWPA.UseVisualStyleBackColor = true;
            this.rdoMixedWPA.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
            // 
            // rdoWPA1
            // 
            this.rdoWPA1.AutoSize = true;
            this.rdoWPA1.Location = new System.Drawing.Point(17, 66);
            this.rdoWPA1.Name = "rdoWPA1";
            this.rdoWPA1.Size = new System.Drawing.Size(56, 17);
            this.rdoWPA1.TabIndex = 2;
            this.rdoWPA1.Text = "WPA1";
            this.rdoWPA1.UseVisualStyleBackColor = true;
            this.rdoWPA1.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
            // 
            // rdoWEP128
            // 
            this.rdoWEP128.AutoSize = true;
            this.rdoWEP128.Location = new System.Drawing.Point(17, 43);
            this.rdoWEP128.Name = "rdoWEP128";
            this.rdoWEP128.Size = new System.Drawing.Size(71, 17);
            this.rdoWEP128.TabIndex = 1;
            this.rdoWEP128.Text = "WEP-128";
            this.rdoWEP128.UseVisualStyleBackColor = true;
            this.rdoWEP128.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
            // 
            // rdoOpen
            // 
            this.rdoOpen.AutoSize = true;
            this.rdoOpen.Checked = true;
            this.rdoOpen.Location = new System.Drawing.Point(17, 20);
            this.rdoOpen.Name = "rdoOpen";
            this.rdoOpen.Size = new System.Drawing.Size(51, 17);
            this.rdoOpen.TabIndex = 0;
            this.rdoOpen.TabStop = true;
            this.rdoOpen.Text = "Open";
            this.rdoOpen.UseVisualStyleBackColor = true;
            this.rdoOpen.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 18;
            this.label2.Text = "(1-13, 0)";
            // 
            // txtChannel
            // 
            this.txtChannel.Location = new System.Drawing.Point(12, 19);
            this.txtChannel.MaxLength = 2;
            this.txtChannel.Name = "txtChannel";
            this.txtChannel.Size = new System.Drawing.Size(43, 20);
            this.txtChannel.TabIndex = 13;
            this.txtChannel.Text = "0";
            this.toolTip1.SetToolTip(this.txtChannel, "1 thru 13, 0 for scan");
            this.txtChannel.Leave += new System.EventHandler(this.txtChannel_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label59);
            this.groupBox1.Controls.Add(this.txtDNSServer);
            this.groupBox1.Controls.Add(this.txtGateway);
            this.groupBox1.Controls.Add(this.label60);
            this.groupBox1.Controls.Add(this.txtSubnetMask);
            this.groupBox1.Controls.Add(this.label58);
            this.groupBox1.Controls.Add(this.txtIPAddress);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.rdoDHCPOn);
            this.groupBox1.Controls.Add(this.rdoDHCPOff);
            this.groupBox1.Location = new System.Drawing.Point(161, 361);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(558, 74);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "DHCP";
            this.toolTip1.SetToolTip(this.groupBox1, "Choose the DHCP method");
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(308, 46);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(74, 13);
            this.label59.TabIndex = 13;
            this.label59.Text = "DNS Server";
            // 
            // txtDNSServer
            // 
            this.txtDNSServer.Enabled = false;
            this.txtDNSServer.Location = new System.Drawing.Point(408, 43);
            this.txtDNSServer.Name = "txtDNSServer";
            this.txtDNSServer.Size = new System.Drawing.Size(134, 20);
            this.txtDNSServer.TabIndex = 11;
            // 
            // txtGateway
            // 
            this.txtGateway.Enabled = false;
            this.txtGateway.Location = new System.Drawing.Point(408, 16);
            this.txtGateway.Name = "txtGateway";
            this.txtGateway.Size = new System.Drawing.Size(134, 20);
            this.txtGateway.TabIndex = 10;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(308, 19);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(101, 13);
            this.label60.TabIndex = 12;
            this.label60.Text = "Default Gateway";
            // 
            // txtSubnetMask
            // 
            this.txtSubnetMask.Enabled = false;
            this.txtSubnetMask.Location = new System.Drawing.Point(190, 42);
            this.txtSubnetMask.Name = "txtSubnetMask";
            this.txtSubnetMask.Size = new System.Drawing.Size(100, 20);
            this.txtSubnetMask.TabIndex = 8;
            this.txtSubnetMask.Text = "255.255.255.255";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(102, 46);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(81, 13);
            this.label58.TabIndex = 9;
            this.label58.Text = "Subnet Mask";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Enabled = false;
            this.txtIPAddress.Location = new System.Drawing.Point(190, 16);
            this.txtIPAddress.MaxLength = 15;
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(100, 20);
            this.txtIPAddress.TabIndex = 2;
            this.txtIPAddress.Leave += new System.EventHandler(this.txtIPAddress_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(103, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "IP Address";
            // 
            // rdoDHCPOn
            // 
            this.rdoDHCPOn.AutoSize = true;
            this.rdoDHCPOn.Checked = true;
            this.rdoDHCPOn.Location = new System.Drawing.Point(14, 42);
            this.rdoDHCPOn.Name = "rdoDHCPOn";
            this.rdoDHCPOn.Size = new System.Drawing.Size(72, 17);
            this.rdoDHCPOn.TabIndex = 1;
            this.rdoDHCPOn.TabStop = true;
            this.rdoDHCPOn.Text = "DHCP On";
            this.rdoDHCPOn.UseVisualStyleBackColor = true;
            this.rdoDHCPOn.CheckedChanged += new System.EventHandler(this.rdoDHCPOff_CheckedChanged);
            // 
            // rdoDHCPOff
            // 
            this.rdoDHCPOff.AutoSize = true;
            this.rdoDHCPOff.Location = new System.Drawing.Point(14, 19);
            this.rdoDHCPOff.Name = "rdoDHCPOff";
            this.rdoDHCPOff.Size = new System.Drawing.Size(72, 17);
            this.rdoDHCPOff.TabIndex = 0;
            this.rdoDHCPOff.Text = "DHCP Off";
            this.rdoDHCPOff.UseVisualStyleBackColor = true;
            this.rdoDHCPOff.CheckedChanged += new System.EventHandler(this.rdoDHCPOff_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoBaycharger);
            this.groupBox3.Controls.Add(this.rdoWorkstation);
            this.groupBox3.Location = new System.Drawing.Point(161, 11);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(107, 69);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Device Type";
            // 
            // rdoBaycharger
            // 
            this.rdoBaycharger.AutoSize = true;
            this.rdoBaycharger.Location = new System.Drawing.Point(13, 41);
            this.rdoBaycharger.Name = "rdoBaycharger";
            this.rdoBaycharger.Size = new System.Drawing.Size(80, 17);
            this.rdoBaycharger.TabIndex = 1;
            this.rdoBaycharger.Text = "BayCharger";
            this.rdoBaycharger.UseVisualStyleBackColor = true;
            this.rdoBaycharger.CheckedChanged += new System.EventHandler(this.rdoWorkstation_CheckedChanged);
            // 
            // rdoWorkstation
            // 
            this.rdoWorkstation.AutoSize = true;
            this.rdoWorkstation.Checked = true;
            this.rdoWorkstation.Location = new System.Drawing.Point(13, 20);
            this.rdoWorkstation.Name = "rdoWorkstation";
            this.rdoWorkstation.Size = new System.Drawing.Size(84, 17);
            this.rdoWorkstation.TabIndex = 0;
            this.rdoWorkstation.TabStop = true;
            this.rdoWorkstation.Text = "WorkStation";
            this.rdoWorkstation.UseVisualStyleBackColor = true;
            this.rdoWorkstation.CheckedChanged += new System.EventHandler(this.rdoWorkstation_CheckedChanged);
            // 
            // btnCommandSend
            // 
            this.btnCommandSend.Location = new System.Drawing.Point(67, 49);
            this.btnCommandSend.Name = "btnCommandSend";
            this.btnCommandSend.Size = new System.Drawing.Size(75, 23);
            this.btnCommandSend.TabIndex = 1;
            this.btnCommandSend.Text = "Send";
            this.toolTip1.SetToolTip(this.btnCommandSend, "Send the Command");
            this.btnCommandSend.UseVisualStyleBackColor = true;
            this.btnCommandSend.Click += new System.EventHandler(this.btnCommandSend_Click);
            // 
            // txtCommandSend
            // 
            this.txtCommandSend.Location = new System.Drawing.Point(67, 23);
            this.txtCommandSend.Name = "txtCommandSend";
            this.txtCommandSend.Size = new System.Drawing.Size(261, 20);
            this.txtCommandSend.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtCommandSend, "Command Text");
            // 
            // txtCommand
            // 
            this.txtCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtCommand.Location = new System.Drawing.Point(12, 603);
            this.txtCommand.Name = "txtCommand";
            this.txtCommand.Size = new System.Drawing.Size(255, 20);
            this.txtCommand.TabIndex = 3;
            this.toolTip1.SetToolTip(this.txtCommand, "Command text");
            // 
            // btnCommands
            // 
            this.btnCommands.Location = new System.Drawing.Point(264, 58);
            this.btnCommands.Name = "btnCommands";
            this.btnCommands.Size = new System.Drawing.Size(75, 23);
            this.btnCommands.TabIndex = 2;
            this.btnCommands.Text = "Commands";
            this.toolTip1.SetToolTip(this.btnCommands, "Command Listing");
            this.btnCommands.UseVisualStyleBackColor = true;
            this.btnCommands.Click += new System.EventHandler(this.btnCommands_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.rdoBayChargerB);
            this.groupBox5.Controls.Add(this.rdoWorkstationB);
            this.groupBox5.Location = new System.Drawing.Point(52, 294);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(169, 87);
            this.groupBox5.TabIndex = 7;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Device Type";
            this.toolTip1.SetToolTip(this.groupBox5, "Choose the device type");
            // 
            // rdoBayChargerB
            // 
            this.rdoBayChargerB.Location = new System.Drawing.Point(13, 49);
            this.rdoBayChargerB.Name = "rdoBayChargerB";
            this.rdoBayChargerB.Size = new System.Drawing.Size(120, 24);
            this.rdoBayChargerB.TabIndex = 1;
            this.rdoBayChargerB.TabStop = true;
            this.rdoBayChargerB.Text = "BayCharger";
            this.rdoBayChargerB.UseVisualStyleBackColor = true;
            // 
            // rdoWorkstationB
            // 
            this.rdoWorkstationB.Checked = true;
            this.rdoWorkstationB.Location = new System.Drawing.Point(13, 23);
            this.rdoWorkstationB.Name = "rdoWorkstationB";
            this.rdoWorkstationB.Size = new System.Drawing.Size(120, 24);
            this.rdoWorkstationB.TabIndex = 0;
            this.rdoWorkstationB.TabStop = true;
            this.rdoWorkstationB.Text = "WorkStation";
            this.rdoWorkstationB.UseVisualStyleBackColor = true;
            this.rdoWorkstationB.CheckedChanged += new System.EventHandler(this.rdoWorkstationB_CheckedChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(200, 319);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 13);
            this.label22.TabIndex = 12;
            this.label22.Text = "(1-13, 0)";
            this.toolTip1.SetToolTip(this.label22, "Between 1 and 13 inclusive");
            // 
            // txtSSIDB
            // 
            this.txtSSIDB.Location = new System.Drawing.Point(166, 186);
            this.txtSSIDB.MaxLength = 32;
            this.txtSSIDB.Name = "txtSSIDB";
            this.txtSSIDB.Size = new System.Drawing.Size(210, 20);
            this.txtSSIDB.TabIndex = 3;
            this.txtSSIDB.Text = "Enter SSID Here";
            this.toolTip1.SetToolTip(this.txtSSIDB, "1 to 32 characters");
            this.txtSSIDB.Leave += new System.EventHandler(this.txtSSIDB_Leave);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.lblSSID4);
            this.groupBox6.Controls.Add(this.groupBox9);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.label31);
            this.groupBox6.Controls.Add(this.txtPassPhraseB);
            this.groupBox6.Controls.Add(this.txtWEPKeyB);
            this.groupBox6.Controls.Add(this.rdoWPA2B);
            this.groupBox6.Controls.Add(this.rdoMixedWPAB);
            this.groupBox6.Controls.Add(this.rdoWPA1B);
            this.groupBox6.Controls.Add(this.rdoWEP128B);
            this.groupBox6.Controls.Add(this.rdoOpenB);
            this.groupBox6.Location = new System.Drawing.Point(166, 78);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(558, 138);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Authentication";
            this.toolTip1.SetToolTip(this.groupBox6, "Choose the Authentication method");
            // 
            // lblSSID4
            // 
            this.lblSSID4.AutoSize = true;
            this.lblSSID4.Location = new System.Drawing.Point(405, 108);
            this.lblSSID4.Name = "lblSSID4";
            this.lblSSID4.Size = new System.Drawing.Size(127, 13);
            this.lblSSID4.TabIndex = 18;
            this.lblSSID4.Text = "(Will convert spaces to $)";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rdo26HEXB);
            this.groupBox9.Controls.Add(this.rdo13ASCIIB);
            this.groupBox9.Location = new System.Drawing.Point(409, 10);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(86, 65);
            this.groupBox9.TabIndex = 11;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "# chars";
            // 
            // rdo26HEXB
            // 
            this.rdo26HEXB.AutoSize = true;
            this.rdo26HEXB.Location = new System.Drawing.Point(10, 36);
            this.rdo26HEXB.Name = "rdo26HEXB";
            this.rdo26HEXB.Size = new System.Drawing.Size(62, 17);
            this.rdo26HEXB.TabIndex = 1;
            this.rdo26HEXB.Text = "26 HEX";
            this.rdo26HEXB.UseVisualStyleBackColor = true;
            this.rdo26HEXB.CheckedChanged += new System.EventHandler(this.rdo13ASCIIB_CheckedChanged);
            // 
            // rdo13ASCIIB
            // 
            this.rdo13ASCIIB.AutoSize = true;
            this.rdo13ASCIIB.Checked = true;
            this.rdo13ASCIIB.Location = new System.Drawing.Point(10, 15);
            this.rdo13ASCIIB.Name = "rdo13ASCIIB";
            this.rdo13ASCIIB.Size = new System.Drawing.Size(70, 17);
            this.rdo13ASCIIB.TabIndex = 0;
            this.rdo13ASCIIB.TabStop = true;
            this.rdo13ASCIIB.Text = "13 ASCII ";
            this.rdo13ASCIIB.UseVisualStyleBackColor = true;
            this.rdo13ASCIIB.CheckedChanged += new System.EventHandler(this.rdo13ASCIIB_CheckedChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(198, 71);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(149, 13);
            this.label30.TabIndex = 8;
            this.label30.Text = "WPA Passphrase (1-64 chars)";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(198, 25);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(120, 13);
            this.label31.TabIndex = 7;
            this.label31.Text = "WEP Key (13/26 chars)";
            // 
            // txtPassPhraseB
            // 
            this.txtPassPhraseB.Enabled = false;
            this.txtPassPhraseB.Location = new System.Drawing.Point(195, 86);
            this.txtPassPhraseB.MaxLength = 64;
            this.txtPassPhraseB.Name = "txtPassPhraseB";
            this.txtPassPhraseB.PasswordChar = '*';
            this.txtPassPhraseB.Size = new System.Drawing.Size(340, 20);
            this.txtPassPhraseB.TabIndex = 6;
            this.toolTip1.SetToolTip(this.txtPassPhraseB, "Between 1 and 64 characters");
            this.txtPassPhraseB.Leave += new System.EventHandler(this.txtPassPhraseB_Leave);
            // 
            // txtWEPKeyB
            // 
            this.txtWEPKeyB.Enabled = false;
            this.txtWEPKeyB.Location = new System.Drawing.Point(195, 40);
            this.txtWEPKeyB.MaxLength = 13;
            this.txtWEPKeyB.Name = "txtWEPKeyB";
            this.txtWEPKeyB.PasswordChar = '*';
            this.txtWEPKeyB.Size = new System.Drawing.Size(196, 20);
            this.txtWEPKeyB.TabIndex = 5;
            this.toolTip1.SetToolTip(this.txtWEPKeyB, "Must be exactly 13 characters");
            this.txtWEPKeyB.Leave += new System.EventHandler(this.txtWEPKeyB_Leave_1);
            // 
            // rdoWPA2B
            // 
            this.rdoWPA2B.AutoSize = true;
            this.rdoWPA2B.Location = new System.Drawing.Point(17, 112);
            this.rdoWPA2B.Name = "rdoWPA2B";
            this.rdoWPA2B.Size = new System.Drawing.Size(80, 17);
            this.rdoWPA2B.TabIndex = 4;
            this.rdoWPA2B.Text = "WPA2-PSK";
            this.rdoWPA2B.UseVisualStyleBackColor = true;
            this.rdoWPA2B.CheckedChanged += new System.EventHandler(this.rdoOpenB_CheckedChanged);
            // 
            // rdoMixedWPAB
            // 
            this.rdoMixedWPAB.AutoSize = true;
            this.rdoMixedWPAB.Location = new System.Drawing.Point(17, 89);
            this.rdoMixedWPAB.Name = "rdoMixedWPAB";
            this.rdoMixedWPAB.Size = new System.Drawing.Size(154, 17);
            this.rdoMixedWPAB.TabIndex = 3;
            this.rdoMixedWPAB.Text = "Mixed WPA1 && WPA2-PSK";
            this.rdoMixedWPAB.UseVisualStyleBackColor = true;
            this.rdoMixedWPAB.CheckedChanged += new System.EventHandler(this.rdoOpenB_CheckedChanged);
            // 
            // rdoWPA1B
            // 
            this.rdoWPA1B.AutoSize = true;
            this.rdoWPA1B.Location = new System.Drawing.Point(17, 66);
            this.rdoWPA1B.Name = "rdoWPA1B";
            this.rdoWPA1B.Size = new System.Drawing.Size(56, 17);
            this.rdoWPA1B.TabIndex = 2;
            this.rdoWPA1B.Text = "WPA1";
            this.rdoWPA1B.UseVisualStyleBackColor = true;
            this.rdoWPA1B.CheckedChanged += new System.EventHandler(this.rdoOpenB_CheckedChanged);
            // 
            // rdoWEP128B
            // 
            this.rdoWEP128B.AutoSize = true;
            this.rdoWEP128B.Location = new System.Drawing.Point(17, 43);
            this.rdoWEP128B.Name = "rdoWEP128B";
            this.rdoWEP128B.Size = new System.Drawing.Size(71, 17);
            this.rdoWEP128B.TabIndex = 1;
            this.rdoWEP128B.Text = "WEP-128";
            this.rdoWEP128B.UseVisualStyleBackColor = true;
            this.rdoWEP128B.CheckedChanged += new System.EventHandler(this.rdoOpenB_CheckedChanged);
            // 
            // rdoOpenB
            // 
            this.rdoOpenB.AutoSize = true;
            this.rdoOpenB.Checked = true;
            this.rdoOpenB.Location = new System.Drawing.Point(17, 20);
            this.rdoOpenB.Name = "rdoOpenB";
            this.rdoOpenB.Size = new System.Drawing.Size(51, 17);
            this.rdoOpenB.TabIndex = 0;
            this.rdoOpenB.TabStop = true;
            this.rdoOpenB.Text = "Open";
            this.rdoOpenB.UseVisualStyleBackColor = true;
            this.rdoOpenB.CheckedChanged += new System.EventHandler(this.rdoOpenB_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtDNSServerB);
            this.groupBox7.Controls.Add(this.txtGatewayB);
            this.groupBox7.Controls.Add(this.txtSubnetMaskB);
            this.groupBox7.Controls.Add(this.label62);
            this.groupBox7.Controls.Add(this.label63);
            this.groupBox7.Controls.Add(this.label61);
            this.groupBox7.Controls.Add(this.txtIPAddressB);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.rdoDHCPOnB);
            this.groupBox7.Controls.Add(this.rdoDHCPOffB);
            this.groupBox7.Location = new System.Drawing.Point(166, 294);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(558, 78);
            this.groupBox7.TabIndex = 3;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "DHCP";
            this.toolTip1.SetToolTip(this.groupBox7, "Choose the DHCP method");
            // 
            // txtDNSServerB
            // 
            this.txtDNSServerB.Enabled = false;
            this.txtDNSServerB.Location = new System.Drawing.Point(419, 44);
            this.txtDNSServerB.Name = "txtDNSServerB";
            this.txtDNSServerB.Size = new System.Drawing.Size(100, 20);
            this.txtDNSServerB.TabIndex = 5;
            // 
            // txtGatewayB
            // 
            this.txtGatewayB.Enabled = false;
            this.txtGatewayB.Location = new System.Drawing.Point(419, 16);
            this.txtGatewayB.Name = "txtGatewayB";
            this.txtGatewayB.Size = new System.Drawing.Size(100, 20);
            this.txtGatewayB.TabIndex = 4;
            // 
            // txtSubnetMaskB
            // 
            this.txtSubnetMaskB.Enabled = false;
            this.txtSubnetMaskB.Location = new System.Drawing.Point(192, 44);
            this.txtSubnetMaskB.Name = "txtSubnetMaskB";
            this.txtSubnetMaskB.Size = new System.Drawing.Size(100, 20);
            this.txtSubnetMaskB.TabIndex = 3;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(353, 48);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(64, 13);
            this.label62.TabIndex = 9;
            this.label62.Text = "DNS Server";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(331, 20);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(86, 13);
            this.label63.TabIndex = 8;
            this.label63.Text = "Default Gateway";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(120, 48);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(70, 13);
            this.label61.TabIndex = 7;
            this.label61.Text = "Subnet Mask";
            // 
            // txtIPAddressB
            // 
            this.txtIPAddressB.Enabled = false;
            this.txtIPAddressB.Location = new System.Drawing.Point(192, 16);
            this.txtIPAddressB.MaxLength = 15;
            this.txtIPAddressB.Name = "txtIPAddressB";
            this.txtIPAddressB.Size = new System.Drawing.Size(100, 20);
            this.txtIPAddressB.TabIndex = 2;
            this.txtIPAddressB.Leave += new System.EventHandler(this.txtIPAddressB_Leave);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(132, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(58, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "IP Address";
            // 
            // rdoDHCPOnB
            // 
            this.rdoDHCPOnB.AutoSize = true;
            this.rdoDHCPOnB.Checked = true;
            this.rdoDHCPOnB.Location = new System.Drawing.Point(14, 42);
            this.rdoDHCPOnB.Name = "rdoDHCPOnB";
            this.rdoDHCPOnB.Size = new System.Drawing.Size(72, 17);
            this.rdoDHCPOnB.TabIndex = 1;
            this.rdoDHCPOnB.TabStop = true;
            this.rdoDHCPOnB.Text = "DHCP On";
            this.rdoDHCPOnB.UseVisualStyleBackColor = true;
            this.rdoDHCPOnB.CheckedChanged += new System.EventHandler(this.rdoDHCPOffB_CheckedChanged);
            // 
            // rdoDHCPOffB
            // 
            this.rdoDHCPOffB.AutoSize = true;
            this.rdoDHCPOffB.Location = new System.Drawing.Point(14, 19);
            this.rdoDHCPOffB.Name = "rdoDHCPOffB";
            this.rdoDHCPOffB.Size = new System.Drawing.Size(72, 17);
            this.rdoDHCPOffB.TabIndex = 0;
            this.rdoDHCPOffB.Text = "DHCP Off";
            this.rdoDHCPOffB.UseVisualStyleBackColor = true;
            this.rdoDHCPOffB.CheckedChanged += new System.EventHandler(this.rdoDHCPOffB_CheckedChanged);
            // 
            // txtResponse
            // 
            this.txtResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtResponse.Location = new System.Drawing.Point(278, 603);
            this.txtResponse.Name = "txtResponse";
            this.txtResponse.Size = new System.Drawing.Size(390, 20);
            this.txtResponse.TabIndex = 4;
            this.toolTip1.SetToolTip(this.txtResponse, "Response text");
            // 
            // txtChannelB
            // 
            this.txtChannelB.Location = new System.Drawing.Point(166, 315);
            this.txtChannelB.MaxLength = 2;
            this.txtChannelB.Name = "txtChannelB";
            this.txtChannelB.Size = new System.Drawing.Size(30, 20);
            this.txtChannelB.TabIndex = 4;
            this.txtChannelB.Text = "0";
            this.toolTip1.SetToolTip(this.txtChannelB, "1 thru 13, 0 for scan");
            this.txtChannelB.Leave += new System.EventHandler(this.txtChannelB_Leave);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.ButtonColor = System.Drawing.Color.Red;
            this.btnClose.ButtonText = "Close";
            this.btnClose.GlowColor = System.Drawing.Color.Pink;
            this.btnClose.Location = new System.Drawing.Point(1430, 595);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(90, 32);
            this.btnClose.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnClose, "Close the App");
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnConnectB
            // 
            this.btnConnectB.BackColor = System.Drawing.Color.Transparent;
            this.btnConnectB.ButtonColor = System.Drawing.Color.Green;
            this.btnConnectB.ButtonText = "Connect";
            this.btnConnectB.GlowColor = System.Drawing.Color.Lime;
            this.btnConnectB.Location = new System.Drawing.Point(617, 11);
            this.btnConnectB.Name = "btnConnectB";
            this.btnConnectB.Size = new System.Drawing.Size(100, 50);
            this.btnConnectB.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnConnectB, "Connect to the device");
            this.btnConnectB.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // btnSetChannelB
            // 
            this.btnSetChannelB.BackColor = System.Drawing.Color.Transparent;
            this.btnSetChannelB.ButtonColor = System.Drawing.Color.Green;
            this.btnSetChannelB.ButtonText = "Set Channel";
            this.btnSetChannelB.GlowColor = System.Drawing.Color.Lime;
            this.btnSetChannelB.Location = new System.Drawing.Point(13, 308);
            this.btnSetChannelB.Name = "btnSetChannelB";
            this.btnSetChannelB.Size = new System.Drawing.Size(116, 33);
            this.btnSetChannelB.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnSetChannelB, "Set the channel number\r\n1 thru 13 or 0 to scan");
            this.btnSetChannelB.Click += new System.EventHandler(this.btnSetChannelB_Click);
            // 
            // btnSetSSIDB
            // 
            this.btnSetSSIDB.BackColor = System.Drawing.Color.Transparent;
            this.btnSetSSIDB.ButtonColor = System.Drawing.Color.Green;
            this.btnSetSSIDB.ButtonText = "Set SSID";
            this.btnSetSSIDB.GlowColor = System.Drawing.Color.Lime;
            this.btnSetSSIDB.Location = new System.Drawing.Point(13, 179);
            this.btnSetSSIDB.Name = "btnSetSSIDB";
            this.btnSetSSIDB.Size = new System.Drawing.Size(116, 33);
            this.btnSetSSIDB.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnSetSSIDB, "Set the SSID");
            this.btnSetSSIDB.Click += new System.EventHandler(this.btnSetSSIDB_Click);
            // 
            // btnCommandModeB
            // 
            this.btnCommandModeB.BackColor = System.Drawing.Color.Transparent;
            this.btnCommandModeB.ButtonColor = System.Drawing.Color.Green;
            this.btnCommandModeB.ButtonText = "Command Mode";
            this.btnCommandModeB.GlowColor = System.Drawing.Color.Lime;
            this.btnCommandModeB.Location = new System.Drawing.Point(13, 55);
            this.btnCommandModeB.Name = "btnCommandModeB";
            this.btnCommandModeB.Size = new System.Drawing.Size(116, 33);
            this.btnCommandModeB.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnCommandModeB, "Establish command mode");
            this.btnCommandModeB.Click += new System.EventHandler(this.btnCommandMode_Click);
            // 
            // btnSetDHCPB
            // 
            this.btnSetDHCPB.BackColor = System.Drawing.Color.Transparent;
            this.btnSetDHCPB.ButtonColor = System.Drawing.Color.Green;
            this.btnSetDHCPB.ButtonText = "Set DHCP";
            this.btnSetDHCPB.GlowColor = System.Drawing.Color.Lime;
            this.btnSetDHCPB.Location = new System.Drawing.Point(13, 289);
            this.btnSetDHCPB.Name = "btnSetDHCPB";
            this.btnSetDHCPB.Size = new System.Drawing.Size(116, 33);
            this.btnSetDHCPB.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnSetDHCPB, "Set the DHCP");
            this.btnSetDHCPB.Click += new System.EventHandler(this.btnSetDHCPB_Click);
            // 
            // btnSetAuthB
            // 
            this.btnSetAuthB.BackColor = System.Drawing.Color.Transparent;
            this.btnSetAuthB.ButtonColor = System.Drawing.Color.Green;
            this.btnSetAuthB.ButtonText = "Set Auth";
            this.btnSetAuthB.GlowColor = System.Drawing.Color.Lime;
            this.btnSetAuthB.Location = new System.Drawing.Point(13, 76);
            this.btnSetAuthB.Name = "btnSetAuthB";
            this.btnSetAuthB.Size = new System.Drawing.Size(116, 33);
            this.btnSetAuthB.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnSetAuthB, "Set the authentication mode");
            this.btnSetAuthB.Click += new System.EventHandler(this.btnSetAuthB_Click);
            // 
            // btnSendDataPacket
            // 
            this.btnSendDataPacket.BackColor = System.Drawing.Color.Transparent;
            this.btnSendDataPacket.BaseColor = System.Drawing.Color.Gold;
            this.btnSendDataPacket.ButtonColor = System.Drawing.Color.Gold;
            this.btnSendDataPacket.ButtonText = "Send Data Packet";
            this.btnSendDataPacket.ForeColor = System.Drawing.Color.Black;
            this.btnSendDataPacket.GlowColor = System.Drawing.Color.Coral;
            this.btnSendDataPacket.Location = new System.Drawing.Point(543, 370);
            this.btnSendDataPacket.Name = "btnSendDataPacket";
            this.btnSendDataPacket.Size = new System.Drawing.Size(160, 33);
            this.btnSendDataPacket.TabIndex = 3;
            this.toolTip1.SetToolTip(this.btnSendDataPacket, "Send a data packet now");
            this.btnSendDataPacket.Click += new System.EventHandler(this.btnSendDataPacket_Click);
            // 
            // btnLEDReset
            // 
            this.btnLEDReset.BackColor = System.Drawing.Color.Transparent;
            this.btnLEDReset.ButtonColor = System.Drawing.Color.LightGray;
            this.btnLEDReset.ButtonText = "Reset LED\'s";
            this.btnLEDReset.GlowColor = System.Drawing.Color.White;
            this.btnLEDReset.Location = new System.Drawing.Point(603, 313);
            this.btnLEDReset.Name = "btnLEDReset";
            this.btnLEDReset.Size = new System.Drawing.Size(100, 33);
            this.btnLEDReset.TabIndex = 2;
            this.toolTip1.SetToolTip(this.btnLEDReset, "Turn all LED\'s off");
            this.btnLEDReset.Click += new System.EventHandler(this.btnResetLED_Click);
            // 
            // btnRebootB
            // 
            this.btnRebootB.BackColor = System.Drawing.Color.Transparent;
            this.btnRebootB.ButtonColor = System.Drawing.Color.Green;
            this.btnRebootB.ButtonText = "Reboot";
            this.btnRebootB.GlowColor = System.Drawing.Color.Lime;
            this.btnRebootB.Location = new System.Drawing.Point(13, 169);
            this.btnRebootB.Name = "btnRebootB";
            this.btnRebootB.Size = new System.Drawing.Size(116, 33);
            this.btnRebootB.TabIndex = 1;
            this.toolTip1.SetToolTip(this.btnRebootB, "Reboot the device");
            this.btnRebootB.Click += new System.EventHandler(this.btnReboot_Click);
            // 
            // btnSaveB
            // 
            this.btnSaveB.BackColor = System.Drawing.Color.Transparent;
            this.btnSaveB.ButtonColor = System.Drawing.Color.Green;
            this.btnSaveB.ButtonText = "Save";
            this.btnSaveB.GlowColor = System.Drawing.Color.Lime;
            this.btnSaveB.Location = new System.Drawing.Point(13, 52);
            this.btnSaveB.Name = "btnSaveB";
            this.btnSaveB.Size = new System.Drawing.Size(116, 33);
            this.btnSaveB.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnSaveB, "Save the configuration");
            this.btnSaveB.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnUpdateRoving
            // 
            this.btnUpdateRoving.BackColor = System.Drawing.Color.Transparent;
            this.btnUpdateRoving.ButtonText = "Update Roving Firmware";
            this.btnUpdateRoving.Location = new System.Drawing.Point(16, 39);
            this.btnUpdateRoving.Name = "btnUpdateRoving";
            this.btnUpdateRoving.Size = new System.Drawing.Size(120, 53);
            this.btnUpdateRoving.TabIndex = 276;
            this.toolTip1.SetToolTip(this.btnUpdateRoving, "Roving Module Firmware Update");
            this.btnUpdateRoving.Click += new System.EventHandler(this.btnUpdateRoving_Click);
            // 
            // btnSendPacketNow
            // 
            this.btnSendPacketNow.BackColor = System.Drawing.Color.Transparent;
            this.btnSendPacketNow.BaseColor = System.Drawing.Color.Gold;
            this.btnSendPacketNow.ButtonColor = System.Drawing.Color.Gold;
            this.btnSendPacketNow.ButtonText = "Send Test Data Packet";
            this.btnSendPacketNow.ForeColor = System.Drawing.Color.Black;
            this.btnSendPacketNow.GlowColor = System.Drawing.Color.Coral;
            this.btnSendPacketNow.Location = new System.Drawing.Point(37, 186);
            this.btnSendPacketNow.Name = "btnSendPacketNow";
            this.btnSendPacketNow.Size = new System.Drawing.Size(86, 50);
            this.btnSendPacketNow.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btnSendPacketNow, "Send Test data packet now");
            this.btnSendPacketNow.Click += new System.EventHandler(this.btnSendPacketNow_Click);
            // 
            // btnSetAllParams
            // 
            this.btnSetAllParams.BackColor = System.Drawing.Color.Transparent;
            this.btnSetAllParams.ButtonColor = System.Drawing.Color.Blue;
            this.btnSetAllParams.ButtonText = "Save All Parameters";
            this.btnSetAllParams.GlowColor = System.Drawing.Color.Aqua;
            this.btnSetAllParams.Location = new System.Drawing.Point(37, 71);
            this.btnSetAllParams.Name = "btnSetAllParams";
            this.btnSetAllParams.Size = new System.Drawing.Size(86, 46);
            this.btnSetAllParams.TabIndex = 9;
            this.toolTip1.SetToolTip(this.btnSetAllParams, "Set all parameters");
            this.btnSetAllParams.Click += new System.EventHandler(this.btnSetAllParams_Click);
            // 
            // btnReboot
            // 
            this.btnReboot.BackColor = System.Drawing.Color.Transparent;
            this.btnReboot.ButtonColor = System.Drawing.Color.Green;
            this.btnReboot.ButtonText = "Reboot";
            this.btnReboot.GlowColor = System.Drawing.Color.Lime;
            this.btnReboot.Location = new System.Drawing.Point(37, 123);
            this.btnReboot.Name = "btnReboot";
            this.btnReboot.Size = new System.Drawing.Size(86, 53);
            this.btnReboot.TabIndex = 8;
            this.toolTip1.SetToolTip(this.btnReboot, "Reboot the device");
            this.btnReboot.Click += new System.EventHandler(this.btnReboot_Click);
            // 
            // btnConnect
            // 
            this.btnConnect.BackColor = System.Drawing.Color.Transparent;
            this.btnConnect.ButtonColor = System.Drawing.Color.Green;
            this.btnConnect.ButtonText = "Connect";
            this.btnConnect.GlowColor = System.Drawing.Color.Lime;
            this.btnConnect.Location = new System.Drawing.Point(37, 11);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(86, 50);
            this.btnConnect.TabIndex = 0;
            this.toolTip1.SetToolTip(this.btnConnect, "Connect to the device");
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // gboxPulseStatus
            // 
            this.gboxPulseStatus.Controls.Add(this.textBox7);
            this.gboxPulseStatus.Controls.Add(this.label67);
            this.gboxPulseStatus.Controls.Add(this.pbxCommunicating);
            this.gboxPulseStatus.Controls.Add(this.textBox4);
            this.gboxPulseStatus.Controls.Add(this.label23);
            this.gboxPulseStatus.Controls.Add(this.textBox5);
            this.gboxPulseStatus.Controls.Add(this.label29);
            this.gboxPulseStatus.Location = new System.Drawing.Point(161, 441);
            this.gboxPulseStatus.Name = "gboxPulseStatus";
            this.gboxPulseStatus.Size = new System.Drawing.Size(558, 109);
            this.gboxPulseStatus.TabIndex = 16;
            this.gboxPulseStatus.TabStop = false;
            this.gboxPulseStatus.Text = "Pulse Status";
            this.toolTip1.SetToolTip(this.gboxPulseStatus, "Choose the DHCP method");
            // 
            // textBox7
            // 
            this.textBox7.Enabled = false;
            this.textBox7.Location = new System.Drawing.Point(188, 71);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(102, 20);
            this.textBox7.TabIndex = 283;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(36, 73);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(129, 13);
            this.label67.TabIndex = 284;
            this.label67.Text = "Internal IP of Device:";
            // 
            // pbxCommunicating
            // 
            this.pbxCommunicating.Location = new System.Drawing.Point(9, 19);
            this.pbxCommunicating.Name = "pbxCommunicating";
            this.pbxCommunicating.Size = new System.Drawing.Size(17, 17);
            this.pbxCommunicating.TabIndex = 279;
            this.pbxCommunicating.TabStop = false;
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(188, 45);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(102, 20);
            this.textBox4.TabIndex = 8;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(36, 48);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(81, 13);
            this.label23.TabIndex = 9;
            this.label23.Text = "Subnet Mask";
            // 
            // textBox5
            // 
            this.textBox5.Enabled = false;
            this.textBox5.Location = new System.Drawing.Point(188, 19);
            this.textBox5.MaxLength = 15;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(102, 20);
            this.textBox5.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(36, 24);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(135, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Device Last Post Date";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBox6);
            this.groupBox12.Controls.Add(this.label66);
            this.groupBox12.Controls.Add(this.label64);
            this.groupBox12.Controls.Add(this.pbxCommunicating2);
            this.groupBox12.Controls.Add(this.textBox1);
            this.groupBox12.Controls.Add(this.label5);
            this.groupBox12.Controls.Add(this.textBox2);
            this.groupBox12.Controls.Add(this.label6);
            this.groupBox12.Location = new System.Drawing.Point(259, 19);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(454, 143);
            this.groupBox12.TabIndex = 17;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Pulse Communication Status";
            this.toolTip1.SetToolTip(this.groupBox12, "Choose the DHCP method");
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Location = new System.Drawing.Point(172, 90);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(137, 20);
            this.textBox6.TabIndex = 281;
            this.textBox6.Text = "255.255.255.255";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(9, 93);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(107, 13);
            this.label66.TabIndex = 282;
            this.label66.Text = "Internal IP of Device:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(34, 27);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(113, 13);
            this.label64.TabIndex = 280;
            this.label64.Text = "Connected To Pulse? ";
            // 
            // pbxCommunicating2
            // 
            this.pbxCommunicating2.Location = new System.Drawing.Point(11, 23);
            this.pbxCommunicating2.Name = "pbxCommunicating2";
            this.pbxCommunicating2.Size = new System.Drawing.Size(17, 17);
            this.pbxCommunicating2.TabIndex = 279;
            this.pbxCommunicating2.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(172, 63);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(137, 20);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "255.255.255.255";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(9, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(159, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Souce IP Address (DMZ/WAN):";
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(280, 21);
            this.textBox2.MaxLength = 15;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(164, 20);
            this.textBox2.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(160, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(114, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Device Last Post Date";
            // 
            // gbPulseAssetSetup
            // 
            this.gbPulseAssetSetup.Controls.Add(this.button1);
            this.gbPulseAssetSetup.Controls.Add(this.textBox11);
            this.gbPulseAssetSetup.Controls.Add(this.label71);
            this.gbPulseAssetSetup.Controls.Add(this.textBox8);
            this.gbPulseAssetSetup.Controls.Add(this.label68);
            this.gbPulseAssetSetup.Controls.Add(this.textBox9);
            this.gbPulseAssetSetup.Controls.Add(this.label69);
            this.gbPulseAssetSetup.Controls.Add(this.textBox10);
            this.gbPulseAssetSetup.Controls.Add(this.label70);
            this.gbPulseAssetSetup.Enabled = false;
            this.gbPulseAssetSetup.Location = new System.Drawing.Point(282, 11);
            this.gbPulseAssetSetup.Name = "gbPulseAssetSetup";
            this.gbPulseAssetSetup.Size = new System.Drawing.Size(437, 106);
            this.gbPulseAssetSetup.TabIndex = 279;
            this.gbPulseAssetSetup.TabStop = false;
            this.gbPulseAssetSetup.Text = "Pulse Asset Setup";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Location = new System.Drawing.Point(287, 19);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(100, 20);
            this.textBox11.TabIndex = 12;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(248, 22);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(39, 13);
            this.label71.TabIndex = 13;
            this.label71.Text = "Floor:";
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Location = new System.Drawing.Point(95, 70);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(147, 20);
            this.textBox8.TabIndex = 10;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(10, 74);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(40, 13);
            this.label68.TabIndex = 11;
            this.label68.Text = "Wing:";
            // 
            // textBox9
            // 
            this.textBox9.Enabled = false;
            this.textBox9.Location = new System.Drawing.Point(95, 45);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(147, 20);
            this.textBox9.TabIndex = 8;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(6, 48);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(80, 13);
            this.label69.TabIndex = 9;
            this.label69.Text = "Department: ";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(142, 19);
            this.textBox10.MaxLength = 15;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(100, 20);
            this.textBox10.TabIndex = 2;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(5, 22);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(127, 13);
            this.label70.TabIndex = 3;
            this.label70.Text = "Asset Tag (Optional):";
            // 
            // ilsLED
            // 
            this.ilsLED.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilsLED.ImageStream")));
            this.ilsLED.TransparentColor = System.Drawing.Color.Transparent;
            this.ilsLED.Images.SetKeyName(0, "LedWhite.ico");
            this.ilsLED.Images.SetKeyName(1, "LedBlue.ico");
            this.ilsLED.Images.SetKeyName(2, "LedRed.ico");
            this.ilsLED.Images.SetKeyName(3, "LedGreen.ico");
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionsToolStripMenuItem,
            this.helpToolStripMenuItem,
            this.webBrowserToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1533, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // functionsToolStripMenuItem
            // 
            this.functionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSubstituteCharacterToolStripMenuItem});
            this.functionsToolStripMenuItem.Name = "functionsToolStripMenuItem";
            this.functionsToolStripMenuItem.Size = new System.Drawing.Size(71, 20);
            this.functionsToolStripMenuItem.Text = "&Functions";
            // 
            // newSubstituteCharacterToolStripMenuItem
            // 
            this.newSubstituteCharacterToolStripMenuItem.Name = "newSubstituteCharacterToolStripMenuItem";
            this.newSubstituteCharacterToolStripMenuItem.Size = new System.Drawing.Size(208, 22);
            this.newSubstituteCharacterToolStripMenuItem.Text = "&New Substitute Character";
            this.newSubstituteCharacterToolStripMenuItem.Click += new System.EventHandler(this.newSubstituteCharacterToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            this.helpToolStripMenuItem.Click += new System.EventHandler(this.helpToolStripMenuItem_Click);
            // 
            // webBrowserToolStripMenuItem
            // 
            this.webBrowserToolStripMenuItem.Name = "webBrowserToolStripMenuItem";
            this.webBrowserToolStripMenuItem.Size = new System.Drawing.Size(88, 20);
            this.webBrowserToolStripMenuItem.Text = "&Web Browser";
            this.webBrowserToolStripMenuItem.Click += new System.EventHandler(this.webBrowserToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.closeToolStripMenuItem.Text = "&Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // ExpertPanel
            // 
            this.ExpertPanel.Controls.Add(this.label74);
            this.ExpertPanel.Controls.Add(this.label73);
            this.ExpertPanel.Controls.Add(this.label72);
            this.ExpertPanel.Controls.Add(this.groupBox16);
            this.ExpertPanel.Controls.Add(this.groupBox15);
            this.ExpertPanel.Controls.Add(this.gbPulseAssetSetup);
            this.ExpertPanel.Controls.Add(this.btnSendPacketNow);
            this.ExpertPanel.Controls.Add(this.gboxPulseStatus);
            this.ExpertPanel.Controls.Add(this.groupBox10);
            this.ExpertPanel.Controls.Add(this.btnSetAllParams);
            this.ExpertPanel.Controls.Add(this.btnReboot);
            this.ExpertPanel.Controls.Add(this.btnConnect);
            this.ExpertPanel.Controls.Add(this.pbxSaveConfiguration);
            this.ExpertPanel.Controls.Add(this.groupBox2);
            this.ExpertPanel.Controls.Add(this.pbxRebootWiFi);
            this.ExpertPanel.Controls.Add(this.groupBox1);
            this.ExpertPanel.Controls.Add(this.groupBox3);
            this.ExpertPanel.Location = new System.Drawing.Point(12, 27);
            this.ExpertPanel.Name = "ExpertPanel";
            this.ExpertPanel.Size = new System.Drawing.Size(750, 558);
            this.ExpertPanel.TabIndex = 0;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.btnWSSNSet);
            this.groupBox10.Controls.Add(this.btnWSSNGet);
            this.groupBox10.Controls.Add(this.txtWSSerialNumber);
            this.groupBox10.Location = new System.Drawing.Point(548, 123);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(171, 89);
            this.groupBox10.TabIndex = 278;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Device Serial Number";
            // 
            // btnWSSNSet
            // 
            this.btnWSSNSet.BackColor = System.Drawing.Color.Transparent;
            this.btnWSSNSet.ButtonText = "Set";
            this.btnWSSNSet.Location = new System.Drawing.Point(87, 42);
            this.btnWSSNSet.Name = "btnWSSNSet";
            this.btnWSSNSet.Size = new System.Drawing.Size(64, 32);
            this.btnWSSNSet.TabIndex = 294;
            this.btnWSSNSet.Click += new System.EventHandler(this.btnWSSNSet_Click);
            // 
            // btnWSSNGet
            // 
            this.btnWSSNGet.BackColor = System.Drawing.Color.Transparent;
            this.btnWSSNGet.ButtonText = "Get";
            this.btnWSSNGet.Location = new System.Drawing.Point(17, 42);
            this.btnWSSNGet.Name = "btnWSSNGet";
            this.btnWSSNGet.Size = new System.Drawing.Size(64, 32);
            this.btnWSSNGet.TabIndex = 294;
            this.btnWSSNGet.Click += new System.EventHandler(this.btnWSSNGet_Click);
            // 
            // txtWSSerialNumber
            // 
            this.txtWSSerialNumber.Location = new System.Drawing.Point(17, 17);
            this.txtWSSerialNumber.Name = "txtWSSerialNumber";
            this.txtWSSerialNumber.Size = new System.Drawing.Size(134, 20);
            this.txtWSSerialNumber.TabIndex = 0;
            // 
            // BeginnerPanel
            // 
            this.BeginnerPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.BeginnerPanel.Controls.Add(this.tabControl1);
            this.BeginnerPanel.Location = new System.Drawing.Point(768, 27);
            this.BeginnerPanel.Name = "BeginnerPanel";
            this.BeginnerPanel.Size = new System.Drawing.Size(750, 559);
            this.BeginnerPanel.TabIndex = 1;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(5, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(742, 551);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.ckbEnableCommandLogging);
            this.tabPage1.Controls.Add(this.label51);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.label45);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(734, 525);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Step 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // ckbEnableCommandLogging
            // 
            this.ckbEnableCommandLogging.AutoSize = true;
            this.ckbEnableCommandLogging.Location = new System.Drawing.Point(378, 309);
            this.ckbEnableCommandLogging.Name = "ckbEnableCommandLogging";
            this.ckbEnableCommandLogging.Size = new System.Drawing.Size(150, 17);
            this.ckbEnableCommandLogging.TabIndex = 7;
            this.ckbEnableCommandLogging.Text = "Enable Command Logging";
            this.ckbEnableCommandLogging.UseVisualStyleBackColor = true;
            this.ckbEnableCommandLogging.Visible = false;
            this.ckbEnableCommandLogging.CheckedChanged += new System.EventHandler(this.ckbEnableCommandLogging_CheckedChanged);
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(533, 194);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(79, 13);
            this.label51.TabIndex = 3;
            this.label51.Text = "(First time only.)";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnCommands);
            this.groupBox4.Controls.Add(this.btnCommandSend);
            this.groupBox4.Controls.Add(this.txtCommandSend);
            this.groupBox4.Controls.Add(this.label49);
            this.groupBox4.Location = new System.Drawing.Point(364, 340);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(345, 87);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Literal Command";
            this.groupBox4.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(11, 27);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(54, 13);
            this.label49.TabIndex = 3;
            this.label49.Text = "Command";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(476, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(248, 114);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(18, 191);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(505, 20);
            this.label45.TabIndex = 2;
            this.label45.Text = "Windows will now go thru the \"New Hardware Found\" setup procedure.";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(18, 412);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(141, 20);
            this.label13.TabIndex = 4;
            this.label13.Text = "Proceed to Step 2.";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(472, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "Connect a USB cable from the PC to the Mobius 3 Power System.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 71);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(425, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "Confirm that the Mobius 3 Power System is up and running.";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnConnectB);
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.label11);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(734, 525);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Step 2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 249);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(288, 20);
            this.label10.TabIndex = 6;
            this.label10.Text = "Is this a WorkStation or a Bay Charger?";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(18, 412);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(141, 20);
            this.label16.TabIndex = 8;
            this.label16.Text = "Proceed to Step 3.";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(18, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(574, 20);
            this.label15.TabIndex = 5;
            this.label15.Text = "and back up. Once it is powered up, press Connect again. Check the USB cable.";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 146);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(663, 20);
            this.label14.TabIndex = 4;
            this.label14.Text = "confirm that the Mobius 3 Power System is powered up. It may be necessary to powe" +
    "r it down";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(18, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(648, 20);
            this.label12.TabIndex = 3;
            this.label12.Text = "If it has a pink background and says:  \"NOT Connected: Connect USB and press Conn" +
    "ect\",";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(18, 64);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(354, 20);
            this.label11.TabIndex = 2;
            this.label11.Text = "The Response box below should say \'Connected\'";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(18, 15);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(213, 20);
            this.label17.TabIndex = 1;
            this.label17.Text = "Press the Connect button -->";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.lblSSID3);
            this.tabPage3.Controls.Add(this.label65);
            this.tabPage3.Controls.Add(this.label41);
            this.tabPage3.Controls.Add(this.label40);
            this.tabPage3.Controls.Add(this.label39);
            this.tabPage3.Controls.Add(this.label20);
            this.tabPage3.Controls.Add(this.label19);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Controls.Add(this.txtChannelB);
            this.tabPage3.Controls.Add(this.pbxSetSSIDB);
            this.tabPage3.Controls.Add(this.label22);
            this.tabPage3.Controls.Add(this.pbxSetChannelB);
            this.tabPage3.Controls.Add(this.textBox3);
            this.tabPage3.Controls.Add(this.txtSSIDB);
            this.tabPage3.Controls.Add(this.pbxCommandModeB);
            this.tabPage3.Controls.Add(this.btnSetChannelB);
            this.tabPage3.Controls.Add(this.btnSetSSIDB);
            this.tabPage3.Controls.Add(this.btnCommandModeB);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(734, 525);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Step 3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // lblSSID3
            // 
            this.lblSSID3.AutoSize = true;
            this.lblSSID3.Location = new System.Drawing.Point(379, 189);
            this.lblSSID3.Name = "lblSSID3";
            this.lblSSID3.Size = new System.Drawing.Size(127, 13);
            this.lblSSID3.TabIndex = 300;
            this.lblSSID3.Text = "(Will convert spaces to $)";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(277, 315);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(318, 20);
            this.label65.TabIndex = 299;
            this.label65.Text = "Use Zero (0) to scan for the proper channel.";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(32, 99);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(193, 13);
            this.label41.TabIndex = 7;
            this.label41.Text = "Response:  <2.20>    (or similar number)";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(32, 350);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(86, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Response:  AOK";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(32, 221);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(86, 13);
            this.label39.TabIndex = 10;
            this.label39.Text = "Response:  AOK";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(18, 412);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(141, 20);
            this.label20.TabIndex = 14;
            this.label20.Text = "Proceed to Step 4.";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(31, 274);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(488, 20);
            this.label19.TabIndex = 11;
            this.label19.Text = "Enter your Access Point\'s Channel Number and press \'Set Channel\':";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(31, 140);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(388, 20);
            this.label18.TabIndex = 8;
            this.label18.Text = "Enter your Access Point\'s SSID and press \'Set SSID\':";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(18, 25);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(559, 20);
            this.label21.TabIndex = 5;
            this.label21.Text = "Press the Command Mode button to put the workstation into Command Mode:";
            // 
            // pbxSetSSIDB
            // 
            this.pbxSetSSIDB.Location = new System.Drawing.Point(134, 187);
            this.pbxSetSSIDB.Name = "pbxSetSSIDB";
            this.pbxSetSSIDB.Size = new System.Drawing.Size(17, 17);
            this.pbxSetSSIDB.TabIndex = 293;
            this.pbxSetSSIDB.TabStop = false;
            // 
            // pbxSetChannelB
            // 
            this.pbxSetChannelB.Location = new System.Drawing.Point(134, 316);
            this.pbxSetChannelB.Name = "pbxSetChannelB";
            this.pbxSetChannelB.Size = new System.Drawing.Size(17, 17);
            this.pbxSetChannelB.TabIndex = 298;
            this.pbxSetChannelB.TabStop = false;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(166, 63);
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(30, 20);
            this.textBox3.TabIndex = 6;
            this.textBox3.Text = "$$$";
            // 
            // pbxCommandModeB
            // 
            this.pbxCommandModeB.Location = new System.Drawing.Point(134, 64);
            this.pbxCommandModeB.Name = "pbxCommandModeB";
            this.pbxCommandModeB.Size = new System.Drawing.Size(17, 17);
            this.pbxCommandModeB.TabIndex = 283;
            this.pbxCommandModeB.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnSetDHCPB);
            this.tabPage4.Controls.Add(this.btnSetAuthB);
            this.tabPage4.Controls.Add(this.label38);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Controls.Add(this.label25);
            this.tabPage4.Controls.Add(this.label24);
            this.tabPage4.Controls.Add(this.label26);
            this.tabPage4.Controls.Add(this.label27);
            this.tabPage4.Controls.Add(this.label28);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.pbxSetAuthB);
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Controls.Add(this.pbxSetDHCPB);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(734, 525);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Step 4";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(32, 335);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(86, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Response:  AOK";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(32, 118);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(86, 13);
            this.label37.TabIndex = 6;
            this.label37.Text = "Response:  AOK";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(18, 412);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(141, 20);
            this.label25.TabIndex = 10;
            this.label25.Text = "Proceed to Step 5.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(31, 262);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(222, 20);
            this.label24.TabIndex = 8;
            this.label24.Text = "Press \'Set DHCP\' when ready.";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(31, 237);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(631, 20);
            this.label26.TabIndex = 7;
            this.label26.Text = "Select your Access Point\'s DHCP method. If you select OFF, enter the static IP Ad" +
    "dress.";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(31, 50);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(211, 20);
            this.label27.TabIndex = 5;
            this.label27.Text = "Press \'Set Auth\' when ready.";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(31, 25);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(611, 20);
            this.label28.TabIndex = 4;
            this.label28.Text = "Select your Access Point\'s Authentication method, and enter any required password" +
    "s.";
            // 
            // pbxSetAuthB
            // 
            this.pbxSetAuthB.Location = new System.Drawing.Point(134, 84);
            this.pbxSetAuthB.Name = "pbxSetAuthB";
            this.pbxSetAuthB.Size = new System.Drawing.Size(17, 17);
            this.pbxSetAuthB.TabIndex = 284;
            this.pbxSetAuthB.TabStop = false;
            // 
            // pbxSetDHCPB
            // 
            this.pbxSetDHCPB.Location = new System.Drawing.Point(134, 297);
            this.pbxSetDHCPB.Name = "pbxSetDHCPB";
            this.pbxSetDHCPB.Size = new System.Drawing.Size(17, 17);
            this.pbxSetDHCPB.TabIndex = 285;
            this.pbxSetDHCPB.TabStop = false;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.groupBox12);
            this.tabPage5.Controls.Add(this.btnSendDataPacket);
            this.tabPage5.Controls.Add(this.btnLEDReset);
            this.tabPage5.Controls.Add(this.btnRebootB);
            this.tabPage5.Controls.Add(this.btnSaveB);
            this.tabPage5.Controls.Add(this.label8);
            this.tabPage5.Controls.Add(this.label47);
            this.tabPage5.Controls.Add(this.label48);
            this.tabPage5.Controls.Add(this.label46);
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.label43);
            this.tabPage5.Controls.Add(this.label42);
            this.tabPage5.Controls.Add(this.label36);
            this.tabPage5.Controls.Add(this.label35);
            this.tabPage5.Controls.Add(this.label34);
            this.tabPage5.Controls.Add(this.label33);
            this.tabPage5.Controls.Add(this.label52);
            this.tabPage5.Controls.Add(this.label53);
            this.tabPage5.Controls.Add(this.label54);
            this.tabPage5.Controls.Add(this.label55);
            this.tabPage5.Controls.Add(this.label56);
            this.tabPage5.Controls.Add(this.pbxSaveConfigurationB);
            this.tabPage5.Controls.Add(this.pbxRebootWiFiB);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(734, 525);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Step 5";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(428, 230);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(248, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Start over at Step 1 after shutting down the system.";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(428, 211);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(260, 13);
            this.label47.TabIndex = 13;
            this.label47.Text = "If this ends in \'SCAN FAILED\', something went wrong.";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(18, 397);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(409, 20);
            this.label48.TabIndex = 18;
            this.label48.Text = "If you like, you can click Web Browser in the menu above.";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(540, 408);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(79, 13);
            this.label46.TabIndex = 19;
            this.label46.Text = "Response:  OK";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(18, 370);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(399, 20);
            this.label44.TabIndex = 17;
            this.label44.Text = "Proceed to the CAST website to see your data packets.";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(18, 341);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(519, 20);
            this.label43.TabIndex = 16;
            this.label43.Text = "Power the Mobius Power System back on. It will now send a data packet.";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(18, 313);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(511, 20);
            this.label42.TabIndex = 15;
            this.label42.Text = "Disconnect the USB cable, and power down the Mobius Power System.";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(157, 257);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(167, 13);
            this.label36.TabIndex = 12;
            this.label36.Text = "ZZZZ is your authentication mode";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(157, 242);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(126, 13);
            this.label35.TabIndex = 11;
            this.label35.Text = "Y is your channel number";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(95, 94);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(83, 13);
            this.label34.TabIndex = 6;
            this.label34.Text = "Storing in config";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(32, 94);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(58, 13);
            this.label33.TabIndex = 5;
            this.label33.Text = "Response:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(116, 228);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(179, 13);
            this.label52.TabIndex = 10;
            this.label52.Text = "Where:  XXXXXXXXXX is your SSID";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(95, 211);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(287, 13);
            this.label53.TabIndex = 9;
            this.label53.Text = "Auto-Assoc XXXXXXXXXX chan=Y mode=ZZZZ SCAN OK";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(32, 211);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(95, 13);
            this.label54.TabIndex = 8;
            this.label54.Text = "Device Response:";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(31, 140);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(187, 20);
            this.label55.TabIndex = 7;
            this.label55.Text = "Press the Reboot button:";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(31, 25);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(170, 20);
            this.label56.TabIndex = 4;
            this.label56.Text = "Press the Save button:";
            // 
            // pbxSaveConfigurationB
            // 
            this.pbxSaveConfigurationB.Location = new System.Drawing.Point(134, 60);
            this.pbxSaveConfigurationB.Name = "pbxSaveConfigurationB";
            this.pbxSaveConfigurationB.Size = new System.Drawing.Size(17, 17);
            this.pbxSaveConfigurationB.TabIndex = 288;
            this.pbxSaveConfigurationB.TabStop = false;
            // 
            // pbxRebootWiFiB
            // 
            this.pbxRebootWiFiB.Location = new System.Drawing.Point(134, 177);
            this.pbxRebootWiFiB.Name = "pbxRebootWiFiB";
            this.pbxRebootWiFiB.Size = new System.Drawing.Size(17, 17);
            this.pbxRebootWiFiB.TabIndex = 289;
            this.pbxRebootWiFiB.TabStop = false;
            // 
            // label50
            // 
            this.label50.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(16, 588);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(79, 13);
            this.label50.TabIndex = 5;
            this.label50.Text = "USB Command";
            // 
            // label57
            // 
            this.label57.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(282, 588);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(92, 13);
            this.label57.TabIndex = 6;
            this.label57.Text = "Device Response";
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.label2);
            this.groupBox14.Controls.Add(this.txtChannel);
            this.groupBox14.Enabled = false;
            this.groupBox14.Location = new System.Drawing.Point(474, 15);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(71, 65);
            this.groupBox14.TabIndex = 11;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Channel";
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.comboBox1);
            this.groupBox15.Controls.Add(this.txtSSID);
            this.groupBox15.Controls.Add(this.lblSSID1);
            this.groupBox15.Location = new System.Drawing.Point(161, 123);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(381, 89);
            this.groupBox15.TabIndex = 295;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "SSID";
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.btnUpdateRoving);
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.Location = new System.Drawing.Point(7, 252);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(148, 104);
            this.groupBox16.TabIndex = 285;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Update Wireless Module Firmware";
            this.toolTip1.SetToolTip(this.groupBox16, "Choose the DHCP method");
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(39, 17);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(324, 21);
            this.comboBox1.TabIndex = 18;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(10, 30);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(17, 17);
            this.label72.TabIndex = 296;
            this.label72.Text = "1";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(10, 88);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(17, 17);
            this.label73.TabIndex = 297;
            this.label73.Text = "2";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(10, 144);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(17, 17);
            this.label74.TabIndex = 298;
            this.label74.Text = "3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(287, 48);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 39);
            this.button1.TabIndex = 14;
            this.button1.Text = "Login To Pulse To Enable";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // HospWiFi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1533, 637);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.txtCommand);
            this.Controls.Add(this.txtResponse);
            this.Controls.Add(this.label57);
            this.Controls.Add(this.label50);
            this.Controls.Add(this.BeginnerPanel);
            this.Controls.Add(this.ExpertPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "HospWiFi";
            this.Text = "Enovate Medical - Pulse Asset Configuration Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HospWiFi_FormClosing);
            this.Load += new System.EventHandler(this.HospWiFi_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.gboxPulseStatus.ResumeLayout(false);
            this.gboxPulseStatus.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommunicating2)).EndInit();
            this.gbPulseAssetSetup.ResumeLayout(false);
            this.gbPulseAssetSetup.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ExpertPanel.ResumeLayout(false);
            this.ExpertPanel.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.BeginnerPanel.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetSSIDB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetChannelB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxCommandModeB)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetAuthB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSetDHCPB)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfigurationB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFiB)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.PictureBox pbxRebootWiFi;
      private System.Windows.Forms.PictureBox pbxSaveConfiguration;
      private System.Windows.Forms.Label lblSSID1;
      private System.Windows.Forms.TextBox txtSSID;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.TextBox txtPassPhrase;
      private System.Windows.Forms.TextBox txtWEPKey;
      private System.Windows.Forms.RadioButton rdoWPA2;
      private System.Windows.Forms.RadioButton rdoMixedWPA;
      private System.Windows.Forms.RadioButton rdoWPA1;
      private System.Windows.Forms.RadioButton rdoWEP128;
      private System.Windows.Forms.RadioButton rdoOpen;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox txtChannel;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.TextBox txtIPAddress;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.RadioButton rdoDHCPOn;
      private System.Windows.Forms.RadioButton rdoDHCPOff;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.RadioButton rdoBaycharger;
      private System.Windows.Forms.RadioButton rdoWorkstation;
      private System.Windows.Forms.ToolTip toolTip1;
      private System.Windows.Forms.ImageList ilsLED;
      private System.Windows.Forms.TextBox txtCommand;
      private System.Windows.Forms.MenuStrip menuStrip1;
      private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
      private System.Windows.Forms.Panel ExpertPanel;
      private System.Windows.Forms.Panel BeginnerPanel;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.Button btnCommands;
      private System.Windows.Forms.Button btnCommandSend;
      private System.Windows.Forms.TextBox txtCommandSend;
      private System.Windows.Forms.Label label49;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.Label label45;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label40;
      private System.Windows.Forms.Label label39;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.TextBox txtChannelB;
      private System.Windows.Forms.PictureBox pbxSetSSIDB;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.PictureBox pbxSetChannelB;
      private System.Windows.Forms.TextBox textBox3;
      private System.Windows.Forms.TextBox txtSSIDB;
      private System.Windows.Forms.PictureBox pbxCommandModeB;
      private System.Windows.Forms.TabPage tabPage4;
      private System.Windows.Forms.Label label38;
      private System.Windows.Forms.Label label37;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.TextBox txtPassPhraseB;
      private System.Windows.Forms.TextBox txtWEPKeyB;
      private System.Windows.Forms.RadioButton rdoWPA2B;
      private System.Windows.Forms.RadioButton rdoMixedWPAB;
      private System.Windows.Forms.RadioButton rdoWPA1B;
      private System.Windows.Forms.RadioButton rdoWEP128B;
      private System.Windows.Forms.RadioButton rdoOpenB;
      private System.Windows.Forms.PictureBox pbxSetAuthB;
      private System.Windows.Forms.GroupBox groupBox7;
      private System.Windows.Forms.TextBox txtIPAddressB;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.RadioButton rdoDHCPOnB;
      private System.Windows.Forms.RadioButton rdoDHCPOffB;
      private System.Windows.Forms.PictureBox pbxSetDHCPB;
      private System.Windows.Forms.TabPage tabPage5;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.Label label46;
      private System.Windows.Forms.Label label44;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.Label label36;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.Label label34;
      private System.Windows.Forms.Label label33;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.Label label53;
      private System.Windows.Forms.Label label54;
      private System.Windows.Forms.Label label55;
      private System.Windows.Forms.Label label56;
      private System.Windows.Forms.PictureBox pbxSaveConfigurationB;
      private System.Windows.Forms.PictureBox pbxRebootWiFiB;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.RadioButton rdoBayChargerB;
      private System.Windows.Forms.RadioButton rdoWorkstationB;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.Label label57;
      private System.Windows.Forms.TextBox txtResponse;
      private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem webBrowserToolStripMenuItem;
      private VistaButton.VistaButton btnConnect;
      private VistaButton.VistaButton btnSendPacketNow;
      private VistaButton.VistaButton btnSetAllParams;
      private VistaButton.VistaButton btnReboot;
      private VistaButton.VistaButton btnClose;
      private VistaButton.VistaButton btnConnectB;
      private VistaButton.VistaButton btnCommandModeB;
      private VistaButton.VistaButton btnSetChannelB;
      private VistaButton.VistaButton btnSetSSIDB;
      private VistaButton.VistaButton btnSetDHCPB;
      private VistaButton.VistaButton btnSetAuthB;
      private VistaButton.VistaButton btnSendDataPacket;
      private VistaButton.VistaButton btnLEDReset;
      private VistaButton.VistaButton btnRebootB;
      private VistaButton.VistaButton btnSaveB;
      private System.Windows.Forms.CheckBox ckbEnableCommandLogging;
      private System.Windows.Forms.GroupBox groupBox8;
      private System.Windows.Forms.RadioButton rdo26HEX;
      private System.Windows.Forms.RadioButton rdo13ASCII;
      private System.Windows.Forms.GroupBox groupBox9;
      private System.Windows.Forms.RadioButton rdo26HEXB;
      private System.Windows.Forms.RadioButton rdo13ASCIIB;
      private System.Windows.Forms.Label label59;
      private System.Windows.Forms.TextBox txtDNSServer;
      private System.Windows.Forms.TextBox txtGateway;
      private System.Windows.Forms.Label label60;
      private System.Windows.Forms.TextBox txtSubnetMask;
      private System.Windows.Forms.Label label58;
      private System.Windows.Forms.TextBox txtDNSServerB;
      private System.Windows.Forms.TextBox txtGatewayB;
      private System.Windows.Forms.TextBox txtSubnetMaskB;
      private System.Windows.Forms.Label label62;
      private System.Windows.Forms.Label label63;
      private System.Windows.Forms.Label label61;
      private VistaButton.VistaButton btnUpdateRoving;
      private System.Windows.Forms.Label label65;
      private System.Windows.Forms.Label lblSSID2;
      private System.Windows.Forms.Label lblSSID4;
      private System.Windows.Forms.ToolStripMenuItem functionsToolStripMenuItem;
      private System.Windows.Forms.ToolStripMenuItem newSubstituteCharacterToolStripMenuItem;
      private System.Windows.Forms.Label lblSSID3;
      private System.Windows.Forms.GroupBox groupBox10;
      private VistaButton.VistaButton btnWSSNSet;
      private VistaButton.VistaButton btnWSSNGet;
      private System.Windows.Forms.TextBox txtWSSerialNumber;
      private System.Windows.Forms.RadioButton rdoHex;
      private System.Windows.Forms.RadioButton rdoASCII;
      private System.Windows.Forms.Panel panel1;
      private System.Windows.Forms.GroupBox gboxPulseStatus;
      private System.Windows.Forms.PictureBox pbxCommunicating;
      private System.Windows.Forms.TextBox textBox4;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.TextBox textBox5;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.GroupBox gbPulseAssetSetup;
      private System.Windows.Forms.TextBox textBox11;
      private System.Windows.Forms.Label label71;
      private System.Windows.Forms.TextBox textBox8;
      private System.Windows.Forms.Label label68;
      private System.Windows.Forms.TextBox textBox9;
      private System.Windows.Forms.Label label69;
      private System.Windows.Forms.TextBox textBox10;
      private System.Windows.Forms.Label label70;
      private System.Windows.Forms.TextBox textBox7;
      private System.Windows.Forms.Label label67;
      private System.Windows.Forms.GroupBox groupBox12;
      private System.Windows.Forms.TextBox textBox6;
      private System.Windows.Forms.Label label66;
      private System.Windows.Forms.Label label64;
      private System.Windows.Forms.PictureBox pbxCommunicating2;
      private System.Windows.Forms.TextBox textBox1;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.TextBox textBox2;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.GroupBox groupBox14;
      private System.Windows.Forms.GroupBox groupBox16;
      private System.Windows.Forms.GroupBox groupBox15;
      private System.Windows.Forms.Label label74;
      private System.Windows.Forms.Label label73;
      private System.Windows.Forms.Label label72;
      private System.Windows.Forms.ComboBox comboBox1;
      private System.Windows.Forms.Button button1;
   }
}

