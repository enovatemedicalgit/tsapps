﻿﻿﻿﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace HospWifi
{
   public partial class Help : Form
   {
      public Help()
      {
         InitializeComponent();

         txtUSBHelp.Text = "USB Connection Help\r\n\r\n" +
            "There may be some situations where the device is not recognized by Windows. Following these suggestions will minimize that possibility.\r\n\r\n" +
            "The first time you connect your PC to the Mobius Power System, Windows will go thru the 'New Hardware Found' process. No external drivers are required, so just select the default settings in the dialogs.\r\n\r\n" +
            "Do not connect the USB cable to the Mobius Power System until it is up and running.\r\n\r\n" +
            "It may be necessary to power the system down and back up again in order for the PC to recognize the device.\r\n\r\n" +
            "Unplug the USB cable before powering the Mobius Power System down.\r\n\r\n" +
            "If all else fails, power your PC down and back up again - don't just do a restart.\r\n";

         txtCommandModeHelp.Text = "Command Mode Help\r\n\r\n" +
            "The wifi device must be put into command mode in order to accept external commands.\r\n" +
            "Pressing the Command Mode button will attempt to initiate this condition.\r\n\r\n" +
            "The device will respond with its firmware version number in the form of: <2.xx> where xx is the minor version number.\r\n\r\n" +
            "Failure to receive this version number usually means that the device is in an undetermined state and the Mobius Power System should be powered down and back up.\r\n";

         txtSSIDHelp.Text = "SSID Help\r\n\r\n" +
            "SSID is short for Service Set IDentifier.\r\n\r\n" +
            "The SSID is limited to 32 characters total length.\r\n\r\n" +
            "Spaces in the name will be replaced with dollar signs ($) for sending to the device. See NOTE below.\r\n\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n"
            + "NOTE: The SSID can contain any printable character except a space. "
            + "If the SSID contains the SPACE (‘ ‘) character, it can be entered using substitution via the dollar sign ($) character. For example, if the SSID of the Access Point is “yellow brick road”, "
            + "you would enter “yellow$brick$road”.  If the SSID contains a dollar sign ($), "
            + "you will need to set a new substitution character using the [Functions - New Substitute Character] menu option. Choose a character that is not in your SSID or pass phrase.\r\n"
         ;

         txtChannelHelp.Text = "Channel Number Help\r\n\r\n" +
            "Valid channel numbers are between 1 and 13 inclusive.\r\n\r\n" +
            "If you have Access Points with the same SSID but with different channel numbers, you can set the channel number to 0 (zero) and the Mobius Power System will scan all channels looking for the SSID. " +
            "It will start with channel 1 and increment thru 13 until it finds a matching SSID.\r\n\r\n" +
            "The device will respond with 'AOK'.\r\n";

         txtAuthenticationHelp.Text = "Authentication Help\r\n\r\n" +
            "There are several types of authentication to choose from.\r\n\r\n" +
            "Open - this choice means that the network is unprotected and anyone within range can connect to it. No password is required.\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n" +
            "WEP-128 - Wired Equivalent Privacy. Provides encrypted data streams between devices. Requires a password of exactly 13 or 26 characters. (Open mode only, not Shared mode.)\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n" +
            "WPA1 - WiFi Protected Access. This is the PSK (Pre-Shared Key) TKIP (Temporal Key Integrity Protocol) mode only. Requires a pass phrase of from 1 to 64 characters which can be a combination of letters and numbers. (See NOTE below.)\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n" +
            "WPA2-PSK - Version 2 of WiFi Protected Access. (AES only (Advanced Encryption Standard)). Requires a pass phrase of from 1 to 64 characters which can be a combination of letters and numbers. (See NOTE below.)\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n" +
            "Mixed WPA1 & WPA2-PSK - A combination of the above two modes. Not supported by all access points. Requires a pass phrase of from 1 to 64 characters which can be a combination of letters and numbers. (See NOTE below.)\r\n\r\n" +
            "The device will respond with 'AOK'.\r\n\r\n"
            + "NOTE: The pass phrase can contain any printable character except a space. "
            + "If the pass phrase contains the SPACE (‘ ‘) character, it can be entered using substitution via the dollar sign ($) character. For example, if the pass phrase of the Access Point is “yellow brick road”, "
            + "you would enter “yellow$brick$road”.  If the pass phrase contains a dollar sign ($), "
            + "you will need to set a new substitution character using the [Functions - New Substitute Character] menu option. Choose a character that is not in your SSID or pass phrase.\r\n"
            ;

         txtDHCPHelp.Text = "DHCP Help\r\n\r\n" +
            "Dynamic Host Configuration Protocol: a protocol for assigning dynamic IP addresses to devices on a network.\r\n\r\n" +
            "If DHCP is on (the default) then the access point will hand out IP Addresses automatically. If DHCP is off, then you must supply the IP Address, Subnet Mask, default Gateway, and preferred DNS Server that the device will use to attach to the network. All four of these have a format of XXX.XXX.XXX.XXX\r\n\r\n" +
            "The device will respond with 'AOK'.\r\n";

         txtSaveRebootHelp.Text = "Save & Reboot Help\r\n\r\n" +
            "Save\r\n\r\n" +
            "Pressing Save will store your changes into the device. These changes will not take effect until the device reboots.\r\n\r\n" +
            "The device will respond with 'Storing in config'.\r\n\r\n" +
            "Reboot\r\n\r\n" +
            "Pressing Reboot will cause the wifi device to reboot, thereby enabling your changes.\r\n\r\n" +
            "Immediately following a reboot, the wifi device may be unresponsive for a couple of minutes. If you made a mistake during setup, power the system down and back up again to restore normal operation, then reprogram the device.\r\n\r\n" +
            "The device will respond with a string of characters describing the setup.\r\n" +
            "SCAN OK at the end of the string means that the device successfully connected to the access point.\r\n\r\n" +
            "If you see 'SCAN FAILED' at the end of the string, then something in the setup failed, or the device is outside the range of the access point.\r\n";

         txtGeneral.Text = "HospWifi General Help\r\n\r\n" +
            "There are two modes of operation: Beginner and Expert.\r\n" +
            "Beginner: All operations are explained on numbered tabs, allowing the novice user to program the wifi device and learn the steps as he progresses.\r\n\r\n" +
            "Expert: All operations are performed on a single form. There is also a 'Set All Parameters' button to allow quick setup once all parameters have been entered.\r\n\r\n" +
            "When first launched, HospWiFi will attempt to connect to and discover the type of device (Workstation or BayCharger) to which it is connected (not all devices have this capability). Be sure and confirm the device type before programming, by selecting the appropriate radio button in the lower left section of the form.\r\n\r\n" +
            "It is always a good idea to power the Mobius Power System down after programming, and let it boot up from scratch.\r\n\r\n" +
            "The 'Reset LED's' button will turn all LED's off, ready for the next device.\r\n\r\n" +
            "When pressing the individual command buttons, pause before pressing the next one to give the system time to respond to the command.\r\n\r\n" +
            "SSID's and Passwords/Phrases which contain a space will have the spaces replaced with a substitute character to eliminate parsing problems. " +
            "The default substitution character is the dollar sign ($). You can change the default to some other character if your entry contains a dollar sign. " +
            "Study your entry and locate an unused character and use it for the new substitute character by selecting Functions-New Substitute Character. " +
            "Make note of this new character for future reference since the device will no longer be set to its default.\r\n\r\n" +
            "\r\n\r\n" +
            "\r\n\r\n";
      }




      private void btnClose_Click( object sender, EventArgs e )
      {
         this.Close();
      }


   }
}
