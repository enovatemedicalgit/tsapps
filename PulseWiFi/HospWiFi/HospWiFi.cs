﻿/**********************************************************************************
 Program Name: HospWiFi.cs
 Author: G.T. Sanford III
 Copyright (c) 2010 by Enovate Medical
 Created: 10/30/2010 at 14:13
-----------------------------------------------------------------------------------
 Description: Utility that hospital IT can use to commission their Mobius devices.


              To access expert mode, expand form to 1546 from 790, then change 
              BeginnerPanel X position from 12 to 768.




-----------------------------------------------------------------------------------

                     M O D I F I C A T I O N    H I S T O R Y 

  Date    By   Vers   Modification
--------  ---  ----  --------------------------------------------------------------
10/30/10  GTS  1.00  Initial version
11/23/10  GTS  1.01  Added expert mode.
11/29/10  GTS  1.02  Changed all buttons to GlassButtons.
12/08/10  GTS  1.03  Changed all buttons to VistaButtons. Added debug logging.
02/15/11  GTS  1.05  Made the URL part of the config file.
03/15/11  GTS  1.07  Added 13/26 chars for WEPkey.
04/27/11  GTS  1.08  Changed the anchor property of the WebBrowser.
05/19/11  GTS  1.09  Added Subnet Mask, Gateway, and DNS Server.
06/13/11  GTS  1.10  Added support for a 64 byte PassPhrase.
06/23/11  GTS  1.11  Fixed problem in SetAllParameters.
06/29/11  GTS  1.12  Fixed problem in txtPassPhrase_Leave().
07/27/11  GTS  1.13  Fixed problem in btnAuth_Click where the passcode wasn't
                     handled properly.
10/11/11  GTS  1.14  Added update Roving firmware. Fixed where SSID wasn't changing
                     spaces to $.
03/09/12  GTS  1.15  Added New Substitution Character dialog.
04/14/12  GTS  1.16  Added Charmap launch to New Substitution Character dialog.
08/21/12  GTS  1.17  Added ability to get and set the Serial Number.
10/01/12  GTS  1.18  Updated Help screens. Added SubstituteChar. 
01/25/13  GTS  1.19  Added ASCII and HEX versions of the PassPhrase. Added checks for
                     spaces and current substitution character in both WEP Key 
                     and PassPhrase.
04/15/13  GTS  1.20  Added threading. Added password hiding. Updated Roving FTP address.
06/04/13  GTS  1.21  Hide all passwords. Encrypt for storage in cfg file.
07/24/13  GTS  1.22  Logo change.
08/05/13  GTS  1.23  Added double-click Mobius 3 logo to show hidden passwords.















------------------------------ ALL RIGHTS RESERVED --------------------------------
**********************************************************************************/

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using at_usb_api;
using VistaButton;
 

namespace HospWiFi
{
   public partial class HospWiFi : Form
   {
      string TVersion = "1.23";
      string VID = "03EB";
      string PID = "204F";
      string sReply = "";
      bool   DataReceived = false;
      bool   USBConnected = false;
      at_usb_api.at_usb_api atusb = new at_usb_api.at_usb_api();
      string WEP_KEY = "";
      string PASS_KEY = "";
      int    ExperienceLevel = 2;
      bool   WDebug = false;
      string CastUrl = "https://Pulse.myenovate.com";
      string SubstituteChar = "$";
      string ProgName = "HospWiFi";

      bool _continue = false;
      string  USBData = "";
      delegate void SetTextCallback( string text );
      System.Threading.Thread readThread;






      #region HospWiFi Constructor
      /****************************************************************************
           HospWiFi                                               10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      public HospWiFi()
      {
         InitializeComponent();

         this.Width = 790;
         BeginnerPanel.Left = 12;

         this.BeginnerPanel.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( ( ( System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom )
            | System.Windows.Forms.AnchorStyles.Left )
            | System.Windows.Forms.AnchorStyles.Right ) ) );

       //  pbxCommandMode.Image = ilsLED.Images[ 0 ];
       //  pbxSetSSID.Image = ilsLED.Images[ 0 ];
         //pbxSetChannel.Image = ilsLED.Images[ 0 ];
      //   pbxSetAuth.Image = ilsLED.Images[ 0 ];
       //  pbxSetDHCP.Image = ilsLED.Images[ 0 ];
         pbxSaveConfiguration.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFi.Image = ilsLED.Images[ 0 ];
          pbxCommunicating.Image = ilsLED.Images[ 0 ];
         pbxCommandModeB.Image = ilsLED.Images[ 0 ];
         pbxSetSSIDB.Image = ilsLED.Images[ 0 ];
         pbxSetChannelB.Image = ilsLED.Images[ 0 ];
         pbxSetAuthB.Image = ilsLED.Images[ 0 ];
         pbxSetDHCPB.Image = ilsLED.Images[ 0 ];
         pbxSaveConfigurationB.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFiB.Image = ilsLED.Images[ 0 ];

         //atusb.InitHID();
         bool OK = atusb.ConnectDevice( VID, PID );
         txtCommand.Text = "Connect";

         USBConnected = false;
         _continue = false;

         if ( OK )
         {
            USBConnected = true;
            txtResponse.Text = "Connected";
            txtResponse.BackColor = Color.White;

            if ( !_continue )
            {
               readThread = new Thread( ReadUSB );
               _continue = true;
               readThread.Start();
            }
         }
         else
         {
            USBConnected = false;
            txtResponse.BackColor = Color.Pink;
            txtResponse.Text = "NOT Connected: Connect USB and press Connect";
            btnConnect.Enabled = true;
         }

         this.Text = "Enovate Medical - Hospital WiFi Setup Utility  " + TVersion;
      }
      /*--- end of HospWiFi() ---------------------------------------------------*/
      #endregion




      #region HospWiFi_Load 
      /****************************************************************************
           HospWiFi_Load                                          10/31/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void HospWiFi_Load( object sender, EventArgs e )
      {
         if ( !atusb.FoundDLL )
            this.Close();

         LoadParams();

         if ( SubstituteChar.Length < 1 )
            SubstituteChar = "$";

         if ( USBConnected )
         {
            txtCommand.Text = "6X";
            atusb.WriteBytes( "6X" );
            WaitForData();
            if ( sReply.Contains( "W" ) )
            {
               rdoWorkstation.Checked = true;
               rdoWorkstationB.Checked = true;
            }
            if ( sReply.Contains( "B" ) )
            {
               rdoBaycharger.Checked = true;
               rdoBayChargerB.Checked = true;
            }
         }

         //lblSSID1.Text = "(Will convert spaces to " + SubstituteChar + ")";
         //lblSSID2.Text = "(Will convert spaces to " + SubstituteChar + ")";

      }
      /*--- end of HospWiFi_Load() ----------------------------------------------*/
      #endregion


//static string GetStringForSSID(Wlan.Dot11Ssid ssid)
//{
//    return Encoding.ASCII.GetString( ssid.SSID, 0, (int) ssid.SSIDLength );
//}

//public void ScanNetworks()
//{
//    WlanClient client = new WlanClient();
//foreach ( WlanClient.WlanInterface wlanIface in client.Interfaces )
//{
//    // Lists all available networks
//    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList( 0 );
//    foreach ( Wlan.WlanAvailableNetwork network in networks )
//    {                     
//        Console.WriteLine( "Found network with SSID {0}.", GetStringForSSID(network.dot11Ssid));
//    }
//}
//}

      #region HospWiFi_FormClosing 
      /****************************************************************************
           HospWiFi_FormClosing                                   10/31/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void HospWiFi_FormClosing( object sender, FormClosingEventArgs e )
      {
         _continue = false;
         SaveParams();
      }
      /*--- end of HospWiFi_FormClosing() ---------------------------------------*/
      #endregion




      #region btnConnect_Click 
      /****************************************************************************
           btnConnect_Click                                       10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnConnect_Click( object sender, EventArgs e )
      {
         bool OK = atusb.ConnectDevice( VID, PID );
         txtCommand.Text = "Connect";

         if ( OK )
         {
            txtResponse.Text = "Connected";
            txtResponse.BackColor = Color.White;
            USBConnected = true;

            if ( !_continue )
            {
               readThread = new Thread( ReadUSB );
               _continue = true;
               readThread.Start();
            }
         }
         else
         {
            txtResponse.BackColor = Color.Pink;
            txtResponse.Text = "NOT Connected: Connect USB and press Connect";
            USBConnected = false;
         }

         SubstituteChar = "$";
         lblSSID1.Text = "(Will convert spaces to " + SubstituteChar + ")";
         lblSSID2.Text = "(Will convert spaces to " + SubstituteChar + ")";
         lblSSID3.Text = "(Will convert spaces to " + SubstituteChar + ")";
         lblSSID4.Text = "(Will convert spaces to " + SubstituteChar + ")";
      }
      /*--- end of btnConnect_Click() -------------------------------------------*/
      #endregion




      #region btnClose_Click 
      /****************************************************************************
           btnClose_Click                                         10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnClose_Click( object sender, EventArgs e )
      {
         _continue = false;

         if ( USBConnected )
            atusb.CloseDevice();

         this.Close();
      }
      /*--- end of btnClose_Click() ---------------------------------------------*/
      #endregion




      #region WriteCommand
      /****************************************************************************
          WriteCommand                                             11/08/06
        Description: Writes debug info to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void WriteCommand( string command )
      {
         if ( !WDebug )
            return;

         try
         {
            StreamWriter textOut =
               new StreamWriter( new FileStream( "Debug.log",
                                                 FileMode.Append,
                                                 FileAccess.Write ) );

            textOut.WriteLine( "HospWiFi: " + DateTime.Now.ToString() );
            textOut.WriteLine( " Command: " + command );
            textOut.WriteLine( "------------------------------------------------------------------------------" );
            textOut.Close();
         }
         catch ( IOException iox )
         {
            string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
         }

         return;
      }
      /*--- end of WriteCommand() -----------------------------------------------*/
      #endregion




      #region WriteResponse
      /****************************************************************************
          WriteResponse                                            11/08/06
        Description: Writes debug info to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void WriteResponse( string response )
      {
         if ( !WDebug )
            return;

         try
         {
            StreamWriter textOut =
               new StreamWriter( new FileStream( "Debug.log",
                                                 FileMode.Append,
                                                 FileAccess.Write ) );

            textOut.WriteLine( "HospWiFi: " + DateTime.Now.ToString() );
            textOut.WriteLine( "Response: " + response );
            textOut.WriteLine( "------------------------------------------------------------------------------" );
            textOut.Close();
         }
         catch ( IOException iox )
         {
            string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
         }

         return;
      }
      /*--- end of WriteResponse() ----------------------------------------------*/
      #endregion




      #region GetParam
      /****************************************************************************
           GetParam                                               08/02/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private string GetParam( StreamReader textIn, string key )
      {
         string sline = "";
         string value = "";

         while ( true )
         {
            try
            {
               sline = textIn.ReadLine();
            }
            catch
            {
               break;
            }

            if ( sline == null )
               break;

            if ( sline.Contains( key ) )
            {
               int ipos = sline.IndexOf( '=' );
               if ( ipos > 0 )
               {
                  try
                  {
                     value = sline.Substring( ipos + 1 );
                  }
                  catch
                  {
                     value = "";
                  }
                  break;
               }
            }
         }

         return value;
      }
      /*--- end of GetParam() ---------------------------------------------------*/
      #endregion




      #region LoadParams
      /****************************************************************************
           LoadParams                                             08/02/10
        Description: Read the saved params and load the textboxes. 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      public void LoadParams()
      {
         StreamReader textIn;
         string sTemp = "";

         try
         {
            textIn = new StreamReader( new FileStream( "PulseWiFi.cfg", FileMode.Open, FileAccess.Read ) );
         }
         catch
         {
            return;
         }

         sTemp = GetParam( textIn, "DHCP" );
         if ( sTemp == "0" )
         {
            rdoDHCPOff.Checked = true;
            rdoDHCPOffB.Checked = true;
         }
         if ( sTemp == "1" )
         {
            rdoDHCPOn.Checked = true;
            rdoDHCPOnB.Checked = true;
         }

         txtIPAddress.Text = GetParam( textIn, "IPADDRESS" );
         txtIPAddressB.Text = txtIPAddress.Text;

         sTemp = GetParam( textIn, "SSID" );
         //sTemp = sTemp.Replace( '$', ' ' );
         txtSSID.Text = sTemp;
         txtSSIDB.Text = sTemp;

         //txtUserID.Text = GetParam( textIn, "USERID" );
         //string unc = GetParam( textIn, "PASSWORD" );
         //txtPassword.Text = ADeCode( unc );
         //sTemp = GetParam( textIn, "REMEMBER" );
         //if ( sTemp == "Yes" )
         //   ckbRememberMe.Checked = true;
         //else
         //   ckbRememberMe.Checked = false;

         sTemp = GetParam( textIn, "AUTHEN" );
         if ( sTemp == "0" )
         {
            rdoOpen.Checked = true;
            rdoOpenB.Checked = true;
            txtWEPKey.Enabled = false;
            txtWEPKeyB.Enabled = false;
            txtPassPhrase.Enabled = false;
            txtPassPhraseB.Enabled = false;
         }
         if ( sTemp == "1" )
         {
            rdoWEP128.Checked = true;
            rdoWEP128B.Checked = true;
            txtWEPKey.Enabled = true;
            txtWEPKeyB.Enabled = true;
            txtPassPhrase.Enabled = false;
            txtPassPhraseB.Enabled = false;
         }
         if ( sTemp == "2" )
         {
            rdoWPA1.Checked = true;
            rdoWPA1B.Checked = true;
            txtWEPKey.Enabled = false;
            txtWEPKeyB.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhraseB.Enabled = true;
         }
         if ( sTemp == "3" )
         {
            rdoMixedWPA.Checked = true;
            rdoMixedWPAB.Checked = true;
            txtWEPKey.Enabled = false;
            txtWEPKeyB.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhraseB.Enabled = true;
         }
         if ( sTemp == "4" )
         {
            rdoWPA2.Checked = true;
            rdoWPA2B.Checked = true;
            txtWEPKey.Enabled = false;
            txtWEPKeyB.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhraseB.Enabled = true;
         }

         sTemp = GetParam( textIn, "WEPKEY" );
         string uenc = "";
         try
         {
            uenc = HospWifi.Crypto.DecryptStringAES( sTemp, ProgName );
         }
         catch
         {
            uenc = "";
         }
         txtWEPKey.Text = uenc;
         txtWEPKeyB.Text = uenc;
         if ( txtWEPKey.Text.Length == 26 )
            rdo26HEX.Checked = true;

         sTemp = GetParam( textIn, "PASSPHRASE" );
         try
         {
            uenc = HospWifi.Crypto.DecryptStringAES( sTemp, ProgName );
         }
         catch
         {
            uenc = "";
         }
         //uenc = uenc.Replace( '$', ' ' );
         txtPassPhrase.Text = uenc;
         txtPassPhraseB.Text = uenc;

         bool isHex = true;
         // is the WEP key currently in ASCII or Hex? 
         foreach ( char c in txtPassPhrase.Text.Trim() )
         {
            if ( c < 0x30 )
            {
               isHex = false;
               break;
            }
            if ( c > 0x39 && c < 0x41 )
            {
               isHex = false;
               break;
            }
            if ( c > 0x46 && c < 0x61 )
            {
               isHex = false;
               break;
            }
            if ( c > 0x66 )
            {
               isHex = false;
               break;
            }
         }

         if ( isHex )
            rdoHex.Checked = true;

         txtChannel.Text = GetParam( textIn, "CHANNEL" );
         txtChannelB.Text = txtChannel.Text;

         sTemp = GetParam( textIn, "LEVEL" );
         ExperienceLevel = int.Parse( sTemp );

         // Set menu check 
         //if ( ExperienceLevel == 1 )    // beginner 
         //{
         //   beginnerToolStripMenuItem1.Checked = true;
         //   expertToolStripMenuItem1.Checked = false;
         //   ExpertPanel.Visible = false;
         //   BeginnerPanel.Visible = true;
         //   //webBrowserToolStripMenuItem.Visible = false;
         //}
         if ( ExperienceLevel == 2 )    // expert 
         {
           // beginnerToolStripMenuItem1.Checked = false;
            //expertToolStripMenuItem1.Checked = true;
            BeginnerPanel.Visible = false;
            ExpertPanel.Visible = true;
            //webBrowserToolStripMenuItem.Visible = true;
         }

         sTemp = GetParam( textIn, "DEVICE" );
         if ( sTemp == "BayCharger" )
         {
            rdoBaycharger.Checked = true;
            rdoBayChargerB.Checked = true;
            rdoWorkstation.Checked = false;
            rdoWorkstationB.Checked = false;
         }
         else
         {
            rdoBaycharger.Checked = false;
            rdoBayChargerB.Checked = false;
            rdoWorkstation.Checked = true;
            rdoWorkstationB.Checked = true;
         }

         CastUrl = GetParam( textIn, "URL" );

         if ( CastUrl.Contains( "stinger" ) )
            CastUrl = "https://pulse.myenovate.com";

         txtGateway.Text = GetParam( textIn, "GATEWAY" );
         txtGatewayB.Text = txtGateway.Text;
               CastUrl = "https://pulse.myenovate.com";
         txtSubnetMask.Text = GetParam( textIn, "SUBNETMASK" );
         txtSubnetMaskB.Text = txtSubnetMask.Text;

         txtDNSServer.Text = GetParam( textIn, "DNSSERVER" );
         txtDNSServerB.Text = txtDNSServer.Text;

         //SubstituteChar = GetParam( textIn, "SUBCHAR" );

         //webBrowser1.Url = new Uri( CastUrl );

         textIn.Close();

         return;
      }
      /*--- end of LoadParams() -------------------------------------------------*/
      #endregion




      #region SaveParams
      /****************************************************************************
           SaveParams                                             08/02/10
        Description: Save the data in the textboxes.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      public void SaveParams()
      {
         StreamWriter textOut;

         try
         {
            textOut = new StreamWriter( new FileStream( "PulseWiFi.cfg", FileMode.Create, FileAccess.Write ) );
         }
         catch
         {
            return;
         }

         if ( rdoDHCPOff.Checked || rdoDHCPOffB.Checked )
            textOut.WriteLine( "DHCP=0" );
         if ( rdoDHCPOn.Checked || rdoDHCPOnB.Checked )
            textOut.WriteLine( "DHCP=1" );

         if ( ExperienceLevel == 1 )
         {
            textOut.WriteLine( "IPADDRESS=" + txtIPAddressB.Text );
            textOut.WriteLine( "SSID=" + txtSSIDB.Text );
         }
         else
         {
            textOut.WriteLine( "IPADDRESS=" + txtIPAddress.Text );
            textOut.WriteLine( "SSID=" + txtSSID.Text );
         }

         //if ( ckbRememberMe.Checked )
         //{
         //   textOut.WriteLine( "USERID=" + txtUserID.Text );
         //   string enc = AEnCode( txtPassword.Text );
         //   textOut.WriteLine( "PASSWORD=" + enc );
         //   textOut.WriteLine( "REMEMBER=Yes" );
         //}
         //else
         //{
         //   textOut.WriteLine( "USERID=" );
         //   textOut.WriteLine( "PASSWORD=" );
         //   textOut.WriteLine( "REMEMBER=No" );
         //}

         if ( rdoOpen.Checked || rdoOpenB.Checked )
            textOut.WriteLine( "AUTHEN=0" );
         if ( rdoWEP128.Checked || rdoWEP128B.Checked )
            textOut.WriteLine( "AUTHEN=1" );
         if ( rdoWPA1.Checked || rdoWPA1B.Checked )
            textOut.WriteLine( "AUTHEN=2" );
         if ( rdoMixedWPA.Checked || rdoMixedWPAB.Checked )
            textOut.WriteLine( "AUTHEN=3" );
         if ( rdoWPA2.Checked || rdoWPA2B.Checked )
            textOut.WriteLine( "AUTHEN=4" );

         string enc = "";

         if ( ExperienceLevel == 1 )
         {
            try
            {
               enc = HospWifi.Crypto.EncryptStringAES( txtWEPKeyB.Text, ProgName );
            }
            catch
            {
               enc = "";
            }
            textOut.WriteLine( "WEPKEY=" + enc );
            try
            {
               enc = HospWifi.Crypto.EncryptStringAES( txtPassPhraseB.Text, ProgName );
            }
            catch
            {
               enc = "";
            }
            textOut.WriteLine( "PASSPHRASE=" + enc );
            textOut.WriteLine( "CHANNEL=" + txtChannelB.Text );
         }
         else
         {
            try
            {
               enc = HospWifi.Crypto.EncryptStringAES( txtWEPKey.Text, ProgName );
            }
            catch
            {
               enc = "";
            }
            textOut.WriteLine( "WEPKEY=" + enc );
            try
            {
               enc = HospWifi.Crypto.EncryptStringAES( txtPassPhrase.Text, ProgName );
            }
            catch
            {
               enc = "";
            }
            textOut.WriteLine( "PASSPHRASE=" + enc );
            textOut.WriteLine( "CHANNEL=" + txtChannel.Text );
         }

         textOut.WriteLine( "LEVEL=" + ExperienceLevel.ToString() );

         if ( rdoBaycharger.Checked || rdoBayChargerB.Checked )
            textOut.WriteLine( "DEVICE=BayCharger" );
         if ( rdoWorkstation.Checked || rdoWorkstationB.Checked )
            textOut.WriteLine( "DEVICE=WorkStation" );

         textOut.WriteLine( "URL=" + CastUrl );

         if ( ExperienceLevel == 1 )
         {
            textOut.WriteLine( "GATEWAY=" + txtGatewayB.Text );
            textOut.WriteLine( "SUBNETMASK=" + txtSubnetMaskB.Text );
            textOut.WriteLine( "DNSSERVER=" + txtDNSServerB.Text );
         }
         else
         {
            textOut.WriteLine( "GATEWAY=" + txtGateway.Text );
            textOut.WriteLine( "SUBNETMASK=" + txtSubnetMask.Text );
            textOut.WriteLine( "DNSSERVER=" + txtDNSServer.Text );
         }

         //textOut.WriteLine( "SUBCHAR=" + SubstituteChar );

         textOut.Close();
         return;
      }
      /*--- end of SaveParams() -------------------------------------------------*/
      #endregion


      

      #region rdoWorkstation_CheckedChanged 
      /****************************************************************************
           rdoWorkstation_CheckedChanged                          10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoWorkstation_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoWorkstation.Checked )
            rdoWorkstationB.Checked = true;
         else
            rdoWorkstationB.Checked = false;
      }
      /*--- end of rdoWorkstation_CheckedChanged() ------------------------------*/
      #endregion




      #region btnResetLED_Click 
      /****************************************************************************
           btnResetLED_Click                                      10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnResetLED_Click( object sender, EventArgs e )
      {
       //  pbxCommandMode.Image = ilsLED.Images[ 0 ];
         //pbxSetSSID.Image = ilsLED.Images[ 0 ];
       //  pbxSetChannel.Image = ilsLED.Images[ 0 ];
        // pbxSetAuth.Image = ilsLED.Images[ 0 ];
       //  pbxSetDHCP.Image = ilsLED.Images[ 0 ];
         pbxSaveConfiguration.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFi.Image = ilsLED.Images[ 0 ];

         pbxCommandModeB.Image = ilsLED.Images[ 0 ];
         pbxSetSSIDB.Image = ilsLED.Images[ 0 ];
         pbxSetChannelB.Image = ilsLED.Images[ 0 ];
         pbxSetAuthB.Image = ilsLED.Images[ 0 ];
         pbxSetDHCPB.Image = ilsLED.Images[ 0 ];
         pbxSaveConfigurationB.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFiB.Image = ilsLED.Images[ 0 ];
      }
      /*--- end of btnResetLED_Click() ------------------------------------------*/
      #endregion




      #region rdoOpen_CheckedChanged 
      /****************************************************************************
           rdoOpen_CheckedChanged                                 10/30/10
        Description: Open Authentication on the Experienced screen,
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoOpen_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoOpen.Checked )
         {
            rdoOpenB.Checked = true;
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = false;
            groupBox8.Enabled = false;
            rdoASCII.Enabled = false;
            rdoHex.Enabled = false;
         }
         if ( rdoWEP128.Checked )
         {
            rdoWEP128B.Checked = true;
            txtWEPKey.Enabled = true;
            txtPassPhrase.Enabled = false;
            groupBox8.Enabled = true;
            rdoASCII.Enabled = false;
            rdoHex.Enabled = false;
            txtWEPKey.Focus();
         }
         if ( rdoWPA1.Checked )
         {
            rdoWPA1B.Checked = true;
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            groupBox8.Enabled = false;
            rdoASCII.Enabled = true;
            rdoHex.Enabled = true;
            txtPassPhrase.Focus();
         }
         if ( rdoMixedWPA.Checked )
         {
            rdoMixedWPAB.Checked = true;
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            groupBox8.Enabled = false;
            rdoASCII.Enabled = true;
            rdoHex.Enabled = true;
            txtPassPhrase.Focus();
         }
         if ( rdoWPA2.Checked )
         {
            rdoWPA2B.Checked = true;
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            groupBox8.Enabled = false;
            rdoASCII.Enabled = true;
            rdoHex.Enabled = true;
            txtPassPhrase.Focus();
         }
      }
      /*--- end of rdoOpen_CheckedChanged() -------------------------------------*/
      #endregion




      #region rdoDHCPOff_CheckedChanged 
      /****************************************************************************
           rdoDHCPOff_CheckedChanged                              10/30/10
        Description: DHCP on the Experienced screen.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoDHCPOff_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoDHCPOff.Checked )
         {
            rdoDHCPOffB.Checked = true;
            txtIPAddress.Enabled = true;
            txtGateway.Enabled = true;
            txtSubnetMask.Enabled = true;
            txtDNSServer.Enabled = true;
            txtIPAddress.Focus();
         }
         else
         {
            rdoDHCPOn.Checked = true;
            rdoDHCPOnB.Checked = true;
            txtIPAddress.Enabled = false;
            txtGateway.Enabled = false;
            txtSubnetMask.Enabled = false;
            txtDNSServer.Enabled = false;
         }
      }
      /*--- end of rdoDHCPOff_CheckedChanged() ----------------------------------*/
      #endregion




      #region ReadUSB 
      /****************************************************************************
           ReadUSB                                                05/10/12
        Description: USB thread
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: Runs continuously while program is running
                 1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
       012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
      "FF 0B 20 55 02 04 7E 02 01 7E D1 03 56 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
      ****************************************************************************/

      public void ReadUSB()
      {
         while ( _continue )
         {
            Thread.Sleep( 1 );
            try
            {
               USBData = atusb.ReadBytes();
               if ( USBData.Length > 2 )
               {
                  //PacketsReceived++;
                  SetText( USBData );
               }
            }
            catch
            {
            }
         }
      }
      /*--- end of ReadUSB() ----------------------------------------------------*/
      #endregion




      #region SetText
      /****************************************************************************
           SetText                                                01/27/09
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: This handles displaying the contents of SerialIn as a string
                     if needed.
      ****************************************************************************/

      private void SetText( string text )
      {
         // InvokeRequired compares the thread ID of the
         // calling thread to the thread ID of the creating thread.
         // If these threads are different, it returns true.
         if ( this.InvokeRequired )
         {
            SetTextCallback stc = new SetTextCallback( SetText );
            this.Invoke( stc, new object[] { text } );
         }
         else
         {
            ProcessCommands( text );
            DataReceived = true;
         }
      }
      /*--- end of SetText() ----------------------------------------------------*/
      #endregion




      #region DeviceSendCommand
      /****************************************************************************
                DeviceSendCommand                                       08/02/10
        Description: Wrapper function for atusb.WriteBytes().
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void DeviceSendCommand( string Command )
      {
         string nCommand = Command;

         if ( rdoBaycharger.Checked || rdoBayChargerB.Checked )
         {
            // insert a 0     6E$$$ -> 6E0$$$ 
            nCommand = Command.Substring( 0, 2 ) + "0" + Command.Substring( 2 );
         }

         txtResponse.Text = "";
         txtResponse.Refresh();
         
         txtCommand.Text = nCommand;
         txtCommand.Refresh();

         DataReceived = false;

         if ( USBConnected )
         {
            WriteCommand( nCommand );
            atusb.WriteBytes( nCommand );
         }

         return;
      }
      /*--- end of DeviceSendCommand() ------------------------------------------*/
      #endregion




      #region DeviceSendCommand
      /****************************************************************************
                DeviceSendCommand                                       08/02/10
        Description: Wrapper function for atusb.WriteBytes().
         Parameters: string Command: command to execute
                     string resp: expected response
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private bool DeviceSendCommand( string Command, string resp )
      {
         string nCommand = Command;
         int tries = 0;
         bool result = false;

         if ( rdoBaycharger.Checked || rdoBayChargerB.Checked )
         {
            // insert a 0     6E$$$ -> 6E0$$$ 
            nCommand = Command.Substring( 0, 2 ) + "0" + Command.Substring( 2 );
         }

         txtResponse.Text = "";
         txtResponse.BackColor = Color.White;
         txtResponse.Refresh();
         
         txtCommand.Text = nCommand;
         txtCommand.Refresh();

         DataReceived = false;

         if ( USBConnected )
         {
            WriteCommand( nCommand );
            atusb.WriteBytes( nCommand );
         }

         tries = 0;
         while ( !DataReceived && USBConnected )
         {
            Application.DoEvents();
            System.Threading.Thread.Sleep( 10 );
            tries++;
            if ( tries > 300 )   // 3 seconds 
            {
               result = false;
               break;
            }
         }

         System.Threading.Thread.Sleep( 100 );

         if ( sReply.Contains( resp ) )
         {
            //ProcessCommands();
            //sReply = "";
            result = true;
         }

         return result;
      }
      /*--- end of DeviceSendCommand() ------------------------------------------*/
      #endregion




      #region WaitForData
      /****************************************************************************
           WaitForData                                            09/08/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private bool WaitForData()
      {
         int tries = 0;

         while ( !DataReceived && USBConnected )
         {
            Application.DoEvents();
            System.Threading.Thread.Sleep( 10 );
            tries++;
            if ( tries > 300 )   // 3 seconds 
               return false;
         }

         return true;
      }
      /*--- end of WaitForData() -----------------------------------------------*/
      #endregion




      #region WaitForThisData
      /****************************************************************************
           WaitForThisData                                        09/08/10
        Description: Waits until the expected response is received from the device.
                     Tries 4 times to perform the command.
         Parameters: string resp: the expected response 
       Return Value: true if found, false otherwise 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private bool WaitForThisData( string resp )
      {
         int tries = 0;
         bool result = false;

         while ( !DataReceived && USBConnected )
         {
            Application.DoEvents();
            System.Threading.Thread.Sleep( 10 );
            tries++;
            if ( tries > 300 )   // 3 seconds 
            {
               result = false;
               break;
            }
         }

         System.Threading.Thread.Sleep( 100 );

         if ( sReply.Contains( resp ) )
            result = true;

         return result;
      }
      /*--- end of WaitForThisData() -------------------------------------------*/
      #endregion




      #region ProcessCommands
      /****************************************************************************
           ProcessCommands                                        02/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void ProcessCommands( string Reply )
      {
         sReply = Reply;
             
         char Device = sReply[ 0 ];
         char Command = sReply[ 1 ];

         WriteResponse( sReply );
         
         if ( rdoBaycharger.Checked || rdoBayChargerB.Checked )
            txtResponse.Text = sReply.Substring( 3 );    // bay number is 3rd 
         else
            txtResponse.Text = sReply.Substring( 2 );

         if ( Command == 'O' )
         {
            if ( rdoBaycharger.Checked || rdoBayChargerB.Checked )
               txtWSSerialNumber.Text = sReply.Substring( 3 );
            else
               txtWSSerialNumber.Text = sReply.Substring( 2 );
         }

         txtResponse.Refresh();

         return;
      }
      /*--- end of ProcessCommands() --------------------------------------------*/
      #endregion




      #region btnCommandMode_Click 
      /****************************************************************************
           btnCommandMode_Click                                   10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnCommandMode_Click( object sender, EventArgs e )
      {
         string command = "6E$$$";
         int tries = 0;
         bool result = false;
 

         while ( !result )
         {
            result = DeviceSendCommand( command, "<" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set Command Mode";
               txtResponse.BackColor = Color.Pink;
               btnCommandModeB.Enabled = true;
               return;
            }
         }
         btnCommandModeB.Enabled = true;
      }
      /*--- end of btnCommandMode_Click() ---------------------------------------*/
      #endregion

      
      
      
      #region btnSetSSID_Click 
      /****************************************************************************
           btnSetSSID_Click                                       10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetSSID_Click( object sender, EventArgs e )
      {
         string command = "6Eset w s " + txtSSID.Text;
         int tries = 0;
         bool result = false;

         //btnSetSSID.Enabled = false;

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set SSID";
               txtResponse.BackColor = Color.Pink;
               //pbxSetSSID.Image = ilsLED.Images[ 2 ];
               pbxSetSSIDB.Image = ilsLED.Images[ 2 ];
           //    btnSetSSID.Enabled = true;
               return;
            }
         }
         
         //DeviceSendCommand( command );
       //  pbxSetSSID.Image = ilsLED.Images[ 3 ];
         pbxSetSSIDB.Image = ilsLED.Images[ 3 ];
        // btnSetSSID.Enabled = true;
      }
      /*--- end of btnSetSSID_Click() -------------------------------------------*/
      #endregion
      
      
      

      #region btnSetChannel_Click 
      /****************************************************************************
           btnSetChannel_Click                                    10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetChannel_Click( object sender, EventArgs e )
      {
         string command = "6Eset w c " + txtChannel.Text;
         int tries = 0;
         bool result = false;

         //btnSetChannel.Enabled = false;

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set Channel Number";
               txtResponse.BackColor = Color.Pink;
        //       pbxSetChannel.Image = ilsLED.Images[ 2 ];
               pbxSetChannelB.Image = ilsLED.Images[ 2 ];
               //btnSetChannel.Enabled = true;
               return;
            }
         }

       //  pbxSetChannel.Image = ilsLED.Images[ 3 ];
         pbxSetChannelB.Image = ilsLED.Images[ 3 ];
         //btnSetChannel.Enabled = true;
      }
      /*--- end of btnSetChannel_Click() ----------------------------------------*/
      #endregion
      
      
      

      #region btnSetAuth_Click 
      /****************************************************************************
           btnSetAuth_Click                                       10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
    SSID : spbat254
password : e34b5ca8df172690293ba5c186d7e40fb1f948325ac7ed606bdfc59174e3082a
           agbpV+/,"{!EWLr'?+HGcF0u~,o<iL>\L'o,=.A\5Ab&u=.r5RO#()_;>qL&Y}B
       *   1...|....1....|....2....|....3....|....4....|....5....|....6....|
      ****************************************************************************/

      private void btnSetAuth_Click( object sender, EventArgs e )
      {
         string command = "";
         int tries = 0;
         bool result = false;


         if ( rdoOpen.Checked )
            command = "6Eset w a 0";   // default 
         if ( rdoWEP128.Checked )
         {
            if ( rdo13ASCII.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKey.Focus();
                  //btnSetAuth.Enabled = true;
                  return;
               }
            }
            if ( rdo26HEX.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 characters long!", "Length Error" );
                  //txtWEPKey.Focus();
            
                  return;
               }
            }
           
            command = "6Eset w a 1";   // WEP-128 
         }
         if ( rdoWPA1.Checked )
            command = "6Eset w a 2";   // 
         if ( rdoMixedWPA.Checked )
            command = "6Eset w a 3";   // 
         if ( rdoWPA2.Checked )
            command = "6Eset w a 4";   // 

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set Authentication";
               txtResponse.BackColor = Color.Pink;
               pbxSetAuthB.Image = ilsLED.Images[ 2 ];
               return;
            }
         }

         if ( rdoWEP128.Checked )
         {
            if ( WEP_KEY.Length != 26 )
               txtWEPKey_Leave( sender, e );

            command = "6Eset w k " + WEP_KEY;

            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set WEP Key";
                  txtResponse.BackColor = Color.Pink;
                  pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                  return;
               }
            }
         }

         if ( rdoWPA1.Checked || rdoWPA2.Checked || rdoMixedWPA.Checked )
         {
            //"set w p e34b5ca8df172690293ba5c186d7e40fb1f948325ac7ed606bdfc59174e3082a"

            txtPassPhrase_Leave( sender, e );

            if ( rdoHex.Checked )
            {
               // convert back to ASCII 
               bool isHex = true;
               // is the PASS key currently in ASCII or Hex? 
               foreach ( char c in txtPassPhrase.Text.Trim() )
               {
                  if ( c < 0x30 )
                  {
                     isHex = false;
                     break;
                  }
                  if ( c > 0x39 && c < 0x41 )
                  {
                     isHex = false;
                     break;
                  }
                  if ( c > 0x46 && c < 0x61 )
                  {
                     isHex = false;
                     break;
                  }
                  if ( c > 0x66 )
                  {
                     isHex = false;
                     break;
                  }
               }

               if ( !isHex )
               {
                  // need to convert it to hex 
                  PASS_KEY = "";
                  foreach ( char c in txtPassPhrase.Text.Trim() )
                     PASS_KEY += String.Format( "{0:X}", (int)c );
                  txtPassPhrase.Text = PASS_KEY;
               }
            }

            command = "6Eset w p " + txtPassPhrase.Text;

            if ( command.Length <= 64 )
            {
               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;

                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                     //btnSetAuth.Enabled = true;
                     return;
                  }
               }
            }
            else      // must split in two 
            {
               int ilen = txtPassPhrase.Text.Length;

               string pc1 = txtPassPhrase.Text.Substring( 0, 32 );   // 1st half
               string pc2 = txtPassPhrase.Text.Substring( 32 ) + Environment.NewLine;      // 2nd half 

               command = "6{" + pc1;

               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;
                  //   pbxSetAuth.Image = ilsLED.Images[ 2 ];
                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                     //btnSetAuth.Enabled = true;
                     return;
                  }
               }

               command = "6}" + pc2;

               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;
          //           pbxSetAuth.Image = ilsLED.Images[ 2 ];
                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
           //          btnSetAuth.Enabled = true;
                     return;
                  }
               }
            }
         }

     //    pbxSetAuth.Image = ilsLED.Images[ 3 ];
         pbxSetAuthB.Image = ilsLED.Images[ 3 ];
         //btnSetAuth.Enabled = true;
      }
      /*--- end of btnSetAuth_Click() -------------------------------------------*/
      #endregion




      #region btnSetDHCP_Click 
      /****************************************************************************
           btnSetDHCP_Click                                       10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetDHCP_Click( object sender, EventArgs e )
      {
         string command = "";
         int tries = 0;
         bool result = false;

       //  btnSetDHCP.Enabled = false;

         command = "6Eset i d 1";   // default 

         if ( rdoDHCPOff.Checked )
            command = "6Eset i d 0";
         if ( rdoDHCPOn.Checked )
            command = "6Eset i d 1";   // default 

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set DHCP";
               txtResponse.BackColor = Color.Pink;
               //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
               pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
            //   btnSetDHCP.Enabled = true;
               return;
            }
         }

         if ( rdoDHCPOff.Checked )
         {
            command = "6Eset i a " + txtIPAddress.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set IP Address";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
               //   btnSetDHCP.Enabled = true;
                  return;
               }
            }

            command = "6Eset i g " + txtGateway.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set Gateway";
                  txtResponse.BackColor = Color.Pink;
               //   pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
                  //btnSetDHCP.Enabled = true;
                  return;
               }
            }

            command = "6Eset i n " + txtSubnetMask.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set Subnet Mask";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
            //      btnSetDHCP.Enabled = true;
                  return;
               }
            }

            command = "6Eset d a " + txtDNSServer.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set DNS Server";
                  txtResponse.BackColor = Color.Pink;
    //              pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
  //                btnSetDHCP.Enabled = true;
                  return;
               }
            }
         }

  //       pbxSetDHCP.Image = ilsLED.Images[ 3 ];
         pbxSetDHCPB.Image = ilsLED.Images[ 3 ];
         //btnSetDHCP.Enabled = true;
      }
      /*--- end of btnSetDHCP_Click() -------------------------------------------*/
      #endregion




      #region btnSave_Click 
      /****************************************************************************
           btnSave_Click                                          10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSave_Click( object sender, EventArgs e )
      {

         int tries = 0;
         bool result = false;

         while ( !result )
         {
            result = DeviceSendCommand( "6Esave", "Storing" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot save config";
               txtResponse.BackColor = Color.Pink;
               pbxSaveConfiguration.Image = ilsLED.Images[ 2 ];
               pbxSaveConfigurationB.Image = ilsLED.Images[ 2 ];
               return;
            }
         }
         pbxSaveConfiguration.Image = ilsLED.Images[ 3 ];
         pbxSaveConfigurationB.Image = ilsLED.Images[ 3 ];

      }
      /*--- end of btnSave_Click() ----------------------------------------------*/
      #endregion




      #region btnReboot_Click 
      /****************************************************************************
           btnReboot_Click                                        10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnReboot_Click( object sender, EventArgs e )
      {
         int tries = 0;
         bool result = false;

         btnReboot.Enabled = false;

         while ( !result )
         {
            result = DeviceSendCommand( "6Ereboot", "Auto" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot reboot device";
               txtResponse.BackColor = Color.Pink;
               pbxRebootWiFi.Image = ilsLED.Images[ 2 ];
               pbxRebootWiFiB.Image = ilsLED.Images[ 2 ];
               btnReboot.Enabled = true;
               return;
            }
         }
         pbxRebootWiFi.Image = ilsLED.Images[ 3 ];
         pbxRebootWiFiB.Image = ilsLED.Images[ 3 ];
         btnReboot.Enabled = true;
      }
      /*--- end of btnReboot_Click() --------------------------------------------*/
      #endregion




      #region txtWEPKey_Leave 
      /****************************************************************************
           txtWEPKey_Leave                                        10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtWEPKey_Leave( object sender, EventArgs e )
      {
         string stmp;
         bool FndSpace = false;
         bool FndSub = false;
         bool Conflict = false;
         string sSub = String.Format( "{0:X}", Convert.ToInt32( SubstituteChar[ 0 ] ) );

         // check for presence of spaces and substitution character 

         if ( rdo13ASCII.Checked )
         {
            if ( txtWEPKey.Text.Contains( " " ) && txtWEPKey.Text.Contains( SubstituteChar ) )
            {
               Conflict = true;
            }
         }
         else
         {
            for ( int i = 0; i < txtWEPKey.Text.Length; i += 2 )
            {
               stmp = txtWEPKey.Text.Substring( i, 2 );
               if ( stmp == "20" )
                  FndSpace = true;
               if ( stmp == sSub )
                  FndSub = true;
            }
            if ( FndSpace && FndSub )
               Conflict = true;
         }

         if ( Conflict )
         {
            MessageBox.Show( "This WEP Key contains both spaces and\n" +
                             "your current Substitution Character\n\n" +
                             "Please select a new Substitution Character\n\n" +
                             "The New Substitution Character selector will\n" +
                             "open when you close this window.", "WEP Key Error" );
            newSubstituteCharacterToolStripMenuItem_Click( sender, e );
            return;
         }
         
         if ( txtWEPKey.Text.Trim().Length > 0 )
         {
            if ( rdo13ASCII.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKey.Focus();
                  return;
               }
               txtWEPKeyB.Text = txtWEPKey.Text;

               WEP_KEY = "";              // Start fresh 
               // Convert ASCII WEP Key to Hex digits: 
               // WirelessLAN4u = 576972656C6573734C414E3475 
               foreach ( char c in txtWEPKey.Text.Trim() )
                  WEP_KEY += String.Format( "{0:X}", (int)c );
            }
            if ( rdo26HEX.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 HEX characters long!", "Length Error" );
                  //txtWEPKey.Focus();
                  return;
               }
               txtWEPKeyB.Text = txtWEPKey.Text;
               WEP_KEY = txtWEPKey.Text.Trim();
            }
         }
         return;
      }
      /*--- end of txtWEPKey_Leave() --------------------------------------------*/
      #endregion




      #region txtPassPhrase_Leave 
      /****************************************************************************
           txtPassPhrase_Leave                                    10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments:                                                                                                             
      ****************************************************************************/

      private void txtPassPhrase_Leave( object sender, EventArgs e )
      {
         string stmp;
         bool FndSpace = false;
         bool FndSub = false;
         bool Conflict = false;
         string sSub = String.Format( "{0:X}", Convert.ToInt32( SubstituteChar[ 0 ] ) );

         // check for presence of spaces and substitution character 

         if ( rdoASCII.Checked )
         {
            if ( txtPassPhrase.Text.Contains( " " ) && txtPassPhrase.Text.Contains( SubstituteChar ) )
            {
               Conflict = true;
            }
         }
         else
         {
            for ( int i = 0; i < txtPassPhrase.Text.Length; i += 2 )
            {
               stmp = txtPassPhrase.Text.Substring( i, 2 );
               if ( stmp == "20" )
                  FndSpace = true;
               if ( stmp == sSub )
                  FndSub = true;
            }
            if ( FndSpace && FndSub )
               Conflict = true;
         }

         if ( Conflict )
         {
            MessageBox.Show( "This passphrase contains both spaces and\n" +
                             "your current Substitution Character\n\n" +
                             "Please select a new Substitution Character\n\n" +
                             "The New Substitution Character selector will\n" +
                             "open when you close this window.", "PassPhrase Error" );
            newSubstituteCharacterToolStripMenuItem_Click( sender, e );
            return;
         }

         // replace spaces with dollar signs 
         if ( rdoASCII.Checked )
         {
            char nch = SubstituteChar[ 0 ];
            txtPassPhrase.Text = txtPassPhrase.Text.Replace( ' ', nch );
            txtPassPhraseB.Text = txtPassPhrase.Text;
         }
         else
         {
            try
            {
               PASS_KEY = "";
               for ( int i = 0; i < txtPassPhrase.Text.Length; i += 2 )
               {
                  stmp = txtPassPhrase.Text.Substring( i, 2 );
                  if ( stmp == "20" )
                     stmp = String.Format( "{0:X}", Convert.ToInt32( SubstituteChar[ 0 ] ) );

                  PASS_KEY = PASS_KEY + stmp;
               }
               txtPassPhrase.Text = PASS_KEY;
            }
            catch
            {
            }
            txtPassPhraseB.Text = txtPassPhrase.Text;
         }
      }
      /*--- end of txtPassPhrase_Leave() ----------------------------------------*/
      #endregion




      #region btnCommandSend_Click
      /****************************************************************************
           btnCommandSend_Click                                   08/02/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnCommandSend_Click( object sender, EventArgs e )
      {
         string command = txtCommandSend.Text.Trim();

         DeviceSendCommand( command );
      }
      /*--- end of btnCommandSend_Click() ---------------------------------------*/
      #endregion




      #region pictureBox1_DoubleClick 
      /****************************************************************************
           pictureBox1_DoubleClick                                10/30/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void pictureBox1_DoubleClick( object sender, EventArgs e )
      {
         if ( groupBox4.Visible )
         {
            groupBox4.Visible = false;
            ckbEnableCommandLogging.Visible = false;
         }
         else
         {
            groupBox4.Visible = true;
            ckbEnableCommandLogging.Visible = true;
         }
      }
      /*--- end of pictureBox1_DoubleClick() ------------------------------------*/
      #endregion




      #region AEnCode
      /****************************************************************************
           AENCODE                                                09/16/07
        Description: Encrypt a string
         Parameters: string to encode
       Return Value: encoded string
         Processing: 
          Called By: 
           Comments: encoded string will 2 times + 1 the original
      ****************************************************************************/

      private string AEnCode( string cpw )
      {
         if ( cpw.Trim().Length == 0 )
            return "";

         string enstring = "";

         for ( int x = 0; x < cpw.Length; x++ )
         {
            string ens = cpw.Substring( x, 1 );
            string nl = "";

            switch ( ens ) 
            {
               case "a":      nl = "01";     break;
               case "b":      nl = "02";     break;
               case "c":      nl = "03";     break;
               case "d":      nl = "04";     break;
               case "e":      nl = "05";     break;
               case "f":      nl = "06";     break;
               case "g":      nl = "07";     break;
               case "h":      nl = "08";     break;
               case "i":      nl = "09";     break;
               case "j":      nl = "10";     break;
               case "k":      nl = "11";     break;
               case "l":      nl = "12";     break;
               case "m":      nl = "13";     break;
               case "n":      nl = "14";     break;
               case "o":      nl = "15";     break;
               case "p":      nl = "16";     break;
               case "q":      nl = "17";     break;
               case "r":      nl = "18";     break;
               case "s":      nl = "19";     break;
               case "t":      nl = "20";     break;
               case "u":      nl = "21";     break;
               case "v":      nl = "22";     break;
               case "w":      nl = "23";     break;
               case "x":      nl = "24";     break;
               case "y":      nl = "25";     break;
               case "z":      nl = "26";     break;
               case "1":      nl = "27";     break;
               case "2":      nl = "28";     break;
               case "3":      nl = "29";     break;
               case "4":      nl = "30";     break;
               case "5":      nl = "31";     break;
               case "6":      nl = "32";     break;
               case "7":      nl = "33";     break;
               case "8":      nl = "34";     break;
               case "9":      nl = "35";     break;
               case "0":      nl = "36";     break;
               case "A":      nl = "37";     break;
               case "B":      nl = "38";     break;
               case "C":      nl = "39";     break;
               case "D":      nl = "40";     break;
               case "E":      nl = "41";     break;
               case "F":      nl = "42";     break;
               case "G":      nl = "43";     break;
               case "H":      nl = "44";     break;
               case "I":      nl = "45";     break;
               case "J":      nl = "46";     break;
               case "K":      nl = "47";     break;
               case "L":      nl = "48";     break;
               case "M":      nl = "49";     break;
               case "N":      nl = "50";     break;
               case "O":      nl = "51";     break;
               case "P":      nl = "52";     break;
               case "Q":      nl = "53";     break;
               case "R":      nl = "54";     break;
               case "S":      nl = "55";     break;
               case "T":      nl = "56";     break;
               case "U":      nl = "57";     break;
               case "V":      nl = "58";     break;
               case "W":      nl = "59";     break;
               case "X":      nl = "60";     break;
               case "Y":      nl = "61";     break;
               case "Z":      nl = "62";     break;
               case "`":      nl = "63";     break;
               case "~":      nl = "64";     break;
               case "!":      nl = "65";     break;
               case "@":      nl = "66";     break;
               case "#":      nl = "67";     break;
               case "$":      nl = "68";     break;
               case "%":      nl = "69";     break;
               case "^":      nl = "70";     break;
               case "&":      nl = "71";     break;
               case "*":      nl = "72";     break;
               case "(":      nl = "73";     break;
               case ")":      nl = "74";     break;
               case "-":      nl = "75";     break;
               case "_":      nl = "76";     break;
               case "=":      nl = "77";     break;
               case "+":      nl = "78";     break;
               case "[":      nl = "79";     break;
               case "]":      nl = "80";     break;
               case "{":      nl = "81";     break;
               case "}":      nl = "82";     break;
               case "\\":     nl = "83";     break;
               case "|":      nl = "84";     break;
               case ";":      nl = "85";     break;
               case ":":      nl = "86";     break;
               case "\"":     nl = "87";     break;
               case "'":      nl = "88";     break;
               case ",":      nl = "89";     break;
               case "<":      nl = "90";     break;
               case ".":      nl = "91";     break;
               case ">":      nl = "92";     break;
               case "/":      nl = "93";     break;
               case "?":      nl = "94";     break;
               default:
                  return( "invalid character" );
               //break;
            }

            enstring = enstring + nl;
         }

         int noc = cpw.Length + 50;
         string cksm = noc.ToString();
         enstring = cksm + enstring;

         return( enstring );
      }
      /*--- end of AEnCode() ----------------------------------------------------*/
      #endregion




      #region ADeCode
      /****************************************************************************
           ADECODE                                                09/16/07
        Description: Decrypt a string encrypted with AEnCode()
         Parameters: encoded string
       Return Value: normal string
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private string ADeCode( string cpw )
      {
         if ( cpw.Trim().Length == 0 )
            return "";

         string unstring = "";
         int noc = cpw.Trim().Length;
         string uns = cpw.Substring( 0, 2 );
         int chksm = Convert.ToInt32( uns );
         chksm -= 50;

         if ( chksm != ((noc / 2) - 1 ) ) 
         {
            return( "" );
         }

         for ( int x = 2; x < noc; x += 2 ) 
         {
            string nc = "";
            uns = cpw.Substring( x, 2 );

            switch ( uns ) 
            {
               case "01":        nc = "a";         break;
               case "02":        nc = "b";         break;
               case "03":        nc = "c";         break;
               case "04":        nc = "d";         break;
               case "05":        nc = "e";         break;
               case "06":        nc = "f";         break;
               case "07":        nc = "g";         break;
               case "08":        nc = "h";         break;
               case "09":        nc = "i";         break;
               case "10":        nc = "j";         break;
               case "11":        nc = "k";         break;
               case "12":        nc = "l";         break;
               case "13":        nc = "m";         break;
               case "14":        nc = "n";         break;
               case "15":        nc = "o";         break;
               case "16":        nc = "p";         break;
               case "17":        nc = "q";         break;
               case "18":        nc = "r";         break;
               case "19":        nc = "s";         break;
               case "20":        nc = "t";         break;
               case "21":        nc = "u";         break;
               case "22":        nc = "v";         break;
               case "23":        nc = "w";         break;
               case "24":        nc = "x";         break;
               case "25":        nc = "y";         break;
               case "26":        nc = "z";         break;
               case "27":        nc = "1";         break;
               case "28":        nc = "2";         break;
               case "29":        nc = "3";         break;
               case "30":        nc = "4";         break;
               case "31":        nc = "5";         break;
               case "32":        nc = "6";         break;
               case "33":        nc = "7";         break;
               case "34":        nc = "8";         break;
               case "35":        nc = "9";         break;
               case "36":        nc = "0";         break;
               case "37":        nc = "A";         break;
               case "38":        nc = "B";         break;
               case "39":        nc = "C";         break;
               case "40":        nc = "D";         break;
               case "41":        nc = "E";         break;
               case "42":        nc = "F";         break;
               case "43":        nc = "G";         break;
               case "44":        nc = "H";         break;
               case "45":        nc = "I";         break;
               case "46":        nc = "J";         break;
               case "47":        nc = "K";         break;
               case "48":        nc = "L";         break;
               case "49":        nc = "M";         break;
               case "50":        nc = "N";         break;
               case "51":        nc = "O";         break;
               case "52":        nc = "P";         break;
               case "53":        nc = "Q";         break;
               case "54":        nc = "R";         break;
               case "55":        nc = "S";         break;
               case "56":        nc = "T";         break;
               case "57":        nc = "U";         break;
               case "58":        nc = "V";         break;
               case "59":        nc = "W";         break;
               case "60":        nc = "X";         break;
               case "61":        nc = "Y";         break;
               case "62":        nc = "Z";         break;
               case "63":        nc = "`";         break;
               case "64":        nc = "~";         break;
               case "65":        nc = "!";         break;
               case "66":        nc = "@";         break;
               case "67":        nc = "#";         break;
               case "68":        nc = "$";         break;
               case "69":        nc = "%";         break;
               case "70":        nc = "^";         break;
               case "71":        nc = "&";         break;
               case "72":        nc = "*";         break;
               case "73":        nc = "(";         break;
               case "74":        nc = ")";         break;
               case "75":        nc = "-";         break;
               case "76":        nc = "_";         break;
               case "77":        nc = "=";         break;
               case "78":        nc = "+";         break;
               case "79":        nc = "[";         break;
               case "80":        nc = "]";         break;
               case "81":        nc = "{";         break;
               case "82":        nc = "}";         break;
               case "83":        nc = "\\";        break;
               case "84":        nc = "|";         break;
               case "85":        nc = ";";         break;
               case "86":        nc = ":";         break;
               case "87":        nc = "\"";        break;
               case "88":        nc = "'";         break;
               case "89":        nc = ",";         break;
               case "90":        nc = "<";         break;
               case "91":        nc = ".";         break;
               case "92":        nc = ">";         break;
               case "93":        nc = "/";         break;
               case "94":        nc = "?";         break;
            }
            unstring = unstring + nc;
         }

         return( unstring );
      }

      /*--- end of ADeCode() ----------------------------------------------------*/
      #endregion




      #region beginnerToolStripMenuItem_Click 
      /****************************************************************************
           beginnerToolStripMenuItem_Click                        11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void beginnerToolStripMenuItem_Click( object sender, EventArgs e )
      {
         //beginnerToolStripMenuItem1.Checked = true;
         //expertToolStripMenuItem1.Checked = false;
         ExpertPanel.Visible = false;
         BeginnerPanel.Visible = true;
         webBrowserToolStripMenuItem.Visible = false;
         ExperienceLevel = 1;
      }
      /*--- end of beginnerToolStripMenuItem_Click() ----------------------------*/
      #endregion




      #region expertToolStripMenuItem_Click 
      /****************************************************************************
           expertToolStripMenuItem_Click                          11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void expertToolStripMenuItem_Click( object sender, EventArgs e )
      {
         //beginnerToolStripMenuItem1.Checked = false;
         //expertToolStripMenuItem1.Checked = true;
         ExpertPanel.Visible = true;
         BeginnerPanel.Visible = false;
         webBrowserToolStripMenuItem.Visible = true;
         ExperienceLevel = 2;
      }
      /*--- end of expertToolStripMenuItem_Click() ------------------------------*/
      #endregion




      #region closeToolStripMenuItem_Click 
      /****************************************************************************
           closeToolStripMenuItem_Click                           11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void closeToolStripMenuItem_Click( object sender, EventArgs e )
      {
         _continue = false;
         if ( USBConnected )
            atusb.CloseDevice();
         this.Close();
      }
      /*--- end of closeToolStripMenuItem_Click() -------------------------------*/
      #endregion

      
      
      
      #region btnCommands_Click 
      /****************************************************************************
           btnCommands_Click                                      11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnCommands_Click( object sender, EventArgs e )
      {
         HospWifi.WiFiCommands wif = new HospWifi.WiFiCommands();
         wif.ShowDialog();
         txtCommandSend.Text = wif.CText;
      }
      /*--- end of btnCommands_Click() ------------------------------------------*/
      #endregion




      #region btnSendDataPacket_Click 
      /****************************************************************************
           btnSendDataPacket_Click                                11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSendDataPacket_Click( object sender, EventArgs e )
      {
         DeviceSendCommand( "0S" );
      }
      /*--- end of btnSendDataPacket_Click() ------------------------------------*/
      #endregion




      #region btnSetSSIDB_Click 
      /****************************************************************************
           btnSetSSIDB_Click                                      11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetSSIDB_Click( object sender, EventArgs e )
      {
         string command = "6Eset w s " + txtSSIDB.Text;
         int tries = 0;
         bool result = false;

         btnSetSSIDB.Enabled = false;

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set SSID";
               txtResponse.BackColor = Color.Pink;
            //   pbxSetSSID.Image = ilsLED.Images[ 2 ];
               pbxSetSSIDB.Image = ilsLED.Images[ 2 ];
               btnSetSSIDB.Enabled = true;
               return;
            }
         }

       //  pbxSetSSID.Image = ilsLED.Images[ 3 ];
         pbxSetSSIDB.Image = ilsLED.Images[ 3 ];
         btnSetSSIDB.Enabled = true;
      }
      /*--- end of btnSetSSIDB_Click() ------------------------------------------*/
      #endregion




      #region btnSetChannelB_Click 
      /****************************************************************************
           btnSetChannelB_Click                                   11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetChannelB_Click( object sender, EventArgs e )
      {
         string command = "6Eset w c " + txtChannelB.Text;
         int tries = 0;
         bool result = false;

         btnSetChannelB.Enabled = false;

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set Channel Number";
               txtResponse.BackColor = Color.Pink;
        //       pbxSetChannel.Image = ilsLED.Images[ 2 ];
               pbxSetChannelB.Image = ilsLED.Images[ 2 ];
               btnSetChannelB.Enabled = true;
               return;
            }
         }

         //pbxSetChannel.Image = ilsLED.Images[ 3 ];
         pbxSetChannelB.Image = ilsLED.Images[ 3 ];
         btnSetChannelB.Enabled = true;
      }
      /*--- end of btnSetChannelB_Click() ---------------------------------------*/
      #endregion




      #region btnSetAuthB_Click 
      /****************************************************************************
           btnSetAuthB_Click                                      11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
    SSID : spbat254
password : e34b5ca8df172690293ba5c186d7e40fb1f948325ac7ed606bdfc59174e3082a
           1...|....1....|....2....|....3....|....4....|....5....|....6....|
      ****************************************************************************/

      private void btnSetAuthB_Click( object sender, EventArgs e )
      {
         string command = "";
         int tries = 0;
         bool result = false;

         btnSetAuthB.Enabled = false;
         
         if ( rdoOpenB.Checked )
            command = "6Eset w a 0";   // default 
         if ( rdoWEP128B.Checked )
         {
            if ( rdo13ASCIIB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKeyB.Focus();
                  btnSetAuthB.Enabled = true;
                  return;
               }
            }
            if ( rdo26HEXB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 characters long!", "Length Error" );
                  //txtWEPKeyB.Focus();
                  btnSetAuthB.Enabled = true;
                  return;
               }
            }
            command = "6Eset w a 1";   // WEP-128 
         }
         if ( rdoWPA1B.Checked )
            command = "6Eset w a 2";   // 
         if ( rdoMixedWPAB.Checked )
            command = "6Eset w a 3";   // 
         if ( rdoWPA2B.Checked )
            command = "6Eset w a 4";   // 

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set Authentication";
               txtResponse.BackColor = Color.Pink;
        //       pbxSetAuth.Image = ilsLED.Images[ 2 ];
               pbxSetAuthB.Image = ilsLED.Images[ 2 ];
               btnSetAuthB.Enabled = true;
               return;
            }
         }

         if ( rdoWEP128B.Checked )
         {
            if ( WEP_KEY.Length != 26 )
            {
               if ( txtWEPKeyB.Text.Trim().Length > 0 )
               {
                  if ( txtWEPKeyB.Text.Trim().Length != 13 )
                  {
                     MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                     txtWEPKeyB.Focus();
                  }

                  WEP_KEY = "";              // Start fresh 
                  // Convert ASCII WEP Key to Hex digits: 
                  // WirelessLAN4u = 576972656C6573734C414E3475 
                  foreach ( char c in txtWEPKeyB.Text.Trim() )
                     WEP_KEY += String.Format( "{0:X}", (int)c );
               }
            }
            command = "6Eset w k " + WEP_KEY;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set WEP Key";
                  txtResponse.BackColor = Color.Pink;
              //    pbxSetAuth.Image = ilsLED.Images[ 2 ];
                  pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                  btnSetAuthB.Enabled = true;
                  return;
               }
            }
         }

         if ( rdoWPA1B.Checked || rdoWPA2B.Checked || rdoMixedWPAB.Checked )
         {
            command = "6Eset w p " + txtPassPhraseB.Text;

            if ( command.Length <= 64 )
            {
               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;
                 //    pbxSetAuth.Image = ilsLED.Images[ 2 ];
                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                     //btnSetAuth.Enabled = true;
                     return;
                  }
               }
            }
            else      // must split in two 
            {
               int ilen = txtPassPhraseB.Text.Length;
               string pc1 = txtPassPhraseB.Text.Substring( 0, 32 );   // 1st half
               string pc2 = txtPassPhraseB.Text.Substring( 32 ) + Environment.NewLine;      // 2nd half 

               command = "6{" + pc1;

               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;
                     //pbxSetAuth.Image = ilsLED.Images[ 2 ];
                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                    // btnSetAuth.Enabled = true;
                     return;
                  }
               }

               command = "6}" + pc2;

               tries = 0;
               result = false;
               while ( !result )
               {
                  result = DeviceSendCommand( command, "AOK" );
                  if ( result )
                     break;
                  tries++;
                  if ( tries > 4 )
                  {
                     txtResponse.Text = "Cannot set Pass Phrase";
                     txtResponse.BackColor = Color.Pink;
                  //   pbxSetAuth.Image = ilsLED.Images[ 2 ];
                     pbxSetAuthB.Image = ilsLED.Images[ 2 ];
                    // btnSetAuth.Enabled = true;
                     return;
                  }
               }
            }
         }

     //    pbxSetAuth.Image = ilsLED.Images[ 3 ];
         pbxSetAuthB.Image = ilsLED.Images[ 3 ];
         btnSetAuthB.Enabled = true;
      }
      /*--- end of btnSetAuthB_Click() ------------------------------------------*/
      #endregion
   
      
      

      #region btnSetDHCPB_Click 
      /****************************************************************************
           btnSetDHCPB_Click                                      11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetDHCPB_Click( object sender, EventArgs e )
      {
         string command = "";
         int tries = 0;
         bool result = false;

         btnSetDHCPB.Enabled = false;

         command = "6Eset i d 1";   // default 

         if ( rdoDHCPOffB.Checked )
            command = "6Eset i d 0";
         if ( rdoDHCPOnB.Checked )
            command = "6Eset i d 1";   // default 

         while ( !result )
         {
            result = DeviceSendCommand( command, "AOK" );
            if ( result )
               break;
            tries++;
            if ( tries > 4 )
            {
               txtResponse.Text = "Cannot set DHCP";
               txtResponse.BackColor = Color.Pink;
            //   pbxSetDHCP.Image = ilsLED.Images[ 2 ];
               pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
               btnSetDHCPB.Enabled = true;
               return;
            }
         }

         if ( rdoDHCPOffB.Checked )
         {
            command = "6Eset i a " + txtIPAddressB.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set IP Address";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
                  btnSetDHCPB.Enabled = true;
                  return;
               }
            }

            command = "6Eset i g " + txtGatewayB.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set Gateway";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
                  btnSetDHCPB.Enabled = true;
                  return;
               }
            }

            command = "6Eset i n " + txtSubnetMaskB.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set Subnet Mask";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
                  btnSetDHCPB.Enabled = true;
                  return;
               }
            }

            command = "6Eset d a " + txtDNSServerB.Text;
            tries = 0;
            result = false;
            while ( !result )
            {
               result = DeviceSendCommand( command, "AOK" );
               if ( result )
                  break;
               tries++;
               if ( tries > 4 )
               {
                  txtResponse.Text = "Cannot set DNS Server";
                  txtResponse.BackColor = Color.Pink;
                  //pbxSetDHCP.Image = ilsLED.Images[ 2 ];
                  pbxSetDHCPB.Image = ilsLED.Images[ 2 ];
                  btnSetDHCPB.Enabled = true;
                  return;
               }
            }
         }

       //  pbxSetDHCP.Image = ilsLED.Images[ 3 ];
         pbxSetDHCPB.Image = ilsLED.Images[ 3 ];
         btnSetDHCPB.Enabled = true;
      }
      /*--- end of btnSetDHCPB_Click() ------------------------------------------*/
      #endregion




      #region rdoWorkstationB_CheckedChanged 
      /****************************************************************************
           rdoWorkstationB_CheckedChanged                         11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoWorkstationB_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoWorkstationB.Checked )
            rdoWorkstation.Checked = true;
         else
            rdoWorkstation.Checked = false;
      }
      /*--- end of rdoWorkstationB_CheckedChanged() -----------------------------*/
      #endregion




      #region txtWEPKeyB_Leave 
      /****************************************************************************
           txtWEPKeyB_Leave                                       11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtWEPKeyB_Leave( object sender, EventArgs e )
      {
         if ( txtWEPKeyB.Text.Trim().Length > 0 )
         {
            if ( rdo13ASCIIB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKeyB.Focus();
                  return;
               }
               txtWEPKey.Text = txtWEPKeyB.Text;

               WEP_KEY = "";              // Start fresh 
               // Convert ASCII WEP Key to Hex digits: 
               // WirelessLAN4u = 576972656C6573734C414E3475 
               foreach ( char c in txtWEPKeyB.Text.Trim() )
                  WEP_KEY += String.Format( "{0:X}", (int)c );
            }
            if ( rdo26HEXB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 HEX characters long!", "Length Error" );
                  //txtWEPKeyB.Focus();
                  return;
               }
               txtWEPKey.Text = txtWEPKeyB.Text;
               WEP_KEY = txtWEPKeyB.Text.Trim();
            }
         }

         return;
      }
      /*--- end of txtWEPKeyB_Leave() -------------------------------------------*/
      #endregion




      #region btnSetAllParams_Click 
      /****************************************************************************
           btnSetAllParams_Click                                  11/23/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetAllParams_Click( object sender, EventArgs e )
      {
         btnSetAllParams.Enabled = false;
         txtResponse.BackColor = Color.White;

         // set command mode 
         btnCommandMode_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // set ssid 
         btnSetSSID_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // set Channel 
         btnSetChannel_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // set Auth 
         btnSetAuth_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // set DHCP 
         btnSetDHCP_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // Save 
         btnSave_Click( sender, e );
         Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );

         // Reboot 
         btnReboot_Click( sender, e );
         Application.DoEvents();

         //ProcessCommands();

         btnSetAllParams.Enabled = true;
      }
      /*--- end of btnSetAllParams_Click() --------------------------------------*/
      #endregion




      #region helpToolStripMenuItem_Click 
      /****************************************************************************
           helpToolStripMenuItem_Click                            11/24/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void helpToolStripMenuItem_Click( object sender, EventArgs e )
      {
         Help h = new Help();
         h.Show();
      }
      /*--- end of helpToolStripMenuItem_Click() --------------------------------*/
      #endregion




      #region rdoOpenB_CheckedChanged 
      /****************************************************************************
           rdoOpenB_CheckedChanged                                11/24/10
        Description: Open Authentication on the Beginner screen,
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoOpenB_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoOpenB.Checked )
         {
            rdoOpen.Checked = true;
            txtWEPKeyB.Enabled = false;
            txtPassPhraseB.Enabled = false;
            groupBox9.Enabled = false;
         }
         if ( rdoWEP128B.Checked )
         {
            rdoWEP128.Checked = true;
            txtWEPKeyB.Enabled = true;
            txtPassPhraseB.Enabled = false;
            groupBox9.Enabled = true;
            txtWEPKeyB.Focus();
         }
         if ( rdoWPA1B.Checked )
         {
            rdoWPA1.Checked = true;
            txtWEPKeyB.Enabled = false;
            txtPassPhraseB.Enabled = true;
            groupBox9.Enabled = false;
            txtPassPhraseB.Focus();
         }
         if ( rdoMixedWPAB.Checked )
         {
            rdoMixedWPA.Checked = true;
            txtWEPKeyB.Enabled = false;
            txtPassPhraseB.Enabled = true;
            groupBox9.Enabled = false;
            txtPassPhraseB.Focus();
         }
         if ( rdoWPA2B.Checked )
         {
            rdoWPA2.Checked = true;
            txtWEPKeyB.Enabled = false;
            groupBox9.Enabled = false;
            txtPassPhraseB.Enabled = true;
            txtPassPhraseB.Focus();
         }
      }
      /*--- end of rdoOpenB_CheckedChanged() ------------------------------------*/
      #endregion




      #region rdoDHCPOffB_CheckedChanged 
      /****************************************************************************
           rdoDHCPOffB_CheckedChanged                             11/24/10
        Description: DHCP on the Beginner screen.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdoDHCPOffB_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoDHCPOffB.Checked )
         {
            rdoDHCPOff.Checked = true;
            txtIPAddressB.Enabled = true;
            txtGatewayB.Enabled = true;
            txtSubnetMaskB.Enabled = true;
            txtDNSServerB.Enabled = true;
            txtIPAddressB.Focus();
         }
         else
         {
            rdoDHCPOn.Checked = true;
            rdoDHCPOnB.Checked = true;
            txtIPAddressB.Enabled = false;
            txtGatewayB.Enabled = false;
            txtSubnetMaskB.Enabled = false;
            txtDNSServerB.Enabled = false;
         }
      }
      /*--- end of rdoDHCPOffB_CheckedChanged() ---------------------------------*/
      #endregion




      #region btnSendPacketNow_Click 
      /****************************************************************************
           btnSendPacketNow_Click                                 11/27/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSendPacketNow_Click( object sender, EventArgs e )
      {
         DeviceSendCommand( "0S" );
      }
      /*--- end of btnSendPacketNow_Click() -------------------------------------*/
      #endregion





      #region webBrowserToolStripMenuItem_Click 
      /****************************************************************************
           webBrowserToolStripMenuItem_Click                      05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void webBrowserToolStripMenuItem_Click( object sender, EventArgs e )
      {
         // launch IE 
         System.Diagnostics.Process process1 = new System.Diagnostics.Process();
         //process1.StartInfo.Arguments = "http://cast-dev.stingermedical.com";
         process1.StartInfo.Arguments = CastUrl;
         process1.StartInfo.FileName = "IExplore.exe";
         process1.Start();
      }
      /*--- end of webBrowserToolStripMenuItem_Click() --------------------------*/
      #endregion
      
      
      

      #region txtPassPhraseB_Leave 
      /****************************************************************************
           txtPassPhraseB_Leave                                   05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtPassPhraseB_Leave( object sender, EventArgs e )
      {
         // replace spaces with dollar signs 
         char nch = SubstituteChar[ 0 ];
         txtPassPhraseB.Text = txtPassPhraseB.Text.Replace( ' ', nch );
         txtPassPhrase.Text = txtPassPhraseB.Text;
      }
      /*--- end of txtPassPhraseB_Leave() ---------------------------------------*/
      #endregion




      #region txtIPAddress_Leave 
      /****************************************************************************
           txtIPAddress_Leave                                     05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtIPAddress_Leave( object sender, EventArgs e )
      {
         txtIPAddressB.Text = txtIPAddress.Text;
         txtGatewayB.Text = txtGateway.Text;
         txtSubnetMaskB.Text = txtSubnetMask.Text;
         txtDNSServerB.Text = txtDNSServer.Text;
      }
      /*--- end of txtIPAddress_Leave() -----------------------------------------*/
      #endregion




      #region txtIPAddressB_Leave 
      /****************************************************************************
           txtIPAddressB_Leave                                    05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtIPAddressB_Leave( object sender, EventArgs e )
      {
         txtIPAddress.Text = txtIPAddressB.Text;
         txtGateway.Text = txtGatewayB.Text;
         txtSubnetMask.Text = txtSubnetMaskB.Text;
         txtDNSServer.Text = txtDNSServerB.Text;
      }
      /*--- end of txtIPAddressB_Leave() ----------------------------------------*/
      #endregion
      
      
      

      #region txtChannel_Leave 
      /****************************************************************************
           txtChannel_Leave                                       05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtChannel_Leave( object sender, EventArgs e )
      {
         txtChannelB.Text = txtChannel.Text;
      }
      /*--- end of txtChannel_Leave() -------------------------------------------*/
      #endregion




      #region txtChannelB_Leave 
      /****************************************************************************
           txtChannelB_Leave                                      05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtChannelB_Leave( object sender, EventArgs e )
      {
         txtChannel.Text = txtChannelB.Text;
      }
      /*--- end of txtChannelB_Leave() ------------------------------------------*/
      #endregion
      
      
      

      #region txtSSID_Leave 
      /****************************************************************************
           txtSSID_Leave                                          05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtSSID_Leave( object sender, EventArgs e )
      {
         char nch = SubstituteChar[ 0 ];
         txtSSID.Text = txtSSID.Text.Replace( ' ', nch );
         txtSSIDB.Text = txtSSID.Text;
      }
      /*--- end of txtSSID_Leave() ----------------------------------------------*/
      #endregion




      #region txtSSIDB_Leave 
      /****************************************************************************
           txtSSIDB_Leave                                         05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtSSIDB_Leave( object sender, EventArgs e )
      {
         char nch = SubstituteChar[ 0 ];
         txtSSIDB.Text = txtSSIDB.Text.Replace( ' ', nch );
         txtSSID.Text = txtSSIDB.Text;
      }
      /*--- end of txtSSIDB_Leave() ---------------------------------------------*/
      #endregion




      #region ckbEnableCommandLogging_CheckedChanged 
      /****************************************************************************
           ckbEnableCommandLogging_CheckedChanged                 05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void ckbEnableCommandLogging_CheckedChanged( object sender, EventArgs e )
      {
         if ( ckbEnableCommandLogging.Checked )
            WDebug = true;
         else
            WDebug = false;
      }
      /*--- end of ckbEnableCommandLogging_CheckedChanged() ---------------------*/
      #endregion




      #region HexToDecimal
      /****************************************************************************
           HexToDecimal                                           04/07/10
        Description: String hex value to integer value
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 8 hex characters max: 4,294,967,295 (0xFFFFFFFF) max.
      ****************************************************************************/

      /// <summary>
      /// Convert a hex string to an integer value.
      /// </summary>
      /// <param name="sHex">String to convert</param>
      /// <returns>unsigned integer value</returns>

      private uint HexToDecimal( string sHex )
      {
         uint value = 0;
         uint Multiplier = 1;
         int len = sHex.Length;  // 8 max 

         if ( len > 8 )
         {
            // throw new string length exception 
            return ( value );
         }

         for ( int i = len - 1; i >= 0; i-- )
         {
            switch ( sHex[ i ] )
            {
               case '0':    value += 0 * Multiplier;     break;
               case '1':    value += 1 * Multiplier;     break;
               case '2':    value += 2 * Multiplier;     break;
               case '3':    value += 3 * Multiplier;     break;
               case '4':    value += 4 * Multiplier;     break;
               case '5':    value += 5 * Multiplier;     break;
               case '6':    value += 6 * Multiplier;     break;
               case '7':    value += 7 * Multiplier;     break;
               case '8':    value += 8 * Multiplier;     break;
               case '9':    value += 9 * Multiplier;     break;
               case 'a':    value += 10 * Multiplier;    break;
               case 'A':    value += 10 * Multiplier;    break;
               case 'b':    value += 11 * Multiplier;    break;
               case 'B':    value += 11 * Multiplier;    break;
               case 'c':    value += 12 * Multiplier;    break;
               case 'C':    value += 12 * Multiplier;    break;
               case 'd':    value += 13 * Multiplier;    break;
               case 'D':    value += 13 * Multiplier;    break;
               case 'e':    value += 14 * Multiplier;    break;
               case 'E':    value += 14 * Multiplier;    break;
               case 'f':    value += 15 * Multiplier;    break;
               case 'F':    value += 15 * Multiplier;    break;
            }
            Multiplier = ( Multiplier * 16 );
         }

         return ( value );
      }
      /*--- end of HexToDecimal() -----------------------------------------------*/
      #endregion




      #region rdo13ASCII_CheckedChanged 
      /****************************************************************************
           rdo13ASCII_CheckedChanged                              05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdo13ASCII_CheckedChanged( object sender, EventArgs e )
      {
         bool isHex = true;
         string stmp;

         if ( rdo13ASCII.Checked )
         {
            //txtWEPKey.MaxLength = 13;
            txtWEPKeyB.MaxLength = 13;
            rdo13ASCIIB.Checked = true;

            isHex = true;
            // is the WEP key currently in ASCII or Hex? 
            foreach ( char c in txtWEPKey.Text.Trim() )
            {
               if ( c < 0x30 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x39 && c < 0x41 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x46 && c < 0x61 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x66 ) 
               {
                  isHex = false;
                  break;
               }
            }

            if ( isHex ) 
            {
               // need to convert it to ASCII 
               try
               {
                  WEP_KEY = "";
                  for ( int i = 0; i < txtWEPKey.Text.Length; i += 2 )
                  {
                     stmp = txtWEPKey.Text.Substring( i, 2 );

                     WEP_KEY = WEP_KEY + Convert.ToChar( HexToDecimal( stmp ) );
                  }
                  txtWEPKey.Text = WEP_KEY;
               }
               catch
               {
                  txtWEPKey.Text = "Error";
               }
            }
         }
         if ( rdo26HEX.Checked )
         {
            //txtWEPKey.MaxLength = 26;
            txtWEPKeyB.MaxLength = 26;
            rdo26HEXB.Checked = true;

            isHex = true;
            // is the WEP key currently in ASCII or Hex? 
            foreach ( char c in txtWEPKey.Text.Trim() )
            {
               if ( c < 0x30 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x39 && c < 0x41 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x46 && c < 0x61 ) 
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x66 ) 
               {
                  isHex = false;
                  break;
               }
            }

            if ( !isHex ) 
            {
               // need to convert it to hex 
               WEP_KEY = "";
               foreach ( char c in txtWEPKey.Text.Trim() )
                  WEP_KEY += String.Format( "{0:X}", (int)c );
               txtWEPKey.Text = WEP_KEY;
            }
         }
      }
      /*--- end of rdo13ASCII_CheckedChanged() ----------------------------------*/
      #endregion




      #region rdo13ASCIIB_CheckedChanged 
      /****************************************************************************
           rdo13ASCIIB_CheckedChanged                             05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void rdo13ASCIIB_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdo13ASCIIB.Checked )
         {
            txtWEPKey.MaxLength = 13;
            txtWEPKeyB.MaxLength = 13;
            rdo13ASCII.Checked = true;
         }
         if ( rdo26HEXB.Checked )
         {
            txtWEPKey.MaxLength = 26;
            txtWEPKeyB.MaxLength = 26;
            rdo26HEX.Checked = true;
         }
      }
      /*--- end of rdo13ASCIIB_CheckedChanged() ---------------------------------*/
      #endregion




      #region txtWEPKeyB_Leave_1 
      /****************************************************************************
           txtWEPKeyB_Leave_1                                     05/19/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtWEPKeyB_Leave_1( object sender, EventArgs e )
      {
         if ( txtWEPKeyB.Text.Trim().Length > 0 )
         {
            if ( rdo13ASCIIB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKeyB.Focus();
                  return;
               }
               txtWEPKey.Text = txtWEPKeyB.Text;

               WEP_KEY = "";              // Start fresh 
               // Convert ASCII WEP Key to Hex digits: 
               // WirelessLAN4u = 576972656C6573734C414E3475 
               foreach ( char c in txtWEPKeyB.Text.Trim() )
                  WEP_KEY += String.Format( "{0:X}", (int)c );
            }
            if ( rdo26HEXB.Checked )
            {
               if ( txtWEPKeyB.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 HEX characters long!", "Length Error" );
                  //txtWEPKeyB.Focus();
                  return;
               }
               txtWEPKey.Text = txtWEPKeyB.Text;
               WEP_KEY = txtWEPKeyB.Text.Trim();
            }
         }
         return;
      }
      /*--- end of txtWEPKeyB_Leave_1() -----------------------------------------*/
      #endregion




      #region btnUpdateRoving_Click 
      /****************************************************************************
           btnUpdateRoving_Click                                  10/11/11
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnUpdateRoving_Click( object sender, EventArgs e )
      {
         string command = "6E$$$";
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eset u f 0";                      // hardware flow control off 
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eset ftp address 198.175.253.161";  // Roving default 
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eset ftp dir public";             // ftp directory 
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eset ftp user roving";            // ftp user 
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eset ftp pass Pass123";           // ftp password 
         DeviceSendCommand( command );

         WaitForData();

         command = "6Eftp u";
         DeviceSendCommand( command );

         MessageBox.Show( "Roving Firmware Update\n\nPlease allow 1 minute for the download,\nThen do a SAVE and REBOOT.", "HospWiFi Message" );
      }
      /*--- end of btnUpdateRoving_Click() --------------------------------------*/
      #endregion




      #region newSubstituteCharacterToolStripMenuItem_Click 
      /****************************************************************************
           newSubstituteCharacterToolStripMenuItem_Click          04/14/12
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void newSubstituteCharacterToolStripMenuItem_Click( object sender, EventArgs e )
      {
         Substitute sub = new Substitute();
         sub.ShowDialog();

         if ( sub.NewCharacter != "Cancel" && sub.NewCharacter.Length == 1 )
         {
            // check for valid character 
            byte nch = (byte)sub.NewCharacter[ 0 ];

            if ( nch < 0x21 || nch > 0x7E )
            {
               MessageBox.Show( "Invalid Replacement Character Chosen\nNo Change." );
               return;
            }

            SubstituteChar = sub.NewCharacter;
            string snch = String.Format( "0x{0:X2}", nch );

            lblSSID1.Text = "(Will convert spaces to " + SubstituteChar + ")";
            lblSSID2.Text = "(Will convert spaces to " + SubstituteChar + ")";
            lblSSID3.Text = "(Will convert spaces to " + SubstituteChar + ")";
            lblSSID4.Text = "(Will convert spaces to " + SubstituteChar + ")";

            string command = "6E$$$";
            DeviceSendCommand( command );

            WaitForData();

            command = "6Eset o r " + snch;    // opt replace  

            DeviceSendCommand( command );

            WaitForData();
         }
      }
      /*--- end of newSubstituteCharacterToolStripMenuItem_Click() --------------*/
      #endregion




      private void btnWSSNGet_Click( object sender, EventArgs e )
      {
         string command = "0O";

         DeviceSendCommand( command );
      }




      private void btnWSSNSet_Click( object sender, EventArgs e )
      {
         string command = "0N" + txtWSSerialNumber.Text.Trim();

         DeviceSendCommand( command );
      }




      private void rdoASCII_CheckedChanged( object sender, EventArgs e )
      {
         bool isHex = true;
         string stmp;

         if ( rdoASCII.Checked )
         {
            isHex = true;
            // is the WEP key currently in ASCII or Hex? 
            foreach ( char c in txtPassPhrase.Text.Trim() )
            {
               if ( c < 0x30 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x39 && c < 0x41 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x46 && c < 0x61 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x66 )
               {
                  isHex = false;
                  break;
               }
            }

            if ( isHex )
            {
               // need to convert it to ASCII 
               try
               {
                  PASS_KEY = "";
                  for ( int i = 0; i < txtPassPhrase.Text.Length; i += 2 )
                  {
                     stmp = txtPassPhrase.Text.Substring( i, 2 );

                     PASS_KEY = PASS_KEY + Convert.ToChar( HexToDecimal( stmp ) );
                  }
                  txtPassPhrase.Text = PASS_KEY;
               }
               catch
               {
                  txtPassPhrase.Text = "Error";
               }
            }
         }
         else      // hex is checked 
         {
            isHex = true;
            // is the PASS key currently in ASCII or Hex? 
            foreach ( char c in txtPassPhrase.Text.Trim() )
            {
               if ( c < 0x30 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x39 && c < 0x41 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x46 && c < 0x61 )
               {
                  isHex = false;
                  break;
               }
               if ( c > 0x66 )
               {
                  isHex = false;
                  break;
               }
            }

            if ( !isHex )
            {
               // need to convert it to hex 
               PASS_KEY = "";
               foreach ( char c in txtPassPhrase.Text.Trim() )
                  PASS_KEY += String.Format( "{0:X}", (int)c );
               txtPassPhrase.Text = PASS_KEY;
            }
         }
      }



      // this is for the field techs 
      private void lblMobius3_DoubleClick( object sender, EventArgs e )
      {
         if ( txtPassPhrase.PasswordChar == '*' )
         {
            txtPassPhrase.PasswordChar = (char)0;
            txtWEPKey.PasswordChar = (char)0;
         }
         else
         {
            txtPassPhrase.PasswordChar = (char)'*';
            txtWEPKey.PasswordChar = (char)'*';
         }
      }










   }
}
/*--- end of HospWiFi.cs --------------------------------------------------------*/

