﻿namespace RhythmField
{
    partial class frmTestSite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Telerik.WinControls.UI.CartesianArea cartesianArea1 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CartesianArea cartesianArea2 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CartesianArea cartesianArea3 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewDateTimeColumn gridViewDateTimeColumn1 = new Telerik.WinControls.UI.GridViewDateTimeColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor1 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.Data.SortDescriptor sortDescriptor2 = new Telerik.WinControls.Data.SortDescriptor();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.CartesianArea cartesianArea4 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CartesianArea cartesianArea5 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.CartesianArea cartesianArea6 = new Telerik.WinControls.UI.CartesianArea();
            Telerik.WinControls.UI.LinearAxis linearAxis1 = new Telerik.WinControls.UI.LinearAxis();
            Telerik.WinControls.UI.LinearAxis linearAxis2 = new Telerik.WinControls.UI.LinearAxis();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.pulseDSTLxy = new RhythmField.pulseDSTLxy();
            this.comboSites = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.testLogXYTableAdapter = new RhythmField.pulseDSTLxyTableAdapters.TestLogXYTableAdapter();
            this.testLogXYBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtStepInterval = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.radChartView4 = new Telerik.WinControls.UI.RadChartView();
            this.radChartView6 = new Telerik.WinControls.UI.RadChartView();
            this.radChartView5 = new Telerik.WinControls.UI.RadChartView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.radChartView3 = new Telerik.WinControls.UI.RadChartView();
            this.radChartView2 = new Telerik.WinControls.UI.RadChartView();
            this.radChartView1 = new Telerik.WinControls.UI.RadChartView();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ibStatus = new FontAwesomeIcons.IconButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSiteName = new System.Windows.Forms.Label();
            this.txtTestStartDate = new System.Windows.Forms.Label();
            this.txtPercentAllocated = new System.Windows.Forms.Label();
            this.txtPercentActive = new System.Windows.Forms.Label();
            this.iconButton1 = new FontAwesomeIcons.IconButton();
            this.btnExport = new FontAwesomeIcons.IconButton();
            this.btnStartTest = new FontAwesomeIcons.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pulseDSTLxy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.testLogXYBindingSource)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView4)).BeginInit();
            this.radChartView4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView5)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView3)).BeginInit();
            this.radChartView3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStartTest)).BeginInit();
            this.SuspendLayout();
            // 
            // bindingSource1
            // 
            this.bindingSource1.DataSource = this.pulseDSTLxy;
            this.bindingSource1.Position = 0;
            // 
            // pulseDSTLxy
            // 
            this.pulseDSTLxy.DataSetName = "pulseDSTLxy";
            this.pulseDSTLxy.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // comboSites
            // 
            this.comboSites.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboSites.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboSites.DisplayMember = "SiteID";
            this.comboSites.FormattingEnabled = true;
            this.comboSites.Location = new System.Drawing.Point(65, 8);
            this.comboSites.Name = "comboSites";
            this.comboSites.Size = new System.Drawing.Size(276, 21);
            this.comboSites.TabIndex = 36;
            this.comboSites.ValueMember = "IDSite";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 20);
            this.label1.TabIndex = 37;
            this.label1.Text = "Site:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(114, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 13);
            this.label2.TabIndex = 39;
            this.label2.Text = "*Do not choose an HQ/IDN*";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.Location = new System.Drawing.Point(536, 5);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(172, 17);
            this.checkBox1.TabIndex = 42;
            this.checkBox1.Text = "Leave Supplied/Scanned AP\'s";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(373, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(34, 13);
            this.label4.TabIndex = 45;
            this.label4.Text = "Start";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(430, 46);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 13);
            this.label5.TabIndex = 46;
            this.label5.Text = "Stop";
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox2.Location = new System.Drawing.Point(536, 35);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(111, 17);
            this.checkBox2.TabIndex = 47;
            this.checkBox2.Text = "Remove Chargers";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // testLogXYTableAdapter
            // 
            this.testLogXYTableAdapter.ClearBeforeFill = true;
            // 
            // testLogXYBindingSource
            // 
            this.testLogXYBindingSource.DataMember = "TestLogXY";
            this.testLogXYBindingSource.DataSource = this.bindingSource1;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox3.Location = new System.Drawing.Point(536, 50);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(170, 17);
            this.checkBox3.TabIndex = 50;
            this.checkBox3.Text = "View Previous Tests Data Grid";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(211, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(129, 13);
            this.label6.TabIndex = 52;
            this.label6.Text = "Step Interval (Tick Marks)";
            this.label6.Visible = false;
            // 
            // txtStepInterval
            // 
            this.txtStepInterval.BackColor = System.Drawing.SystemColors.Control;
            this.txtStepInterval.Location = new System.Drawing.Point(185, 56);
            this.txtStepInterval.Name = "txtStepInterval";
            this.txtStepInterval.Size = new System.Drawing.Size(29, 20);
            this.txtStepInterval.TabIndex = 51;
            this.txtStepInterval.Text = "120";
            this.txtStepInterval.Visible = false;
            // 
            // tabPage4
            // 
            this.tabPage4.AutoScroll = true;
            this.tabPage4.BackColor = System.Drawing.Color.White;
            this.tabPage4.Controls.Add(this.radChartView4);
            this.tabPage4.Controls.Add(this.radChartView5);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1284, 668);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Charts";
            // 
            // radChartView4
            // 
            cartesianArea1.GridDesign.DrawVerticalFills = false;
            cartesianArea1.ShowGrid = true;
            this.radChartView4.AreaDesign = cartesianArea1;
            this.radChartView4.AutoScroll = true;
            this.radChartView4.Controls.Add(this.radChartView6);
            this.radChartView4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radChartView4.LegendTitle = "Device Type";
            this.radChartView4.Location = new System.Drawing.Point(3, 426);
            this.radChartView4.MinimumSize = new System.Drawing.Size(1278, 423);
            this.radChartView4.Name = "radChartView4";
            // 
            // 
            // 
            this.radChartView4.RootElement.MinSize = new System.Drawing.Size(1278, 423);
            this.radChartView4.SelectionMode = Telerik.WinControls.UI.ChartSelectionMode.SingleDataPoint;
            this.radChartView4.ShowLegend = true;
            this.radChartView4.ShowPanZoom = true;
            this.radChartView4.ShowTitle = true;
            this.radChartView4.ShowToolTip = true;
            this.radChartView4.ShowTrackBall = true;
            this.radChartView4.Size = new System.Drawing.Size(1278, 423);
            this.radChartView4.TabIndex = 1;
            this.radChartView4.Text = "radChartView4";
            this.radChartView4.Title = "Allocated (Floor/Wing - Double Click To Change)";
            this.radChartView4.DoubleClick += new System.EventHandler(this.radChartView4_DoubleClick);
            // 
            // radChartView6
            // 
            cartesianArea2.GridDesign.DrawVerticalFills = false;
            cartesianArea2.ShowGrid = true;
            this.radChartView6.AreaDesign = cartesianArea2;
            this.radChartView6.AutoScroll = true;
            this.radChartView6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radChartView6.LegendTitle = "Device Type";
            this.radChartView6.Location = new System.Drawing.Point(0, 0);
            this.radChartView6.MinimumSize = new System.Drawing.Size(1278, 423);
            this.radChartView6.Name = "radChartView6";
            // 
            // 
            // 
            this.radChartView6.RootElement.MinSize = new System.Drawing.Size(1278, 423);
            this.radChartView6.SelectionMode = Telerik.WinControls.UI.ChartSelectionMode.SingleDataPoint;
            this.radChartView6.ShowLegend = true;
            this.radChartView6.ShowPanZoom = true;
            this.radChartView6.ShowTitle = true;
            this.radChartView6.ShowToolTip = true;
            this.radChartView6.ShowTrackBall = true;
            this.radChartView6.Size = new System.Drawing.Size(1278, 423);
            this.radChartView6.TabIndex = 2;
            this.radChartView6.Text = "radChartView6";
            this.radChartView6.Title = "Allocated (Department - Double Click To Change)";
            this.radChartView6.DoubleClick += new System.EventHandler(this.radChartView6_DoubleClick);
            // 
            // radChartView5
            // 
            cartesianArea3.GridDesign.DrawVerticalFills = false;
            cartesianArea3.ShowGrid = true;
            this.radChartView5.AreaDesign = cartesianArea3;
            this.radChartView5.AutoScroll = true;
            this.radChartView5.Dock = System.Windows.Forms.DockStyle.Top;
            this.radChartView5.LegendTitle = "Device Type";
            this.radChartView5.Location = new System.Drawing.Point(3, 3);
            this.radChartView5.Margin = new System.Windows.Forms.Padding(1);
            this.radChartView5.Name = "radChartView5";
            this.radChartView5.SelectionMode = Telerik.WinControls.UI.ChartSelectionMode.SingleDataPoint;
            this.radChartView5.ShowLegend = true;
            this.radChartView5.ShowPanZoom = true;
            this.radChartView5.ShowTitle = true;
            this.radChartView5.ShowToolTip = true;
            this.radChartView5.ShowTrackBall = true;
            this.radChartView5.Size = new System.Drawing.Size(1261, 426);
            this.radChartView5.TabIndex = 0;
            this.radChartView5.Text = "radChartView5";
            this.radChartView5.Title = "Total Active";
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView5.GetChildAt(0))).AutoSize = true;
            ((Telerik.WinControls.UI.ChartLegendElement)(this.radChartView5.GetChildAt(0).GetChildAt(0).GetChildAt(2))).AutoSize = true;
            ((Telerik.WinControls.UI.ChartLegendElement)(this.radChartView5.GetChildAt(0).GetChildAt(0).GetChildAt(2))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.radGridView1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1284, 668);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // radGridView1
            // 
            this.radGridView1.BackColor = System.Drawing.Color.Transparent;
            this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
            this.radGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 8.25F);
            this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.radGridView1.Location = new System.Drawing.Point(3, 3);
            // 
            // 
            // 
            this.radGridView1.MasterTemplate.AllowAddNewRow = false;
            this.radGridView1.MasterTemplate.AllowDeleteRow = false;
            this.radGridView1.MasterTemplate.AllowEditRow = false;
            this.radGridView1.MasterTemplate.AllowSearchRow = true;
            this.radGridView1.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
            this.radGridView1.MasterTemplate.ClipboardCopyMode = Telerik.WinControls.UI.GridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            gridViewTextBoxColumn1.EnableExpressionEditor = false;
            gridViewTextBoxColumn1.FieldName = "SiteName";
            gridViewTextBoxColumn1.HeaderText = "Site";
            gridViewTextBoxColumn1.Name = "SiteName";
            gridViewTextBoxColumn1.ReadOnly = true;
            gridViewTextBoxColumn1.Width = 23;
            gridViewDateTimeColumn1.EnableExpressionEditor = false;
            gridViewDateTimeColumn1.FieldName = "LogDate";
            gridViewDateTimeColumn1.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            gridViewDateTimeColumn1.HeaderText = "LogDate";
            gridViewDateTimeColumn1.Name = "LogDate";
            gridViewDateTimeColumn1.ReadOnly = true;
            gridViewDateTimeColumn1.SortOrder = Telerik.WinControls.UI.RadSortOrder.Descending;
            gridViewDateTimeColumn1.Width = 64;
            gridViewTextBoxColumn2.EnableExpressionEditor = false;
            gridViewTextBoxColumn2.FieldName = "workstations";
            gridViewTextBoxColumn2.HeaderText = "Workstations";
            gridViewTextBoxColumn2.Multiline = true;
            gridViewTextBoxColumn2.Name = "workstations";
            gridViewTextBoxColumn2.ReadOnly = true;
            gridViewTextBoxColumn2.Width = 68;
            gridViewTextBoxColumn3.EnableExpressionEditor = false;
            gridViewTextBoxColumn3.FieldName = "WorkstationsOriginal";
            gridViewTextBoxColumn3.HeaderText = "Workstations Original";
            gridViewTextBoxColumn3.Multiline = true;
            gridViewTextBoxColumn3.Name = "WorkstationsOriginal";
            gridViewTextBoxColumn3.ReadOnly = true;
            gridViewTextBoxColumn3.Width = 109;
            gridViewTextBoxColumn4.EnableExpressionEditor = false;
            gridViewTextBoxColumn4.FieldName = "chargers";
            gridViewTextBoxColumn4.HeaderText = "Chargers";
            gridViewTextBoxColumn4.Multiline = true;
            gridViewTextBoxColumn4.Name = "chargers";
            gridViewTextBoxColumn4.ReadOnly = true;
            gridViewTextBoxColumn4.Width = 49;
            gridViewTextBoxColumn5.EnableExpressionEditor = false;
            gridViewTextBoxColumn5.FieldName = "chargersoriginal";
            gridViewTextBoxColumn5.HeaderText = "Chargers Original";
            gridViewTextBoxColumn5.Multiline = true;
            gridViewTextBoxColumn5.Name = "chargersoriginal";
            gridViewTextBoxColumn5.ReadOnly = true;
            gridViewTextBoxColumn5.Width = 89;
            gridViewTextBoxColumn6.EnableExpressionEditor = false;
            gridViewTextBoxColumn6.FieldName = "batteries";
            gridViewTextBoxColumn6.HeaderText = "Batteries";
            gridViewTextBoxColumn6.Multiline = true;
            gridViewTextBoxColumn6.Name = "batteries";
            gridViewTextBoxColumn6.ReadOnly = true;
            gridViewTextBoxColumn6.Width = 49;
            gridViewTextBoxColumn7.EnableExpressionEditor = false;
            gridViewTextBoxColumn7.FieldName = "batteriesoriginal";
            gridViewTextBoxColumn7.HeaderText = "Batteries Original";
            gridViewTextBoxColumn7.Multiline = true;
            gridViewTextBoxColumn7.Name = "batteriesoriginal";
            gridViewTextBoxColumn7.ReadOnly = true;
            gridViewTextBoxColumn7.Width = 87;
            gridViewTextBoxColumn8.EnableExpressionEditor = false;
            gridViewTextBoxColumn8.FieldName = "WorkstationsAllocated";
            gridViewTextBoxColumn8.HeaderText = "Workstations Floor";
            gridViewTextBoxColumn8.Multiline = true;
            gridViewTextBoxColumn8.Name = "WorkstationsAllocated";
            gridViewTextBoxColumn8.ReadOnly = true;
            gridViewTextBoxColumn8.Width = 115;
            gridViewTextBoxColumn9.EnableExpressionEditor = false;
            gridViewTextBoxColumn9.FieldName = "WorkstationsAllocatedOriginal";
            gridViewTextBoxColumn9.HeaderText = "Original";
            gridViewTextBoxColumn9.Multiline = true;
            gridViewTextBoxColumn9.Name = "WorkstationsAllocatedOriginal";
            gridViewTextBoxColumn9.ReadOnly = true;
            gridViewTextBoxColumn9.Width = 45;
            gridViewTextBoxColumn10.EnableExpressionEditor = false;
            gridViewTextBoxColumn10.FieldName = "ChargersAllocated";
            gridViewTextBoxColumn10.HeaderText = "Chargers Floor";
            gridViewTextBoxColumn10.Multiline = true;
            gridViewTextBoxColumn10.Name = "ChargersAllocated";
            gridViewTextBoxColumn10.ReadOnly = true;
            gridViewTextBoxColumn10.Width = 98;
            gridViewTextBoxColumn11.EnableExpressionEditor = false;
            gridViewTextBoxColumn11.FieldName = "ChargersAllocatedOriginal";
            gridViewTextBoxColumn11.HeaderText = "Original";
            gridViewTextBoxColumn11.Multiline = true;
            gridViewTextBoxColumn11.Name = "ChargersAllocatedOriginal";
            gridViewTextBoxColumn11.ReadOnly = true;
            gridViewTextBoxColumn11.Width = 45;
            gridViewTextBoxColumn12.EnableExpressionEditor = false;
            gridViewTextBoxColumn12.FieldName = "WorkstationsAllocatedDepartment";
            gridViewTextBoxColumn12.HeaderText = "Workstations Department";
            gridViewTextBoxColumn12.Multiline = true;
            gridViewTextBoxColumn12.Name = "WorkstationsAllocatedDepartment";
            gridViewTextBoxColumn12.ReadOnly = true;
            gridViewTextBoxColumn12.SortOrder = Telerik.WinControls.UI.RadSortOrder.Ascending;
            gridViewTextBoxColumn12.Width = 192;
            gridViewTextBoxColumn13.EnableExpressionEditor = false;
            gridViewTextBoxColumn13.FieldName = "WorkstationsAllocatedDepartmentOriginal";
            gridViewTextBoxColumn13.HeaderText = "Original";
            gridViewTextBoxColumn13.Multiline = true;
            gridViewTextBoxColumn13.Name = "WorkstationsAllocatedDepartmentOriginal";
            gridViewTextBoxColumn13.ReadOnly = true;
            gridViewTextBoxColumn13.Width = 45;
            gridViewTextBoxColumn14.EnableExpressionEditor = false;
            gridViewTextBoxColumn14.FieldName = "ChargersAllocatedDepartment";
            gridViewTextBoxColumn14.HeaderText = "Chargerss Department";
            gridViewTextBoxColumn14.Multiline = true;
            gridViewTextBoxColumn14.Name = "ChargersAllocatedDepartment";
            gridViewTextBoxColumn14.ReadOnly = true;
            gridViewTextBoxColumn14.Width = 119;
            gridViewTextBoxColumn15.EnableExpressionEditor = false;
            gridViewTextBoxColumn15.FieldName = "ChargersAllocatedDepartmentOriginal";
            gridViewTextBoxColumn15.HeaderText = "Original";
            gridViewTextBoxColumn15.Multiline = true;
            gridViewTextBoxColumn15.Name = "ChargersAllocatedDepartmentOriginal";
            gridViewTextBoxColumn15.ReadOnly = true;
            gridViewTextBoxColumn15.Width = 75;
            this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewDateTimeColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15});
            this.radGridView1.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridView1.MasterTemplate.MultiSelect = true;
            this.radGridView1.MasterTemplate.SelectionMode = Telerik.WinControls.UI.GridViewSelectionMode.CellSelect;
            sortDescriptor1.Direction = System.ComponentModel.ListSortDirection.Descending;
            sortDescriptor1.PropertyName = "LogDate";
            sortDescriptor2.PropertyName = "WorkstationsAllocatedDepartment";
            this.radGridView1.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor1,
            sortDescriptor2});
            this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView1.Name = "radGridView1";
            this.radGridView1.ReadOnly = true;
            this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.radGridView1.Size = new System.Drawing.Size(1278, 662);
            this.radGridView1.TabIndex = 42;
            this.radGridView1.Text = "radGridView1";
            // 
            // tabPage1
            // 
            this.tabPage1.AutoScroll = true;
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.radChartView3);
            this.tabPage1.Controls.Add(this.radChartView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1284, 668);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Extra Charts";
            // 
            // radChartView3
            // 
            this.radChartView3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            cartesianArea4.ShowGrid = true;
            this.radChartView3.AreaDesign = cartesianArea4;
            this.radChartView3.Controls.Add(this.radChartView2);
            this.radChartView3.LegendTitle = "Device Types";
            this.radChartView3.Location = new System.Drawing.Point(0, 356);
            this.radChartView3.Name = "radChartView3";
            this.radChartView3.ShowLegend = true;
            this.radChartView3.ShowTitle = true;
            this.radChartView3.ShowToolTip = true;
            this.radChartView3.ShowTrackBall = true;
            this.radChartView3.Size = new System.Drawing.Size(856, 357);
            this.radChartView3.TabIndex = 45;
            this.radChartView3.Text = "radChartView3";
            this.radChartView3.Title = "Allocated (Floor/Wing)";
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView3.GetChildAt(0))).NumberOfColors = 10;
            // 
            // radChartView2
            // 
            this.radChartView2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            cartesianArea5.ShowGrid = true;
            this.radChartView2.AreaDesign = cartesianArea5;
            this.radChartView2.AutoScroll = true;
            this.radChartView2.LegendTitle = "Device Type";
            this.radChartView2.Location = new System.Drawing.Point(0, 1);
            this.radChartView2.Name = "radChartView2";
            this.radChartView2.ShowLegend = true;
            this.radChartView2.ShowTitle = true;
            this.radChartView2.ShowToolTip = true;
            this.radChartView2.ShowTrackBall = true;
            this.radChartView2.Size = new System.Drawing.Size(853, 350);
            this.radChartView2.TabIndex = 44;
            this.radChartView2.Text = "radChartView2";
            this.radChartView2.Title = "Allocated (Department)";
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView2.GetChildAt(0))).NumberOfColors = 10;
            // 
            // radChartView1
            // 
            cartesianArea6.ShowGrid = true;
            this.radChartView1.AreaDesign = cartesianArea6;
            this.radChartView1.AutoScroll = true;
            linearAxis1.IsPrimary = true;
            linearAxis1.TickOrigin = null;
            linearAxis2.AxisType = Telerik.Charting.AxisType.Second;
            linearAxis2.IsPrimary = true;
            linearAxis2.TickOrigin = null;
            this.radChartView1.Axes.AddRange(new Telerik.WinControls.UI.Axis[] {
            linearAxis1,
            linearAxis2});
            this.radChartView1.Dock = System.Windows.Forms.DockStyle.Top;
            this.radChartView1.LegendTitle = "Device Types";
            this.radChartView1.Location = new System.Drawing.Point(3, 3);
            this.radChartView1.Name = "radChartView1";
            this.radChartView1.ShowLegend = true;
            this.radChartView1.ShowPanZoom = true;
            this.radChartView1.ShowTitle = true;
            this.radChartView1.ShowToolTip = true;
            this.radChartView1.ShowTrackBall = true;
            this.radChartView1.Size = new System.Drawing.Size(1261, 350);
            this.radChartView1.TabIndex = 43;
            this.radChartView1.Text = "radChartView1";
            this.radChartView1.Title = "Total";
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView1.GetChildAt(0))).NumberOfColors = 10;
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView1.GetChildAt(0))).TextOrientation = System.Windows.Forms.Orientation.Horizontal;
            ((Telerik.WinControls.UI.RadChartElement)(this.radChartView1.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(0, 76);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1292, 694);
            this.tabControl1.TabIndex = 49;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            this.tabControl1.Deselected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Deselected);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox4.Location = new System.Drawing.Point(536, 20);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(127, 17);
            this.checkBox4.TabIndex = 53;
            this.checkBox4.Text = "Remove Manual Flag";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(481, 46);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 56;
            this.label7.Text = "Export";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.ibStatus);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtSiteName);
            this.groupBox1.Controls.Add(this.txtTestStartDate);
            this.groupBox1.Controls.Add(this.txtPercentAllocated);
            this.groupBox1.Controls.Add(this.txtPercentActive);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(714, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(566, 73);
            this.groupBox1.TabIndex = 57;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "System Status";
            // 
            // ibStatus
            // 
            this.ibStatus.ActiveColor = System.Drawing.Color.Black;
            this.ibStatus.BackColor = System.Drawing.Color.Transparent;
            this.ibStatus.IconType = FontAwesomeIcons.IconType.CheckSquareO;
            this.ibStatus.InActiveColor = System.Drawing.Color.DimGray;
            this.ibStatus.Location = new System.Drawing.Point(5, 14);
            this.ibStatus.Margin = new System.Windows.Forms.Padding(0);
            this.ibStatus.Name = "ibStatus";
            this.ibStatus.Size = new System.Drawing.Size(35, 40);
            this.ibStatus.TabIndex = 61;
            this.ibStatus.TabStop = false;
            this.ibStatus.ToolTipText = null;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(255, 38);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 62;
            this.label8.Text = "Accuracy:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(255, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 16);
            this.label3.TabIndex = 58;
            this.label3.Text = "Complete:";
            // 
            // txtSiteName
            // 
            this.txtSiteName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSiteName.AutoSize = true;
            this.txtSiteName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSiteName.Location = new System.Drawing.Point(41, 17);
            this.txtSiteName.Name = "txtSiteName";
            this.txtSiteName.Size = new System.Drawing.Size(60, 20);
            this.txtSiteName.TabIndex = 61;
            this.txtSiteName.Text = "Active: ";
            // 
            // txtTestStartDate
            // 
            this.txtTestStartDate.AutoSize = true;
            this.txtTestStartDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTestStartDate.Location = new System.Drawing.Point(45, 43);
            this.txtTestStartDate.Name = "txtTestStartDate";
            this.txtTestStartDate.Size = new System.Drawing.Size(35, 13);
            this.txtTestStartDate.TabIndex = 1;
            this.txtTestStartDate.Text = "label3";
            this.txtTestStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtPercentAllocated
            // 
            this.txtPercentAllocated.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPercentAllocated.AutoSize = true;
            this.txtPercentAllocated.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPercentAllocated.Location = new System.Drawing.Point(340, 39);
            this.txtPercentAllocated.Name = "txtPercentAllocated";
            this.txtPercentAllocated.Size = new System.Drawing.Size(65, 16);
            this.txtPercentAllocated.TabIndex = 60;
            this.txtPercentAllocated.Text = "Allocated";
            // 
            // txtPercentActive
            // 
            this.txtPercentActive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPercentActive.AutoSize = true;
            this.txtPercentActive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPercentActive.Location = new System.Drawing.Point(340, 18);
            this.txtPercentActive.Name = "txtPercentActive";
            this.txtPercentActive.Size = new System.Drawing.Size(51, 16);
            this.txtPercentActive.TabIndex = 59;
            this.txtPercentActive.Text = "Active: ";
            // 
            // iconButton1
            // 
            this.iconButton1.ActiveColor = System.Drawing.Color.Red;
            this.iconButton1.BackColor = System.Drawing.Color.Transparent;
            this.iconButton1.IconType = FontAwesomeIcons.IconType.Stop;
            this.iconButton1.InActiveColor = System.Drawing.Color.DimGray;
            this.iconButton1.Location = new System.Drawing.Point(432, 13);
            this.iconButton1.Name = "iconButton1";
            this.iconButton1.Size = new System.Drawing.Size(31, 31);
            this.iconButton1.TabIndex = 61;
            this.iconButton1.TabStop = false;
            this.iconButton1.ToolTipText = null;
            this.iconButton1.Click += new System.EventHandler(this.iconButton1_Click_1);
            // 
            // btnExport
            // 
            this.btnExport.ActiveColor = System.Drawing.Color.Black;
            this.btnExport.BackColor = System.Drawing.Color.Transparent;
            this.btnExport.IconType = FontAwesomeIcons.IconType.FileExcelO;
            this.btnExport.InActiveColor = System.Drawing.Color.DimGray;
            this.btnExport.Location = new System.Drawing.Point(488, 13);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(30, 31);
            this.btnExport.TabIndex = 60;
            this.btnExport.TabStop = false;
            this.btnExport.ToolTipText = null;
            // 
            // btnStartTest
            // 
            this.btnStartTest.ActiveColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStartTest.BackColor = System.Drawing.Color.Transparent;
            this.btnStartTest.IconType = FontAwesomeIcons.IconType.Play;
            this.btnStartTest.InActiveColor = System.Drawing.Color.DimGray;
            this.btnStartTest.Location = new System.Drawing.Point(376, 13);
            this.btnStartTest.Name = "btnStartTest";
            this.btnStartTest.Size = new System.Drawing.Size(31, 31);
            this.btnStartTest.TabIndex = 58;
            this.btnStartTest.TabStop = false;
            this.btnStartTest.ToolTipText = null;
            this.btnStartTest.Click += new System.EventHandler(this.btnStartTest_Click);
            // 
            // frmTestSite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1292, 742);
            this.Controls.Add(this.iconButton1);
            this.Controls.Add(this.btnExport);
            this.Controls.Add(this.btnStartTest);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtStepInterval);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.comboSites);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmTestSite";
            this.ShowIcon = false;
            this.Text = "Test Site Control Panel";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmTestSite_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pulseDSTLxy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.testLogXYBindingSource)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView4)).EndInit();
            this.radChartView4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView5)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView3)).EndInit();
            this.radChartView3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radChartView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radChartView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ibStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnExport)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnStartTest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboSites;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtStepInterval;
        private pulseDSTLxy pulseDSTLxy;
        private System.Windows.Forms.BindingSource bindingSource1;
        private pulseDSTLxyTableAdapters.TestLogXYTableAdapter testLogXYTableAdapter;
        private System.Windows.Forms.BindingSource testLogXYBindingSource;
        private System.Windows.Forms.TabPage tabPage4;
        private Telerik.WinControls.UI.RadChartView radChartView4;
        private Telerik.WinControls.UI.RadChartView radChartView5;
        private System.Windows.Forms.TabPage tabPage2;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private System.Windows.Forms.TabPage tabPage1;
        private Telerik.WinControls.UI.RadChartView radChartView2;
        private Telerik.WinControls.UI.RadChartView radChartView3;
        private Telerik.WinControls.UI.RadChartView radChartView1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label txtTestStartDate;
        private Telerik.WinControls.UI.RadChartView radChartView6;
        private System.Windows.Forms.Label txtPercentAllocated;
        private System.Windows.Forms.Label txtPercentActive;
        private System.Windows.Forms.Label txtSiteName;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label3;
        private FontAwesomeIcons.IconButton ibStatus;
        private FontAwesomeIcons.IconButton btnStartTest;
        private FontAwesomeIcons.IconButton btnExport;
        private FontAwesomeIcons.IconButton iconButton1;

    }
}