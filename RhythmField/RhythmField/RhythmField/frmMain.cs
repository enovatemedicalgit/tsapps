﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using RhythmField;
using Telerik.WinControls.Export;
using Telerik.WinControls.UI;
using Telerik.WinControls.Tools;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Telerik.Windows;
using Telerik.WinControls.Themes;
using Telerik.Windows.Documents.Core;
using Telerik.Windows.Documents.Fixed;
using Telerik.Windows.Documents.Spreadsheet;
using System.Runtime.InteropServices;
using Telerik.Windows.Zip;
using Telerik.WinControls.Data;

using Telerik.WinControls.UI.Docking;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Spreadsheet.Expressions.Functions;
using Telerik.Windows.Documents.Spreadsheet.Model;
using Telerik.WinControls.Export;
using System.Reflection;

//using System.Net.Mail;
//using Microsoft.Office;
//using Outlook = Microsoft.Office;


namespace RhythmField
{
    public partial class frmMain : Form
    {

        public string URL = "http://rhythm.myenovate.com/";
        private long _currentSite = 0;
        private string _currentSiteName = "";
        SpreadExportRenderer exportRenderer = new SpreadExportRenderer();
        private GridViewSpreadExport spreadsheetExporter;
        private bool bRememberMe;
        private string sUsername = "";
        private string sPassword = "";
        public string sUserTypeID = "";
        public string iUserID = "0";
        private string sSiteID = "";
        private bool bLoggedIn;
        public string curPage;
        public string IPAddress = "";
        public int detectedSiteID = 4;
        public int detectedCustomerID = 4;
        public int iTimercnt = 0;
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        public frmMain()
        {
            InitializeComponent();
            txtPacketSerialNo.Visible = false;
            lblPacketSerial.Visible = false;
            cmdPacketSearch.Visible = false;
            lblSearch.Visible = false;
            lblBatteriesExporting.Visible = false;
            lblChargersExporting.Visible = false;
            lblWorkstationsExporting.Visible = false;
            lblPacketExport.Visible = false;
            LoadParams();
            EngQS.Enabled = false;
            EngPackets.Enabled = false;
            EngQS.Item.Visibility = ElementVisibility.Collapsed;
            EngPackets.Item.Visibility = ElementVisibility.Hidden;
            using (var form = new frmLoginToPulse())
            {
                form.StartPosition = FormStartPosition.CenterParent;
                form.Username = sUsername;
                form.Password = sPassword;

                if (bRememberMe == true)
                {
                    form.RememberMe = true;

                }
                var result = form.ShowDialog();
                sUsername = form.Username;
                sPassword = form.Password;
                if (sUsername.Trim() == "") Environment.Exit(1);

                if (result == DialogResult.OK && form.siteID != null)
                {
                    string val = form.siteID;
                    bRememberMe = form.RememberMe;
                    sUsername = form.Username;
                    sPassword = form.Password;
                    iUserID = form.iUserID;
                    sUserTypeID = form.iUserType;
                    lblUsername.Text = " " + sUsername + " - User: " + iUserID.ToString();
                
                    if (sUsername.ToUpper() == "Jason.Batts@enovatemedical.com".ToUpper() || 
                        sUsername.ToUpper() == "Mark.Dalen@enovatemedical.com".ToUpper()|| 
                        sUsername.ToUpper() == "arlow.farrell@enovatemedical.com".ToUpper() ||
                                          sUsername.ToUpper() == "chris.stewart@enovatemedical.com".ToUpper() ||
                        sUsername.ToUpper() == "bill.murray@enovatemedical.com".ToUpper() || 
                        sUsername.ToUpper() == "Mark.Dalen@enovatemedical.com".ToUpper())
                    
                    {
                        btnAdmin.Visible = true;
                        iconButton10.Visible = true;
                        label10.Visible = true;
                        //txtLocation.Visible = true;
                    }
                    if (sUsername == "frank.sims@enovatemedical.com" || 
                        sUsername == "mark.selman@enovatemedical.com" || 
                        sUsername == "arlow.farrell@enovatemedical.com" || 
                        sUsername == "chris.stewart@enovatemedical.com" ||
                        sUsername == "Mark.Dalen@enovatemedical.com" ||
                        sUsername.ToUpper() == "bill.murray@enovatemedical.com".ToUpper() ||
                        sUserTypeID == "4")
                    {
                        EngQS.Visible = true;
                        EngPackets.Visible = true;
                        EngQS.Item.Visibility = ElementVisibility.Visible;
                        EngPackets.Item.Visibility = ElementVisibility.Visible;
                    }
                    if (System.Convert.ToInt32(sUserTypeID) > 4)
                    {
                        gbManagerReports.Visible = true;
                        gbManagerReports.Enabled = true;    
                    }
                     if (sUsername.ToUpper() == "gordon.waid@enovatemedical.com".ToUpper()  || 
                        sUsername.ToUpper()  == "jason.batts@enovatemedical.com".ToUpper()  || 
                        sUsername.ToUpper()  == "arlow.farrell@enovatemedical.com".ToUpper() 
                        )
                     {
                         btnSHutdown.Visible = true;

                     }
                    sSiteID = form.siteID;
                    bLoggedIn = true;
                    SaveParams();
                  Application.DoEvents();
            resizeLoop();
                       Application.DoEvents();
                }

                else
                {
                    Environment.Exit(1);
                }

            }



            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            string strVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                strVersion = ad.CurrentVersion.ToString();
            }
            else
            {
                strVersion = assemName.Version.ToString();
            }
            label2.Text = System.String.Format("{0} ({1})", assemName.Name, strVersion);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;



                        this.WindowState = FormWindowState.Normal;
           
            LoadSites();
            timer1.Enabled = true;


            //URL = "http://localhost:49814/"; // for local debugging
            Application.DoEvents();
            resizeLoop();
            Application.DoEvents();
             this.WindowState = FormWindowState.Maximized;
        }
          public void  GetLocation(object sender, DoWorkEventArgs e)
        {
               
            try
            {
                string url = "http://checkip.dyndns.org";
                System.Net.WebRequest req = System.Net.WebRequest.Create(url);
                req.Timeout = 18000;
                System.Net.WebResponse resp = req.GetResponse();
                System.IO.StreamReader sr = new System.IO.StreamReader(resp.GetResponseStream());
                string response =sr.ReadToEnd().Trim();
                string[] a = response.Split(':');
                string a2 = a[1].Substring(1);
                string[] a3 = a2.Split('<');
                string a4 = a3[0];
                var parms = new Dictionary<string, string>();
                parms.Add("IPAddress", a4);
                IPAddress = a4;
                DataTable dtloc = ExecStoredProc("[prcSitesSelect]", false, parms);
                Application.DoEvents();
                  string res = "http://ipinfo.io/" + a4 + "/city";
                  string ipResponse;
                if (dtloc.Rows.Count > 0)
                {
                    int ids = System.Convert.ToInt32(dtloc.Rows[0]["IDSite"].ToString());
                    detectedSiteID = System.Convert.ToInt32(ids);
                    detectedCustomerID = System.Convert.ToInt32(dtloc.Rows[0]["CustomerID"].ToString());
                    try
                    {
                              ipResponse = IPRequestHelper(res);
                    }
                    catch  (Exception ex)
                    {
                        ipResponse = "";
                    }
                     e.Result = " Currently at " + dtloc.Rows[0]["SiteName"].ToString() + " (" + a4 + ") 15 miles from " + ipResponse;

                }
                else
                {
                 
                     ipResponse = IPRequestHelper(res);
                   e.Result =  " Currently in " + ipResponse + " (" + a4 + ")";
                }
                    
            }
            catch (Exception ex) 
            {
                timer1.Enabled = false;
                e.Result = "";
            }
       }
       

        public string IPRequestHelper(string url)
        {

            string checkURL = url;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            StreamReader responseStream = new StreamReader(objResponse.GetResponseStream());
            string responseRead = responseStream.ReadToEnd();
            responseRead = responseRead.Replace("\n", String.Empty);
            responseStream.Close();
            responseStream.Dispose();
            return responseRead;
        }


        private void cmdBatteries_Click(object sender, EventArgs e)
        {
            if (_currentSite == 0 || _currentSiteName == "System.Data.DataRowView")
            {
                MessageBox.Show("Please Select a Site first...");
                return;
            }

            lblBatteries.Text = "Working...";
            radPageView1.SelectedPage = PageBattery;
            Application.DoEvents();
            var parms = new Dictionary<string, string>();

            if (radDropDownSites.SelectedItem["IsIDN"].ToString() == "1")
            {
                parms.Add("CustomerID", _currentSite.ToString());
            }
            else
            {
                parms.Add("SiteID", _currentSite.ToString());
            }
            radGridViewBatteries.DataSource = null;
            try
            {
                radGridViewBatteries.DataSource = ExecStoredProc("[prcGetBattery]", false, parms);
            }
            catch
            {
                MessageBox.Show("No Batteries found");
            }

            radGridViewBatteries.MasterTemplate.BestFitColumns();
            lblBatteries.Text = "Batteries";
            try
            {
                ExpressionFormattingObject obj = new ExpressionFormattingObject("Shutdown or BRP", "[Shutdown or BRP] = 'Y'", true);
                obj.RowBackColor = Color.Red;
                obj.CellForeColor = Color.Black;

                this.radGridViewBatteries.Columns["Shutdown or BRP"].ConditionalFormattingObjectList.Add(obj);
            }
            catch
            {

            }
        }

        public void GetChargers()
        {

        }
        private void cmdChargers_Click(object sender, EventArgs e)
        {
            if (_currentSite == 0 || _currentSiteName == "System.Data.DataRowView")
            {
                MessageBox.Show("Please Select a Site first...");
                return;
            }
            lblChargers.Text = "Working...";
            radPageView1.SelectedPage = PageCharger;
            Application.DoEvents();
            var parms = new Dictionary<string, string>();

            if (radDropDownSites.SelectedItem["IsIDN"].ToString() == "1")
            {
                parms.Add("CustomerID", _currentSite.ToString());
            }
            else
            {
                parms.Add("SiteID", _currentSite.ToString());
            }
            radGridViewChargers.DataSource = null;
            try
            {
                radGridViewChargers.DataSource = ExecStoredProc("[prcGetCharger]", false, parms);
            }
            catch
            {
                MessageBox.Show("No Chargers found");
            }
            radGridViewChargers.MasterTemplate.BestFitColumns();
            lblChargers.Text = "Chargers";

            // AddSummaryRow(radGridViewChargers);
        }

        private void cmdWorkStations_Click(object sender, EventArgs e)
        {
            if (_currentSite == 0 || _currentSiteName == "System.Data.DataRowView")
            {
                MessageBox.Show("Please Select a Site first...");
                return;
            }
            lblWorkstations.Text = "Working...";
            radPageView1.SelectedPage = PageWorkStations;
            Application.DoEvents();
            var parms = new Dictionary<string, string>();

            if (radDropDownSites.SelectedItem["IsIDN"].ToString() == "1")
            {
                parms.Add("CustomerID", _currentSite.ToString());
            }
            else
            {
                parms.Add("SiteID", _currentSite.ToString());
            }
            try
            {

                radGridViewWorkstations.DataSource = null;
                radGridViewWorkstations.DataSource = ExecStoredProc("[prcGetWorkstation]", false, parms);
            }
            catch
            {
                MessageBox.Show("No Workstations found");
            }
            radGridViewWorkstations.MasterTemplate.BestFitColumns();
            lblWorkstations.Text = "Workstations";
         

        }
        public void SendErrorMail()
        {

        }
     
        public void DoSearch()
        {
            if (_currentSite == 0 && _currentSiteName == "System.Data.DataRowView")
            {
                MessageBox.Show("Please Select a Site first...");
                return;
            }
            if (txtPacketSerialNo.Text.Trim() == "")
            {
                MessageBox.Show("Please Ener a Serial Number first...");
                return;
            }

            lblSearch.Text = "Working...";

            Application.DoEvents();
            var parms = new Dictionary<string, string>();

            parms.Add("AssetSerialNumber", txtPacketSerialNo.Text.Trim());
            if (sUserTypeID == "4" || sUserTypeID == "6")
            {
                  parms.Add("IsAdmin", "1");
            }
            else
            {
                    parms.Add("IsAdmin", "0");
            }
            radGridViewPackets.DataSource = null;
            try
            {
                radGridViewPackets.DataSource = ExecStoredProc("[GetPacketsFast]", false, parms);

            }
            catch
            {
                MessageBox.Show("No Packets found");
            }
            radGridViewPackets.MasterTemplate.BestFitColumns();
            lblSearch.Text = "Search";

        }
        private void cmdPacketSearch_Click(object sender, EventArgs e)
        {

        }




        private DataTable ExecStoredProc(string procName, bool isEng, Dictionary<string, string> paramsDictionary)
        {
            ApiLayer2 api = new ApiLayer2();
            if (isEng == true)
            {
                api.URL = "http://rhythm.myenovate.com/";
            }
            else
            {
                api.URL = URL;
            }
            DataTable dt = new DataTable();
            dt = api.ExecSpApi(procName, paramsDictionary);
            return dt;
        }

        private void LoadSites()
        {
            try
            {
                var parms = new Dictionary<string, string>();
                parms.Add("IDSite", "4");
                radDropDownSites.DataSource = ExecStoredProc("[prcSitesAndCustomersSelect]", false, parms);
                radDropDownSites.ValueMember = "IDSite";
                radDropDownSites.DisplayMember = "SiteDescription";
                radDropDownSites.SortStyle = SortStyle.Ascending;
                radDropDownSites.Text = "Select Site";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void radDropDownSites_SelectedIndexChanged(object sender, Telerik.WinControls.UI.Data.PositionChangedEventArgs e)
        {

        }

        private void radDropDownSites_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                _currentSite = (long)((Telerik.WinControls.UI.Data.ValueChangedEventArgs)(e)).NewValue;
                _currentSiteName = ((Telerik.WinControls.UI.RadDropDownList)(sender)).SelectedText;
            }
            catch (Exception ex)
            {
                //just skip if this is the 1st time in before everything's been inited
            }
            lblSiteId.Text = _currentSite.ToString();
        }



        private void radPageView1_SelectedPageChanged(object sender, EventArgs e)
        {
            curPage = ((Telerik.WinControls.UI.RadPageView)(sender)).SelectedPage.Text;
            switch (curPage)
            {
                case "Packets":
                    txtPacketSerialNo.Visible = true;
                    lblPacketSerial.Visible = true;

                    lblSite.Visible = false;
                    lblSiteId.Visible = false;

                    lblSiteId.Text = _currentSiteName == "System.Data.DataRowView" ? "" : _currentSiteName;
                    radDropDownSites.Visible = false;
                    cmdPacketSearch.Visible = true;
                    lblSearch.Visible = true;
                    break;
                default:
                    txtPacketSerialNo.Visible = false;
                    lblPacketSerial.Visible = false;

                    lblSite.Visible = true;
                    lblSiteId.Visible = true;
                    lblSiteId.Text = _currentSite.ToString();
                    cmdPacketSearch.Visible = false;
                    lblSearch.Visible = false;
                    radDropDownSites.Visible = true;
                    break;

            }
        }


        public void ExportToExcel(RadGridView radGridExport)
        {
            string exportDialogFileName = "";
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.RestoreDirectory = true;

            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            dialog.Title = "Save Grid to Excel";


            string s = "Export.xls";

            radGridExport.ThemeName = "Office2010";
            spreadsheetExporter = new GridViewSpreadExport(radGridExport);

            spreadsheetExporter.ExportFormat = SpreadExportFormat.Xlsx;

            spreadsheetExporter.ExportVisualSettings = true;
            spreadsheetExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadsheetExporter.SheetMaxRows = Telerik.WinControls.UI.Export.ExcelMaxRows._65536;

            dialog.DefaultExt = ".xlsx";
            dialog.AddExtension = true;
            dialog.Filter = "Excel Files|*.xlsx";
            if (curPage == null)
            {
                curPage = "Batteries";
            }

            if (curPage == "Packets")
            {
                dialog.FileName = txtPacketSerialNo.Text.Trim() + " ";
            }
            else
            {
                dialog.FileName = radDropDownSites.SelectedItem.Text.ToString().Trim() + " " + curPage.ToString();
            }
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                s = dialog.FileName;
                exportDialogFileName = s;

            }
            else
            {
                //lblSearching.Visible = false;
                return;
            }

            spreadsheetExporter.ExportVisualSettings = true;
            try
            {
                spreadsheetExporter.RunExport(exportDialogFileName, exportRenderer);
            }
            catch
            {
                MessageBox.Show(dialog.FileName.ToString() + " is open by another process, saving as " + dialog.FileName + "1");
                spreadsheetExporter.RunExport(exportDialogFileName + "1", exportRenderer);
            }
            MessageBox.Show("Export Complete...");
        }

        private void cmdExportBatteries_Click(object sender, EventArgs e)
        {
            if (radGridViewBatteries.Rows.Count < 1)
            {
                MessageBox.Show("No data to export");
                return;
            }
            lblBatteriesExporting.Visible = true;
            Application.DoEvents();
            ExportToExcel(radGridViewBatteries);
            lblBatteriesExporting.Visible = false;

        }

        private void cmdExportChargers_Click(object sender, EventArgs e)
        {
            if (radGridViewChargers.Rows.Count < 1)
            {
                MessageBox.Show("No data to export");
                return;
            }

            lblChargersExporting.Visible = true;
            Application.DoEvents();
            ExportToExcel(radGridViewChargers);
            lblChargersExporting.Visible = false;
        }

        private void cmdExportWorkstations_Click(object sender, EventArgs e)
        {
            if (radGridViewWorkstations.Rows.Count < 1)
            {
                MessageBox.Show("No data to export");
                return;
            }

            lblWorkstationsExporting.Visible = true;
            Application.DoEvents();

            ExportToExcel(radGridViewWorkstations);
            lblWorkstationsExporting.Visible = false;
        }

        private void cmdExportPackets_Click(object sender, EventArgs e)
        {
            if (radGridViewPackets.Rows.Count < 1)
            {
                MessageBox.Show("No data to export");
                return;
            }

            lblPacketExport.Visible = true;
            Application.DoEvents();
            try
            {
                ExportToExcel(radGridViewPackets);
            }
            catch
            {

            }
            lblPacketExport.Visible = false;
        }


        #region WriteCommand
        /****************************************************************************
          WriteCommand                                             11/08/06
        Description: Writes debug info to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void WriteCommand(string command)
        {
            //if (!WDebug)
            //    return;

            try
            {
                StreamWriter textOut =
                   new StreamWriter(new FileStream("Debug.log",
                                                     FileMode.Append,
                                                     FileAccess.Write));

                textOut.WriteLine("HospWiFi: " + DateTime.Now.ToString());
                textOut.WriteLine(" Command: " + command);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
            }

            return;
        }
        /*--- end of WriteCommand() -----------------------------------------------*/
        #endregion

        #region WriteResponse
        /****************************************************************************
          WriteResponse                                            11/08/06
        Description: Writes debug info to a text file.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private void WriteResponse(string response)
        {
            //if (!WDebug)
            //    return;

            try
            {
                StreamWriter textOut =
                   new StreamWriter(new FileStream("Debug.log",
                                                     FileMode.Append,
                                                     FileAccess.Write));

                textOut.WriteLine("HospWiFi: " + DateTime.Now.ToString());
                textOut.WriteLine("Response: " + response);
                textOut.WriteLine("------------------------------------------------------------------------------");
                textOut.Close();
            }
            catch (IOException iox)
            {
                string errfptr = "Could not write to Debug.log. Error: " + iox.ToString();
            }

            return;
        }
        /*--- end of WriteResponse() ----------------------------------------------*/
        #endregion

        #region GetParam
        /****************************************************************************
           GetParam                                               08/02/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private string GetParam(StreamReader textIn, string key)
        {
            string sline = "";
            string value = "";

            while (true)
            {
                try
                {
                    sline = textIn.ReadLine();
                }
                catch
                {
                    break;
                }

                if (sline == null)
                    break;

                if (sline.Contains(key))
                {
                    int ipos = sline.IndexOf('=');
                    if (ipos > 0)
                    {
                        try
                        {
                            value = sline.Substring(ipos + 1);
                        }
                        catch
                        {
                            value = "";
                        }
                        break;
                    }
                }
            }

            return value;
        }
        /*--- end of GetParam() ---------------------------------------------------*/
        #endregion

        #region LoadParams
        /****************************************************************************
           LoadParams                                             08/02/10
        Description: Read the saved params and load the textboxes. 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        public void LoadParams()
        {
            StreamReader textIn;
            string sTemp = "";

            try
            {
                textIn = new StreamReader(new FileStream("RhythmField.cfg", FileMode.Open, FileAccess.Read));
            }
            catch
            {
                return;
            }

            sUsername = GetParam(textIn, "USERNAME");
            sPassword = GetParam(textIn, "PASSWORD");

            if (sUsername.Length > 1)
            {
                bRememberMe = true;

            }

            textIn.Close();

            return;
        }
        /*--- end of LoadParams() -------------------------------------------------*/
        #endregion

        #region SaveParams
        /****************************************************************************
           SaveParams                                             08/02/10
        Description: Save the data in the textboxes.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        public void SaveParams()
        {
            StreamWriter textOut;

            try
            {
                textOut = new StreamWriter(new FileStream("RhythmField.cfg", FileMode.Create, FileAccess.ReadWrite));
            }
            catch
            {
                MessageBox.Show("Cannot save to RhythmField.cfg file; please make sure you have permission to write to this file.");
                return;
            }

            if (bRememberMe == true)
            {
                textOut.WriteLine("USERNAME=" + sUsername);
                textOut.WriteLine("PASSWORD=" + sPassword);
            }
            textOut.Close();
            return;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            if (this.Width >= Screen.PrimaryScreen.WorkingArea.Width-21 && this.Height >= Screen.PrimaryScreen.WorkingArea.Height-21)
            {
                 this.WindowState = FormWindowState.Normal;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - Screen.PrimaryScreen.WorkingArea.Width/10;
              this.Height = Screen.PrimaryScreen.WorkingArea.Height  - Screen.PrimaryScreen.WorkingArea.Height/10;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.CenterToScreen();
                this.radPageView1.Height = this.Height - 120;
            }
            else
            {
                // this.WindowState = FormWindowState.Maximized;
               //  this.Width = Screen.PrimaryScreen.WorkingArea.Width-20;
               // this.Height = Screen.PrimaryScreen.WorkingArea.Height-20;
                   this.WindowState = FormWindowState.Maximized;
               // FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                           this.radPageView1.Height = this.Height - 110;
          
               
            }

        }
        private void resizeLoop(){
 
            Application.DoEvents();
                   this.WindowState = FormWindowState.Maximized;
               // FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                           this.radPageView1.Height = this.Height - 110;
          
                       Application.DoEvents();
           
            Application.DoEvents();
            if (this.Width >= Screen.PrimaryScreen.WorkingArea.Width-21 && this.Height >= Screen.PrimaryScreen.WorkingArea.Height-21)
            {
                 this.WindowState = FormWindowState.Normal;
                this.Width = Screen.PrimaryScreen.WorkingArea.Width - Screen.PrimaryScreen.WorkingArea.Width/10;
              this.Height = Screen.PrimaryScreen.WorkingArea.Height  - Screen.PrimaryScreen.WorkingArea.Height/10;
                this.StartPosition = FormStartPosition.CenterScreen;
                this.CenterToScreen();
                this.radPageView1.Height = this.Height - 120;
            }
            else
            {
                // this.WindowState = FormWindowState.Maximized;
               //  this.Width = Screen.PrimaryScreen.WorkingArea.Width-20;
               // this.Height = Screen.PrimaryScreen.WorkingArea.Height-20;
                   this.WindowState = FormWindowState.Maximized;
               // FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
                           this.radPageView1.Height = this.Height - 110;
          
               
            }
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblBatteries_Click(object sender, EventArgs e)
        {

        }

        private void AddFooterBatteries(object sender, GridViewBindingCompleteEventArgs e)
        {
            Debug.WriteLine("test");

        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
             //ImportLDAPIntoBugTrack asd =  new ImportLDAPIntoBugTrack();
             //asd.ImportThem();
            frmAdmin fma = new frmAdmin();
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Show();

        }

        private void txtPacketSerialNo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DoSearch();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            Process.Start("mailto:Pulse.Support@EnovateMedical.com");
        }

       
        private void iconButton1_Click(object sender, EventArgs e)
        {

            Application.DoEvents();
            var parms = new Dictionary<string, string>();


            radGridViewPackets.DataSource = null;
            try
            {
                radgridEngPackets.DataSource = ExecStoredProc("[GetPacketsFast]", true, parms);
            }
            catch
            {
                MessageBox.Show("No Packets found");
            }
            radgridEngPackets.MasterTemplate.BestFitColumns();
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
           
            Application.DoEvents();
            var parms = new Dictionary<string, string>();

            radGridViewEngQS.DataSource = null;
            try
            {
                radgridEngPackets.DataSource = ExecStoredProc("[prcGetQSPacket]", true, parms);
            }
            catch
            {
                MessageBox.Show("No Packets found");
            }
            radGridViewEngQS.MasterTemplate.BestFitColumns();
        }

        private void iconButton3_Click(object sender, EventArgs e)
        {
            InputRecycle fma = new InputRecycle();
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.iUserID = iUserID;
            fma.Show();
            fma.BringToFront();
            

        }

        private void iconButton4_Click(object sender, EventArgs e)
        {
            if (_currentSite == 0 || _currentSiteName == "System.Data.DataRowView")
            {
                MessageBox.Show("Please Select a Site first...");
                return;
            }
            var parms = new Dictionary<string, string>();
            parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

            var table = new DataTable();

            table = ExecStoredProc("[spSalesforceCasesReport]", false, parms);



            RadGridView RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "CellImbalanceReport" + DateTime.Now.Minute.ToString();


              RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;

            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
             

            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.BestFitColumns();
            GroupDescriptor descriptor = new GroupDescriptor();
            descriptor.GroupNames.Add("Case Status", ListSortDirection.Descending);
            RadGridviewSearch.AutoExpandGroups = true;
            RadGridviewSearch.GroupDescriptors.Add(descriptor);

            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);


            fma.Show();

        }

        private void iconButton6_Click(object sender, EventArgs e)
        {

            var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

            var table = new DataTable();

            table = ExecStoredProc("[spAPScanReport]", false, parms);



            RadGridView RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "CellImbalanceReport" + DateTime.Now.Minute.ToString();


            RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;
            RadGridviewSearch.AllowSearchRow = true;
            RadGridviewSearch.AllowMultiColumnSorting = true;
            RadGridviewSearch.EnableFiltering = true;
            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);


            fma.Show();
        }

        private void iconButton7_Click(object sender, EventArgs e)
        {
            var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

            var table = new DataTable();

            table = ExecStoredProc("[prcGetDisabledBatteries]", false, parms);



            RadGridView RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "DisabledBatteriesReport" + DateTime.Now.Minute.ToString();


            RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;

            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);

            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);

            fma.Show();
            try
            {
                ExpressionFormattingObject obj = new ExpressionFormattingObject("MaxCycleCount", "[MaxCycleCount] = '10'", false);
                obj.RowBackColor = Color.Red;
                obj.CellForeColor = Color.Black;

                RadGridviewSearch.Columns["MaxCycleCount"].ConditionalFormattingObjectList.Add(obj);
            }
            catch
            {

            }
            ConditionalFormattingObject obj2 = new ConditionalFormattingObject("MaxCycleCount", ConditionTypes.Greater, "10", "", false);
            obj2.CellBackColor = Color.Red;
            obj2.CellForeColor = Color.Black;
            RadGridviewSearch.Columns["MaxCycleCount"].ConditionalFormattingObjectList.Add(obj2);
        }

        private void iconButton5_Click(object sender, EventArgs e)
        {
            var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

            var table = new DataTable();

            table = ExecStoredProc("[prcGetRecycledAssets]", false, parms);



            RadGridView RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "RecycledReport" + DateTime.Now.Minute.ToString();


            RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;

            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.AllowSearchRow = true;
            RadGridviewSearch.MasterTemplate.ShowFilteringRow = true;
            RadGridviewSearch.MasterTemplate.ShowTotals = true;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);

            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);

            fma.Show();
        }



        private void Report_Click(object sender, EventArgs e)
        {

            var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

            var table = new DataTable();

            table = ExecStoredProc("[spSiteTestReport]", false, parms);



            RadGridView RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "CellImbalanceReport" + DateTime.Now.Minute.ToString();


            RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;

            RadGridviewSearch.DataSource = table;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);


            fma.Show();
        }

        private void iconButton10_Click(object sender, EventArgs e)
        {

        }

        private void btnMark_Click(object sender, EventArgs e)
        {

        }

        private void frmMain_Load(object sender, EventArgs e)
        {
             //    this.Height = Screen.PrimaryScreen.WorkingArea.Height-100;
             //this.Width  = Screen.PrimaryScreen.WorkingArea.Width-100;
             //          this.radPageView1.Height = this.Height - 180;
             ////this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
             //          this.WindowState = FormWindowState.Normal;
             //    this.CenterToScreen();
             //    this.WindowState = FormWindowState.Maximized;
        }

 
        private void pictureBox6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void iconButton10_Click_1(object sender, EventArgs e)
        {
            frmTestSite fma2 = new frmTestSite();
            fma2.StartPosition = FormStartPosition.CenterScreen;
            fma2.Show();
        }

        private void iconButton11_Click(object sender, EventArgs e)
        {
            DoSearch();
        }

        private void iconButton8_Click(object sender, EventArgs e)
        {

        }

      void bgWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
{
    if (e.Error != null)
    {
       // MessageBox.Show(e.Error.Message);
    }
    else
    {
        string location = (string)e.Result;
        //txtLocation.Text = location;
    }
}
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (iTimercnt == 0)
            {
                BackgroundWorker bgWorker;
                bgWorker = new BackgroundWorker();
                bgWorker.WorkerSupportsCancellation = true;
                bgWorker.DoWork += new DoWorkEventHandler(GetLocation);
                bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorker_RunWorkerCompleted);
                bgWorker.RunWorkerAsync(); 
            }
             Application.DoEvents();
            if (iTimercnt > 2) { 
            timer1.Enabled = false;
            }
          
            iTimercnt = iTimercnt + 1;
           
              _currentSite = detectedSiteID;
             
               if (_currentSite == 0 || detectedSiteID < 1 || _currentSite == 28 || _currentSite == 4)
            {
                timer1.Enabled = false;
                return;
            }
          
         
            var parms = new Dictionary<string, string>();
            switch (iTimercnt)
            {
                case 1:
                      lblChargers.Text = "Working...";
                    parms.Add("SiteID", _currentSite.ToString());
                    Application.DoEvents();
                    radGridViewChargers.DataSource = null;
                    try
                    {
                        radGridViewChargers.DataSource = ExecStoredProc("[prcGetCharger]", false, parms);
                    }
                    catch
                    {

                    }
                    radGridViewChargers.MasterTemplate.BestFitColumns();
                  
                    radPageView1.SelectedPage = PageCharger;
                    Application.DoEvents();
                          lblChargers.Text = "Chargers";
                    break;
                case 2:
                    radGridViewWorkstations.DataSource = null;
                    parms.Clear();
                    parms.Add("SiteID", _currentSite.ToString());
                    try
                    {
                        radGridViewWorkstations.DataSource = ExecStoredProc("[prcGetWorkstation]", false, parms);
                    }
                    catch
                    {

                    }
                    radGridViewWorkstations.MasterTemplate.BestFitColumns();
                  
                    radPageView1.SelectedPage = PageWorkStations;
                    Application.DoEvents();
                      lblWorkstations.Text = "Workstations";
                    break;
                case 3:
                    radGridViewBatteries.DataSource = null;
                    parms.Clear();
                    parms.Add("SiteID", _currentSite.ToString());
                    try
                    {
                        radGridViewBatteries.DataSource = ExecStoredProc("[prcGetBattery]", false, parms);
                    }
                    catch
                    {

                    }
                    radGridViewBatteries.MasterTemplate.BestFitColumns();
                   
                    radPageView1.SelectedPage = PageBattery;
                    Application.DoEvents();
                     lblBatteries.Text = "Batteries";
                     try
                     {
                       
                     }
                     catch (Exception ex)
                     {
                     if (lblUsername.Text == "arlow.farrell@enovatemedical.com" ) {
                         MessageBox.Show(ex.ToString());
                     }
                     }
                    break;
            }
        }

        private void tmrAlerts_Tick(object sender, EventArgs e)
        {
               var parms = new Dictionary<string, string>();
               Debug.WriteLine(iUserID.ToString());
                    parms.Add("Username", sUsername);
                    Application.DoEvents();

                    try
                    {
                        DataTable deskalerts = ExecStoredProc("[prcGetAppNotifications]", false, parms);
                        if (deskalerts.Rows.Count > 0)
                        {
                             Debug.WriteLine(deskalerts.Rows[0]["ContentText"].ToString());
                            foreach (DataRow dr in deskalerts.Rows){
                            tmrAlerts.Enabled = false;
                            DataRow dtResults = dr;
                            Debug.WriteLine(dr["CaptionText"].ToString());
                            radDesktopAlert1.CaptionText = dr["CaptionText"].ToString();
                            Debug.WriteLine(dr["ContentText"].ToString());
                            radDesktopAlert1.ContentText = dr["ContentText"].ToString();
                            radDesktopAlert1.Show();
                            Application.DoEvents();
                                }
                             parms.Add("bDelete", "1");
                            deskalerts = ExecStoredProc("[prcGetAppNotifications]", false, parms);
                        }
                    }
                    catch
                    {

                    }
        }

        private void lblPacketSerial_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this.Width.ToString());
        }

        private void lblWorkstations_Click(object sender, EventArgs e)
        {

        }

        private void lblChargers_Click(object sender, EventArgs e)
        {

        }

        private void lblSearch_Click(object sender, EventArgs e)
        {
                      DoSearch();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        /*--- end of SaveParams() -------------------------------------------------*/
        #endregion

        //This gives us the ability to resize the borderless from any borders instead of just the lower right corner
        protected override void WndProc(ref Message m)
        {
            const int wmNcHitTest = 0x84;
            const int htLeft = 10;
            const int htRight = 11;
            const int htTop = 12;
            const int htTopLeft = 13;
            const int htTopRight = 14;
            const int htBottom = 15;
            const int htBottomLeft = 16;
            const int htBottomRight = 17;

            if (m.Msg == wmNcHitTest)
            {
                int x = (int)(m.LParam.ToInt64() & 0xFFFF);
                int y = (int)((m.LParam.ToInt64() & 0xFFFF0000) >> 16);
                Point pt = PointToClient(new Point(x, y));
                Size clientSize = ClientSize;
                ///allow resize on the lower right corner
                if (pt.X >= clientSize.Width - 16 && pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htBottomLeft : htBottomRight);
                    return;
                }
                ///allow resize on the lower left corner
                if (pt.X <= 16 && pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htBottomRight : htBottomLeft);
                    return;
                }
                ///allow resize on the upper right corner
                if (pt.X <= 16 && pt.Y <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htTopRight : htTopLeft);
                    return;
                }
                ///allow resize on the upper left corner
                if (pt.X >= clientSize.Width - 16 && pt.Y <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htTopLeft : htTopRight);
                    return;
                }
                //allow resize on the top border
                //if (pt.Y <= 16 && clientSize.Height >= 16)
                //{
                //    m.Result = (IntPtr)(htTop);
                //    return;
                //}
                ///allow resize on the bottom border
                if (pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htBottom);
                    return;
                }
                ///allow resize on the left border
                if (pt.X <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htLeft);
                    return;
                }
                ///allow resize on the right border
                if (pt.X >= clientSize.Width - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htRight);
                    return;
                }
            }
            base.WndProc(ref m);
        }
        //***********************************************************
        //***********************************************************
        //This gives us the ability to drag the borderless form to a new location
        //public const int WM_NCLBUTTONDOWN = 0xA1;
        //public const int HT_CAPTION = 0x2;

        //[DllImportAttribute("user32.dll")]
        //public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        //[DllImportAttribute("user32.dll")]
        //public static extern bool ReleaseCapture();

        private void frmMain_MouseDown(object sender, MouseEventArgs e)
        {
            //ctrl-leftclick anywhere on the control to drag the form to a new location 
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }
        //***********************************************************
        //***********************************************************
        //This gives us the drop shadow behind the borderless form
        private const int CS_DROPSHADOW = 0x20000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }

        private void frmMain_Resize(object sender, EventArgs e)
        {
            if (this.Width < 1200)
            {
                int l = radDropDownSites.Left - radDropDownSites.Width;
                int maxl = groupBox1.Left - radDropDownSites.Left;
                radDropDownSites.Width = maxl - 2;
                txtPacketSerialNo.Width = maxl - 2;
                cmdPacketSearch.Left = groupBox1.Left - cmdPacketSearch.Width;
                lblSearch.Left = cmdPacketSearch.Left - lblSearch.Width - 10;
                lblSearch.Top = cmdPacketSearch.Top;
                btnAdmin.Left = groupBox1.Left - btnAdmin.Width - 20;
            }
            else
            {
                lblSearch.Top = txtPacketSerialNo.Top;
                lblSearch.Left = txtPacketSerialNo.Left + txtPacketSerialNo.Width + 10;
            }
        }
        private void ShutDown()
        {
             //this.radGridViewBatteries.SelectedCells[0].Value.ToString();
             var parms = new Dictionary<string, string>();

                  parms.Add("BatterySerialNumber", this.radGridViewPackets.CurrentRow.Cells["BatterySerialNumber"].Value.ToString());
       
            radGridViewPackets.DataSource = null;
            try
            {
               DataTable  dt2 = ExecStoredProc("[prcBatteryCycleSet]", false, parms);
                
            }
            catch
            {
                MessageBox.Show("No Packets found");
            }
        }

      

        private void btnSHutdown_Click_1(object sender, EventArgs e)
        {
            ShutDown();
        }

        private void cmdExportBatteries_Click_1(object sender, EventArgs e)
        {
            
            if (radGridViewBatteries.Rows.Count < 1)
            {
                MessageBox.Show("No data to export");
                return;
            }
            lblBatteriesExporting.Visible = true;
            Application.DoEvents();
            ExportToExcel(radGridViewBatteries);
            lblBatteriesExporting.Visible = false;
        }
        //**

    }
}