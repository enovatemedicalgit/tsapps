﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace RhythmField
{
    public partial class frmAdmin : Telerik.WinControls.UI.ShapedForm
    {
        string userSiteList = "";
        DataTable usertable = null;
        RadGridView RadGridviewSearch;
        public frmAdmin()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
              {
            for (int i=0; i < ddlSites1.CheckedItems.Count ;i++){
                userSiteList += ddlSites1.CheckedItems[i].Value.ToString();
                if( i != ddlSites1.CheckedItems.Count-1){
                   userSiteList =  userSiteList + ',';
            }
            }
        
        }
                catch (Exception ex)
              {
                  MessageBox.Show(" User added to all sites except " + ddlSites1.SelectedItems[ddlSites1.SelectedItems.Count - 1].Text.ToString());
                return;
                }
          
          
       
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@Username", txtUsernameAdd.Text },
                { "@FirstName", txtFirstNameAdd.Text},
                { "@LastName", txtLastNameAdd.Text},  // enovate123  Xyen2O4zJg0qBMPCEHQBTw==
               { "@Password", "Xyen2O4zJg0qBMPCEHQBTw=="},
               { "@SiteList",  userSiteList},
                { "@SiteID",  ddlSites1.CheckedItems[0].Value.ToString()}
            };
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcAddUsername", parameters);
            MessageBox.Show("Successfully Added User : " + txtUsernameAdd.Text);
        }
        public void loadDDLs()
        {
            try
            {
                ApiLayer2 api = new ApiLayer2();
                DataTable dt = new DataTable();
                DataTable dt2 = new DataTable();
                   DataTable dt3 = new DataTable();
                Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@IDSite", "4" } };
                  Dictionary<string, string> parameters2 = new Dictionary<string, string>() {
                { "@IDSite", "4" } };
                          Dictionary<string, string> parameters3 = new Dictionary<string, string>() {
                { "@IDSite", "4" } };
                dt = api.ExecSpApi("prcSitesSelect", parameters);
                if (dt.Rows.Count > 1) //Found Site for User
                {

                    ddlSites1.DataSource = dt;
                    ddlSites2.DataSource = dt;
                    ddlSites3.DataSource = dt;
                       ddlSites5.DataSource = dt;
                     ddlSitesUpdate.DataSource = dt;
                }
                 dt3 = api.ExecSpApi("prcCustomersSelect", new Dictionary<string, string>() { { "@IDCustomer", "0" }});
                if (dt3.Rows.Count > 1) //Found Site for User
                {
                    ddlCustomers.DataSource = dt3;
                    ddlCustomers2.DataSource = dt3;
                   

                }
                api = new ApiLayer2();
                dt2 = api.ExecSpApi("prcGetSalesReps", new Dictionary<string, string>() { });
                if (dt2.Rows.Count > 1) //Found Site for User
                {
                    ddlSalesReps.DataSource = dt2;
                }

                   var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

             usertable = new DataTable();

            
            ApiLayer2 api2 = new ApiLayer2();
         

            usertable = api2.ExecSpApi("[prcGetUserList]", parms);


            dgUsers.DataSource = usertable;

            dgUsers.DoubleClick += setUserTexts();
      
            }
            catch (Exception ex)
            {

            }
        }
        private void frmAdmin_Load(object sender, EventArgs e)
        {
            loadDDLs();
        }
     
        private void button1_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@Username", txtUsernameDelete.Text }
               
            };
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcUserDelete", parameters);
             MessageBox.Show("Successfully Deleted User : " + txtUsernameDelete.Text);
        }
        public void GetUserList()
        {
          var parms = new Dictionary<string, string>();
            // parms.Add("SiteID", radDropDownSites.SelectedValue.ToString());

             usertable = new DataTable();

            
            ApiLayer2 api = new ApiLayer2();
         

            usertable = api.ExecSpApi("[prcGetUserList]", parms);



             RadGridviewSearch = new RadGridView();
            RadGridviewSearch.Name = "UsersList" + DateTime.Now.Minute.ToString();


            RadGridviewSearch.Dock = DockStyle.Fill;
            RadGridviewSearch.AllowEditRow = false;
            RadGridviewSearch.AllowSearchRow = true;
            RadGridviewSearch.AllowMultiColumnSorting = true;
            RadGridviewSearch.EnableFiltering = true;
            RadGridviewSearch.DataSource = usertable;
            RadGridviewSearch.BestFitColumns();
            RadGridviewSearch.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;


            frmPopup fma = new frmPopup();
            fma.currentGrid = ReflectionHelper.Clone<RadGridView>(RadGridviewSearch);
            fma.WindowState = FormWindowState.Maximized;
            fma.StartPosition = FormStartPosition.CenterScreen;
            fma.Controls.Add(RadGridviewSearch);
          //  RadGridviewSearch.SelectedRows[0].ToString();

              
            fma.Show();
      
            
        }

        private  EventHandler setUserTexts()
        {
            if (dgUsers.SelectedRows.Count > 0)
            {
                txtUsernameAdd.Text = dgUsers.SelectedRows[0].Cells["Username"].Value.ToString();
                txtFirstNameAdd.Text = dgUsers.SelectedRows[0].Cells["UserFL"].Value.ToString().Split(' ')[0];
                txtLastNameAdd.Text = dgUsers.SelectedRows[0].Cells["UserFL"].Value.ToString().Split(' ')[1];
            }

           return null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string temps;
            txtSerialRecycle.Text = "'" + txtSerialRecycle.Text + "'";
            txtSerialRecycle.Text.Replace(",,",",");
           temps =  txtSerialRecycle.Text.Replace(",","','");

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SerialList",  temps }
               
            };

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcRecycleAssets", parameters);
            MessageBox.Show("Successfully Updated Serials : " + txtSerialRecycle.Text);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
             string temps;
         
               txtSerialsMove.Text = "'" + txtSerialsMove.Text + "'";
            temps =   txtSerialsMove.Text.Replace(System.Environment.NewLine, ",");
        temps =    temps.Replace("\r\n", ",").Replace("\n", ",").Replace("\r", ",");
          temps =   temps.Replace(",,",",");
           temps =  temps.Replace(",","','");
          temps =    temps.Replace(",''","");

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SerialList",  temps },
                { "@SiteID",  ddlSites2.SelectedValue.ToString() }
               
            };

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcAssetsMove", parameters);
            MessageBox.Show("Successfully Moved Serials : " + txtSerialsMove.Text + " to " + ddlSites2.SelectedText.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
                   
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@CustomerID",  ddlCustomers.SelectedValue.ToString() },
                { "@SiteID",  ddlSites3.SelectedValue.ToString() }
               
            };

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcSiteMove", parameters);
            MessageBox.Show("Successfully Moved Site : " + ddlSites3.SelectedText.ToString() + " under IDN " + ddlCustomers.SelectedText.ToString());
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@IDSite", ddlSites3.SelectedValue.ToString() }
               
            };
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcSiteDelete", parameters);
             MessageBox.Show("Successfully Deleted Site : " + ddlSites3.SelectedValue.ToString() );
             loadDDLs();
        }

        private void button7_Click(object sender, EventArgs e)
        {
             Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@IDCustomer", ddlCustomers.SelectedValue.ToString() }
               
            };
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcCustomerDelete", parameters);
             MessageBox.Show("Successfully Deleted Customer/IDN : " + ddlCustomers.SelectedValue.ToString() );
             loadDDLs();
        }

        private void button8_Click(object sender, EventArgs e)
        {
             Dictionary<string, string> parameters;
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();
             if (rbFacility.IsChecked == true)
             {
                 parameters = new Dictionary<string, string>() {
                { "@SiteName", txtFacilityIDNName.Text },
                { "@SiteDescription", txtFacilityIDNName.Text},
                { "@SfCustomerAcctID", txtSFCNum.Text},
                 { "@SfAcctOwnerId", ddlSalesReps.SelectedValue.ToString() },
                { "@SfAccountId", txtSFAccountID.Text },
                { "@CustomerID", ddlCustomers2.SelectedValue.ToString() }  
            
            };
                     dt = api.ExecSpApi("usp_SitesInsert", parameters);
                 
                 MessageBox.Show("Site " + txtFacilityIDNName.Text + " added to Pulse.");
                    loadDDLs();
             }
             else
             {
                 parameters = new Dictionary<string, string>() {
                { "@CustomerName", txtFacilityIDNName.Text },
                { "@SytelineCustomerNum", txtSFCNum.Text}
              
             };
                     dt = api.ExecSpApi("prcAddCustomer", parameters);
                     MessageBox.Show("Customer " + txtFacilityIDNName.Text + " added to Pulse.");
                     loadDDLs();
             }
    
        
        }

        private void rbIDN_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbIDN.IsChecked == true)
            {
                rbFacility.IsChecked = false;
                ddlCustomers2.Enabled = false;
            }
            else
            {
                ddlCustomers2.Enabled = true;
                     rbIDN.IsChecked = true;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
             Dictionary<string, string> parameters;
            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();
          
                 parameters = new Dictionary<string, string>() {
                { "@SiteName", txtFacilityIDNName.Text },
                { "@SiteDescription", txtFacilityIDNName.Text},
                { "@SfCustomerAcctID", txtSFCNum.Text},
                 { "@SfAcctOwnerId", ddlSalesReps.SelectedValue.ToString() },
                { "@SfAccountId", txtSFAccountID.Text },
                  { "@SiteId", ddlSitesUpdate.SelectedValue.ToString() }
                
            
            };
                     dt = api.ExecSpApi("usp_SitesUpdate", parameters);
                 
                 MessageBox.Show("Site " + txtFacilityIDNName.Text + " added to Pulse.");
                    loadDDLs();
         
    
        }

        private void button11_Click(object sender, EventArgs e)
        {
                      
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            string temps;
         
               txtAPsMove.Text = "'" + txtAPsMove.Text + "'";
            temps =   txtAPsMove.Text.Replace(System.Environment.NewLine, ",");
        temps =    temps.Replace("\r\n", ",").Replace("\n", ",").Replace("\r", ",");
          temps =   temps.Replace(",,",",");
           temps =  temps.Replace(",","','");
          temps =    temps.Replace(",''","");

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@APMACList",  temps },
                { "@UserID",  "56" },
                { "@SiteID",  ddlSites5.SelectedValue.ToString() }
               
            };

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcAPsMove", parameters);
            MessageBox.Show("Successfully Moved AP's : " + txtAPsMove.Text + " to " + ddlSites5.SelectedText.ToString());
        }

        private void rbFacility_ToggleStateChanged(object sender, Telerik.WinControls.UI.StateChangedEventArgs args)
        {
            if (rbFacility.IsChecked == true)
            {
                rbFacility.IsChecked = true;
                ddlCustomers2.Enabled = true;
            }
            else
            {
                ddlCustomers2.Enabled = false;
                     rbIDN.IsChecked = false;
            }
        }

        private void btnUserReport_Click(object sender, EventArgs e)
        {
            GetUserList();
        }

    }
}
