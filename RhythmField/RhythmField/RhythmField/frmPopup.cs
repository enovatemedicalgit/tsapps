﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.UI;
using Telerik.WinControls.Export;
using Telerik.WinControls.UI;
using Telerik.WinControls.Tools;
using Telerik.WinControls;
using Telerik.WinControls.Enumerations;
using Telerik.Windows;
using Telerik.WinControls.Themes;
using Telerik.Windows.Documents.Core;
using Telerik.Windows.Documents.Fixed;
using Telerik.Windows.Documents.Spreadsheet;
using System.Runtime.InteropServices;
using Telerik.Windows.Zip;
using Telerik.WinControls.Data;

using Telerik.WinControls.UI.Docking;
using Telerik.WinControls.UI.Export;
using Telerik.Windows.Documents.Spreadsheet.Expressions.Functions;
using Telerik.Windows.Documents.Spreadsheet.Model;


namespace RhythmField
{
    public partial class frmPopup : Form
    {
            public  RadGridView currentGrid  { get; set; } 
                SpreadExportRenderer exportRenderer = new SpreadExportRenderer();
        private GridViewSpreadExport spreadsheetExporter;

        public frmPopup()
        {
            InitializeComponent();
            btnExport.BringToFront();
            
        }

   
         public void ExportToExcel(RadGridView radGridExport)
        {
            string exportDialogFileName = "";
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.RestoreDirectory = true;

            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            dialog.Title = "Save Grid to Excel";


            string s = "Export.xls";

            radGridExport.ThemeName = "Office2010";
            spreadsheetExporter = new GridViewSpreadExport(radGridExport);

            spreadsheetExporter.ExportFormat = SpreadExportFormat.Xlsx;

            spreadsheetExporter.ExportVisualSettings = true;
            spreadsheetExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadsheetExporter.SheetMaxRows = Telerik.WinControls.UI.Export.ExcelMaxRows._65536;

            dialog.DefaultExt = ".xlsx";
            dialog.AddExtension = true;
            dialog.Filter = "Excel Files|*.xlsx";

            dialog.FileName = "";
            
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                s = dialog.FileName;
                exportDialogFileName = s;

            }
            else
            {
                //lblSearching.Visible = false;
                return;
            }

            spreadsheetExporter.ExportVisualSettings = true;
            try
            {
                spreadsheetExporter.RunExport(exportDialogFileName, exportRenderer);
            }
            catch
            {
                MessageBox.Show(dialog.FileName.ToString() + " is open by another process, saving as " + dialog.FileName + "1");
                spreadsheetExporter.RunExport(exportDialogFileName + "1", exportRenderer);
            }
            MessageBox.Show("Export Complete...");
        }

         private void cmdExportWorkstations_Click(object sender, EventArgs e)
         {
                 currentGrid.Visible = true;
            Application.DoEvents();

            ExportToExcel(currentGrid);
         }
    }
}
