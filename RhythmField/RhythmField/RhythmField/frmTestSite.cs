﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.Export;
using Telerik.Charting;

using Telerik.WinControls.UI;
using System.Text.RegularExpressions;
 

namespace RhythmField
{
    public partial class frmTestSite : Form
    {
        public string URL = "http://rhythm.myenovate.com/";
        private long _currentSite = 0;
        private string _currentSiteName = "";
        public DataTable dtTestResults;
        public DataTable dtTestResults2;
        public DataTable dtTestResults3;
        public int stepInterval = 5;
        public RadGridView currentGrid;
        public string[] dtColumns;
        Telerik.WinControls.Export.SpreadExportRenderer exportRenderer = new SpreadExportRenderer();
        private Telerik.WinControls.Export.GridViewSpreadExport spreadsheetExporter;
        public frmTestSite()
        {
            InitializeComponent();
        }
        private DataTable ExecStoredProc(string procName, bool isEng, Dictionary<string, string> paramsDictionary)
        {
            ApiLayer2 api = new ApiLayer2();
            if (isEng == true)
            {
                api.URL = "http://rhythm.myenovate.com/";
            }
            else
            {
                api.URL = URL;
            }
            DataTable dt = new DataTable();
            dt = api.ExecSpApi(procName, paramsDictionary);
            return dt;
        }
        private void frmTestSite_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'pulseDSTLxy.TestLogXY' table. You can move, or remove it, as needed.
            this.testLogXYTableAdapter.Fill(this.pulseDSTLxy.TestLogXY);

            var parms = new Dictionary<string, string>();
            parms.Add("IDSite", "4");
            comboSites.DataSource = ExecStoredProc("[prcSitesAndCustomersSelect]", false, parms);
            comboSites.ValueMember = "IDSite";
            comboSites.DisplayMember = "SiteDescription";
            setStepInterval();
            BuildCharts();
            getTestStatus();

            // comboSites.Sorted = true;


        }
        public void setStepInterval()
        {
            var parms2 = new Dictionary<string, string>();
            DataTable dtTestStatusResults = ExecStoredProc("[prcGetSystemSettings]", false, parms2);
            DataRow dtResults = dtTestStatusResults.Rows[0];
            string startDate = Convert.ToDateTime(dtResults["TestStartTime"]).ToString();
            if (24 >= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 540;
            }
            else if (24 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 240;
            }
            else if (12 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 120;
            }
            else if (6 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 45;
            }
            else if (3 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 30;
            }
            else if (1 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalHours)
            {
                stepInterval = 20;
            }
            else if (60 <= (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalMinutes)
            {
                stepInterval = 20;
            }
            else if ((System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalMinutes < 60 && (System.DateTime.UtcNow - Convert.ToDateTime(startDate)).TotalMinutes > 15)
            {
                stepInterval = 10;
            }
            else
            {
                stepInterval = 1;
            }
        }
        public void BuildCharts()
        {
            try
            {

                comboSites.Text = "Select Site";
                var parms2 = new Dictionary<string, string>();
                if (checkBox3.Checked == true)
                {
                    parms2.Add("ViewAll", "1");
                }
                this.radGridView1.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(radGridView1_ViewCellFormatting);
                this.radGridView1.AutoSizeRows = true;

                this.radGridView1.RowFormatting += new RowFormattingEventHandler(radGridView1_RowFormatting);
           

                dtTestResults = ExecStoredProc("[spTestSiteResults]", false, parms2);
                var parms3 = new Dictionary<string, string>();
                dtTestResults2 = ExecStoredProc("[spTestSiteResults]", false, parms3);
                var parms4 = new Dictionary<string, string>();
                dtTestResults3 = ExecStoredProc("[spTestSiteResults]", false, parms4);
                this.radGridView1.DataSource = dtTestResults;
                     //this.radGridView1.MasterTemplate.Columns["TestSiteID"].IsVisible = false;
                     //this.radGridView1.MasterTemplate.Columns["SiteReplicatedID"].IsVisible = false;


                //  comboSites.SelectedValue = dtTestResults.Rows[0]["TestSiteID"];
                radChartView1.DataSource = dtTestResults;
                radChartView2.DataSource = dtTestResults2;
                radChartView3.DataSource = dtTestResults3;
                //LineSeries lsworkstations = new LineSeries();
                //lsworkstations.ValueMember = "workstations";
                //lsworkstations.DataMember = "workstations";
                //lsworkstations.LegendTitle = "Workstations";
                //lsworkstations.DisplayMember = "workstations";
                //lsworkstations.CategoryMember = "LogDate";
                //lsworkstations.PointSize = new SizeF(4, 4);
                //lsworkstations.LinesToLabelsColor = Color.Green;
                //DateTimeContinuousAxis horizontalAxis = new DateTimeContinuousAxis();
                //horizontalAxis.LabelFitMode = AxisLabelFitMode.Rotate;
                //horizontalAxis.MajorStepUnit = Telerik.Charting.TimeInterval.Minute;
                //horizontalAxis.MajorStep = stepInterval;
                //horizontalAxis.LabelRotationAngle = 45;
                //horizontalAxis.LabelFormatProvider = new DateTimeFormatProvider();
                //lsworkstations.HorizontalAxis = horizontalAxis;

                //radChartView1.Series.Add(lsworkstations);

                //LineSeries lsChargers = new LineSeries();
                //lsChargers.ValueMember = "Chargers";
                //lsChargers.DataMember = "Chargers";
                //lsChargers.LegendTitle = "Chargers";
                //lsChargers.DisplayMember = "Chargers";
                //lsChargers.CategoryMember = "LogDate";
                //lsChargers.PointSize = new SizeF(4, 4);
                //radChartView1.Series.Add(lsChargers);
                //LineSeries lsBatteries = new LineSeries();
                //lsBatteries.ValueMember = "batteries";
                //lsBatteries.DataMember = "batteries";
                //lsBatteries.LegendTitle = "Batteries";
                //lsBatteries.DisplayMember = "Batteries";
                //lsBatteries.CategoryMember = "LogDate";
                //lsBatteries.PointSize = new SizeF(4, 4);
                //radChartView1.Series.Add(lsBatteries);
                //LineSeries lsworkstationsoriginal = new LineSeries();
                //lsworkstationsoriginal.ValueMember = "workstationsoriginal";
                //lsworkstationsoriginal.DataMember = "workstationsoriginal";
                //lsworkstationsoriginal.LegendTitle = "Workstations Original";
                //lsworkstationsoriginal.DisplayMember = "workstationsoriginal";
                //lsworkstationsoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lsworkstationsoriginal.CategoryMember = "LogDate";
                //lsworkstationsoriginal.LinesToLabelsColor = Color.Green;
                //lsworkstationsoriginal.PointSize = new SizeF(4, 4);
                //radChartView1.Series.Add(lsworkstationsoriginal);
                //LineSeries lschargersoriginal = new LineSeries();
                //lschargersoriginal.ValueMember = "chargersoriginal";
                //lschargersoriginal.DataMember = "chargersoriginal";
                //lschargersoriginal.LegendTitle = "Chargers Original";
                //lschargersoriginal.DisplayMember = "chargersoriginal";
                //lschargersoriginal.CategoryMember = "LogDate";
                //lschargersoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lschargersoriginal.PointSize = new SizeF(4, 4);
                //radChartView1.Series.Add(lschargersoriginal);
                //LineSeries lsbatteriesoriginal = new LineSeries();
                //lsbatteriesoriginal.ValueMember = "batteriesoriginal";
                //lsbatteriesoriginal.DataMember = "batteriesoriginal";
                //lsbatteriesoriginal.LegendTitle = "Batteries Original";
                //lsbatteriesoriginal.DisplayMember = "batteriesoriginal";
                //lsbatteriesoriginal.CategoryMember = "LogDate";
                //lsbatteriesoriginal.PointSize = new SizeF(4, 4);
                //lsbatteriesoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //radChartView1.Series.Add(lsbatteriesoriginal);
                ////-------------------------------------------------------------------------

                //LineSeries lsworkstationsallocated = new LineSeries();
                //lsworkstationsallocated.ValueMember = "workstationsallocated";
                //lsworkstationsallocated.DataMember = "workstationsallocated";
                //lsworkstationsallocated.LegendTitle = "Workstations Allocated";
                //lsworkstationsallocated.DisplayMember = "workstationsallocated";
                //lsworkstationsallocated.CategoryMember = "LogDate";
                //DateTimeContinuousAxis horizontalAxis2 = new DateTimeContinuousAxis();
                //horizontalAxis2.LabelFitMode = AxisLabelFitMode.Rotate;
                //horizontalAxis2.MajorStepUnit = Telerik.Charting.TimeInterval.Minute;
                //horizontalAxis2.MajorStep = stepInterval;
                //horizontalAxis2.LabelRotationAngle = 45;
                //horizontalAxis2.LabelFormatProvider = new DateTimeFormatProvider();
                //lsworkstationsallocated.HorizontalAxis = horizontalAxis2;
                //lsworkstationsallocated.PointSize = new SizeF(4, 4);
                //radChartView2.Series.Add(lsworkstationsallocated);

                //LineSeries lschargersallocated = new LineSeries();
                //lschargersallocated.ValueMember = "chargersallocated";
                //lschargersallocated.DataMember = "chargersallocated";
                //lschargersallocated.LegendTitle = "Chargers Allocated";
                //lschargersallocated.DisplayMember = "chargersallocated";
                //lschargersallocated.CategoryMember = "LogDate";
                //lschargersallocated.PointSize = new SizeF(4, 4);
                //radChartView2.Series.Add(lschargersallocated);

                //LineSeries lsworkstationsallocatedoriginal = new LineSeries();
                //lsworkstationsallocatedoriginal.ValueMember = "workstationsallocatedoriginal";
                //lsworkstationsallocatedoriginal.DataMember = "workstationsallocatedoriginal";
                //lsworkstationsallocatedoriginal.LegendTitle = "Workstations Allocated Original";
                //lsworkstationsallocatedoriginal.DisplayMember = "workstationsallocatedoriginal";
                //lsworkstationsallocatedoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lsworkstationsallocatedoriginal.PointSize = new SizeF(4, 4);
                //lsworkstationsallocatedoriginal.CategoryMember = "LogDate";

                //radChartView2.Series.Add(lsworkstationsallocatedoriginal);

                //LineSeries lschargersallocatedoriginal = new LineSeries();
                //lschargersallocatedoriginal.ValueMember = "chargersallocatedoriginal";
                //lschargersallocatedoriginal.DataMember = "chargersallocatedoriginal";
                //lschargersallocatedoriginal.LegendTitle = "Chargers Allocated Original";
                //lschargersallocatedoriginal.DisplayMember = "chargersallocatedoriginal";
                //lschargersallocatedoriginal.CategoryMember = "LogDate";
                //lschargersallocatedoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lschargersallocatedoriginal.PointSize = new SizeF(4, 4);
                //radChartView2.Series.Add(lschargersallocatedoriginal);

                ////-----------------------------------------------------------------------------------------

                //LineSeries lsworkstationsallocatedDepartment = new LineSeries();
                //lsworkstationsallocatedDepartment.ValueMember = "workstationsallocatedDepartment";
                //lsworkstationsallocatedDepartment.DataMember = "workstationsallocatedDepartment";
                //lsworkstationsallocatedDepartment.LegendTitle = "Workstations Allocated Department";
                //lsworkstationsallocatedDepartment.DisplayMember = "workstationsallocatedDepartment";
                //lsworkstationsallocatedDepartment.CategoryMember = "LogDate";
                //lsworkstationsallocatedDepartment.PointSize = new SizeF(4, 4);
                //DateTimeContinuousAxis horizontalAxis3 = new DateTimeContinuousAxis();
                //horizontalAxis3.LabelFitMode = AxisLabelFitMode.Rotate;
                //horizontalAxis3.MajorStepUnit = Telerik.Charting.TimeInterval.Minute;
                //horizontalAxis3.MajorStep = stepInterval;
                //horizontalAxis3.LabelRotationAngle = 45;
                //horizontalAxis3.LabelFormatProvider = new DateTimeFormatProvider();
                //lsworkstationsallocatedDepartment.HorizontalAxis = horizontalAxis3;
                //radChartView3.Series.Add(lsworkstationsallocatedDepartment);
                //LineSeries lschargersallocatedDepartment = new LineSeries();
                //lschargersallocatedDepartment.ValueMember = "chargersallocatedDepartment";
                //lschargersallocatedDepartment.DataMember = "chargersallocatedDepartment";
                //lschargersallocatedDepartment.LegendTitle = "Chargers Allocated Department";
                //lschargersallocatedDepartment.DisplayMember = "chargersallocatedDepartment";
                //lschargersallocatedDepartment.CategoryMember = "LogDate";
                //lschargersallocatedDepartment.PointSize = new SizeF(4, 4);
                //radChartView3.Series.Add(lschargersallocatedDepartment);

                //LineSeries lsworkstationsallocatedDepartmentoriginal = new LineSeries();
                //lsworkstationsallocatedDepartmentoriginal.ValueMember = "workstationsallocatedDepartmentoriginal";
                //lsworkstationsallocatedDepartmentoriginal.DataMember = "workstationsallocatedDepartmentoriginal";
                //lsworkstationsallocatedDepartmentoriginal.LegendTitle = "Workstations Allocated Department Original";
                //lsworkstationsallocatedDepartmentoriginal.DisplayMember = "workstationsallocatedDepartmentoriginal";
                //lsworkstationsallocatedDepartmentoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lsworkstationsallocatedDepartmentoriginal.CategoryMember = "LogDate";
                //lsworkstationsallocatedDepartmentoriginal.PointSize = new SizeF(4, 4);
                //radChartView3.Series.Add(lsworkstationsallocatedDepartmentoriginal);
                //LineSeries lschargersallocatedDepartmentoriginal = new LineSeries();
                //lschargersallocatedDepartmentoriginal.ValueMember = "chargersallocatedDepartmentoriginal";
                //lschargersallocatedDepartmentoriginal.DataMember = "chargersallocatedDepartmentoriginal";
                //lschargersallocatedDepartmentoriginal.LegendTitle = "Chargers Allocated Department Original";
                //lschargersallocatedDepartmentoriginal.DisplayMember = "chargersallocatedDepartmentoriginal";
                //lschargersallocatedDepartmentoriginal.CategoryMember = "LogDate";
                //lschargersallocatedDepartmentoriginal.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash;
                //lschargersallocatedDepartmentoriginal.PointSize = new SizeF(4, 4);
                //radChartView3.Series.Add(lschargersallocatedDepartmentoriginal);
                //LineSeries lsbatteriesallocatedDepartmentoriginal = new LineSeries();
                //Dictionary<int, CartesianSeries> seriesnames = new Dictionary<int, CartesianSeries>();



                dtColumns = new string[] { "workstations", "workstationsoriginal" , "chargers", "chargersoriginal", "batteries","batteriesoriginal" };
                
                GenerateSeries("Line", dtColumns, "radChartView5");
         
                dtColumns = new string[] { "WorkstationsAllocated", "WorkstationsAllocatedOriginal", "ChargersAllocated",  "ChargersAllocatedOriginal" };
                GenerateSeries2("Line", dtColumns, "radChartView4");

                dtColumns = new string[] { "WorkstationsAllocatedDepartment", "WorkstationsAllocatedDepartmentOriginal", "ChargersAllocatedDepartment",  "ChargersAllocatedDepartmentOriginal" };
                GenerateSeries3("Line", dtColumns, "radChartView6");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
        private void GenerateSeries(string seriesType, string[] columnname, string chartControlName = "", System.Drawing.Drawing2D.DashStyle linestyle = System.Drawing.Drawing2D.DashStyle.Dash) { DateTimeContinuousAxis horizontalAxis = new DateTimeContinuousAxis(); LinearAxis verticalAxis = new LinearAxis(); verticalAxis.AxisType = AxisType.Second; for (int i = 0; i < dtColumns.Length; i++) { CartesianSeries series; if (seriesType == "Area") { series = new AreaSeries(); } else { series = new LineSeries(); } series.PointSize = new SizeF(2, 2); series.HorizontalAxis = horizontalAxis; series.VerticalAxis = verticalAxis; series.BorderWidth = 3; series.CategoryMember = "LogDate"; if (dtColumns[i].ToString().Contains("original") || dtColumns[i].ToString().Contains("Original")) { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash; } else { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid; } if (dtColumns[i].ToString().Contains("workstation") || dtColumns[i].ToString().Contains("Workstation")) { series.BorderColor = Color.Red; series.ForeColor = Color.Red; series.BackColor = Color.Red; series.SyncLinesToLabelsColor = true; } else if (dtColumns[i].ToString().Contains("chargers") || dtColumns[i].ToString().Contains("Chargers")) { series.BorderColor = Color.Green; series.ForeColor = Color.Green; series.BackColor = Color.Green; series.SyncLinesToLabelsColor = true; } else { series.BorderColor = Color.Black; series.ForeColor = Color.Black; series.BackColor = Color.Black; series.SyncLinesToLabelsColor = true; } series.ValueMember = dtColumns[i].ToString(); series.LegendTitle = SplitCamelCase(dtColumns[i].ToString()); series.DataSource = dtTestResults; horizontalAxis.MaximumTicks = 10000; if (dtTestResults.Rows.Count > 60) { horizontalAxis.MajorStepUnit = Telerik.Charting.TimeInterval.Hour; horizontalAxis.MajorStep = 1; } horizontalAxis.LabelRotationAngle = 45; horizontalAxis.LabelFormatProvider = new DateTimeFormatProvider(); series.HorizontalAxis.LabelFitMode = AxisLabelFitMode.Rotate; series.CombineMode = ChartSeriesCombineMode.Cluster; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).ShowLegend = true; series.IsVisibleInLegend = true; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).Series.Add(series); } }
        private void GenerateSeries2(string seriesType, string[] columnname, string chartControlName = "", System.Drawing.Drawing2D.DashStyle linestyle = System.Drawing.Drawing2D.DashStyle.Dash) { DateTimeContinuousAxis horizontalAxis = new DateTimeContinuousAxis(); LinearAxis verticalAxis = new LinearAxis(); verticalAxis.AxisType = AxisType.Second; for (int i = 0; i < dtColumns.Length; i++) { CartesianSeries series; if (seriesType == "Area") { series = new AreaSeries(); } else { series = new LineSeries(); } series.PointSize = new SizeF(2, 2); series.HorizontalAxis = horizontalAxis; series.VerticalAxis = verticalAxis; series.BorderWidth = 3; series.CategoryMember = "LogDate"; if (dtColumns[i].ToString().Contains("original") || dtColumns[i].ToString().Contains("Original")) { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash; } else { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid; } if (dtColumns[i].ToString().Contains("workstation") || dtColumns[i].ToString().Contains("Workstation")) { series.BorderColor = Color.Red; series.ForeColor = Color.Red; series.BackColor = Color.Red; series.SyncLinesToLabelsColor = true; } else if (dtColumns[i].ToString().Contains("chargers") || dtColumns[i].ToString().Contains("Chargers")) { series.BorderColor = Color.Green; series.ForeColor = Color.Green; series.BackColor = Color.Green; series.SyncLinesToLabelsColor = true; } else { series.BorderColor = Color.Black; series.ForeColor = Color.Black; series.BackColor = Color.Black; series.SyncLinesToLabelsColor = true; } series.ValueMember = dtColumns[i].ToString(); series.LegendTitle = SplitCamelCase(dtColumns[i].ToString()); series.DataSource = dtTestResults2; horizontalAxis.MaximumTicks = 10000; if (dtTestResults2.Rows.Count > 60) { horizontalAxis.MajorStepUnit = Telerik.Charting.TimeInterval.Hour; horizontalAxis.MajorStep = 1; } horizontalAxis.LabelRotationAngle = 45; horizontalAxis.LabelFormatProvider = new DateTimeFormatProvider(); series.HorizontalAxis.LabelFitMode = AxisLabelFitMode.Rotate; series.CombineMode = ChartSeriesCombineMode.Cluster; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).ShowLegend = true; series.IsVisibleInLegend = true; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).Series.Add(series); } }
        private void GenerateSeries3(string seriesType, string[] columnname, string chartControlName = "", System.Drawing.Drawing2D.DashStyle linestyle = System.Drawing.Drawing2D.DashStyle.Dash) { DateTimeContinuousAxis horizontalAxis = new DateTimeContinuousAxis(); LinearAxis verticalAxis = new LinearAxis(); verticalAxis.AxisType = AxisType.Second; for (int i = 0; i < dtColumns.Length; i++) { CartesianSeries series; if (seriesType == "Area") { series = new AreaSeries(); } else { series = new LineSeries(); } series.PointSize = new SizeF(2, 2); series.HorizontalAxis = horizontalAxis; series.VerticalAxis = verticalAxis; series.BorderWidth = 3; series.CategoryMember = "LogDate"; if (dtColumns[i].ToString().Contains("original") || dtColumns[i].ToString().Contains("Original")) { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Dash; } else { series.BorderDashStyle = System.Drawing.Drawing2D.DashStyle.Solid; } if (dtColumns[i].ToString().Contains("workstation") || dtColumns[i].ToString().Contains("Workstation")) { series.BorderColor = Color.Red; series.ForeColor = Color.Red; series.BackColor = Color.Red; series.SyncLinesToLabelsColor = true; } else if (dtColumns[i].ToString().Contains("chargers") || dtColumns[i].ToString().Contains("Chargers")) { series.BorderColor = Color.Green; series.ForeColor = Color.Green; series.BackColor = Color.Green; series.SyncLinesToLabelsColor = true; } else { series.BorderColor = Color.Black; series.ForeColor = Color.Black; series.BackColor = Color.Black; series.SyncLinesToLabelsColor = true; } series.ValueMember = dtColumns[i].ToString(); series.LegendTitle = SplitCamelCase(dtColumns[i].ToString()); series.DataSource = dtTestResults3; ; horizontalAxis.MaximumTicks = 10000; if (dtTestResults3.Rows.Count > 60) { horizontalAxis.MajorStepUnit = Telerik.Charting.TimeInterval.Hour; horizontalAxis.MajorStep = 1; } horizontalAxis.LabelRotationAngle = 45; horizontalAxis.LabelFormatProvider = new DateTimeFormatProvider(); series.HorizontalAxis.LabelFitMode = AxisLabelFitMode.Rotate; series.CombineMode = ChartSeriesCombineMode.Cluster; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).ShowLegend = true; series.IsVisibleInLegend = true; ((RadChartView)this.Controls.Find(chartControlName, true)[0]).Series.Add(series); } }
        private void btnRunTest_Click(object sender, EventArgs e)
        {
          
        }




        void radGridView1_ViewCellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            if (e.CellElement is GridHeaderCellElement)
            {
                e.CellElement.TextWrap = true;
               // e.Column.HeaderText = SplitCamelCase(e.Column.HeaderText.ToString());

            }
        }
        void radGridView1_RowFormatting(object sender, RowFormattingEventArgs e)
        {
            e.RowElement.RowInfo.MaxHeight = e.RowElement.RowInfo.MinHeight = 25;
        }
        private void iconButton1_Click(object sender, EventArgs e)
        {
            
        }
        public void StartTest()
        {
            try
            {
                int iLeaveAPs = 0;
                int iRemoveChargers = 0;
                int iRemoveManualFlag = 0;
                if (checkBox1.Checked == true)
                {
                    iLeaveAPs = 1;
                }
                if (checkBox2.Checked == true)
                {
                    iRemoveChargers = 1;
                }
                if (checkBox4.Checked == true)
                {
                    iRemoveManualFlag = 1;
                }
                var parms2 = new Dictionary<string, string>();
                parms2.Add("SiteID", comboSites.SelectedValue.ToString());
                parms2.Add("RemoveChargers", iRemoveChargers.ToString());
                parms2.Add("LeaveAPs", iLeaveAPs.ToString());
                parms2.Add("RemoveManualFlag", iRemoveManualFlag.ToString());
                DataTable dtStoppeDataTable = ExecStoredProc("[prcSiteCommissionSeedTest]", false, parms2);
                MessageBox.Show("Site Test Successfully Started");
            }
            catch
            {
                MessageBox.Show("Site Test Failed");
            }
        }
        public void getTestStatus()
        {
            try
            {

                var parms2 = new Dictionary<string, string>();
                DataTable dtTestStatusResults = ExecStoredProc("[prcGetSystemSettings]", false, parms2);

                DataRow dtResults = dtTestStatusResults.Rows[0];

                if (dtTestStatusResults.Rows[0]["TestMode"].ToString() == "1")
                {
                    btnStartTest.Enabled = false;
                    label4.ForeColor = Color.Gainsboro;
                    btnStartTest.InActiveColor = Color.Gainsboro;
                    iconButton1.Enabled = true;
                    label5.ForeColor = Color.Black;
                    iconButton1.InActiveColor = Color.Red;

                    groupBox1.Text = "System Status: Testing";
                    ibStatus.IconType = FontAwesomeIcons.IconType.CheckCircle;
                    ibStatus.InActiveColor = Color.Green;
                    txtSiteName.Text = " " + dtResults["TestSiteName"].ToString();
                    txtTestStartDate.Text = "Started: " + Convert.ToDateTime(dtResults["TestStartTime"]).ToShortDateString().ToString();
                    if (dtTestResults.Rows.Count < 2) { return; }
                    decimal totalActive = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["workstations"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["chargers"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["batteries"].ToString()));
                    decimal totalActiveGoal = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["workstationsoriginal"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["chargersoriginal"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["batteriesoriginal"].ToString()));
                    decimal totalAllocated = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["WorkstationsAllocated"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["ChargersAllocated"].ToString()) );
                    decimal totalAllocatedGoal = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["WorkstationsAllocatedOriginal"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["ChargersAllocatedOriginal"].ToString()) );
                    decimal totalAllocatedDepartment = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["WorkstationsAllocatedDepartment"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["ChargersAllocatedDepartment"].ToString()));
                    decimal totalAllocatedGoalDepartment = Convert.ToInt32(Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["WorkstationsAllocatedDepartmentOriginal"].ToString()) + Convert.ToDecimal(dtTestResults.Rows[dtTestResults.Rows.Count - 1]["ChargersAllocatedDepartmentOriginal"].ToString()) );
                    if (totalAllocated > totalAllocatedDepartment)
                    {
                        txtPercentAllocated.Text = Convert.ToString(Convert.ToInt32((totalAllocated / totalAllocatedGoal) * 100)) + "% (" + totalAllocated.ToString() + " Allocated " + "of " + totalAllocatedGoal.ToString() + ")";
                    }
                    else
                    {
                        txtPercentAllocated.Text = Convert.ToString(Convert.ToInt32((totalAllocatedDepartment / totalAllocatedGoalDepartment) * 100)) + "% (" + totalAllocatedDepartment.ToString() + " Allocated " + "of " + totalAllocatedGoalDepartment.ToString() + ")";

                    }
                    txtPercentActive.Text = Convert.ToString(Convert.ToInt32((totalActive / totalActiveGoal) * 100)) + "% (" + totalActive.ToString() + " assigned to site " + "of " + totalActiveGoal.ToString() + ")";

                }

                else
                {
                    groupBox1.Text = "System Status: Inactive";
                    ibStatus.IconType = FontAwesomeIcons.IconType.PowerOff;
                    ibStatus.InActiveColor = Color.Red;
                    txtSiteName.Text = "Not Available";
                    txtTestStartDate.Text = "";
                     this.txtPercentActive.Text = "";
            this.txtPercentAllocated.Text = "";
            txtTestStartDate.Text = "";
                    btnStartTest.Enabled = true;
                    btnStartTest.InActiveColor = Color.ForestGreen;
                    label4.ForeColor = Color.Black;
                    iconButton1.Enabled = false;
                    iconButton1.InActiveColor = Color.Gainsboro;
                    label5.ForeColor = Color.Gainsboro;
                }

                //SiteReplicatedID,
                //SnapshotInterval,
                //TestMode,
                //TestNumHours,
                //TestSiteID,
                //TestSiteLastDuplication,
                //TestStartTime
            }
            catch
            {

            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            var parms2 = new Dictionary<string, string>();
            stepInterval = Convert.ToInt32(txtStepInterval.Text);
            if (checkBox3.Checked == true)
            {
                parms2.Add("ViewAll", "1");
            }
            tabControl1.Invalidate(true);
            tabControl1.Refresh();
            dtTestResults = null;
            dtTestResults2 = null;
            dtTestResults3 = null;
            dtTestResults = ExecStoredProc("[spTestSiteResults]", false, parms2);
            var parms3 = new Dictionary<string, string>();
            dtTestResults2 = ExecStoredProc("[spTestSiteResults]", false, parms3);
            var parms4 = new Dictionary<string, string>();
            dtTestResults3 = ExecStoredProc("[spTestSiteResults]", false, parms4);
            getTestStatus();


            this.radChartView1.DataBindings.Clear();
            this.radGridView1.DataSource = dtTestResults;
            


            //radChartView1.Series.Clear();
            //radChartView2.Series.Clear();
            //radChartView3.Series.Clear();
            //  BuildCharts();
            tabControl1.Invalidate();
            tabControl1.Refresh();

 



        }

        private void button1_Click(object sender, EventArgs e)
        {

        }




        public void ExportToExcel(RadGridView radGridExport)
        {
            string exportDialogFileName = "";
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.RestoreDirectory = true;

            dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            dialog.Title = "Save Grid to Excel";


            string s = "Export.xls";

            radGridExport.ThemeName = "Office2010";
            spreadsheetExporter = new GridViewSpreadExport(radGridExport);

            spreadsheetExporter.ExportFormat = SpreadExportFormat.Xlsx;

            spreadsheetExporter.ExportVisualSettings = true;
            spreadsheetExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
            spreadsheetExporter.SheetMaxRows = Telerik.WinControls.UI.Export.ExcelMaxRows._65536;

            dialog.DefaultExt = ".xlsx";
            dialog.AddExtension = true;
            dialog.Filter = "Excel Files|*.xlsx";
            dialog.FileName = "SiteSeedTest";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                s = dialog.FileName;
                exportDialogFileName = s;

            }
            else
            {
                //lblSearching.Visible = false;
                return;
            }

            spreadsheetExporter.ExportVisualSettings = true;
            try
            {
                spreadsheetExporter.RunExport(exportDialogFileName, exportRenderer);
            }
            catch
            {
                MessageBox.Show(dialog.FileName.ToString() + " is open by another process, saving as " + "1" + dialog.FileName);
                spreadsheetExporter.RunExport("1" + dialog.FileName, exportRenderer);
            }
            MessageBox.Show("Export Complete...");
        }


        private void chartControl1_Click(object sender, EventArgs e)
        {

        }
        public string SplitCamelCase(string str)
        {
            if (str == "TestSiteID" || str == "LogDate")
            {
                return str;
            }
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            ).Replace("orig", " Orig").Replace("work","Work").Replace("char","Char").Replace("batt","Batt");
        }



        private Telerik.WinControls.UI.ChartSeries GetBarSeries(string p)
        {
            foreach (Telerik.WinControls.UI.ChartSeries s in this.radChartView4.Series)
            {
                if (s.Name == p)
                {
                    return s;
                }
            }

            return null;
        }

        private void tabControl1_Selected(object sender, TabControlEventArgs e)
        {

            timer1.Interval = 30000;
        }

        private void tabControl1_Deselected(object sender, TabControlEventArgs e)
        {
            timer1.Interval = 30000;
        }

        private void iconButton2_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog dialog = new SaveFileDialog();
                dialog.RestoreDirectory = true;

                dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dialog.Title = "Save Grid to Excel";


                string s = "Export.xls";

                radGridView1.ThemeName = "Office2010";
                spreadsheetExporter = new GridViewSpreadExport(radGridView1);

                spreadsheetExporter.ExportFormat = SpreadExportFormat.Xlsx;

                spreadsheetExporter.ExportVisualSettings = true;
                spreadsheetExporter.PagingExportOption = Telerik.WinControls.UI.Export.PagingExportOption.AllPages;
                spreadsheetExporter.SheetMaxRows = Telerik.WinControls.UI.Export.ExcelMaxRows._65536;

                dialog.DefaultExt = ".xlsx";
                dialog.AddExtension = true;
                dialog.Filter = "Excel Files|*.xlsx";
                dialog.Title = "Save Charts as PNG";

                currentGrid = radGridView1;
                ExportToExcel(currentGrid);

                string exportDialogFileName = "";
                SaveFileDialog dialog2 = new SaveFileDialog();
                dialog2.RestoreDirectory = true;

                dialog2.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                dialog2.Title = "Save Graphs";

                dialog2.DefaultExt = ".png";
                dialog2.AddExtension = true;
                dialog2.Filter = "PNG Files|*.PNG";
                dialog2.FileName = "SiteSeedTest";
                if (dialog2.ShowDialog() == DialogResult.OK)
                {
                    s = dialog2.FileName;
                    exportDialogFileName = s;
                    this.radChartView4.ExportToImage(dialog2.FileName, this.radChartView4.Size, System.Drawing.Imaging.ImageFormat.Png);
                    this.radChartView5.ExportToImage(dialog2.FileName, this.radChartView5.Size, System.Drawing.Imaging.ImageFormat.Png);
                    this.radChartView6.ExportToImage(dialog2.FileName, this.radChartView6.Size, System.Drawing.Imaging.ImageFormat.Png);
                    MessageBox.Show("Saved all three Charts");
                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }

        private void radChartView6_DoubleClick(object sender, EventArgs e)
        {
            if (radChartView6.Visible == false)
            {
                radChartView6.Visible = true;
                radChartView4.Visible = false;
            }
            else
            {
                radChartView4.Visible = true;
                radChartView6.Visible = false;
            }

        }

        private void radChartView4_DoubleClick(object sender, EventArgs e)
        {
            if (radChartView6.Visible == false)
            {
                radChartView6.Visible = true;

            }
            else
            {

                radChartView6.Visible = false;
            }


        }

        private void btnStopTest_Click(object sender, EventArgs e)
        {
            StopTest();
        }

        private void StopTest()
        {
            var parms2 = new Dictionary<string, string>();
            DataTable dtStoppeDataTable = ExecStoredProc("[spStopTest]", false, parms2);
            this.txtPercentActive.Text = "";
            this.txtPercentAllocated.Text = "";
            txtTestStartDate.Text = "";


            MessageBox.Show("Site Test Stopped");
        }

        private void iconButton1_Click_1(object sender, EventArgs e)
        {
            StopTest();
        }

        private void btnStartTest_Click(object sender, EventArgs e)
        {
            StartTest();
        }
        private void LegendElement_VisualItemCreating(object sender, LegendItemElementCreatingEventArgs e)
{
    e.ItemElement = new CustomLegendItemElement(e.LegendItem);
    e.ItemElement.Text = "TESTING";
}


    }
}
public class DateTimeFormatProvider : IFormatProvider, ICustomFormatter
{
    public object GetFormat(Type formatType)
    {
        return this;
    }
    public string Format(string format, object arg, IFormatProvider formatProvider)
    {
        DateTime val = (DateTime)arg;
        val.AddHours(1);
        val = val.ToLocalTime();
        if (val.Hour == 0)
        {
            return val.DayOfWeek.ToString();
        }
        else
        {
            return val.ToString("ddd hh:mm tt");
        }
    }
}
public class MyFormatProvider : IFormatProvider, ICustomFormatter
{
    public object GetFormat(Type formatType)
    {
        return this;
    }
    public string Format(string format, object arg, IFormatProvider formatProvider)
    {
        string s = arg.ToString();
        switch (s)
        {
            case "0":
                return "0 seconds";
            case "30":
                return "1/2 min";
            case "60":
                return "1 min";
            case "90":
                return "90 seconds";
        }
        return null;
    }
}

public class CustomLegendItemElement : LegendItemElement
{
    public CustomLegendItemElement(LegendItem item)
        : base(item)
    {
        //this.Children.Remove(this.MarkerElement);
        //this.TitleElement.DrawFill = true;
        //this.TitleElement.DrawBorder = true;
        //this.StretchHorizontally = true;
     

    }
    protected override void Synchronize()
    {
        base.Synchronize();
        this.SyncVisualStyleProperties(this.LegendItem.Element, this.TitleElement);
        this.TitleElement.ForeColor = Color.Black;
    }
}
