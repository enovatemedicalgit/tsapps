﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;


namespace RhythmField
{
    public partial class frmLoginToPulse : Telerik.WinControls.UI.ShapedForm
    {
        private static string mInternalSecret = "C0mp@ssion";
        public string siteID { get; set; }
     
        public string Fname { get; set; }
        public string SiteName { get; set; }
        public string iUserID { get; set; }
              public string iUserType { get; set; }
        public DataTable arrSites { get; set; }
        public DataTable arrDepartments { get; set; }
        public string assetSerial { get; set; }
         public string sURL { get; set; }
        public bool RememberMe { get; set; }
        public  string Username
        {
            get { return txtUsername.Text; }
            set { txtUsername.Text = value; }
        }
        public  string Password
        {
            get { return txtPassword.Text; }
            set { txtPassword.Text = value; }
        }


        public frmLoginToPulse()
        {
            InitializeComponent();
            //if (RememberMe == true)
            //{
           txtUsername.Text = Username;
             txtPassword.Text = Password;
            cbRememberMe.Checked = true;
            var asForm = System.Windows.Automation.AutomationElement.FromHandle(this.Handle); 
            //
        }

        private void radButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void radButton1_Click(object sender, EventArgs e)
        {
            if (txtUsername.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter UserName");
                return;
            }
            if (txtPassword.Text.Trim() == "")
            {
                MessageBox.Show("Please Enter Password");
                return;
            }
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@username", txtUsername.Text },
                { "@password", EncryptString(txtPassword.Text) }
            };
            ApiLayer2 api = new ApiLayer2();
            api.URL = sURL;
            DataTable dt = new DataTable();
            dt = api.ExecSpApi("prcGetUser", parameters);
            if (dt.Rows.Count > 0 ) 
            {
                Fname = dt.Rows[0]["FirstName"].ToString();
                siteID = dt.Rows[0]["IDSite"].ToString();
              iUserType = dt.Rows[0]["UserTypeID"].ToString();
               iUserID = dt.Rows[0]["IDUser"].ToString();
               if ((dt.Rows[0]["IDSite"].ToString() == "1502" || Convert.ToInt32(iUserType) >= 4))
               { }
               else {    MessageBox.Show("Login Failed - Username/Password Invalid.");
                Environment.Exit(0);
               }
                parameters = new Dictionary<string, string>() {
                { "@IDSite", siteID } };

                dt = api.ExecSpApi("prcSitesSelect",  parameters);
                if (dt.Rows.Count > 1) //Found Site for User
                {
                    siteID = dt.Rows[0]["IDSite"].ToString();
                    SiteName = dt.Rows[0]["SiteName"].ToString();
                    arrSites = dt;
                    string siteListString = "";
                    RememberMe = cbRememberMe.Checked;
                    if (RememberMe == true)
                    {
                        Username = txtUsername.Text;
                        Password = txtPassword.Text;
                    }
                    if (dt.Rows != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            siteListString += dt.Rows[i]["SiteName"].ToString() + " and ";
                        }
                        siteListString = siteListString.Remove(siteListString.Length - 4, 4);
                        if (dt.Rows.Count > 20)
                        {
                            siteListString = "all Sites; you are an Enovate Employee.";
                        }
                    }
                    

                }
                else if (dt.Rows.Count > 0) //Found Site For User
                {
                    siteID = dt.Rows[0]["IDSite"].ToString();
                    SiteName = dt.Rows[0]["SiteName"].ToString();

                    arrSites = dt;
                    RememberMe = cbRememberMe.Checked;
                     if (RememberMe == true)
                     {
                         Username = txtUsername.Text;
                         Password = txtPassword.Text;
                     }
               

                }
            }
            else
            {
                MessageBox.Show("Login Failed - Username/Password Invalid or you are not an Enovate Employee.");
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        public static string EncryptString(string sIn)
        {
            //string x = Crypto.DecryptStringAES("mTB4TEOMZd2JIvr0M2WTEg==", mInternalSecret);
            return Crypto.EncryptStringAES(sIn, mInternalSecret);
        }

        private void radButton3_Click(object sender, EventArgs e)
        {
              Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@username", txtUsername.Text },
                { "@password", EncryptString(txtPassword.Text) }
            };
            ApiLayer2 api = new ApiLayer2();
            api.URL = sURL;
            DataTable dt = new DataTable();
            dt = api.ExecSpApi("prcGetUser", parameters);
            if (dt.Rows.Count > 0)
            { 
            }
        }



    }
}
