﻿namespace RhythmField
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition2 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition3 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.GridViewSummaryItem gridViewSummaryItem1 = new Telerik.WinControls.UI.GridViewSummaryItem();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition5 = new Telerik.WinControls.UI.TableViewDefinition();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition6 = new Telerik.WinControls.UI.TableViewDefinition();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.radDesktopAlert1 = new Telerik.WinControls.UI.RadDesktopAlert(this.components);
            this.tmrAlerts = new System.Windows.Forms.Timer(this.components);
            this.BottomToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.TopToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.RightToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.LeftToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            this.ContentPanel = new System.Windows.Forms.ToolStripContentPanel();
            this.lblSite = new System.Windows.Forms.Label();
            this.lblBatteries = new System.Windows.Forms.Label();
            this.lblChargers = new System.Windows.Forms.Label();
            this.lblWorkstations = new System.Windows.Forms.Label();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.PageBattery = new Telerik.WinControls.UI.RadPageViewPage();
            this.cmdExportBatteries = new System.Windows.Forms.Button();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblBatteriesExporting = new System.Windows.Forms.Label();
            this.radGridViewBatteries = new Telerik.WinControls.UI.RadGridView();
            this.PageCharger = new Telerik.WinControls.UI.RadPageViewPage();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.lblChargersExporting = new System.Windows.Forms.Label();
            this.cmdExportChargers = new System.Windows.Forms.Button();
            this.radGridViewChargers = new Telerik.WinControls.UI.RadGridView();
            this.PageWorkStations = new Telerik.WinControls.UI.RadPageViewPage();
            this.cmdExportWorkstations = new System.Windows.Forms.Button();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.lblWorkstationsExporting = new System.Windows.Forms.Label();
            this.radGridViewWorkstations = new Telerik.WinControls.UI.RadGridView();
            this.PagePackets = new Telerik.WinControls.UI.RadPageViewPage();
            this.btnSHutdown = new FontAwesomeIcons.IconButton();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.lblPacketExport = new System.Windows.Forms.Label();
            this.cmdExportPackets = new System.Windows.Forms.Button();
            this.radGridViewPackets = new Telerik.WinControls.UI.RadGridView();
            this.EngQS = new Telerik.WinControls.UI.RadPageViewPage();
            this.radGridViewEngQS = new Telerik.WinControls.UI.RadGridView();
            this.EngPackets = new Telerik.WinControls.UI.RadPageViewPage();
            this.radgridEngPackets = new Telerik.WinControls.UI.RadGridView();
            this.lblSiteId = new System.Windows.Forms.Label();
            this.lblPacketSerial = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnAdmin = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.iconButton3 = new FontAwesomeIcons.IconButton();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.iconButton4 = new FontAwesomeIcons.IconButton();
            this.iconButton7 = new FontAwesomeIcons.IconButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPacketSerialNo = new System.Windows.Forms.TextBox();
            this.radDropDownSites = new Telerik.WinControls.UI.RadDropDownList();
            this.lblSearch = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gbManagerReports = new System.Windows.Forms.GroupBox();
            this.iconButton8 = new FontAwesomeIcons.IconButton();
            this.label8 = new System.Windows.Forms.Label();
            this.iconButton5 = new FontAwesomeIcons.IconButton();
            this.label5 = new System.Windows.Forms.Label();
            this.iconButton6 = new FontAwesomeIcons.IconButton();
            this.label6 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.iconButton10 = new FontAwesomeIcons.IconButton();
            this.cmdBatteries = new FontAwesomeIcons.IconButton();
            this.cmdChargers = new FontAwesomeIcons.IconButton();
            this.cmdWorkStations = new FontAwesomeIcons.IconButton();
            this.cmdPacketSearch = new FontAwesomeIcons.IconButton();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.PageBattery.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewBatteries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewBatteries.MasterTemplate)).BeginInit();
            this.PageCharger.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewChargers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewChargers.MasterTemplate)).BeginInit();
            this.PageWorkStations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewWorkstations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewWorkstations.MasterTemplate)).BeginInit();
            this.PagePackets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSHutdown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewPackets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewPackets.MasterTemplate)).BeginInit();
            this.EngQS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEngQS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEngQS.MasterTemplate)).BeginInit();
            this.EngPackets.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radgridEngPackets)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radgridEngPackets.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton3)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownSites)).BeginInit();
            this.gbManagerReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatteries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChargers)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdWorkStations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPacketSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // radDesktopAlert1
            // 
            this.radDesktopAlert1.CaptionText = "Important Message";
            this.radDesktopAlert1.Opacity = 1F;
            // 
            // tmrAlerts
            // 
            this.tmrAlerts.Enabled = true;
            this.tmrAlerts.Interval = 60000;
            this.tmrAlerts.Tick += new System.EventHandler(this.tmrAlerts_Tick);
            // 
            // BottomToolStripPanel
            // 
            this.BottomToolStripPanel.Location = new System.Drawing.Point(-14, 0);
            this.BottomToolStripPanel.Name = "BottomToolStripPanel";
            this.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.BottomToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.BottomToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // TopToolStripPanel
            // 
            this.TopToolStripPanel.Location = new System.Drawing.Point(-14, 0);
            this.TopToolStripPanel.Name = "TopToolStripPanel";
            this.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.TopToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.TopToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // RightToolStripPanel
            // 
            this.RightToolStripPanel.Location = new System.Drawing.Point(-14, 0);
            this.RightToolStripPanel.Name = "RightToolStripPanel";
            this.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RightToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.RightToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // LeftToolStripPanel
            // 
            this.LeftToolStripPanel.Location = new System.Drawing.Point(-14, 0);
            this.LeftToolStripPanel.Name = "LeftToolStripPanel";
            this.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.LeftToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.LeftToolStripPanel.Size = new System.Drawing.Size(0, 0);
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoScroll = true;
            this.ContentPanel.Size = new System.Drawing.Size(1360, 609);
            // 
            // lblSite
            // 
            this.lblSite.AutoSize = true;
            this.lblSite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSite.Location = new System.Drawing.Point(293, 55);
            this.lblSite.Name = "lblSite";
            this.lblSite.Size = new System.Drawing.Size(21, 13);
            this.lblSite.TabIndex = 4;
            this.lblSite.Text = "ID:";
            // 
            // lblBatteries
            // 
            this.lblBatteries.AutoSize = true;
            this.lblBatteries.BackColor = System.Drawing.Color.Transparent;
            this.lblBatteries.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatteries.Location = new System.Drawing.Point(7, 76);
            this.lblBatteries.Name = "lblBatteries";
            this.lblBatteries.Size = new System.Drawing.Size(63, 15);
            this.lblBatteries.TabIndex = 6;
            this.lblBatteries.Text = "Batteries";
            this.lblBatteries.Click += new System.EventHandler(this.lblBatteries_Click);
            // 
            // lblChargers
            // 
            this.lblChargers.AutoSize = true;
            this.lblChargers.BackColor = System.Drawing.Color.Transparent;
            this.lblChargers.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChargers.Location = new System.Drawing.Point(98, 76);
            this.lblChargers.Name = "lblChargers";
            this.lblChargers.Size = new System.Drawing.Size(61, 15);
            this.lblChargers.TabIndex = 7;
            this.lblChargers.Text = "Chargers";
            this.lblChargers.Click += new System.EventHandler(this.lblChargers_Click);
            // 
            // lblWorkstations
            // 
            this.lblWorkstations.AutoSize = true;
            this.lblWorkstations.BackColor = System.Drawing.Color.Transparent;
            this.lblWorkstations.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkstations.Location = new System.Drawing.Point(177, 76);
            this.lblWorkstations.Name = "lblWorkstations";
            this.lblWorkstations.Size = new System.Drawing.Size(86, 15);
            this.lblWorkstations.TabIndex = 8;
            this.lblWorkstations.Text = "Workstations";
            this.lblWorkstations.Click += new System.EventHandler(this.lblWorkstations_Click);
            // 
            // radPageView1
            // 
            this.radPageView1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radPageView1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("radPageView1.BackgroundImage")));
            this.radPageView1.Controls.Add(this.PageBattery);
            this.radPageView1.Controls.Add(this.PageCharger);
            this.radPageView1.Controls.Add(this.PageWorkStations);
            this.radPageView1.Controls.Add(this.PagePackets);
            this.radPageView1.Controls.Add(this.EngQS);
            this.radPageView1.Controls.Add(this.EngPackets);
            this.radPageView1.DefaultPage = this.PageWorkStations;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPageView1.Location = new System.Drawing.Point(1, 103);
            this.radPageView1.Margin = new System.Windows.Forms.Padding(1);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.Padding = new System.Windows.Forms.Padding(4);
            // 
            // 
            // 
            this.radPageView1.RootElement.ControlBounds = new System.Drawing.Rectangle(1, 103, 400, 300);
            this.radPageView1.SelectedPage = this.PageCharger;
            this.radPageView1.Size = new System.Drawing.Size(1389, 343);
            this.radPageView1.TabIndex = 11;
            this.radPageView1.Text = "radPageViewBattery";
            this.radPageView1.SelectedPageChanged += new System.EventHandler(this.radPageView1_SelectedPageChanged);
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).Padding = new System.Windows.Forms.Padding(4);
            // 
            // PageBattery
            // 
            this.PageBattery.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.PageBattery.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PageBattery.Controls.Add(this.cmdExportBatteries);
            this.PageBattery.Controls.Add(this.linkLabel1);
            this.PageBattery.Controls.Add(this.label1);
            this.PageBattery.Controls.Add(this.lblBatteriesExporting);
            this.PageBattery.Controls.Add(this.radGridViewBatteries);
            this.PageBattery.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PageBattery.ImageAlignment = System.Drawing.ContentAlignment.TopCenter;
            this.PageBattery.ItemSize = new System.Drawing.SizeF(59F, 28F);
            this.PageBattery.Location = new System.Drawing.Point(9, 36);
            this.PageBattery.Name = "PageBattery";
            this.PageBattery.Size = new System.Drawing.Size(1371, 298);
            this.PageBattery.Text = "Batteries";
            this.PageBattery.TextAlignment = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // cmdExportBatteries
            // 
            this.cmdExportBatteries.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportBatteries.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportBatteries.Image")));
            this.cmdExportBatteries.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportBatteries.Location = new System.Drawing.Point(5, -2);
            this.cmdExportBatteries.Name = "cmdExportBatteries";
            this.cmdExportBatteries.Size = new System.Drawing.Size(75, 26);
            this.cmdExportBatteries.TabIndex = 32;
            this.cmdExportBatteries.Text = "Export";
            this.cmdExportBatteries.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdExportBatteries.UseVisualStyleBackColor = true;
            this.cmdExportBatteries.Click += new System.EventHandler(this.cmdExportBatteries_Click_1);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.Location = new System.Drawing.Point(1130, 10);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(116, 16);
            this.linkLabel1.TabIndex = 24;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Pulse Support";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(177, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(551, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "Do not provide an export of this list to a customer with MaxCycleCount, Shutdown," +
    " or SF Customer Acct.";
            // 
            // lblBatteriesExporting
            // 
            this.lblBatteriesExporting.AutoSize = true;
            this.lblBatteriesExporting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBatteriesExporting.Location = new System.Drawing.Point(86, 7);
            this.lblBatteriesExporting.Name = "lblBatteriesExporting";
            this.lblBatteriesExporting.Size = new System.Drawing.Size(80, 17);
            this.lblBatteriesExporting.TabIndex = 20;
            this.lblBatteriesExporting.Text = "Exporting...";
            // 
            // radGridViewBatteries
            // 
            this.radGridViewBatteries.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewBatteries.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridViewBatteries.Location = new System.Drawing.Point(2, 30);
            // 
            // 
            // 
            this.radGridViewBatteries.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewBatteries.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewBatteries.MasterTemplate.AllowEditRow = false;
            this.radGridViewBatteries.MasterTemplate.AllowRowResize = false;
            this.radGridViewBatteries.MasterTemplate.AllowSearchRow = true;
            this.radGridViewBatteries.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewBatteries.MasterTemplate.EnableFiltering = true;
            this.radGridViewBatteries.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[0]));
            this.radGridViewBatteries.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridViewBatteries.Name = "radGridViewBatteries";
            // 
            // 
            // 
            this.radGridViewBatteries.RootElement.ControlBounds = new System.Drawing.Rectangle(2, 30, 240, 150);
            this.radGridViewBatteries.Size = new System.Drawing.Size(1369, 264);
            this.radGridViewBatteries.TabIndex = 2;
            this.radGridViewBatteries.Text = "radGridView1";
            this.radGridViewBatteries.DataBindingComplete += new Telerik.WinControls.UI.GridViewBindingCompleteEventHandler(this.AddFooterBatteries);
            // 
            // PageCharger
            // 
            this.PageCharger.Controls.Add(this.linkLabel2);
            this.PageCharger.Controls.Add(this.lblChargersExporting);
            this.PageCharger.Controls.Add(this.cmdExportChargers);
            this.PageCharger.Controls.Add(this.radGridViewChargers);
            this.PageCharger.ItemSize = new System.Drawing.SizeF(61F, 28F);
            this.PageCharger.Location = new System.Drawing.Point(9, 36);
            this.PageCharger.Name = "PageCharger";
            this.PageCharger.Size = new System.Drawing.Size(1371, 298);
            this.PageCharger.Text = "Chargers";
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel2.Location = new System.Drawing.Point(1130, 10);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(84, 16);
            this.linkLabel2.TabIndex = 23;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Pulse Support";
            // 
            // lblChargersExporting
            // 
            this.lblChargersExporting.AutoSize = true;
            this.lblChargersExporting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChargersExporting.Location = new System.Drawing.Point(85, 8);
            this.lblChargersExporting.Name = "lblChargersExporting";
            this.lblChargersExporting.Size = new System.Drawing.Size(80, 17);
            this.lblChargersExporting.TabIndex = 20;
            this.lblChargersExporting.Text = "Exporting...";
            // 
            // cmdExportChargers
            // 
            this.cmdExportChargers.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportChargers.Image")));
            this.cmdExportChargers.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportChargers.Location = new System.Drawing.Point(4, 2);
            this.cmdExportChargers.Name = "cmdExportChargers";
            this.cmdExportChargers.Size = new System.Drawing.Size(75, 26);
            this.cmdExportChargers.TabIndex = 18;
            this.cmdExportChargers.Text = "Export";
            this.cmdExportChargers.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdExportChargers.UseVisualStyleBackColor = true;
            this.cmdExportChargers.Click += new System.EventHandler(this.cmdExportChargers_Click);
            // 
            // radGridViewChargers
            // 
            this.radGridViewChargers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewChargers.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridViewChargers.Location = new System.Drawing.Point(0, 32);
            // 
            // 
            // 
            this.radGridViewChargers.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewChargers.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewChargers.MasterTemplate.AllowEditRow = false;
            this.radGridViewChargers.MasterTemplate.AllowRowResize = false;
            this.radGridViewChargers.MasterTemplate.AllowSearchRow = true;
            this.radGridViewChargers.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewChargers.MasterTemplate.EnableFiltering = true;
            this.radGridViewChargers.MasterTemplate.ViewDefinition = tableViewDefinition2;
            this.radGridViewChargers.Name = "radGridViewChargers";
            // 
            // 
            // 
            this.radGridViewChargers.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 32, 240, 150);
            this.radGridViewChargers.Size = new System.Drawing.Size(1368, 259);
            this.radGridViewChargers.TabIndex = 2;
            this.radGridViewChargers.Text = "radGridViewCharger";
            // 
            // PageWorkStations
            // 
            this.PageWorkStations.Controls.Add(this.cmdExportWorkstations);
            this.PageWorkStations.Controls.Add(this.linkLabel3);
            this.PageWorkStations.Controls.Add(this.lblWorkstationsExporting);
            this.PageWorkStations.Controls.Add(this.radGridViewWorkstations);
            this.PageWorkStations.ItemSize = new System.Drawing.SizeF(82F, 28F);
            this.PageWorkStations.Location = new System.Drawing.Point(9, 36);
            this.PageWorkStations.Name = "PageWorkStations";
            this.PageWorkStations.Size = new System.Drawing.Size(1371, 298);
            this.PageWorkStations.Text = "Workstations";
            // 
            // cmdExportWorkstations
            // 
            this.cmdExportWorkstations.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportWorkstations.Image")));
            this.cmdExportWorkstations.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportWorkstations.Location = new System.Drawing.Point(3, 0);
            this.cmdExportWorkstations.Name = "cmdExportWorkstations";
            this.cmdExportWorkstations.Size = new System.Drawing.Size(75, 26);
            this.cmdExportWorkstations.TabIndex = 18;
            this.cmdExportWorkstations.Text = "Export";
            this.cmdExportWorkstations.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdExportWorkstations.UseVisualStyleBackColor = true;
            this.cmdExportWorkstations.Click += new System.EventHandler(this.cmdExportWorkstations_Click);
            // 
            // linkLabel3
            // 
            this.linkLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel3.Location = new System.Drawing.Point(1130, 10);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(83, 17);
            this.linkLabel3.TabIndex = 24;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Pulse Support";
            // 
            // lblWorkstationsExporting
            // 
            this.lblWorkstationsExporting.AutoSize = true;
            this.lblWorkstationsExporting.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWorkstationsExporting.Location = new System.Drawing.Point(110, 5);
            this.lblWorkstationsExporting.Name = "lblWorkstationsExporting";
            this.lblWorkstationsExporting.Size = new System.Drawing.Size(80, 17);
            this.lblWorkstationsExporting.TabIndex = 20;
            this.lblWorkstationsExporting.Text = "Exporting...";
            // 
            // radGridViewWorkstations
            // 
            this.radGridViewWorkstations.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewWorkstations.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridViewWorkstations.Location = new System.Drawing.Point(2, 29);
            // 
            // 
            // 
            this.radGridViewWorkstations.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewWorkstations.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewWorkstations.MasterTemplate.AllowEditRow = false;
            this.radGridViewWorkstations.MasterTemplate.AllowRowResize = false;
            this.radGridViewWorkstations.MasterTemplate.AllowSearchRow = true;
            this.radGridViewWorkstations.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewWorkstations.MasterTemplate.EnableFiltering = true;
            this.radGridViewWorkstations.MasterTemplate.ViewDefinition = tableViewDefinition3;
            this.radGridViewWorkstations.Name = "radGridViewWorkstations";
            // 
            // 
            // 
            this.radGridViewWorkstations.RootElement.ControlBounds = new System.Drawing.Rectangle(2, 29, 240, 150);
            this.radGridViewWorkstations.Size = new System.Drawing.Size(1369, 266);
            this.radGridViewWorkstations.TabIndex = 1;
            this.radGridViewWorkstations.Text = "radGridViewWorkstation";
            // 
            // PagePackets
            // 
            this.PagePackets.Controls.Add(this.btnSHutdown);
            this.PagePackets.Controls.Add(this.linkLabel4);
            this.PagePackets.Controls.Add(this.lblPacketExport);
            this.PagePackets.Controls.Add(this.cmdExportPackets);
            this.PagePackets.Controls.Add(this.radGridViewPackets);
            this.PagePackets.ItemSize = new System.Drawing.SizeF(54F, 28F);
            this.PagePackets.Location = new System.Drawing.Point(9, 32);
            this.PagePackets.Name = "PagePackets";
            this.PagePackets.Size = new System.Drawing.Size(1371, 302);
            this.PagePackets.Text = "Packets";
            // 
            // btnSHutdown
            // 
            this.btnSHutdown.ActiveColor = System.Drawing.Color.Black;
            this.btnSHutdown.BackColor = System.Drawing.Color.Transparent;
            this.btnSHutdown.IconType = FontAwesomeIcons.IconType.Warning;
            this.btnSHutdown.InActiveColor = System.Drawing.Color.Red;
            this.btnSHutdown.Location = new System.Drawing.Point(1289, 3);
            this.btnSHutdown.Name = "btnSHutdown";
            this.btnSHutdown.Size = new System.Drawing.Size(34, 28);
            this.btnSHutdown.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.btnSHutdown.TabIndex = 32;
            this.btnSHutdown.TabStop = false;
            this.btnSHutdown.ToolTipText = "Shutdown Or Restore";
            this.btnSHutdown.Visible = false;
            this.btnSHutdown.Click += new System.EventHandler(this.btnSHutdown_Click_1);
            // 
            // linkLabel4
            // 
            this.linkLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel4.Location = new System.Drawing.Point(1130, 10);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(84, 17);
            this.linkLabel4.TabIndex = 24;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "Pulse Support";
            // 
            // lblPacketExport
            // 
            this.lblPacketExport.AutoSize = true;
            this.lblPacketExport.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacketExport.Location = new System.Drawing.Point(84, 7);
            this.lblPacketExport.Name = "lblPacketExport";
            this.lblPacketExport.Size = new System.Drawing.Size(80, 17);
            this.lblPacketExport.TabIndex = 19;
            this.lblPacketExport.Text = "Exporting...";
            // 
            // cmdExportPackets
            // 
            this.cmdExportPackets.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportPackets.Image")));
            this.cmdExportPackets.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportPackets.Location = new System.Drawing.Point(3, 1);
            this.cmdExportPackets.Name = "cmdExportPackets";
            this.cmdExportPackets.Size = new System.Drawing.Size(75, 26);
            this.cmdExportPackets.TabIndex = 18;
            this.cmdExportPackets.Text = "Export";
            this.cmdExportPackets.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdExportPackets.UseVisualStyleBackColor = true;
            this.cmdExportPackets.Click += new System.EventHandler(this.cmdExportPackets_Click);
            // 
            // radGridViewPackets
            // 
            this.radGridViewPackets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewPackets.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridViewPackets.Location = new System.Drawing.Point(-8, 32);
            // 
            // 
            // 
            this.radGridViewPackets.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewPackets.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewPackets.MasterTemplate.AllowEditRow = false;
            this.radGridViewPackets.MasterTemplate.AllowRowResize = false;
            this.radGridViewPackets.MasterTemplate.AllowSearchRow = true;
            this.radGridViewPackets.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewPackets.MasterTemplate.EnableFiltering = true;
            gridViewSummaryItem1.Aggregate = Telerik.WinControls.UI.GridAggregateFunction.Count;
            gridViewSummaryItem1.AggregateExpression = null;
            gridViewSummaryItem1.FormatString = "{0}";
            gridViewSummaryItem1.Name = null;
            this.radGridViewPackets.MasterTemplate.SummaryRowsBottom.Add(new Telerik.WinControls.UI.GridViewSummaryRowItem(new Telerik.WinControls.UI.GridViewSummaryItem[] {
                gridViewSummaryItem1}));
            this.radGridViewPackets.MasterTemplate.ViewDefinition = tableViewDefinition4;
            this.radGridViewPackets.Name = "radGridViewPackets";
            // 
            // 
            // 
            this.radGridViewPackets.RootElement.ControlBounds = new System.Drawing.Rectangle(-8, 32, 240, 150);
            this.radGridViewPackets.Size = new System.Drawing.Size(1379, 271);
            this.radGridViewPackets.TabIndex = 2;
            this.radGridViewPackets.Text = "radGridViewPacket";
            // 
            // EngQS
            // 
            this.EngQS.Controls.Add(this.radGridViewEngQS);
            this.EngQS.ItemSize = new System.Drawing.SizeF(137F, 28F);
            this.EngQS.Location = new System.Drawing.Point(9, 36);
            this.EngQS.Name = "EngQS";
            this.EngQS.Size = new System.Drawing.Size(1371, 298);
            this.EngQS.Text = "Eng Querystring Packets";
            // 
            // radGridViewEngQS
            // 
            this.radGridViewEngQS.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridViewEngQS.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radGridViewEngQS.Location = new System.Drawing.Point(-12, 18);
            // 
            // 
            // 
            this.radGridViewEngQS.MasterTemplate.AllowAddNewRow = false;
            this.radGridViewEngQS.MasterTemplate.AllowDeleteRow = false;
            this.radGridViewEngQS.MasterTemplate.AllowEditRow = false;
            this.radGridViewEngQS.MasterTemplate.AllowRowResize = false;
            this.radGridViewEngQS.MasterTemplate.AllowSearchRow = true;
            this.radGridViewEngQS.MasterTemplate.EnableAlternatingRowColor = true;
            this.radGridViewEngQS.MasterTemplate.EnableFiltering = true;
            this.radGridViewEngQS.MasterTemplate.ViewDefinition = tableViewDefinition5;
            this.radGridViewEngQS.Name = "radGridViewEngQS";
            // 
            // 
            // 
            this.radGridViewEngQS.RootElement.ControlBounds = new System.Drawing.Rectangle(-12, 18, 240, 150);
            this.radGridViewEngQS.Size = new System.Drawing.Size(1368, 262);
            this.radGridViewEngQS.TabIndex = 2;
            this.radGridViewEngQS.Text = "radGridViewEngQS";
            // 
            // EngPackets
            // 
            this.EngPackets.Controls.Add(this.radgridEngPackets);
            this.EngPackets.ItemSize = new System.Drawing.SizeF(76F, 28F);
            this.EngPackets.Location = new System.Drawing.Point(9, 36);
            this.EngPackets.Name = "EngPackets";
            this.EngPackets.Size = new System.Drawing.Size(1371, 298);
            this.EngPackets.Text = "Eng Packets";
            // 
            // radgridEngPackets
            // 
            this.radgridEngPackets.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radgridEngPackets.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radgridEngPackets.Location = new System.Drawing.Point(-12, 18);
            // 
            // 
            // 
            this.radgridEngPackets.MasterTemplate.AllowAddNewRow = false;
            this.radgridEngPackets.MasterTemplate.AllowDeleteRow = false;
            this.radgridEngPackets.MasterTemplate.AllowEditRow = false;
            this.radgridEngPackets.MasterTemplate.AllowRowResize = false;
            this.radgridEngPackets.MasterTemplate.AllowSearchRow = true;
            this.radgridEngPackets.MasterTemplate.EnableAlternatingRowColor = true;
            this.radgridEngPackets.MasterTemplate.EnableFiltering = true;
            this.radgridEngPackets.MasterTemplate.MultiSelect = true;
            this.radgridEngPackets.MasterTemplate.ViewDefinition = tableViewDefinition6;
            this.radgridEngPackets.Name = "radgridEngPackets";
            // 
            // 
            // 
            this.radgridEngPackets.RootElement.ControlBounds = new System.Drawing.Rectangle(-12, 18, 240, 150);
            this.radgridEngPackets.Size = new System.Drawing.Size(1368, 262);
            this.radgridEngPackets.TabIndex = 2;
            this.radgridEngPackets.Text = "radGridViewEngPackets";
            // 
            // lblSiteId
            // 
            this.lblSiteId.BackColor = System.Drawing.Color.Transparent;
            this.lblSiteId.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSiteId.Location = new System.Drawing.Point(316, 55);
            this.lblSiteId.Name = "lblSiteId";
            this.lblSiteId.Size = new System.Drawing.Size(45, 23);
            this.lblSiteId.TabIndex = 13;
            this.lblSiteId.Text = "...";
            // 
            // lblPacketSerial
            // 
            this.lblPacketSerial.AutoSize = true;
            this.lblPacketSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPacketSerial.Location = new System.Drawing.Point(261, 41);
            this.lblPacketSerial.Name = "lblPacketSerial";
            this.lblPacketSerial.Size = new System.Drawing.Size(101, 20);
            this.lblPacketSerial.TabIndex = 14;
            this.lblPacketSerial.Text = "Device Serial";
            this.lblPacketSerial.Click += new System.EventHandler(this.lblPacketSerial_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1322, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(21, 20);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1346, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(21, 20);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(1369, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(21, 20);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // btnAdmin
            // 
            this.btnAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdmin.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAdmin.Location = new System.Drawing.Point(532, 10);
            this.btnAdmin.Name = "btnAdmin";
            this.btnAdmin.Size = new System.Drawing.Size(89, 24);
            this.btnAdmin.TabIndex = 19;
            this.btnAdmin.Text = "Admin Panel";
            this.btnAdmin.UseVisualStyleBackColor = true;
            this.btnAdmin.Visible = false;
            this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.iconButton3);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(878, 10);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(114, 75);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Utilities";
            // 
            // iconButton3
            // 
            this.iconButton3.ActiveColor = System.Drawing.Color.Black;
            this.iconButton3.BackColor = System.Drawing.Color.Transparent;
            this.iconButton3.IconType = FontAwesomeIcons.IconType.Refresh;
            this.iconButton3.InActiveColor = System.Drawing.Color.Blue;
            this.iconButton3.Location = new System.Drawing.Point(35, 18);
            this.iconButton3.Name = "iconButton3";
            this.iconButton3.Size = new System.Drawing.Size(46, 37);
            this.iconButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton3.TabIndex = 25;
            this.iconButton3.TabStop = false;
            this.iconButton3.ToolTipText = null;
            this.iconButton3.Click += new System.EventHandler(this.iconButton3_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 15);
            this.label3.TabIndex = 26;
            this.label3.Text = "Recycle Assets";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.iconButton4);
            this.groupBox2.Controls.Add(this.iconButton7);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(997, 10);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(147, 75);
            this.groupBox2.TabIndex = 28;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Reports";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // iconButton4
            // 
            this.iconButton4.ActiveColor = System.Drawing.Color.Black;
            this.iconButton4.BackColor = System.Drawing.Color.Transparent;
            this.iconButton4.IconType = FontAwesomeIcons.IconType.Warning;
            this.iconButton4.InActiveColor = System.Drawing.Color.Maroon;
            this.iconButton4.Location = new System.Drawing.Point(19, 16);
            this.iconButton4.Name = "iconButton4";
            this.iconButton4.Size = new System.Drawing.Size(47, 39);
            this.iconButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton4.TabIndex = 27;
            this.iconButton4.TabStop = false;
            this.iconButton4.ToolTipText = null;
            this.iconButton4.Click += new System.EventHandler(this.iconButton4_Click);
            // 
            // iconButton7
            // 
            this.iconButton7.ActiveColor = System.Drawing.Color.Black;
            this.iconButton7.BackColor = System.Drawing.Color.Transparent;
            this.iconButton7.IconType = FontAwesomeIcons.IconType.Ban;
            this.iconButton7.InActiveColor = System.Drawing.Color.Plum;
            this.iconButton7.Location = new System.Drawing.Point(86, 16);
            this.iconButton7.Name = "iconButton7";
            this.iconButton7.Size = new System.Drawing.Size(42, 39);
            this.iconButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton7.TabIndex = 29;
            this.iconButton7.TabStop = false;
            this.iconButton7.ToolTipText = null;
            this.iconButton7.Click += new System.EventHandler(this.iconButton7_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 15);
            this.label4.TabIndex = 28;
            this.label4.Text = "Site Alerts";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(78, 54);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 15);
            this.label7.TabIndex = 30;
            this.label7.Text = "Disabled ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(271, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 15);
            this.label10.TabIndex = 40;
            this.label10.Text = "Site Testing";
            this.label10.Visible = false;
            // 
            // txtPacketSerialNo
            // 
            this.txtPacketSerialNo.Location = new System.Drawing.Point(367, 40);
            this.txtPacketSerialNo.Name = "txtPacketSerialNo";
            this.txtPacketSerialNo.Size = new System.Drawing.Size(278, 20);
            this.txtPacketSerialNo.TabIndex = 15;
            this.txtPacketSerialNo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPacketSerialNo_KeyDown);
            // 
            // radDropDownSites
            // 
            this.radDropDownSites.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.radDropDownSites.AutoSizeItems = true;
            this.radDropDownSites.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.radDropDownSites.DefaultItemsCountInDropDown = 50;
            this.radDropDownSites.DropDownHeight = 500;
            this.radDropDownSites.EnableAlternatingItemColor = true;
            this.radDropDownSites.FitItemsToSize = false;
            this.radDropDownSites.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.radDropDownSites.Location = new System.Drawing.Point(368, 45);
            this.radDropDownSites.MaxDropDownItems = 50;
            this.radDropDownSites.Name = "radDropDownSites";
            // 
            // 
            // 
            this.radDropDownSites.RootElement.ControlBounds = new System.Drawing.Rectangle(368, 45, 125, 20);
            this.radDropDownSites.RootElement.StretchVertically = true;
            this.radDropDownSites.Size = new System.Drawing.Size(273, 32);
            this.radDropDownSites.SortStyle = Telerik.WinControls.Enumerations.SortStyle.Ascending;
            this.radDropDownSites.TabIndex = 12;
            this.radDropDownSites.Text = "Select Site";
            this.radDropDownSites.SelectedIndexChanged += new Telerik.WinControls.UI.Data.PositionChangedEventHandler(this.radDropDownSites_SelectedIndexChanged);
            this.radDropDownSites.SelectedValueChanged += new System.EventHandler(this.radDropDownSites_SelectedValueChanged);
            // 
            // lblSearch
            // 
            this.lblSearch.AutoSize = true;
            this.lblSearch.BackColor = System.Drawing.Color.Transparent;
            this.lblSearch.Font = new System.Drawing.Font("Arial Black", 8F);
            this.lblSearch.Location = new System.Drawing.Point(648, 45);
            this.lblSearch.Name = "lblSearch";
            this.lblSearch.Size = new System.Drawing.Size(48, 15);
            this.lblSearch.TabIndex = 42;
            this.lblSearch.Text = "Search";
            this.lblSearch.Visible = false;
            this.lblSearch.Click += new System.EventHandler(this.lblSearch_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(411, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 21;
            this.label2.Text = "Rhythm v.1.39";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gbManagerReports
            // 
            this.gbManagerReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbManagerReports.Controls.Add(this.iconButton8);
            this.gbManagerReports.Controls.Add(this.label8);
            this.gbManagerReports.Controls.Add(this.iconButton5);
            this.gbManagerReports.Controls.Add(this.label5);
            this.gbManagerReports.Controls.Add(this.iconButton6);
            this.gbManagerReports.Controls.Add(this.label6);
            this.gbManagerReports.Location = new System.Drawing.Point(1148, 10);
            this.gbManagerReports.Name = "gbManagerReports";
            this.gbManagerReports.Size = new System.Drawing.Size(155, 75);
            this.gbManagerReports.TabIndex = 29;
            this.gbManagerReports.TabStop = false;
            this.gbManagerReports.Text = "Manager Reports";
            this.gbManagerReports.Visible = false;
            // 
            // iconButton8
            // 
            this.iconButton8.ActiveColor = System.Drawing.Color.Black;
            this.iconButton8.BackColor = System.Drawing.Color.Transparent;
            this.iconButton8.IconType = FontAwesomeIcons.IconType.Exclamation;
            this.iconButton8.InActiveColor = System.Drawing.Color.Turquoise;
            this.iconButton8.Location = new System.Drawing.Point(110, 19);
            this.iconButton8.Name = "iconButton8";
            this.iconButton8.Size = new System.Drawing.Size(38, 35);
            this.iconButton8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton8.TabIndex = 29;
            this.iconButton8.TabStop = false;
            this.iconButton8.ToolTipText = null;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(108, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 15);
            this.label8.TabIndex = 30;
            this.label8.Text = "Notify";
            // 
            // iconButton5
            // 
            this.iconButton5.ActiveColor = System.Drawing.Color.Black;
            this.iconButton5.BackColor = System.Drawing.Color.Transparent;
            this.iconButton5.IconType = FontAwesomeIcons.IconType.TrashO;
            this.iconButton5.InActiveColor = System.Drawing.Color.MediumOrchid;
            this.iconButton5.Location = new System.Drawing.Point(58, 19);
            this.iconButton5.Name = "iconButton5";
            this.iconButton5.Size = new System.Drawing.Size(37, 37);
            this.iconButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton5.TabIndex = 27;
            this.iconButton5.TabStop = false;
            this.iconButton5.ToolTipText = null;
            this.iconButton5.Click += new System.EventHandler(this.iconButton5_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(46, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 15);
            this.label5.TabIndex = 28;
            this.label5.Text = "Recycled";
            // 
            // iconButton6
            // 
            this.iconButton6.ActiveColor = System.Drawing.Color.Black;
            this.iconButton6.BackColor = System.Drawing.Color.Transparent;
            this.iconButton6.IconType = FontAwesomeIcons.IconType.Signal;
            this.iconButton6.InActiveColor = System.Drawing.Color.Pink;
            this.iconButton6.Location = new System.Drawing.Point(9, 19);
            this.iconButton6.Name = "iconButton6";
            this.iconButton6.Size = new System.Drawing.Size(35, 37);
            this.iconButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.iconButton6.TabIndex = 25;
            this.iconButton6.TabStop = false;
            this.iconButton6.ToolTipText = null;
            this.iconButton6.Click += new System.EventHandler(this.iconButton6_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Arial Black", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 15);
            this.label6.TabIndex = 26;
            this.label6.Text = "APs";
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsername.Location = new System.Drawing.Point(1, 1);
            this.lblUsername.Margin = new System.Windows.Forms.Padding(0);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(62, 13);
            this.lblUsername.TabIndex = 18;
            this.lblUsername.Text = "Jason Batts";
            // 
            // iconButton10
            // 
            this.iconButton10.ActiveColor = System.Drawing.Color.Lime;
            this.iconButton10.BackColor = System.Drawing.Color.Transparent;
            this.iconButton10.IconType = FontAwesomeIcons.IconType.Random;
            this.iconButton10.InActiveColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.iconButton10.Location = new System.Drawing.Point(357, 10);
            this.iconButton10.Name = "iconButton10";
            this.iconButton10.Size = new System.Drawing.Size(29, 24);
            this.iconButton10.TabIndex = 39;
            this.iconButton10.TabStop = false;
            this.iconButton10.ToolTipText = "Open Site Test Panel";
            this.iconButton10.Visible = false;
            this.iconButton10.Click += new System.EventHandler(this.iconButton10_Click_1);
            // 
            // cmdBatteries
            // 
            this.cmdBatteries.ActiveColor = System.Drawing.Color.Black;
            this.cmdBatteries.BackColor = System.Drawing.Color.Transparent;
            this.cmdBatteries.IconType = FontAwesomeIcons.IconType.BatteryThreeQuarters;
            this.cmdBatteries.InActiveColor = System.Drawing.Color.Green;
            this.cmdBatteries.Location = new System.Drawing.Point(4, 23);
            this.cmdBatteries.Name = "cmdBatteries";
            this.cmdBatteries.Size = new System.Drawing.Size(68, 51);
            this.cmdBatteries.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdBatteries.TabIndex = 0;
            this.cmdBatteries.TabStop = false;
            this.cmdBatteries.ToolTipText = null;
            this.cmdBatteries.Click += new System.EventHandler(this.cmdBatteries_Click);
            // 
            // cmdChargers
            // 
            this.cmdChargers.ActiveColor = System.Drawing.Color.Black;
            this.cmdChargers.BackColor = System.Drawing.Color.Transparent;
            this.cmdChargers.IconType = FontAwesomeIcons.IconType.Flash;
            this.cmdChargers.InActiveColor = System.Drawing.Color.RoyalBlue;
            this.cmdChargers.Location = new System.Drawing.Point(95, 23);
            this.cmdChargers.Name = "cmdChargers";
            this.cmdChargers.Size = new System.Drawing.Size(68, 51);
            this.cmdChargers.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdChargers.TabIndex = 1;
            this.cmdChargers.TabStop = false;
            this.cmdChargers.ToolTipText = null;
            this.cmdChargers.Click += new System.EventHandler(this.cmdChargers_Click);
            // 
            // cmdWorkStations
            // 
            this.cmdWorkStations.ActiveColor = System.Drawing.Color.BlanchedAlmond;
            this.cmdWorkStations.BackColor = System.Drawing.Color.Transparent;
            this.cmdWorkStations.IconType = FontAwesomeIcons.IconType.Laptop;
            this.cmdWorkStations.InActiveColor = System.Drawing.Color.Red;
            this.cmdWorkStations.Location = new System.Drawing.Point(185, 23);
            this.cmdWorkStations.Name = "cmdWorkStations";
            this.cmdWorkStations.Size = new System.Drawing.Size(68, 51);
            this.cmdWorkStations.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdWorkStations.TabIndex = 2;
            this.cmdWorkStations.TabStop = false;
            this.cmdWorkStations.ToolTipText = null;
            this.cmdWorkStations.Click += new System.EventHandler(this.cmdWorkStations_Click);
            // 
            // cmdPacketSearch
            // 
            this.cmdPacketSearch.ActiveColor = System.Drawing.Color.Blue;
            this.cmdPacketSearch.BackColor = System.Drawing.Color.Transparent;
            this.cmdPacketSearch.IconType = FontAwesomeIcons.IconType.Search;
            this.cmdPacketSearch.InActiveColor = System.Drawing.Color.Black;
            this.cmdPacketSearch.Location = new System.Drawing.Point(613, 62);
            this.cmdPacketSearch.Name = "cmdPacketSearch";
            this.cmdPacketSearch.Size = new System.Drawing.Size(28, 26);
            this.cmdPacketSearch.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.cmdPacketSearch.TabIndex = 41;
            this.cmdPacketSearch.TabStop = false;
            this.cmdPacketSearch.ToolTipText = "Open Site Test Panel";
            this.cmdPacketSearch.Visible = false;
            this.cmdPacketSearch.Click += new System.EventHandler(this.iconButton11_Click);
            // 
            // frmMain
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1391, 447);
            this.ControlBox = false;
            this.Controls.Add(this.btnAdmin);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.iconButton10);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txtPacketSerialNo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.lblSearch);
            this.Controls.Add(this.lblChargers);
            this.Controls.Add(this.cmdBatteries);
            this.Controls.Add(this.lblBatteries);
            this.Controls.Add(this.lblSite);
            this.Controls.Add(this.cmdChargers);
            this.Controls.Add(this.cmdWorkStations);
            this.Controls.Add(this.cmdPacketSearch);
            this.Controls.Add(this.lblWorkstations);
            this.Controls.Add(this.gbManagerReports);
            this.Controls.Add(this.lblSiteId);
            this.Controls.Add(this.radPageView1);
            this.Controls.Add(this.lblPacketSerial);
            this.Controls.Add(this.radDropDownSites);
            this.Name = "frmMain";
            this.Padding = new System.Windows.Forms.Padding(1);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseDown);
            this.Resize += new System.EventHandler(this.frmMain_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.PageBattery.ResumeLayout(false);
            this.PageBattery.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewBatteries.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewBatteries)).EndInit();
            this.PageCharger.ResumeLayout(false);
            this.PageCharger.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewChargers.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewChargers)).EndInit();
            this.PageWorkStations.ResumeLayout(false);
            this.PageWorkStations.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewWorkstations.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewWorkstations)).EndInit();
            this.PagePackets.ResumeLayout(false);
            this.PagePackets.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSHutdown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewPackets.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewPackets)).EndInit();
            this.EngQS.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEngQS.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridViewEngQS)).EndInit();
            this.EngPackets.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radgridEngPackets.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radgridEngPackets)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton3)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDropDownSites)).EndInit();
            this.gbManagerReports.ResumeLayout(false);
            this.gbManagerReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.iconButton10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdBatteries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdChargers)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdWorkStations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPacketSearch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private Telerik.WinControls.UI.RadDesktopAlert radDesktopAlert1;
        private System.Windows.Forms.Timer tmrAlerts;
        private FontAwesomeIcons.IconButton cmdPacketSearch;
        private FontAwesomeIcons.IconButton cmdChargers;
        private FontAwesomeIcons.IconButton cmdWorkStations;
        private System.Windows.Forms.Label lblSite;
        private FontAwesomeIcons.IconButton cmdBatteries;
        private System.Windows.Forms.Label lblBatteries;
        private System.Windows.Forms.Label lblChargers;
        private System.Windows.Forms.Label lblWorkstations;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage PageBattery;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblBatteriesExporting;
        private Telerik.WinControls.UI.RadGridView radGridViewBatteries;
        private Telerik.WinControls.UI.RadPageViewPage PageCharger;
        public System.Windows.Forms.Label lblChargersExporting;
        private System.Windows.Forms.Button cmdExportChargers;
        private Telerik.WinControls.UI.RadGridView radGridViewChargers;
        private Telerik.WinControls.UI.RadPageViewPage PageWorkStations;
        public System.Windows.Forms.Label lblWorkstationsExporting;
        private System.Windows.Forms.Button cmdExportWorkstations;
        private Telerik.WinControls.UI.RadGridView radGridViewWorkstations;
        private Telerik.WinControls.UI.RadPageViewPage PagePackets;
        public System.Windows.Forms.Label lblPacketExport;
        private System.Windows.Forms.Button cmdExportPackets;
        private Telerik.WinControls.UI.RadGridView radGridViewPackets;
        private Telerik.WinControls.UI.RadPageViewPage EngQS;
        private Telerik.WinControls.UI.RadGridView radGridViewEngQS;
        private Telerik.WinControls.UI.RadPageViewPage EngPackets;
        private Telerik.WinControls.UI.RadGridView radgridEngPackets;
        private System.Windows.Forms.Label lblSiteId;
        private System.Windows.Forms.Label lblPacketSerial;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.GroupBox groupBox1;
        private FontAwesomeIcons.IconButton iconButton3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private FontAwesomeIcons.IconButton iconButton4;
        private FontAwesomeIcons.IconButton iconButton7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private FontAwesomeIcons.IconButton iconButton10;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPacketSerialNo;
        private Telerik.WinControls.UI.RadDropDownList radDropDownSites;
        private System.Windows.Forms.Label lblSearch;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gbManagerReports;
        private FontAwesomeIcons.IconButton iconButton8;
        private System.Windows.Forms.Label label8;
        private FontAwesomeIcons.IconButton iconButton5;
        private System.Windows.Forms.Label label5;
        private FontAwesomeIcons.IconButton iconButton6;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.ToolStripPanel BottomToolStripPanel;
        private System.Windows.Forms.ToolStripPanel TopToolStripPanel;
        private System.Windows.Forms.ToolStripPanel RightToolStripPanel;
        private System.Windows.Forms.ToolStripPanel LeftToolStripPanel;
        private System.Windows.Forms.ToolStripContentPanel ContentPanel;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button cmdExportBatteries;
        private FontAwesomeIcons.IconButton btnSHutdown;



    }
}

