﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RhythmField
{
    public partial class InputRecycle : Form
    {
        public string iUserID  { get; set; }

        public InputRecycle()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void RecycleAss()
        {
             string temps;
         
            txtSerialRecycle.Text = "'" + txtSerialRecycle.Text + "'";
            temps =   txtSerialRecycle.Text.Replace(System.Environment.NewLine, ",");
        temps =    temps.Replace("\r\n", ",").Replace("\n", ",").Replace("\r", ",");
          temps =   temps.Replace(",,",",");
           temps =  temps.Replace(",","','");
          temps =    temps.Replace(",''","");
         
          //  temps =   temps.Replace(System.Environment.NewLine,"");
            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SerialList",  temps },
                { "@SiteID",  "1125"}
            };
                MessageBox.Show("Successfully Recycled Assets");

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("sp_AssetsMove", parameters);
        }

        private void button1_Click(object sender, EventArgs e)
        {
           // RecycleAss();
            //return; 

            string temps;
             txtSerialRecycle.Text.Replace("'", "");
            txtSerialRecycle.Text.Replace("'3", "3");
            txtSerialRecycle.Text = "'" + txtSerialRecycle.Text + "'";
            temps =   txtSerialRecycle.Text.Replace(System.Environment.NewLine, ",");
        temps =    temps.Replace("\r\n", ",").Replace("\n", ",").Replace("\r", ",");
          temps =   temps.Replace(",,",",");
           temps =  temps.Replace(",","','");
          temps =    temps.Replace(",''","");

            Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@SerialList",  temps },
                 { "@iUserID",  iUserID  }
            };

            ApiLayer2 api = new ApiLayer2();
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("sp_RecycleAssets", parameters);
            MessageBox.Show("Successfully removed assets and assigned them to Enovate Recycle : " + txtSerialRecycle.Text);
        }
    }
}
