﻿﻿﻿/**********************************************************************************
 Program Name: BattStatD.cs
 Author: G.T. Sanford III
 Copyright (c) 2010 by Stinger Medical
 Created: 6/6/2010 at 6:47
-----------------------------------------------------------------------------------
 Description: 



-----------------------------------------------------------------------------------

                     M O D I F I C A T I O N    H I S T O R Y 

  Date    By   Vers   Modification
--------  ---  ----  --------------------------------------------------------------




------------------------------ ALL RIGHTS RESERVED --------------------------------
**********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace MobiusUSB
{
   public partial class BattStatD : Form
   {
      private string bitfields;

      public string BitFields
      {
         get
         {
            return bitfields;
         }
         set
         {
            bitfields = value;
         }
      }




      #region BattStatD 
      /****************************************************************************
           BattStatD                                              06/06/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      public BattStatD()
      {
         InitializeComponent();
      }
      /*--- end of BattStatD() --------------------------------------------------*/
      #endregion




      #region btnClose_Click 
      /****************************************************************************
           btnClose_Click                                         06/06/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnClose_Click( object sender, EventArgs e )
      {
         this.Close();
      }
      /*--- end of btnClose_Click() ---------------------------------------------*/
      #endregion




      #region HexToDecimal
      /****************************************************************************
           HexToDecimal                                           04/07/10
        Description: String hex value to integer value
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 8 hex characters max.
      ****************************************************************************/

      private Int32 HexToDecimal( string sHex )
      {
         Int32 value = 0;
         int len = sHex.Length;  // 8 
         Int32 Multiplier = 1;

         // FFFFFFFF 
         // 01234567 

         for ( int i = len - 1; i >= 0; i-- )
         {
            switch ( sHex[ i ] )
            {
               case '0':    value +=  0 * Multiplier;    break;
               case '1':    value +=  1 * Multiplier;    break;
               case '2':    value +=  2 * Multiplier;    break;
               case '3':    value +=  3 * Multiplier;    break;
               case '4':    value +=  4 * Multiplier;    break;
               case '5':    value +=  5 * Multiplier;    break;
               case '6':    value +=  6 * Multiplier;    break;
               case '7':    value +=  7 * Multiplier;    break;
               case '8':    value +=  8 * Multiplier;    break;
               case '9':    value +=  9 * Multiplier;    break;
               case 'a':    value += 10 * Multiplier;    break;
               case 'A':    value += 10 * Multiplier;    break;
               case 'b':    value += 11 * Multiplier;    break;
               case 'B':    value += 11 * Multiplier;    break;
               case 'c':    value += 12 * Multiplier;    break;
               case 'C':    value += 12 * Multiplier;    break;
               case 'd':    value += 13 * Multiplier;    break;
               case 'D':    value += 13 * Multiplier;    break;
               case 'e':    value += 14 * Multiplier;    break;
               case 'E':    value += 14 * Multiplier;    break;
               case 'f':    value += 15 * Multiplier;    break;
               case 'F':    value += 15 * Multiplier;    break;
            }
            Multiplier = (Multiplier * 16);
         }

         return( value );
      }
      /*--- end of HexToDecimal() -----------------------------------------------*/
      #endregion




      #region BattStatD_Load 
      /****************************************************************************
           BattStatD_Load                                         06/06/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void BattStatD_Load( object sender, EventArgs e )
      {
         Int32 ival = 0;

         //bitfields = "00C0";   // ZZZZZZZZZZZZZZZZZZZZ 

         txtBitField.Text = bitfields;

         try
         {
            ival = HexToDecimal( bitfields );
         }
         catch
         {
            ival = 0;
         }

         // upper row 
         if ( ( ival & 0x8000 ) == 0x8000 )        // OCA 
            label1.BackColor = Color.Pink;
         else
            label1.BackColor = Color.LightGreen;

         if ( ( ival & 0x4000 ) == 0x4000 )        // TCA 
            label2.BackColor = Color.Pink;
         else
            label2.BackColor = Color.LightGreen;

         if ( ( ival & 0x2000 ) == 0x2000 )        // RSVD 
            label3.BackColor = Color.Pink;
         else
            label3.BackColor = Color.LightGreen;

         if ( ( ival & 0x1000 ) == 0x1000 )        // OTA 
            label4.BackColor = Color.Pink;
         else
            label4.BackColor = Color.LightGreen;

         if ( ( ival & 0x0800 ) == 0x0800 )        // TDA 
            label5.BackColor = Color.Pink;
         else
            label5.BackColor = Color.LightGreen;

         if ( ( ival & 0x0400 ) == 0x0400 )        // RSVD 
            label6.BackColor = Color.Pink;
         else
            label6.BackColor = Color.LightGreen;

         if ( ( ival & 0x0200 ) == 0x0200 )        // RCA 
            label7.BackColor = Color.Pink;
         else
            label7.BackColor = Color.LightGreen;

         if ( ( ival & 0x0100 ) == 0x0100 )        // RTA 
            label8.BackColor = Color.Pink;
         else
            label8.BackColor = Color.LightGreen;

         // lower row 
         if ( ( ival & 0x0080 ) == 0x0080 )        // INIT 
            label9.BackColor = Color.LightGray;
         else
            label9.BackColor = Color.LightGreen;

         if ( ( ival & 0x0040 ) == 0x0040 )        // DSG 
            label10.BackColor = Color.LightGray;
         else
            label10.BackColor = Color.LightGreen;

         if ( ( ival & 0x0020 ) == 0x0020 )        // FC 
            label11.BackColor = Color.Pink;
         else
            label11.BackColor = Color.LightGreen;

         if ( ( ival & 0x0010 ) == 0x0010 )        // FD 
            label12.BackColor = Color.Pink;
         else
            label12.BackColor = Color.LightGreen;

         if ( ( ival & 0x0008 ) == 0x0008 )        // EC3 
            label13.BackColor = Color.Pink;
         else
            label13.BackColor = Color.LightGreen;

         if ( ( ival & 0x0004 ) == 0x0004 )        // EC2 
            label14.BackColor = Color.Pink;
         else
            label14.BackColor = Color.LightGreen;

         if ( ( ival & 0x0002 ) == 0x0002 )        // EC1 
            label15.BackColor = Color.Pink;
         else
            label15.BackColor = Color.LightGreen;

         if ( ( ival & 0x0001 ) == 0x0001 )        // EC0 
            label16.BackColor = Color.Pink;
         else
            label16.BackColor = Color.LightGreen;
      }
      /*--- end of BattStatD_Load() ---------------------------------------------*/
      #endregion


   }
}
/*--- end of BattStatD.cs -------------------------------------------------------*/

