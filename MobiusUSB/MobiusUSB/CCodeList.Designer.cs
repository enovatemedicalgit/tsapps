﻿﻿namespace MobiusUSB
{
   partial class CCodeList
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.btnClose = new System.Windows.Forms.Button();
         this.label10 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.label12 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.label14 = new System.Windows.Forms.Label();
         this.label15 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.label17 = new System.Windows.Forms.Label();
         this.label18 = new System.Windows.Forms.Label();
         this.label19 = new System.Windows.Forms.Label();
         this.label20 = new System.Windows.Forms.Label();
         this.label21 = new System.Windows.Forms.Label();
         this.label22 = new System.Windows.Forms.Label();
         this.label23 = new System.Windows.Forms.Label();
         this.label24 = new System.Windows.Forms.Label();
         this.label25 = new System.Windows.Forms.Label();
         this.label26 = new System.Windows.Forms.Label();
         this.label33 = new System.Windows.Forms.Label();
         this.label34 = new System.Windows.Forms.Label();
         this.label35 = new System.Windows.Forms.Label();
         this.label36 = new System.Windows.Forms.Label();
         this.label37 = new System.Windows.Forms.Label();
         this.label38 = new System.Windows.Forms.Label();
         this.label39 = new System.Windows.Forms.Label();
         this.label40 = new System.Windows.Forms.Label();
         this.label41 = new System.Windows.Forms.Label();
         this.label42 = new System.Windows.Forms.Label();
         this.label43 = new System.Windows.Forms.Label();
         this.label44 = new System.Windows.Forms.Label();
         this.label45 = new System.Windows.Forms.Label();
         this.label46 = new System.Windows.Forms.Label();
         this.label47 = new System.Windows.Forms.Label();
         this.label48 = new System.Windows.Forms.Label();
         this.label49 = new System.Windows.Forms.Label();
         this.label50 = new System.Windows.Forms.Label();
         this.label51 = new System.Windows.Forms.Label();
         this.label52 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.label27 = new System.Windows.Forms.Label();
         this.label28 = new System.Windows.Forms.Label();
         this.label29 = new System.Windows.Forms.Label();
         this.label30 = new System.Windows.Forms.Label();
         this.label31 = new System.Windows.Forms.Label();
         this.label32 = new System.Windows.Forms.Label();
         this.label53 = new System.Windows.Forms.Label();
         this.label54 = new System.Windows.Forms.Label();
         this.label55 = new System.Windows.Forms.Label();
         this.label56 = new System.Windows.Forms.Label();
         this.label57 = new System.Windows.Forms.Label();
         this.label58 = new System.Windows.Forms.Label();
         this.label59 = new System.Windows.Forms.Label();
         this.label60 = new System.Windows.Forms.Label();
         this.label61 = new System.Windows.Forms.Label();
         this.label62 = new System.Windows.Forms.Label();
         this.label63 = new System.Windows.Forms.Label();
         this.label64 = new System.Windows.Forms.Label();
         this.label65 = new System.Windows.Forms.Label();
         this.label66 = new System.Windows.Forms.Label();
         this.SuspendLayout();
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.Location = new System.Drawing.Point(13, 13);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(108, 17);
         this.label1.TabIndex = 0;
         this.label1.Text = "Control Board";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label2.Location = new System.Drawing.Point(13, 132);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(123, 17);
         this.label2.TabIndex = 1;
         this.label2.Text = "Holster Charger";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label3.Location = new System.Drawing.Point(13, 236);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(38, 17);
         this.label3.TabIndex = 2;
         this.label3.Text = "LCD";
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label4.Location = new System.Drawing.Point(205, 13);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(70, 17);
         this.label4.TabIndex = 3;
         this.label4.Text = "DC-DC 1";
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.Location = new System.Drawing.Point(205, 183);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(70, 17);
         this.label5.TabIndex = 4;
         this.label5.Text = "DC-DC 2";
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label8.Location = new System.Drawing.Point(366, 168);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(116, 17);
         this.label8.TabIndex = 7;
         this.label8.Text = "Mobius Battery";
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label9.Location = new System.Drawing.Point(621, 13);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(96, 17);
         this.label9.TabIndex = 8;
         this.label9.Text = "WiFi Module";
         // 
         // btnClose
         // 
         this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnClose.Location = new System.Drawing.Point(740, 335);
         this.btnClose.Name = "btnClose";
         this.btnClose.Size = new System.Drawing.Size(75, 23);
         this.btnClose.TabIndex = 9;
         this.btnClose.Text = "Close";
         this.btnClose.UseVisualStyleBackColor = true;
         this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Location = new System.Drawing.Point(13, 34);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(168, 13);
         this.label10.TabIndex = 10;
         this.label10.Text = "101 - Download Bootload Image []";
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(13, 51);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(134, 13);
         this.label11.TabIndex = 11;
         this.label11.Text = "102 - New Serial Number []";
         // 
         // label12
         // 
         this.label12.AutoSize = true;
         this.label12.Location = new System.Drawing.Point(13, 102);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(129, 13);
         this.label12.TabIndex = 12;
         this.label12.Text = "110 - Jump to BootLoader";
         // 
         // label13
         // 
         this.label13.AutoSize = true;
         this.label13.Location = new System.Drawing.Point(13, 153);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(99, 13);
         this.label13.TabIndex = 13;
         this.label13.Text = "201 - Reset System";
         // 
         // label14
         // 
         this.label14.AutoSize = true;
         this.label14.Location = new System.Drawing.Point(13, 169);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(113, 13);
         this.label14.TabIndex = 14;
         this.label14.Text = "202 - Turn Off Charger";
         // 
         // label15
         // 
         this.label15.AutoSize = true;
         this.label15.Location = new System.Drawing.Point(13, 186);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(113, 13);
         this.label15.TabIndex = 15;
         this.label15.Text = "203 - Turn On Charger";
         // 
         // label16
         // 
         this.label16.AutoSize = true;
         this.label16.Location = new System.Drawing.Point(13, 203);
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size(129, 13);
         this.label16.TabIndex = 16;
         this.label16.Text = "210 - Jump to BootLoader";
         // 
         // label17
         // 
         this.label17.AutoSize = true;
         this.label17.Location = new System.Drawing.Point(13, 257);
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size(106, 13);
         this.label17.TabIndex = 17;
         this.label17.Text = "301 - Flash Backlight";
         // 
         // label18
         // 
         this.label18.AutoSize = true;
         this.label18.Location = new System.Drawing.Point(13, 274);
         this.label18.Name = "label18";
         this.label18.Size = new System.Drawing.Size(91, 13);
         this.label18.TabIndex = 18;
         this.label18.Text = "302 - Single Beep";
         // 
         // label19
         // 
         this.label19.AutoSize = true;
         this.label19.Location = new System.Drawing.Point(13, 291);
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size(96, 13);
         this.label19.TabIndex = 19;
         this.label19.Text = "303 - Double Beep";
         // 
         // label20
         // 
         this.label20.AutoSize = true;
         this.label20.Location = new System.Drawing.Point(13, 308);
         this.label20.Name = "label20";
         this.label20.Size = new System.Drawing.Size(88, 13);
         this.label20.TabIndex = 20;
         this.label20.Text = "304 - Triple Beep";
         // 
         // label21
         // 
         this.label21.AutoSize = true;
         this.label21.Location = new System.Drawing.Point(13, 325);
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size(143, 13);
         this.label21.TabIndex = 21;
         this.label21.Text = "305 - LCD Brightness [0-100]";
         // 
         // label22
         // 
         this.label22.AutoSize = true;
         this.label22.Location = new System.Drawing.Point(205, 34);
         this.label22.Name = "label22";
         this.label22.Size = new System.Drawing.Size(102, 13);
         this.label22.TabIndex = 22;
         this.label22.Text = "401 - Lock Voltages";
         // 
         // label23
         // 
         this.label23.AutoSize = true;
         this.label23.Location = new System.Drawing.Point(205, 51);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(112, 13);
         this.label23.TabIndex = 23;
         this.label23.Text = "402 - Unlock Voltages";
         // 
         // label24
         // 
         this.label24.AutoSize = true;
         this.label24.Location = new System.Drawing.Point(205, 68);
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size(117, 13);
         this.label24.TabIndex = 24;
         this.label24.Text = "403 - Turn Off Output 1";
         // 
         // label25
         // 
         this.label25.AutoSize = true;
         this.label25.Location = new System.Drawing.Point(205, 85);
         this.label25.Name = "label25";
         this.label25.Size = new System.Drawing.Size(117, 13);
         this.label25.TabIndex = 25;
         this.label25.Text = "404 - Turn On Output 1";
         // 
         // label26
         // 
         this.label26.AutoSize = true;
         this.label26.Location = new System.Drawing.Point(205, 136);
         this.label26.Name = "label26";
         this.label26.Size = new System.Drawing.Size(129, 13);
         this.label26.TabIndex = 26;
         this.label26.Text = "410 - Jump to BootLoader";
         // 
         // label33
         // 
         this.label33.AutoSize = true;
         this.label33.Location = new System.Drawing.Point(366, 189);
         this.label33.Name = "label33";
         this.label33.Size = new System.Drawing.Size(121, 13);
         this.label33.TabIndex = 33;
         this.label33.Text = "801 - Flashing LED\'s On";
         // 
         // label34
         // 
         this.label34.AutoSize = true;
         this.label34.Location = new System.Drawing.Point(366, 206);
         this.label34.Name = "label34";
         this.label34.Size = new System.Drawing.Size(121, 13);
         this.label34.TabIndex = 34;
         this.label34.Text = "802 - Flashing LED\'s Off";
         // 
         // label35
         // 
         this.label35.AutoSize = true;
         this.label35.Location = new System.Drawing.Point(366, 223);
         this.label35.Name = "label35";
         this.label35.Size = new System.Drawing.Size(219, 13);
         this.label35.TabIndex = 35;
         this.label35.Text = "803 - Increment Max Cycle Count 100 Cycles";
         // 
         // label36
         // 
         this.label36.AutoSize = true;
         this.label36.Location = new System.Drawing.Point(366, 240);
         this.label36.Name = "label36";
         this.label36.Size = new System.Drawing.Size(219, 13);
         this.label36.TabIndex = 36;
         this.label36.Text = "804 - Increment Max Cycle Count 200 Cycles";
         // 
         // label37
         // 
         this.label37.AutoSize = true;
         this.label37.Location = new System.Drawing.Point(366, 257);
         this.label37.Name = "label37";
         this.label37.Size = new System.Drawing.Size(219, 13);
         this.label37.TabIndex = 37;
         this.label37.Text = "805 - Increment Max Cycle Count 600 Cycles";
         // 
         // label38
         // 
         this.label38.AutoSize = true;
         this.label38.Location = new System.Drawing.Point(366, 274);
         this.label38.Name = "label38";
         this.label38.Size = new System.Drawing.Size(174, 13);
         this.label38.TabIndex = 38;
         this.label38.Text = "806 - Turn Battery Off (Open FET\'s)";
         // 
         // label39
         // 
         this.label39.AutoSize = true;
         this.label39.Location = new System.Drawing.Point(366, 291);
         this.label39.Name = "label39";
         this.label39.Size = new System.Drawing.Size(174, 13);
         this.label39.TabIndex = 39;
         this.label39.Text = "807 - Turn Battery On (Close FET\'s)";
         // 
         // label40
         // 
         this.label40.AutoSize = true;
         this.label40.Location = new System.Drawing.Point(621, 34);
         this.label40.Name = "label40";
         this.label40.Size = new System.Drawing.Size(191, 13);
         this.label40.TabIndex = 40;
         this.label40.Text = "901 - Set Transmit Time to 20 Seconds";
         // 
         // label41
         // 
         this.label41.AutoSize = true;
         this.label41.Location = new System.Drawing.Point(621, 51);
         this.label41.Name = "label41";
         this.label41.Size = new System.Drawing.Size(175, 13);
         this.label41.TabIndex = 41;
         this.label41.Text = "902 - Set Transmit Time to 1 Minute";
         // 
         // label42
         // 
         this.label42.AutoSize = true;
         this.label42.Location = new System.Drawing.Point(621, 68);
         this.label42.Name = "label42";
         this.label42.Size = new System.Drawing.Size(180, 13);
         this.label42.TabIndex = 42;
         this.label42.Text = "903 - Set Transmit Time to 5 Minutes";
         // 
         // label43
         // 
         this.label43.AutoSize = true;
         this.label43.Location = new System.Drawing.Point(621, 85);
         this.label43.Name = "label43";
         this.label43.Size = new System.Drawing.Size(138, 13);
         this.label43.TabIndex = 43;
         this.label43.Text = "904 - Set Host IP Address []";
         // 
         // label44
         // 
         this.label44.AutoSize = true;
         this.label44.Location = new System.Drawing.Point(621, 102);
         this.label44.Name = "label44";
         this.label44.Size = new System.Drawing.Size(161, 13);
         this.label44.TabIndex = 44;
         this.label44.Text = "905 - Set Remote Port Number []";
         // 
         // label45
         // 
         this.label45.AutoSize = true;
         this.label45.Location = new System.Drawing.Point(621, 119);
         this.label45.Name = "label45";
         this.label45.Size = new System.Drawing.Size(106, 13);
         this.label45.TabIndex = 45;
         this.label45.Text = "906 - Save && Reboot";
         // 
         // label46
         // 
         this.label46.AutoSize = true;
         this.label46.Location = new System.Drawing.Point(621, 136);
         this.label46.Name = "label46";
         this.label46.Size = new System.Drawing.Size(130, 13);
         this.label46.TabIndex = 46;
         this.label46.Text = "907 - Generic Command []";
         // 
         // label47
         // 
         this.label47.AutoSize = true;
         this.label47.Location = new System.Drawing.Point(205, 306);
         this.label47.Name = "label47";
         this.label47.Size = new System.Drawing.Size(129, 13);
         this.label47.TabIndex = 51;
         this.label47.Text = "510 - Jump to BootLoader";
         // 
         // label48
         // 
         this.label48.AutoSize = true;
         this.label48.Location = new System.Drawing.Point(205, 289);
         this.label48.Name = "label48";
         this.label48.Size = new System.Drawing.Size(117, 13);
         this.label48.TabIndex = 50;
         this.label48.Text = "506 - Turn On Output 2";
         // 
         // label49
         // 
         this.label49.AutoSize = true;
         this.label49.Location = new System.Drawing.Point(205, 238);
         this.label49.Name = "label49";
         this.label49.Size = new System.Drawing.Size(117, 13);
         this.label49.TabIndex = 49;
         this.label49.Text = "503 - Turn Off Output 1";
         // 
         // label50
         // 
         this.label50.AutoSize = true;
         this.label50.Location = new System.Drawing.Point(205, 221);
         this.label50.Name = "label50";
         this.label50.Size = new System.Drawing.Size(112, 13);
         this.label50.TabIndex = 48;
         this.label50.Text = "502 - Unlock Voltages";
         // 
         // label51
         // 
         this.label51.AutoSize = true;
         this.label51.Location = new System.Drawing.Point(205, 204);
         this.label51.Name = "label51";
         this.label51.Size = new System.Drawing.Size(102, 13);
         this.label51.TabIndex = 47;
         this.label51.Text = "501 - Lock Voltages";
         // 
         // label52
         // 
         this.label52.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.label52.AutoSize = true;
         this.label52.Location = new System.Drawing.Point(598, 340);
         this.label52.Name = "label52";
         this.label52.Size = new System.Drawing.Size(112, 13);
         this.label52.TabIndex = 52;
         this.label52.Text = "[] = requires parameter";
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.Location = new System.Drawing.Point(366, 13);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(81, 17);
         this.label6.TabIndex = 53;
         this.label6.Text = "MedBoard";
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(366, 85);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(112, 13);
         this.label7.TabIndex = 57;
         this.label7.Text = "704 - Set Admin PIN []";
         // 
         // label27
         // 
         this.label27.AutoSize = true;
         this.label27.Location = new System.Drawing.Point(366, 68);
         this.label27.Name = "label27";
         this.label27.Size = new System.Drawing.Size(184, 13);
         this.label27.TabIndex = 56;
         this.label27.Text = "703 - Set Drawer Open Time [10-600]";
         // 
         // label28
         // 
         this.label28.AutoSize = true;
         this.label28.Location = new System.Drawing.Point(366, 51);
         this.label28.Name = "label28";
         this.label28.Size = new System.Drawing.Size(100, 13);
         this.label28.TabIndex = 55;
         this.label28.Text = "702 - Lock Drawers";
         // 
         // label29
         // 
         this.label29.AutoSize = true;
         this.label29.Location = new System.Drawing.Point(366, 34);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(110, 13);
         this.label29.TabIndex = 54;
         this.label29.Text = "701 - Unlock Drawers";
         // 
         // label30
         // 
         this.label30.AutoSize = true;
         this.label30.Location = new System.Drawing.Point(13, 68);
         this.label30.Name = "label30";
         this.label30.Size = new System.Drawing.Size(98, 13);
         this.label30.TabIndex = 58;
         this.label30.Text = "103 - Turn Off BUB";
         // 
         // label31
         // 
         this.label31.AutoSize = true;
         this.label31.Location = new System.Drawing.Point(366, 102);
         this.label31.Name = "label31";
         this.label31.Size = new System.Drawing.Size(147, 13);
         this.label31.TabIndex = 59;
         this.label31.Text = "706 - Set Buzzer Mute Time []";
         // 
         // label32
         // 
         this.label32.AutoSize = true;
         this.label32.Location = new System.Drawing.Point(621, 153);
         this.label32.Name = "label32";
         this.label32.Size = new System.Drawing.Size(140, 13);
         this.label32.TabIndex = 60;
         this.label32.Text = "908 - Send QC Data Packet";
         // 
         // label53
         // 
         this.label53.AutoSize = true;
         this.label53.Location = new System.Drawing.Point(366, 308);
         this.label53.Name = "label53";
         this.label53.Size = new System.Drawing.Size(234, 13);
         this.label53.TabIndex = 61;
         this.label53.Text = "808 - Increment Max Cycle Count XXX Cycles  []";
         // 
         // label54
         // 
         this.label54.AutoSize = true;
         this.label54.Location = new System.Drawing.Point(366, 325);
         this.label54.Name = "label54";
         this.label54.Size = new System.Drawing.Size(215, 13);
         this.label54.TabIndex = 62;
         this.label54.Text = "809 - Set Max Cycle Count to XXX Cycles  []";
         // 
         // label55
         // 
         this.label55.AutoSize = true;
         this.label55.Location = new System.Drawing.Point(621, 170);
         this.label55.Name = "label55";
         this.label55.Size = new System.Drawing.Size(123, 13);
         this.label55.TabIndex = 63;
         this.label55.Text = "909 - Set FTP Address []";
         // 
         // label56
         // 
         this.label56.AutoSize = true;
         this.label56.Location = new System.Drawing.Point(13, 85);
         this.label56.Name = "label56";
         this.label56.Size = new System.Drawing.Size(148, 13);
         this.label56.TabIndex = 64;
         this.label56.Text = "104 - Speaker Volume [0-100]";
         // 
         // label57
         // 
         this.label57.AutoSize = true;
         this.label57.Location = new System.Drawing.Point(621, 187);
         this.label57.Name = "label57";
         this.label57.Size = new System.Drawing.Size(132, 13);
         this.label57.TabIndex = 65;
         this.label57.Text = "910 - Send WLAN Params";
         // 
         // label58
         // 
         this.label58.AutoSize = true;
         this.label58.Location = new System.Drawing.Point(621, 204);
         this.label58.Name = "label58";
         this.label58.Size = new System.Drawing.Size(124, 13);
         this.label58.TabIndex = 66;
         this.label58.Text = "911 - Send Board Serials";
         // 
         // label59
         // 
         this.label59.AutoSize = true;
         this.label59.Location = new System.Drawing.Point(621, 221);
         this.label59.Name = "label59";
         this.label59.Size = new System.Drawing.Size(106, 13);
         this.label59.TabIndex = 67;
         this.label59.Text = "912 - Set CAST URL";
         // 
         // label60
         // 
         this.label60.AutoSize = true;
         this.label60.Location = new System.Drawing.Point(205, 102);
         this.label60.Name = "label60";
         this.label60.Size = new System.Drawing.Size(117, 13);
         this.label60.TabIndex = 68;
         this.label60.Text = "405 - Turn Off Output 2";
         // 
         // label61
         // 
         this.label61.AutoSize = true;
         this.label61.Location = new System.Drawing.Point(205, 119);
         this.label61.Name = "label61";
         this.label61.Size = new System.Drawing.Size(117, 13);
         this.label61.TabIndex = 69;
         this.label61.Text = "406 - Turn On Output 2";
         // 
         // label62
         // 
         this.label62.AutoSize = true;
         this.label62.Location = new System.Drawing.Point(205, 255);
         this.label62.Name = "label62";
         this.label62.Size = new System.Drawing.Size(117, 13);
         this.label62.TabIndex = 70;
         this.label62.Text = "504 - Turn On Output 1";
         // 
         // label63
         // 
         this.label63.AutoSize = true;
         this.label63.Location = new System.Drawing.Point(205, 272);
         this.label63.Name = "label63";
         this.label63.Size = new System.Drawing.Size(117, 13);
         this.label63.TabIndex = 71;
         this.label63.Text = "505 - Turn Off Output 2";
         // 
         // label64
         // 
         this.label64.AutoSize = true;
         this.label64.Location = new System.Drawing.Point(13, 342);
         this.label64.Name = "label64";
         this.label64.Size = new System.Drawing.Size(129, 13);
         this.label64.TabIndex = 72;
         this.label64.Text = "310 - Jump to BootLoader";
         // 
         // label65
         // 
         this.label65.AutoSize = true;
         this.label65.Location = new System.Drawing.Point(621, 238);
         this.label65.Name = "label65";
         this.label65.Size = new System.Drawing.Size(172, 13);
         this.label65.TabIndex = 73;
         this.label65.Text = "913 - Send Specific Data Packet []";
         // 
         // label66
         // 
         this.label66.AutoSize = true;
         this.label66.Location = new System.Drawing.Point(366, 119);
         this.label66.Name = "label66";
         this.label66.Size = new System.Drawing.Size(129, 13);
         this.label66.TabIndex = 74;
         this.label66.Text = "710 - Jump to BootLoader";
         // 
         // CCodeList
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(827, 370);
         this.Controls.Add(this.label66);
         this.Controls.Add(this.label65);
         this.Controls.Add(this.label64);
         this.Controls.Add(this.label63);
         this.Controls.Add(this.label62);
         this.Controls.Add(this.label61);
         this.Controls.Add(this.label60);
         this.Controls.Add(this.label59);
         this.Controls.Add(this.label58);
         this.Controls.Add(this.label57);
         this.Controls.Add(this.label56);
         this.Controls.Add(this.label55);
         this.Controls.Add(this.label54);
         this.Controls.Add(this.label53);
         this.Controls.Add(this.label32);
         this.Controls.Add(this.label31);
         this.Controls.Add(this.label30);
         this.Controls.Add(this.label7);
         this.Controls.Add(this.label27);
         this.Controls.Add(this.label28);
         this.Controls.Add(this.label29);
         this.Controls.Add(this.label6);
         this.Controls.Add(this.label52);
         this.Controls.Add(this.label47);
         this.Controls.Add(this.label48);
         this.Controls.Add(this.label49);
         this.Controls.Add(this.label50);
         this.Controls.Add(this.label51);
         this.Controls.Add(this.label46);
         this.Controls.Add(this.label45);
         this.Controls.Add(this.label44);
         this.Controls.Add(this.label43);
         this.Controls.Add(this.label42);
         this.Controls.Add(this.label41);
         this.Controls.Add(this.label40);
         this.Controls.Add(this.label39);
         this.Controls.Add(this.label38);
         this.Controls.Add(this.label37);
         this.Controls.Add(this.label36);
         this.Controls.Add(this.label35);
         this.Controls.Add(this.label34);
         this.Controls.Add(this.label33);
         this.Controls.Add(this.label26);
         this.Controls.Add(this.label25);
         this.Controls.Add(this.label24);
         this.Controls.Add(this.label23);
         this.Controls.Add(this.label22);
         this.Controls.Add(this.label21);
         this.Controls.Add(this.label20);
         this.Controls.Add(this.label19);
         this.Controls.Add(this.label18);
         this.Controls.Add(this.label17);
         this.Controls.Add(this.label16);
         this.Controls.Add(this.label15);
         this.Controls.Add(this.label14);
         this.Controls.Add(this.label13);
         this.Controls.Add(this.label12);
         this.Controls.Add(this.label11);
         this.Controls.Add(this.label10);
         this.Controls.Add(this.btnClose);
         this.Controls.Add(this.label9);
         this.Controls.Add(this.label8);
         this.Controls.Add(this.label5);
         this.Controls.Add(this.label4);
         this.Controls.Add(this.label3);
         this.Controls.Add(this.label2);
         this.Controls.Add(this.label1);
         this.Name = "CCodeList";
         this.Text = "Mobius 4 Command Code List";
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Button btnClose;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Label label33;
      private System.Windows.Forms.Label label34;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.Label label36;
      private System.Windows.Forms.Label label37;
      private System.Windows.Forms.Label label38;
      private System.Windows.Forms.Label label39;
      private System.Windows.Forms.Label label40;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.Label label44;
      private System.Windows.Forms.Label label45;
      private System.Windows.Forms.Label label46;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.Label label49;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label28;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.Label label53;
      private System.Windows.Forms.Label label54;
      private System.Windows.Forms.Label label55;
      private System.Windows.Forms.Label label56;
      private System.Windows.Forms.Label label57;
      private System.Windows.Forms.Label label58;
      private System.Windows.Forms.Label label59;
      private System.Windows.Forms.Label label60;
      private System.Windows.Forms.Label label61;
      private System.Windows.Forms.Label label62;
      private System.Windows.Forms.Label label63;
      private System.Windows.Forms.Label label64;
      private System.Windows.Forms.Label label65;
      private System.Windows.Forms.Label label66;
   }
}
