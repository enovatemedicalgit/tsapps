﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace ATUSBTrial
{
   public partial class ATUSBTrial : Form
   {
      bool ExpectReply = false;


      public ATUSBTrial()
      {
         InitializeComponent();
         txtStatus.Text = "Not Connected";
      }




      private void btnClose_Click( object sender, EventArgs e )
      {
         at_usb_api.CloseDevice();
         this.Close();
      }




      private void btnConnect_Click( object sender, EventArgs e )
      {
         at_usb_api.InitHID();

         bool ok = at_usb_api.ConnectDevice( txtVendorID.Text, txtProductID.Text );

         if ( ok )
            txtStatus.Text = "Connected";
         else
            txtStatus.Text = "NOT Connected";
      }




      private void btnWrite_Click( object sender, EventArgs e )
      {
         at_usb_api.WriteBytes( txtCommand.Text );
         ExpectReply = true;
         timer1.Enabled = true;
      }




      private string FormatData( string sin )
      {
         string sout = "";
         int  i;

         for ( i = 0; i < 64; i += 2 )
         {
            string stmp = sin[ i ].ToString() + sin[ i + 1 ].ToString();
            int iamt = Convert.ToInt32( stmp );
            if ( iamt > 0 && iamt < 32 )
            {
               stmp = sin[ i ].ToString() + sin[ i + 1 ].ToString() + sin[ i + 2 ].ToString();
               iamt = Convert.ToInt32( stmp );
               i++;
            }

            sout += Convert.ToChar( iamt );
         }

         return sout;
      }




      private void timer1_Tick( object sender, EventArgs e )
      {
         string sbuf = at_usb_api.ReadBytes();

         if ( sbuf.Length > 0 )
         {
            //  G - 9                 
            // 7145570000000000000000000000000000000000000000000000000000000000000
            // 01...|....1....|....2....|....3....|....4....|....5....|....6....|....7....|

            string sout = FormatData( sbuf );
            txtByte.Text = txtByte.Text + sbuf + Environment.NewLine;
            txtByte.Text = txtByte.Text + sout + Environment.NewLine + Environment.NewLine;
            txtByte.Refresh();

            ExpectReply = false;
            timer1.Enabled = false;
         }
      }









   }
}
