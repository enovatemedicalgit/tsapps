﻿﻿﻿﻿namespace MobiusUSB
{
   partial class MobiusUSB
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MobiusUSB));
         this.btnClose = new System.Windows.Forms.Button();
         this.tabControl1 = new System.Windows.Forms.TabControl();
         this.tabPage1 = new System.Windows.Forms.TabPage();
         this.btnPowerSource = new System.Windows.Forms.Button();
         this.txtPowerSource = new System.Windows.Forms.TextBox();
         this.btnBUBChrgOff = new System.Windows.Forms.Button();
         this.btnBUBChrgOn = new System.Windows.Forms.Button();
         this.txtForcedReboot = new System.Windows.Forms.TextBox();
         this.btnForcedReboot = new System.Windows.Forms.Button();
         this.groupBox18 = new System.Windows.Forms.GroupBox();
         this.btnXMax = new System.Windows.Forms.Button();
         this.btnYMax = new System.Windows.Forms.Button();
         this.btnZMax = new System.Windows.Forms.Button();
         this.btnResetMax = new System.Windows.Forms.Button();
         this.txtXMax = new System.Windows.Forms.TextBox();
         this.txtYMax = new System.Windows.Forms.TextBox();
         this.txtMotion = new System.Windows.Forms.TextBox();
         this.txtZMax = new System.Windows.Forms.TextBox();
         this.btnMotion = new System.Windows.Forms.Button();
         this.txtZValue = new System.Windows.Forms.TextBox();
         this.btnXValue = new System.Windows.Forms.Button();
         this.txtYValue = new System.Windows.Forms.TextBox();
         this.btnYValue = new System.Windows.Forms.Button();
         this.txtXValue = new System.Windows.Forms.TextBox();
         this.btnZValue = new System.Windows.Forms.Button();
         this.pictureBox2 = new System.Windows.Forms.PictureBox();
         this.ckbActivate = new System.Windows.Forms.CheckBox();
         this.label52 = new System.Windows.Forms.Label();
         this.txtSparePins = new System.Windows.Forms.TextBox();
         this.btnSparePins = new System.Windows.Forms.Button();
         this.btnRebootMobius = new System.Windows.Forms.Button();
         this.groupBox4 = new System.Windows.Forms.GroupBox();
         this.txtBootDevice = new System.Windows.Forms.TextBox();
         this.txtFWRev = new System.Windows.Forms.TextBox();
         this.txtPageCount = new System.Windows.Forms.TextBox();
         this.label51 = new System.Windows.Forms.Label();
         this.label50 = new System.Windows.Forms.Label();
         this.label49 = new System.Windows.Forms.Label();
         this.txtPageNo = new System.Windows.Forms.TextBox();
         this.btnWriteSPI = new System.Windows.Forms.Button();
         this.label32 = new System.Windows.Forms.Label();
         this.btnReadSPI = new System.Windows.Forms.Button();
         this.txtWriteSPI = new System.Windows.Forms.TextBox();
         this.txtReadSPI = new System.Windows.Forms.TextBox();
         this.txtBUBChargerStatus = new System.Windows.Forms.TextBox();
         this.btnBUBMeasuredVoltage = new System.Windows.Forms.Button();
         this.txtBUBMeasuredVoltage = new System.Windows.Forms.TextBox();
         this.btnBUBChargerStatus = new System.Windows.Forms.Button();
         this.txtCBFirmware = new System.Windows.Forms.TextBox();
         this.btnCBFirmware = new System.Windows.Forms.Button();
         this.tabPage2 = new System.Windows.Forms.TabPage();
         this.groupBox38 = new System.Windows.Forms.GroupBox();
         this.btnSetBuzzerMuteTime = new System.Windows.Forms.Button();
         this.btnGetBuzzerMuteTime = new System.Windows.Forms.Button();
         this.label160 = new System.Windows.Forms.Label();
         this.txtBuzzerMuteTime = new System.Windows.Forms.TextBox();
         this.groupBox11 = new System.Windows.Forms.GroupBox();
         this.btnTempMute = new System.Windows.Forms.Button();
         this.btnUnMuteBuzzer = new System.Windows.Forms.Button();
         this.btnSingleBeep = new System.Windows.Forms.Button();
         this.btnDoubleBeep = new System.Windows.Forms.Button();
         this.btnBuzzerState = new System.Windows.Forms.Button();
         this.btnTripleBeep = new System.Windows.Forms.Button();
         this.txtBuzzerState = new System.Windows.Forms.TextBox();
         this.btnMuteBuzzer = new System.Windows.Forms.Button();
         this.groupBox10 = new System.Windows.Forms.GroupBox();
         this.btnBLOn = new System.Windows.Forms.Button();
         this.txtBLightState = new System.Windows.Forms.TextBox();
         this.btnBLOff = new System.Windows.Forms.Button();
         this.btnBLightstate = new System.Windows.Forms.Button();
         this.btnFlashBL = new System.Windows.Forms.Button();
         this.btnLCDFirmware = new System.Windows.Forms.Button();
         this.txtLCDFirmware = new System.Windows.Forms.TextBox();
         this.tabPage3 = new System.Windows.Forms.TabPage();
         this.groupBox5 = new System.Windows.Forms.GroupBox();
         this.ckbACConnected = new System.Windows.Forms.CheckBox();
         this.btnACConnected = new System.Windows.Forms.Button();
         this.txtChargerCurrent = new System.Windows.Forms.TextBox();
         this.btnChargerCurrent = new System.Windows.Forms.Button();
         this.txtChargerVoltage = new System.Windows.Forms.TextBox();
         this.btnChargerVoltage = new System.Windows.Forms.Button();
         this.txtChargerStatus = new System.Windows.Forms.TextBox();
         this.ckbChargerOFF = new System.Windows.Forms.CheckBox();
         this.btnHChargerStatus = new System.Windows.Forms.Button();
         this.btnChargerOff = new System.Windows.Forms.Button();
         this.txtHCFirmware = new System.Windows.Forms.TextBox();
         this.btnHCFirmware = new System.Windows.Forms.Button();
         this.tabPage4 = new System.Windows.Forms.TabPage();
         this.txtAreVoltages1Locked = new System.Windows.Forms.TextBox();
         this.btnAreVoltages1Locked = new System.Windows.Forms.Button();
         this.label54 = new System.Windows.Forms.Label();
         this.btnEnableOutputs1 = new System.Windows.Forms.Button();
         this.btnDisableOutputs1 = new System.Windows.Forms.Button();
         this.btnUnLock1Voltages = new System.Windows.Forms.Button();
         this.btnLock1Voltages = new System.Windows.Forms.Button();
         this.btnEnableSet1 = new System.Windows.Forms.Button();
         this.label31 = new System.Windows.Forms.Label();
         this.label30 = new System.Windows.Forms.Label();
         this.label29 = new System.Windows.Forms.Label();
         this.groupBox1 = new System.Windows.Forms.GroupBox();
         this.pbxVoltageCh1A = new System.Windows.Forms.PictureBox();
         this.label99 = new System.Windows.Forms.Label();
         this.label36 = new System.Windows.Forms.Label();
         this.txtDC1AWatts = new System.Windows.Forms.TextBox();
         this.label20 = new System.Windows.Forms.Label();
         this.label21 = new System.Windows.Forms.Label();
         this.label22 = new System.Windows.Forms.Label();
         this.pbxFanOnCh1A = new System.Windows.Forms.PictureBox();
         this.label1 = new System.Windows.Forms.Label();
         this.btnDC1CurrA = new System.Windows.Forms.Button();
         this.pbxOutputCh1A = new System.Windows.Forms.PictureBox();
         this.txtDC1CurrA = new System.Windows.Forms.TextBox();
         this.txtDC1VoltA = new System.Windows.Forms.TextBox();
         this.btnDC1VoltA = new System.Windows.Forms.Button();
         this.pbxBusOnCh1A = new System.Windows.Forms.PictureBox();
         this.label23 = new System.Windows.Forms.Label();
         this.label24 = new System.Windows.Forms.Label();
         this.label25 = new System.Windows.Forms.Label();
         this.pbxShortCh1A = new System.Windows.Forms.PictureBox();
         this.label26 = new System.Windows.Forms.Label();
         this.btnSetVoltage1A = new System.Windows.Forms.Button();
         this.pbxFanBlockedCh1A = new System.Windows.Forms.PictureBox();
         this.label27 = new System.Windows.Forms.Label();
         this.label28 = new System.Windows.Forms.Label();
         this.VoltMeter1Ch2 = new Meter.Meter();
         this.txtVoltageSetting1A = new System.Windows.Forms.TextBox();
         this.btnVoltSetting1A = new System.Windows.Forms.Button();
         this.txtDC1Status = new System.Windows.Forms.TextBox();
         this.groupBox6 = new System.Windows.Forms.GroupBox();
         this.pbxVoltageCh1B = new System.Windows.Forms.PictureBox();
         this.label98 = new System.Windows.Forms.Label();
         this.label35 = new System.Windows.Forms.Label();
         this.txtDC1BWatts = new System.Windows.Forms.TextBox();
         this.VoltMeter1Ch1 = new Meter.Meter();
         this.label7 = new System.Windows.Forms.Label();
         this.btnDC1CurrB = new System.Windows.Forms.Button();
         this.label5 = new System.Windows.Forms.Label();
         this.btnDC1VoltB = new System.Windows.Forms.Button();
         this.label8 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.pbxFanOnCh1B = new System.Windows.Forms.PictureBox();
         this.pbxOutputCh1B = new System.Windows.Forms.PictureBox();
         this.pbxBusOnCh1B = new System.Windows.Forms.PictureBox();
         this.pbxFanBlockedCh1B = new System.Windows.Forms.PictureBox();
         this.label11 = new System.Windows.Forms.Label();
         this.pbxShortCh1B = new System.Windows.Forms.PictureBox();
         this.label16 = new System.Windows.Forms.Label();
         this.lblVoltage = new System.Windows.Forms.Label();
         this.btnSetVoltage1B = new System.Windows.Forms.Button();
         this.btnVoltSetting1B = new System.Windows.Forms.Button();
         this.label14 = new System.Windows.Forms.Label();
         this.txtVoltageSetting1B = new System.Windows.Forms.TextBox();
         this.txtDC1VoltB = new System.Windows.Forms.TextBox();
         this.label15 = new System.Windows.Forms.Label();
         this.txtDC1CurrB = new System.Windows.Forms.TextBox();
         this.label19 = new System.Windows.Forms.Label();
         this.txtDC1Errors = new System.Windows.Forms.TextBox();
         this.btnDC1Firmware = new System.Windows.Forms.Button();
         this.txtDC1Firmware = new System.Windows.Forms.TextBox();
         this.btnDC1Errors = new System.Windows.Forms.Button();
         this.btnDC1Status = new System.Windows.Forms.Button();
         this.tabPage5 = new System.Windows.Forms.TabPage();
         this.txtAreVoltages2Locked = new System.Windows.Forms.TextBox();
         this.btnAreVoltages2Locked = new System.Windows.Forms.Button();
         this.btnEnableOutputs2 = new System.Windows.Forms.Button();
         this.btnDisableOutputs2 = new System.Windows.Forms.Button();
         this.btnUnLock2Voltages = new System.Windows.Forms.Button();
         this.btnLock2Voltages = new System.Windows.Forms.Button();
         this.btnEnableSet2 = new System.Windows.Forms.Button();
         this.label58 = new System.Windows.Forms.Label();
         this.label59 = new System.Windows.Forms.Label();
         this.label60 = new System.Windows.Forms.Label();
         this.label55 = new System.Windows.Forms.Label();
         this.groupBox17 = new System.Windows.Forms.GroupBox();
         this.pbxVoltageCh2A = new System.Windows.Forms.PictureBox();
         this.label101 = new System.Windows.Forms.Label();
         this.label69 = new System.Windows.Forms.Label();
         this.label70 = new System.Windows.Forms.Label();
         this.label71 = new System.Windows.Forms.Label();
         this.pbxFanOnCh2A = new System.Windows.Forms.PictureBox();
         this.pbxOutputCh2A = new System.Windows.Forms.PictureBox();
         this.pbxBusOnCh2A = new System.Windows.Forms.PictureBox();
         this.label72 = new System.Windows.Forms.Label();
         this.pbxShortCh2A = new System.Windows.Forms.PictureBox();
         this.label73 = new System.Windows.Forms.Label();
         this.pbxFanBlockedCh2A = new System.Windows.Forms.PictureBox();
         this.btnSetVoltage2A = new System.Windows.Forms.Button();
         this.btnVoltSetting2A = new System.Windows.Forms.Button();
         this.txtVoltageSetting2A = new System.Windows.Forms.TextBox();
         this.label63 = new System.Windows.Forms.Label();
         this.label57 = new System.Windows.Forms.Label();
         this.VoltMeter2Ch2 = new Meter.Meter();
         this.btnDC2VoltA = new System.Windows.Forms.Button();
         this.txtDC2CurrA = new System.Windows.Forms.TextBox();
         this.label44 = new System.Windows.Forms.Label();
         this.txtDC2VoltA = new System.Windows.Forms.TextBox();
         this.txtDC2AWatts = new System.Windows.Forms.TextBox();
         this.label18 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.btnDC2CurrA = new System.Windows.Forms.Button();
         this.label10 = new System.Windows.Forms.Label();
         this.groupBox16 = new System.Windows.Forms.GroupBox();
         this.pbxVoltageCh2B = new System.Windows.Forms.PictureBox();
         this.label100 = new System.Windows.Forms.Label();
         this.label64 = new System.Windows.Forms.Label();
         this.label65 = new System.Windows.Forms.Label();
         this.label66 = new System.Windows.Forms.Label();
         this.pbxFanOnCh2B = new System.Windows.Forms.PictureBox();
         this.pbxOutputCh2B = new System.Windows.Forms.PictureBox();
         this.pbxBusOnCh2B = new System.Windows.Forms.PictureBox();
         this.pbxFanBlockedCh2B = new System.Windows.Forms.PictureBox();
         this.label67 = new System.Windows.Forms.Label();
         this.pbxShortCh2B = new System.Windows.Forms.PictureBox();
         this.label68 = new System.Windows.Forms.Label();
         this.btnSetVoltage2B = new System.Windows.Forms.Button();
         this.btnVoltSetting2B = new System.Windows.Forms.Button();
         this.txtVoltageSetting2B = new System.Windows.Forms.TextBox();
         this.label53 = new System.Windows.Forms.Label();
         this.label56 = new System.Windows.Forms.Label();
         this.VoltMeter2Ch1 = new Meter.Meter();
         this.btnDC2VoltB = new System.Windows.Forms.Button();
         this.label45 = new System.Windows.Forms.Label();
         this.txtDC2CurrB = new System.Windows.Forms.TextBox();
         this.txtDC2VoltB = new System.Windows.Forms.TextBox();
         this.txtDC2BWatts = new System.Windows.Forms.TextBox();
         this.label17 = new System.Windows.Forms.Label();
         this.label12 = new System.Windows.Forms.Label();
         this.btnDC2CurrB = new System.Windows.Forms.Button();
         this.label6 = new System.Windows.Forms.Label();
         this.txtDC2Status = new System.Windows.Forms.TextBox();
         this.txtDC2Errors = new System.Windows.Forms.TextBox();
         this.btnDC2Status = new System.Windows.Forms.Button();
         this.btnDC2Errors = new System.Windows.Forms.Button();
         this.btnDC2Firmware = new System.Windows.Forms.Button();
         this.txtDC2Firmware = new System.Windows.Forms.TextBox();
         this.tabPage6 = new System.Windows.Forms.TabPage();
         this.btnCommFromFlash = new System.Windows.Forms.Button();
         this.groupBox25 = new System.Windows.Forms.GroupBox();
         this.btnSetCaptureURL = new System.Windows.Forms.Button();
         this.btnGetCaptureURL = new System.Windows.Forms.Button();
         this.txtCaptureURL = new System.Windows.Forms.TextBox();
         this.btnSendWLAN = new System.Windows.Forms.Button();
         this.groupBox8 = new System.Windows.Forms.GroupBox();
         this.btnSetTxInterval = new System.Windows.Forms.Button();
         this.btnGetTxInterval = new System.Windows.Forms.Button();
         this.label33 = new System.Windows.Forms.Label();
         this.txtTxInterval = new System.Windows.Forms.TextBox();
         this.btnResetBaudSetFlag = new System.Windows.Forms.Button();
         this.pictureBox1 = new System.Windows.Forms.PictureBox();
         this.lblWLAN = new System.Windows.Forms.Label();
         this.txtWLAN = new System.Windows.Forms.TextBox();
         this.btnGetWLAN = new System.Windows.Forms.Button();
         this.txtDeviceMACAddress = new System.Windows.Forms.TextBox();
         this.btnDeviceMACAddress = new System.Windows.Forms.Button();
         this.groupBox20 = new System.Windows.Forms.GroupBox();
         this.label161 = new System.Windows.Forms.Label();
         this.txtPIN = new System.Windows.Forms.TextBox();
         this.rdoDrawersUnlockedDP = new System.Windows.Forms.RadioButton();
         this.txtServiceRequest = new System.Windows.Forms.TextBox();
         this.rdoServiceRequestDP = new System.Windows.Forms.RadioButton();
         this.rdoMedBoardStatsDP = new System.Windows.Forms.RadioButton();
         this.rdoBoardSerialsDP = new System.Windows.Forms.RadioButton();
         this.rdoWLANDP = new System.Windows.Forms.RadioButton();
         this.rdoAlertDP = new System.Windows.Forms.RadioButton();
         this.rdoQCDP = new System.Windows.Forms.RadioButton();
         this.rdoCommCodeAckDP = new System.Windows.Forms.RadioButton();
         this.rdoRebootRequestDP = new System.Windows.Forms.RadioButton();
         this.rdoBatteryDepletedDP = new System.Windows.Forms.RadioButton();
         this.rdoACDisconnectedDP = new System.Windows.Forms.RadioButton();
         this.rdoACConnectedDP = new System.Windows.Forms.RadioButton();
         this.rdoHeartBeatDP = new System.Windows.Forms.RadioButton();
         this.rdoBatteryRemovedDP = new System.Windows.Forms.RadioButton();
         this.rdoBatteryInsertedDP = new System.Windows.Forms.RadioButton();
         this.rdoStandardDP = new System.Windows.Forms.RadioButton();
         this.btnSendData = new System.Windows.Forms.Button();
         this.txtLinkQuality = new System.Windows.Forms.TextBox();
         this.txtMACAddress = new System.Windows.Forms.TextBox();
         this.txtIPAddress = new System.Windows.Forms.TextBox();
         this.btnLinkQuality = new System.Windows.Forms.Button();
         this.btnMACAddress = new System.Windows.Forms.Button();
         this.btnIPAddress = new System.Windows.Forms.Button();
         this.btnWiFiFirmware = new System.Windows.Forms.Button();
         this.txtWiFiFirmware = new System.Windows.Forms.TextBox();
         this.tabPage7 = new System.Windows.Forms.TabPage();
         this.label127 = new System.Windows.Forms.Label();
         this.label126 = new System.Windows.Forms.Label();
         this.label125 = new System.Windows.Forms.Label();
         this.label124 = new System.Windows.Forms.Label();
         this.label123 = new System.Windows.Forms.Label();
         this.label122 = new System.Windows.Forms.Label();
         this.label121 = new System.Windows.Forms.Label();
         this.label120 = new System.Windows.Forms.Label();
         this.label119 = new System.Windows.Forms.Label();
         this.btnPollBattery = new System.Windows.Forms.Button();
         this.label118 = new System.Windows.Forms.Label();
         this.label117 = new System.Windows.Forms.Label();
         this.label116 = new System.Windows.Forms.Label();
         this.label115 = new System.Windows.Forms.Label();
         this.label114 = new System.Windows.Forms.Label();
         this.label113 = new System.Windows.Forms.Label();
         this.label112 = new System.Windows.Forms.Label();
         this.label111 = new System.Windows.Forms.Label();
         this.label110 = new System.Windows.Forms.Label();
         this.label109 = new System.Windows.Forms.Label();
         this.label108 = new System.Windows.Forms.Label();
         this.label107 = new System.Windows.Forms.Label();
         this.label106 = new System.Windows.Forms.Label();
         this.label105 = new System.Windows.Forms.Label();
         this.txtMainBatteryOut = new System.Windows.Forms.TextBox();
         this.btnMainBatteryOut = new System.Windows.Forms.Button();
         this.label48 = new System.Windows.Forms.Label();
         this.txtThermTemp = new System.Windows.Forms.TextBox();
         this.label47 = new System.Windows.Forms.Label();
         this.txtEfficiency = new System.Windows.Forms.TextBox();
         this.label46 = new System.Windows.Forms.Label();
         this.groupBox35 = new System.Windows.Forms.GroupBox();
         this.btnHexConvert = new System.Windows.Forms.Button();
         this.txtHexValue = new System.Windows.Forms.TextBox();
         this.txtDecimalValue = new System.Windows.Forms.TextBox();
         this.label103 = new System.Windows.Forms.Label();
         this.label102 = new System.Windows.Forms.Label();
         this.groupBox2 = new System.Windows.Forms.GroupBox();
         this.btnOpenDischargeFET = new System.Windows.Forms.Button();
         this.btnOpenFETs = new System.Windows.Forms.Button();
         this.btnUnlockPack = new System.Windows.Forms.Button();
         this.btnLockPack = new System.Windows.Forms.Button();
         this.btnCloseFETs = new System.Windows.Forms.Button();
         this.btnResetPack = new System.Windows.Forms.Button();
         this.btnResetPermFailure = new System.Windows.Forms.Button();
         this.btnBattModeD = new System.Windows.Forms.Button();
         this.btnChargeStatD = new System.Windows.Forms.Button();
         this.btnSafeAlertD = new System.Windows.Forms.Button();
         this.btnPFAlertD = new System.Windows.Forms.Button();
         this.btnPFStatD = new System.Windows.Forms.Button();
         this.btnSafeStatD = new System.Windows.Forms.Button();
         this.btnBattStatD = new System.Windows.Forms.Button();
         this.btnOpStatD = new System.Windows.Forms.Button();
         this.txtBatteryMode = new System.Windows.Forms.TextBox();
         this.txtChargingStatus = new System.Windows.Forms.TextBox();
         this.txtSafetyAlert = new System.Windows.Forms.TextBox();
         this.txtPFAlert = new System.Windows.Forms.TextBox();
         this.txtPFStatus = new System.Windows.Forms.TextBox();
         this.txtSafetyStatus = new System.Windows.Forms.TextBox();
         this.txtBatteryStatus = new System.Windows.Forms.TextBox();
         this.txtOperationStatus = new System.Windows.Forms.TextBox();
         this.txtFETStatus = new System.Windows.Forms.TextBox();
         this.groupBox7 = new System.Windows.Forms.GroupBox();
         this.btnBatteryErrors = new System.Windows.Forms.Button();
         this.rdoError0 = new System.Windows.Forms.RadioButton();
         this.rdoError5 = new System.Windows.Forms.RadioButton();
         this.rdoError4 = new System.Windows.Forms.RadioButton();
         this.rdoError3 = new System.Windows.Forms.RadioButton();
         this.rdoError2 = new System.Windows.Forms.RadioButton();
         this.rdoError1 = new System.Windows.Forms.RadioButton();
         this.label104 = new System.Windows.Forms.Label();
         this.txtMeasuredVoltage = new System.Windows.Forms.TextBox();
         this.label43 = new System.Windows.Forms.Label();
         this.txtBattWatts = new System.Windows.Forms.TextBox();
         this.txtCell3 = new System.Windows.Forms.TextBox();
         this.txtCell2 = new System.Windows.Forms.TextBox();
         this.txtCell1 = new System.Windows.Forms.TextBox();
         this.label40 = new System.Windows.Forms.Label();
         this.txtBatCurrent = new System.Windows.Forms.TextBox();
         this.label4 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.txtBatteryName = new System.Windows.Forms.TextBox();
         this.lblPercent = new System.Windows.Forms.Label();
         this.progressBar1 = new System.Windows.Forms.ProgressBar();
         this.txtBatCycle = new System.Windows.Forms.TextBox();
         this.txtBatSerial = new System.Windows.Forms.TextBox();
         this.txtBatRemain = new System.Windows.Forms.TextBox();
         this.txtBatTemp = new System.Windows.Forms.TextBox();
         this.txtBatVoltage = new System.Windows.Forms.TextBox();
         this.txtBatCapacity = new System.Windows.Forms.TextBox();
         this.pictureBox3 = new System.Windows.Forms.PictureBox();
         this.groupBox19 = new System.Windows.Forms.GroupBox();
         this.btnFlashLEDs = new System.Windows.Forms.Button();
         this.btnIsFlashing = new System.Windows.Forms.Button();
         this.txtIsFlashing = new System.Windows.Forms.TextBox();
         this.btnLEDsOff = new System.Windows.Forms.Button();
         this.groupBox15 = new System.Windows.Forms.GroupBox();
         this.btnSetMaxCC = new System.Windows.Forms.Button();
         this.btnGetMaxCC = new System.Windows.Forms.Button();
         this.txtMaxCC = new System.Windows.Forms.TextBox();
         this.tabPage13 = new System.Windows.Forms.TabPage();
         this.btnPollBUB = new System.Windows.Forms.Button();
         this.label146 = new System.Windows.Forms.Label();
         this.label152 = new System.Windows.Forms.Label();
         this.label153 = new System.Windows.Forms.Label();
         this.label154 = new System.Windows.Forms.Label();
         this.label155 = new System.Windows.Forms.Label();
         this.label156 = new System.Windows.Forms.Label();
         this.label157 = new System.Windows.Forms.Label();
         this.label158 = new System.Windows.Forms.Label();
         this.label159 = new System.Windows.Forms.Label();
         this.btnBUBBattModeD = new System.Windows.Forms.Button();
         this.btnBUBChargeStatD = new System.Windows.Forms.Button();
         this.btnBUBSafeAlertD = new System.Windows.Forms.Button();
         this.btnBUBPFAlertD = new System.Windows.Forms.Button();
         this.btnBUBPFStatD = new System.Windows.Forms.Button();
         this.btnBUBSafeStatD = new System.Windows.Forms.Button();
         this.btnBUBBattStatD = new System.Windows.Forms.Button();
         this.btnBUBOpStatD = new System.Windows.Forms.Button();
         this.txtBUBBatteryMode = new System.Windows.Forms.TextBox();
         this.txtBUBChargingStatus = new System.Windows.Forms.TextBox();
         this.txtBUBSafetyAlert = new System.Windows.Forms.TextBox();
         this.txtBUBPFAlert = new System.Windows.Forms.TextBox();
         this.txtBUBPFStatus = new System.Windows.Forms.TextBox();
         this.txtBUBSafetyStatus = new System.Windows.Forms.TextBox();
         this.txtBUBBatteryStatus = new System.Windows.Forms.TextBox();
         this.txtBUBOperationStatus = new System.Windows.Forms.TextBox();
         this.txtBUBFETStatus = new System.Windows.Forms.TextBox();
         this.label128 = new System.Windows.Forms.Label();
         this.label129 = new System.Windows.Forms.Label();
         this.label130 = new System.Windows.Forms.Label();
         this.label131 = new System.Windows.Forms.Label();
         this.label132 = new System.Windows.Forms.Label();
         this.label133 = new System.Windows.Forms.Label();
         this.label136 = new System.Windows.Forms.Label();
         this.label140 = new System.Windows.Forms.Label();
         this.label141 = new System.Windows.Forms.Label();
         this.label142 = new System.Windows.Forms.Label();
         this.label143 = new System.Windows.Forms.Label();
         this.label144 = new System.Windows.Forms.Label();
         this.label145 = new System.Windows.Forms.Label();
         this.txtBUBMeasuredVoltage2 = new System.Windows.Forms.TextBox();
         this.txtBUBCell2Voltage = new System.Windows.Forms.TextBox();
         this.txtBUBCell1Voltage = new System.Windows.Forms.TextBox();
         this.label147 = new System.Windows.Forms.Label();
         this.txtBUBCurrent = new System.Windows.Forms.TextBox();
         this.label148 = new System.Windows.Forms.Label();
         this.label149 = new System.Windows.Forms.Label();
         this.label150 = new System.Windows.Forms.Label();
         this.txtBUBatteryName = new System.Windows.Forms.TextBox();
         this.lblBUBPercent = new System.Windows.Forms.Label();
         this.pBarBUBChargeLevel = new System.Windows.Forms.ProgressBar();
         this.txtBUBCycleCount = new System.Windows.Forms.TextBox();
         this.txtBUBSerialNumber = new System.Windows.Forms.TextBox();
         this.txtBUBRemainingTime = new System.Windows.Forms.TextBox();
         this.txtBUBTemperature = new System.Windows.Forms.TextBox();
         this.txtBUBVoltage = new System.Windows.Forms.TextBox();
         this.txtBUBFCC = new System.Windows.Forms.TextBox();
         this.tabPage8 = new System.Windows.Forms.TabPage();
         this.label85 = new System.Windows.Forms.Label();
         this.label84 = new System.Windows.Forms.Label();
         this.label83 = new System.Windows.Forms.Label();
         this.label82 = new System.Windows.Forms.Label();
         this.label81 = new System.Windows.Forms.Label();
         this.label80 = new System.Windows.Forms.Label();
         this.label79 = new System.Windows.Forms.Label();
         this.pbxRebootWiFi = new System.Windows.Forms.PictureBox();
         this.pbxSaveConfiguration = new System.Windows.Forms.PictureBox();
         this.pbxSetExternalAntenna = new System.Windows.Forms.PictureBox();
         this.pbxSetCommTimeout = new System.Windows.Forms.PictureBox();
         this.pbxSetCommSize = new System.Windows.Forms.PictureBox();
         this.pbxSetRemoteResponse = new System.Windows.Forms.PictureBox();
         this.pbxSetRemotePort = new System.Windows.Forms.PictureBox();
         this.label78 = new System.Windows.Forms.Label();
         this.pbxSetHostIPAddress = new System.Windows.Forms.PictureBox();
         this.label77 = new System.Windows.Forms.Label();
         this.pbxSetSSID = new System.Windows.Forms.PictureBox();
         this.label76 = new System.Windows.Forms.Label();
         this.pbxBaudRate = new System.Windows.Forms.PictureBox();
         this.label75 = new System.Windows.Forms.Label();
         this.pbxCommandMode = new System.Windows.Forms.PictureBox();
         this.ckbNewDevice = new System.Windows.Forms.CheckBox();
         this.btnSetAllParameters = new System.Windows.Forms.Button();
         this.groupBox9 = new System.Windows.Forms.GroupBox();
         this.rdoExternal = new System.Windows.Forms.RadioButton();
         this.btnSendWiFi = new System.Windows.Forms.Button();
         this.rdoInternal = new System.Windows.Forms.RadioButton();
         this.btnSetBaudHigh = new System.Windows.Forms.Button();
         this.btnUpdateRoving = new System.Windows.Forms.Button();
         this.btnSetBaudLow = new System.Windows.Forms.Button();
         this.label34 = new System.Windows.Forms.Label();
         this.txtWiFiResponse = new System.Windows.Forms.TextBox();
         this.btnWiFiSend = new System.Windows.Forms.Button();
         this.txtWiFiCommand = new System.Windows.Forms.TextBox();
         this.btnFactoryDefault = new System.Windows.Forms.Button();
         this.btnSetAdHocMode = new System.Windows.Forms.Button();
         this.txtReboot = new System.Windows.Forms.TextBox();
         this.txtSave = new System.Windows.Forms.TextBox();
         this.txtExtAnt = new System.Windows.Forms.TextBox();
         this.txtCommTime = new System.Windows.Forms.TextBox();
         this.txtCommSize = new System.Windows.Forms.TextBox();
         this.txtRemoteResponse = new System.Windows.Forms.TextBox();
         this.txtRemotePort = new System.Windows.Forms.TextBox();
         this.txtHostAddress = new System.Windows.Forms.TextBox();
         this.txtSSID = new System.Windows.Forms.TextBox();
         this.txtBaudRate = new System.Windows.Forms.TextBox();
         this.txtEnterCommandMode = new System.Windows.Forms.TextBox();
         this.btnReboot = new System.Windows.Forms.Button();
         this.btnSave = new System.Windows.Forms.Button();
         this.btnExtAnt = new System.Windows.Forms.Button();
         this.btnCommTime = new System.Windows.Forms.Button();
         this.btnCommSize = new System.Windows.Forms.Button();
         this.btnRemoteResponse = new System.Windows.Forms.Button();
         this.btnRemotePort = new System.Windows.Forms.Button();
         this.btnHostAddress = new System.Windows.Forms.Button();
         this.btnSetSSID = new System.Windows.Forms.Button();
         this.btnSetBaudRate = new System.Windows.Forms.Button();
         this.btnCommandMode = new System.Windows.Forms.Button();
         this.tabPage10 = new System.Windows.Forms.TabPage();
         this.label87 = new System.Windows.Forms.Label();
         this.pbxSetChannel = new System.Windows.Forms.PictureBox();
         this.groupBox27 = new System.Windows.Forms.GroupBox();
         this.label95 = new System.Windows.Forms.Label();
         this.txtDNSServer = new System.Windows.Forms.TextBox();
         this.txtGateway = new System.Windows.Forms.TextBox();
         this.txtSubnetMask = new System.Windows.Forms.TextBox();
         this.label96 = new System.Windows.Forms.Label();
         this.label97 = new System.Windows.Forms.Label();
         this.label89 = new System.Windows.Forms.Label();
         this.pbxSetDHCP = new System.Windows.Forms.PictureBox();
         this.btnSetDHCP = new System.Windows.Forms.Button();
         this.rdoDHCPCache = new System.Windows.Forms.RadioButton();
         this.txtDHCPIPAddress = new System.Windows.Forms.TextBox();
         this.label139 = new System.Windows.Forms.Label();
         this.rdoAutoIP = new System.Windows.Forms.RadioButton();
         this.rdoDHCPOn = new System.Windows.Forms.RadioButton();
         this.rdoDHCPOff = new System.Windows.Forms.RadioButton();
         this.label138 = new System.Windows.Forms.Label();
         this.txtChannel = new System.Windows.Forms.TextBox();
         this.btnSetChannel = new System.Windows.Forms.Button();
         this.groupBox28 = new System.Windows.Forms.GroupBox();
         this.label88 = new System.Windows.Forms.Label();
         this.pbxSetJoin = new System.Windows.Forms.PictureBox();
         this.btnSetJoin = new System.Windows.Forms.Button();
         this.rdoAdHoc = new System.Windows.Forms.RadioButton();
         this.rdoSecurity = new System.Windows.Forms.RadioButton();
         this.rdoSSID = new System.Windows.Forms.RadioButton();
         this.rdoManual = new System.Windows.Forms.RadioButton();
         this.groupBox29 = new System.Windows.Forms.GroupBox();
         this.groupBox34 = new System.Windows.Forms.GroupBox();
         this.rdo26HEX = new System.Windows.Forms.RadioButton();
         this.rdo13ASCII = new System.Windows.Forms.RadioButton();
         this.label86 = new System.Windows.Forms.Label();
         this.pbxSetAuth = new System.Windows.Forms.PictureBox();
         this.label134 = new System.Windows.Forms.Label();
         this.label135 = new System.Windows.Forms.Label();
         this.label137 = new System.Windows.Forms.Label();
         this.txtPassPhrase = new System.Windows.Forms.TextBox();
         this.txtWEPKey = new System.Windows.Forms.TextBox();
         this.btnSetAuth = new System.Windows.Forms.Button();
         this.rdoWPA2 = new System.Windows.Forms.RadioButton();
         this.rdoMixedWPA = new System.Windows.Forms.RadioButton();
         this.rdoWPA1 = new System.Windows.Forms.RadioButton();
         this.rdoWEP128 = new System.Windows.Forms.RadioButton();
         this.rdoOpen = new System.Windows.Forms.RadioButton();
         this.tabPage9 = new System.Windows.Forms.TabPage();
         this.groupBox33 = new System.Windows.Forms.GroupBox();
         this.btnGetMOTSerial = new System.Windows.Forms.Button();
         this.btnSetMOTSerial = new System.Windows.Forms.Button();
         this.txtBatterySerial = new System.Windows.Forms.TextBox();
         this.groupBox32 = new System.Windows.Forms.GroupBox();
         this.btnSetMEDSerial = new System.Windows.Forms.Button();
         this.btnGetMEDSerial = new System.Windows.Forms.Button();
         this.txtMEDSerial = new System.Windows.Forms.TextBox();
         this.btnSendBoardSerials = new System.Windows.Forms.Button();
         this.btnGetAllSerialNumbers = new System.Windows.Forms.Button();
         this.groupBox26 = new System.Windows.Forms.GroupBox();
         this.btnSetDC2Serial = new System.Windows.Forms.Button();
         this.btnGetDC2Serial = new System.Windows.Forms.Button();
         this.txtDC2Serial = new System.Windows.Forms.TextBox();
         this.groupBox24 = new System.Windows.Forms.GroupBox();
         this.btnSetLCDSerial = new System.Windows.Forms.Button();
         this.btnGetLCDSerial = new System.Windows.Forms.Button();
         this.txtLCDSerial = new System.Windows.Forms.TextBox();
         this.groupBox23 = new System.Windows.Forms.GroupBox();
         this.btnSetHCSerial = new System.Windows.Forms.Button();
         this.btnGetHCSerial = new System.Windows.Forms.Button();
         this.txtHCSerial = new System.Windows.Forms.TextBox();
         this.groupBox22 = new System.Windows.Forms.GroupBox();
         this.btnSetCBSerial = new System.Windows.Forms.Button();
         this.btnGetCBSerial = new System.Windows.Forms.Button();
         this.txtCBSerial = new System.Windows.Forms.TextBox();
         this.groupBox3 = new System.Windows.Forms.GroupBox();
         this.btnWSSNSet = new System.Windows.Forms.Button();
         this.btnWSSNGet = new System.Windows.Forms.Button();
         this.txtWSSerialNumber = new System.Windows.Forms.TextBox();
         this.tabPage11 = new System.Windows.Forms.TabPage();
         this.btnReleaseCassette = new System.Windows.Forms.Button();
         this.groupBox37 = new System.Windows.Forms.GroupBox();
         this.btnMotorMove = new System.Windows.Forms.Button();
         this.rdoMotor8 = new System.Windows.Forms.RadioButton();
         this.rdoMotor7 = new System.Windows.Forms.RadioButton();
         this.rdoMotor6 = new System.Windows.Forms.RadioButton();
         this.rdoMotor5 = new System.Windows.Forms.RadioButton();
         this.rdoMotor4 = new System.Windows.Forms.RadioButton();
         this.rdoMotor3 = new System.Windows.Forms.RadioButton();
         this.rdoMotor2 = new System.Windows.Forms.RadioButton();
         this.rdoMotor1 = new System.Windows.Forms.RadioButton();
         this.btnMotorIn = new System.Windows.Forms.Button();
         this.btnMotorOut = new System.Windows.Forms.Button();
         this.txtDrawerCount = new System.Windows.Forms.TextBox();
         this.btnDrawerCount = new System.Windows.Forms.Button();
         this.txtMotorDnCurrent = new System.Windows.Forms.TextBox();
         this.txtMotorUpCurrent = new System.Windows.Forms.TextBox();
         this.btnMotorDnCurrent = new System.Windows.Forms.Button();
         this.btnMotorUpCurrent = new System.Windows.Forms.Button();
         this.btnZeroLockCount = new System.Windows.Forms.Button();
         this.groupBox31 = new System.Windows.Forms.GroupBox();
         this.label151 = new System.Windows.Forms.Label();
         this.txtUserPin6 = new System.Windows.Forms.TextBox();
         this.btnUserPin6 = new System.Windows.Forms.Button();
         this.label94 = new System.Windows.Forms.Label();
         this.label93 = new System.Windows.Forms.Label();
         this.label92 = new System.Windows.Forms.Label();
         this.label91 = new System.Windows.Forms.Label();
         this.label90 = new System.Windows.Forms.Label();
         this.btnSetNarcPin = new System.Windows.Forms.Button();
         this.btnSetAdminPin = new System.Windows.Forms.Button();
         this.txtUserPin5 = new System.Windows.Forms.TextBox();
         this.txtUserPin4 = new System.Windows.Forms.TextBox();
         this.txtUserPin3 = new System.Windows.Forms.TextBox();
         this.txtUserPin2 = new System.Windows.Forms.TextBox();
         this.txtUserPin1 = new System.Windows.Forms.TextBox();
         this.btnUserPin5 = new System.Windows.Forms.Button();
         this.btnUserPin4 = new System.Windows.Forms.Button();
         this.btnUserPin3 = new System.Windows.Forms.Button();
         this.btnUserPin2 = new System.Windows.Forms.Button();
         this.btnUserPin1 = new System.Windows.Forms.Button();
         this.btnGetAdminPin = new System.Windows.Forms.Button();
         this.txtNarcPin = new System.Windows.Forms.TextBox();
         this.btnGetNarcPin = new System.Windows.Forms.Button();
         this.txtAdminPin = new System.Windows.Forms.TextBox();
         this.txtUnlockCount = new System.Windows.Forms.TextBox();
         this.btnUnlockCount = new System.Windows.Forms.Button();
         this.btnLockDrawers = new System.Windows.Forms.Button();
         this.btnUnlockDrawers = new System.Windows.Forms.Button();
         this.groupBox30 = new System.Windows.Forms.GroupBox();
         this.btnSetDrawerOpenTime = new System.Windows.Forms.Button();
         this.btnGetDrawerOpenTime = new System.Windows.Forms.Button();
         this.label74 = new System.Windows.Forms.Label();
         this.txtDrawerOpenTime = new System.Windows.Forms.TextBox();
         this.txtKCFirmware = new System.Windows.Forms.TextBox();
         this.btnKCFirmware = new System.Windows.Forms.Button();
         this.tabPage12 = new System.Windows.Forms.TabPage();
         this.groupBox36 = new System.Windows.Forms.GroupBox();
         this.txtFTPAddress = new System.Windows.Forms.TextBox();
         this.btnSetFTPAddress = new System.Windows.Forms.Button();
         this.btnGetFTPAddress = new System.Windows.Forms.Button();
         this.btnBLMedBoard = new System.Windows.Forms.Button();
         this.btnBLControl = new System.Windows.Forms.Button();
         this.btnBLHolster = new System.Windows.Forms.Button();
         this.btnBLDC = new System.Windows.Forms.Button();
         this.btnBLLCD = new System.Windows.Forms.Button();
         this.btnDLMEDHEX = new System.Windows.Forms.Button();
         this.btnDLCTLHEX = new System.Windows.Forms.Button();
         this.btnLDHOLHEX = new System.Windows.Forms.Button();
         this.btnDLDCDCHEX = new System.Windows.Forms.Button();
         this.btnDLLCDHEX = new System.Windows.Forms.Button();
         this.groupBox21 = new System.Windows.Forms.GroupBox();
         this.btnCodes = new System.Windows.Forms.Button();
         this.label62 = new System.Windows.Forms.Label();
         this.label61 = new System.Windows.Forms.Label();
         this.txtParameter = new System.Windows.Forms.TextBox();
         this.btnSendCommandCode = new System.Windows.Forms.Button();
         this.txtCommandCode = new System.Windows.Forms.TextBox();
         this.btnConnect = new System.Windows.Forms.Button();
         this.txtStatus = new System.Windows.Forms.TextBox();
         this.ilsLED = new System.Windows.Forms.ImageList(this.components);
         this.button1 = new System.Windows.Forms.Button();
         this.button2 = new System.Windows.Forms.Button();
         this.button3 = new System.Windows.Forms.Button();
         this.groupBox12 = new System.Windows.Forms.GroupBox();
         this.button4 = new System.Windows.Forms.Button();
         this.button5 = new System.Windows.Forms.Button();
         this.button6 = new System.Windows.Forms.Button();
         this.button7 = new System.Windows.Forms.Button();
         this.button8 = new System.Windows.Forms.Button();
         this.button9 = new System.Windows.Forms.Button();
         this.groupBox13 = new System.Windows.Forms.GroupBox();
         this.checkBox1 = new System.Windows.Forms.CheckBox();
         this.button10 = new System.Windows.Forms.Button();
         this.button11 = new System.Windows.Forms.Button();
         this.button12 = new System.Windows.Forms.Button();
         this.checkBox2 = new System.Windows.Forms.CheckBox();
         this.button13 = new System.Windows.Forms.Button();
         this.button14 = new System.Windows.Forms.Button();
         this.button15 = new System.Windows.Forms.Button();
         this.button16 = new System.Windows.Forms.Button();
         this.button17 = new System.Windows.Forms.Button();
         this.button18 = new System.Windows.Forms.Button();
         this.button19 = new System.Windows.Forms.Button();
         this.button20 = new System.Windows.Forms.Button();
         this.button21 = new System.Windows.Forms.Button();
         this.button22 = new System.Windows.Forms.Button();
         this.button23 = new System.Windows.Forms.Button();
         this.button24 = new System.Windows.Forms.Button();
         this.button25 = new System.Windows.Forms.Button();
         this.button26 = new System.Windows.Forms.Button();
         this.button27 = new System.Windows.Forms.Button();
         this.button28 = new System.Windows.Forms.Button();
         this.button29 = new System.Windows.Forms.Button();
         this.button30 = new System.Windows.Forms.Button();
         this.button31 = new System.Windows.Forms.Button();
         this.button32 = new System.Windows.Forms.Button();
         this.button33 = new System.Windows.Forms.Button();
         this.button34 = new System.Windows.Forms.Button();
         this.button35 = new System.Windows.Forms.Button();
         this.button36 = new System.Windows.Forms.Button();
         this.groupBox14 = new System.Windows.Forms.GroupBox();
         this.button37 = new System.Windows.Forms.Button();
         this.radioButton1 = new System.Windows.Forms.RadioButton();
         this.radioButton2 = new System.Windows.Forms.RadioButton();
         this.radioButton3 = new System.Windows.Forms.RadioButton();
         this.radioButton4 = new System.Windows.Forms.RadioButton();
         this.radioButton5 = new System.Windows.Forms.RadioButton();
         this.radioButton6 = new System.Windows.Forms.RadioButton();
         this.label37 = new System.Windows.Forms.Label();
         this.label38 = new System.Windows.Forms.Label();
         this.label39 = new System.Windows.Forms.Label();
         this.label41 = new System.Windows.Forms.Label();
         this.label42 = new System.Windows.Forms.Label();
         this.btnClearAllValues = new System.Windows.Forms.Button();
         this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
         this.txtResponse = new System.Windows.Forms.TextBox();
         this.btnIsDrawerOut = new System.Windows.Forms.Button();
         this.btnIsDrawerIn = new System.Windows.Forms.Button();
         this.txtIsDrawerOut = new System.Windows.Forms.TextBox();
         this.txtIsDrawerIn = new System.Windows.Forms.TextBox();
         this.tabControl1.SuspendLayout();
         this.tabPage1.SuspendLayout();
         this.groupBox18.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
         this.groupBox4.SuspendLayout();
         this.tabPage2.SuspendLayout();
         this.groupBox38.SuspendLayout();
         this.groupBox11.SuspendLayout();
         this.groupBox10.SuspendLayout();
         this.tabPage3.SuspendLayout();
         this.groupBox5.SuspendLayout();
         this.tabPage4.SuspendLayout();
         this.groupBox1.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh1A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh1A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh1A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh1A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh1A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh1A)).BeginInit();
         this.groupBox6.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh1B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh1B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh1B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh1B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh1B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh1B)).BeginInit();
         this.tabPage5.SuspendLayout();
         this.groupBox17.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh2A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh2A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh2A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh2A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh2A)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh2A)).BeginInit();
         this.groupBox16.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh2B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh2B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh2B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh2B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh2B)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh2B)).BeginInit();
         this.tabPage6.SuspendLayout();
         this.groupBox25.SuspendLayout();
         this.groupBox8.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
         this.groupBox20.SuspendLayout();
         this.tabPage7.SuspendLayout();
         this.groupBox35.SuspendLayout();
         this.groupBox2.SuspendLayout();
         this.groupBox7.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
         this.groupBox19.SuspendLayout();
         this.groupBox15.SuspendLayout();
         this.tabPage13.SuspendLayout();
         this.tabPage8.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetExternalAntenna)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetCommTimeout)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetCommSize)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetRemoteResponse)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetRemotePort)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetHostIPAddress)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetSSID)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBaudRate)).BeginInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxCommandMode)).BeginInit();
         this.groupBox9.SuspendLayout();
         this.tabPage10.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetChannel)).BeginInit();
         this.groupBox27.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetDHCP)).BeginInit();
         this.groupBox28.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetJoin)).BeginInit();
         this.groupBox29.SuspendLayout();
         this.groupBox34.SuspendLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetAuth)).BeginInit();
         this.tabPage9.SuspendLayout();
         this.groupBox33.SuspendLayout();
         this.groupBox32.SuspendLayout();
         this.groupBox26.SuspendLayout();
         this.groupBox24.SuspendLayout();
         this.groupBox23.SuspendLayout();
         this.groupBox22.SuspendLayout();
         this.groupBox3.SuspendLayout();
         this.tabPage11.SuspendLayout();
         this.groupBox37.SuspendLayout();
         this.groupBox31.SuspendLayout();
         this.groupBox30.SuspendLayout();
         this.tabPage12.SuspendLayout();
         this.groupBox36.SuspendLayout();
         this.groupBox21.SuspendLayout();
         this.groupBox12.SuspendLayout();
         this.groupBox13.SuspendLayout();
         this.groupBox14.SuspendLayout();
         this.SuspendLayout();
         // 
         // btnClose
         // 
         this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnClose.Location = new System.Drawing.Point(797, 527);
         this.btnClose.Name = "btnClose";
         this.btnClose.Size = new System.Drawing.Size(75, 23);
         this.btnClose.TabIndex = 0;
         this.btnClose.Text = "Close";
         this.btnClose.UseVisualStyleBackColor = true;
         this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
         // 
         // tabControl1
         // 
         this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.tabControl1.Controls.Add(this.tabPage1);
         this.tabControl1.Controls.Add(this.tabPage2);
         this.tabControl1.Controls.Add(this.tabPage3);
         this.tabControl1.Controls.Add(this.tabPage4);
         this.tabControl1.Controls.Add(this.tabPage5);
         this.tabControl1.Controls.Add(this.tabPage6);
         this.tabControl1.Controls.Add(this.tabPage7);
         this.tabControl1.Controls.Add(this.tabPage13);
         this.tabControl1.Controls.Add(this.tabPage8);
         this.tabControl1.Controls.Add(this.tabPage10);
         this.tabControl1.Controls.Add(this.tabPage9);
         this.tabControl1.Controls.Add(this.tabPage11);
         this.tabControl1.Controls.Add(this.tabPage12);
         this.tabControl1.Location = new System.Drawing.Point(12, 39);
         this.tabControl1.Name = "tabControl1";
         this.tabControl1.SelectedIndex = 0;
         this.tabControl1.Size = new System.Drawing.Size(860, 482);
         this.tabControl1.TabIndex = 0;
         // 
         // tabPage1
         // 
         this.tabPage1.Controls.Add(this.btnPowerSource);
         this.tabPage1.Controls.Add(this.txtPowerSource);
         this.tabPage1.Controls.Add(this.btnBUBChrgOff);
         this.tabPage1.Controls.Add(this.btnBUBChrgOn);
         this.tabPage1.Controls.Add(this.txtForcedReboot);
         this.tabPage1.Controls.Add(this.btnForcedReboot);
         this.tabPage1.Controls.Add(this.groupBox18);
         this.tabPage1.Controls.Add(this.pictureBox2);
         this.tabPage1.Controls.Add(this.ckbActivate);
         this.tabPage1.Controls.Add(this.label52);
         this.tabPage1.Controls.Add(this.txtSparePins);
         this.tabPage1.Controls.Add(this.btnSparePins);
         this.tabPage1.Controls.Add(this.btnRebootMobius);
         this.tabPage1.Controls.Add(this.groupBox4);
         this.tabPage1.Controls.Add(this.txtBUBChargerStatus);
         this.tabPage1.Controls.Add(this.btnBUBMeasuredVoltage);
         this.tabPage1.Controls.Add(this.txtBUBMeasuredVoltage);
         this.tabPage1.Controls.Add(this.btnBUBChargerStatus);
         this.tabPage1.Controls.Add(this.txtCBFirmware);
         this.tabPage1.Controls.Add(this.btnCBFirmware);
         this.tabPage1.Location = new System.Drawing.Point(4, 22);
         this.tabPage1.Name = "tabPage1";
         this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage1.Size = new System.Drawing.Size(852, 456);
         this.tabPage1.TabIndex = 0;
         this.tabPage1.Text = "Control";
         this.tabPage1.UseVisualStyleBackColor = true;
         // 
         // btnPowerSource
         // 
         this.btnPowerSource.Location = new System.Drawing.Point(6, 223);
         this.btnPowerSource.Name = "btnPowerSource";
         this.btnPowerSource.Size = new System.Drawing.Size(100, 23);
         this.btnPowerSource.TabIndex = 23;
         this.btnPowerSource.Text = "PowerSource";
         this.btnPowerSource.UseVisualStyleBackColor = true;
         this.btnPowerSource.Click += new System.EventHandler(this.btnPowerSource_Click);
         // 
         // txtPowerSource
         // 
         this.txtPowerSource.Location = new System.Drawing.Point(110, 225);
         this.txtPowerSource.Name = "txtPowerSource";
         this.txtPowerSource.Size = new System.Drawing.Size(100, 20);
         this.txtPowerSource.TabIndex = 21;
         // 
         // btnBUBChrgOff
         // 
         this.btnBUBChrgOff.Location = new System.Drawing.Point(284, 85);
         this.btnBUBChrgOff.Name = "btnBUBChrgOff";
         this.btnBUBChrgOff.Size = new System.Drawing.Size(130, 23);
         this.btnBUBChrgOff.TabIndex = 17;
         this.btnBUBChrgOff.Text = "BUB Charger Off";
         this.btnBUBChrgOff.UseVisualStyleBackColor = true;
         this.btnBUBChrgOff.Click += new System.EventHandler(this.btnBUBChrgOff_Click);
         // 
         // btnBUBChrgOn
         // 
         this.btnBUBChrgOn.Location = new System.Drawing.Point(284, 53);
         this.btnBUBChrgOn.Name = "btnBUBChrgOn";
         this.btnBUBChrgOn.Size = new System.Drawing.Size(130, 23);
         this.btnBUBChrgOn.TabIndex = 16;
         this.btnBUBChrgOn.Text = "BUB Charger On";
         this.btnBUBChrgOn.UseVisualStyleBackColor = true;
         this.btnBUBChrgOn.Click += new System.EventHandler(this.btnBUBChrgOn_Click);
         // 
         // txtForcedReboot
         // 
         this.txtForcedReboot.Location = new System.Drawing.Point(110, 187);
         this.txtForcedReboot.Name = "txtForcedReboot";
         this.txtForcedReboot.Size = new System.Drawing.Size(28, 20);
         this.txtForcedReboot.TabIndex = 15;
         // 
         // btnForcedReboot
         // 
         this.btnForcedReboot.Location = new System.Drawing.Point(6, 185);
         this.btnForcedReboot.Name = "btnForcedReboot";
         this.btnForcedReboot.Size = new System.Drawing.Size(100, 23);
         this.btnForcedReboot.TabIndex = 14;
         this.btnForcedReboot.Text = "Forced Reboot";
         this.btnForcedReboot.UseVisualStyleBackColor = true;
         this.btnForcedReboot.Click += new System.EventHandler(this.btnForcedReboot_Click);
         // 
         // groupBox18
         // 
         this.groupBox18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox18.Controls.Add(this.btnXMax);
         this.groupBox18.Controls.Add(this.btnYMax);
         this.groupBox18.Controls.Add(this.btnZMax);
         this.groupBox18.Controls.Add(this.btnResetMax);
         this.groupBox18.Controls.Add(this.txtXMax);
         this.groupBox18.Controls.Add(this.txtYMax);
         this.groupBox18.Controls.Add(this.txtMotion);
         this.groupBox18.Controls.Add(this.txtZMax);
         this.groupBox18.Controls.Add(this.btnMotion);
         this.groupBox18.Controls.Add(this.txtZValue);
         this.groupBox18.Controls.Add(this.btnXValue);
         this.groupBox18.Controls.Add(this.txtYValue);
         this.groupBox18.Controls.Add(this.btnYValue);
         this.groupBox18.Controls.Add(this.txtXValue);
         this.groupBox18.Controls.Add(this.btnZValue);
         this.groupBox18.Location = new System.Drawing.Point(633, 150);
         this.groupBox18.Name = "groupBox18";
         this.groupBox18.Size = new System.Drawing.Size(212, 300);
         this.groupBox18.TabIndex = 20;
         this.groupBox18.TabStop = false;
         this.groupBox18.Text = "Accelerometer";
         // 
         // btnXMax
         // 
         this.btnXMax.Location = new System.Drawing.Point(13, 127);
         this.btnXMax.Name = "btnXMax";
         this.btnXMax.Size = new System.Drawing.Size(75, 23);
         this.btnXMax.TabIndex = 3;
         this.btnXMax.Text = "X Max";
         this.btnXMax.UseVisualStyleBackColor = true;
         this.btnXMax.Click += new System.EventHandler(this.btnXMax_Click);
         // 
         // btnYMax
         // 
         this.btnYMax.Location = new System.Drawing.Point(13, 156);
         this.btnYMax.Name = "btnYMax";
         this.btnYMax.Size = new System.Drawing.Size(75, 23);
         this.btnYMax.TabIndex = 4;
         this.btnYMax.Text = "Y Max";
         this.btnYMax.UseVisualStyleBackColor = true;
         this.btnYMax.Click += new System.EventHandler(this.btnYMax_Click);
         // 
         // btnZMax
         // 
         this.btnZMax.Location = new System.Drawing.Point(13, 185);
         this.btnZMax.Name = "btnZMax";
         this.btnZMax.Size = new System.Drawing.Size(75, 23);
         this.btnZMax.TabIndex = 5;
         this.btnZMax.Text = "Z Max";
         this.btnZMax.UseVisualStyleBackColor = true;
         this.btnZMax.Click += new System.EventHandler(this.btnZMax_Click);
         // 
         // btnResetMax
         // 
         this.btnResetMax.Location = new System.Drawing.Point(13, 214);
         this.btnResetMax.Name = "btnResetMax";
         this.btnResetMax.Size = new System.Drawing.Size(75, 23);
         this.btnResetMax.TabIndex = 6;
         this.btnResetMax.Text = "Reset Max";
         this.btnResetMax.UseVisualStyleBackColor = true;
         this.btnResetMax.Click += new System.EventHandler(this.btnResetMax_Click);
         // 
         // txtXMax
         // 
         this.txtXMax.Location = new System.Drawing.Point(94, 127);
         this.txtXMax.Name = "txtXMax";
         this.txtXMax.Size = new System.Drawing.Size(100, 20);
         this.txtXMax.TabIndex = 15;
         // 
         // txtYMax
         // 
         this.txtYMax.Location = new System.Drawing.Point(94, 158);
         this.txtYMax.Name = "txtYMax";
         this.txtYMax.Size = new System.Drawing.Size(100, 20);
         this.txtYMax.TabIndex = 16;
         // 
         // txtMotion
         // 
         this.txtMotion.Location = new System.Drawing.Point(90, 262);
         this.txtMotion.Name = "txtMotion";
         this.txtMotion.Size = new System.Drawing.Size(40, 20);
         this.txtMotion.TabIndex = 18;
         // 
         // txtZMax
         // 
         this.txtZMax.Location = new System.Drawing.Point(94, 187);
         this.txtZMax.Name = "txtZMax";
         this.txtZMax.Size = new System.Drawing.Size(100, 20);
         this.txtZMax.TabIndex = 17;
         // 
         // btnMotion
         // 
         this.btnMotion.Location = new System.Drawing.Point(13, 260);
         this.btnMotion.Name = "btnMotion";
         this.btnMotion.Size = new System.Drawing.Size(75, 23);
         this.btnMotion.TabIndex = 7;
         this.btnMotion.Text = "Motion";
         this.btnMotion.UseVisualStyleBackColor = true;
         this.btnMotion.Click += new System.EventHandler(this.btnMotion_Click);
         // 
         // txtZValue
         // 
         this.txtZValue.Location = new System.Drawing.Point(94, 83);
         this.txtZValue.Name = "txtZValue";
         this.txtZValue.Size = new System.Drawing.Size(100, 20);
         this.txtZValue.TabIndex = 14;
         // 
         // btnXValue
         // 
         this.btnXValue.Location = new System.Drawing.Point(13, 23);
         this.btnXValue.Name = "btnXValue";
         this.btnXValue.Size = new System.Drawing.Size(75, 23);
         this.btnXValue.TabIndex = 0;
         this.btnXValue.Text = "X Value";
         this.btnXValue.UseVisualStyleBackColor = true;
         this.btnXValue.Click += new System.EventHandler(this.btnXValue_Click);
         // 
         // txtYValue
         // 
         this.txtYValue.Location = new System.Drawing.Point(94, 54);
         this.txtYValue.Name = "txtYValue";
         this.txtYValue.Size = new System.Drawing.Size(100, 20);
         this.txtYValue.TabIndex = 13;
         // 
         // btnYValue
         // 
         this.btnYValue.Location = new System.Drawing.Point(13, 52);
         this.btnYValue.Name = "btnYValue";
         this.btnYValue.Size = new System.Drawing.Size(75, 23);
         this.btnYValue.TabIndex = 1;
         this.btnYValue.Text = "Y Value";
         this.btnYValue.UseVisualStyleBackColor = true;
         this.btnYValue.Click += new System.EventHandler(this.btnYValue_Click);
         // 
         // txtXValue
         // 
         this.txtXValue.Location = new System.Drawing.Point(94, 25);
         this.txtXValue.Name = "txtXValue";
         this.txtXValue.Size = new System.Drawing.Size(100, 20);
         this.txtXValue.TabIndex = 12;
         // 
         // btnZValue
         // 
         this.btnZValue.Location = new System.Drawing.Point(13, 81);
         this.btnZValue.Name = "btnZValue";
         this.btnZValue.Size = new System.Drawing.Size(75, 23);
         this.btnZValue.TabIndex = 2;
         this.btnZValue.Text = "Z Value";
         this.btnZValue.UseVisualStyleBackColor = true;
         this.btnZValue.Click += new System.EventHandler(this.btnZValue_Click);
         // 
         // pictureBox2
         // 
         this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
         this.pictureBox2.Location = new System.Drawing.Point(597, 7);
         this.pictureBox2.Name = "pictureBox2";
         this.pictureBox2.Size = new System.Drawing.Size(248, 73);
         this.pictureBox2.TabIndex = 13;
         this.pictureBox2.TabStop = false;
         // 
         // ckbActivate
         // 
         this.ckbActivate.AutoSize = true;
         this.ckbActivate.Location = new System.Drawing.Point(285, 125);
         this.ckbActivate.Name = "ckbActivate";
         this.ckbActivate.Size = new System.Drawing.Size(120, 17);
         this.ckbActivate.TabIndex = 12;
         this.ckbActivate.Text = "Activate All Controls";
         this.ckbActivate.UseVisualStyleBackColor = true;
         this.ckbActivate.CheckedChanged += new System.EventHandler(this.ckbActivate_CheckedChanged);
         // 
         // label52
         // 
         this.label52.AutoSize = true;
         this.label52.Location = new System.Drawing.Point(78, 156);
         this.label52.Name = "label52";
         this.label52.Size = new System.Drawing.Size(84, 13);
         this.label52.TabIndex = 11;
         this.label52.Text = "J2 Pin 1234 567";
         // 
         // txtSparePins
         // 
         this.txtSparePins.Location = new System.Drawing.Point(110, 134);
         this.txtSparePins.Name = "txtSparePins";
         this.txtSparePins.Size = new System.Drawing.Size(78, 20);
         this.txtSparePins.TabIndex = 10;
         // 
         // btnSparePins
         // 
         this.btnSparePins.Location = new System.Drawing.Point(6, 133);
         this.btnSparePins.Name = "btnSparePins";
         this.btnSparePins.Size = new System.Drawing.Size(100, 23);
         this.btnSparePins.TabIndex = 9;
         this.btnSparePins.Text = "Spare Pins";
         this.btnSparePins.UseVisualStyleBackColor = true;
         this.btnSparePins.Click += new System.EventHandler(this.btnSparePins_Click);
         // 
         // btnRebootMobius
         // 
         this.btnRebootMobius.Location = new System.Drawing.Point(284, 156);
         this.btnRebootMobius.Name = "btnRebootMobius";
         this.btnRebootMobius.Size = new System.Drawing.Size(100, 23);
         this.btnRebootMobius.TabIndex = 8;
         this.btnRebootMobius.Text = "Reboot Mobius";
         this.btnRebootMobius.UseVisualStyleBackColor = true;
         this.btnRebootMobius.Click += new System.EventHandler(this.btnRebootMobius_Click);
         // 
         // groupBox4
         // 
         this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.groupBox4.Controls.Add(this.txtBootDevice);
         this.groupBox4.Controls.Add(this.txtFWRev);
         this.groupBox4.Controls.Add(this.txtPageCount);
         this.groupBox4.Controls.Add(this.label51);
         this.groupBox4.Controls.Add(this.label50);
         this.groupBox4.Controls.Add(this.label49);
         this.groupBox4.Controls.Add(this.txtPageNo);
         this.groupBox4.Controls.Add(this.btnWriteSPI);
         this.groupBox4.Controls.Add(this.label32);
         this.groupBox4.Controls.Add(this.btnReadSPI);
         this.groupBox4.Controls.Add(this.txtWriteSPI);
         this.groupBox4.Controls.Add(this.txtReadSPI);
         this.groupBox4.Location = new System.Drawing.Point(6, 290);
         this.groupBox4.Name = "groupBox4";
         this.groupBox4.Size = new System.Drawing.Size(513, 160);
         this.groupBox4.TabIndex = 4;
         this.groupBox4.TabStop = false;
         this.groupBox4.Text = "DataFlash";
         // 
         // txtBootDevice
         // 
         this.txtBootDevice.Location = new System.Drawing.Point(397, 58);
         this.txtBootDevice.Name = "txtBootDevice";
         this.txtBootDevice.Size = new System.Drawing.Size(100, 20);
         this.txtBootDevice.TabIndex = 10;
         // 
         // txtFWRev
         // 
         this.txtFWRev.Location = new System.Drawing.Point(397, 125);
         this.txtFWRev.Name = "txtFWRev";
         this.txtFWRev.Size = new System.Drawing.Size(100, 20);
         this.txtFWRev.TabIndex = 9;
         this.txtFWRev.Visible = false;
         // 
         // txtPageCount
         // 
         this.txtPageCount.Location = new System.Drawing.Point(397, 29);
         this.txtPageCount.Name = "txtPageCount";
         this.txtPageCount.Size = new System.Drawing.Size(100, 20);
         this.txtPageCount.TabIndex = 8;
         // 
         // label51
         // 
         this.label51.AutoSize = true;
         this.label51.Location = new System.Drawing.Point(343, 62);
         this.label51.Name = "label51";
         this.label51.Size = new System.Drawing.Size(52, 13);
         this.label51.TabIndex = 13;
         this.label51.Text = "Boot Dev";
         // 
         // label50
         // 
         this.label50.AutoSize = true;
         this.label50.Location = new System.Drawing.Point(348, 129);
         this.label50.Name = "label50";
         this.label50.Size = new System.Drawing.Size(47, 13);
         this.label50.TabIndex = 12;
         this.label50.Text = "FW Rev";
         this.label50.Visible = false;
         // 
         // label49
         // 
         this.label49.AutoSize = true;
         this.label49.Location = new System.Drawing.Point(336, 33);
         this.label49.Name = "label49";
         this.label49.Size = new System.Drawing.Size(59, 13);
         this.label49.TabIndex = 11;
         this.label49.Text = "# of Pages";
         // 
         // txtPageNo
         // 
         this.txtPageNo.Location = new System.Drawing.Point(87, 32);
         this.txtPageNo.Name = "txtPageNo";
         this.txtPageNo.Size = new System.Drawing.Size(40, 20);
         this.txtPageNo.TabIndex = 3;
         this.txtPageNo.Text = "0";
         // 
         // btnWriteSPI
         // 
         this.btnWriteSPI.Enabled = false;
         this.btnWriteSPI.Location = new System.Drawing.Point(12, 57);
         this.btnWriteSPI.Name = "btnWriteSPI";
         this.btnWriteSPI.Size = new System.Drawing.Size(75, 23);
         this.btnWriteSPI.TabIndex = 0;
         this.btnWriteSPI.Text = "Write SPI";
         this.btnWriteSPI.UseVisualStyleBackColor = true;
         this.btnWriteSPI.Click += new System.EventHandler(this.btnWriteSPI_Click);
         // 
         // label32
         // 
         this.label32.AutoSize = true;
         this.label32.Location = new System.Drawing.Point(53, 36);
         this.label32.Name = "label32";
         this.label32.Size = new System.Drawing.Size(32, 13);
         this.label32.TabIndex = 7;
         this.label32.Text = "Page";
         // 
         // btnReadSPI
         // 
         this.btnReadSPI.Location = new System.Drawing.Point(12, 92);
         this.btnReadSPI.Name = "btnReadSPI";
         this.btnReadSPI.Size = new System.Drawing.Size(75, 23);
         this.btnReadSPI.TabIndex = 1;
         this.btnReadSPI.Text = "Read SPI";
         this.btnReadSPI.UseVisualStyleBackColor = true;
         this.btnReadSPI.Click += new System.EventHandler(this.btnReadSPI_Click);
         // 
         // txtWriteSPI
         // 
         this.txtWriteSPI.Location = new System.Drawing.Point(87, 58);
         this.txtWriteSPI.Name = "txtWriteSPI";
         this.txtWriteSPI.Size = new System.Drawing.Size(210, 20);
         this.txtWriteSPI.TabIndex = 4;
         // 
         // txtReadSPI
         // 
         this.txtReadSPI.Location = new System.Drawing.Point(87, 93);
         this.txtReadSPI.Name = "txtReadSPI";
         this.txtReadSPI.Size = new System.Drawing.Size(410, 20);
         this.txtReadSPI.TabIndex = 5;
         // 
         // txtBUBChargerStatus
         // 
         this.txtBUBChargerStatus.Location = new System.Drawing.Point(110, 85);
         this.txtBUBChargerStatus.Name = "txtBUBChargerStatus";
         this.txtBUBChargerStatus.Size = new System.Drawing.Size(156, 20);
         this.txtBUBChargerStatus.TabIndex = 7;
         // 
         // btnBUBMeasuredVoltage
         // 
         this.btnBUBMeasuredVoltage.Location = new System.Drawing.Point(6, 55);
         this.btnBUBMeasuredVoltage.Name = "btnBUBMeasuredVoltage";
         this.btnBUBMeasuredVoltage.Size = new System.Drawing.Size(100, 23);
         this.btnBUBMeasuredVoltage.TabIndex = 1;
         this.btnBUBMeasuredVoltage.Text = "BUB Voltage";
         this.btnBUBMeasuredVoltage.UseVisualStyleBackColor = true;
         this.btnBUBMeasuredVoltage.Click += new System.EventHandler(this.btnBUBMeasuredVoltage_Click);
         // 
         // txtBUBMeasuredVoltage
         // 
         this.txtBUBMeasuredVoltage.Location = new System.Drawing.Point(110, 56);
         this.txtBUBMeasuredVoltage.Name = "txtBUBMeasuredVoltage";
         this.txtBUBMeasuredVoltage.Size = new System.Drawing.Size(100, 20);
         this.txtBUBMeasuredVoltage.TabIndex = 6;
         // 
         // btnBUBChargerStatus
         // 
         this.btnBUBChargerStatus.Location = new System.Drawing.Point(6, 84);
         this.btnBUBChargerStatus.Name = "btnBUBChargerStatus";
         this.btnBUBChargerStatus.Size = new System.Drawing.Size(100, 23);
         this.btnBUBChargerStatus.TabIndex = 2;
         this.btnBUBChargerStatus.Text = "ChargerStatus";
         this.btnBUBChargerStatus.UseVisualStyleBackColor = true;
         this.btnBUBChargerStatus.Click += new System.EventHandler(this.btnBUBChargerStatus_Click);
         // 
         // txtCBFirmware
         // 
         this.txtCBFirmware.Location = new System.Drawing.Point(110, 8);
         this.txtCBFirmware.Name = "txtCBFirmware";
         this.txtCBFirmware.Size = new System.Drawing.Size(100, 20);
         this.txtCBFirmware.TabIndex = 5;
         // 
         // btnCBFirmware
         // 
         this.btnCBFirmware.Location = new System.Drawing.Point(6, 6);
         this.btnCBFirmware.Name = "btnCBFirmware";
         this.btnCBFirmware.Size = new System.Drawing.Size(100, 23);
         this.btnCBFirmware.TabIndex = 0;
         this.btnCBFirmware.Text = "Firmware";
         this.btnCBFirmware.UseVisualStyleBackColor = true;
         this.btnCBFirmware.Click += new System.EventHandler(this.btnCBFirmware_Click);
         // 
         // tabPage2
         // 
         this.tabPage2.Controls.Add(this.groupBox38);
         this.tabPage2.Controls.Add(this.groupBox11);
         this.tabPage2.Controls.Add(this.groupBox10);
         this.tabPage2.Controls.Add(this.btnLCDFirmware);
         this.tabPage2.Controls.Add(this.txtLCDFirmware);
         this.tabPage2.Location = new System.Drawing.Point(4, 22);
         this.tabPage2.Name = "tabPage2";
         this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage2.Size = new System.Drawing.Size(852, 456);
         this.tabPage2.TabIndex = 1;
         this.tabPage2.Text = "LCD";
         this.tabPage2.UseVisualStyleBackColor = true;
         // 
         // groupBox38
         // 
         this.groupBox38.Controls.Add(this.btnSetBuzzerMuteTime);
         this.groupBox38.Controls.Add(this.btnGetBuzzerMuteTime);
         this.groupBox38.Controls.Add(this.label160);
         this.groupBox38.Controls.Add(this.txtBuzzerMuteTime);
         this.groupBox38.Location = new System.Drawing.Point(621, 31);
         this.groupBox38.Name = "groupBox38";
         this.groupBox38.Size = new System.Drawing.Size(180, 83);
         this.groupBox38.TabIndex = 8;
         this.groupBox38.TabStop = false;
         this.groupBox38.Text = "Buzzer Mute Time";
         // 
         // btnSetBuzzerMuteTime
         // 
         this.btnSetBuzzerMuteTime.Location = new System.Drawing.Point(101, 45);
         this.btnSetBuzzerMuteTime.Name = "btnSetBuzzerMuteTime";
         this.btnSetBuzzerMuteTime.Size = new System.Drawing.Size(54, 23);
         this.btnSetBuzzerMuteTime.TabIndex = 3;
         this.btnSetBuzzerMuteTime.Text = "Set";
         this.btnSetBuzzerMuteTime.UseVisualStyleBackColor = true;
         this.btnSetBuzzerMuteTime.Click += new System.EventHandler(this.btnSetBuzzerMuteTime_Click);
         // 
         // btnGetBuzzerMuteTime
         // 
         this.btnGetBuzzerMuteTime.Location = new System.Drawing.Point(17, 45);
         this.btnGetBuzzerMuteTime.Name = "btnGetBuzzerMuteTime";
         this.btnGetBuzzerMuteTime.Size = new System.Drawing.Size(54, 23);
         this.btnGetBuzzerMuteTime.TabIndex = 2;
         this.btnGetBuzzerMuteTime.Text = "Get";
         this.btnGetBuzzerMuteTime.UseVisualStyleBackColor = true;
         this.btnGetBuzzerMuteTime.Click += new System.EventHandler(this.btnGetBuzzerMuteTime_Click);
         // 
         // label160
         // 
         this.label160.AutoSize = true;
         this.label160.Location = new System.Drawing.Point(75, 22);
         this.label160.Name = "label160";
         this.label160.Size = new System.Drawing.Size(95, 13);
         this.label160.TabIndex = 1;
         this.label160.Text = "10 to 600 seconds";
         // 
         // txtBuzzerMuteTime
         // 
         this.txtBuzzerMuteTime.Location = new System.Drawing.Point(17, 19);
         this.txtBuzzerMuteTime.Name = "txtBuzzerMuteTime";
         this.txtBuzzerMuteTime.Size = new System.Drawing.Size(52, 20);
         this.txtBuzzerMuteTime.TabIndex = 0;
         // 
         // groupBox11
         // 
         this.groupBox11.Controls.Add(this.btnTempMute);
         this.groupBox11.Controls.Add(this.btnUnMuteBuzzer);
         this.groupBox11.Controls.Add(this.btnSingleBeep);
         this.groupBox11.Controls.Add(this.btnDoubleBeep);
         this.groupBox11.Controls.Add(this.btnBuzzerState);
         this.groupBox11.Controls.Add(this.btnTripleBeep);
         this.groupBox11.Controls.Add(this.txtBuzzerState);
         this.groupBox11.Controls.Add(this.btnMuteBuzzer);
         this.groupBox11.Location = new System.Drawing.Point(249, 8);
         this.groupBox11.Name = "groupBox11";
         this.groupBox11.Size = new System.Drawing.Size(356, 134);
         this.groupBox11.TabIndex = 6;
         this.groupBox11.TabStop = false;
         this.groupBox11.Text = "LCD Buzzer";
         // 
         // btnTempMute
         // 
         this.btnTempMute.Location = new System.Drawing.Point(238, 23);
         this.btnTempMute.Name = "btnTempMute";
         this.btnTempMute.Size = new System.Drawing.Size(99, 23);
         this.btnTempMute.TabIndex = 7;
         this.btnTempMute.Text = "30 Sec Mute";
         this.btnTempMute.UseVisualStyleBackColor = true;
         this.btnTempMute.Click += new System.EventHandler(this.btnTempMute_Click);
         // 
         // btnUnMuteBuzzer
         // 
         this.btnUnMuteBuzzer.Location = new System.Drawing.Point(132, 52);
         this.btnUnMuteBuzzer.Name = "btnUnMuteBuzzer";
         this.btnUnMuteBuzzer.Size = new System.Drawing.Size(99, 23);
         this.btnUnMuteBuzzer.TabIndex = 4;
         this.btnUnMuteBuzzer.Text = "UnMute Buzzer";
         this.btnUnMuteBuzzer.UseVisualStyleBackColor = true;
         this.btnUnMuteBuzzer.Click += new System.EventHandler(this.btnUnMuteBuzzer_Click);
         // 
         // btnSingleBeep
         // 
         this.btnSingleBeep.Location = new System.Drawing.Point(13, 23);
         this.btnSingleBeep.Name = "btnSingleBeep";
         this.btnSingleBeep.Size = new System.Drawing.Size(100, 23);
         this.btnSingleBeep.TabIndex = 0;
         this.btnSingleBeep.Text = "Single Beep";
         this.btnSingleBeep.UseVisualStyleBackColor = true;
         this.btnSingleBeep.Click += new System.EventHandler(this.btnSingleBeep_Click);
         // 
         // btnDoubleBeep
         // 
         this.btnDoubleBeep.Location = new System.Drawing.Point(13, 52);
         this.btnDoubleBeep.Name = "btnDoubleBeep";
         this.btnDoubleBeep.Size = new System.Drawing.Size(100, 23);
         this.btnDoubleBeep.TabIndex = 1;
         this.btnDoubleBeep.Text = "Double Beep";
         this.btnDoubleBeep.UseVisualStyleBackColor = true;
         this.btnDoubleBeep.Click += new System.EventHandler(this.btnDoubleBeep_Click);
         // 
         // btnBuzzerState
         // 
         this.btnBuzzerState.Location = new System.Drawing.Point(156, 90);
         this.btnBuzzerState.Name = "btnBuzzerState";
         this.btnBuzzerState.Size = new System.Drawing.Size(75, 23);
         this.btnBuzzerState.TabIndex = 5;
         this.btnBuzzerState.Text = "Buzzer State";
         this.btnBuzzerState.UseVisualStyleBackColor = true;
         this.btnBuzzerState.Click += new System.EventHandler(this.btnBuzzerState_Click);
         // 
         // btnTripleBeep
         // 
         this.btnTripleBeep.Location = new System.Drawing.Point(13, 81);
         this.btnTripleBeep.Name = "btnTripleBeep";
         this.btnTripleBeep.Size = new System.Drawing.Size(100, 23);
         this.btnTripleBeep.TabIndex = 2;
         this.btnTripleBeep.Text = "Triple Beep";
         this.btnTripleBeep.UseVisualStyleBackColor = true;
         this.btnTripleBeep.Click += new System.EventHandler(this.btnTripleBeep_Click);
         // 
         // txtBuzzerState
         // 
         this.txtBuzzerState.Location = new System.Drawing.Point(232, 91);
         this.txtBuzzerState.Name = "txtBuzzerState";
         this.txtBuzzerState.Size = new System.Drawing.Size(105, 20);
         this.txtBuzzerState.TabIndex = 6;
         // 
         // btnMuteBuzzer
         // 
         this.btnMuteBuzzer.Location = new System.Drawing.Point(132, 23);
         this.btnMuteBuzzer.Name = "btnMuteBuzzer";
         this.btnMuteBuzzer.Size = new System.Drawing.Size(100, 23);
         this.btnMuteBuzzer.TabIndex = 3;
         this.btnMuteBuzzer.Text = "Mute Buzzer";
         this.btnMuteBuzzer.UseVisualStyleBackColor = true;
         this.btnMuteBuzzer.Click += new System.EventHandler(this.btnMuteBuzzer_Click);
         // 
         // groupBox10
         // 
         this.groupBox10.Controls.Add(this.btnBLOn);
         this.groupBox10.Controls.Add(this.txtBLightState);
         this.groupBox10.Controls.Add(this.btnBLOff);
         this.groupBox10.Controls.Add(this.btnBLightstate);
         this.groupBox10.Controls.Add(this.btnFlashBL);
         this.groupBox10.Location = new System.Drawing.Point(249, 232);
         this.groupBox10.Name = "groupBox10";
         this.groupBox10.Size = new System.Drawing.Size(356, 134);
         this.groupBox10.TabIndex = 7;
         this.groupBox10.TabStop = false;
         this.groupBox10.Text = "LCD BackLight";
         // 
         // btnBLOn
         // 
         this.btnBLOn.Location = new System.Drawing.Point(13, 23);
         this.btnBLOn.Name = "btnBLOn";
         this.btnBLOn.Size = new System.Drawing.Size(100, 23);
         this.btnBLOn.TabIndex = 0;
         this.btnBLOn.Text = "BackLight On";
         this.btnBLOn.UseVisualStyleBackColor = true;
         this.btnBLOn.Click += new System.EventHandler(this.btnBLOn_Click);
         // 
         // txtBLightState
         // 
         this.txtBLightState.Location = new System.Drawing.Point(233, 25);
         this.txtBLightState.Name = "txtBLightState";
         this.txtBLightState.Size = new System.Drawing.Size(104, 20);
         this.txtBLightState.TabIndex = 4;
         // 
         // btnBLOff
         // 
         this.btnBLOff.Location = new System.Drawing.Point(13, 52);
         this.btnBLOff.Name = "btnBLOff";
         this.btnBLOff.Size = new System.Drawing.Size(100, 23);
         this.btnBLOff.TabIndex = 1;
         this.btnBLOff.Text = "BackLight Off";
         this.btnBLOff.UseVisualStyleBackColor = true;
         this.btnBLOff.Click += new System.EventHandler(this.btnBLOff_Click);
         // 
         // btnBLightstate
         // 
         this.btnBLightstate.Location = new System.Drawing.Point(135, 23);
         this.btnBLightstate.Name = "btnBLightstate";
         this.btnBLightstate.Size = new System.Drawing.Size(96, 23);
         this.btnBLightstate.TabIndex = 3;
         this.btnBLightstate.Text = "BackLight State";
         this.btnBLightstate.UseVisualStyleBackColor = true;
         this.btnBLightstate.Click += new System.EventHandler(this.btnBLightstate_Click);
         // 
         // btnFlashBL
         // 
         this.btnFlashBL.Location = new System.Drawing.Point(13, 90);
         this.btnFlashBL.Name = "btnFlashBL";
         this.btnFlashBL.Size = new System.Drawing.Size(100, 23);
         this.btnFlashBL.TabIndex = 2;
         this.btnFlashBL.Text = "Flash BackLight";
         this.btnFlashBL.UseVisualStyleBackColor = true;
         this.btnFlashBL.Click += new System.EventHandler(this.btnFlashBL_Click);
         // 
         // btnLCDFirmware
         // 
         this.btnLCDFirmware.Location = new System.Drawing.Point(6, 6);
         this.btnLCDFirmware.Name = "btnLCDFirmware";
         this.btnLCDFirmware.Size = new System.Drawing.Size(100, 23);
         this.btnLCDFirmware.TabIndex = 0;
         this.btnLCDFirmware.Text = "Firmware";
         this.btnLCDFirmware.UseVisualStyleBackColor = true;
         this.btnLCDFirmware.Click += new System.EventHandler(this.btnLCDFirmware_Click);
         // 
         // txtLCDFirmware
         // 
         this.txtLCDFirmware.Location = new System.Drawing.Point(110, 8);
         this.txtLCDFirmware.Name = "txtLCDFirmware";
         this.txtLCDFirmware.Size = new System.Drawing.Size(115, 20);
         this.txtLCDFirmware.TabIndex = 3;
         // 
         // tabPage3
         // 
         this.tabPage3.Controls.Add(this.groupBox5);
         this.tabPage3.Controls.Add(this.txtHCFirmware);
         this.tabPage3.Controls.Add(this.btnHCFirmware);
         this.tabPage3.Location = new System.Drawing.Point(4, 22);
         this.tabPage3.Name = "tabPage3";
         this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage3.Size = new System.Drawing.Size(852, 456);
         this.tabPage3.TabIndex = 2;
         this.tabPage3.Text = "Holster";
         this.tabPage3.UseVisualStyleBackColor = true;
         // 
         // groupBox5
         // 
         this.groupBox5.Controls.Add(this.ckbACConnected);
         this.groupBox5.Controls.Add(this.btnACConnected);
         this.groupBox5.Controls.Add(this.txtChargerCurrent);
         this.groupBox5.Controls.Add(this.btnChargerCurrent);
         this.groupBox5.Controls.Add(this.txtChargerVoltage);
         this.groupBox5.Controls.Add(this.btnChargerVoltage);
         this.groupBox5.Controls.Add(this.txtChargerStatus);
         this.groupBox5.Controls.Add(this.ckbChargerOFF);
         this.groupBox5.Controls.Add(this.btnHChargerStatus);
         this.groupBox5.Controls.Add(this.btnChargerOff);
         this.groupBox5.Location = new System.Drawing.Point(298, 33);
         this.groupBox5.Name = "groupBox5";
         this.groupBox5.Size = new System.Drawing.Size(213, 145);
         this.groupBox5.TabIndex = 126;
         this.groupBox5.TabStop = false;
         this.groupBox5.Text = "Charger";
         // 
         // ckbACConnected
         // 
         this.ckbACConnected.AutoSize = true;
         this.ckbACConnected.Location = new System.Drawing.Point(114, 94);
         this.ckbACConnected.Name = "ckbACConnected";
         this.ckbACConnected.Size = new System.Drawing.Size(95, 17);
         this.ckbACConnected.TabIndex = 8;
         this.ckbACConnected.Text = "AC Connected";
         this.ckbACConnected.UseVisualStyleBackColor = true;
         // 
         // btnACConnected
         // 
         this.btnACConnected.Location = new System.Drawing.Point(6, 91);
         this.btnACConnected.Name = "btnACConnected";
         this.btnACConnected.Size = new System.Drawing.Size(105, 23);
         this.btnACConnected.TabIndex = 3;
         this.btnACConnected.Text = "AC Connected";
         this.btnACConnected.UseVisualStyleBackColor = true;
         this.btnACConnected.Click += new System.EventHandler(this.btnACConnected_Click);
         // 
         // txtChargerCurrent
         // 
         this.txtChargerCurrent.Location = new System.Drawing.Point(114, 69);
         this.txtChargerCurrent.Name = "txtChargerCurrent";
         this.txtChargerCurrent.Size = new System.Drawing.Size(88, 20);
         this.txtChargerCurrent.TabIndex = 7;
         // 
         // btnChargerCurrent
         // 
         this.btnChargerCurrent.Location = new System.Drawing.Point(7, 67);
         this.btnChargerCurrent.Name = "btnChargerCurrent";
         this.btnChargerCurrent.Size = new System.Drawing.Size(105, 23);
         this.btnChargerCurrent.TabIndex = 2;
         this.btnChargerCurrent.Text = "Charger Current";
         this.btnChargerCurrent.UseVisualStyleBackColor = true;
         this.btnChargerCurrent.Click += new System.EventHandler(this.btnChargerCurrent_Click);
         // 
         // txtChargerVoltage
         // 
         this.txtChargerVoltage.Location = new System.Drawing.Point(114, 45);
         this.txtChargerVoltage.Name = "txtChargerVoltage";
         this.txtChargerVoltage.Size = new System.Drawing.Size(88, 20);
         this.txtChargerVoltage.TabIndex = 6;
         // 
         // btnChargerVoltage
         // 
         this.btnChargerVoltage.Location = new System.Drawing.Point(7, 43);
         this.btnChargerVoltage.Name = "btnChargerVoltage";
         this.btnChargerVoltage.Size = new System.Drawing.Size(105, 23);
         this.btnChargerVoltage.TabIndex = 1;
         this.btnChargerVoltage.Text = "Charger Voltage";
         this.btnChargerVoltage.UseVisualStyleBackColor = true;
         this.btnChargerVoltage.Click += new System.EventHandler(this.btnChargerVoltage_Click);
         // 
         // txtChargerStatus
         // 
         this.txtChargerStatus.Location = new System.Drawing.Point(114, 21);
         this.txtChargerStatus.Name = "txtChargerStatus";
         this.txtChargerStatus.Size = new System.Drawing.Size(88, 20);
         this.txtChargerStatus.TabIndex = 5;
         // 
         // ckbChargerOFF
         // 
         this.ckbChargerOFF.AutoSize = true;
         this.ckbChargerOFF.Location = new System.Drawing.Point(114, 118);
         this.ckbChargerOFF.Name = "ckbChargerOFF";
         this.ckbChargerOFF.Size = new System.Drawing.Size(96, 17);
         this.ckbChargerOFF.TabIndex = 9;
         this.ckbChargerOFF.Text = "Charger is OFF";
         this.ckbChargerOFF.UseVisualStyleBackColor = true;
         // 
         // btnHChargerStatus
         // 
         this.btnHChargerStatus.Location = new System.Drawing.Point(7, 19);
         this.btnHChargerStatus.Name = "btnHChargerStatus";
         this.btnHChargerStatus.Size = new System.Drawing.Size(105, 23);
         this.btnHChargerStatus.TabIndex = 0;
         this.btnHChargerStatus.Text = "Charger Status";
         this.btnHChargerStatus.UseVisualStyleBackColor = true;
         this.btnHChargerStatus.Click += new System.EventHandler(this.btnChargerStatus_Click);
         // 
         // btnChargerOff
         // 
         this.btnChargerOff.Location = new System.Drawing.Point(6, 115);
         this.btnChargerOff.Name = "btnChargerOff";
         this.btnChargerOff.Size = new System.Drawing.Size(105, 23);
         this.btnChargerOff.TabIndex = 4;
         this.btnChargerOff.Text = "Turn Charger Off";
         this.btnChargerOff.UseVisualStyleBackColor = true;
         this.btnChargerOff.Click += new System.EventHandler(this.btnChargerOff_Click);
         // 
         // txtHCFirmware
         // 
         this.txtHCFirmware.Location = new System.Drawing.Point(120, 19);
         this.txtHCFirmware.Name = "txtHCFirmware";
         this.txtHCFirmware.Size = new System.Drawing.Size(100, 20);
         this.txtHCFirmware.TabIndex = 81;
         // 
         // btnHCFirmware
         // 
         this.btnHCFirmware.Location = new System.Drawing.Point(18, 17);
         this.btnHCFirmware.Name = "btnHCFirmware";
         this.btnHCFirmware.Size = new System.Drawing.Size(100, 23);
         this.btnHCFirmware.TabIndex = 0;
         this.btnHCFirmware.Text = "Firmware";
         this.btnHCFirmware.UseVisualStyleBackColor = true;
         this.btnHCFirmware.Click += new System.EventHandler(this.btnHCFirmware_Click);
         // 
         // tabPage4
         // 
         this.tabPage4.Controls.Add(this.txtAreVoltages1Locked);
         this.tabPage4.Controls.Add(this.btnAreVoltages1Locked);
         this.tabPage4.Controls.Add(this.label54);
         this.tabPage4.Controls.Add(this.btnEnableOutputs1);
         this.tabPage4.Controls.Add(this.btnDisableOutputs1);
         this.tabPage4.Controls.Add(this.btnUnLock1Voltages);
         this.tabPage4.Controls.Add(this.btnLock1Voltages);
         this.tabPage4.Controls.Add(this.btnEnableSet1);
         this.tabPage4.Controls.Add(this.label31);
         this.tabPage4.Controls.Add(this.label30);
         this.tabPage4.Controls.Add(this.label29);
         this.tabPage4.Controls.Add(this.groupBox1);
         this.tabPage4.Controls.Add(this.txtDC1Status);
         this.tabPage4.Controls.Add(this.groupBox6);
         this.tabPage4.Controls.Add(this.txtDC1Errors);
         this.tabPage4.Controls.Add(this.btnDC1Firmware);
         this.tabPage4.Controls.Add(this.txtDC1Firmware);
         this.tabPage4.Controls.Add(this.btnDC1Errors);
         this.tabPage4.Controls.Add(this.btnDC1Status);
         this.tabPage4.Location = new System.Drawing.Point(4, 22);
         this.tabPage4.Name = "tabPage4";
         this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage4.Size = new System.Drawing.Size(852, 456);
         this.tabPage4.TabIndex = 3;
         this.tabPage4.Text = "DC-DC 1";
         this.tabPage4.UseVisualStyleBackColor = true;
         // 
         // txtAreVoltages1Locked
         // 
         this.txtAreVoltages1Locked.Location = new System.Drawing.Point(710, 184);
         this.txtAreVoltages1Locked.Name = "txtAreVoltages1Locked";
         this.txtAreVoltages1Locked.Size = new System.Drawing.Size(34, 20);
         this.txtAreVoltages1Locked.TabIndex = 18;
         // 
         // btnAreVoltages1Locked
         // 
         this.btnAreVoltages1Locked.Location = new System.Drawing.Point(589, 183);
         this.btnAreVoltages1Locked.Name = "btnAreVoltages1Locked";
         this.btnAreVoltages1Locked.Size = new System.Drawing.Size(120, 23);
         this.btnAreVoltages1Locked.TabIndex = 17;
         this.btnAreVoltages1Locked.Text = "Are Voltages Locked";
         this.btnAreVoltages1Locked.UseVisualStyleBackColor = true;
         this.btnAreVoltages1Locked.Click += new System.EventHandler(this.btnAreVoltages1Locked_Click);
         // 
         // label54
         // 
         this.label54.AutoSize = true;
         this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label54.Location = new System.Drawing.Point(240, 16);
         this.label54.Name = "label54";
         this.label54.Size = new System.Drawing.Size(55, 24);
         this.label54.TabIndex = 16;
         this.label54.Text = "DC 1";
         // 
         // btnEnableOutputs1
         // 
         this.btnEnableOutputs1.Location = new System.Drawing.Point(587, 266);
         this.btnEnableOutputs1.Name = "btnEnableOutputs1";
         this.btnEnableOutputs1.Size = new System.Drawing.Size(111, 23);
         this.btnEnableOutputs1.TabIndex = 15;
         this.btnEnableOutputs1.Text = "Enable Outputs";
         this.btnEnableOutputs1.UseVisualStyleBackColor = true;
         this.btnEnableOutputs1.Click += new System.EventHandler(this.btnEnableOutputs_Click);
         // 
         // btnDisableOutputs1
         // 
         this.btnDisableOutputs1.Location = new System.Drawing.Point(587, 236);
         this.btnDisableOutputs1.Name = "btnDisableOutputs1";
         this.btnDisableOutputs1.Size = new System.Drawing.Size(111, 23);
         this.btnDisableOutputs1.TabIndex = 14;
         this.btnDisableOutputs1.Text = "Disable Outputs";
         this.btnDisableOutputs1.UseVisualStyleBackColor = true;
         this.btnDisableOutputs1.Click += new System.EventHandler(this.btnDisableOutputs_Click);
         // 
         // btnUnLock1Voltages
         // 
         this.btnUnLock1Voltages.Location = new System.Drawing.Point(587, 153);
         this.btnUnLock1Voltages.Name = "btnUnLock1Voltages";
         this.btnUnLock1Voltages.Size = new System.Drawing.Size(111, 23);
         this.btnUnLock1Voltages.TabIndex = 13;
         this.btnUnLock1Voltages.Text = "Unlock Voltages";
         this.btnUnLock1Voltages.UseVisualStyleBackColor = true;
         this.btnUnLock1Voltages.Click += new System.EventHandler(this.btnUnLockVoltages_Click);
         // 
         // btnLock1Voltages
         // 
         this.btnLock1Voltages.Location = new System.Drawing.Point(587, 124);
         this.btnLock1Voltages.Name = "btnLock1Voltages";
         this.btnLock1Voltages.Size = new System.Drawing.Size(111, 23);
         this.btnLock1Voltages.TabIndex = 12;
         this.btnLock1Voltages.Text = "Lock Voltages";
         this.btnLock1Voltages.UseVisualStyleBackColor = true;
         this.btnLock1Voltages.Click += new System.EventHandler(this.btnLockVoltages_Click);
         // 
         // btnEnableSet1
         // 
         this.btnEnableSet1.Location = new System.Drawing.Point(587, 83);
         this.btnEnableSet1.Name = "btnEnableSet1";
         this.btnEnableSet1.Size = new System.Drawing.Size(110, 23);
         this.btnEnableSet1.TabIndex = 7;
         this.btnEnableSet1.Text = "Enable Set Buttons";
         this.btnEnableSet1.UseVisualStyleBackColor = true;
         this.btnEnableSet1.Click += new System.EventHandler(this.btnEnableSet_Click);
         // 
         // label31
         // 
         this.label31.AutoSize = true;
         this.label31.Location = new System.Drawing.Point(584, 62);
         this.label31.Name = "label31";
         this.label31.Size = new System.Drawing.Size(146, 13);
         this.label31.TabIndex = 6;
         this.label31.Text = "when devices are plugged in.";
         // 
         // label30
         // 
         this.label30.AutoSize = true;
         this.label30.Location = new System.Drawing.Point(584, 44);
         this.label30.Name = "label30";
         this.label30.Size = new System.Drawing.Size(155, 13);
         this.label30.TabIndex = 5;
         this.label30.Text = "Do not change voltage settings";
         // 
         // label29
         // 
         this.label29.AutoSize = true;
         this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label29.ForeColor = System.Drawing.Color.Red;
         this.label29.Location = new System.Drawing.Point(583, 20);
         this.label29.Name = "label29";
         this.label29.Size = new System.Drawing.Size(86, 20);
         this.label29.TabIndex = 4;
         this.label29.Text = "Caution !!";
         // 
         // groupBox1
         // 
         this.groupBox1.Controls.Add(this.pbxVoltageCh1A);
         this.groupBox1.Controls.Add(this.label99);
         this.groupBox1.Controls.Add(this.label36);
         this.groupBox1.Controls.Add(this.txtDC1AWatts);
         this.groupBox1.Controls.Add(this.label20);
         this.groupBox1.Controls.Add(this.label21);
         this.groupBox1.Controls.Add(this.label22);
         this.groupBox1.Controls.Add(this.pbxFanOnCh1A);
         this.groupBox1.Controls.Add(this.label1);
         this.groupBox1.Controls.Add(this.btnDC1CurrA);
         this.groupBox1.Controls.Add(this.pbxOutputCh1A);
         this.groupBox1.Controls.Add(this.txtDC1CurrA);
         this.groupBox1.Controls.Add(this.txtDC1VoltA);
         this.groupBox1.Controls.Add(this.btnDC1VoltA);
         this.groupBox1.Controls.Add(this.pbxBusOnCh1A);
         this.groupBox1.Controls.Add(this.label23);
         this.groupBox1.Controls.Add(this.label24);
         this.groupBox1.Controls.Add(this.label25);
         this.groupBox1.Controls.Add(this.pbxShortCh1A);
         this.groupBox1.Controls.Add(this.label26);
         this.groupBox1.Controls.Add(this.btnSetVoltage1A);
         this.groupBox1.Controls.Add(this.pbxFanBlockedCh1A);
         this.groupBox1.Controls.Add(this.label27);
         this.groupBox1.Controls.Add(this.label28);
         this.groupBox1.Controls.Add(this.VoltMeter1Ch2);
         this.groupBox1.Controls.Add(this.txtVoltageSetting1A);
         this.groupBox1.Controls.Add(this.btnVoltSetting1A);
         this.groupBox1.Location = new System.Drawing.Point(306, 60);
         this.groupBox1.Name = "groupBox1";
         this.groupBox1.Size = new System.Drawing.Size(260, 320);
         this.groupBox1.TabIndex = 3;
         this.groupBox1.TabStop = false;
         this.groupBox1.Text = "Channel 2 : 5 - 12";
         // 
         // pbxVoltageCh1A
         // 
         this.pbxVoltageCh1A.Location = new System.Drawing.Point(161, 199);
         this.pbxVoltageCh1A.Name = "pbxVoltageCh1A";
         this.pbxVoltageCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxVoltageCh1A.TabIndex = 101;
         this.pbxVoltageCh1A.TabStop = false;
         // 
         // label99
         // 
         this.label99.AutoSize = true;
         this.label99.Location = new System.Drawing.Point(180, 201);
         this.label99.Name = "label99";
         this.label99.Size = new System.Drawing.Size(68, 13);
         this.label99.TabIndex = 100;
         this.label99.Text = "Voltage Error";
         // 
         // label36
         // 
         this.label36.AutoSize = true;
         this.label36.Location = new System.Drawing.Point(23, 82);
         this.label36.Name = "label36";
         this.label36.Size = new System.Drawing.Size(35, 13);
         this.label36.TabIndex = 9;
         this.label36.Text = "Watts";
         // 
         // txtDC1AWatts
         // 
         this.txtDC1AWatts.Location = new System.Drawing.Point(60, 78);
         this.txtDC1AWatts.Name = "txtDC1AWatts";
         this.txtDC1AWatts.Size = new System.Drawing.Size(76, 20);
         this.txtDC1AWatts.TabIndex = 10;
         // 
         // label20
         // 
         this.label20.AutoSize = true;
         this.label20.Location = new System.Drawing.Point(180, 290);
         this.label20.Name = "label20";
         this.label20.Size = new System.Drawing.Size(42, 13);
         this.label20.TabIndex = 18;
         this.label20.Text = "Fan On";
         // 
         // label21
         // 
         this.label21.AutoSize = true;
         this.label21.Location = new System.Drawing.Point(180, 267);
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size(56, 13);
         this.label21.TabIndex = 17;
         this.label21.Text = "Output On";
         // 
         // label22
         // 
         this.label22.AutoSize = true;
         this.label22.Location = new System.Drawing.Point(180, 244);
         this.label22.Name = "label22";
         this.label22.Size = new System.Drawing.Size(69, 13);
         this.label22.TabIndex = 16;
         this.label22.Text = "Bus Input On";
         // 
         // pbxFanOnCh1A
         // 
         this.pbxFanOnCh1A.Location = new System.Drawing.Point(161, 288);
         this.pbxFanOnCh1A.Name = "pbxFanOnCh1A";
         this.pbxFanOnCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxFanOnCh1A.TabIndex = 99;
         this.pbxFanOnCh1A.TabStop = false;
         // 
         // label1
         // 
         this.label1.AutoSize = true;
         this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label1.Location = new System.Drawing.Point(6, 12);
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size(15, 13);
         this.label1.TabIndex = 4;
         this.label1.Text = "A";
         // 
         // btnDC1CurrA
         // 
         this.btnDC1CurrA.Location = new System.Drawing.Point(168, 50);
         this.btnDC1CurrA.Name = "btnDC1CurrA";
         this.btnDC1CurrA.Size = new System.Drawing.Size(40, 23);
         this.btnDC1CurrA.TabIndex = 1;
         this.btnDC1CurrA.Text = "Get";
         this.btnDC1CurrA.UseVisualStyleBackColor = true;
         this.btnDC1CurrA.Click += new System.EventHandler(this.btnDC1CurrA_Click);
         // 
         // pbxOutputCh1A
         // 
         this.pbxOutputCh1A.Location = new System.Drawing.Point(161, 265);
         this.pbxOutputCh1A.Name = "pbxOutputCh1A";
         this.pbxOutputCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxOutputCh1A.TabIndex = 98;
         this.pbxOutputCh1A.TabStop = false;
         // 
         // txtDC1CurrA
         // 
         this.txtDC1CurrA.Location = new System.Drawing.Point(60, 52);
         this.txtDC1CurrA.Name = "txtDC1CurrA";
         this.txtDC1CurrA.Size = new System.Drawing.Size(76, 20);
         this.txtDC1CurrA.TabIndex = 8;
         // 
         // txtDC1VoltA
         // 
         this.txtDC1VoltA.Location = new System.Drawing.Point(60, 26);
         this.txtDC1VoltA.Name = "txtDC1VoltA";
         this.txtDC1VoltA.Size = new System.Drawing.Size(100, 20);
         this.txtDC1VoltA.TabIndex = 6;
         // 
         // btnDC1VoltA
         // 
         this.btnDC1VoltA.Location = new System.Drawing.Point(168, 24);
         this.btnDC1VoltA.Name = "btnDC1VoltA";
         this.btnDC1VoltA.Size = new System.Drawing.Size(40, 23);
         this.btnDC1VoltA.TabIndex = 0;
         this.btnDC1VoltA.Text = "Get";
         this.btnDC1VoltA.UseVisualStyleBackColor = true;
         this.btnDC1VoltA.Click += new System.EventHandler(this.btnDC1VoltA_Click);
         // 
         // pbxBusOnCh1A
         // 
         this.pbxBusOnCh1A.Location = new System.Drawing.Point(161, 242);
         this.pbxBusOnCh1A.Name = "pbxBusOnCh1A";
         this.pbxBusOnCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxBusOnCh1A.TabIndex = 97;
         this.pbxBusOnCh1A.TabStop = false;
         // 
         // label23
         // 
         this.label23.AutoSize = true;
         this.label23.Location = new System.Drawing.Point(15, 30);
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size(43, 13);
         this.label23.TabIndex = 5;
         this.label23.Text = "Voltage";
         // 
         // label24
         // 
         this.label24.AutoSize = true;
         this.label24.Location = new System.Drawing.Point(180, 178);
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size(64, 13);
         this.label24.TabIndex = 15;
         this.label24.Text = "Short Circuit";
         // 
         // label25
         // 
         this.label25.AutoSize = true;
         this.label25.Location = new System.Drawing.Point(17, 56);
         this.label25.Name = "label25";
         this.label25.Size = new System.Drawing.Size(41, 13);
         this.label25.TabIndex = 7;
         this.label25.Text = "Current";
         // 
         // pbxShortCh1A
         // 
         this.pbxShortCh1A.Location = new System.Drawing.Point(161, 176);
         this.pbxShortCh1A.Name = "pbxShortCh1A";
         this.pbxShortCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxShortCh1A.TabIndex = 92;
         this.pbxShortCh1A.TabStop = false;
         // 
         // label26
         // 
         this.label26.AutoSize = true;
         this.label26.Location = new System.Drawing.Point(180, 156);
         this.label26.Name = "label26";
         this.label26.Size = new System.Drawing.Size(67, 13);
         this.label26.TabIndex = 14;
         this.label26.Text = "Fan Blocked";
         // 
         // btnSetVoltage1A
         // 
         this.btnSetVoltage1A.Enabled = false;
         this.btnSetVoltage1A.Location = new System.Drawing.Point(212, 116);
         this.btnSetVoltage1A.Name = "btnSetVoltage1A";
         this.btnSetVoltage1A.Size = new System.Drawing.Size(40, 23);
         this.btnSetVoltage1A.TabIndex = 3;
         this.btnSetVoltage1A.Text = "Set";
         this.btnSetVoltage1A.UseVisualStyleBackColor = true;
         this.btnSetVoltage1A.Click += new System.EventHandler(this.btnSetVoltage1A_Click);
         // 
         // pbxFanBlockedCh1A
         // 
         this.pbxFanBlockedCh1A.Location = new System.Drawing.Point(161, 154);
         this.pbxFanBlockedCh1A.Name = "pbxFanBlockedCh1A";
         this.pbxFanBlockedCh1A.Size = new System.Drawing.Size(17, 17);
         this.pbxFanBlockedCh1A.TabIndex = 88;
         this.pbxFanBlockedCh1A.TabStop = false;
         // 
         // label27
         // 
         this.label27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label27.Location = new System.Drawing.Point(17, 268);
         this.label27.Name = "label27";
         this.label27.Size = new System.Drawing.Size(120, 23);
         this.label27.TabIndex = 19;
         this.label27.Text = "Ch 2 Voltage";
         this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // label28
         // 
         this.label28.AutoSize = true;
         this.label28.Location = new System.Drawing.Point(5, 122);
         this.label28.Name = "label28";
         this.label28.Size = new System.Drawing.Size(79, 13);
         this.label28.TabIndex = 11;
         this.label28.Text = "Voltage Setting";
         // 
         // VoltMeter1Ch2
         // 
         this.VoltMeter1Ch2.BaseArcColor = System.Drawing.Color.Gray;
         this.VoltMeter1Ch2.BaseArcRadius = 50;
         this.VoltMeter1Ch2.BaseArcStart = 150;
         this.VoltMeter1Ch2.BaseArcSweep = 240;
         this.VoltMeter1Ch2.BaseArcWidth = 2;
         this.VoltMeter1Ch2.Cap_Idx = ((byte)(1));
         this.VoltMeter1Ch2.CapColors = new System.Drawing.Color[] {
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black};
         this.VoltMeter1Ch2.CapPosition = new System.Drawing.Point(10, 10);
         this.VoltMeter1Ch2.CapsPosition = new System.Drawing.Point[] {
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10)};
         this.VoltMeter1Ch2.CapsText = new string[] {
        "",
        "",
        "",
        "",
        ""};
         this.VoltMeter1Ch2.CapText = "";
         this.VoltMeter1Ch2.Center = new System.Drawing.Point(68, 80);
         this.VoltMeter1Ch2.Location = new System.Drawing.Point(9, 144);
         this.VoltMeter1Ch2.MaxValue = 13F;
         this.VoltMeter1Ch2.MinValue = 4F;
         this.VoltMeter1Ch2.Name = "VoltMeter1Ch2";
         this.VoltMeter1Ch2.NeedleColor1 = Meter.Meter.NeedleColorEnum.Red;
         this.VoltMeter1Ch2.NeedleColor2 = System.Drawing.Color.DimGray;
         this.VoltMeter1Ch2.NeedleRadius = 50;
         this.VoltMeter1Ch2.NeedleType = 0;
         this.VoltMeter1Ch2.NeedleWidth = 3;
         this.VoltMeter1Ch2.Range_Idx = ((byte)(1));
         this.VoltMeter1Ch2.RangeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
         this.VoltMeter1Ch2.RangeEnabled = false;
         this.VoltMeter1Ch2.RangeEndValue = 16F;
         this.VoltMeter1Ch2.RangeInnerRadius = 10;
         this.VoltMeter1Ch2.RangeOuterRadius = 40;
         this.VoltMeter1Ch2.RangesColor = new System.Drawing.Color[] {
        System.Drawing.Color.LightGreen,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))),
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control};
         this.VoltMeter1Ch2.RangesEnabled = new bool[] {
        false,
        false,
        false,
        false,
        false};
         this.VoltMeter1Ch2.RangesEndValue = new float[] {
        300F,
        16F,
        0F,
        0F,
        0F};
         this.VoltMeter1Ch2.RangesInnerRadius = new int[] {
        70,
        10,
        70,
        70,
        70};
         this.VoltMeter1Ch2.RangesOuterRadius = new int[] {
        80,
        40,
        80,
        80,
        80};
         this.VoltMeter1Ch2.RangesStartValue = new float[] {
        -100F,
        5F,
        0F,
        0F,
        0F};
         this.VoltMeter1Ch2.RangeStartValue = 5F;
         this.VoltMeter1Ch2.ScaleLinesInterColor = System.Drawing.Color.Red;
         this.VoltMeter1Ch2.ScaleLinesInterInnerRadius = 45;
         this.VoltMeter1Ch2.ScaleLinesInterOuterRadius = 50;
         this.VoltMeter1Ch2.ScaleLinesInterWidth = 1;
         this.VoltMeter1Ch2.ScaleLinesMajorColor = System.Drawing.Color.Black;
         this.VoltMeter1Ch2.ScaleLinesMajorInnerRadius = 40;
         this.VoltMeter1Ch2.ScaleLinesMajorOuterRadius = 50;
         this.VoltMeter1Ch2.ScaleLinesMajorStepValue = 1F;
         this.VoltMeter1Ch2.ScaleLinesMajorWidth = 2;
         this.VoltMeter1Ch2.ScaleLinesMinorColor = System.Drawing.Color.Gray;
         this.VoltMeter1Ch2.ScaleLinesMinorInnerRadius = 45;
         this.VoltMeter1Ch2.ScaleLinesMinorNumOf = 3;
         this.VoltMeter1Ch2.ScaleLinesMinorOuterRadius = 50;
         this.VoltMeter1Ch2.ScaleLinesMinorWidth = 1;
         this.VoltMeter1Ch2.ScaleNumbersColor = System.Drawing.Color.Black;
         this.VoltMeter1Ch2.ScaleNumbersFormat = null;
         this.VoltMeter1Ch2.ScaleNumbersRadius = 62;
         this.VoltMeter1Ch2.ScaleNumbersRotation = 90;
         this.VoltMeter1Ch2.ScaleNumbersStartScaleLine = 1;
         this.VoltMeter1Ch2.ScaleNumbersStepScaleLines = 2;
         this.VoltMeter1Ch2.Size = new System.Drawing.Size(136, 123);
         this.VoltMeter1Ch2.TabIndex = 13;
         this.VoltMeter1Ch2.Text = "meter1";
         this.VoltMeter1Ch2.Value = 4F;
         // 
         // txtVoltageSetting1A
         // 
         this.txtVoltageSetting1A.Location = new System.Drawing.Point(86, 118);
         this.txtVoltageSetting1A.Name = "txtVoltageSetting1A";
         this.txtVoltageSetting1A.Size = new System.Drawing.Size(74, 20);
         this.txtVoltageSetting1A.TabIndex = 12;
         // 
         // btnVoltSetting1A
         // 
         this.btnVoltSetting1A.Location = new System.Drawing.Point(168, 116);
         this.btnVoltSetting1A.Name = "btnVoltSetting1A";
         this.btnVoltSetting1A.Size = new System.Drawing.Size(40, 23);
         this.btnVoltSetting1A.TabIndex = 2;
         this.btnVoltSetting1A.Text = "Get";
         this.btnVoltSetting1A.UseVisualStyleBackColor = true;
         this.btnVoltSetting1A.Click += new System.EventHandler(this.btnVoltSetting1A_Click);
         // 
         // txtDC1Status
         // 
         this.txtDC1Status.Location = new System.Drawing.Point(668, 355);
         this.txtDC1Status.Name = "txtDC1Status";
         this.txtDC1Status.Size = new System.Drawing.Size(72, 20);
         this.txtDC1Status.TabIndex = 11;
         // 
         // groupBox6
         // 
         this.groupBox6.Controls.Add(this.pbxVoltageCh1B);
         this.groupBox6.Controls.Add(this.label98);
         this.groupBox6.Controls.Add(this.label35);
         this.groupBox6.Controls.Add(this.txtDC1BWatts);
         this.groupBox6.Controls.Add(this.VoltMeter1Ch1);
         this.groupBox6.Controls.Add(this.label7);
         this.groupBox6.Controls.Add(this.btnDC1CurrB);
         this.groupBox6.Controls.Add(this.label5);
         this.groupBox6.Controls.Add(this.btnDC1VoltB);
         this.groupBox6.Controls.Add(this.label8);
         this.groupBox6.Controls.Add(this.label9);
         this.groupBox6.Controls.Add(this.pbxFanOnCh1B);
         this.groupBox6.Controls.Add(this.pbxOutputCh1B);
         this.groupBox6.Controls.Add(this.pbxBusOnCh1B);
         this.groupBox6.Controls.Add(this.pbxFanBlockedCh1B);
         this.groupBox6.Controls.Add(this.label11);
         this.groupBox6.Controls.Add(this.pbxShortCh1B);
         this.groupBox6.Controls.Add(this.label16);
         this.groupBox6.Controls.Add(this.lblVoltage);
         this.groupBox6.Controls.Add(this.btnSetVoltage1B);
         this.groupBox6.Controls.Add(this.btnVoltSetting1B);
         this.groupBox6.Controls.Add(this.label14);
         this.groupBox6.Controls.Add(this.txtVoltageSetting1B);
         this.groupBox6.Controls.Add(this.txtDC1VoltB);
         this.groupBox6.Controls.Add(this.label15);
         this.groupBox6.Controls.Add(this.txtDC1CurrB);
         this.groupBox6.Controls.Add(this.label19);
         this.groupBox6.Location = new System.Drawing.Point(23, 60);
         this.groupBox6.Name = "groupBox6";
         this.groupBox6.Size = new System.Drawing.Size(260, 320);
         this.groupBox6.TabIndex = 2;
         this.groupBox6.TabStop = false;
         this.groupBox6.Text = "Channel 1 : 12 - 24";
         // 
         // pbxVoltageCh1B
         // 
         this.pbxVoltageCh1B.Location = new System.Drawing.Point(161, 199);
         this.pbxVoltageCh1B.Name = "pbxVoltageCh1B";
         this.pbxVoltageCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxVoltageCh1B.TabIndex = 98;
         this.pbxVoltageCh1B.TabStop = false;
         // 
         // label98
         // 
         this.label98.AutoSize = true;
         this.label98.Location = new System.Drawing.Point(180, 201);
         this.label98.Name = "label98";
         this.label98.Size = new System.Drawing.Size(68, 13);
         this.label98.TabIndex = 97;
         this.label98.Text = "Voltage Error";
         // 
         // label35
         // 
         this.label35.AutoSize = true;
         this.label35.Location = new System.Drawing.Point(23, 82);
         this.label35.Name = "label35";
         this.label35.Size = new System.Drawing.Size(35, 13);
         this.label35.TabIndex = 9;
         this.label35.Text = "Watts";
         // 
         // txtDC1BWatts
         // 
         this.txtDC1BWatts.Location = new System.Drawing.Point(60, 78);
         this.txtDC1BWatts.Name = "txtDC1BWatts";
         this.txtDC1BWatts.Size = new System.Drawing.Size(76, 20);
         this.txtDC1BWatts.TabIndex = 10;
         // 
         // VoltMeter1Ch1
         // 
         this.VoltMeter1Ch1.BaseArcColor = System.Drawing.Color.Gray;
         this.VoltMeter1Ch1.BaseArcRadius = 50;
         this.VoltMeter1Ch1.BaseArcStart = 150;
         this.VoltMeter1Ch1.BaseArcSweep = 240;
         this.VoltMeter1Ch1.BaseArcWidth = 2;
         this.VoltMeter1Ch1.Cap_Idx = ((byte)(1));
         this.VoltMeter1Ch1.CapColors = new System.Drawing.Color[] {
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black};
         this.VoltMeter1Ch1.CapPosition = new System.Drawing.Point(10, 10);
         this.VoltMeter1Ch1.CapsPosition = new System.Drawing.Point[] {
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10)};
         this.VoltMeter1Ch1.CapsText = new string[] {
        "",
        "",
        "",
        "",
        ""};
         this.VoltMeter1Ch1.CapText = "";
         this.VoltMeter1Ch1.Center = new System.Drawing.Point(68, 80);
         this.VoltMeter1Ch1.Location = new System.Drawing.Point(9, 144);
         this.VoltMeter1Ch1.MaxValue = 25F;
         this.VoltMeter1Ch1.MinValue = 11F;
         this.VoltMeter1Ch1.Name = "VoltMeter1Ch1";
         this.VoltMeter1Ch1.NeedleColor1 = Meter.Meter.NeedleColorEnum.Red;
         this.VoltMeter1Ch1.NeedleColor2 = System.Drawing.Color.DimGray;
         this.VoltMeter1Ch1.NeedleRadius = 50;
         this.VoltMeter1Ch1.NeedleType = 0;
         this.VoltMeter1Ch1.NeedleWidth = 3;
         this.VoltMeter1Ch1.Range_Idx = ((byte)(1));
         this.VoltMeter1Ch1.RangeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
         this.VoltMeter1Ch1.RangeEnabled = false;
         this.VoltMeter1Ch1.RangeEndValue = 16F;
         this.VoltMeter1Ch1.RangeInnerRadius = 10;
         this.VoltMeter1Ch1.RangeOuterRadius = 40;
         this.VoltMeter1Ch1.RangesColor = new System.Drawing.Color[] {
        System.Drawing.Color.LightGreen,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))),
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control};
         this.VoltMeter1Ch1.RangesEnabled = new bool[] {
        false,
        false,
        false,
        false,
        false};
         this.VoltMeter1Ch1.RangesEndValue = new float[] {
        300F,
        16F,
        0F,
        0F,
        0F};
         this.VoltMeter1Ch1.RangesInnerRadius = new int[] {
        70,
        10,
        70,
        70,
        70};
         this.VoltMeter1Ch1.RangesOuterRadius = new int[] {
        80,
        40,
        80,
        80,
        80};
         this.VoltMeter1Ch1.RangesStartValue = new float[] {
        -100F,
        12F,
        0F,
        0F,
        0F};
         this.VoltMeter1Ch1.RangeStartValue = 12F;
         this.VoltMeter1Ch1.ScaleLinesInterColor = System.Drawing.Color.Red;
         this.VoltMeter1Ch1.ScaleLinesInterInnerRadius = 45;
         this.VoltMeter1Ch1.ScaleLinesInterOuterRadius = 50;
         this.VoltMeter1Ch1.ScaleLinesInterWidth = 1;
         this.VoltMeter1Ch1.ScaleLinesMajorColor = System.Drawing.Color.Black;
         this.VoltMeter1Ch1.ScaleLinesMajorInnerRadius = 40;
         this.VoltMeter1Ch1.ScaleLinesMajorOuterRadius = 50;
         this.VoltMeter1Ch1.ScaleLinesMajorStepValue = 1F;
         this.VoltMeter1Ch1.ScaleLinesMajorWidth = 2;
         this.VoltMeter1Ch1.ScaleLinesMinorColor = System.Drawing.Color.Gray;
         this.VoltMeter1Ch1.ScaleLinesMinorInnerRadius = 45;
         this.VoltMeter1Ch1.ScaleLinesMinorNumOf = 3;
         this.VoltMeter1Ch1.ScaleLinesMinorOuterRadius = 50;
         this.VoltMeter1Ch1.ScaleLinesMinorWidth = 1;
         this.VoltMeter1Ch1.ScaleNumbersColor = System.Drawing.Color.Black;
         this.VoltMeter1Ch1.ScaleNumbersFormat = null;
         this.VoltMeter1Ch1.ScaleNumbersRadius = 62;
         this.VoltMeter1Ch1.ScaleNumbersRotation = 90;
         this.VoltMeter1Ch1.ScaleNumbersStartScaleLine = 1;
         this.VoltMeter1Ch1.ScaleNumbersStepScaleLines = 2;
         this.VoltMeter1Ch1.Size = new System.Drawing.Size(136, 123);
         this.VoltMeter1Ch1.TabIndex = 13;
         this.VoltMeter1Ch1.Text = "VoltMeter";
         this.VoltMeter1Ch1.Value = 11F;
         // 
         // label7
         // 
         this.label7.AutoSize = true;
         this.label7.Location = new System.Drawing.Point(180, 290);
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size(42, 13);
         this.label7.TabIndex = 18;
         this.label7.Text = "Fan On";
         // 
         // btnDC1CurrB
         // 
         this.btnDC1CurrB.Location = new System.Drawing.Point(168, 50);
         this.btnDC1CurrB.Name = "btnDC1CurrB";
         this.btnDC1CurrB.Size = new System.Drawing.Size(40, 23);
         this.btnDC1CurrB.TabIndex = 1;
         this.btnDC1CurrB.Text = "Get";
         this.btnDC1CurrB.UseVisualStyleBackColor = true;
         this.btnDC1CurrB.Click += new System.EventHandler(this.btnDC1CurrB_Click);
         // 
         // label5
         // 
         this.label5.AutoSize = true;
         this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label5.Location = new System.Drawing.Point(6, 12);
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size(15, 13);
         this.label5.TabIndex = 4;
         this.label5.Text = "B";
         // 
         // btnDC1VoltB
         // 
         this.btnDC1VoltB.Location = new System.Drawing.Point(168, 24);
         this.btnDC1VoltB.Name = "btnDC1VoltB";
         this.btnDC1VoltB.Size = new System.Drawing.Size(40, 23);
         this.btnDC1VoltB.TabIndex = 0;
         this.btnDC1VoltB.Text = "Get";
         this.btnDC1VoltB.UseVisualStyleBackColor = true;
         this.btnDC1VoltB.Click += new System.EventHandler(this.btnDC1VoltB_Click);
         // 
         // label8
         // 
         this.label8.AutoSize = true;
         this.label8.Location = new System.Drawing.Point(180, 267);
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size(56, 13);
         this.label8.TabIndex = 17;
         this.label8.Text = "Output On";
         // 
         // label9
         // 
         this.label9.AutoSize = true;
         this.label9.Location = new System.Drawing.Point(180, 244);
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size(69, 13);
         this.label9.TabIndex = 16;
         this.label9.Text = "Bus Input On";
         // 
         // pbxFanOnCh1B
         // 
         this.pbxFanOnCh1B.Location = new System.Drawing.Point(161, 288);
         this.pbxFanOnCh1B.Name = "pbxFanOnCh1B";
         this.pbxFanOnCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxFanOnCh1B.TabIndex = 96;
         this.pbxFanOnCh1B.TabStop = false;
         // 
         // pbxOutputCh1B
         // 
         this.pbxOutputCh1B.Location = new System.Drawing.Point(161, 265);
         this.pbxOutputCh1B.Name = "pbxOutputCh1B";
         this.pbxOutputCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxOutputCh1B.TabIndex = 95;
         this.pbxOutputCh1B.TabStop = false;
         // 
         // pbxBusOnCh1B
         // 
         this.pbxBusOnCh1B.Location = new System.Drawing.Point(161, 242);
         this.pbxBusOnCh1B.Name = "pbxBusOnCh1B";
         this.pbxBusOnCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxBusOnCh1B.TabIndex = 94;
         this.pbxBusOnCh1B.TabStop = false;
         // 
         // pbxFanBlockedCh1B
         // 
         this.pbxFanBlockedCh1B.Location = new System.Drawing.Point(161, 154);
         this.pbxFanBlockedCh1B.Name = "pbxFanBlockedCh1B";
         this.pbxFanBlockedCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxFanBlockedCh1B.TabIndex = 87;
         this.pbxFanBlockedCh1B.TabStop = false;
         // 
         // label11
         // 
         this.label11.AutoSize = true;
         this.label11.Location = new System.Drawing.Point(180, 156);
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size(67, 13);
         this.label11.TabIndex = 14;
         this.label11.Text = "Fan Blocked";
         // 
         // pbxShortCh1B
         // 
         this.pbxShortCh1B.Location = new System.Drawing.Point(161, 176);
         this.pbxShortCh1B.Name = "pbxShortCh1B";
         this.pbxShortCh1B.Size = new System.Drawing.Size(17, 17);
         this.pbxShortCh1B.TabIndex = 91;
         this.pbxShortCh1B.TabStop = false;
         // 
         // label16
         // 
         this.label16.AutoSize = true;
         this.label16.Location = new System.Drawing.Point(180, 178);
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size(64, 13);
         this.label16.TabIndex = 15;
         this.label16.Text = "Short Circuit";
         // 
         // lblVoltage
         // 
         this.lblVoltage.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.lblVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.lblVoltage.Location = new System.Drawing.Point(17, 268);
         this.lblVoltage.Name = "lblVoltage";
         this.lblVoltage.Size = new System.Drawing.Size(120, 23);
         this.lblVoltage.TabIndex = 19;
         this.lblVoltage.Text = "Ch 1 Voltage";
         this.lblVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // btnSetVoltage1B
         // 
         this.btnSetVoltage1B.Enabled = false;
         this.btnSetVoltage1B.Location = new System.Drawing.Point(212, 116);
         this.btnSetVoltage1B.Name = "btnSetVoltage1B";
         this.btnSetVoltage1B.Size = new System.Drawing.Size(40, 23);
         this.btnSetVoltage1B.TabIndex = 3;
         this.btnSetVoltage1B.Text = "Set";
         this.btnSetVoltage1B.UseVisualStyleBackColor = true;
         this.btnSetVoltage1B.Click += new System.EventHandler(this.btnSetVoltage1B_Click);
         // 
         // btnVoltSetting1B
         // 
         this.btnVoltSetting1B.Location = new System.Drawing.Point(168, 116);
         this.btnVoltSetting1B.Name = "btnVoltSetting1B";
         this.btnVoltSetting1B.Size = new System.Drawing.Size(40, 23);
         this.btnVoltSetting1B.TabIndex = 2;
         this.btnVoltSetting1B.Text = "Get";
         this.btnVoltSetting1B.UseVisualStyleBackColor = true;
         this.btnVoltSetting1B.Click += new System.EventHandler(this.btnVoltSetting1B_Click);
         // 
         // label14
         // 
         this.label14.AutoSize = true;
         this.label14.Location = new System.Drawing.Point(15, 30);
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size(43, 13);
         this.label14.TabIndex = 5;
         this.label14.Text = "Voltage";
         // 
         // txtVoltageSetting1B
         // 
         this.txtVoltageSetting1B.Location = new System.Drawing.Point(86, 118);
         this.txtVoltageSetting1B.Name = "txtVoltageSetting1B";
         this.txtVoltageSetting1B.Size = new System.Drawing.Size(74, 20);
         this.txtVoltageSetting1B.TabIndex = 12;
         // 
         // txtDC1VoltB
         // 
         this.txtDC1VoltB.Location = new System.Drawing.Point(60, 26);
         this.txtDC1VoltB.Name = "txtDC1VoltB";
         this.txtDC1VoltB.Size = new System.Drawing.Size(100, 20);
         this.txtDC1VoltB.TabIndex = 6;
         // 
         // label15
         // 
         this.label15.AutoSize = true;
         this.label15.Location = new System.Drawing.Point(17, 56);
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size(41, 13);
         this.label15.TabIndex = 7;
         this.label15.Text = "Current";
         // 
         // txtDC1CurrB
         // 
         this.txtDC1CurrB.Location = new System.Drawing.Point(60, 52);
         this.txtDC1CurrB.Name = "txtDC1CurrB";
         this.txtDC1CurrB.Size = new System.Drawing.Size(76, 20);
         this.txtDC1CurrB.TabIndex = 8;
         // 
         // label19
         // 
         this.label19.AutoSize = true;
         this.label19.Location = new System.Drawing.Point(5, 122);
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size(79, 13);
         this.label19.TabIndex = 11;
         this.label19.Text = "Voltage Setting";
         // 
         // txtDC1Errors
         // 
         this.txtDC1Errors.Location = new System.Drawing.Point(668, 329);
         this.txtDC1Errors.Name = "txtDC1Errors";
         this.txtDC1Errors.Size = new System.Drawing.Size(72, 20);
         this.txtDC1Errors.TabIndex = 10;
         // 
         // btnDC1Firmware
         // 
         this.btnDC1Firmware.Location = new System.Drawing.Point(6, 6);
         this.btnDC1Firmware.Name = "btnDC1Firmware";
         this.btnDC1Firmware.Size = new System.Drawing.Size(100, 23);
         this.btnDC1Firmware.TabIndex = 0;
         this.btnDC1Firmware.Text = "Firmware";
         this.btnDC1Firmware.UseVisualStyleBackColor = true;
         this.btnDC1Firmware.Click += new System.EventHandler(this.btnDC1Firmware_Click);
         // 
         // txtDC1Firmware
         // 
         this.txtDC1Firmware.Location = new System.Drawing.Point(110, 8);
         this.txtDC1Firmware.Name = "txtDC1Firmware";
         this.txtDC1Firmware.Size = new System.Drawing.Size(100, 20);
         this.txtDC1Firmware.TabIndex = 1;
         // 
         // btnDC1Errors
         // 
         this.btnDC1Errors.Location = new System.Drawing.Point(587, 327);
         this.btnDC1Errors.Name = "btnDC1Errors";
         this.btnDC1Errors.Size = new System.Drawing.Size(75, 23);
         this.btnDC1Errors.TabIndex = 8;
         this.btnDC1Errors.Text = "Errors";
         this.btnDC1Errors.UseVisualStyleBackColor = true;
         this.btnDC1Errors.Click += new System.EventHandler(this.btnDC1Errors_Click);
         // 
         // btnDC1Status
         // 
         this.btnDC1Status.Location = new System.Drawing.Point(587, 355);
         this.btnDC1Status.Name = "btnDC1Status";
         this.btnDC1Status.Size = new System.Drawing.Size(75, 23);
         this.btnDC1Status.TabIndex = 9;
         this.btnDC1Status.Text = "Status";
         this.btnDC1Status.UseVisualStyleBackColor = true;
         this.btnDC1Status.Click += new System.EventHandler(this.btnDC1Status_Click);
         // 
         // tabPage5
         // 
         this.tabPage5.Controls.Add(this.txtAreVoltages2Locked);
         this.tabPage5.Controls.Add(this.btnAreVoltages2Locked);
         this.tabPage5.Controls.Add(this.btnEnableOutputs2);
         this.tabPage5.Controls.Add(this.btnDisableOutputs2);
         this.tabPage5.Controls.Add(this.btnUnLock2Voltages);
         this.tabPage5.Controls.Add(this.btnLock2Voltages);
         this.tabPage5.Controls.Add(this.btnEnableSet2);
         this.tabPage5.Controls.Add(this.label58);
         this.tabPage5.Controls.Add(this.label59);
         this.tabPage5.Controls.Add(this.label60);
         this.tabPage5.Controls.Add(this.label55);
         this.tabPage5.Controls.Add(this.groupBox17);
         this.tabPage5.Controls.Add(this.groupBox16);
         this.tabPage5.Controls.Add(this.txtDC2Status);
         this.tabPage5.Controls.Add(this.txtDC2Errors);
         this.tabPage5.Controls.Add(this.btnDC2Status);
         this.tabPage5.Controls.Add(this.btnDC2Errors);
         this.tabPage5.Controls.Add(this.btnDC2Firmware);
         this.tabPage5.Controls.Add(this.txtDC2Firmware);
         this.tabPage5.Location = new System.Drawing.Point(4, 22);
         this.tabPage5.Name = "tabPage5";
         this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage5.Size = new System.Drawing.Size(852, 456);
         this.tabPage5.TabIndex = 4;
         this.tabPage5.Text = "DC-DC 2";
         this.tabPage5.UseVisualStyleBackColor = true;
         // 
         // txtAreVoltages2Locked
         // 
         this.txtAreVoltages2Locked.Location = new System.Drawing.Point(710, 184);
         this.txtAreVoltages2Locked.Name = "txtAreVoltages2Locked";
         this.txtAreVoltages2Locked.Size = new System.Drawing.Size(34, 20);
         this.txtAreVoltages2Locked.TabIndex = 36;
         // 
         // btnAreVoltages2Locked
         // 
         this.btnAreVoltages2Locked.Location = new System.Drawing.Point(589, 183);
         this.btnAreVoltages2Locked.Name = "btnAreVoltages2Locked";
         this.btnAreVoltages2Locked.Size = new System.Drawing.Size(120, 23);
         this.btnAreVoltages2Locked.TabIndex = 35;
         this.btnAreVoltages2Locked.Text = "Are Voltages Locked";
         this.btnAreVoltages2Locked.UseVisualStyleBackColor = true;
         this.btnAreVoltages2Locked.Click += new System.EventHandler(this.btnAreVoltages2Locked_Click);
         // 
         // btnEnableOutputs2
         // 
         this.btnEnableOutputs2.Location = new System.Drawing.Point(587, 266);
         this.btnEnableOutputs2.Name = "btnEnableOutputs2";
         this.btnEnableOutputs2.Size = new System.Drawing.Size(111, 23);
         this.btnEnableOutputs2.TabIndex = 34;
         this.btnEnableOutputs2.Text = "Enable Outputs";
         this.btnEnableOutputs2.UseVisualStyleBackColor = true;
         this.btnEnableOutputs2.Click += new System.EventHandler(this.btnEnableOutputs2_Click);
         // 
         // btnDisableOutputs2
         // 
         this.btnDisableOutputs2.Location = new System.Drawing.Point(587, 236);
         this.btnDisableOutputs2.Name = "btnDisableOutputs2";
         this.btnDisableOutputs2.Size = new System.Drawing.Size(111, 23);
         this.btnDisableOutputs2.TabIndex = 33;
         this.btnDisableOutputs2.Text = "Disable Outputs";
         this.btnDisableOutputs2.UseVisualStyleBackColor = true;
         this.btnDisableOutputs2.Click += new System.EventHandler(this.btnDisableOutputs2_Click);
         // 
         // btnUnLock2Voltages
         // 
         this.btnUnLock2Voltages.Location = new System.Drawing.Point(587, 153);
         this.btnUnLock2Voltages.Name = "btnUnLock2Voltages";
         this.btnUnLock2Voltages.Size = new System.Drawing.Size(111, 23);
         this.btnUnLock2Voltages.TabIndex = 32;
         this.btnUnLock2Voltages.Text = "Unlock Voltages";
         this.btnUnLock2Voltages.UseVisualStyleBackColor = true;
         this.btnUnLock2Voltages.Click += new System.EventHandler(this.btnUnLock2Voltages_Click);
         // 
         // btnLock2Voltages
         // 
         this.btnLock2Voltages.Location = new System.Drawing.Point(587, 124);
         this.btnLock2Voltages.Name = "btnLock2Voltages";
         this.btnLock2Voltages.Size = new System.Drawing.Size(111, 23);
         this.btnLock2Voltages.TabIndex = 31;
         this.btnLock2Voltages.Text = "Lock Voltages";
         this.btnLock2Voltages.UseVisualStyleBackColor = true;
         this.btnLock2Voltages.Click += new System.EventHandler(this.btnLock2Voltages_Click);
         // 
         // btnEnableSet2
         // 
         this.btnEnableSet2.Location = new System.Drawing.Point(587, 83);
         this.btnEnableSet2.Name = "btnEnableSet2";
         this.btnEnableSet2.Size = new System.Drawing.Size(110, 23);
         this.btnEnableSet2.TabIndex = 30;
         this.btnEnableSet2.Text = "Enable Set Buttons";
         this.btnEnableSet2.UseVisualStyleBackColor = true;
         this.btnEnableSet2.Click += new System.EventHandler(this.btnEnableSet2_Click);
         // 
         // label58
         // 
         this.label58.AutoSize = true;
         this.label58.Location = new System.Drawing.Point(584, 62);
         this.label58.Name = "label58";
         this.label58.Size = new System.Drawing.Size(146, 13);
         this.label58.TabIndex = 29;
         this.label58.Text = "when devices are plugged in.";
         // 
         // label59
         // 
         this.label59.AutoSize = true;
         this.label59.Location = new System.Drawing.Point(584, 44);
         this.label59.Name = "label59";
         this.label59.Size = new System.Drawing.Size(155, 13);
         this.label59.TabIndex = 28;
         this.label59.Text = "Do not change voltage settings";
         // 
         // label60
         // 
         this.label60.AutoSize = true;
         this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label60.ForeColor = System.Drawing.Color.Red;
         this.label60.Location = new System.Drawing.Point(583, 20);
         this.label60.Name = "label60";
         this.label60.Size = new System.Drawing.Size(86, 20);
         this.label60.TabIndex = 27;
         this.label60.Text = "Caution !!";
         // 
         // label55
         // 
         this.label55.AutoSize = true;
         this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label55.Location = new System.Drawing.Point(240, 16);
         this.label55.Name = "label55";
         this.label55.Size = new System.Drawing.Size(55, 24);
         this.label55.TabIndex = 26;
         this.label55.Text = "DC 2";
         // 
         // groupBox17
         // 
         this.groupBox17.Controls.Add(this.pbxVoltageCh2A);
         this.groupBox17.Controls.Add(this.label101);
         this.groupBox17.Controls.Add(this.label69);
         this.groupBox17.Controls.Add(this.label70);
         this.groupBox17.Controls.Add(this.label71);
         this.groupBox17.Controls.Add(this.pbxFanOnCh2A);
         this.groupBox17.Controls.Add(this.pbxOutputCh2A);
         this.groupBox17.Controls.Add(this.pbxBusOnCh2A);
         this.groupBox17.Controls.Add(this.label72);
         this.groupBox17.Controls.Add(this.pbxShortCh2A);
         this.groupBox17.Controls.Add(this.label73);
         this.groupBox17.Controls.Add(this.pbxFanBlockedCh2A);
         this.groupBox17.Controls.Add(this.btnSetVoltage2A);
         this.groupBox17.Controls.Add(this.btnVoltSetting2A);
         this.groupBox17.Controls.Add(this.txtVoltageSetting2A);
         this.groupBox17.Controls.Add(this.label63);
         this.groupBox17.Controls.Add(this.label57);
         this.groupBox17.Controls.Add(this.VoltMeter2Ch2);
         this.groupBox17.Controls.Add(this.btnDC2VoltA);
         this.groupBox17.Controls.Add(this.txtDC2CurrA);
         this.groupBox17.Controls.Add(this.label44);
         this.groupBox17.Controls.Add(this.txtDC2VoltA);
         this.groupBox17.Controls.Add(this.txtDC2AWatts);
         this.groupBox17.Controls.Add(this.label18);
         this.groupBox17.Controls.Add(this.label13);
         this.groupBox17.Controls.Add(this.btnDC2CurrA);
         this.groupBox17.Controls.Add(this.label10);
         this.groupBox17.Location = new System.Drawing.Point(306, 60);
         this.groupBox17.Name = "groupBox17";
         this.groupBox17.Size = new System.Drawing.Size(260, 320);
         this.groupBox17.TabIndex = 25;
         this.groupBox17.TabStop = false;
         this.groupBox17.Text = "Channel 2 : 5 - 12";
         // 
         // pbxVoltageCh2A
         // 
         this.pbxVoltageCh2A.Location = new System.Drawing.Point(161, 199);
         this.pbxVoltageCh2A.Name = "pbxVoltageCh2A";
         this.pbxVoltageCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxVoltageCh2A.TabIndex = 111;
         this.pbxVoltageCh2A.TabStop = false;
         // 
         // label101
         // 
         this.label101.AutoSize = true;
         this.label101.Location = new System.Drawing.Point(180, 201);
         this.label101.Name = "label101";
         this.label101.Size = new System.Drawing.Size(68, 13);
         this.label101.TabIndex = 110;
         this.label101.Text = "Voltage Error";
         // 
         // label69
         // 
         this.label69.AutoSize = true;
         this.label69.Location = new System.Drawing.Point(180, 290);
         this.label69.Name = "label69";
         this.label69.Size = new System.Drawing.Size(42, 13);
         this.label69.TabIndex = 104;
         this.label69.Text = "Fan On";
         // 
         // label70
         // 
         this.label70.AutoSize = true;
         this.label70.Location = new System.Drawing.Point(180, 267);
         this.label70.Name = "label70";
         this.label70.Size = new System.Drawing.Size(56, 13);
         this.label70.TabIndex = 103;
         this.label70.Text = "Output On";
         // 
         // label71
         // 
         this.label71.AutoSize = true;
         this.label71.Location = new System.Drawing.Point(180, 244);
         this.label71.Name = "label71";
         this.label71.Size = new System.Drawing.Size(69, 13);
         this.label71.TabIndex = 102;
         this.label71.Text = "Bus Input On";
         // 
         // pbxFanOnCh2A
         // 
         this.pbxFanOnCh2A.Location = new System.Drawing.Point(161, 288);
         this.pbxFanOnCh2A.Name = "pbxFanOnCh2A";
         this.pbxFanOnCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxFanOnCh2A.TabIndex = 109;
         this.pbxFanOnCh2A.TabStop = false;
         // 
         // pbxOutputCh2A
         // 
         this.pbxOutputCh2A.Location = new System.Drawing.Point(161, 265);
         this.pbxOutputCh2A.Name = "pbxOutputCh2A";
         this.pbxOutputCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxOutputCh2A.TabIndex = 108;
         this.pbxOutputCh2A.TabStop = false;
         // 
         // pbxBusOnCh2A
         // 
         this.pbxBusOnCh2A.Location = new System.Drawing.Point(161, 242);
         this.pbxBusOnCh2A.Name = "pbxBusOnCh2A";
         this.pbxBusOnCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxBusOnCh2A.TabIndex = 107;
         this.pbxBusOnCh2A.TabStop = false;
         // 
         // label72
         // 
         this.label72.AutoSize = true;
         this.label72.Location = new System.Drawing.Point(180, 178);
         this.label72.Name = "label72";
         this.label72.Size = new System.Drawing.Size(64, 13);
         this.label72.TabIndex = 101;
         this.label72.Text = "Short Circuit";
         // 
         // pbxShortCh2A
         // 
         this.pbxShortCh2A.Location = new System.Drawing.Point(161, 176);
         this.pbxShortCh2A.Name = "pbxShortCh2A";
         this.pbxShortCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxShortCh2A.TabIndex = 106;
         this.pbxShortCh2A.TabStop = false;
         // 
         // label73
         // 
         this.label73.AutoSize = true;
         this.label73.Location = new System.Drawing.Point(180, 156);
         this.label73.Name = "label73";
         this.label73.Size = new System.Drawing.Size(67, 13);
         this.label73.TabIndex = 100;
         this.label73.Text = "Fan Blocked";
         // 
         // pbxFanBlockedCh2A
         // 
         this.pbxFanBlockedCh2A.Location = new System.Drawing.Point(161, 154);
         this.pbxFanBlockedCh2A.Name = "pbxFanBlockedCh2A";
         this.pbxFanBlockedCh2A.Size = new System.Drawing.Size(17, 17);
         this.pbxFanBlockedCh2A.TabIndex = 105;
         this.pbxFanBlockedCh2A.TabStop = false;
         // 
         // btnSetVoltage2A
         // 
         this.btnSetVoltage2A.Enabled = false;
         this.btnSetVoltage2A.Location = new System.Drawing.Point(212, 116);
         this.btnSetVoltage2A.Name = "btnSetVoltage2A";
         this.btnSetVoltage2A.Size = new System.Drawing.Size(40, 23);
         this.btnSetVoltage2A.TabIndex = 29;
         this.btnSetVoltage2A.Text = "Set";
         this.btnSetVoltage2A.UseVisualStyleBackColor = true;
         this.btnSetVoltage2A.Click += new System.EventHandler(this.btnSetVoltage2A_Click);
         // 
         // btnVoltSetting2A
         // 
         this.btnVoltSetting2A.Location = new System.Drawing.Point(168, 116);
         this.btnVoltSetting2A.Name = "btnVoltSetting2A";
         this.btnVoltSetting2A.Size = new System.Drawing.Size(40, 23);
         this.btnVoltSetting2A.TabIndex = 28;
         this.btnVoltSetting2A.Text = "Get";
         this.btnVoltSetting2A.UseVisualStyleBackColor = true;
         this.btnVoltSetting2A.Click += new System.EventHandler(this.btnVoltSetting2A_Click);
         // 
         // txtVoltageSetting2A
         // 
         this.txtVoltageSetting2A.Location = new System.Drawing.Point(86, 118);
         this.txtVoltageSetting2A.Name = "txtVoltageSetting2A";
         this.txtVoltageSetting2A.Size = new System.Drawing.Size(74, 20);
         this.txtVoltageSetting2A.TabIndex = 31;
         // 
         // label63
         // 
         this.label63.AutoSize = true;
         this.label63.Location = new System.Drawing.Point(5, 122);
         this.label63.Name = "label63";
         this.label63.Size = new System.Drawing.Size(79, 13);
         this.label63.TabIndex = 30;
         this.label63.Text = "Voltage Setting";
         // 
         // label57
         // 
         this.label57.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label57.Location = new System.Drawing.Point(17, 268);
         this.label57.Name = "label57";
         this.label57.Size = new System.Drawing.Size(120, 23);
         this.label57.TabIndex = 20;
         this.label57.Text = "Ch 2 Voltage";
         this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // VoltMeter2Ch2
         // 
         this.VoltMeter2Ch2.BaseArcColor = System.Drawing.Color.Gray;
         this.VoltMeter2Ch2.BaseArcRadius = 50;
         this.VoltMeter2Ch2.BaseArcStart = 150;
         this.VoltMeter2Ch2.BaseArcSweep = 240;
         this.VoltMeter2Ch2.BaseArcWidth = 2;
         this.VoltMeter2Ch2.Cap_Idx = ((byte)(1));
         this.VoltMeter2Ch2.CapColors = new System.Drawing.Color[] {
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black};
         this.VoltMeter2Ch2.CapPosition = new System.Drawing.Point(10, 10);
         this.VoltMeter2Ch2.CapsPosition = new System.Drawing.Point[] {
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10)};
         this.VoltMeter2Ch2.CapsText = new string[] {
        "",
        "",
        "",
        "",
        ""};
         this.VoltMeter2Ch2.CapText = "";
         this.VoltMeter2Ch2.Center = new System.Drawing.Point(68, 80);
         this.VoltMeter2Ch2.Location = new System.Drawing.Point(9, 144);
         this.VoltMeter2Ch2.MaxValue = 13F;
         this.VoltMeter2Ch2.MinValue = 4F;
         this.VoltMeter2Ch2.Name = "VoltMeter2Ch2";
         this.VoltMeter2Ch2.NeedleColor1 = Meter.Meter.NeedleColorEnum.Red;
         this.VoltMeter2Ch2.NeedleColor2 = System.Drawing.Color.DimGray;
         this.VoltMeter2Ch2.NeedleRadius = 50;
         this.VoltMeter2Ch2.NeedleType = 0;
         this.VoltMeter2Ch2.NeedleWidth = 3;
         this.VoltMeter2Ch2.Range_Idx = ((byte)(1));
         this.VoltMeter2Ch2.RangeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
         this.VoltMeter2Ch2.RangeEnabled = false;
         this.VoltMeter2Ch2.RangeEndValue = 16F;
         this.VoltMeter2Ch2.RangeInnerRadius = 10;
         this.VoltMeter2Ch2.RangeOuterRadius = 40;
         this.VoltMeter2Ch2.RangesColor = new System.Drawing.Color[] {
        System.Drawing.Color.LightGreen,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))),
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control};
         this.VoltMeter2Ch2.RangesEnabled = new bool[] {
        false,
        false,
        false,
        false,
        false};
         this.VoltMeter2Ch2.RangesEndValue = new float[] {
        300F,
        16F,
        0F,
        0F,
        0F};
         this.VoltMeter2Ch2.RangesInnerRadius = new int[] {
        70,
        10,
        70,
        70,
        70};
         this.VoltMeter2Ch2.RangesOuterRadius = new int[] {
        80,
        40,
        80,
        80,
        80};
         this.VoltMeter2Ch2.RangesStartValue = new float[] {
        -100F,
        5F,
        0F,
        0F,
        0F};
         this.VoltMeter2Ch2.RangeStartValue = 5F;
         this.VoltMeter2Ch2.ScaleLinesInterColor = System.Drawing.Color.Red;
         this.VoltMeter2Ch2.ScaleLinesInterInnerRadius = 45;
         this.VoltMeter2Ch2.ScaleLinesInterOuterRadius = 50;
         this.VoltMeter2Ch2.ScaleLinesInterWidth = 1;
         this.VoltMeter2Ch2.ScaleLinesMajorColor = System.Drawing.Color.Black;
         this.VoltMeter2Ch2.ScaleLinesMajorInnerRadius = 40;
         this.VoltMeter2Ch2.ScaleLinesMajorOuterRadius = 50;
         this.VoltMeter2Ch2.ScaleLinesMajorStepValue = 1F;
         this.VoltMeter2Ch2.ScaleLinesMajorWidth = 2;
         this.VoltMeter2Ch2.ScaleLinesMinorColor = System.Drawing.Color.Gray;
         this.VoltMeter2Ch2.ScaleLinesMinorInnerRadius = 45;
         this.VoltMeter2Ch2.ScaleLinesMinorNumOf = 3;
         this.VoltMeter2Ch2.ScaleLinesMinorOuterRadius = 50;
         this.VoltMeter2Ch2.ScaleLinesMinorWidth = 1;
         this.VoltMeter2Ch2.ScaleNumbersColor = System.Drawing.Color.Black;
         this.VoltMeter2Ch2.ScaleNumbersFormat = null;
         this.VoltMeter2Ch2.ScaleNumbersRadius = 62;
         this.VoltMeter2Ch2.ScaleNumbersRotation = 90;
         this.VoltMeter2Ch2.ScaleNumbersStartScaleLine = 1;
         this.VoltMeter2Ch2.ScaleNumbersStepScaleLines = 2;
         this.VoltMeter2Ch2.Size = new System.Drawing.Size(136, 123);
         this.VoltMeter2Ch2.TabIndex = 15;
         this.VoltMeter2Ch2.Text = "meter1";
         this.VoltMeter2Ch2.Value = 4F;
         // 
         // btnDC2VoltA
         // 
         this.btnDC2VoltA.Location = new System.Drawing.Point(168, 24);
         this.btnDC2VoltA.Name = "btnDC2VoltA";
         this.btnDC2VoltA.Size = new System.Drawing.Size(40, 23);
         this.btnDC2VoltA.TabIndex = 1;
         this.btnDC2VoltA.Text = "Get";
         this.btnDC2VoltA.UseVisualStyleBackColor = true;
         this.btnDC2VoltA.Click += new System.EventHandler(this.btnDC2VoltA_Click);
         // 
         // txtDC2CurrA
         // 
         this.txtDC2CurrA.Location = new System.Drawing.Point(60, 52);
         this.txtDC2CurrA.Name = "txtDC2CurrA";
         this.txtDC2CurrA.Size = new System.Drawing.Size(76, 20);
         this.txtDC2CurrA.TabIndex = 11;
         // 
         // label44
         // 
         this.label44.AutoSize = true;
         this.label44.Location = new System.Drawing.Point(23, 82);
         this.label44.Name = "label44";
         this.label44.Size = new System.Drawing.Size(35, 13);
         this.label44.TabIndex = 14;
         this.label44.Text = "Watts";
         // 
         // txtDC2VoltA
         // 
         this.txtDC2VoltA.Location = new System.Drawing.Point(60, 26);
         this.txtDC2VoltA.Name = "txtDC2VoltA";
         this.txtDC2VoltA.Size = new System.Drawing.Size(100, 20);
         this.txtDC2VoltA.TabIndex = 9;
         // 
         // txtDC2AWatts
         // 
         this.txtDC2AWatts.Location = new System.Drawing.Point(60, 78);
         this.txtDC2AWatts.Name = "txtDC2AWatts";
         this.txtDC2AWatts.Size = new System.Drawing.Size(76, 20);
         this.txtDC2AWatts.TabIndex = 13;
         // 
         // label18
         // 
         this.label18.AutoSize = true;
         this.label18.Location = new System.Drawing.Point(15, 30);
         this.label18.Name = "label18";
         this.label18.Size = new System.Drawing.Size(43, 13);
         this.label18.TabIndex = 10;
         this.label18.Text = "Voltage";
         // 
         // label13
         // 
         this.label13.AutoSize = true;
         this.label13.Location = new System.Drawing.Point(17, 56);
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size(41, 13);
         this.label13.TabIndex = 12;
         this.label13.Text = "Current";
         // 
         // btnDC2CurrA
         // 
         this.btnDC2CurrA.Location = new System.Drawing.Point(168, 50);
         this.btnDC2CurrA.Name = "btnDC2CurrA";
         this.btnDC2CurrA.Size = new System.Drawing.Size(40, 23);
         this.btnDC2CurrA.TabIndex = 2;
         this.btnDC2CurrA.Text = "Get";
         this.btnDC2CurrA.UseVisualStyleBackColor = true;
         this.btnDC2CurrA.Click += new System.EventHandler(this.btnDC2CurrA_Click);
         // 
         // label10
         // 
         this.label10.AutoSize = true;
         this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label10.Location = new System.Drawing.Point(6, 12);
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size(15, 13);
         this.label10.TabIndex = 8;
         this.label10.Text = "A";
         // 
         // groupBox16
         // 
         this.groupBox16.Controls.Add(this.pbxVoltageCh2B);
         this.groupBox16.Controls.Add(this.label100);
         this.groupBox16.Controls.Add(this.label64);
         this.groupBox16.Controls.Add(this.label65);
         this.groupBox16.Controls.Add(this.label66);
         this.groupBox16.Controls.Add(this.pbxFanOnCh2B);
         this.groupBox16.Controls.Add(this.pbxOutputCh2B);
         this.groupBox16.Controls.Add(this.pbxBusOnCh2B);
         this.groupBox16.Controls.Add(this.pbxFanBlockedCh2B);
         this.groupBox16.Controls.Add(this.label67);
         this.groupBox16.Controls.Add(this.pbxShortCh2B);
         this.groupBox16.Controls.Add(this.label68);
         this.groupBox16.Controls.Add(this.btnSetVoltage2B);
         this.groupBox16.Controls.Add(this.btnVoltSetting2B);
         this.groupBox16.Controls.Add(this.txtVoltageSetting2B);
         this.groupBox16.Controls.Add(this.label53);
         this.groupBox16.Controls.Add(this.label56);
         this.groupBox16.Controls.Add(this.VoltMeter2Ch1);
         this.groupBox16.Controls.Add(this.btnDC2VoltB);
         this.groupBox16.Controls.Add(this.label45);
         this.groupBox16.Controls.Add(this.txtDC2CurrB);
         this.groupBox16.Controls.Add(this.txtDC2VoltB);
         this.groupBox16.Controls.Add(this.txtDC2BWatts);
         this.groupBox16.Controls.Add(this.label17);
         this.groupBox16.Controls.Add(this.label12);
         this.groupBox16.Controls.Add(this.btnDC2CurrB);
         this.groupBox16.Controls.Add(this.label6);
         this.groupBox16.Location = new System.Drawing.Point(23, 60);
         this.groupBox16.Name = "groupBox16";
         this.groupBox16.Size = new System.Drawing.Size(260, 320);
         this.groupBox16.TabIndex = 24;
         this.groupBox16.TabStop = false;
         this.groupBox16.Text = "Channel 1 : 12 - 24";
         // 
         // pbxVoltageCh2B
         // 
         this.pbxVoltageCh2B.Location = new System.Drawing.Point(161, 199);
         this.pbxVoltageCh2B.Name = "pbxVoltageCh2B";
         this.pbxVoltageCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxVoltageCh2B.TabIndex = 108;
         this.pbxVoltageCh2B.TabStop = false;
         // 
         // label100
         // 
         this.label100.AutoSize = true;
         this.label100.Location = new System.Drawing.Point(180, 201);
         this.label100.Name = "label100";
         this.label100.Size = new System.Drawing.Size(68, 13);
         this.label100.TabIndex = 107;
         this.label100.Text = "Voltage Error";
         // 
         // label64
         // 
         this.label64.AutoSize = true;
         this.label64.Location = new System.Drawing.Point(180, 290);
         this.label64.Name = "label64";
         this.label64.Size = new System.Drawing.Size(42, 13);
         this.label64.TabIndex = 101;
         this.label64.Text = "Fan On";
         // 
         // label65
         // 
         this.label65.AutoSize = true;
         this.label65.Location = new System.Drawing.Point(180, 267);
         this.label65.Name = "label65";
         this.label65.Size = new System.Drawing.Size(56, 13);
         this.label65.TabIndex = 100;
         this.label65.Text = "Output On";
         // 
         // label66
         // 
         this.label66.AutoSize = true;
         this.label66.Location = new System.Drawing.Point(180, 244);
         this.label66.Name = "label66";
         this.label66.Size = new System.Drawing.Size(69, 13);
         this.label66.TabIndex = 99;
         this.label66.Text = "Bus Input On";
         // 
         // pbxFanOnCh2B
         // 
         this.pbxFanOnCh2B.Location = new System.Drawing.Point(161, 288);
         this.pbxFanOnCh2B.Name = "pbxFanOnCh2B";
         this.pbxFanOnCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxFanOnCh2B.TabIndex = 106;
         this.pbxFanOnCh2B.TabStop = false;
         // 
         // pbxOutputCh2B
         // 
         this.pbxOutputCh2B.Location = new System.Drawing.Point(161, 265);
         this.pbxOutputCh2B.Name = "pbxOutputCh2B";
         this.pbxOutputCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxOutputCh2B.TabIndex = 105;
         this.pbxOutputCh2B.TabStop = false;
         // 
         // pbxBusOnCh2B
         // 
         this.pbxBusOnCh2B.Location = new System.Drawing.Point(161, 242);
         this.pbxBusOnCh2B.Name = "pbxBusOnCh2B";
         this.pbxBusOnCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxBusOnCh2B.TabIndex = 104;
         this.pbxBusOnCh2B.TabStop = false;
         // 
         // pbxFanBlockedCh2B
         // 
         this.pbxFanBlockedCh2B.Location = new System.Drawing.Point(161, 154);
         this.pbxFanBlockedCh2B.Name = "pbxFanBlockedCh2B";
         this.pbxFanBlockedCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxFanBlockedCh2B.TabIndex = 102;
         this.pbxFanBlockedCh2B.TabStop = false;
         // 
         // label67
         // 
         this.label67.AutoSize = true;
         this.label67.Location = new System.Drawing.Point(180, 156);
         this.label67.Name = "label67";
         this.label67.Size = new System.Drawing.Size(67, 13);
         this.label67.TabIndex = 97;
         this.label67.Text = "Fan Blocked";
         // 
         // pbxShortCh2B
         // 
         this.pbxShortCh2B.Location = new System.Drawing.Point(161, 176);
         this.pbxShortCh2B.Name = "pbxShortCh2B";
         this.pbxShortCh2B.Size = new System.Drawing.Size(17, 17);
         this.pbxShortCh2B.TabIndex = 103;
         this.pbxShortCh2B.TabStop = false;
         // 
         // label68
         // 
         this.label68.AutoSize = true;
         this.label68.Location = new System.Drawing.Point(180, 178);
         this.label68.Name = "label68";
         this.label68.Size = new System.Drawing.Size(64, 13);
         this.label68.TabIndex = 98;
         this.label68.Text = "Short Circuit";
         // 
         // btnSetVoltage2B
         // 
         this.btnSetVoltage2B.Enabled = false;
         this.btnSetVoltage2B.Location = new System.Drawing.Point(212, 116);
         this.btnSetVoltage2B.Name = "btnSetVoltage2B";
         this.btnSetVoltage2B.Size = new System.Drawing.Size(40, 23);
         this.btnSetVoltage2B.TabIndex = 25;
         this.btnSetVoltage2B.Text = "Set";
         this.btnSetVoltage2B.UseVisualStyleBackColor = true;
         this.btnSetVoltage2B.Click += new System.EventHandler(this.btnSetVoltage2B_Click);
         // 
         // btnVoltSetting2B
         // 
         this.btnVoltSetting2B.Location = new System.Drawing.Point(168, 116);
         this.btnVoltSetting2B.Name = "btnVoltSetting2B";
         this.btnVoltSetting2B.Size = new System.Drawing.Size(40, 23);
         this.btnVoltSetting2B.TabIndex = 24;
         this.btnVoltSetting2B.Text = "Get";
         this.btnVoltSetting2B.UseVisualStyleBackColor = true;
         this.btnVoltSetting2B.Click += new System.EventHandler(this.btnVoltSetting2B_Click);
         // 
         // txtVoltageSetting2B
         // 
         this.txtVoltageSetting2B.Location = new System.Drawing.Point(86, 118);
         this.txtVoltageSetting2B.Name = "txtVoltageSetting2B";
         this.txtVoltageSetting2B.Size = new System.Drawing.Size(74, 20);
         this.txtVoltageSetting2B.TabIndex = 27;
         // 
         // label53
         // 
         this.label53.AutoSize = true;
         this.label53.Location = new System.Drawing.Point(5, 122);
         this.label53.Name = "label53";
         this.label53.Size = new System.Drawing.Size(79, 13);
         this.label53.TabIndex = 26;
         this.label53.Text = "Voltage Setting";
         // 
         // label56
         // 
         this.label56.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label56.Location = new System.Drawing.Point(17, 268);
         this.label56.Name = "label56";
         this.label56.Size = new System.Drawing.Size(120, 23);
         this.label56.TabIndex = 23;
         this.label56.Text = "Ch 1 Voltage";
         this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         // 
         // VoltMeter2Ch1
         // 
         this.VoltMeter2Ch1.BaseArcColor = System.Drawing.Color.Gray;
         this.VoltMeter2Ch1.BaseArcRadius = 50;
         this.VoltMeter2Ch1.BaseArcStart = 150;
         this.VoltMeter2Ch1.BaseArcSweep = 240;
         this.VoltMeter2Ch1.BaseArcWidth = 2;
         this.VoltMeter2Ch1.Cap_Idx = ((byte)(1));
         this.VoltMeter2Ch1.CapColors = new System.Drawing.Color[] {
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black,
        System.Drawing.Color.Black};
         this.VoltMeter2Ch1.CapPosition = new System.Drawing.Point(10, 10);
         this.VoltMeter2Ch1.CapsPosition = new System.Drawing.Point[] {
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10),
        new System.Drawing.Point(10, 10)};
         this.VoltMeter2Ch1.CapsText = new string[] {
        "",
        "",
        "",
        "",
        ""};
         this.VoltMeter2Ch1.CapText = "";
         this.VoltMeter2Ch1.Center = new System.Drawing.Point(68, 80);
         this.VoltMeter2Ch1.Location = new System.Drawing.Point(9, 144);
         this.VoltMeter2Ch1.MaxValue = 25F;
         this.VoltMeter2Ch1.MinValue = 11F;
         this.VoltMeter2Ch1.Name = "VoltMeter2Ch1";
         this.VoltMeter2Ch1.NeedleColor1 = Meter.Meter.NeedleColorEnum.Red;
         this.VoltMeter2Ch1.NeedleColor2 = System.Drawing.Color.DimGray;
         this.VoltMeter2Ch1.NeedleRadius = 50;
         this.VoltMeter2Ch1.NeedleType = 0;
         this.VoltMeter2Ch1.NeedleWidth = 3;
         this.VoltMeter2Ch1.Range_Idx = ((byte)(1));
         this.VoltMeter2Ch1.RangeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
         this.VoltMeter2Ch1.RangeEnabled = false;
         this.VoltMeter2Ch1.RangeEndValue = 16F;
         this.VoltMeter2Ch1.RangeInnerRadius = 10;
         this.VoltMeter2Ch1.RangeOuterRadius = 40;
         this.VoltMeter2Ch1.RangesColor = new System.Drawing.Color[] {
        System.Drawing.Color.LightGreen,
        System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128))))),
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control,
        System.Drawing.SystemColors.Control};
         this.VoltMeter2Ch1.RangesEnabled = new bool[] {
        false,
        false,
        false,
        false,
        false};
         this.VoltMeter2Ch1.RangesEndValue = new float[] {
        300F,
        16F,
        0F,
        0F,
        0F};
         this.VoltMeter2Ch1.RangesInnerRadius = new int[] {
        70,
        10,
        70,
        70,
        70};
         this.VoltMeter2Ch1.RangesOuterRadius = new int[] {
        80,
        40,
        80,
        80,
        80};
         this.VoltMeter2Ch1.RangesStartValue = new float[] {
        -100F,
        12F,
        0F,
        0F,
        0F};
         this.VoltMeter2Ch1.RangeStartValue = 12F;
         this.VoltMeter2Ch1.ScaleLinesInterColor = System.Drawing.Color.Red;
         this.VoltMeter2Ch1.ScaleLinesInterInnerRadius = 45;
         this.VoltMeter2Ch1.ScaleLinesInterOuterRadius = 50;
         this.VoltMeter2Ch1.ScaleLinesInterWidth = 1;
         this.VoltMeter2Ch1.ScaleLinesMajorColor = System.Drawing.Color.Black;
         this.VoltMeter2Ch1.ScaleLinesMajorInnerRadius = 40;
         this.VoltMeter2Ch1.ScaleLinesMajorOuterRadius = 50;
         this.VoltMeter2Ch1.ScaleLinesMajorStepValue = 1F;
         this.VoltMeter2Ch1.ScaleLinesMajorWidth = 2;
         this.VoltMeter2Ch1.ScaleLinesMinorColor = System.Drawing.Color.Gray;
         this.VoltMeter2Ch1.ScaleLinesMinorInnerRadius = 45;
         this.VoltMeter2Ch1.ScaleLinesMinorNumOf = 3;
         this.VoltMeter2Ch1.ScaleLinesMinorOuterRadius = 50;
         this.VoltMeter2Ch1.ScaleLinesMinorWidth = 1;
         this.VoltMeter2Ch1.ScaleNumbersColor = System.Drawing.Color.Black;
         this.VoltMeter2Ch1.ScaleNumbersFormat = null;
         this.VoltMeter2Ch1.ScaleNumbersRadius = 62;
         this.VoltMeter2Ch1.ScaleNumbersRotation = 90;
         this.VoltMeter2Ch1.ScaleNumbersStartScaleLine = 1;
         this.VoltMeter2Ch1.ScaleNumbersStepScaleLines = 2;
         this.VoltMeter2Ch1.Size = new System.Drawing.Size(136, 123);
         this.VoltMeter2Ch1.TabIndex = 22;
         this.VoltMeter2Ch1.Text = "VoltMeter";
         this.VoltMeter2Ch1.Value = 11F;
         // 
         // btnDC2VoltB
         // 
         this.btnDC2VoltB.Location = new System.Drawing.Point(168, 24);
         this.btnDC2VoltB.Name = "btnDC2VoltB";
         this.btnDC2VoltB.Size = new System.Drawing.Size(40, 23);
         this.btnDC2VoltB.TabIndex = 3;
         this.btnDC2VoltB.Text = "Get";
         this.btnDC2VoltB.UseVisualStyleBackColor = true;
         this.btnDC2VoltB.Click += new System.EventHandler(this.btnDC2VoltB_Click);
         // 
         // label45
         // 
         this.label45.AutoSize = true;
         this.label45.Location = new System.Drawing.Point(23, 82);
         this.label45.Name = "label45";
         this.label45.Size = new System.Drawing.Size(35, 13);
         this.label45.TabIndex = 21;
         this.label45.Text = "Watts";
         // 
         // txtDC2CurrB
         // 
         this.txtDC2CurrB.Location = new System.Drawing.Point(60, 52);
         this.txtDC2CurrB.Name = "txtDC2CurrB";
         this.txtDC2CurrB.Size = new System.Drawing.Size(76, 20);
         this.txtDC2CurrB.TabIndex = 18;
         // 
         // txtDC2VoltB
         // 
         this.txtDC2VoltB.Location = new System.Drawing.Point(60, 26);
         this.txtDC2VoltB.Name = "txtDC2VoltB";
         this.txtDC2VoltB.Size = new System.Drawing.Size(100, 20);
         this.txtDC2VoltB.TabIndex = 16;
         // 
         // txtDC2BWatts
         // 
         this.txtDC2BWatts.Location = new System.Drawing.Point(60, 78);
         this.txtDC2BWatts.Name = "txtDC2BWatts";
         this.txtDC2BWatts.Size = new System.Drawing.Size(76, 20);
         this.txtDC2BWatts.TabIndex = 20;
         // 
         // label17
         // 
         this.label17.AutoSize = true;
         this.label17.Location = new System.Drawing.Point(15, 30);
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size(43, 13);
         this.label17.TabIndex = 17;
         this.label17.Text = "Voltage";
         // 
         // label12
         // 
         this.label12.AutoSize = true;
         this.label12.Location = new System.Drawing.Point(17, 56);
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size(41, 13);
         this.label12.TabIndex = 19;
         this.label12.Text = "Current";
         // 
         // btnDC2CurrB
         // 
         this.btnDC2CurrB.Location = new System.Drawing.Point(168, 50);
         this.btnDC2CurrB.Name = "btnDC2CurrB";
         this.btnDC2CurrB.Size = new System.Drawing.Size(40, 23);
         this.btnDC2CurrB.TabIndex = 4;
         this.btnDC2CurrB.Text = "Get";
         this.btnDC2CurrB.UseVisualStyleBackColor = true;
         this.btnDC2CurrB.Click += new System.EventHandler(this.btnDC2CurrB_Click);
         // 
         // label6
         // 
         this.label6.AutoSize = true;
         this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
         this.label6.Location = new System.Drawing.Point(6, 12);
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size(15, 13);
         this.label6.TabIndex = 15;
         this.label6.Text = "B";
         // 
         // txtDC2Status
         // 
         this.txtDC2Status.Location = new System.Drawing.Point(668, 355);
         this.txtDC2Status.Name = "txtDC2Status";
         this.txtDC2Status.Size = new System.Drawing.Size(72, 20);
         this.txtDC2Status.TabIndex = 23;
         // 
         // txtDC2Errors
         // 
         this.txtDC2Errors.Location = new System.Drawing.Point(668, 329);
         this.txtDC2Errors.Name = "txtDC2Errors";
         this.txtDC2Errors.Size = new System.Drawing.Size(72, 20);
         this.txtDC2Errors.TabIndex = 22;
         // 
         // btnDC2Status
         // 
         this.btnDC2Status.Location = new System.Drawing.Point(587, 355);
         this.btnDC2Status.Name = "btnDC2Status";
         this.btnDC2Status.Size = new System.Drawing.Size(75, 23);
         this.btnDC2Status.TabIndex = 6;
         this.btnDC2Status.Text = "Status";
         this.btnDC2Status.UseVisualStyleBackColor = true;
         this.btnDC2Status.Click += new System.EventHandler(this.btnDC2Status_Click);
         // 
         // btnDC2Errors
         // 
         this.btnDC2Errors.Location = new System.Drawing.Point(587, 327);
         this.btnDC2Errors.Name = "btnDC2Errors";
         this.btnDC2Errors.Size = new System.Drawing.Size(75, 23);
         this.btnDC2Errors.TabIndex = 5;
         this.btnDC2Errors.Text = "Errors";
         this.btnDC2Errors.UseVisualStyleBackColor = true;
         this.btnDC2Errors.Click += new System.EventHandler(this.btnDC2Errors_Click);
         // 
         // btnDC2Firmware
         // 
         this.btnDC2Firmware.Location = new System.Drawing.Point(6, 6);
         this.btnDC2Firmware.Name = "btnDC2Firmware";
         this.btnDC2Firmware.Size = new System.Drawing.Size(100, 23);
         this.btnDC2Firmware.TabIndex = 0;
         this.btnDC2Firmware.Text = "Firmware";
         this.btnDC2Firmware.UseVisualStyleBackColor = true;
         this.btnDC2Firmware.Click += new System.EventHandler(this.btnDC2Firmware_Click);
         // 
         // txtDC2Firmware
         // 
         this.txtDC2Firmware.Location = new System.Drawing.Point(110, 8);
         this.txtDC2Firmware.Name = "txtDC2Firmware";
         this.txtDC2Firmware.Size = new System.Drawing.Size(100, 20);
         this.txtDC2Firmware.TabIndex = 7;
         // 
         // tabPage6
         // 
         this.tabPage6.Controls.Add(this.btnCommFromFlash);
         this.tabPage6.Controls.Add(this.groupBox25);
         this.tabPage6.Controls.Add(this.btnSendWLAN);
         this.tabPage6.Controls.Add(this.groupBox8);
         this.tabPage6.Controls.Add(this.btnResetBaudSetFlag);
         this.tabPage6.Controls.Add(this.pictureBox1);
         this.tabPage6.Controls.Add(this.lblWLAN);
         this.tabPage6.Controls.Add(this.txtWLAN);
         this.tabPage6.Controls.Add(this.btnGetWLAN);
         this.tabPage6.Controls.Add(this.txtDeviceMACAddress);
         this.tabPage6.Controls.Add(this.btnDeviceMACAddress);
         this.tabPage6.Controls.Add(this.groupBox20);
         this.tabPage6.Controls.Add(this.txtLinkQuality);
         this.tabPage6.Controls.Add(this.txtMACAddress);
         this.tabPage6.Controls.Add(this.txtIPAddress);
         this.tabPage6.Controls.Add(this.btnLinkQuality);
         this.tabPage6.Controls.Add(this.btnMACAddress);
         this.tabPage6.Controls.Add(this.btnIPAddress);
         this.tabPage6.Controls.Add(this.btnWiFiFirmware);
         this.tabPage6.Controls.Add(this.txtWiFiFirmware);
         this.tabPage6.Location = new System.Drawing.Point(4, 22);
         this.tabPage6.Name = "tabPage6";
         this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage6.Size = new System.Drawing.Size(852, 456);
         this.tabPage6.TabIndex = 5;
         this.tabPage6.Text = "WiFi";
         this.tabPage6.UseVisualStyleBackColor = true;
         // 
         // btnCommFromFlash
         // 
         this.btnCommFromFlash.Location = new System.Drawing.Point(541, 365);
         this.btnCommFromFlash.Name = "btnCommFromFlash";
         this.btnCommFromFlash.Size = new System.Drawing.Size(229, 23);
         this.btnCommFromFlash.TabIndex = 27;
         this.btnCommFromFlash.Text = "Commission Roving From Flash";
         this.btnCommFromFlash.UseVisualStyleBackColor = true;
         this.btnCommFromFlash.Click += new System.EventHandler(this.btnCommFromFlash_Click);
         // 
         // groupBox25
         // 
         this.groupBox25.Controls.Add(this.btnSetCaptureURL);
         this.groupBox25.Controls.Add(this.btnGetCaptureURL);
         this.groupBox25.Controls.Add(this.txtCaptureURL);
         this.groupBox25.Location = new System.Drawing.Point(279, 277);
         this.groupBox25.Name = "groupBox25";
         this.groupBox25.Size = new System.Drawing.Size(424, 82);
         this.groupBox25.TabIndex = 26;
         this.groupBox25.TabStop = false;
         this.groupBox25.Text = "Capture URL";
         // 
         // btnSetCaptureURL
         // 
         this.btnSetCaptureURL.Location = new System.Drawing.Point(116, 45);
         this.btnSetCaptureURL.Name = "btnSetCaptureURL";
         this.btnSetCaptureURL.Size = new System.Drawing.Size(40, 23);
         this.btnSetCaptureURL.TabIndex = 27;
         this.btnSetCaptureURL.Text = "Set";
         this.btnSetCaptureURL.UseVisualStyleBackColor = true;
         this.btnSetCaptureURL.Click += new System.EventHandler(this.btnSetCaptureURL_Click);
         // 
         // btnGetCaptureURL
         // 
         this.btnGetCaptureURL.Location = new System.Drawing.Point(12, 45);
         this.btnGetCaptureURL.Name = "btnGetCaptureURL";
         this.btnGetCaptureURL.Size = new System.Drawing.Size(40, 23);
         this.btnGetCaptureURL.TabIndex = 26;
         this.btnGetCaptureURL.Text = "Get";
         this.btnGetCaptureURL.UseVisualStyleBackColor = true;
         this.btnGetCaptureURL.Click += new System.EventHandler(this.btnGetCaptureURL_Click);
         // 
         // txtCaptureURL
         // 
         this.txtCaptureURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
         this.txtCaptureURL.Location = new System.Drawing.Point(12, 19);
         this.txtCaptureURL.Name = "txtCaptureURL";
         this.txtCaptureURL.Size = new System.Drawing.Size(397, 20);
         this.txtCaptureURL.TabIndex = 25;
         // 
         // btnSendWLAN
         // 
         this.btnSendWLAN.Location = new System.Drawing.Point(565, 83);
         this.btnSendWLAN.Name = "btnSendWLAN";
         this.btnSendWLAN.Size = new System.Drawing.Size(177, 23);
         this.btnSendWLAN.TabIndex = 23;
         this.btnSendWLAN.Text = "Send WLAN Parameters";
         this.btnSendWLAN.UseVisualStyleBackColor = true;
         this.btnSendWLAN.Visible = false;
         this.btnSendWLAN.Click += new System.EventHandler(this.btnSendWLAN_Click);
         // 
         // groupBox8
         // 
         this.groupBox8.Controls.Add(this.btnSetTxInterval);
         this.groupBox8.Controls.Add(this.btnGetTxInterval);
         this.groupBox8.Controls.Add(this.label33);
         this.groupBox8.Controls.Add(this.txtTxInterval);
         this.groupBox8.Location = new System.Drawing.Point(565, 124);
         this.groupBox8.Name = "groupBox8";
         this.groupBox8.Size = new System.Drawing.Size(138, 100);
         this.groupBox8.TabIndex = 5;
         this.groupBox8.TabStop = false;
         this.groupBox8.Text = "Transmit Interval";
         // 
         // btnSetTxInterval
         // 
         this.btnSetTxInterval.Location = new System.Drawing.Point(67, 57);
         this.btnSetTxInterval.Name = "btnSetTxInterval";
         this.btnSetTxInterval.Size = new System.Drawing.Size(40, 23);
         this.btnSetTxInterval.TabIndex = 1;
         this.btnSetTxInterval.Text = "Set";
         this.btnSetTxInterval.UseVisualStyleBackColor = true;
         this.btnSetTxInterval.Click += new System.EventHandler(this.btnSetTxInterval_Click);
         // 
         // btnGetTxInterval
         // 
         this.btnGetTxInterval.Location = new System.Drawing.Point(15, 57);
         this.btnGetTxInterval.Name = "btnGetTxInterval";
         this.btnGetTxInterval.Size = new System.Drawing.Size(40, 23);
         this.btnGetTxInterval.TabIndex = 0;
         this.btnGetTxInterval.Text = "Get";
         this.btnGetTxInterval.UseVisualStyleBackColor = true;
         this.btnGetTxInterval.Click += new System.EventHandler(this.btnTxInterval_Click);
         // 
         // label33
         // 
         this.label33.AutoSize = true;
         this.label33.Location = new System.Drawing.Point(64, 24);
         this.label33.Name = "label33";
         this.label33.Size = new System.Drawing.Size(49, 13);
         this.label33.TabIndex = 3;
         this.label33.Text = "Seconds";
         // 
         // txtTxInterval
         // 
         this.txtTxInterval.Location = new System.Drawing.Point(15, 20);
         this.txtTxInterval.Name = "txtTxInterval";
         this.txtTxInterval.Size = new System.Drawing.Size(47, 20);
         this.txtTxInterval.TabIndex = 2;
         // 
         // btnResetBaudSetFlag
         // 
         this.btnResetBaudSetFlag.Enabled = false;
         this.btnResetBaudSetFlag.Location = new System.Drawing.Point(40, 350);
         this.btnResetBaudSetFlag.Name = "btnResetBaudSetFlag";
         this.btnResetBaudSetFlag.Size = new System.Drawing.Size(157, 23);
         this.btnResetBaudSetFlag.TabIndex = 22;
         this.btnResetBaudSetFlag.Text = "Reset Baud Set Flag";
         this.btnResetBaudSetFlag.UseVisualStyleBackColor = true;
         this.btnResetBaudSetFlag.Click += new System.EventHandler(this.btnResetBaudSetFlag_Click);
         // 
         // pictureBox1
         // 
         this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
         this.pictureBox1.Location = new System.Drawing.Point(565, 6);
         this.pictureBox1.Name = "pictureBox1";
         this.pictureBox1.Size = new System.Drawing.Size(281, 71);
         this.pictureBox1.TabIndex = 21;
         this.pictureBox1.TabStop = false;
         this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
         // 
         // lblWLAN
         // 
         this.lblWLAN.AutoSize = true;
         this.lblWLAN.Location = new System.Drawing.Point(147, 430);
         this.lblWLAN.Name = "lblWLAN";
         this.lblWLAN.Size = new System.Drawing.Size(201, 13);
         this.lblWLAN.TabIndex = 20;
         this.lblWLAN.Text = "SSID | Channel | ExtAnt | Join | Auth | Key";
         this.lblWLAN.Visible = false;
         // 
         // txtWLAN
         // 
         this.txtWLAN.Location = new System.Drawing.Point(146, 403);
         this.txtWLAN.Name = "txtWLAN";
         this.txtWLAN.Size = new System.Drawing.Size(624, 20);
         this.txtWLAN.TabIndex = 19;
         this.txtWLAN.Visible = false;
         // 
         // btnGetWLAN
         // 
         this.btnGetWLAN.Location = new System.Drawing.Point(40, 401);
         this.btnGetWLAN.Name = "btnGetWLAN";
         this.btnGetWLAN.Size = new System.Drawing.Size(100, 23);
         this.btnGetWLAN.TabIndex = 18;
         this.btnGetWLAN.Text = "Get WLAN Data";
         this.btnGetWLAN.UseVisualStyleBackColor = true;
         this.btnGetWLAN.Visible = false;
         this.btnGetWLAN.Click += new System.EventHandler(this.btnGetWLAN_Click);
         // 
         // txtDeviceMACAddress
         // 
         this.txtDeviceMACAddress.Location = new System.Drawing.Point(146, 95);
         this.txtDeviceMACAddress.Name = "txtDeviceMACAddress";
         this.txtDeviceMACAddress.Size = new System.Drawing.Size(110, 20);
         this.txtDeviceMACAddress.TabIndex = 13;
         // 
         // btnDeviceMACAddress
         // 
         this.btnDeviceMACAddress.Location = new System.Drawing.Point(40, 93);
         this.btnDeviceMACAddress.Name = "btnDeviceMACAddress";
         this.btnDeviceMACAddress.Size = new System.Drawing.Size(100, 23);
         this.btnDeviceMACAddress.TabIndex = 12;
         this.btnDeviceMACAddress.Text = "MAC Address";
         this.btnDeviceMACAddress.UseVisualStyleBackColor = true;
         this.btnDeviceMACAddress.Click += new System.EventHandler(this.btnDeviceMACAddress_Click);
         // 
         // groupBox20
         // 
         this.groupBox20.Controls.Add(this.label161);
         this.groupBox20.Controls.Add(this.txtPIN);
         this.groupBox20.Controls.Add(this.rdoDrawersUnlockedDP);
         this.groupBox20.Controls.Add(this.txtServiceRequest);
         this.groupBox20.Controls.Add(this.rdoServiceRequestDP);
         this.groupBox20.Controls.Add(this.rdoMedBoardStatsDP);
         this.groupBox20.Controls.Add(this.rdoBoardSerialsDP);
         this.groupBox20.Controls.Add(this.rdoWLANDP);
         this.groupBox20.Controls.Add(this.rdoAlertDP);
         this.groupBox20.Controls.Add(this.rdoQCDP);
         this.groupBox20.Controls.Add(this.rdoCommCodeAckDP);
         this.groupBox20.Controls.Add(this.rdoRebootRequestDP);
         this.groupBox20.Controls.Add(this.rdoBatteryDepletedDP);
         this.groupBox20.Controls.Add(this.rdoACDisconnectedDP);
         this.groupBox20.Controls.Add(this.rdoACConnectedDP);
         this.groupBox20.Controls.Add(this.rdoHeartBeatDP);
         this.groupBox20.Controls.Add(this.rdoBatteryRemovedDP);
         this.groupBox20.Controls.Add(this.rdoBatteryInsertedDP);
         this.groupBox20.Controls.Add(this.rdoStandardDP);
         this.groupBox20.Controls.Add(this.btnSendData);
         this.groupBox20.Location = new System.Drawing.Point(279, 8);
         this.groupBox20.Name = "groupBox20";
         this.groupBox20.Size = new System.Drawing.Size(279, 263);
         this.groupBox20.TabIndex = 11;
         this.groupBox20.TabStop = false;
         this.groupBox20.Text = "Data Packet";
         // 
         // label161
         // 
         this.label161.AutoSize = true;
         this.label161.Location = new System.Drawing.Point(213, 202);
         this.label161.Name = "label161";
         this.label161.Size = new System.Drawing.Size(25, 13);
         this.label161.TabIndex = 23;
         this.label161.Text = "PIN";
         // 
         // txtPIN
         // 
         this.txtPIN.Location = new System.Drawing.Point(133, 198);
         this.txtPIN.Name = "txtPIN";
         this.txtPIN.Size = new System.Drawing.Size(77, 20);
         this.txtPIN.TabIndex = 22;
         // 
         // rdoDrawersUnlockedDP
         // 
         this.rdoDrawersUnlockedDP.AutoSize = true;
         this.rdoDrawersUnlockedDP.Location = new System.Drawing.Point(14, 199);
         this.rdoDrawersUnlockedDP.Name = "rdoDrawersUnlockedDP";
         this.rdoDrawersUnlockedDP.Size = new System.Drawing.Size(113, 17);
         this.rdoDrawersUnlockedDP.TabIndex = 21;
         this.rdoDrawersUnlockedDP.TabStop = true;
         this.rdoDrawersUnlockedDP.Text = "Drawers Unlocked";
         this.rdoDrawersUnlockedDP.UseVisualStyleBackColor = true;
         // 
         // txtServiceRequest
         // 
         this.txtServiceRequest.Location = new System.Drawing.Point(119, 174);
         this.txtServiceRequest.Name = "txtServiceRequest";
         this.txtServiceRequest.Size = new System.Drawing.Size(51, 20);
         this.txtServiceRequest.TabIndex = 20;
         // 
         // rdoServiceRequestDP
         // 
         this.rdoServiceRequestDP.AutoSize = true;
         this.rdoServiceRequestDP.Location = new System.Drawing.Point(14, 175);
         this.rdoServiceRequestDP.Name = "rdoServiceRequestDP";
         this.rdoServiceRequestDP.Size = new System.Drawing.Size(104, 17);
         this.rdoServiceRequestDP.TabIndex = 19;
         this.rdoServiceRequestDP.TabStop = true;
         this.rdoServiceRequestDP.Text = "Service Request";
         this.rdoServiceRequestDP.UseVisualStyleBackColor = true;
         // 
         // rdoMedBoardStatsDP
         // 
         this.rdoMedBoardStatsDP.AutoSize = true;
         this.rdoMedBoardStatsDP.Location = new System.Drawing.Point(148, 153);
         this.rdoMedBoardStatsDP.Name = "rdoMedBoardStatsDP";
         this.rdoMedBoardStatsDP.Size = new System.Drawing.Size(101, 17);
         this.rdoMedBoardStatsDP.TabIndex = 18;
         this.rdoMedBoardStatsDP.Text = "MedBoard Stats";
         this.rdoMedBoardStatsDP.UseVisualStyleBackColor = true;
         // 
         // rdoBoardSerialsDP
         // 
         this.rdoBoardSerialsDP.AutoSize = true;
         this.rdoBoardSerialsDP.Location = new System.Drawing.Point(148, 131);
         this.rdoBoardSerialsDP.Name = "rdoBoardSerialsDP";
         this.rdoBoardSerialsDP.Size = new System.Drawing.Size(87, 17);
         this.rdoBoardSerialsDP.TabIndex = 17;
         this.rdoBoardSerialsDP.Text = "Board Serials";
         this.rdoBoardSerialsDP.UseVisualStyleBackColor = true;
         // 
         // rdoWLANDP
         // 
         this.rdoWLANDP.AutoSize = true;
         this.rdoWLANDP.Location = new System.Drawing.Point(148, 109);
         this.rdoWLANDP.Name = "rdoWLANDP";
         this.rdoWLANDP.Size = new System.Drawing.Size(113, 17);
         this.rdoWLANDP.TabIndex = 16;
         this.rdoWLANDP.Text = "WLAN Parameters";
         this.rdoWLANDP.UseVisualStyleBackColor = true;
         // 
         // rdoAlertDP
         // 
         this.rdoAlertDP.AutoSize = true;
         this.rdoAlertDP.Location = new System.Drawing.Point(148, 87);
         this.rdoAlertDP.Name = "rdoAlertDP";
         this.rdoAlertDP.Size = new System.Drawing.Size(46, 17);
         this.rdoAlertDP.TabIndex = 15;
         this.rdoAlertDP.Text = "Alert";
         this.rdoAlertDP.UseVisualStyleBackColor = true;
         // 
         // rdoQCDP
         // 
         this.rdoQCDP.AutoSize = true;
         this.rdoQCDP.Location = new System.Drawing.Point(148, 65);
         this.rdoQCDP.Name = "rdoQCDP";
         this.rdoQCDP.Size = new System.Drawing.Size(40, 17);
         this.rdoQCDP.TabIndex = 14;
         this.rdoQCDP.Text = "QC";
         this.rdoQCDP.UseVisualStyleBackColor = true;
         // 
         // rdoCommCodeAckDP
         // 
         this.rdoCommCodeAckDP.AutoSize = true;
         this.rdoCommCodeAckDP.Location = new System.Drawing.Point(148, 43);
         this.rdoCommCodeAckDP.Name = "rdoCommCodeAckDP";
         this.rdoCommCodeAckDP.Size = new System.Drawing.Size(124, 17);
         this.rdoCommCodeAckDP.TabIndex = 13;
         this.rdoCommCodeAckDP.Text = "Command Code ACK";
         this.rdoCommCodeAckDP.UseVisualStyleBackColor = true;
         // 
         // rdoRebootRequestDP
         // 
         this.rdoRebootRequestDP.AutoSize = true;
         this.rdoRebootRequestDP.Location = new System.Drawing.Point(148, 21);
         this.rdoRebootRequestDP.Name = "rdoRebootRequestDP";
         this.rdoRebootRequestDP.Size = new System.Drawing.Size(115, 17);
         this.rdoRebootRequestDP.TabIndex = 12;
         this.rdoRebootRequestDP.Text = "Reboot Requested";
         this.rdoRebootRequestDP.UseVisualStyleBackColor = true;
         // 
         // rdoBatteryDepletedDP
         // 
         this.rdoBatteryDepletedDP.AutoSize = true;
         this.rdoBatteryDepletedDP.Location = new System.Drawing.Point(14, 153);
         this.rdoBatteryDepletedDP.Name = "rdoBatteryDepletedDP";
         this.rdoBatteryDepletedDP.Size = new System.Drawing.Size(104, 17);
         this.rdoBatteryDepletedDP.TabIndex = 11;
         this.rdoBatteryDepletedDP.Text = "Battery Depleted";
         this.rdoBatteryDepletedDP.UseVisualStyleBackColor = true;
         // 
         // rdoACDisconnectedDP
         // 
         this.rdoACDisconnectedDP.AutoSize = true;
         this.rdoACDisconnectedDP.Location = new System.Drawing.Point(14, 131);
         this.rdoACDisconnectedDP.Name = "rdoACDisconnectedDP";
         this.rdoACDisconnectedDP.Size = new System.Drawing.Size(108, 17);
         this.rdoACDisconnectedDP.TabIndex = 10;
         this.rdoACDisconnectedDP.Text = "AC Disconnected";
         this.rdoACDisconnectedDP.UseVisualStyleBackColor = true;
         // 
         // rdoACConnectedDP
         // 
         this.rdoACConnectedDP.AutoSize = true;
         this.rdoACConnectedDP.Location = new System.Drawing.Point(14, 109);
         this.rdoACConnectedDP.Name = "rdoACConnectedDP";
         this.rdoACConnectedDP.Size = new System.Drawing.Size(94, 17);
         this.rdoACConnectedDP.TabIndex = 9;
         this.rdoACConnectedDP.Text = "AC Connected";
         this.rdoACConnectedDP.UseVisualStyleBackColor = true;
         // 
         // rdoHeartBeatDP
         // 
         this.rdoHeartBeatDP.AutoSize = true;
         this.rdoHeartBeatDP.Location = new System.Drawing.Point(14, 87);
         this.rdoHeartBeatDP.Name = "rdoHeartBeatDP";
         this.rdoHeartBeatDP.Size = new System.Drawing.Size(76, 17);
         this.rdoHeartBeatDP.TabIndex = 8;
         this.rdoHeartBeatDP.Text = "Heart Beat";
         this.rdoHeartBeatDP.UseVisualStyleBackColor = true;
         // 
         // rdoBatteryRemovedDP
         // 
         this.rdoBatteryRemovedDP.AutoSize = true;
         this.rdoBatteryRemovedDP.Location = new System.Drawing.Point(14, 65);
         this.rdoBatteryRemovedDP.Name = "rdoBatteryRemovedDP";
         this.rdoBatteryRemovedDP.Size = new System.Drawing.Size(107, 17);
         this.rdoBatteryRemovedDP.TabIndex = 7;
         this.rdoBatteryRemovedDP.Text = "Battery Removed";
         this.rdoBatteryRemovedDP.UseVisualStyleBackColor = true;
         // 
         // rdoBatteryInsertedDP
         // 
         this.rdoBatteryInsertedDP.AutoSize = true;
         this.rdoBatteryInsertedDP.Location = new System.Drawing.Point(14, 43);
         this.rdoBatteryInsertedDP.Name = "rdoBatteryInsertedDP";
         this.rdoBatteryInsertedDP.Size = new System.Drawing.Size(99, 17);
         this.rdoBatteryInsertedDP.TabIndex = 6;
         this.rdoBatteryInsertedDP.Text = "Battery Inserted";
         this.rdoBatteryInsertedDP.UseVisualStyleBackColor = true;
         // 
         // rdoStandardDP
         // 
         this.rdoStandardDP.AutoSize = true;
         this.rdoStandardDP.Checked = true;
         this.rdoStandardDP.Location = new System.Drawing.Point(14, 21);
         this.rdoStandardDP.Name = "rdoStandardDP";
         this.rdoStandardDP.Size = new System.Drawing.Size(68, 17);
         this.rdoStandardDP.TabIndex = 5;
         this.rdoStandardDP.TabStop = true;
         this.rdoStandardDP.Text = "Standard";
         this.rdoStandardDP.UseVisualStyleBackColor = true;
         // 
         // btnSendData
         // 
         this.btnSendData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnSendData.Location = new System.Drawing.Point(148, 230);
         this.btnSendData.Name = "btnSendData";
         this.btnSendData.Size = new System.Drawing.Size(121, 23);
         this.btnSendData.TabIndex = 4;
         this.btnSendData.Text = "Send Data Packet";
         this.btnSendData.UseVisualStyleBackColor = true;
         this.btnSendData.Click += new System.EventHandler(this.btnSendData_Click);
         // 
         // txtLinkQuality
         // 
         this.txtLinkQuality.Location = new System.Drawing.Point(146, 124);
         this.txtLinkQuality.Name = "txtLinkQuality";
         this.txtLinkQuality.Size = new System.Drawing.Size(64, 20);
         this.txtLinkQuality.TabIndex = 10;
         // 
         // txtMACAddress
         // 
         this.txtMACAddress.Location = new System.Drawing.Point(146, 182);
         this.txtMACAddress.Name = "txtMACAddress";
         this.txtMACAddress.Size = new System.Drawing.Size(110, 20);
         this.txtMACAddress.TabIndex = 9;
         // 
         // txtIPAddress
         // 
         this.txtIPAddress.Location = new System.Drawing.Point(146, 66);
         this.txtIPAddress.Name = "txtIPAddress";
         this.txtIPAddress.Size = new System.Drawing.Size(100, 20);
         this.txtIPAddress.TabIndex = 8;
         // 
         // btnLinkQuality
         // 
         this.btnLinkQuality.Location = new System.Drawing.Point(40, 122);
         this.btnLinkQuality.Name = "btnLinkQuality";
         this.btnLinkQuality.Size = new System.Drawing.Size(100, 23);
         this.btnLinkQuality.TabIndex = 3;
         this.btnLinkQuality.Text = "Link Quality";
         this.btnLinkQuality.UseVisualStyleBackColor = true;
         this.btnLinkQuality.Click += new System.EventHandler(this.btnLinkQuality_Click);
         // 
         // btnMACAddress
         // 
         this.btnMACAddress.Location = new System.Drawing.Point(40, 180);
         this.btnMACAddress.Name = "btnMACAddress";
         this.btnMACAddress.Size = new System.Drawing.Size(100, 23);
         this.btnMACAddress.TabIndex = 2;
         this.btnMACAddress.Text = "AP MAC Address";
         this.btnMACAddress.UseVisualStyleBackColor = true;
         this.btnMACAddress.Click += new System.EventHandler(this.btnMACAddress_Click);
         // 
         // btnIPAddress
         // 
         this.btnIPAddress.Location = new System.Drawing.Point(40, 64);
         this.btnIPAddress.Name = "btnIPAddress";
         this.btnIPAddress.Size = new System.Drawing.Size(100, 23);
         this.btnIPAddress.TabIndex = 1;
         this.btnIPAddress.Text = "IP Address";
         this.btnIPAddress.UseVisualStyleBackColor = true;
         this.btnIPAddress.Click += new System.EventHandler(this.btnIPAddress_Click);
         // 
         // btnWiFiFirmware
         // 
         this.btnWiFiFirmware.Location = new System.Drawing.Point(6, 6);
         this.btnWiFiFirmware.Name = "btnWiFiFirmware";
         this.btnWiFiFirmware.Size = new System.Drawing.Size(100, 23);
         this.btnWiFiFirmware.TabIndex = 0;
         this.btnWiFiFirmware.Text = "Firmware";
         this.btnWiFiFirmware.UseVisualStyleBackColor = true;
         this.btnWiFiFirmware.Click += new System.EventHandler(this.btnWiFiFirmware_Click);
         // 
         // txtWiFiFirmware
         // 
         this.txtWiFiFirmware.Location = new System.Drawing.Point(110, 8);
         this.txtWiFiFirmware.Name = "txtWiFiFirmware";
         this.txtWiFiFirmware.Size = new System.Drawing.Size(100, 20);
         this.txtWiFiFirmware.TabIndex = 7;
         // 
         // tabPage7
         // 
         this.tabPage7.Controls.Add(this.label127);
         this.tabPage7.Controls.Add(this.label126);
         this.tabPage7.Controls.Add(this.label125);
         this.tabPage7.Controls.Add(this.label124);
         this.tabPage7.Controls.Add(this.label123);
         this.tabPage7.Controls.Add(this.label122);
         this.tabPage7.Controls.Add(this.label121);
         this.tabPage7.Controls.Add(this.label120);
         this.tabPage7.Controls.Add(this.label119);
         this.tabPage7.Controls.Add(this.btnPollBattery);
         this.tabPage7.Controls.Add(this.label118);
         this.tabPage7.Controls.Add(this.label117);
         this.tabPage7.Controls.Add(this.label116);
         this.tabPage7.Controls.Add(this.label115);
         this.tabPage7.Controls.Add(this.label114);
         this.tabPage7.Controls.Add(this.label113);
         this.tabPage7.Controls.Add(this.label112);
         this.tabPage7.Controls.Add(this.label111);
         this.tabPage7.Controls.Add(this.label110);
         this.tabPage7.Controls.Add(this.label109);
         this.tabPage7.Controls.Add(this.label108);
         this.tabPage7.Controls.Add(this.label107);
         this.tabPage7.Controls.Add(this.label106);
         this.tabPage7.Controls.Add(this.label105);
         this.tabPage7.Controls.Add(this.txtMainBatteryOut);
         this.tabPage7.Controls.Add(this.btnMainBatteryOut);
         this.tabPage7.Controls.Add(this.label48);
         this.tabPage7.Controls.Add(this.txtThermTemp);
         this.tabPage7.Controls.Add(this.label47);
         this.tabPage7.Controls.Add(this.txtEfficiency);
         this.tabPage7.Controls.Add(this.label46);
         this.tabPage7.Controls.Add(this.groupBox35);
         this.tabPage7.Controls.Add(this.groupBox2);
         this.tabPage7.Controls.Add(this.btnBattModeD);
         this.tabPage7.Controls.Add(this.btnChargeStatD);
         this.tabPage7.Controls.Add(this.btnSafeAlertD);
         this.tabPage7.Controls.Add(this.btnPFAlertD);
         this.tabPage7.Controls.Add(this.btnPFStatD);
         this.tabPage7.Controls.Add(this.btnSafeStatD);
         this.tabPage7.Controls.Add(this.btnBattStatD);
         this.tabPage7.Controls.Add(this.btnOpStatD);
         this.tabPage7.Controls.Add(this.txtBatteryMode);
         this.tabPage7.Controls.Add(this.txtChargingStatus);
         this.tabPage7.Controls.Add(this.txtSafetyAlert);
         this.tabPage7.Controls.Add(this.txtPFAlert);
         this.tabPage7.Controls.Add(this.txtPFStatus);
         this.tabPage7.Controls.Add(this.txtSafetyStatus);
         this.tabPage7.Controls.Add(this.txtBatteryStatus);
         this.tabPage7.Controls.Add(this.txtOperationStatus);
         this.tabPage7.Controls.Add(this.txtFETStatus);
         this.tabPage7.Controls.Add(this.groupBox7);
         this.tabPage7.Controls.Add(this.label104);
         this.tabPage7.Controls.Add(this.txtMeasuredVoltage);
         this.tabPage7.Controls.Add(this.label43);
         this.tabPage7.Controls.Add(this.txtBattWatts);
         this.tabPage7.Controls.Add(this.txtCell3);
         this.tabPage7.Controls.Add(this.txtCell2);
         this.tabPage7.Controls.Add(this.txtCell1);
         this.tabPage7.Controls.Add(this.label40);
         this.tabPage7.Controls.Add(this.txtBatCurrent);
         this.tabPage7.Controls.Add(this.label4);
         this.tabPage7.Controls.Add(this.label3);
         this.tabPage7.Controls.Add(this.label2);
         this.tabPage7.Controls.Add(this.txtBatteryName);
         this.tabPage7.Controls.Add(this.lblPercent);
         this.tabPage7.Controls.Add(this.progressBar1);
         this.tabPage7.Controls.Add(this.txtBatCycle);
         this.tabPage7.Controls.Add(this.txtBatSerial);
         this.tabPage7.Controls.Add(this.txtBatRemain);
         this.tabPage7.Controls.Add(this.txtBatTemp);
         this.tabPage7.Controls.Add(this.txtBatVoltage);
         this.tabPage7.Controls.Add(this.txtBatCapacity);
         this.tabPage7.Controls.Add(this.pictureBox3);
         this.tabPage7.Controls.Add(this.groupBox19);
         this.tabPage7.Controls.Add(this.groupBox15);
         this.tabPage7.Location = new System.Drawing.Point(4, 22);
         this.tabPage7.Name = "tabPage7";
         this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage7.Size = new System.Drawing.Size(852, 456);
         this.tabPage7.TabIndex = 6;
         this.tabPage7.Text = "Main Battery";
         this.tabPage7.UseVisualStyleBackColor = true;
         // 
         // label127
         // 
         this.label127.AutoSize = true;
         this.label127.Location = new System.Drawing.Point(378, 284);
         this.label127.Name = "label127";
         this.label127.Size = new System.Drawing.Size(70, 13);
         this.label127.TabIndex = 176;
         this.label127.Text = "Battery Mode";
         // 
         // label126
         // 
         this.label126.AutoSize = true;
         this.label126.Location = new System.Drawing.Point(366, 258);
         this.label126.Name = "label126";
         this.label126.Size = new System.Drawing.Size(82, 13);
         this.label126.TabIndex = 175;
         this.label126.Text = "Charging Status";
         // 
         // label125
         // 
         this.label125.AutoSize = true;
         this.label125.Location = new System.Drawing.Point(387, 232);
         this.label125.Name = "label125";
         this.label125.Size = new System.Drawing.Size(61, 13);
         this.label125.TabIndex = 174;
         this.label125.Text = "Safety Alert";
         // 
         // label124
         // 
         this.label124.AutoSize = true;
         this.label124.Location = new System.Drawing.Point(404, 206);
         this.label124.Name = "label124";
         this.label124.Size = new System.Drawing.Size(44, 13);
         this.label124.TabIndex = 173;
         this.label124.Text = "PF Alert";
         // 
         // label123
         // 
         this.label123.AutoSize = true;
         this.label123.Location = new System.Drawing.Point(395, 180);
         this.label123.Name = "label123";
         this.label123.Size = new System.Drawing.Size(53, 13);
         this.label123.TabIndex = 172;
         this.label123.Text = "PF Status";
         // 
         // label122
         // 
         this.label122.AutoSize = true;
         this.label122.Location = new System.Drawing.Point(378, 154);
         this.label122.Name = "label122";
         this.label122.Size = new System.Drawing.Size(70, 13);
         this.label122.TabIndex = 171;
         this.label122.Text = "Safety Status";
         // 
         // label121
         // 
         this.label121.AutoSize = true;
         this.label121.Location = new System.Drawing.Point(375, 128);
         this.label121.Name = "label121";
         this.label121.Size = new System.Drawing.Size(73, 13);
         this.label121.TabIndex = 170;
         this.label121.Text = "Battery Status";
         // 
         // label120
         // 
         this.label120.AutoSize = true;
         this.label120.Location = new System.Drawing.Point(362, 102);
         this.label120.Name = "label120";
         this.label120.Size = new System.Drawing.Size(86, 13);
         this.label120.TabIndex = 169;
         this.label120.Text = "Operation Status";
         // 
         // label119
         // 
         this.label119.AutoSize = true;
         this.label119.Location = new System.Drawing.Point(388, 76);
         this.label119.Name = "label119";
         this.label119.Size = new System.Drawing.Size(60, 13);
         this.label119.TabIndex = 168;
         this.label119.Text = "FET Status";
         // 
         // btnPollBattery
         // 
         this.btnPollBattery.Location = new System.Drawing.Point(298, 6);
         this.btnPollBattery.Name = "btnPollBattery";
         this.btnPollBattery.Size = new System.Drawing.Size(114, 46);
         this.btnPollBattery.TabIndex = 167;
         this.btnPollBattery.Text = "Poll Battery";
         this.btnPollBattery.UseVisualStyleBackColor = true;
         this.btnPollBattery.Click += new System.EventHandler(this.btnPollBattery_Click);
         // 
         // label118
         // 
         this.label118.AutoSize = true;
         this.label118.Location = new System.Drawing.Point(63, 391);
         this.label118.Name = "label118";
         this.label118.Size = new System.Drawing.Size(56, 13);
         this.label118.TabIndex = 166;
         this.label118.Text = "Thermistor";
         // 
         // label117
         // 
         this.label117.AutoSize = true;
         this.label117.Location = new System.Drawing.Point(47, 362);
         this.label117.Name = "label117";
         this.label117.Size = new System.Drawing.Size(72, 13);
         this.label117.TabIndex = 165;
         this.label117.Text = "Cell 3 Voltage";
         // 
         // label116
         // 
         this.label116.AutoSize = true;
         this.label116.Location = new System.Drawing.Point(47, 336);
         this.label116.Name = "label116";
         this.label116.Size = new System.Drawing.Size(72, 13);
         this.label116.TabIndex = 164;
         this.label116.Text = "Cell 2 Voltage";
         // 
         // label115
         // 
         this.label115.AutoSize = true;
         this.label115.Location = new System.Drawing.Point(47, 310);
         this.label115.Name = "label115";
         this.label115.Size = new System.Drawing.Size(72, 13);
         this.label115.TabIndex = 163;
         this.label115.Text = "Cell 1 Voltage";
         // 
         // label114
         // 
         this.label114.AutoSize = true;
         this.label114.Location = new System.Drawing.Point(48, 284);
         this.label114.Name = "label114";
         this.label114.Size = new System.Drawing.Size(70, 13);
         this.label114.TabIndex = 162;
         this.label114.Text = "Charge Level";
         // 
         // label113
         // 
         this.label113.AutoSize = true;
         this.label113.Location = new System.Drawing.Point(55, 257);
         this.label113.Name = "label113";
         this.label113.Size = new System.Drawing.Size(64, 13);
         this.label113.TabIndex = 161;
         this.label113.Text = "Cycle Count";
         // 
         // label112
         // 
         this.label112.AutoSize = true;
         this.label112.Location = new System.Drawing.Point(46, 231);
         this.label112.Name = "label112";
         this.label112.Size = new System.Drawing.Size(73, 13);
         this.label112.TabIndex = 160;
         this.label112.Text = "Serial Number";
         // 
         // label111
         // 
         this.label111.AutoSize = true;
         this.label111.Location = new System.Drawing.Point(36, 205);
         this.label111.Name = "label111";
         this.label111.Size = new System.Drawing.Size(83, 13);
         this.label111.TabIndex = 159;
         this.label111.Text = "Remaining Time";
         // 
         // label110
         // 
         this.label110.AutoSize = true;
         this.label110.Location = new System.Drawing.Point(52, 179);
         this.label110.Name = "label110";
         this.label110.Size = new System.Drawing.Size(67, 13);
         this.label110.TabIndex = 158;
         this.label110.Text = "Temperature";
         // 
         // label109
         // 
         this.label109.AutoSize = true;
         this.label109.Location = new System.Drawing.Point(78, 127);
         this.label109.Name = "label109";
         this.label109.Size = new System.Drawing.Size(41, 13);
         this.label109.TabIndex = 157;
         this.label109.Text = "Current";
         // 
         // label108
         // 
         this.label108.AutoSize = true;
         this.label108.Location = new System.Drawing.Point(26, 101);
         this.label108.Name = "label108";
         this.label108.Size = new System.Drawing.Size(93, 13);
         this.label108.TabIndex = 156;
         this.label108.Text = "Measured Voltage";
         // 
         // label107
         // 
         this.label107.AutoSize = true;
         this.label107.Location = new System.Drawing.Point(75, 75);
         this.label107.Name = "label107";
         this.label107.Size = new System.Drawing.Size(43, 13);
         this.label107.TabIndex = 155;
         this.label107.Text = "Voltage";
         // 
         // label106
         // 
         this.label106.AutoSize = true;
         this.label106.Location = new System.Drawing.Point(92, 49);
         this.label106.Name = "label106";
         this.label106.Size = new System.Drawing.Size(27, 13);
         this.label106.TabIndex = 154;
         this.label106.Text = "FCC";
         // 
         // label105
         // 
         this.label105.AutoSize = true;
         this.label105.Location = new System.Drawing.Point(18, 23);
         this.label105.Name = "label105";
         this.label105.Size = new System.Drawing.Size(101, 13);
         this.label105.TabIndex = 153;
         this.label105.Text = "Manufacturer Name";
         // 
         // txtMainBatteryOut
         // 
         this.txtMainBatteryOut.Location = new System.Drawing.Point(678, 425);
         this.txtMainBatteryOut.Name = "txtMainBatteryOut";
         this.txtMainBatteryOut.Size = new System.Drawing.Size(39, 20);
         this.txtMainBatteryOut.TabIndex = 152;
         // 
         // btnMainBatteryOut
         // 
         this.btnMainBatteryOut.Location = new System.Drawing.Point(572, 423);
         this.btnMainBatteryOut.Name = "btnMainBatteryOut";
         this.btnMainBatteryOut.Size = new System.Drawing.Size(102, 23);
         this.btnMainBatteryOut.TabIndex = 151;
         this.btnMainBatteryOut.Text = "Main Battery In";
         this.btnMainBatteryOut.UseVisualStyleBackColor = true;
         this.btnMainBatteryOut.Click += new System.EventHandler(this.btnMainBatteryOut_Click);
         // 
         // label48
         // 
         this.label48.AutoSize = true;
         this.label48.Location = new System.Drawing.Point(173, 391);
         this.label48.Name = "label48";
         this.label48.Size = new System.Drawing.Size(17, 13);
         this.label48.TabIndex = 150;
         this.label48.Text = "°F";
         // 
         // txtThermTemp
         // 
         this.txtThermTemp.Location = new System.Drawing.Point(121, 387);
         this.txtThermTemp.Name = "txtThermTemp";
         this.txtThermTemp.Size = new System.Drawing.Size(50, 20);
         this.txtThermTemp.TabIndex = 149;
         // 
         // label47
         // 
         this.label47.AutoSize = true;
         this.label47.Location = new System.Drawing.Point(173, 417);
         this.label47.Name = "label47";
         this.label47.Size = new System.Drawing.Size(15, 13);
         this.label47.TabIndex = 147;
         this.label47.Text = "%";
         // 
         // txtEfficiency
         // 
         this.txtEfficiency.Location = new System.Drawing.Point(121, 413);
         this.txtEfficiency.Name = "txtEfficiency";
         this.txtEfficiency.Size = new System.Drawing.Size(50, 20);
         this.txtEfficiency.TabIndex = 146;
         // 
         // label46
         // 
         this.label46.AutoSize = true;
         this.label46.Location = new System.Drawing.Point(66, 417);
         this.label46.Name = "label46";
         this.label46.Size = new System.Drawing.Size(53, 13);
         this.label46.TabIndex = 145;
         this.label46.Text = "Efficiency";
         // 
         // groupBox35
         // 
         this.groupBox35.Controls.Add(this.btnHexConvert);
         this.groupBox35.Controls.Add(this.txtHexValue);
         this.groupBox35.Controls.Add(this.txtDecimalValue);
         this.groupBox35.Controls.Add(this.label103);
         this.groupBox35.Controls.Add(this.label102);
         this.groupBox35.Location = new System.Drawing.Point(396, 315);
         this.groupBox35.Name = "groupBox35";
         this.groupBox35.Size = new System.Drawing.Size(156, 74);
         this.groupBox35.TabIndex = 144;
         this.groupBox35.TabStop = false;
         this.groupBox35.Text = "Hex Converter";
         // 
         // btnHexConvert
         // 
         this.btnHexConvert.Location = new System.Drawing.Point(116, 30);
         this.btnHexConvert.Name = "btnHexConvert";
         this.btnHexConvert.Size = new System.Drawing.Size(30, 23);
         this.btnHexConvert.TabIndex = 4;
         this.btnHexConvert.Text = "C";
         this.toolTip1.SetToolTip(this.btnHexConvert, "Convert");
         this.btnHexConvert.UseVisualStyleBackColor = true;
         // 
         // txtHexValue
         // 
         this.txtHexValue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtHexValue.Location = new System.Drawing.Point(53, 46);
         this.txtHexValue.Name = "txtHexValue";
         this.txtHexValue.Size = new System.Drawing.Size(58, 20);
         this.txtHexValue.TabIndex = 3;
         // 
         // txtDecimalValue
         // 
         this.txtDecimalValue.Location = new System.Drawing.Point(54, 19);
         this.txtDecimalValue.Name = "txtDecimalValue";
         this.txtDecimalValue.Size = new System.Drawing.Size(57, 20);
         this.txtDecimalValue.TabIndex = 2;
         // 
         // label103
         // 
         this.label103.AutoSize = true;
         this.label103.Location = new System.Drawing.Point(25, 49);
         this.label103.Name = "label103";
         this.label103.Size = new System.Drawing.Size(26, 13);
         this.label103.TabIndex = 1;
         this.label103.Text = "Hex";
         // 
         // label102
         // 
         this.label102.AutoSize = true;
         this.label102.Location = new System.Drawing.Point(7, 23);
         this.label102.Name = "label102";
         this.label102.Size = new System.Drawing.Size(45, 13);
         this.label102.TabIndex = 0;
         this.label102.Text = "Decimal";
         // 
         // groupBox2
         // 
         this.groupBox2.Controls.Add(this.btnOpenDischargeFET);
         this.groupBox2.Controls.Add(this.btnOpenFETs);
         this.groupBox2.Controls.Add(this.btnUnlockPack);
         this.groupBox2.Controls.Add(this.btnLockPack);
         this.groupBox2.Controls.Add(this.btnCloseFETs);
         this.groupBox2.Controls.Add(this.btnResetPack);
         this.groupBox2.Controls.Add(this.btnResetPermFailure);
         this.groupBox2.Location = new System.Drawing.Point(570, 199);
         this.groupBox2.Name = "groupBox2";
         this.groupBox2.Size = new System.Drawing.Size(134, 219);
         this.groupBox2.TabIndex = 143;
         this.groupBox2.TabStop = false;
         this.groupBox2.Text = "Battery Pack Control";
         // 
         // btnOpenDischargeFET
         // 
         this.btnOpenDischargeFET.Enabled = false;
         this.btnOpenDischargeFET.Location = new System.Drawing.Point(11, 71);
         this.btnOpenDischargeFET.Name = "btnOpenDischargeFET";
         this.btnOpenDischargeFET.Size = new System.Drawing.Size(116, 23);
         this.btnOpenDischargeFET.TabIndex = 77;
         this.btnOpenDischargeFET.Text = "Open Discharge FET";
         this.btnOpenDischargeFET.UseVisualStyleBackColor = true;
         this.btnOpenDischargeFET.Click += new System.EventHandler(this.btnOpenDischargeFET_Click);
         // 
         // btnOpenFETs
         // 
         this.btnOpenFETs.Enabled = false;
         this.btnOpenFETs.Location = new System.Drawing.Point(11, 97);
         this.btnOpenFETs.Name = "btnOpenFETs";
         this.btnOpenFETs.Size = new System.Drawing.Size(87, 23);
         this.btnOpenFETs.TabIndex = 76;
         this.btnOpenFETs.Text = "Open FETs";
         this.btnOpenFETs.UseVisualStyleBackColor = true;
         this.btnOpenFETs.Click += new System.EventHandler(this.btnOpenFETs_Click);
         // 
         // btnUnlockPack
         // 
         this.btnUnlockPack.Enabled = false;
         this.btnUnlockPack.Location = new System.Drawing.Point(11, 19);
         this.btnUnlockPack.Name = "btnUnlockPack";
         this.btnUnlockPack.Size = new System.Drawing.Size(87, 23);
         this.btnUnlockPack.TabIndex = 0;
         this.btnUnlockPack.Text = "Unlock Pack";
         this.btnUnlockPack.UseVisualStyleBackColor = true;
         this.btnUnlockPack.Click += new System.EventHandler(this.btnUnlockPack_Click);
         // 
         // btnLockPack
         // 
         this.btnLockPack.Enabled = false;
         this.btnLockPack.Location = new System.Drawing.Point(11, 175);
         this.btnLockPack.Name = "btnLockPack";
         this.btnLockPack.Size = new System.Drawing.Size(75, 23);
         this.btnLockPack.TabIndex = 4;
         this.btnLockPack.Text = "Lock Pack";
         this.btnLockPack.UseVisualStyleBackColor = true;
         this.btnLockPack.Click += new System.EventHandler(this.btnLockPack_Click);
         // 
         // btnCloseFETs
         // 
         this.btnCloseFETs.Enabled = false;
         this.btnCloseFETs.Location = new System.Drawing.Point(11, 45);
         this.btnCloseFETs.Name = "btnCloseFETs";
         this.btnCloseFETs.Size = new System.Drawing.Size(87, 23);
         this.btnCloseFETs.TabIndex = 1;
         this.btnCloseFETs.Text = "Close FETs";
         this.btnCloseFETs.UseVisualStyleBackColor = true;
         this.btnCloseFETs.Click += new System.EventHandler(this.btnCloseFETs_Click);
         // 
         // btnResetPack
         // 
         this.btnResetPack.Enabled = false;
         this.btnResetPack.Location = new System.Drawing.Point(11, 149);
         this.btnResetPack.Name = "btnResetPack";
         this.btnResetPack.Size = new System.Drawing.Size(75, 23);
         this.btnResetPack.TabIndex = 3;
         this.btnResetPack.Text = "Reset Pack";
         this.btnResetPack.UseVisualStyleBackColor = true;
         this.btnResetPack.Click += new System.EventHandler(this.btnResetPack_Click);
         // 
         // btnResetPermFailure
         // 
         this.btnResetPermFailure.Enabled = false;
         this.btnResetPermFailure.Location = new System.Drawing.Point(11, 123);
         this.btnResetPermFailure.Name = "btnResetPermFailure";
         this.btnResetPermFailure.Size = new System.Drawing.Size(105, 23);
         this.btnResetPermFailure.TabIndex = 2;
         this.btnResetPermFailure.Text = "Reset Perm Failure";
         this.btnResetPermFailure.UseVisualStyleBackColor = true;
         this.btnResetPermFailure.Click += new System.EventHandler(this.btnResetPermFailure_Click);
         // 
         // btnBattModeD
         // 
         this.btnBattModeD.Location = new System.Drawing.Point(530, 279);
         this.btnBattModeD.Name = "btnBattModeD";
         this.btnBattModeD.Size = new System.Drawing.Size(22, 22);
         this.btnBattModeD.TabIndex = 133;
         this.btnBattModeD.Text = "D";
         this.btnBattModeD.UseVisualStyleBackColor = true;
         this.btnBattModeD.Click += new System.EventHandler(this.btnBattModeD_Click);
         // 
         // btnChargeStatD
         // 
         this.btnChargeStatD.Location = new System.Drawing.Point(530, 253);
         this.btnChargeStatD.Name = "btnChargeStatD";
         this.btnChargeStatD.Size = new System.Drawing.Size(22, 22);
         this.btnChargeStatD.TabIndex = 132;
         this.btnChargeStatD.Text = "D";
         this.btnChargeStatD.UseVisualStyleBackColor = true;
         this.btnChargeStatD.Click += new System.EventHandler(this.btnChargeStatD_Click);
         // 
         // btnSafeAlertD
         // 
         this.btnSafeAlertD.Location = new System.Drawing.Point(530, 227);
         this.btnSafeAlertD.Name = "btnSafeAlertD";
         this.btnSafeAlertD.Size = new System.Drawing.Size(22, 22);
         this.btnSafeAlertD.TabIndex = 131;
         this.btnSafeAlertD.Text = "D";
         this.btnSafeAlertD.UseVisualStyleBackColor = true;
         this.btnSafeAlertD.Click += new System.EventHandler(this.btnSafeAlertD_Click);
         // 
         // btnPFAlertD
         // 
         this.btnPFAlertD.Location = new System.Drawing.Point(530, 201);
         this.btnPFAlertD.Name = "btnPFAlertD";
         this.btnPFAlertD.Size = new System.Drawing.Size(22, 22);
         this.btnPFAlertD.TabIndex = 130;
         this.btnPFAlertD.Text = "D";
         this.btnPFAlertD.UseVisualStyleBackColor = true;
         this.btnPFAlertD.Click += new System.EventHandler(this.btnPFAlertD_Click);
         // 
         // btnPFStatD
         // 
         this.btnPFStatD.Location = new System.Drawing.Point(530, 175);
         this.btnPFStatD.Name = "btnPFStatD";
         this.btnPFStatD.Size = new System.Drawing.Size(22, 22);
         this.btnPFStatD.TabIndex = 129;
         this.btnPFStatD.Text = "D";
         this.btnPFStatD.UseVisualStyleBackColor = true;
         this.btnPFStatD.Click += new System.EventHandler(this.btnPFStatD_Click);
         // 
         // btnSafeStatD
         // 
         this.btnSafeStatD.Location = new System.Drawing.Point(530, 149);
         this.btnSafeStatD.Name = "btnSafeStatD";
         this.btnSafeStatD.Size = new System.Drawing.Size(22, 22);
         this.btnSafeStatD.TabIndex = 128;
         this.btnSafeStatD.Text = "D";
         this.btnSafeStatD.UseVisualStyleBackColor = true;
         this.btnSafeStatD.Click += new System.EventHandler(this.btnSafeStatD_Click);
         // 
         // btnBattStatD
         // 
         this.btnBattStatD.Location = new System.Drawing.Point(530, 123);
         this.btnBattStatD.Name = "btnBattStatD";
         this.btnBattStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBattStatD.TabIndex = 127;
         this.btnBattStatD.Text = "D";
         this.btnBattStatD.UseVisualStyleBackColor = true;
         this.btnBattStatD.Click += new System.EventHandler(this.btnBattStatD_Click);
         // 
         // btnOpStatD
         // 
         this.btnOpStatD.Location = new System.Drawing.Point(530, 97);
         this.btnOpStatD.Name = "btnOpStatD";
         this.btnOpStatD.Size = new System.Drawing.Size(22, 22);
         this.btnOpStatD.TabIndex = 126;
         this.btnOpStatD.Text = "D";
         this.btnOpStatD.UseVisualStyleBackColor = true;
         this.btnOpStatD.Click += new System.EventHandler(this.btnOpStatD_Click);
         // 
         // txtBatteryMode
         // 
         this.txtBatteryMode.Location = new System.Drawing.Point(450, 280);
         this.txtBatteryMode.Name = "txtBatteryMode";
         this.txtBatteryMode.Size = new System.Drawing.Size(80, 20);
         this.txtBatteryMode.TabIndex = 142;
         // 
         // txtChargingStatus
         // 
         this.txtChargingStatus.Location = new System.Drawing.Point(450, 254);
         this.txtChargingStatus.Name = "txtChargingStatus";
         this.txtChargingStatus.Size = new System.Drawing.Size(80, 20);
         this.txtChargingStatus.TabIndex = 141;
         // 
         // txtSafetyAlert
         // 
         this.txtSafetyAlert.Location = new System.Drawing.Point(450, 228);
         this.txtSafetyAlert.Name = "txtSafetyAlert";
         this.txtSafetyAlert.Size = new System.Drawing.Size(80, 20);
         this.txtSafetyAlert.TabIndex = 140;
         // 
         // txtPFAlert
         // 
         this.txtPFAlert.Location = new System.Drawing.Point(450, 202);
         this.txtPFAlert.Name = "txtPFAlert";
         this.txtPFAlert.Size = new System.Drawing.Size(80, 20);
         this.txtPFAlert.TabIndex = 139;
         // 
         // txtPFStatus
         // 
         this.txtPFStatus.Location = new System.Drawing.Point(450, 176);
         this.txtPFStatus.Name = "txtPFStatus";
         this.txtPFStatus.Size = new System.Drawing.Size(80, 20);
         this.txtPFStatus.TabIndex = 138;
         // 
         // txtSafetyStatus
         // 
         this.txtSafetyStatus.Location = new System.Drawing.Point(450, 150);
         this.txtSafetyStatus.Name = "txtSafetyStatus";
         this.txtSafetyStatus.Size = new System.Drawing.Size(80, 20);
         this.txtSafetyStatus.TabIndex = 137;
         // 
         // txtBatteryStatus
         // 
         this.txtBatteryStatus.Location = new System.Drawing.Point(450, 124);
         this.txtBatteryStatus.Name = "txtBatteryStatus";
         this.txtBatteryStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBatteryStatus.TabIndex = 136;
         // 
         // txtOperationStatus
         // 
         this.txtOperationStatus.Location = new System.Drawing.Point(450, 98);
         this.txtOperationStatus.Name = "txtOperationStatus";
         this.txtOperationStatus.Size = new System.Drawing.Size(80, 20);
         this.txtOperationStatus.TabIndex = 135;
         // 
         // txtFETStatus
         // 
         this.txtFETStatus.Location = new System.Drawing.Point(450, 72);
         this.txtFETStatus.Name = "txtFETStatus";
         this.txtFETStatus.Size = new System.Drawing.Size(118, 20);
         this.txtFETStatus.TabIndex = 134;
         // 
         // groupBox7
         // 
         this.groupBox7.Controls.Add(this.btnBatteryErrors);
         this.groupBox7.Controls.Add(this.rdoError0);
         this.groupBox7.Controls.Add(this.rdoError5);
         this.groupBox7.Controls.Add(this.rdoError4);
         this.groupBox7.Controls.Add(this.rdoError3);
         this.groupBox7.Controls.Add(this.rdoError2);
         this.groupBox7.Controls.Add(this.rdoError1);
         this.groupBox7.Location = new System.Drawing.Point(600, 9);
         this.groupBox7.Name = "groupBox7";
         this.groupBox7.Size = new System.Drawing.Size(110, 183);
         this.groupBox7.TabIndex = 115;
         this.groupBox7.TabStop = false;
         this.groupBox7.Text = "Battery Errors";
         // 
         // btnBatteryErrors
         // 
         this.btnBatteryErrors.Location = new System.Drawing.Point(9, 19);
         this.btnBatteryErrors.Name = "btnBatteryErrors";
         this.btnBatteryErrors.Size = new System.Drawing.Size(92, 23);
         this.btnBatteryErrors.TabIndex = 0;
         this.btnBatteryErrors.Text = "Errors";
         this.btnBatteryErrors.UseVisualStyleBackColor = true;
         this.btnBatteryErrors.Click += new System.EventHandler(this.btnBatteryErrors_Click);
         // 
         // rdoError0
         // 
         this.rdoError0.AutoSize = true;
         this.rdoError0.Checked = true;
         this.rdoError0.Location = new System.Drawing.Point(10, 46);
         this.rdoError0.Name = "rdoError0";
         this.rdoError0.Size = new System.Drawing.Size(64, 17);
         this.rdoError0.TabIndex = 1;
         this.rdoError0.TabStop = true;
         this.rdoError0.Text = "No Error";
         this.rdoError0.UseVisualStyleBackColor = true;
         // 
         // rdoError5
         // 
         this.rdoError5.AutoSize = true;
         this.rdoError5.Location = new System.Drawing.Point(10, 156);
         this.rdoError5.Name = "rdoError5";
         this.rdoError5.Size = new System.Drawing.Size(78, 17);
         this.rdoError5.TabIndex = 6;
         this.rdoError5.Text = "Over Temp";
         this.rdoError5.UseVisualStyleBackColor = true;
         // 
         // rdoError4
         // 
         this.rdoError4.AutoSize = true;
         this.rdoError4.Location = new System.Drawing.Point(10, 134);
         this.rdoError4.Name = "rdoError4";
         this.rdoError4.Size = new System.Drawing.Size(88, 17);
         this.rdoError4.TabIndex = 5;
         this.rdoError4.Text = "D_FET Open";
         this.rdoError4.UseVisualStyleBackColor = true;
         // 
         // rdoError3
         // 
         this.rdoError3.AutoSize = true;
         this.rdoError3.Location = new System.Drawing.Point(10, 112);
         this.rdoError3.Name = "rdoError3";
         this.rdoError3.Size = new System.Drawing.Size(79, 17);
         this.rdoError3.TabIndex = 4;
         this.rdoError3.Text = "Comm Error";
         this.rdoError3.UseVisualStyleBackColor = true;
         // 
         // rdoError2
         // 
         this.rdoError2.AutoSize = true;
         this.rdoError2.Location = new System.Drawing.Point(10, 90);
         this.rdoError2.Name = "rdoError2";
         this.rdoError2.Size = new System.Drawing.Size(94, 17);
         this.rdoError2.TabIndex = 3;
         this.rdoError2.Text = "Cell Imbalance";
         this.rdoError2.UseVisualStyleBackColor = true;
         // 
         // rdoError1
         // 
         this.rdoError1.AutoSize = true;
         this.rdoError1.Location = new System.Drawing.Point(10, 68);
         this.rdoError1.Name = "rdoError1";
         this.rdoError1.Size = new System.Drawing.Size(68, 17);
         this.rdoError1.TabIndex = 2;
         this.rdoError1.Text = "FCC Low";
         this.rdoError1.UseVisualStyleBackColor = true;
         // 
         // label104
         // 
         this.label104.AutoSize = true;
         this.label104.Location = new System.Drawing.Point(223, 101);
         this.label104.Name = "label104";
         this.label104.Size = new System.Drawing.Size(30, 13);
         this.label104.TabIndex = 101;
         this.label104.Text = "Volts";
         // 
         // txtMeasuredVoltage
         // 
         this.txtMeasuredVoltage.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtMeasuredVoltage.Location = new System.Drawing.Point(121, 97);
         this.txtMeasuredVoltage.Name = "txtMeasuredVoltage";
         this.txtMeasuredVoltage.ReadOnly = true;
         this.txtMeasuredVoltage.Size = new System.Drawing.Size(100, 20);
         this.txtMeasuredVoltage.TabIndex = 100;
         this.toolTip1.SetToolTip(this.txtMeasuredVoltage, "Measured Voltage");
         // 
         // label43
         // 
         this.label43.AutoSize = true;
         this.label43.Location = new System.Drawing.Point(223, 153);
         this.label43.Name = "label43";
         this.label43.Size = new System.Drawing.Size(35, 13);
         this.label43.TabIndex = 96;
         this.label43.Text = "Watts";
         // 
         // txtBattWatts
         // 
         this.txtBattWatts.Location = new System.Drawing.Point(121, 149);
         this.txtBattWatts.Name = "txtBattWatts";
         this.txtBattWatts.Size = new System.Drawing.Size(100, 20);
         this.txtBattWatts.TabIndex = 85;
         // 
         // txtCell3
         // 
         this.txtCell3.Location = new System.Drawing.Point(121, 358);
         this.txtCell3.Name = "txtCell3";
         this.txtCell3.Size = new System.Drawing.Size(80, 20);
         this.txtCell3.TabIndex = 93;
         // 
         // txtCell2
         // 
         this.txtCell2.Location = new System.Drawing.Point(121, 332);
         this.txtCell2.Name = "txtCell2";
         this.txtCell2.Size = new System.Drawing.Size(80, 20);
         this.txtCell2.TabIndex = 92;
         // 
         // txtCell1
         // 
         this.txtCell1.Location = new System.Drawing.Point(121, 306);
         this.txtCell1.Name = "txtCell1";
         this.txtCell1.Size = new System.Drawing.Size(80, 20);
         this.txtCell1.TabIndex = 91;
         // 
         // label40
         // 
         this.label40.AutoSize = true;
         this.label40.Location = new System.Drawing.Point(223, 127);
         this.label40.Name = "label40";
         this.label40.Size = new System.Drawing.Size(50, 13);
         this.label40.TabIndex = 95;
         this.label40.Text = "MilliAmps";
         // 
         // txtBatCurrent
         // 
         this.txtBatCurrent.Location = new System.Drawing.Point(121, 123);
         this.txtBatCurrent.Name = "txtBatCurrent";
         this.txtBatCurrent.Size = new System.Drawing.Size(100, 20);
         this.txtBatCurrent.TabIndex = 84;
         // 
         // label4
         // 
         this.label4.AutoSize = true;
         this.label4.Location = new System.Drawing.Point(223, 205);
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size(89, 13);
         this.label4.TabIndex = 98;
         this.label4.Text = "Minutes   HR:MN";
         // 
         // label3
         // 
         this.label3.AutoSize = true;
         this.label3.Location = new System.Drawing.Point(223, 179);
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size(17, 13);
         this.label3.TabIndex = 97;
         this.label3.Text = "°F";
         // 
         // label2
         // 
         this.label2.AutoSize = true;
         this.label2.Location = new System.Drawing.Point(223, 75);
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size(30, 13);
         this.label2.TabIndex = 94;
         this.label2.Text = "Volts";
         // 
         // txtBatteryName
         // 
         this.txtBatteryName.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatteryName.Location = new System.Drawing.Point(121, 19);
         this.txtBatteryName.Name = "txtBatteryName";
         this.txtBatteryName.ReadOnly = true;
         this.txtBatteryName.Size = new System.Drawing.Size(100, 20);
         this.txtBatteryName.TabIndex = 81;
         // 
         // lblPercent
         // 
         this.lblPercent.Location = new System.Drawing.Point(224, 284);
         this.lblPercent.Name = "lblPercent";
         this.lblPercent.Size = new System.Drawing.Size(43, 13);
         this.lblPercent.TabIndex = 99;
         this.lblPercent.Text = "%";
         // 
         // progressBar1
         // 
         this.progressBar1.Location = new System.Drawing.Point(121, 279);
         this.progressBar1.Name = "progressBar1";
         this.progressBar1.Size = new System.Drawing.Size(100, 23);
         this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
         this.progressBar1.TabIndex = 90;
         // 
         // txtBatCycle
         // 
         this.txtBatCycle.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatCycle.Location = new System.Drawing.Point(121, 253);
         this.txtBatCycle.Name = "txtBatCycle";
         this.txtBatCycle.ReadOnly = true;
         this.txtBatCycle.Size = new System.Drawing.Size(100, 20);
         this.txtBatCycle.TabIndex = 89;
         // 
         // txtBatSerial
         // 
         this.txtBatSerial.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatSerial.Location = new System.Drawing.Point(121, 227);
         this.txtBatSerial.Name = "txtBatSerial";
         this.txtBatSerial.ReadOnly = true;
         this.txtBatSerial.Size = new System.Drawing.Size(100, 20);
         this.txtBatSerial.TabIndex = 88;
         // 
         // txtBatRemain
         // 
         this.txtBatRemain.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatRemain.Location = new System.Drawing.Point(121, 201);
         this.txtBatRemain.Name = "txtBatRemain";
         this.txtBatRemain.ReadOnly = true;
         this.txtBatRemain.Size = new System.Drawing.Size(100, 20);
         this.txtBatRemain.TabIndex = 87;
         // 
         // txtBatTemp
         // 
         this.txtBatTemp.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatTemp.Location = new System.Drawing.Point(121, 175);
         this.txtBatTemp.Name = "txtBatTemp";
         this.txtBatTemp.ReadOnly = true;
         this.txtBatTemp.Size = new System.Drawing.Size(100, 20);
         this.txtBatTemp.TabIndex = 86;
         // 
         // txtBatVoltage
         // 
         this.txtBatVoltage.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatVoltage.Location = new System.Drawing.Point(121, 71);
         this.txtBatVoltage.Name = "txtBatVoltage";
         this.txtBatVoltage.ReadOnly = true;
         this.txtBatVoltage.Size = new System.Drawing.Size(100, 20);
         this.txtBatVoltage.TabIndex = 83;
         this.toolTip1.SetToolTip(this.txtBatVoltage, "Reported Voltage");
         // 
         // txtBatCapacity
         // 
         this.txtBatCapacity.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBatCapacity.Location = new System.Drawing.Point(121, 45);
         this.txtBatCapacity.Name = "txtBatCapacity";
         this.txtBatCapacity.ReadOnly = true;
         this.txtBatCapacity.Size = new System.Drawing.Size(100, 20);
         this.txtBatCapacity.TabIndex = 82;
         // 
         // pictureBox3
         // 
         this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
         this.pictureBox3.Location = new System.Drawing.Point(736, 255);
         this.pictureBox3.Name = "pictureBox3";
         this.pictureBox3.Size = new System.Drawing.Size(110, 195);
         this.pictureBox3.TabIndex = 22;
         this.pictureBox3.TabStop = false;
         // 
         // groupBox19
         // 
         this.groupBox19.Controls.Add(this.btnFlashLEDs);
         this.groupBox19.Controls.Add(this.btnIsFlashing);
         this.groupBox19.Controls.Add(this.txtIsFlashing);
         this.groupBox19.Controls.Add(this.btnLEDsOff);
         this.groupBox19.Location = new System.Drawing.Point(718, 120);
         this.groupBox19.Name = "groupBox19";
         this.groupBox19.Size = new System.Drawing.Size(128, 132);
         this.groupBox19.TabIndex = 21;
         this.groupBox19.TabStop = false;
         this.groupBox19.Text = "Battery LED\'s";
         // 
         // btnFlashLEDs
         // 
         this.btnFlashLEDs.Location = new System.Drawing.Point(16, 24);
         this.btnFlashLEDs.Name = "btnFlashLEDs";
         this.btnFlashLEDs.Size = new System.Drawing.Size(75, 23);
         this.btnFlashLEDs.TabIndex = 8;
         this.btnFlashLEDs.Text = "Flash LEDs";
         this.btnFlashLEDs.UseVisualStyleBackColor = true;
         this.btnFlashLEDs.Click += new System.EventHandler(this.btnFlashLEDs_Click);
         // 
         // btnIsFlashing
         // 
         this.btnIsFlashing.Location = new System.Drawing.Point(16, 92);
         this.btnIsFlashing.Name = "btnIsFlashing";
         this.btnIsFlashing.Size = new System.Drawing.Size(75, 23);
         this.btnIsFlashing.TabIndex = 10;
         this.btnIsFlashing.Text = "Flashing";
         this.btnIsFlashing.UseVisualStyleBackColor = true;
         this.btnIsFlashing.Click += new System.EventHandler(this.btnIsFlashing_Click);
         // 
         // txtIsFlashing
         // 
         this.txtIsFlashing.Location = new System.Drawing.Point(93, 94);
         this.txtIsFlashing.Name = "txtIsFlashing";
         this.txtIsFlashing.Size = new System.Drawing.Size(29, 20);
         this.txtIsFlashing.TabIndex = 19;
         // 
         // btnLEDsOff
         // 
         this.btnLEDsOff.Location = new System.Drawing.Point(16, 53);
         this.btnLEDsOff.Name = "btnLEDsOff";
         this.btnLEDsOff.Size = new System.Drawing.Size(75, 23);
         this.btnLEDsOff.TabIndex = 9;
         this.btnLEDsOff.Text = "LEDs Off";
         this.btnLEDsOff.UseVisualStyleBackColor = true;
         this.btnLEDsOff.Click += new System.EventHandler(this.btnLEDsOff_Click);
         // 
         // groupBox15
         // 
         this.groupBox15.Controls.Add(this.btnSetMaxCC);
         this.groupBox15.Controls.Add(this.btnGetMaxCC);
         this.groupBox15.Controls.Add(this.txtMaxCC);
         this.groupBox15.Location = new System.Drawing.Point(718, 8);
         this.groupBox15.Name = "groupBox15";
         this.groupBox15.Size = new System.Drawing.Size(128, 111);
         this.groupBox15.TabIndex = 11;
         this.groupBox15.TabStop = false;
         this.groupBox15.Text = "Maximum Cycle Count";
         // 
         // btnSetMaxCC
         // 
         this.btnSetMaxCC.Enabled = false;
         this.btnSetMaxCC.Location = new System.Drawing.Point(15, 77);
         this.btnSetMaxCC.Name = "btnSetMaxCC";
         this.btnSetMaxCC.Size = new System.Drawing.Size(75, 23);
         this.btnSetMaxCC.TabIndex = 2;
         this.btnSetMaxCC.Text = "Set";
         this.btnSetMaxCC.UseVisualStyleBackColor = true;
         this.btnSetMaxCC.Click += new System.EventHandler(this.btnSetMaxCC_Click);
         // 
         // btnGetMaxCC
         // 
         this.btnGetMaxCC.Location = new System.Drawing.Point(15, 48);
         this.btnGetMaxCC.Name = "btnGetMaxCC";
         this.btnGetMaxCC.Size = new System.Drawing.Size(75, 23);
         this.btnGetMaxCC.TabIndex = 1;
         this.btnGetMaxCC.Text = "Get";
         this.btnGetMaxCC.UseVisualStyleBackColor = true;
         this.btnGetMaxCC.Click += new System.EventHandler(this.btnGetMaxCC_Click);
         // 
         // txtMaxCC
         // 
         this.txtMaxCC.Location = new System.Drawing.Point(15, 22);
         this.txtMaxCC.Name = "txtMaxCC";
         this.txtMaxCC.Size = new System.Drawing.Size(100, 20);
         this.txtMaxCC.TabIndex = 0;
         // 
         // tabPage13
         // 
         this.tabPage13.Controls.Add(this.btnPollBUB);
         this.tabPage13.Controls.Add(this.label146);
         this.tabPage13.Controls.Add(this.label152);
         this.tabPage13.Controls.Add(this.label153);
         this.tabPage13.Controls.Add(this.label154);
         this.tabPage13.Controls.Add(this.label155);
         this.tabPage13.Controls.Add(this.label156);
         this.tabPage13.Controls.Add(this.label157);
         this.tabPage13.Controls.Add(this.label158);
         this.tabPage13.Controls.Add(this.label159);
         this.tabPage13.Controls.Add(this.btnBUBBattModeD);
         this.tabPage13.Controls.Add(this.btnBUBChargeStatD);
         this.tabPage13.Controls.Add(this.btnBUBSafeAlertD);
         this.tabPage13.Controls.Add(this.btnBUBPFAlertD);
         this.tabPage13.Controls.Add(this.btnBUBPFStatD);
         this.tabPage13.Controls.Add(this.btnBUBSafeStatD);
         this.tabPage13.Controls.Add(this.btnBUBBattStatD);
         this.tabPage13.Controls.Add(this.btnBUBOpStatD);
         this.tabPage13.Controls.Add(this.txtBUBBatteryMode);
         this.tabPage13.Controls.Add(this.txtBUBChargingStatus);
         this.tabPage13.Controls.Add(this.txtBUBSafetyAlert);
         this.tabPage13.Controls.Add(this.txtBUBPFAlert);
         this.tabPage13.Controls.Add(this.txtBUBPFStatus);
         this.tabPage13.Controls.Add(this.txtBUBSafetyStatus);
         this.tabPage13.Controls.Add(this.txtBUBBatteryStatus);
         this.tabPage13.Controls.Add(this.txtBUBOperationStatus);
         this.tabPage13.Controls.Add(this.txtBUBFETStatus);
         this.tabPage13.Controls.Add(this.label128);
         this.tabPage13.Controls.Add(this.label129);
         this.tabPage13.Controls.Add(this.label130);
         this.tabPage13.Controls.Add(this.label131);
         this.tabPage13.Controls.Add(this.label132);
         this.tabPage13.Controls.Add(this.label133);
         this.tabPage13.Controls.Add(this.label136);
         this.tabPage13.Controls.Add(this.label140);
         this.tabPage13.Controls.Add(this.label141);
         this.tabPage13.Controls.Add(this.label142);
         this.tabPage13.Controls.Add(this.label143);
         this.tabPage13.Controls.Add(this.label144);
         this.tabPage13.Controls.Add(this.label145);
         this.tabPage13.Controls.Add(this.txtBUBMeasuredVoltage2);
         this.tabPage13.Controls.Add(this.txtBUBCell2Voltage);
         this.tabPage13.Controls.Add(this.txtBUBCell1Voltage);
         this.tabPage13.Controls.Add(this.label147);
         this.tabPage13.Controls.Add(this.txtBUBCurrent);
         this.tabPage13.Controls.Add(this.label148);
         this.tabPage13.Controls.Add(this.label149);
         this.tabPage13.Controls.Add(this.label150);
         this.tabPage13.Controls.Add(this.txtBUBatteryName);
         this.tabPage13.Controls.Add(this.lblBUBPercent);
         this.tabPage13.Controls.Add(this.pBarBUBChargeLevel);
         this.tabPage13.Controls.Add(this.txtBUBCycleCount);
         this.tabPage13.Controls.Add(this.txtBUBSerialNumber);
         this.tabPage13.Controls.Add(this.txtBUBRemainingTime);
         this.tabPage13.Controls.Add(this.txtBUBTemperature);
         this.tabPage13.Controls.Add(this.txtBUBVoltage);
         this.tabPage13.Controls.Add(this.txtBUBFCC);
         this.tabPage13.Location = new System.Drawing.Point(4, 22);
         this.tabPage13.Name = "tabPage13";
         this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage13.Size = new System.Drawing.Size(852, 456);
         this.tabPage13.TabIndex = 12;
         this.tabPage13.Text = "Backup Battery";
         this.tabPage13.UseVisualStyleBackColor = true;
         // 
         // btnPollBUB
         // 
         this.btnPollBUB.Location = new System.Drawing.Point(298, 6);
         this.btnPollBUB.Name = "btnPollBUB";
         this.btnPollBUB.Size = new System.Drawing.Size(114, 46);
         this.btnPollBUB.TabIndex = 223;
         this.btnPollBUB.Text = "Poll Battery";
         this.btnPollBUB.UseVisualStyleBackColor = true;
         this.btnPollBUB.Click += new System.EventHandler(this.btnPollBUB_Click);
         // 
         // label146
         // 
         this.label146.AutoSize = true;
         this.label146.Location = new System.Drawing.Point(378, 284);
         this.label146.Name = "label146";
         this.label146.Size = new System.Drawing.Size(70, 13);
         this.label146.TabIndex = 222;
         this.label146.Text = "Battery Mode";
         // 
         // label152
         // 
         this.label152.AutoSize = true;
         this.label152.Location = new System.Drawing.Point(366, 258);
         this.label152.Name = "label152";
         this.label152.Size = new System.Drawing.Size(82, 13);
         this.label152.TabIndex = 221;
         this.label152.Text = "Charging Status";
         // 
         // label153
         // 
         this.label153.AutoSize = true;
         this.label153.Location = new System.Drawing.Point(387, 232);
         this.label153.Name = "label153";
         this.label153.Size = new System.Drawing.Size(61, 13);
         this.label153.TabIndex = 220;
         this.label153.Text = "Safety Alert";
         // 
         // label154
         // 
         this.label154.AutoSize = true;
         this.label154.Location = new System.Drawing.Point(404, 206);
         this.label154.Name = "label154";
         this.label154.Size = new System.Drawing.Size(44, 13);
         this.label154.TabIndex = 219;
         this.label154.Text = "PF Alert";
         // 
         // label155
         // 
         this.label155.AutoSize = true;
         this.label155.Location = new System.Drawing.Point(395, 180);
         this.label155.Name = "label155";
         this.label155.Size = new System.Drawing.Size(53, 13);
         this.label155.TabIndex = 218;
         this.label155.Text = "PF Status";
         // 
         // label156
         // 
         this.label156.AutoSize = true;
         this.label156.Location = new System.Drawing.Point(378, 154);
         this.label156.Name = "label156";
         this.label156.Size = new System.Drawing.Size(70, 13);
         this.label156.TabIndex = 217;
         this.label156.Text = "Safety Status";
         // 
         // label157
         // 
         this.label157.AutoSize = true;
         this.label157.Location = new System.Drawing.Point(375, 128);
         this.label157.Name = "label157";
         this.label157.Size = new System.Drawing.Size(73, 13);
         this.label157.TabIndex = 216;
         this.label157.Text = "Battery Status";
         // 
         // label158
         // 
         this.label158.AutoSize = true;
         this.label158.Location = new System.Drawing.Point(362, 102);
         this.label158.Name = "label158";
         this.label158.Size = new System.Drawing.Size(86, 13);
         this.label158.TabIndex = 215;
         this.label158.Text = "Operation Status";
         // 
         // label159
         // 
         this.label159.AutoSize = true;
         this.label159.Location = new System.Drawing.Point(388, 76);
         this.label159.Name = "label159";
         this.label159.Size = new System.Drawing.Size(60, 13);
         this.label159.TabIndex = 214;
         this.label159.Text = "FET Status";
         // 
         // btnBUBBattModeD
         // 
         this.btnBUBBattModeD.Location = new System.Drawing.Point(530, 279);
         this.btnBUBBattModeD.Name = "btnBUBBattModeD";
         this.btnBUBBattModeD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBBattModeD.TabIndex = 204;
         this.btnBUBBattModeD.Text = "D";
         this.btnBUBBattModeD.UseVisualStyleBackColor = true;
         this.btnBUBBattModeD.Click += new System.EventHandler(this.btnBUBBattModeD_Click);
         // 
         // btnBUBChargeStatD
         // 
         this.btnBUBChargeStatD.Location = new System.Drawing.Point(530, 253);
         this.btnBUBChargeStatD.Name = "btnBUBChargeStatD";
         this.btnBUBChargeStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBChargeStatD.TabIndex = 203;
         this.btnBUBChargeStatD.Text = "D";
         this.btnBUBChargeStatD.UseVisualStyleBackColor = true;
         this.btnBUBChargeStatD.Click += new System.EventHandler(this.btnBUBChargeStatD_Click);
         // 
         // btnBUBSafeAlertD
         // 
         this.btnBUBSafeAlertD.Location = new System.Drawing.Point(530, 227);
         this.btnBUBSafeAlertD.Name = "btnBUBSafeAlertD";
         this.btnBUBSafeAlertD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBSafeAlertD.TabIndex = 202;
         this.btnBUBSafeAlertD.Text = "D";
         this.btnBUBSafeAlertD.UseVisualStyleBackColor = true;
         this.btnBUBSafeAlertD.Click += new System.EventHandler(this.btnBUBSafeAlertD_Click);
         // 
         // btnBUBPFAlertD
         // 
         this.btnBUBPFAlertD.Location = new System.Drawing.Point(530, 201);
         this.btnBUBPFAlertD.Name = "btnBUBPFAlertD";
         this.btnBUBPFAlertD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBPFAlertD.TabIndex = 201;
         this.btnBUBPFAlertD.Text = "D";
         this.btnBUBPFAlertD.UseVisualStyleBackColor = true;
         this.btnBUBPFAlertD.Click += new System.EventHandler(this.btnBUBPFAlertD_Click);
         // 
         // btnBUBPFStatD
         // 
         this.btnBUBPFStatD.Location = new System.Drawing.Point(530, 175);
         this.btnBUBPFStatD.Name = "btnBUBPFStatD";
         this.btnBUBPFStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBPFStatD.TabIndex = 200;
         this.btnBUBPFStatD.Text = "D";
         this.btnBUBPFStatD.UseVisualStyleBackColor = true;
         this.btnBUBPFStatD.Click += new System.EventHandler(this.btnBUBPFStatD_Click);
         // 
         // btnBUBSafeStatD
         // 
         this.btnBUBSafeStatD.Location = new System.Drawing.Point(530, 149);
         this.btnBUBSafeStatD.Name = "btnBUBSafeStatD";
         this.btnBUBSafeStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBSafeStatD.TabIndex = 199;
         this.btnBUBSafeStatD.Text = "D";
         this.btnBUBSafeStatD.UseVisualStyleBackColor = true;
         this.btnBUBSafeStatD.Click += new System.EventHandler(this.btnBUBSafeStatD_Click);
         // 
         // btnBUBBattStatD
         // 
         this.btnBUBBattStatD.Location = new System.Drawing.Point(530, 123);
         this.btnBUBBattStatD.Name = "btnBUBBattStatD";
         this.btnBUBBattStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBBattStatD.TabIndex = 198;
         this.btnBUBBattStatD.Text = "D";
         this.btnBUBBattStatD.UseVisualStyleBackColor = true;
         this.btnBUBBattStatD.Click += new System.EventHandler(this.btnBUBBattStatD_Click);
         // 
         // btnBUBOpStatD
         // 
         this.btnBUBOpStatD.Location = new System.Drawing.Point(530, 97);
         this.btnBUBOpStatD.Name = "btnBUBOpStatD";
         this.btnBUBOpStatD.Size = new System.Drawing.Size(22, 22);
         this.btnBUBOpStatD.TabIndex = 197;
         this.btnBUBOpStatD.Text = "D";
         this.btnBUBOpStatD.UseVisualStyleBackColor = true;
         this.btnBUBOpStatD.Click += new System.EventHandler(this.btnBUBOpStatD_Click);
         // 
         // txtBUBBatteryMode
         // 
         this.txtBUBBatteryMode.Location = new System.Drawing.Point(450, 280);
         this.txtBUBBatteryMode.Name = "txtBUBBatteryMode";
         this.txtBUBBatteryMode.Size = new System.Drawing.Size(80, 20);
         this.txtBUBBatteryMode.TabIndex = 213;
         // 
         // txtBUBChargingStatus
         // 
         this.txtBUBChargingStatus.Location = new System.Drawing.Point(450, 254);
         this.txtBUBChargingStatus.Name = "txtBUBChargingStatus";
         this.txtBUBChargingStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBUBChargingStatus.TabIndex = 212;
         // 
         // txtBUBSafetyAlert
         // 
         this.txtBUBSafetyAlert.Location = new System.Drawing.Point(450, 228);
         this.txtBUBSafetyAlert.Name = "txtBUBSafetyAlert";
         this.txtBUBSafetyAlert.Size = new System.Drawing.Size(80, 20);
         this.txtBUBSafetyAlert.TabIndex = 211;
         // 
         // txtBUBPFAlert
         // 
         this.txtBUBPFAlert.Location = new System.Drawing.Point(450, 202);
         this.txtBUBPFAlert.Name = "txtBUBPFAlert";
         this.txtBUBPFAlert.Size = new System.Drawing.Size(80, 20);
         this.txtBUBPFAlert.TabIndex = 210;
         // 
         // txtBUBPFStatus
         // 
         this.txtBUBPFStatus.Location = new System.Drawing.Point(450, 176);
         this.txtBUBPFStatus.Name = "txtBUBPFStatus";
         this.txtBUBPFStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBUBPFStatus.TabIndex = 209;
         // 
         // txtBUBSafetyStatus
         // 
         this.txtBUBSafetyStatus.Location = new System.Drawing.Point(450, 150);
         this.txtBUBSafetyStatus.Name = "txtBUBSafetyStatus";
         this.txtBUBSafetyStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBUBSafetyStatus.TabIndex = 208;
         // 
         // txtBUBBatteryStatus
         // 
         this.txtBUBBatteryStatus.Location = new System.Drawing.Point(450, 124);
         this.txtBUBBatteryStatus.Name = "txtBUBBatteryStatus";
         this.txtBUBBatteryStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBUBBatteryStatus.TabIndex = 207;
         // 
         // txtBUBOperationStatus
         // 
         this.txtBUBOperationStatus.Location = new System.Drawing.Point(450, 98);
         this.txtBUBOperationStatus.Name = "txtBUBOperationStatus";
         this.txtBUBOperationStatus.Size = new System.Drawing.Size(80, 20);
         this.txtBUBOperationStatus.TabIndex = 206;
         // 
         // txtBUBFETStatus
         // 
         this.txtBUBFETStatus.Location = new System.Drawing.Point(450, 72);
         this.txtBUBFETStatus.Name = "txtBUBFETStatus";
         this.txtBUBFETStatus.Size = new System.Drawing.Size(118, 20);
         this.txtBUBFETStatus.TabIndex = 205;
         // 
         // label128
         // 
         this.label128.AutoSize = true;
         this.label128.Location = new System.Drawing.Point(47, 336);
         this.label128.Name = "label128";
         this.label128.Size = new System.Drawing.Size(72, 13);
         this.label128.TabIndex = 196;
         this.label128.Text = "Cell 2 Voltage";
         // 
         // label129
         // 
         this.label129.AutoSize = true;
         this.label129.Location = new System.Drawing.Point(47, 310);
         this.label129.Name = "label129";
         this.label129.Size = new System.Drawing.Size(72, 13);
         this.label129.TabIndex = 195;
         this.label129.Text = "Cell 1 Voltage";
         // 
         // label130
         // 
         this.label130.AutoSize = true;
         this.label130.Location = new System.Drawing.Point(48, 284);
         this.label130.Name = "label130";
         this.label130.Size = new System.Drawing.Size(70, 13);
         this.label130.TabIndex = 194;
         this.label130.Text = "Charge Level";
         // 
         // label131
         // 
         this.label131.AutoSize = true;
         this.label131.Location = new System.Drawing.Point(55, 257);
         this.label131.Name = "label131";
         this.label131.Size = new System.Drawing.Size(64, 13);
         this.label131.TabIndex = 193;
         this.label131.Text = "Cycle Count";
         // 
         // label132
         // 
         this.label132.AutoSize = true;
         this.label132.Location = new System.Drawing.Point(46, 231);
         this.label132.Name = "label132";
         this.label132.Size = new System.Drawing.Size(73, 13);
         this.label132.TabIndex = 192;
         this.label132.Text = "Serial Number";
         // 
         // label133
         // 
         this.label133.AutoSize = true;
         this.label133.Location = new System.Drawing.Point(36, 205);
         this.label133.Name = "label133";
         this.label133.Size = new System.Drawing.Size(83, 13);
         this.label133.TabIndex = 191;
         this.label133.Text = "Remaining Time";
         // 
         // label136
         // 
         this.label136.AutoSize = true;
         this.label136.Location = new System.Drawing.Point(52, 179);
         this.label136.Name = "label136";
         this.label136.Size = new System.Drawing.Size(67, 13);
         this.label136.TabIndex = 190;
         this.label136.Text = "Temperature";
         // 
         // label140
         // 
         this.label140.AutoSize = true;
         this.label140.Location = new System.Drawing.Point(78, 127);
         this.label140.Name = "label140";
         this.label140.Size = new System.Drawing.Size(41, 13);
         this.label140.TabIndex = 189;
         this.label140.Text = "Current";
         // 
         // label141
         // 
         this.label141.AutoSize = true;
         this.label141.Location = new System.Drawing.Point(26, 101);
         this.label141.Name = "label141";
         this.label141.Size = new System.Drawing.Size(93, 13);
         this.label141.TabIndex = 188;
         this.label141.Text = "Measured Voltage";
         // 
         // label142
         // 
         this.label142.AutoSize = true;
         this.label142.Location = new System.Drawing.Point(75, 75);
         this.label142.Name = "label142";
         this.label142.Size = new System.Drawing.Size(43, 13);
         this.label142.TabIndex = 187;
         this.label142.Text = "Voltage";
         // 
         // label143
         // 
         this.label143.AutoSize = true;
         this.label143.Location = new System.Drawing.Point(92, 49);
         this.label143.Name = "label143";
         this.label143.Size = new System.Drawing.Size(27, 13);
         this.label143.TabIndex = 186;
         this.label143.Text = "FCC";
         // 
         // label144
         // 
         this.label144.AutoSize = true;
         this.label144.Location = new System.Drawing.Point(18, 23);
         this.label144.Name = "label144";
         this.label144.Size = new System.Drawing.Size(101, 13);
         this.label144.TabIndex = 185;
         this.label144.Text = "Manufacturer Name";
         // 
         // label145
         // 
         this.label145.AutoSize = true;
         this.label145.Location = new System.Drawing.Point(223, 101);
         this.label145.Name = "label145";
         this.label145.Size = new System.Drawing.Size(30, 13);
         this.label145.TabIndex = 184;
         this.label145.Text = "Volts";
         // 
         // txtBUBMeasuredVoltage2
         // 
         this.txtBUBMeasuredVoltage2.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBMeasuredVoltage2.Location = new System.Drawing.Point(121, 97);
         this.txtBUBMeasuredVoltage2.Name = "txtBUBMeasuredVoltage2";
         this.txtBUBMeasuredVoltage2.ReadOnly = true;
         this.txtBUBMeasuredVoltage2.Size = new System.Drawing.Size(100, 20);
         this.txtBUBMeasuredVoltage2.TabIndex = 183;
         this.toolTip1.SetToolTip(this.txtBUBMeasuredVoltage2, "Measured Voltage");
         // 
         // txtBUBCell2Voltage
         // 
         this.txtBUBCell2Voltage.Location = new System.Drawing.Point(121, 332);
         this.txtBUBCell2Voltage.Name = "txtBUBCell2Voltage";
         this.txtBUBCell2Voltage.Size = new System.Drawing.Size(80, 20);
         this.txtBUBCell2Voltage.TabIndex = 176;
         // 
         // txtBUBCell1Voltage
         // 
         this.txtBUBCell1Voltage.Location = new System.Drawing.Point(121, 306);
         this.txtBUBCell1Voltage.Name = "txtBUBCell1Voltage";
         this.txtBUBCell1Voltage.Size = new System.Drawing.Size(80, 20);
         this.txtBUBCell1Voltage.TabIndex = 175;
         // 
         // label147
         // 
         this.label147.AutoSize = true;
         this.label147.Location = new System.Drawing.Point(223, 127);
         this.label147.Name = "label147";
         this.label147.Size = new System.Drawing.Size(50, 13);
         this.label147.TabIndex = 178;
         this.label147.Text = "MilliAmps";
         // 
         // txtBUBCurrent
         // 
         this.txtBUBCurrent.Location = new System.Drawing.Point(121, 123);
         this.txtBUBCurrent.Name = "txtBUBCurrent";
         this.txtBUBCurrent.Size = new System.Drawing.Size(100, 20);
         this.txtBUBCurrent.TabIndex = 168;
         // 
         // label148
         // 
         this.label148.AutoSize = true;
         this.label148.Location = new System.Drawing.Point(223, 205);
         this.label148.Name = "label148";
         this.label148.Size = new System.Drawing.Size(89, 13);
         this.label148.TabIndex = 181;
         this.label148.Text = "Minutes   HR:MN";
         // 
         // label149
         // 
         this.label149.AutoSize = true;
         this.label149.Location = new System.Drawing.Point(223, 179);
         this.label149.Name = "label149";
         this.label149.Size = new System.Drawing.Size(17, 13);
         this.label149.TabIndex = 180;
         this.label149.Text = "°F";
         // 
         // label150
         // 
         this.label150.AutoSize = true;
         this.label150.Location = new System.Drawing.Point(223, 75);
         this.label150.Name = "label150";
         this.label150.Size = new System.Drawing.Size(30, 13);
         this.label150.TabIndex = 177;
         this.label150.Text = "Volts";
         // 
         // txtBUBatteryName
         // 
         this.txtBUBatteryName.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBatteryName.Location = new System.Drawing.Point(121, 19);
         this.txtBUBatteryName.Name = "txtBUBatteryName";
         this.txtBUBatteryName.ReadOnly = true;
         this.txtBUBatteryName.Size = new System.Drawing.Size(100, 20);
         this.txtBUBatteryName.TabIndex = 165;
         // 
         // lblBUBPercent
         // 
         this.lblBUBPercent.Location = new System.Drawing.Point(224, 284);
         this.lblBUBPercent.Name = "lblBUBPercent";
         this.lblBUBPercent.Size = new System.Drawing.Size(43, 13);
         this.lblBUBPercent.TabIndex = 182;
         this.lblBUBPercent.Text = "%";
         // 
         // pBarBUBChargeLevel
         // 
         this.pBarBUBChargeLevel.Location = new System.Drawing.Point(121, 279);
         this.pBarBUBChargeLevel.Name = "pBarBUBChargeLevel";
         this.pBarBUBChargeLevel.Size = new System.Drawing.Size(100, 23);
         this.pBarBUBChargeLevel.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
         this.pBarBUBChargeLevel.TabIndex = 174;
         // 
         // txtBUBCycleCount
         // 
         this.txtBUBCycleCount.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBCycleCount.Location = new System.Drawing.Point(121, 253);
         this.txtBUBCycleCount.Name = "txtBUBCycleCount";
         this.txtBUBCycleCount.ReadOnly = true;
         this.txtBUBCycleCount.Size = new System.Drawing.Size(100, 20);
         this.txtBUBCycleCount.TabIndex = 173;
         // 
         // txtBUBSerialNumber
         // 
         this.txtBUBSerialNumber.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBSerialNumber.Location = new System.Drawing.Point(121, 227);
         this.txtBUBSerialNumber.Name = "txtBUBSerialNumber";
         this.txtBUBSerialNumber.ReadOnly = true;
         this.txtBUBSerialNumber.Size = new System.Drawing.Size(100, 20);
         this.txtBUBSerialNumber.TabIndex = 172;
         // 
         // txtBUBRemainingTime
         // 
         this.txtBUBRemainingTime.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBRemainingTime.Location = new System.Drawing.Point(121, 201);
         this.txtBUBRemainingTime.Name = "txtBUBRemainingTime";
         this.txtBUBRemainingTime.ReadOnly = true;
         this.txtBUBRemainingTime.Size = new System.Drawing.Size(100, 20);
         this.txtBUBRemainingTime.TabIndex = 171;
         // 
         // txtBUBTemperature
         // 
         this.txtBUBTemperature.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBTemperature.Location = new System.Drawing.Point(121, 175);
         this.txtBUBTemperature.Name = "txtBUBTemperature";
         this.txtBUBTemperature.ReadOnly = true;
         this.txtBUBTemperature.Size = new System.Drawing.Size(100, 20);
         this.txtBUBTemperature.TabIndex = 170;
         // 
         // txtBUBVoltage
         // 
         this.txtBUBVoltage.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBVoltage.Location = new System.Drawing.Point(121, 71);
         this.txtBUBVoltage.Name = "txtBUBVoltage";
         this.txtBUBVoltage.ReadOnly = true;
         this.txtBUBVoltage.Size = new System.Drawing.Size(100, 20);
         this.txtBUBVoltage.TabIndex = 167;
         this.toolTip1.SetToolTip(this.txtBUBVoltage, "Reported Voltage");
         // 
         // txtBUBFCC
         // 
         this.txtBUBFCC.BackColor = System.Drawing.SystemColors.ControlLightLight;
         this.txtBUBFCC.Location = new System.Drawing.Point(121, 45);
         this.txtBUBFCC.Name = "txtBUBFCC";
         this.txtBUBFCC.ReadOnly = true;
         this.txtBUBFCC.Size = new System.Drawing.Size(100, 20);
         this.txtBUBFCC.TabIndex = 166;
         // 
         // tabPage8
         // 
         this.tabPage8.Controls.Add(this.label85);
         this.tabPage8.Controls.Add(this.label84);
         this.tabPage8.Controls.Add(this.label83);
         this.tabPage8.Controls.Add(this.label82);
         this.tabPage8.Controls.Add(this.label81);
         this.tabPage8.Controls.Add(this.label80);
         this.tabPage8.Controls.Add(this.label79);
         this.tabPage8.Controls.Add(this.pbxRebootWiFi);
         this.tabPage8.Controls.Add(this.pbxSaveConfiguration);
         this.tabPage8.Controls.Add(this.pbxSetExternalAntenna);
         this.tabPage8.Controls.Add(this.pbxSetCommTimeout);
         this.tabPage8.Controls.Add(this.pbxSetCommSize);
         this.tabPage8.Controls.Add(this.pbxSetRemoteResponse);
         this.tabPage8.Controls.Add(this.pbxSetRemotePort);
         this.tabPage8.Controls.Add(this.label78);
         this.tabPage8.Controls.Add(this.pbxSetHostIPAddress);
         this.tabPage8.Controls.Add(this.label77);
         this.tabPage8.Controls.Add(this.pbxSetSSID);
         this.tabPage8.Controls.Add(this.label76);
         this.tabPage8.Controls.Add(this.pbxBaudRate);
         this.tabPage8.Controls.Add(this.label75);
         this.tabPage8.Controls.Add(this.pbxCommandMode);
         this.tabPage8.Controls.Add(this.ckbNewDevice);
         this.tabPage8.Controls.Add(this.btnSetAllParameters);
         this.tabPage8.Controls.Add(this.groupBox9);
         this.tabPage8.Controls.Add(this.txtReboot);
         this.tabPage8.Controls.Add(this.txtSave);
         this.tabPage8.Controls.Add(this.txtExtAnt);
         this.tabPage8.Controls.Add(this.txtCommTime);
         this.tabPage8.Controls.Add(this.txtCommSize);
         this.tabPage8.Controls.Add(this.txtRemoteResponse);
         this.tabPage8.Controls.Add(this.txtRemotePort);
         this.tabPage8.Controls.Add(this.txtHostAddress);
         this.tabPage8.Controls.Add(this.txtSSID);
         this.tabPage8.Controls.Add(this.txtBaudRate);
         this.tabPage8.Controls.Add(this.txtEnterCommandMode);
         this.tabPage8.Controls.Add(this.btnReboot);
         this.tabPage8.Controls.Add(this.btnSave);
         this.tabPage8.Controls.Add(this.btnExtAnt);
         this.tabPage8.Controls.Add(this.btnCommTime);
         this.tabPage8.Controls.Add(this.btnCommSize);
         this.tabPage8.Controls.Add(this.btnRemoteResponse);
         this.tabPage8.Controls.Add(this.btnRemotePort);
         this.tabPage8.Controls.Add(this.btnHostAddress);
         this.tabPage8.Controls.Add(this.btnSetSSID);
         this.tabPage8.Controls.Add(this.btnSetBaudRate);
         this.tabPage8.Controls.Add(this.btnCommandMode);
         this.tabPage8.Location = new System.Drawing.Point(4, 22);
         this.tabPage8.Name = "tabPage8";
         this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage8.Size = new System.Drawing.Size(852, 456);
         this.tabPage8.TabIndex = 7;
         this.tabPage8.Text = "WiFi Setup 1";
         this.tabPage8.UseVisualStyleBackColor = true;
         // 
         // label85
         // 
         this.label85.AutoSize = true;
         this.label85.Location = new System.Drawing.Point(165, 313);
         this.label85.Name = "label85";
         this.label85.Size = new System.Drawing.Size(66, 13);
         this.label85.TabIndex = 276;
         this.label85.Text = "Reboot WiFi";
         // 
         // label84
         // 
         this.label84.AutoSize = true;
         this.label84.Location = new System.Drawing.Point(165, 283);
         this.label84.Name = "label84";
         this.label84.Size = new System.Drawing.Size(99, 13);
         this.label84.TabIndex = 275;
         this.label84.Text = "Save configurartion";
         // 
         // label83
         // 
         this.label83.AutoSize = true;
         this.label83.Location = new System.Drawing.Point(165, 254);
         this.label83.Name = "label83";
         this.label83.Size = new System.Drawing.Size(107, 13);
         this.label83.TabIndex = 274;
         this.label83.Text = "Set External Antenna";
         // 
         // label82
         // 
         this.label82.AutoSize = true;
         this.label82.Location = new System.Drawing.Point(165, 225);
         this.label82.Name = "label82";
         this.label82.Size = new System.Drawing.Size(96, 13);
         this.label82.TabIndex = 273;
         this.label82.Text = "Set Comm Timeout";
         // 
         // label81
         // 
         this.label81.AutoSize = true;
         this.label81.Location = new System.Drawing.Point(165, 196);
         this.label81.Name = "label81";
         this.label81.Size = new System.Drawing.Size(78, 13);
         this.label81.TabIndex = 272;
         this.label81.Text = "Set Comm Size";
         // 
         // label80
         // 
         this.label80.AutoSize = true;
         this.label80.Location = new System.Drawing.Point(165, 167);
         this.label80.Name = "label80";
         this.label80.Size = new System.Drawing.Size(114, 13);
         this.label80.TabIndex = 271;
         this.label80.Text = "Set Remote Response";
         // 
         // label79
         // 
         this.label79.AutoSize = true;
         this.label79.Location = new System.Drawing.Point(165, 138);
         this.label79.Name = "label79";
         this.label79.Size = new System.Drawing.Size(85, 13);
         this.label79.TabIndex = 270;
         this.label79.Text = "Set Remote Port";
         // 
         // pbxRebootWiFi
         // 
         this.pbxRebootWiFi.Location = new System.Drawing.Point(146, 311);
         this.pbxRebootWiFi.Name = "pbxRebootWiFi";
         this.pbxRebootWiFi.Size = new System.Drawing.Size(17, 17);
         this.pbxRebootWiFi.TabIndex = 269;
         this.pbxRebootWiFi.TabStop = false;
         // 
         // pbxSaveConfiguration
         // 
         this.pbxSaveConfiguration.Location = new System.Drawing.Point(146, 281);
         this.pbxSaveConfiguration.Name = "pbxSaveConfiguration";
         this.pbxSaveConfiguration.Size = new System.Drawing.Size(17, 17);
         this.pbxSaveConfiguration.TabIndex = 268;
         this.pbxSaveConfiguration.TabStop = false;
         // 
         // pbxSetExternalAntenna
         // 
         this.pbxSetExternalAntenna.Location = new System.Drawing.Point(146, 252);
         this.pbxSetExternalAntenna.Name = "pbxSetExternalAntenna";
         this.pbxSetExternalAntenna.Size = new System.Drawing.Size(17, 17);
         this.pbxSetExternalAntenna.TabIndex = 267;
         this.pbxSetExternalAntenna.TabStop = false;
         // 
         // pbxSetCommTimeout
         // 
         this.pbxSetCommTimeout.Location = new System.Drawing.Point(146, 223);
         this.pbxSetCommTimeout.Name = "pbxSetCommTimeout";
         this.pbxSetCommTimeout.Size = new System.Drawing.Size(17, 17);
         this.pbxSetCommTimeout.TabIndex = 266;
         this.pbxSetCommTimeout.TabStop = false;
         // 
         // pbxSetCommSize
         // 
         this.pbxSetCommSize.Location = new System.Drawing.Point(146, 194);
         this.pbxSetCommSize.Name = "pbxSetCommSize";
         this.pbxSetCommSize.Size = new System.Drawing.Size(17, 17);
         this.pbxSetCommSize.TabIndex = 265;
         this.pbxSetCommSize.TabStop = false;
         // 
         // pbxSetRemoteResponse
         // 
         this.pbxSetRemoteResponse.Location = new System.Drawing.Point(146, 165);
         this.pbxSetRemoteResponse.Name = "pbxSetRemoteResponse";
         this.pbxSetRemoteResponse.Size = new System.Drawing.Size(17, 17);
         this.pbxSetRemoteResponse.TabIndex = 264;
         this.pbxSetRemoteResponse.TabStop = false;
         // 
         // pbxSetRemotePort
         // 
         this.pbxSetRemotePort.Location = new System.Drawing.Point(146, 136);
         this.pbxSetRemotePort.Name = "pbxSetRemotePort";
         this.pbxSetRemotePort.Size = new System.Drawing.Size(17, 17);
         this.pbxSetRemotePort.TabIndex = 263;
         this.pbxSetRemotePort.TabStop = false;
         // 
         // label78
         // 
         this.label78.AutoSize = true;
         this.label78.Location = new System.Drawing.Point(165, 109);
         this.label78.Name = "label78";
         this.label78.Size = new System.Drawing.Size(102, 13);
         this.label78.TabIndex = 262;
         this.label78.Text = "Set Host IP Address";
         // 
         // pbxSetHostIPAddress
         // 
         this.pbxSetHostIPAddress.Location = new System.Drawing.Point(146, 107);
         this.pbxSetHostIPAddress.Name = "pbxSetHostIPAddress";
         this.pbxSetHostIPAddress.Size = new System.Drawing.Size(17, 17);
         this.pbxSetHostIPAddress.TabIndex = 261;
         this.pbxSetHostIPAddress.TabStop = false;
         // 
         // label77
         // 
         this.label77.AutoSize = true;
         this.label77.Location = new System.Drawing.Point(165, 80);
         this.label77.Name = "label77";
         this.label77.Size = new System.Drawing.Size(51, 13);
         this.label77.TabIndex = 260;
         this.label77.Text = "Set SSID";
         // 
         // pbxSetSSID
         // 
         this.pbxSetSSID.Location = new System.Drawing.Point(146, 78);
         this.pbxSetSSID.Name = "pbxSetSSID";
         this.pbxSetSSID.Size = new System.Drawing.Size(17, 17);
         this.pbxSetSSID.TabIndex = 259;
         this.pbxSetSSID.TabStop = false;
         // 
         // label76
         // 
         this.label76.AutoSize = true;
         this.label76.Location = new System.Drawing.Point(165, 51);
         this.label76.Name = "label76";
         this.label76.Size = new System.Drawing.Size(77, 13);
         this.label76.TabIndex = 258;
         this.label76.Text = "Set Baud Rate";
         // 
         // pbxBaudRate
         // 
         this.pbxBaudRate.Location = new System.Drawing.Point(146, 49);
         this.pbxBaudRate.Name = "pbxBaudRate";
         this.pbxBaudRate.Size = new System.Drawing.Size(17, 17);
         this.pbxBaudRate.TabIndex = 257;
         this.pbxBaudRate.TabStop = false;
         // 
         // label75
         // 
         this.label75.AutoSize = true;
         this.label75.Location = new System.Drawing.Point(165, 22);
         this.label75.Name = "label75";
         this.label75.Size = new System.Drawing.Size(112, 13);
         this.label75.TabIndex = 256;
         this.label75.Text = "Enter Command Mode";
         // 
         // pbxCommandMode
         // 
         this.pbxCommandMode.Location = new System.Drawing.Point(146, 20);
         this.pbxCommandMode.Name = "pbxCommandMode";
         this.pbxCommandMode.Size = new System.Drawing.Size(17, 17);
         this.pbxCommandMode.TabIndex = 255;
         this.pbxCommandMode.TabStop = false;
         // 
         // ckbNewDevice
         // 
         this.ckbNewDevice.AutoSize = true;
         this.ckbNewDevice.Location = new System.Drawing.Point(217, 372);
         this.ckbNewDevice.Name = "ckbNewDevice";
         this.ckbNewDevice.Size = new System.Drawing.Size(170, 17);
         this.ckbNewDevice.TabIndex = 231;
         this.ckbNewDevice.Text = "New Device  (sets 9600 baud)";
         this.toolTip1.SetToolTip(this.ckbNewDevice, "Check if device has never been commissioned");
         this.ckbNewDevice.UseVisualStyleBackColor = true;
         // 
         // btnSetAllParameters
         // 
         this.btnSetAllParameters.Location = new System.Drawing.Point(18, 368);
         this.btnSetAllParameters.Name = "btnSetAllParameters";
         this.btnSetAllParameters.Size = new System.Drawing.Size(177, 23);
         this.btnSetAllParameters.TabIndex = 230;
         this.btnSetAllParameters.Text = "Set All Parameters";
         this.btnSetAllParameters.UseVisualStyleBackColor = true;
         this.btnSetAllParameters.Click += new System.EventHandler(this.btnSetAllParameters_Click);
         // 
         // groupBox9
         // 
         this.groupBox9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox9.Controls.Add(this.rdoExternal);
         this.groupBox9.Controls.Add(this.btnSendWiFi);
         this.groupBox9.Controls.Add(this.rdoInternal);
         this.groupBox9.Controls.Add(this.btnSetBaudHigh);
         this.groupBox9.Controls.Add(this.btnUpdateRoving);
         this.groupBox9.Controls.Add(this.btnSetBaudLow);
         this.groupBox9.Controls.Add(this.label34);
         this.groupBox9.Controls.Add(this.txtWiFiResponse);
         this.groupBox9.Controls.Add(this.btnWiFiSend);
         this.groupBox9.Controls.Add(this.txtWiFiCommand);
         this.groupBox9.Controls.Add(this.btnFactoryDefault);
         this.groupBox9.Controls.Add(this.btnSetAdHocMode);
         this.groupBox9.Location = new System.Drawing.Point(485, 159);
         this.groupBox9.Name = "groupBox9";
         this.groupBox9.Size = new System.Drawing.Size(361, 291);
         this.groupBox9.TabIndex = 227;
         this.groupBox9.TabStop = false;
         this.groupBox9.Text = "Roving Module Command";
         // 
         // rdoExternal
         // 
         this.rdoExternal.AutoSize = true;
         this.rdoExternal.Location = new System.Drawing.Point(263, 176);
         this.rdoExternal.Name = "rdoExternal";
         this.rdoExternal.Size = new System.Drawing.Size(63, 17);
         this.rdoExternal.TabIndex = 279;
         this.rdoExternal.Text = "External";
         this.rdoExternal.UseVisualStyleBackColor = true;
         // 
         // btnSendWiFi
         // 
         this.btnSendWiFi.Location = new System.Drawing.Point(268, 63);
         this.btnSendWiFi.Name = "btnSendWiFi";
         this.btnSendWiFi.Size = new System.Drawing.Size(75, 23);
         this.btnSendWiFi.TabIndex = 6;
         this.btnSendWiFi.Text = "Send Raw";
         this.btnSendWiFi.UseVisualStyleBackColor = true;
         this.btnSendWiFi.Click += new System.EventHandler(this.btnSendWiFi_Click);
         // 
         // rdoInternal
         // 
         this.rdoInternal.AutoSize = true;
         this.rdoInternal.Checked = true;
         this.rdoInternal.Location = new System.Drawing.Point(182, 176);
         this.rdoInternal.Name = "rdoInternal";
         this.rdoInternal.Size = new System.Drawing.Size(60, 17);
         this.rdoInternal.TabIndex = 278;
         this.rdoInternal.TabStop = true;
         this.rdoInternal.Text = "Internal";
         this.rdoInternal.UseVisualStyleBackColor = true;
         // 
         // btnSetBaudHigh
         // 
         this.btnSetBaudHigh.Location = new System.Drawing.Point(139, 140);
         this.btnSetBaudHigh.Name = "btnSetBaudHigh";
         this.btnSetBaudHigh.Size = new System.Drawing.Size(100, 23);
         this.btnSetBaudHigh.TabIndex = 5;
         this.btnSetBaudHigh.Text = "Set Baud 38400";
         this.btnSetBaudHigh.UseVisualStyleBackColor = true;
         this.btnSetBaudHigh.Click += new System.EventHandler(this.btnSetBaudHigh_Click);
         // 
         // btnUpdateRoving
         // 
         this.btnUpdateRoving.Location = new System.Drawing.Point(167, 195);
         this.btnUpdateRoving.Name = "btnUpdateRoving";
         this.btnUpdateRoving.Size = new System.Drawing.Size(176, 23);
         this.btnUpdateRoving.TabIndex = 277;
         this.btnUpdateRoving.Text = "Update Roving Firmware";
         this.btnUpdateRoving.UseVisualStyleBackColor = true;
         this.btnUpdateRoving.Click += new System.EventHandler(this.btnUpdateRoving_Click);
         // 
         // btnSetBaudLow
         // 
         this.btnSetBaudLow.Location = new System.Drawing.Point(18, 140);
         this.btnSetBaudLow.Name = "btnSetBaudLow";
         this.btnSetBaudLow.Size = new System.Drawing.Size(100, 23);
         this.btnSetBaudLow.TabIndex = 4;
         this.btnSetBaudLow.Text = "Set Baud 9600";
         this.btnSetBaudLow.UseVisualStyleBackColor = true;
         this.btnSetBaudLow.Click += new System.EventHandler(this.btnSetBaudLow_Click);
         // 
         // label34
         // 
         this.label34.AutoSize = true;
         this.label34.Location = new System.Drawing.Point(16, 105);
         this.label34.Name = "label34";
         this.label34.Size = new System.Drawing.Size(79, 13);
         this.label34.TabIndex = 3;
         this.label34.Text = "WiFi Response";
         // 
         // txtWiFiResponse
         // 
         this.txtWiFiResponse.Location = new System.Drawing.Point(97, 101);
         this.txtWiFiResponse.Name = "txtWiFiResponse";
         this.txtWiFiResponse.Size = new System.Drawing.Size(246, 20);
         this.txtWiFiResponse.TabIndex = 2;
         // 
         // btnWiFiSend
         // 
         this.btnWiFiSend.Location = new System.Drawing.Point(18, 61);
         this.btnWiFiSend.Name = "btnWiFiSend";
         this.btnWiFiSend.Size = new System.Drawing.Size(75, 23);
         this.btnWiFiSend.TabIndex = 1;
         this.btnWiFiSend.Text = "Send";
         this.btnWiFiSend.UseVisualStyleBackColor = true;
         this.btnWiFiSend.Click += new System.EventHandler(this.btnWiFiSend_Click);
         // 
         // txtWiFiCommand
         // 
         this.txtWiFiCommand.Location = new System.Drawing.Point(18, 25);
         this.txtWiFiCommand.Name = "txtWiFiCommand";
         this.txtWiFiCommand.Size = new System.Drawing.Size(325, 20);
         this.txtWiFiCommand.TabIndex = 0;
         // 
         // btnFactoryDefault
         // 
         this.btnFactoryDefault.Enabled = false;
         this.btnFactoryDefault.Location = new System.Drawing.Point(167, 224);
         this.btnFactoryDefault.Name = "btnFactoryDefault";
         this.btnFactoryDefault.Size = new System.Drawing.Size(178, 23);
         this.btnFactoryDefault.TabIndex = 228;
         this.btnFactoryDefault.Text = "Set Factory Default";
         this.btnFactoryDefault.UseVisualStyleBackColor = true;
         this.btnFactoryDefault.Click += new System.EventHandler(this.btnFactoryDefault_Click);
         // 
         // btnSetAdHocMode
         // 
         this.btnSetAdHocMode.Enabled = false;
         this.btnSetAdHocMode.Location = new System.Drawing.Point(167, 253);
         this.btnSetAdHocMode.Name = "btnSetAdHocMode";
         this.btnSetAdHocMode.Size = new System.Drawing.Size(178, 23);
         this.btnSetAdHocMode.TabIndex = 229;
         this.btnSetAdHocMode.Text = "Set Ad Hoc Mode";
         this.btnSetAdHocMode.UseVisualStyleBackColor = true;
         this.btnSetAdHocMode.Click += new System.EventHandler(this.btnSetAdHocMode_Click);
         // 
         // txtReboot
         // 
         this.txtReboot.Location = new System.Drawing.Point(282, 310);
         this.txtReboot.Name = "txtReboot";
         this.txtReboot.Size = new System.Drawing.Size(90, 20);
         this.txtReboot.TabIndex = 225;
         this.txtReboot.Text = "reboot";
         // 
         // txtSave
         // 
         this.txtSave.Location = new System.Drawing.Point(282, 280);
         this.txtSave.Name = "txtSave";
         this.txtSave.Size = new System.Drawing.Size(90, 20);
         this.txtSave.TabIndex = 223;
         this.txtSave.Text = "save";
         // 
         // txtExtAnt
         // 
         this.txtExtAnt.Location = new System.Drawing.Point(283, 251);
         this.txtExtAnt.Name = "txtExtAnt";
         this.txtExtAnt.Size = new System.Drawing.Size(89, 20);
         this.txtExtAnt.TabIndex = 221;
         this.txtExtAnt.Text = "1";
         // 
         // txtCommTime
         // 
         this.txtCommTime.Location = new System.Drawing.Point(282, 222);
         this.txtCommTime.Name = "txtCommTime";
         this.txtCommTime.Size = new System.Drawing.Size(90, 20);
         this.txtCommTime.TabIndex = 219;
         this.txtCommTime.Text = "400";
         // 
         // txtCommSize
         // 
         this.txtCommSize.Location = new System.Drawing.Point(282, 194);
         this.txtCommSize.Name = "txtCommSize";
         this.txtCommSize.Size = new System.Drawing.Size(90, 20);
         this.txtCommSize.TabIndex = 217;
         this.txtCommSize.Text = "1420";
         // 
         // txtRemoteResponse
         // 
         this.txtRemoteResponse.Location = new System.Drawing.Point(282, 165);
         this.txtRemoteResponse.Name = "txtRemoteResponse";
         this.txtRemoteResponse.Size = new System.Drawing.Size(90, 20);
         this.txtRemoteResponse.TabIndex = 215;
         this.txtRemoteResponse.Text = "0";
         // 
         // txtRemotePort
         // 
         this.txtRemotePort.Location = new System.Drawing.Point(282, 135);
         this.txtRemotePort.Name = "txtRemotePort";
         this.txtRemotePort.Size = new System.Drawing.Size(90, 20);
         this.txtRemotePort.TabIndex = 213;
         this.txtRemotePort.Text = "80";
         // 
         // txtHostAddress
         // 
         this.txtHostAddress.Location = new System.Drawing.Point(282, 106);
         this.txtHostAddress.Name = "txtHostAddress";
         this.txtHostAddress.Size = new System.Drawing.Size(90, 20);
         this.txtHostAddress.TabIndex = 211;
         this.txtHostAddress.Text = "208.62.45.106";
         // 
         // txtSSID
         // 
         this.txtSSID.Location = new System.Drawing.Point(282, 77);
         this.txtSSID.Name = "txtSSID";
         this.txtSSID.Size = new System.Drawing.Size(90, 20);
         this.txtSSID.TabIndex = 209;
         this.txtSSID.Text = "WOW";
         this.txtSSID.Leave += new System.EventHandler(this.txtSSID1_Leave);
         // 
         // txtBaudRate
         // 
         this.txtBaudRate.Location = new System.Drawing.Point(282, 48);
         this.txtBaudRate.Name = "txtBaudRate";
         this.txtBaudRate.Size = new System.Drawing.Size(90, 20);
         this.txtBaudRate.TabIndex = 208;
         this.txtBaudRate.Text = "38400";
         this.toolTip1.SetToolTip(this.txtBaudRate, "9600 or 38400 only");
         // 
         // txtEnterCommandMode
         // 
         this.txtEnterCommandMode.Location = new System.Drawing.Point(282, 19);
         this.txtEnterCommandMode.Name = "txtEnterCommandMode";
         this.txtEnterCommandMode.ReadOnly = true;
         this.txtEnterCommandMode.Size = new System.Drawing.Size(90, 20);
         this.txtEnterCommandMode.TabIndex = 207;
         this.txtEnterCommandMode.Text = "$$$";
         // 
         // btnReboot
         // 
         this.btnReboot.Location = new System.Drawing.Point(18, 308);
         this.btnReboot.Name = "btnReboot";
         this.btnReboot.Size = new System.Drawing.Size(122, 23);
         this.btnReboot.TabIndex = 205;
         this.btnReboot.Text = "Reboot";
         this.btnReboot.UseVisualStyleBackColor = true;
         this.btnReboot.Click += new System.EventHandler(this.btnReboot_Click);
         // 
         // btnSave
         // 
         this.btnSave.Location = new System.Drawing.Point(18, 278);
         this.btnSave.Name = "btnSave";
         this.btnSave.Size = new System.Drawing.Size(122, 23);
         this.btnSave.TabIndex = 203;
         this.btnSave.Text = "Save";
         this.btnSave.UseVisualStyleBackColor = true;
         this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
         // 
         // btnExtAnt
         // 
         this.btnExtAnt.Enabled = false;
         this.btnExtAnt.Location = new System.Drawing.Point(18, 249);
         this.btnExtAnt.Name = "btnExtAnt";
         this.btnExtAnt.Size = new System.Drawing.Size(122, 23);
         this.btnExtAnt.TabIndex = 201;
         this.btnExtAnt.Text = "External Antenna";
         this.btnExtAnt.UseVisualStyleBackColor = true;
         this.btnExtAnt.Click += new System.EventHandler(this.btnExtAnt_Click);
         // 
         // btnCommTime
         // 
         this.btnCommTime.Enabled = false;
         this.btnCommTime.Location = new System.Drawing.Point(18, 220);
         this.btnCommTime.Name = "btnCommTime";
         this.btnCommTime.Size = new System.Drawing.Size(122, 23);
         this.btnCommTime.TabIndex = 199;
         this.btnCommTime.Text = "Comm Timeout";
         this.btnCommTime.UseVisualStyleBackColor = true;
         this.btnCommTime.Click += new System.EventHandler(this.btnCommTime_Click);
         // 
         // btnCommSize
         // 
         this.btnCommSize.Enabled = false;
         this.btnCommSize.Location = new System.Drawing.Point(18, 191);
         this.btnCommSize.Name = "btnCommSize";
         this.btnCommSize.Size = new System.Drawing.Size(122, 23);
         this.btnCommSize.TabIndex = 197;
         this.btnCommSize.Text = "Comm Size";
         this.btnCommSize.UseVisualStyleBackColor = true;
         this.btnCommSize.Click += new System.EventHandler(this.btnCommSize_Click);
         // 
         // btnRemoteResponse
         // 
         this.btnRemoteResponse.Enabled = false;
         this.btnRemoteResponse.Location = new System.Drawing.Point(18, 162);
         this.btnRemoteResponse.Name = "btnRemoteResponse";
         this.btnRemoteResponse.Size = new System.Drawing.Size(122, 23);
         this.btnRemoteResponse.TabIndex = 195;
         this.btnRemoteResponse.Text = "Remote Response";
         this.btnRemoteResponse.UseVisualStyleBackColor = true;
         this.btnRemoteResponse.Click += new System.EventHandler(this.btnRemoteResponse_Click);
         // 
         // btnRemotePort
         // 
         this.btnRemotePort.Enabled = false;
         this.btnRemotePort.Location = new System.Drawing.Point(18, 133);
         this.btnRemotePort.Name = "btnRemotePort";
         this.btnRemotePort.Size = new System.Drawing.Size(122, 23);
         this.btnRemotePort.TabIndex = 193;
         this.btnRemotePort.Text = "Remote Port";
         this.btnRemotePort.UseVisualStyleBackColor = true;
         this.btnRemotePort.Click += new System.EventHandler(this.btnRemotePort_Click);
         // 
         // btnHostAddress
         // 
         this.btnHostAddress.Enabled = false;
         this.btnHostAddress.Location = new System.Drawing.Point(18, 104);
         this.btnHostAddress.Name = "btnHostAddress";
         this.btnHostAddress.Size = new System.Drawing.Size(122, 23);
         this.btnHostAddress.TabIndex = 191;
         this.btnHostAddress.Text = "Host IP Address";
         this.btnHostAddress.UseVisualStyleBackColor = true;
         this.btnHostAddress.Click += new System.EventHandler(this.btnHostAddress_Click);
         // 
         // btnSetSSID
         // 
         this.btnSetSSID.Location = new System.Drawing.Point(18, 75);
         this.btnSetSSID.Name = "btnSetSSID";
         this.btnSetSSID.Size = new System.Drawing.Size(122, 23);
         this.btnSetSSID.TabIndex = 189;
         this.btnSetSSID.Text = "Set SSID";
         this.btnSetSSID.UseVisualStyleBackColor = true;
         this.btnSetSSID.Click += new System.EventHandler(this.btnSetSSID1_Click);
         // 
         // btnSetBaudRate
         // 
         this.btnSetBaudRate.Location = new System.Drawing.Point(18, 46);
         this.btnSetBaudRate.Name = "btnSetBaudRate";
         this.btnSetBaudRate.Size = new System.Drawing.Size(122, 23);
         this.btnSetBaudRate.TabIndex = 187;
         this.btnSetBaudRate.Text = "Set Baud Rate";
         this.btnSetBaudRate.UseVisualStyleBackColor = true;
         this.btnSetBaudRate.Click += new System.EventHandler(this.btnSetBaudRate_Click);
         // 
         // btnCommandMode
         // 
         this.btnCommandMode.Location = new System.Drawing.Point(18, 17);
         this.btnCommandMode.Name = "btnCommandMode";
         this.btnCommandMode.Size = new System.Drawing.Size(122, 23);
         this.btnCommandMode.TabIndex = 185;
         this.btnCommandMode.Text = "Command Mode";
         this.btnCommandMode.UseVisualStyleBackColor = true;
         this.btnCommandMode.Click += new System.EventHandler(this.btnCommandMode_Click);
         // 
         // tabPage10
         // 
         this.tabPage10.Controls.Add(this.label87);
         this.tabPage10.Controls.Add(this.pbxSetChannel);
         this.tabPage10.Controls.Add(this.groupBox27);
         this.tabPage10.Controls.Add(this.label138);
         this.tabPage10.Controls.Add(this.txtChannel);
         this.tabPage10.Controls.Add(this.btnSetChannel);
         this.tabPage10.Controls.Add(this.groupBox28);
         this.tabPage10.Controls.Add(this.groupBox29);
         this.tabPage10.Location = new System.Drawing.Point(4, 22);
         this.tabPage10.Name = "tabPage10";
         this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage10.Size = new System.Drawing.Size(852, 456);
         this.tabPage10.TabIndex = 9;
         this.tabPage10.Text = "WiFi Setup 2";
         this.tabPage10.UseVisualStyleBackColor = true;
         // 
         // label87
         // 
         this.label87.AutoSize = true;
         this.label87.Location = new System.Drawing.Point(478, 213);
         this.label87.Name = "label87";
         this.label87.Size = new System.Drawing.Size(65, 13);
         this.label87.TabIndex = 260;
         this.label87.Text = "Set Channel";
         // 
         // pbxSetChannel
         // 
         this.pbxSetChannel.Location = new System.Drawing.Point(459, 211);
         this.pbxSetChannel.Name = "pbxSetChannel";
         this.pbxSetChannel.Size = new System.Drawing.Size(17, 17);
         this.pbxSetChannel.TabIndex = 259;
         this.pbxSetChannel.TabStop = false;
         // 
         // groupBox27
         // 
         this.groupBox27.Controls.Add(this.label95);
         this.groupBox27.Controls.Add(this.txtDNSServer);
         this.groupBox27.Controls.Add(this.txtGateway);
         this.groupBox27.Controls.Add(this.txtSubnetMask);
         this.groupBox27.Controls.Add(this.label96);
         this.groupBox27.Controls.Add(this.label97);
         this.groupBox27.Controls.Add(this.label89);
         this.groupBox27.Controls.Add(this.pbxSetDHCP);
         this.groupBox27.Controls.Add(this.btnSetDHCP);
         this.groupBox27.Controls.Add(this.rdoDHCPCache);
         this.groupBox27.Controls.Add(this.txtDHCPIPAddress);
         this.groupBox27.Controls.Add(this.label139);
         this.groupBox27.Controls.Add(this.rdoAutoIP);
         this.groupBox27.Controls.Add(this.rdoDHCPOn);
         this.groupBox27.Controls.Add(this.rdoDHCPOff);
         this.groupBox27.Location = new System.Drawing.Point(6, 294);
         this.groupBox27.Name = "groupBox27";
         this.groupBox27.Size = new System.Drawing.Size(432, 120);
         this.groupBox27.TabIndex = 24;
         this.groupBox27.TabStop = false;
         this.groupBox27.Text = "DHCP";
         // 
         // label95
         // 
         this.label95.AutoSize = true;
         this.label95.Location = new System.Drawing.Point(160, 88);
         this.label95.Name = "label95";
         this.label95.Size = new System.Drawing.Size(64, 13);
         this.label95.TabIndex = 266;
         this.label95.Text = "DNS Server";
         // 
         // txtDNSServer
         // 
         this.txtDNSServer.Enabled = false;
         this.txtDNSServer.Location = new System.Drawing.Point(226, 84);
         this.txtDNSServer.Name = "txtDNSServer";
         this.txtDNSServer.Size = new System.Drawing.Size(100, 20);
         this.txtDNSServer.TabIndex = 263;
         // 
         // txtGateway
         // 
         this.txtGateway.Enabled = false;
         this.txtGateway.Location = new System.Drawing.Point(226, 62);
         this.txtGateway.Name = "txtGateway";
         this.txtGateway.Size = new System.Drawing.Size(100, 20);
         this.txtGateway.TabIndex = 262;
         // 
         // txtSubnetMask
         // 
         this.txtSubnetMask.Enabled = false;
         this.txtSubnetMask.Location = new System.Drawing.Point(226, 40);
         this.txtSubnetMask.Name = "txtSubnetMask";
         this.txtSubnetMask.Size = new System.Drawing.Size(100, 20);
         this.txtSubnetMask.TabIndex = 261;
         this.txtSubnetMask.Text = "255.255.255.255";
         // 
         // label96
         // 
         this.label96.AutoSize = true;
         this.label96.Location = new System.Drawing.Point(154, 44);
         this.label96.Name = "label96";
         this.label96.Size = new System.Drawing.Size(70, 13);
         this.label96.TabIndex = 264;
         this.label96.Text = "Subnet Mask";
         // 
         // label97
         // 
         this.label97.AutoSize = true;
         this.label97.Location = new System.Drawing.Point(139, 65);
         this.label97.Name = "label97";
         this.label97.Size = new System.Drawing.Size(86, 13);
         this.label97.TabIndex = 265;
         this.label97.Text = "Default Gateway";
         // 
         // label89
         // 
         this.label89.AutoSize = true;
         this.label89.Location = new System.Drawing.Point(361, 46);
         this.label89.Name = "label89";
         this.label89.Size = new System.Drawing.Size(56, 13);
         this.label89.TabIndex = 260;
         this.label89.Text = "Set DHCP";
         // 
         // pbxSetDHCP
         // 
         this.pbxSetDHCP.Location = new System.Drawing.Point(342, 44);
         this.pbxSetDHCP.Name = "pbxSetDHCP";
         this.pbxSetDHCP.Size = new System.Drawing.Size(17, 17);
         this.pbxSetDHCP.TabIndex = 259;
         this.pbxSetDHCP.TabStop = false;
         // 
         // btnSetDHCP
         // 
         this.btnSetDHCP.Enabled = false;
         this.btnSetDHCP.Location = new System.Drawing.Point(342, 17);
         this.btnSetDHCP.Name = "btnSetDHCP";
         this.btnSetDHCP.Size = new System.Drawing.Size(75, 23);
         this.btnSetDHCP.TabIndex = 5;
         this.btnSetDHCP.Text = "Set DHCP";
         this.btnSetDHCP.UseVisualStyleBackColor = true;
         this.btnSetDHCP.Click += new System.EventHandler(this.btnSetDHCP_Click);
         // 
         // rdoDHCPCache
         // 
         this.rdoDHCPCache.AutoSize = true;
         this.rdoDHCPCache.Location = new System.Drawing.Point(14, 88);
         this.rdoDHCPCache.Name = "rdoDHCPCache";
         this.rdoDHCPCache.Size = new System.Drawing.Size(119, 17);
         this.rdoDHCPCache.TabIndex = 3;
         this.rdoDHCPCache.Text = "DHCP Cache Mode";
         this.rdoDHCPCache.UseVisualStyleBackColor = true;
         // 
         // txtDHCPIPAddress
         // 
         this.txtDHCPIPAddress.Enabled = false;
         this.txtDHCPIPAddress.Location = new System.Drawing.Point(226, 18);
         this.txtDHCPIPAddress.Name = "txtDHCPIPAddress";
         this.txtDHCPIPAddress.Size = new System.Drawing.Size(100, 20);
         this.txtDHCPIPAddress.TabIndex = 4;
         // 
         // label139
         // 
         this.label139.AutoSize = true;
         this.label139.Location = new System.Drawing.Point(166, 22);
         this.label139.Name = "label139";
         this.label139.Size = new System.Drawing.Size(58, 13);
         this.label139.TabIndex = 7;
         this.label139.Text = "IP Address";
         // 
         // rdoAutoIP
         // 
         this.rdoAutoIP.AutoSize = true;
         this.rdoAutoIP.Location = new System.Drawing.Point(14, 65);
         this.rdoAutoIP.Name = "rdoAutoIP";
         this.rdoAutoIP.Size = new System.Drawing.Size(105, 17);
         this.rdoAutoIP.TabIndex = 2;
         this.rdoAutoIP.Text = "Auto-IP  (ad hoc)";
         this.rdoAutoIP.UseVisualStyleBackColor = true;
         // 
         // rdoDHCPOn
         // 
         this.rdoDHCPOn.AutoSize = true;
         this.rdoDHCPOn.Checked = true;
         this.rdoDHCPOn.Location = new System.Drawing.Point(14, 42);
         this.rdoDHCPOn.Name = "rdoDHCPOn";
         this.rdoDHCPOn.Size = new System.Drawing.Size(72, 17);
         this.rdoDHCPOn.TabIndex = 1;
         this.rdoDHCPOn.TabStop = true;
         this.rdoDHCPOn.Text = "DHCP On";
         this.rdoDHCPOn.UseVisualStyleBackColor = true;
         this.rdoDHCPOn.CheckedChanged += new System.EventHandler(this.rdoDHCPOff_CheckedChanged);
         // 
         // rdoDHCPOff
         // 
         this.rdoDHCPOff.AutoSize = true;
         this.rdoDHCPOff.Location = new System.Drawing.Point(14, 19);
         this.rdoDHCPOff.Name = "rdoDHCPOff";
         this.rdoDHCPOff.Size = new System.Drawing.Size(72, 17);
         this.rdoDHCPOff.TabIndex = 0;
         this.rdoDHCPOff.Text = "DHCP Off";
         this.rdoDHCPOff.UseVisualStyleBackColor = true;
         this.rdoDHCPOff.CheckedChanged += new System.EventHandler(this.rdoDHCPOff_CheckedChanged);
         // 
         // label138
         // 
         this.label138.AutoSize = true;
         this.label138.Location = new System.Drawing.Point(595, 214);
         this.label138.Name = "label138";
         this.label138.Size = new System.Drawing.Size(43, 13);
         this.label138.TabIndex = 23;
         this.label138.Text = "(0,1-13)";
         // 
         // txtChannel
         // 
         this.txtChannel.Location = new System.Drawing.Point(551, 210);
         this.txtChannel.Name = "txtChannel";
         this.txtChannel.Size = new System.Drawing.Size(42, 20);
         this.txtChannel.TabIndex = 21;
         this.txtChannel.Text = "0";
         // 
         // btnSetChannel
         // 
         this.btnSetChannel.Location = new System.Drawing.Point(332, 208);
         this.btnSetChannel.Name = "btnSetChannel";
         this.btnSetChannel.Size = new System.Drawing.Size(122, 23);
         this.btnSetChannel.TabIndex = 20;
         this.btnSetChannel.Text = "Set Channel";
         this.btnSetChannel.UseVisualStyleBackColor = true;
         this.btnSetChannel.Click += new System.EventHandler(this.btnSetChannel_Click);
         // 
         // groupBox28
         // 
         this.groupBox28.Controls.Add(this.label88);
         this.groupBox28.Controls.Add(this.pbxSetJoin);
         this.groupBox28.Controls.Add(this.btnSetJoin);
         this.groupBox28.Controls.Add(this.rdoAdHoc);
         this.groupBox28.Controls.Add(this.rdoSecurity);
         this.groupBox28.Controls.Add(this.rdoSSID);
         this.groupBox28.Controls.Add(this.rdoManual);
         this.groupBox28.Location = new System.Drawing.Point(6, 165);
         this.groupBox28.Name = "groupBox28";
         this.groupBox28.Size = new System.Drawing.Size(259, 123);
         this.groupBox28.TabIndex = 19;
         this.groupBox28.TabStop = false;
         this.groupBox28.Text = "Network Joining";
         // 
         // label88
         // 
         this.label88.AutoSize = true;
         this.label88.Location = new System.Drawing.Point(197, 99);
         this.label88.Name = "label88";
         this.label88.Size = new System.Drawing.Size(45, 13);
         this.label88.TabIndex = 260;
         this.label88.Text = "Set Join";
         // 
         // pbxSetJoin
         // 
         this.pbxSetJoin.Location = new System.Drawing.Point(178, 97);
         this.pbxSetJoin.Name = "pbxSetJoin";
         this.pbxSetJoin.Size = new System.Drawing.Size(17, 17);
         this.pbxSetJoin.TabIndex = 259;
         this.pbxSetJoin.TabStop = false;
         // 
         // btnSetJoin
         // 
         this.btnSetJoin.Enabled = false;
         this.btnSetJoin.Location = new System.Drawing.Point(178, 70);
         this.btnSetJoin.Name = "btnSetJoin";
         this.btnSetJoin.Size = new System.Drawing.Size(75, 23);
         this.btnSetJoin.TabIndex = 4;
         this.btnSetJoin.Text = "Set Join";
         this.btnSetJoin.UseVisualStyleBackColor = true;
         this.btnSetJoin.Click += new System.EventHandler(this.btnSetJoin_Click);
         // 
         // rdoAdHoc
         // 
         this.rdoAdHoc.AutoSize = true;
         this.rdoAdHoc.Location = new System.Drawing.Point(17, 89);
         this.rdoAdHoc.Name = "rdoAdHoc";
         this.rdoAdHoc.Size = new System.Drawing.Size(138, 17);
         this.rdoAdHoc.TabIndex = 3;
         this.rdoAdHoc.Text = "Create Ad Hoc Network";
         this.rdoAdHoc.UseVisualStyleBackColor = true;
         // 
         // rdoSecurity
         // 
         this.rdoSecurity.AutoSize = true;
         this.rdoSecurity.Location = new System.Drawing.Point(17, 66);
         this.rdoSecurity.Name = "rdoSecurity";
         this.rdoSecurity.Size = new System.Drawing.Size(96, 17);
         this.rdoSecurity.TabIndex = 2;
         this.rdoSecurity.Text = "Match Security";
         this.rdoSecurity.UseVisualStyleBackColor = true;
         // 
         // rdoSSID
         // 
         this.rdoSSID.AutoSize = true;
         this.rdoSSID.Checked = true;
         this.rdoSSID.Location = new System.Drawing.Point(17, 43);
         this.rdoSSID.Name = "rdoSSID";
         this.rdoSSID.Size = new System.Drawing.Size(168, 17);
         this.rdoSSID.TabIndex = 1;
         this.rdoSSID.TabStop = true;
         this.rdoSSID.Text = "Match SSID,Passkey,Channel";
         this.rdoSSID.UseVisualStyleBackColor = true;
         // 
         // rdoManual
         // 
         this.rdoManual.AutoSize = true;
         this.rdoManual.Location = new System.Drawing.Point(17, 20);
         this.rdoManual.Name = "rdoManual";
         this.rdoManual.Size = new System.Drawing.Size(60, 17);
         this.rdoManual.TabIndex = 0;
         this.rdoManual.Text = "Manual";
         this.rdoManual.UseVisualStyleBackColor = true;
         // 
         // groupBox29
         // 
         this.groupBox29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox29.Controls.Add(this.groupBox34);
         this.groupBox29.Controls.Add(this.label86);
         this.groupBox29.Controls.Add(this.pbxSetAuth);
         this.groupBox29.Controls.Add(this.label134);
         this.groupBox29.Controls.Add(this.label135);
         this.groupBox29.Controls.Add(this.label137);
         this.groupBox29.Controls.Add(this.txtPassPhrase);
         this.groupBox29.Controls.Add(this.txtWEPKey);
         this.groupBox29.Controls.Add(this.btnSetAuth);
         this.groupBox29.Controls.Add(this.rdoWPA2);
         this.groupBox29.Controls.Add(this.rdoMixedWPA);
         this.groupBox29.Controls.Add(this.rdoWPA1);
         this.groupBox29.Controls.Add(this.rdoWEP128);
         this.groupBox29.Controls.Add(this.rdoOpen);
         this.groupBox29.Location = new System.Drawing.Point(6, 6);
         this.groupBox29.Name = "groupBox29";
         this.groupBox29.Size = new System.Drawing.Size(665, 138);
         this.groupBox29.TabIndex = 18;
         this.groupBox29.TabStop = false;
         this.groupBox29.Text = "Authentication";
         // 
         // groupBox34
         // 
         this.groupBox34.Controls.Add(this.rdo26HEX);
         this.groupBox34.Controls.Add(this.rdo13ASCII);
         this.groupBox34.Location = new System.Drawing.Point(404, 13);
         this.groupBox34.Name = "groupBox34";
         this.groupBox34.Size = new System.Drawing.Size(86, 65);
         this.groupBox34.TabIndex = 259;
         this.groupBox34.TabStop = false;
         // 
         // rdo26HEX
         // 
         this.rdo26HEX.AutoSize = true;
         this.rdo26HEX.Location = new System.Drawing.Point(10, 36);
         this.rdo26HEX.Name = "rdo26HEX";
         this.rdo26HEX.Size = new System.Drawing.Size(62, 17);
         this.rdo26HEX.TabIndex = 1;
         this.rdo26HEX.Text = "26 HEX";
         this.rdo26HEX.UseVisualStyleBackColor = true;
         // 
         // rdo13ASCII
         // 
         this.rdo13ASCII.AutoSize = true;
         this.rdo13ASCII.Checked = true;
         this.rdo13ASCII.Location = new System.Drawing.Point(10, 15);
         this.rdo13ASCII.Name = "rdo13ASCII";
         this.rdo13ASCII.Size = new System.Drawing.Size(70, 17);
         this.rdo13ASCII.TabIndex = 0;
         this.rdo13ASCII.TabStop = true;
         this.rdo13ASCII.Text = "13 ASCII ";
         this.rdo13ASCII.UseVisualStyleBackColor = true;
         this.rdo13ASCII.CheckedChanged += new System.EventHandler(this.rdo13ASCII_CheckedChanged);
         // 
         // label86
         // 
         this.label86.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.label86.AutoSize = true;
         this.label86.Location = new System.Drawing.Point(605, 42);
         this.label86.Name = "label86";
         this.label86.Size = new System.Drawing.Size(48, 13);
         this.label86.TabIndex = 258;
         this.label86.Text = "Set Auth";
         // 
         // pbxSetAuth
         // 
         this.pbxSetAuth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.pbxSetAuth.Location = new System.Drawing.Point(586, 40);
         this.pbxSetAuth.Name = "pbxSetAuth";
         this.pbxSetAuth.Size = new System.Drawing.Size(17, 17);
         this.pbxSetAuth.TabIndex = 257;
         this.pbxSetAuth.TabStop = false;
         // 
         // label134
         // 
         this.label134.AutoSize = true;
         this.label134.Location = new System.Drawing.Point(445, 108);
         this.label134.Name = "label134";
         this.label134.Size = new System.Drawing.Size(87, 13);
         this.label134.TabIndex = 11;
         this.label134.Text = "Use $ for spaces";
         // 
         // label135
         // 
         this.label135.AutoSize = true;
         this.label135.Location = new System.Drawing.Point(198, 71);
         this.label135.Name = "label135";
         this.label135.Size = new System.Drawing.Size(149, 13);
         this.label135.TabIndex = 10;
         this.label135.Text = "WPA Passphrase (1-64 chars)";
         // 
         // label137
         // 
         this.label137.AutoSize = true;
         this.label137.Location = new System.Drawing.Point(198, 25);
         this.label137.Name = "label137";
         this.label137.Size = new System.Drawing.Size(120, 13);
         this.label137.TabIndex = 9;
         this.label137.Text = "WEP Key (13/26 chars)";
         // 
         // txtPassPhrase
         // 
         this.txtPassPhrase.Enabled = false;
         this.txtPassPhrase.Location = new System.Drawing.Point(195, 86);
         this.txtPassPhrase.Name = "txtPassPhrase";
         this.txtPassPhrase.Size = new System.Drawing.Size(457, 20);
         this.txtPassPhrase.TabIndex = 6;
         this.txtPassPhrase.Leave += new System.EventHandler(this.txtPassPhrase_Leave);
         // 
         // txtWEPKey
         // 
         this.txtWEPKey.Location = new System.Drawing.Point(195, 40);
         this.txtWEPKey.Name = "txtWEPKey";
         this.txtWEPKey.Size = new System.Drawing.Size(196, 20);
         this.txtWEPKey.TabIndex = 5;
         this.txtWEPKey.Text = "testpowerhere";
         this.toolTip1.SetToolTip(this.txtWEPKey, "Must be exactly 13 charactors");
         this.txtWEPKey.Leave += new System.EventHandler(this.txtWEPKey_Leave);
         // 
         // btnSetAuth
         // 
         this.btnSetAuth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.btnSetAuth.Location = new System.Drawing.Point(584, 14);
         this.btnSetAuth.Name = "btnSetAuth";
         this.btnSetAuth.Size = new System.Drawing.Size(75, 23);
         this.btnSetAuth.TabIndex = 7;
         this.btnSetAuth.Text = "Set Auth";
         this.btnSetAuth.UseVisualStyleBackColor = true;
         this.btnSetAuth.Click += new System.EventHandler(this.btnSetAuth_Click);
         // 
         // rdoWPA2
         // 
         this.rdoWPA2.AutoSize = true;
         this.rdoWPA2.Location = new System.Drawing.Point(17, 112);
         this.rdoWPA2.Name = "rdoWPA2";
         this.rdoWPA2.Size = new System.Drawing.Size(80, 17);
         this.rdoWPA2.TabIndex = 4;
         this.rdoWPA2.Text = "WPA2-PSK";
         this.rdoWPA2.UseVisualStyleBackColor = true;
         this.rdoWPA2.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
         // 
         // rdoMixedWPA
         // 
         this.rdoMixedWPA.AutoSize = true;
         this.rdoMixedWPA.Location = new System.Drawing.Point(17, 89);
         this.rdoMixedWPA.Name = "rdoMixedWPA";
         this.rdoMixedWPA.Size = new System.Drawing.Size(154, 17);
         this.rdoMixedWPA.TabIndex = 3;
         this.rdoMixedWPA.Text = "Mixed WPA1 && WPA2-PSK";
         this.rdoMixedWPA.UseVisualStyleBackColor = true;
         this.rdoMixedWPA.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
         // 
         // rdoWPA1
         // 
         this.rdoWPA1.AutoSize = true;
         this.rdoWPA1.Location = new System.Drawing.Point(17, 66);
         this.rdoWPA1.Name = "rdoWPA1";
         this.rdoWPA1.Size = new System.Drawing.Size(56, 17);
         this.rdoWPA1.TabIndex = 2;
         this.rdoWPA1.Text = "WPA1";
         this.rdoWPA1.UseVisualStyleBackColor = true;
         this.rdoWPA1.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
         // 
         // rdoWEP128
         // 
         this.rdoWEP128.AutoSize = true;
         this.rdoWEP128.Checked = true;
         this.rdoWEP128.Location = new System.Drawing.Point(17, 43);
         this.rdoWEP128.Name = "rdoWEP128";
         this.rdoWEP128.Size = new System.Drawing.Size(71, 17);
         this.rdoWEP128.TabIndex = 1;
         this.rdoWEP128.TabStop = true;
         this.rdoWEP128.Text = "WEP-128";
         this.rdoWEP128.UseVisualStyleBackColor = true;
         this.rdoWEP128.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
         // 
         // rdoOpen
         // 
         this.rdoOpen.AutoSize = true;
         this.rdoOpen.Location = new System.Drawing.Point(17, 20);
         this.rdoOpen.Name = "rdoOpen";
         this.rdoOpen.Size = new System.Drawing.Size(51, 17);
         this.rdoOpen.TabIndex = 0;
         this.rdoOpen.Text = "Open";
         this.rdoOpen.UseVisualStyleBackColor = true;
         this.rdoOpen.CheckedChanged += new System.EventHandler(this.rdoOpen_CheckedChanged);
         // 
         // tabPage9
         // 
         this.tabPage9.Controls.Add(this.groupBox33);
         this.tabPage9.Controls.Add(this.groupBox32);
         this.tabPage9.Controls.Add(this.btnSendBoardSerials);
         this.tabPage9.Controls.Add(this.btnGetAllSerialNumbers);
         this.tabPage9.Controls.Add(this.groupBox26);
         this.tabPage9.Controls.Add(this.groupBox24);
         this.tabPage9.Controls.Add(this.groupBox23);
         this.tabPage9.Controls.Add(this.groupBox22);
         this.tabPage9.Controls.Add(this.groupBox3);
         this.tabPage9.Location = new System.Drawing.Point(4, 22);
         this.tabPage9.Name = "tabPage9";
         this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage9.Size = new System.Drawing.Size(852, 456);
         this.tabPage9.TabIndex = 8;
         this.tabPage9.Text = "Serial Numbers";
         this.tabPage9.UseVisualStyleBackColor = true;
         // 
         // groupBox33
         // 
         this.groupBox33.Controls.Add(this.btnGetMOTSerial);
         this.groupBox33.Controls.Add(this.btnSetMOTSerial);
         this.groupBox33.Controls.Add(this.txtBatterySerial);
         this.groupBox33.Location = new System.Drawing.Point(246, 262);
         this.groupBox33.Name = "groupBox33";
         this.groupBox33.Size = new System.Drawing.Size(200, 95);
         this.groupBox33.TabIndex = 26;
         this.groupBox33.TabStop = false;
         this.groupBox33.Text = "Motor Board Serial Number";
         // 
         // btnGetMOTSerial
         // 
         this.btnGetMOTSerial.Location = new System.Drawing.Point(17, 53);
         this.btnGetMOTSerial.Name = "btnGetMOTSerial";
         this.btnGetMOTSerial.Size = new System.Drawing.Size(75, 23);
         this.btnGetMOTSerial.TabIndex = 6;
         this.btnGetMOTSerial.Text = "Get";
         this.btnGetMOTSerial.UseVisualStyleBackColor = true;
         this.btnGetMOTSerial.Click += new System.EventHandler(this.btnGetMOTSerial_Click);
         // 
         // btnSetMOTSerial
         // 
         this.btnSetMOTSerial.Location = new System.Drawing.Point(107, 53);
         this.btnSetMOTSerial.Name = "btnSetMOTSerial";
         this.btnSetMOTSerial.Size = new System.Drawing.Size(75, 23);
         this.btnSetMOTSerial.TabIndex = 5;
         this.btnSetMOTSerial.Text = "Set";
         this.btnSetMOTSerial.UseVisualStyleBackColor = true;
         // 
         // txtBatterySerial
         // 
         this.txtBatterySerial.Location = new System.Drawing.Point(17, 23);
         this.txtBatterySerial.Name = "txtBatterySerial";
         this.txtBatterySerial.Size = new System.Drawing.Size(100, 20);
         this.txtBatterySerial.TabIndex = 0;
         // 
         // groupBox32
         // 
         this.groupBox32.Controls.Add(this.btnSetMEDSerial);
         this.groupBox32.Controls.Add(this.btnGetMEDSerial);
         this.groupBox32.Controls.Add(this.txtMEDSerial);
         this.groupBox32.Location = new System.Drawing.Point(480, 140);
         this.groupBox32.Name = "groupBox32";
         this.groupBox32.Size = new System.Drawing.Size(200, 100);
         this.groupBox32.TabIndex = 25;
         this.groupBox32.TabStop = false;
         this.groupBox32.Text = "MED Board Serial Number";
         // 
         // btnSetMEDSerial
         // 
         this.btnSetMEDSerial.Enabled = false;
         this.btnSetMEDSerial.Location = new System.Drawing.Point(107, 53);
         this.btnSetMEDSerial.Name = "btnSetMEDSerial";
         this.btnSetMEDSerial.Size = new System.Drawing.Size(75, 23);
         this.btnSetMEDSerial.TabIndex = 4;
         this.btnSetMEDSerial.Text = "Set";
         this.btnSetMEDSerial.UseVisualStyleBackColor = true;
         this.btnSetMEDSerial.Click += new System.EventHandler(this.btnSetMEDSerial_Click);
         // 
         // btnGetMEDSerial
         // 
         this.btnGetMEDSerial.Location = new System.Drawing.Point(17, 53);
         this.btnGetMEDSerial.Name = "btnGetMEDSerial";
         this.btnGetMEDSerial.Size = new System.Drawing.Size(75, 23);
         this.btnGetMEDSerial.TabIndex = 3;
         this.btnGetMEDSerial.Text = "Get";
         this.btnGetMEDSerial.UseVisualStyleBackColor = true;
         this.btnGetMEDSerial.Click += new System.EventHandler(this.btnGetMEDSerial_Click);
         // 
         // txtMEDSerial
         // 
         this.txtMEDSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtMEDSerial.Location = new System.Drawing.Point(17, 23);
         this.txtMEDSerial.Name = "txtMEDSerial";
         this.txtMEDSerial.Size = new System.Drawing.Size(100, 20);
         this.txtMEDSerial.TabIndex = 0;
         // 
         // btnSendBoardSerials
         // 
         this.btnSendBoardSerials.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnSendBoardSerials.Location = new System.Drawing.Point(683, 427);
         this.btnSendBoardSerials.Name = "btnSendBoardSerials";
         this.btnSendBoardSerials.Size = new System.Drawing.Size(163, 23);
         this.btnSendBoardSerials.TabIndex = 24;
         this.btnSendBoardSerials.Text = "Send Board Serials";
         this.btnSendBoardSerials.UseVisualStyleBackColor = true;
         this.btnSendBoardSerials.Click += new System.EventHandler(this.btnSendBoardSerials_Click);
         // 
         // btnGetAllSerialNumbers
         // 
         this.btnGetAllSerialNumbers.Location = new System.Drawing.Point(18, 334);
         this.btnGetAllSerialNumbers.Name = "btnGetAllSerialNumbers";
         this.btnGetAllSerialNumbers.Size = new System.Drawing.Size(159, 23);
         this.btnGetAllSerialNumbers.TabIndex = 10;
         this.btnGetAllSerialNumbers.Text = "Get All Serial Numbers";
         this.btnGetAllSerialNumbers.UseVisualStyleBackColor = true;
         this.btnGetAllSerialNumbers.Click += new System.EventHandler(this.btnGetAllSerialNumbers_Click);
         // 
         // groupBox26
         // 
         this.groupBox26.Controls.Add(this.btnSetDC2Serial);
         this.groupBox26.Controls.Add(this.btnGetDC2Serial);
         this.groupBox26.Controls.Add(this.txtDC2Serial);
         this.groupBox26.Location = new System.Drawing.Point(246, 140);
         this.groupBox26.Name = "groupBox26";
         this.groupBox26.Size = new System.Drawing.Size(200, 100);
         this.groupBox26.TabIndex = 9;
         this.groupBox26.TabStop = false;
         this.groupBox26.Text = "DC 2 Board Serial Number";
         // 
         // btnSetDC2Serial
         // 
         this.btnSetDC2Serial.Enabled = false;
         this.btnSetDC2Serial.Location = new System.Drawing.Point(107, 53);
         this.btnSetDC2Serial.Name = "btnSetDC2Serial";
         this.btnSetDC2Serial.Size = new System.Drawing.Size(75, 23);
         this.btnSetDC2Serial.TabIndex = 4;
         this.btnSetDC2Serial.Text = "Set";
         this.btnSetDC2Serial.UseVisualStyleBackColor = true;
         this.btnSetDC2Serial.Click += new System.EventHandler(this.btnSetDC2Serial_Click);
         // 
         // btnGetDC2Serial
         // 
         this.btnGetDC2Serial.Location = new System.Drawing.Point(17, 53);
         this.btnGetDC2Serial.Name = "btnGetDC2Serial";
         this.btnGetDC2Serial.Size = new System.Drawing.Size(75, 23);
         this.btnGetDC2Serial.TabIndex = 3;
         this.btnGetDC2Serial.Text = "Get";
         this.btnGetDC2Serial.UseVisualStyleBackColor = true;
         this.btnGetDC2Serial.Click += new System.EventHandler(this.btnGetDC2Serial_Click);
         // 
         // txtDC2Serial
         // 
         this.txtDC2Serial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtDC2Serial.Location = new System.Drawing.Point(17, 23);
         this.txtDC2Serial.Name = "txtDC2Serial";
         this.txtDC2Serial.Size = new System.Drawing.Size(100, 20);
         this.txtDC2Serial.TabIndex = 0;
         // 
         // groupBox24
         // 
         this.groupBox24.Controls.Add(this.btnSetLCDSerial);
         this.groupBox24.Controls.Add(this.btnGetLCDSerial);
         this.groupBox24.Controls.Add(this.txtLCDSerial);
         this.groupBox24.Location = new System.Drawing.Point(18, 140);
         this.groupBox24.Name = "groupBox24";
         this.groupBox24.Size = new System.Drawing.Size(200, 100);
         this.groupBox24.TabIndex = 7;
         this.groupBox24.TabStop = false;
         this.groupBox24.Text = "LCD Board Serial Number";
         // 
         // btnSetLCDSerial
         // 
         this.btnSetLCDSerial.Enabled = false;
         this.btnSetLCDSerial.Location = new System.Drawing.Point(107, 53);
         this.btnSetLCDSerial.Name = "btnSetLCDSerial";
         this.btnSetLCDSerial.Size = new System.Drawing.Size(75, 23);
         this.btnSetLCDSerial.TabIndex = 4;
         this.btnSetLCDSerial.Text = "Set";
         this.btnSetLCDSerial.UseVisualStyleBackColor = true;
         this.btnSetLCDSerial.Click += new System.EventHandler(this.btnSetLCDSerial_Click);
         // 
         // btnGetLCDSerial
         // 
         this.btnGetLCDSerial.Location = new System.Drawing.Point(17, 53);
         this.btnGetLCDSerial.Name = "btnGetLCDSerial";
         this.btnGetLCDSerial.Size = new System.Drawing.Size(75, 23);
         this.btnGetLCDSerial.TabIndex = 3;
         this.btnGetLCDSerial.Text = "Get";
         this.btnGetLCDSerial.UseVisualStyleBackColor = true;
         this.btnGetLCDSerial.Click += new System.EventHandler(this.btnGetLCDSerial_Click);
         // 
         // txtLCDSerial
         // 
         this.txtLCDSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtLCDSerial.Location = new System.Drawing.Point(17, 23);
         this.txtLCDSerial.Name = "txtLCDSerial";
         this.txtLCDSerial.Size = new System.Drawing.Size(100, 20);
         this.txtLCDSerial.TabIndex = 0;
         // 
         // groupBox23
         // 
         this.groupBox23.Controls.Add(this.btnSetHCSerial);
         this.groupBox23.Controls.Add(this.btnGetHCSerial);
         this.groupBox23.Controls.Add(this.txtHCSerial);
         this.groupBox23.Location = new System.Drawing.Point(477, 17);
         this.groupBox23.Name = "groupBox23";
         this.groupBox23.Size = new System.Drawing.Size(200, 100);
         this.groupBox23.TabIndex = 6;
         this.groupBox23.TabStop = false;
         this.groupBox23.Text = "Holster Board Serial Number";
         // 
         // btnSetHCSerial
         // 
         this.btnSetHCSerial.Enabled = false;
         this.btnSetHCSerial.Location = new System.Drawing.Point(107, 53);
         this.btnSetHCSerial.Name = "btnSetHCSerial";
         this.btnSetHCSerial.Size = new System.Drawing.Size(75, 23);
         this.btnSetHCSerial.TabIndex = 4;
         this.btnSetHCSerial.Text = "Set";
         this.btnSetHCSerial.UseVisualStyleBackColor = true;
         this.btnSetHCSerial.Click += new System.EventHandler(this.btnSetHCSerial_Click);
         // 
         // btnGetHCSerial
         // 
         this.btnGetHCSerial.Location = new System.Drawing.Point(17, 53);
         this.btnGetHCSerial.Name = "btnGetHCSerial";
         this.btnGetHCSerial.Size = new System.Drawing.Size(75, 23);
         this.btnGetHCSerial.TabIndex = 3;
         this.btnGetHCSerial.Text = "Get";
         this.btnGetHCSerial.UseVisualStyleBackColor = true;
         this.btnGetHCSerial.Click += new System.EventHandler(this.btnGetHCSerial_Click);
         // 
         // txtHCSerial
         // 
         this.txtHCSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtHCSerial.Location = new System.Drawing.Point(17, 23);
         this.txtHCSerial.Name = "txtHCSerial";
         this.txtHCSerial.Size = new System.Drawing.Size(100, 20);
         this.txtHCSerial.TabIndex = 0;
         // 
         // groupBox22
         // 
         this.groupBox22.Controls.Add(this.btnSetCBSerial);
         this.groupBox22.Controls.Add(this.btnGetCBSerial);
         this.groupBox22.Controls.Add(this.txtCBSerial);
         this.groupBox22.Location = new System.Drawing.Point(246, 17);
         this.groupBox22.Name = "groupBox22";
         this.groupBox22.Size = new System.Drawing.Size(200, 100);
         this.groupBox22.TabIndex = 5;
         this.groupBox22.TabStop = false;
         this.groupBox22.Text = "Control Board Serial Number";
         // 
         // btnSetCBSerial
         // 
         this.btnSetCBSerial.Enabled = false;
         this.btnSetCBSerial.Location = new System.Drawing.Point(107, 53);
         this.btnSetCBSerial.Name = "btnSetCBSerial";
         this.btnSetCBSerial.Size = new System.Drawing.Size(75, 23);
         this.btnSetCBSerial.TabIndex = 4;
         this.btnSetCBSerial.Text = "Set";
         this.btnSetCBSerial.UseVisualStyleBackColor = true;
         this.btnSetCBSerial.Click += new System.EventHandler(this.btnSetCBSerial_Click);
         // 
         // btnGetCBSerial
         // 
         this.btnGetCBSerial.Location = new System.Drawing.Point(17, 53);
         this.btnGetCBSerial.Name = "btnGetCBSerial";
         this.btnGetCBSerial.Size = new System.Drawing.Size(75, 23);
         this.btnGetCBSerial.TabIndex = 3;
         this.btnGetCBSerial.Text = "Get";
         this.btnGetCBSerial.UseVisualStyleBackColor = true;
         this.btnGetCBSerial.Click += new System.EventHandler(this.btnGetCBSerial_Click);
         // 
         // txtCBSerial
         // 
         this.txtCBSerial.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
         this.txtCBSerial.Location = new System.Drawing.Point(17, 23);
         this.txtCBSerial.Name = "txtCBSerial";
         this.txtCBSerial.Size = new System.Drawing.Size(100, 20);
         this.txtCBSerial.TabIndex = 0;
         // 
         // groupBox3
         // 
         this.groupBox3.Controls.Add(this.btnWSSNSet);
         this.groupBox3.Controls.Add(this.btnWSSNGet);
         this.groupBox3.Controls.Add(this.txtWSSerialNumber);
         this.groupBox3.Location = new System.Drawing.Point(18, 17);
         this.groupBox3.Name = "groupBox3";
         this.groupBox3.Size = new System.Drawing.Size(200, 100);
         this.groupBox3.TabIndex = 4;
         this.groupBox3.TabStop = false;
         this.groupBox3.Text = "Workstation Serial Number";
         // 
         // btnWSSNSet
         // 
         this.btnWSSNSet.Enabled = false;
         this.btnWSSNSet.Location = new System.Drawing.Point(107, 53);
         this.btnWSSNSet.Name = "btnWSSNSet";
         this.btnWSSNSet.Size = new System.Drawing.Size(75, 23);
         this.btnWSSNSet.TabIndex = 2;
         this.btnWSSNSet.Text = "Set";
         this.btnWSSNSet.UseVisualStyleBackColor = true;
         this.btnWSSNSet.Click += new System.EventHandler(this.btnWSSNSet_Click);
         // 
         // btnWSSNGet
         // 
         this.btnWSSNGet.Location = new System.Drawing.Point(17, 53);
         this.btnWSSNGet.Name = "btnWSSNGet";
         this.btnWSSNGet.Size = new System.Drawing.Size(75, 23);
         this.btnWSSNGet.TabIndex = 1;
         this.btnWSSNGet.Text = "Get";
         this.btnWSSNGet.UseVisualStyleBackColor = true;
         this.btnWSSNGet.Click += new System.EventHandler(this.btnWSSNGet_Click);
         // 
         // txtWSSerialNumber
         // 
         this.txtWSSerialNumber.Location = new System.Drawing.Point(17, 23);
         this.txtWSSerialNumber.Name = "txtWSSerialNumber";
         this.txtWSSerialNumber.Size = new System.Drawing.Size(158, 20);
         this.txtWSSerialNumber.TabIndex = 0;
         // 
         // tabPage11
         // 
         this.tabPage11.Controls.Add(this.btnReleaseCassette);
         this.tabPage11.Controls.Add(this.groupBox37);
         this.tabPage11.Controls.Add(this.txtDrawerCount);
         this.tabPage11.Controls.Add(this.btnDrawerCount);
         this.tabPage11.Controls.Add(this.txtMotorDnCurrent);
         this.tabPage11.Controls.Add(this.txtMotorUpCurrent);
         this.tabPage11.Controls.Add(this.btnMotorDnCurrent);
         this.tabPage11.Controls.Add(this.btnMotorUpCurrent);
         this.tabPage11.Controls.Add(this.btnZeroLockCount);
         this.tabPage11.Controls.Add(this.groupBox31);
         this.tabPage11.Controls.Add(this.txtUnlockCount);
         this.tabPage11.Controls.Add(this.btnUnlockCount);
         this.tabPage11.Controls.Add(this.btnLockDrawers);
         this.tabPage11.Controls.Add(this.btnUnlockDrawers);
         this.tabPage11.Controls.Add(this.groupBox30);
         this.tabPage11.Controls.Add(this.txtKCFirmware);
         this.tabPage11.Controls.Add(this.btnKCFirmware);
         this.tabPage11.Location = new System.Drawing.Point(4, 22);
         this.tabPage11.Name = "tabPage11";
         this.tabPage11.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage11.Size = new System.Drawing.Size(852, 456);
         this.tabPage11.TabIndex = 10;
         this.tabPage11.Text = "Med Board";
         this.tabPage11.UseVisualStyleBackColor = true;
         // 
         // btnReleaseCassette
         // 
         this.btnReleaseCassette.Location = new System.Drawing.Point(327, 230);
         this.btnReleaseCassette.Name = "btnReleaseCassette";
         this.btnReleaseCassette.Size = new System.Drawing.Size(122, 23);
         this.btnReleaseCassette.TabIndex = 20;
         this.btnReleaseCassette.Text = "Release Cassette";
         this.btnReleaseCassette.UseVisualStyleBackColor = true;
         this.btnReleaseCassette.Click += new System.EventHandler(this.btnReleaseCassette_Click);
         // 
         // groupBox37
         // 
         this.groupBox37.Controls.Add(this.txtIsDrawerIn);
         this.groupBox37.Controls.Add(this.txtIsDrawerOut);
         this.groupBox37.Controls.Add(this.btnIsDrawerIn);
         this.groupBox37.Controls.Add(this.btnIsDrawerOut);
         this.groupBox37.Controls.Add(this.btnMotorMove);
         this.groupBox37.Controls.Add(this.rdoMotor8);
         this.groupBox37.Controls.Add(this.rdoMotor7);
         this.groupBox37.Controls.Add(this.rdoMotor6);
         this.groupBox37.Controls.Add(this.rdoMotor5);
         this.groupBox37.Controls.Add(this.rdoMotor4);
         this.groupBox37.Controls.Add(this.rdoMotor3);
         this.groupBox37.Controls.Add(this.rdoMotor2);
         this.groupBox37.Controls.Add(this.rdoMotor1);
         this.groupBox37.Controls.Add(this.btnMotorIn);
         this.groupBox37.Controls.Add(this.btnMotorOut);
         this.groupBox37.Location = new System.Drawing.Point(237, 21);
         this.groupBox37.Name = "groupBox37";
         this.groupBox37.Size = new System.Drawing.Size(212, 183);
         this.groupBox37.TabIndex = 19;
         this.groupBox37.TabStop = false;
         this.groupBox37.Text = "Motor";
         // 
         // btnMotorMove
         // 
         this.btnMotorMove.Location = new System.Drawing.Point(118, 19);
         this.btnMotorMove.Name = "btnMotorMove";
         this.btnMotorMove.Size = new System.Drawing.Size(75, 23);
         this.btnMotorMove.TabIndex = 10;
         this.btnMotorMove.Text = "Move";
         this.btnMotorMove.UseVisualStyleBackColor = true;
         this.btnMotorMove.Click += new System.EventHandler(this.btnMotorMove_Click);
         // 
         // rdoMotor8
         // 
         this.rdoMotor8.AutoSize = true;
         this.rdoMotor8.Location = new System.Drawing.Point(67, 99);
         this.rdoMotor8.Name = "rdoMotor8";
         this.rdoMotor8.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor8.TabIndex = 9;
         this.rdoMotor8.TabStop = true;
         this.rdoMotor8.Text = "8";
         this.rdoMotor8.UseVisualStyleBackColor = true;
         // 
         // rdoMotor7
         // 
         this.rdoMotor7.AutoSize = true;
         this.rdoMotor7.Location = new System.Drawing.Point(17, 99);
         this.rdoMotor7.Name = "rdoMotor7";
         this.rdoMotor7.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor7.TabIndex = 8;
         this.rdoMotor7.TabStop = true;
         this.rdoMotor7.Text = "7";
         this.rdoMotor7.UseVisualStyleBackColor = true;
         // 
         // rdoMotor6
         // 
         this.rdoMotor6.AutoSize = true;
         this.rdoMotor6.Location = new System.Drawing.Point(67, 73);
         this.rdoMotor6.Name = "rdoMotor6";
         this.rdoMotor6.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor6.TabIndex = 7;
         this.rdoMotor6.TabStop = true;
         this.rdoMotor6.Text = "6";
         this.rdoMotor6.UseVisualStyleBackColor = true;
         // 
         // rdoMotor5
         // 
         this.rdoMotor5.AutoSize = true;
         this.rdoMotor5.Location = new System.Drawing.Point(17, 73);
         this.rdoMotor5.Name = "rdoMotor5";
         this.rdoMotor5.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor5.TabIndex = 6;
         this.rdoMotor5.TabStop = true;
         this.rdoMotor5.Text = "5";
         this.rdoMotor5.UseVisualStyleBackColor = true;
         // 
         // rdoMotor4
         // 
         this.rdoMotor4.AutoSize = true;
         this.rdoMotor4.Location = new System.Drawing.Point(67, 46);
         this.rdoMotor4.Name = "rdoMotor4";
         this.rdoMotor4.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor4.TabIndex = 5;
         this.rdoMotor4.TabStop = true;
         this.rdoMotor4.Text = "4";
         this.rdoMotor4.UseVisualStyleBackColor = true;
         // 
         // rdoMotor3
         // 
         this.rdoMotor3.AutoSize = true;
         this.rdoMotor3.Location = new System.Drawing.Point(17, 46);
         this.rdoMotor3.Name = "rdoMotor3";
         this.rdoMotor3.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor3.TabIndex = 4;
         this.rdoMotor3.TabStop = true;
         this.rdoMotor3.Text = "3";
         this.rdoMotor3.UseVisualStyleBackColor = true;
         // 
         // rdoMotor2
         // 
         this.rdoMotor2.AutoSize = true;
         this.rdoMotor2.Location = new System.Drawing.Point(67, 22);
         this.rdoMotor2.Name = "rdoMotor2";
         this.rdoMotor2.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor2.TabIndex = 3;
         this.rdoMotor2.TabStop = true;
         this.rdoMotor2.Text = "2";
         this.rdoMotor2.UseVisualStyleBackColor = true;
         // 
         // rdoMotor1
         // 
         this.rdoMotor1.AutoSize = true;
         this.rdoMotor1.Checked = true;
         this.rdoMotor1.Location = new System.Drawing.Point(17, 22);
         this.rdoMotor1.Name = "rdoMotor1";
         this.rdoMotor1.Size = new System.Drawing.Size(31, 17);
         this.rdoMotor1.TabIndex = 2;
         this.rdoMotor1.TabStop = true;
         this.rdoMotor1.Text = "1";
         this.rdoMotor1.UseVisualStyleBackColor = true;
         // 
         // btnMotorIn
         // 
         this.btnMotorIn.Location = new System.Drawing.Point(118, 91);
         this.btnMotorIn.Name = "btnMotorIn";
         this.btnMotorIn.Size = new System.Drawing.Size(75, 23);
         this.btnMotorIn.TabIndex = 1;
         this.btnMotorIn.Text = "In";
         this.btnMotorIn.UseVisualStyleBackColor = true;
         this.btnMotorIn.Click += new System.EventHandler(this.btnMotorIn_Click);
         // 
         // btnMotorOut
         // 
         this.btnMotorOut.Location = new System.Drawing.Point(118, 55);
         this.btnMotorOut.Name = "btnMotorOut";
         this.btnMotorOut.Size = new System.Drawing.Size(75, 23);
         this.btnMotorOut.TabIndex = 0;
         this.btnMotorOut.Text = "Out";
         this.btnMotorOut.UseVisualStyleBackColor = true;
         this.btnMotorOut.Click += new System.EventHandler(this.btnMotorOut_Click);
         // 
         // txtDrawerCount
         // 
         this.txtDrawerCount.Location = new System.Drawing.Point(144, 302);
         this.txtDrawerCount.Name = "txtDrawerCount";
         this.txtDrawerCount.Size = new System.Drawing.Size(100, 20);
         this.txtDrawerCount.TabIndex = 18;
         // 
         // btnDrawerCount
         // 
         this.btnDrawerCount.Location = new System.Drawing.Point(21, 301);
         this.btnDrawerCount.Name = "btnDrawerCount";
         this.btnDrawerCount.Size = new System.Drawing.Size(120, 23);
         this.btnDrawerCount.TabIndex = 17;
         this.btnDrawerCount.Text = "Drawer Count";
         this.btnDrawerCount.UseVisualStyleBackColor = true;
         this.btnDrawerCount.Click += new System.EventHandler(this.btnDrawerCount_Click);
         // 
         // txtMotorDnCurrent
         // 
         this.txtMotorDnCurrent.Location = new System.Drawing.Point(144, 259);
         this.txtMotorDnCurrent.Name = "txtMotorDnCurrent";
         this.txtMotorDnCurrent.Size = new System.Drawing.Size(100, 20);
         this.txtMotorDnCurrent.TabIndex = 16;
         // 
         // txtMotorUpCurrent
         // 
         this.txtMotorUpCurrent.Location = new System.Drawing.Point(144, 230);
         this.txtMotorUpCurrent.Name = "txtMotorUpCurrent";
         this.txtMotorUpCurrent.Size = new System.Drawing.Size(100, 20);
         this.txtMotorUpCurrent.TabIndex = 15;
         // 
         // btnMotorDnCurrent
         // 
         this.btnMotorDnCurrent.Location = new System.Drawing.Point(21, 258);
         this.btnMotorDnCurrent.Name = "btnMotorDnCurrent";
         this.btnMotorDnCurrent.Size = new System.Drawing.Size(120, 23);
         this.btnMotorDnCurrent.TabIndex = 14;
         this.btnMotorDnCurrent.Text = "Motor Dn Current";
         this.btnMotorDnCurrent.UseVisualStyleBackColor = true;
         this.btnMotorDnCurrent.Click += new System.EventHandler(this.btnMotorDnCurrent_Click);
         // 
         // btnMotorUpCurrent
         // 
         this.btnMotorUpCurrent.Location = new System.Drawing.Point(21, 229);
         this.btnMotorUpCurrent.Name = "btnMotorUpCurrent";
         this.btnMotorUpCurrent.Size = new System.Drawing.Size(120, 23);
         this.btnMotorUpCurrent.TabIndex = 13;
         this.btnMotorUpCurrent.Text = "Motor Up Current";
         this.btnMotorUpCurrent.UseVisualStyleBackColor = true;
         this.btnMotorUpCurrent.Click += new System.EventHandler(this.btnMotorUpCurrent_Click);
         // 
         // btnZeroLockCount
         // 
         this.btnZeroLockCount.Enabled = false;
         this.btnZeroLockCount.Location = new System.Drawing.Point(247, 412);
         this.btnZeroLockCount.Name = "btnZeroLockCount";
         this.btnZeroLockCount.Size = new System.Drawing.Size(46, 23);
         this.btnZeroLockCount.TabIndex = 12;
         this.btnZeroLockCount.Text = "Zero";
         this.btnZeroLockCount.UseVisualStyleBackColor = true;
         this.btnZeroLockCount.Click += new System.EventHandler(this.btnZeroLockCount_Click);
         // 
         // groupBox31
         // 
         this.groupBox31.Controls.Add(this.label151);
         this.groupBox31.Controls.Add(this.txtUserPin6);
         this.groupBox31.Controls.Add(this.btnUserPin6);
         this.groupBox31.Controls.Add(this.label94);
         this.groupBox31.Controls.Add(this.label93);
         this.groupBox31.Controls.Add(this.label92);
         this.groupBox31.Controls.Add(this.label91);
         this.groupBox31.Controls.Add(this.label90);
         this.groupBox31.Controls.Add(this.btnSetNarcPin);
         this.groupBox31.Controls.Add(this.btnSetAdminPin);
         this.groupBox31.Controls.Add(this.txtUserPin5);
         this.groupBox31.Controls.Add(this.txtUserPin4);
         this.groupBox31.Controls.Add(this.txtUserPin3);
         this.groupBox31.Controls.Add(this.txtUserPin2);
         this.groupBox31.Controls.Add(this.txtUserPin1);
         this.groupBox31.Controls.Add(this.btnUserPin5);
         this.groupBox31.Controls.Add(this.btnUserPin4);
         this.groupBox31.Controls.Add(this.btnUserPin3);
         this.groupBox31.Controls.Add(this.btnUserPin2);
         this.groupBox31.Controls.Add(this.btnUserPin1);
         this.groupBox31.Controls.Add(this.btnGetAdminPin);
         this.groupBox31.Controls.Add(this.txtNarcPin);
         this.groupBox31.Controls.Add(this.btnGetNarcPin);
         this.groupBox31.Controls.Add(this.txtAdminPin);
         this.groupBox31.Location = new System.Drawing.Point(500, 17);
         this.groupBox31.Name = "groupBox31";
         this.groupBox31.Size = new System.Drawing.Size(346, 424);
         this.groupBox31.TabIndex = 11;
         this.groupBox31.TabStop = false;
         this.groupBox31.Text = "Pin Numbers";
         // 
         // label151
         // 
         this.label151.AutoSize = true;
         this.label151.Location = new System.Drawing.Point(63, 385);
         this.label151.Name = "label151";
         this.label151.Size = new System.Drawing.Size(63, 13);
         this.label151.TabIndex = 30;
         this.label151.Text = "Pins 51 - 60";
         // 
         // txtUserPin6
         // 
         this.txtUserPin6.Location = new System.Drawing.Point(134, 361);
         this.txtUserPin6.Multiline = true;
         this.txtUserPin6.Name = "txtUserPin6";
         this.txtUserPin6.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin6.TabIndex = 29;
         // 
         // btnUserPin6
         // 
         this.btnUserPin6.Location = new System.Drawing.Point(10, 360);
         this.btnUserPin6.Name = "btnUserPin6";
         this.btnUserPin6.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin6.TabIndex = 28;
         this.btnUserPin6.Text = "Get User Pin 6";
         this.btnUserPin6.UseVisualStyleBackColor = true;
         this.btnUserPin6.Click += new System.EventHandler(this.btnUserPin6_Click);
         // 
         // label94
         // 
         this.label94.AutoSize = true;
         this.label94.Location = new System.Drawing.Point(63, 329);
         this.label94.Name = "label94";
         this.label94.Size = new System.Drawing.Size(63, 13);
         this.label94.TabIndex = 27;
         this.label94.Text = "Pins 41 - 50";
         // 
         // label93
         // 
         this.label93.AutoSize = true;
         this.label93.Location = new System.Drawing.Point(63, 274);
         this.label93.Name = "label93";
         this.label93.Size = new System.Drawing.Size(63, 13);
         this.label93.TabIndex = 26;
         this.label93.Text = "Pins 31 - 40";
         // 
         // label92
         // 
         this.label92.AutoSize = true;
         this.label92.Location = new System.Drawing.Point(63, 217);
         this.label92.Name = "label92";
         this.label92.Size = new System.Drawing.Size(63, 13);
         this.label92.TabIndex = 25;
         this.label92.Text = "Pins 21 - 30";
         // 
         // label91
         // 
         this.label91.AutoSize = true;
         this.label91.Location = new System.Drawing.Point(63, 162);
         this.label91.Name = "label91";
         this.label91.Size = new System.Drawing.Size(63, 13);
         this.label91.TabIndex = 24;
         this.label91.Text = "Pins 11 - 20";
         // 
         // label90
         // 
         this.label90.AutoSize = true;
         this.label90.Location = new System.Drawing.Point(69, 105);
         this.label90.Name = "label90";
         this.label90.Size = new System.Drawing.Size(57, 13);
         this.label90.TabIndex = 23;
         this.label90.Text = "Pins 1 - 10";
         // 
         // btnSetNarcPin
         // 
         this.btnSetNarcPin.Enabled = false;
         this.btnSetNarcPin.Location = new System.Drawing.Point(290, 51);
         this.btnSetNarcPin.Name = "btnSetNarcPin";
         this.btnSetNarcPin.Size = new System.Drawing.Size(40, 23);
         this.btnSetNarcPin.TabIndex = 22;
         this.btnSetNarcPin.Text = "Set";
         this.btnSetNarcPin.UseVisualStyleBackColor = true;
         this.btnSetNarcPin.Click += new System.EventHandler(this.btnSetNarcPin_Click);
         // 
         // btnSetAdminPin
         // 
         this.btnSetAdminPin.Enabled = false;
         this.btnSetAdminPin.Location = new System.Drawing.Point(290, 22);
         this.btnSetAdminPin.Name = "btnSetAdminPin";
         this.btnSetAdminPin.Size = new System.Drawing.Size(40, 23);
         this.btnSetAdminPin.TabIndex = 21;
         this.btnSetAdminPin.Text = "Set";
         this.btnSetAdminPin.UseVisualStyleBackColor = true;
         this.btnSetAdminPin.Click += new System.EventHandler(this.btnSetAdminPin_Click);
         // 
         // txtUserPin5
         // 
         this.txtUserPin5.Location = new System.Drawing.Point(134, 305);
         this.txtUserPin5.Multiline = true;
         this.txtUserPin5.Name = "txtUserPin5";
         this.txtUserPin5.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin5.TabIndex = 20;
         // 
         // txtUserPin4
         // 
         this.txtUserPin4.Location = new System.Drawing.Point(134, 249);
         this.txtUserPin4.Multiline = true;
         this.txtUserPin4.Name = "txtUserPin4";
         this.txtUserPin4.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin4.TabIndex = 19;
         // 
         // txtUserPin3
         // 
         this.txtUserPin3.Location = new System.Drawing.Point(134, 193);
         this.txtUserPin3.Multiline = true;
         this.txtUserPin3.Name = "txtUserPin3";
         this.txtUserPin3.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin3.TabIndex = 18;
         // 
         // txtUserPin2
         // 
         this.txtUserPin2.Location = new System.Drawing.Point(134, 137);
         this.txtUserPin2.Multiline = true;
         this.txtUserPin2.Name = "txtUserPin2";
         this.txtUserPin2.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin2.TabIndex = 17;
         // 
         // txtUserPin1
         // 
         this.txtUserPin1.Location = new System.Drawing.Point(134, 81);
         this.txtUserPin1.Multiline = true;
         this.txtUserPin1.Name = "txtUserPin1";
         this.txtUserPin1.Size = new System.Drawing.Size(150, 50);
         this.txtUserPin1.TabIndex = 16;
         // 
         // btnUserPin5
         // 
         this.btnUserPin5.Location = new System.Drawing.Point(10, 304);
         this.btnUserPin5.Name = "btnUserPin5";
         this.btnUserPin5.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin5.TabIndex = 15;
         this.btnUserPin5.Text = "Get User Pin 5";
         this.btnUserPin5.UseVisualStyleBackColor = true;
         this.btnUserPin5.Click += new System.EventHandler(this.btnUserPin5_Click);
         // 
         // btnUserPin4
         // 
         this.btnUserPin4.Location = new System.Drawing.Point(10, 249);
         this.btnUserPin4.Name = "btnUserPin4";
         this.btnUserPin4.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin4.TabIndex = 14;
         this.btnUserPin4.Text = "Get User Pin 4";
         this.btnUserPin4.UseVisualStyleBackColor = true;
         this.btnUserPin4.Click += new System.EventHandler(this.btnUserPin4_Click);
         // 
         // btnUserPin3
         // 
         this.btnUserPin3.Location = new System.Drawing.Point(10, 192);
         this.btnUserPin3.Name = "btnUserPin3";
         this.btnUserPin3.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin3.TabIndex = 13;
         this.btnUserPin3.Text = "Get User Pin 3";
         this.btnUserPin3.UseVisualStyleBackColor = true;
         this.btnUserPin3.Click += new System.EventHandler(this.btnUserPin3_Click);
         // 
         // btnUserPin2
         // 
         this.btnUserPin2.Location = new System.Drawing.Point(10, 137);
         this.btnUserPin2.Name = "btnUserPin2";
         this.btnUserPin2.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin2.TabIndex = 12;
         this.btnUserPin2.Text = "Get User Pin 2";
         this.btnUserPin2.UseVisualStyleBackColor = true;
         this.btnUserPin2.Click += new System.EventHandler(this.btnUserPin2_Click);
         // 
         // btnUserPin1
         // 
         this.btnUserPin1.Location = new System.Drawing.Point(10, 80);
         this.btnUserPin1.Name = "btnUserPin1";
         this.btnUserPin1.Size = new System.Drawing.Size(120, 23);
         this.btnUserPin1.TabIndex = 11;
         this.btnUserPin1.Text = "Get User Pin 1";
         this.btnUserPin1.UseVisualStyleBackColor = true;
         this.btnUserPin1.Click += new System.EventHandler(this.btnUserPin1_Click);
         // 
         // btnGetAdminPin
         // 
         this.btnGetAdminPin.Location = new System.Drawing.Point(10, 22);
         this.btnGetAdminPin.Name = "btnGetAdminPin";
         this.btnGetAdminPin.Size = new System.Drawing.Size(120, 23);
         this.btnGetAdminPin.TabIndex = 7;
         this.btnGetAdminPin.Text = "Get Admin Pin";
         this.btnGetAdminPin.UseVisualStyleBackColor = true;
         this.btnGetAdminPin.Click += new System.EventHandler(this.btnGetAdminPin_Click);
         // 
         // txtNarcPin
         // 
         this.txtNarcPin.Location = new System.Drawing.Point(134, 52);
         this.txtNarcPin.Name = "txtNarcPin";
         this.txtNarcPin.Size = new System.Drawing.Size(150, 20);
         this.txtNarcPin.TabIndex = 10;
         // 
         // btnGetNarcPin
         // 
         this.btnGetNarcPin.Location = new System.Drawing.Point(10, 51);
         this.btnGetNarcPin.Name = "btnGetNarcPin";
         this.btnGetNarcPin.Size = new System.Drawing.Size(120, 23);
         this.btnGetNarcPin.TabIndex = 8;
         this.btnGetNarcPin.Text = "Get Narc Pin";
         this.btnGetNarcPin.UseVisualStyleBackColor = true;
         this.btnGetNarcPin.Click += new System.EventHandler(this.btnGetNarcPin_Click);
         // 
         // txtAdminPin
         // 
         this.txtAdminPin.Location = new System.Drawing.Point(134, 23);
         this.txtAdminPin.Name = "txtAdminPin";
         this.txtAdminPin.Size = new System.Drawing.Size(150, 20);
         this.txtAdminPin.TabIndex = 9;
         // 
         // txtUnlockCount
         // 
         this.txtUnlockCount.Location = new System.Drawing.Point(144, 414);
         this.txtUnlockCount.Name = "txtUnlockCount";
         this.txtUnlockCount.Size = new System.Drawing.Size(100, 20);
         this.txtUnlockCount.TabIndex = 6;
         // 
         // btnUnlockCount
         // 
         this.btnUnlockCount.Location = new System.Drawing.Point(21, 412);
         this.btnUnlockCount.Name = "btnUnlockCount";
         this.btnUnlockCount.Size = new System.Drawing.Size(120, 23);
         this.btnUnlockCount.TabIndex = 5;
         this.btnUnlockCount.Text = "Unlock Count";
         this.btnUnlockCount.UseVisualStyleBackColor = true;
         this.btnUnlockCount.Click += new System.EventHandler(this.btnUnlockCount_Click);
         // 
         // btnLockDrawers
         // 
         this.btnLockDrawers.Location = new System.Drawing.Point(21, 375);
         this.btnLockDrawers.Name = "btnLockDrawers";
         this.btnLockDrawers.Size = new System.Drawing.Size(120, 23);
         this.btnLockDrawers.TabIndex = 4;
         this.btnLockDrawers.Text = "Lock Drawers";
         this.btnLockDrawers.UseVisualStyleBackColor = true;
         this.btnLockDrawers.Click += new System.EventHandler(this.btnLockDrawers_Click);
         // 
         // btnUnlockDrawers
         // 
         this.btnUnlockDrawers.Location = new System.Drawing.Point(21, 337);
         this.btnUnlockDrawers.Name = "btnUnlockDrawers";
         this.btnUnlockDrawers.Size = new System.Drawing.Size(120, 23);
         this.btnUnlockDrawers.TabIndex = 3;
         this.btnUnlockDrawers.Text = "Unlock Drawers";
         this.btnUnlockDrawers.UseVisualStyleBackColor = true;
         this.btnUnlockDrawers.Click += new System.EventHandler(this.btnUnlockDrawers_Click);
         // 
         // groupBox30
         // 
         this.groupBox30.Controls.Add(this.btnSetDrawerOpenTime);
         this.groupBox30.Controls.Add(this.btnGetDrawerOpenTime);
         this.groupBox30.Controls.Add(this.label74);
         this.groupBox30.Controls.Add(this.txtDrawerOpenTime);
         this.groupBox30.Location = new System.Drawing.Point(21, 54);
         this.groupBox30.Name = "groupBox30";
         this.groupBox30.Size = new System.Drawing.Size(172, 90);
         this.groupBox30.TabIndex = 2;
         this.groupBox30.TabStop = false;
         this.groupBox30.Text = "Drawer Open Time";
         // 
         // btnSetDrawerOpenTime
         // 
         this.btnSetDrawerOpenTime.Enabled = false;
         this.btnSetDrawerOpenTime.Location = new System.Drawing.Point(114, 53);
         this.btnSetDrawerOpenTime.Name = "btnSetDrawerOpenTime";
         this.btnSetDrawerOpenTime.Size = new System.Drawing.Size(44, 23);
         this.btnSetDrawerOpenTime.TabIndex = 3;
         this.btnSetDrawerOpenTime.Text = "Set";
         this.btnSetDrawerOpenTime.UseVisualStyleBackColor = true;
         this.btnSetDrawerOpenTime.Click += new System.EventHandler(this.btnSetDrawerOpenTime_Click);
         // 
         // btnGetDrawerOpenTime
         // 
         this.btnGetDrawerOpenTime.Location = new System.Drawing.Point(10, 53);
         this.btnGetDrawerOpenTime.Name = "btnGetDrawerOpenTime";
         this.btnGetDrawerOpenTime.Size = new System.Drawing.Size(50, 23);
         this.btnGetDrawerOpenTime.TabIndex = 2;
         this.btnGetDrawerOpenTime.Text = "Get";
         this.btnGetDrawerOpenTime.UseVisualStyleBackColor = true;
         this.btnGetDrawerOpenTime.Click += new System.EventHandler(this.btnGetDrawerOpenTime_Click);
         // 
         // label74
         // 
         this.label74.AutoSize = true;
         this.label74.Location = new System.Drawing.Point(112, 25);
         this.label74.Name = "label74";
         this.label74.Size = new System.Drawing.Size(49, 13);
         this.label74.TabIndex = 1;
         this.label74.Text = "Seconds";
         // 
         // txtDrawerOpenTime
         // 
         this.txtDrawerOpenTime.Location = new System.Drawing.Point(10, 22);
         this.txtDrawerOpenTime.Name = "txtDrawerOpenTime";
         this.txtDrawerOpenTime.Size = new System.Drawing.Size(100, 20);
         this.txtDrawerOpenTime.TabIndex = 0;
         // 
         // txtKCFirmware
         // 
         this.txtKCFirmware.Location = new System.Drawing.Point(110, 8);
         this.txtKCFirmware.Name = "txtKCFirmware";
         this.txtKCFirmware.Size = new System.Drawing.Size(100, 20);
         this.txtKCFirmware.TabIndex = 1;
         // 
         // btnKCFirmware
         // 
         this.btnKCFirmware.Location = new System.Drawing.Point(6, 6);
         this.btnKCFirmware.Name = "btnKCFirmware";
         this.btnKCFirmware.Size = new System.Drawing.Size(100, 23);
         this.btnKCFirmware.TabIndex = 0;
         this.btnKCFirmware.Text = "Firmware";
         this.btnKCFirmware.UseVisualStyleBackColor = true;
         this.btnKCFirmware.Click += new System.EventHandler(this.btnKCFirmware_Click);
         // 
         // tabPage12
         // 
         this.tabPage12.Controls.Add(this.groupBox36);
         this.tabPage12.Controls.Add(this.btnBLMedBoard);
         this.tabPage12.Controls.Add(this.btnBLControl);
         this.tabPage12.Controls.Add(this.btnBLHolster);
         this.tabPage12.Controls.Add(this.btnBLDC);
         this.tabPage12.Controls.Add(this.btnBLLCD);
         this.tabPage12.Controls.Add(this.btnDLMEDHEX);
         this.tabPage12.Controls.Add(this.btnDLCTLHEX);
         this.tabPage12.Controls.Add(this.btnLDHOLHEX);
         this.tabPage12.Controls.Add(this.btnDLDCDCHEX);
         this.tabPage12.Controls.Add(this.btnDLLCDHEX);
         this.tabPage12.Controls.Add(this.groupBox21);
         this.tabPage12.Location = new System.Drawing.Point(4, 22);
         this.tabPage12.Name = "tabPage12";
         this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
         this.tabPage12.Size = new System.Drawing.Size(852, 456);
         this.tabPage12.TabIndex = 11;
         this.tabPage12.Text = "Command Codes";
         this.tabPage12.UseVisualStyleBackColor = true;
         // 
         // groupBox36
         // 
         this.groupBox36.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.groupBox36.Controls.Add(this.txtFTPAddress);
         this.groupBox36.Controls.Add(this.btnSetFTPAddress);
         this.groupBox36.Controls.Add(this.btnGetFTPAddress);
         this.groupBox36.Location = new System.Drawing.Point(618, 323);
         this.groupBox36.Name = "groupBox36";
         this.groupBox36.Size = new System.Drawing.Size(188, 86);
         this.groupBox36.TabIndex = 170;
         this.groupBox36.TabStop = false;
         this.groupBox36.Text = "FTP Address";
         // 
         // txtFTPAddress
         // 
         this.txtFTPAddress.Location = new System.Drawing.Point(14, 20);
         this.txtFTPAddress.Name = "txtFTPAddress";
         this.txtFTPAddress.Size = new System.Drawing.Size(115, 20);
         this.txtFTPAddress.TabIndex = 168;
         // 
         // btnSetFTPAddress
         // 
         this.btnSetFTPAddress.Location = new System.Drawing.Point(128, 48);
         this.btnSetFTPAddress.Name = "btnSetFTPAddress";
         this.btnSetFTPAddress.Size = new System.Drawing.Size(45, 23);
         this.btnSetFTPAddress.TabIndex = 165;
         this.btnSetFTPAddress.Text = "Set";
         this.btnSetFTPAddress.UseVisualStyleBackColor = true;
         this.btnSetFTPAddress.Click += new System.EventHandler(this.btnSetFTPAddress_Click);
         // 
         // btnGetFTPAddress
         // 
         this.btnGetFTPAddress.Location = new System.Drawing.Point(14, 48);
         this.btnGetFTPAddress.Name = "btnGetFTPAddress";
         this.btnGetFTPAddress.Size = new System.Drawing.Size(75, 23);
         this.btnGetFTPAddress.TabIndex = 166;
         this.btnGetFTPAddress.Text = "Get";
         this.btnGetFTPAddress.UseVisualStyleBackColor = true;
         this.btnGetFTPAddress.Click += new System.EventHandler(this.btnGetFTPAddress_Click);
         // 
         // btnBLMedBoard
         // 
         this.btnBLMedBoard.Location = new System.Drawing.Point(551, 169);
         this.btnBLMedBoard.Name = "btnBLMedBoard";
         this.btnBLMedBoard.Size = new System.Drawing.Size(200, 30);
         this.btnBLMedBoard.TabIndex = 27;
         this.btnBLMedBoard.Text = "BootLoad MedBoard";
         this.btnBLMedBoard.UseVisualStyleBackColor = true;
         this.btnBLMedBoard.Click += new System.EventHandler(this.btnBLMedBoard_Click);
         // 
         // btnBLControl
         // 
         this.btnBLControl.Location = new System.Drawing.Point(551, 134);
         this.btnBLControl.Name = "btnBLControl";
         this.btnBLControl.Size = new System.Drawing.Size(200, 30);
         this.btnBLControl.TabIndex = 26;
         this.btnBLControl.Text = "BootLoad Control";
         this.btnBLControl.UseVisualStyleBackColor = true;
         this.btnBLControl.Click += new System.EventHandler(this.btnBLControl_Click);
         // 
         // btnBLHolster
         // 
         this.btnBLHolster.Location = new System.Drawing.Point(551, 99);
         this.btnBLHolster.Name = "btnBLHolster";
         this.btnBLHolster.Size = new System.Drawing.Size(200, 30);
         this.btnBLHolster.TabIndex = 25;
         this.btnBLHolster.Text = "BootLoad Holster";
         this.btnBLHolster.UseVisualStyleBackColor = true;
         this.btnBLHolster.Click += new System.EventHandler(this.btnBLHolster_Click);
         // 
         // btnBLDC
         // 
         this.btnBLDC.Location = new System.Drawing.Point(551, 65);
         this.btnBLDC.Name = "btnBLDC";
         this.btnBLDC.Size = new System.Drawing.Size(200, 30);
         this.btnBLDC.TabIndex = 24;
         this.btnBLDC.Text = "BootLoad DC";
         this.btnBLDC.UseVisualStyleBackColor = true;
         this.btnBLDC.Click += new System.EventHandler(this.btnBLDC_Click);
         // 
         // btnBLLCD
         // 
         this.btnBLLCD.Location = new System.Drawing.Point(551, 29);
         this.btnBLLCD.Name = "btnBLLCD";
         this.btnBLLCD.Size = new System.Drawing.Size(200, 30);
         this.btnBLLCD.TabIndex = 23;
         this.btnBLLCD.Text = "BootLoad LCD";
         this.btnBLLCD.UseVisualStyleBackColor = true;
         this.btnBLLCD.Click += new System.EventHandler(this.btnBLLCD_Click);
         // 
         // btnDLMEDHEX
         // 
         this.btnDLMEDHEX.Location = new System.Drawing.Point(330, 169);
         this.btnDLMEDHEX.Name = "btnDLMEDHEX";
         this.btnDLMEDHEX.Size = new System.Drawing.Size(200, 30);
         this.btnDLMEDHEX.TabIndex = 22;
         this.btnDLMEDHEX.Text = "Download MedBoard Hex File";
         this.btnDLMEDHEX.UseVisualStyleBackColor = true;
         this.btnDLMEDHEX.Click += new System.EventHandler(this.btnDLMEDHEX_Click);
         // 
         // btnDLCTLHEX
         // 
         this.btnDLCTLHEX.Location = new System.Drawing.Point(330, 134);
         this.btnDLCTLHEX.Name = "btnDLCTLHEX";
         this.btnDLCTLHEX.Size = new System.Drawing.Size(200, 30);
         this.btnDLCTLHEX.TabIndex = 21;
         this.btnDLCTLHEX.Text = "Download Control Hex File";
         this.btnDLCTLHEX.UseVisualStyleBackColor = true;
         this.btnDLCTLHEX.Click += new System.EventHandler(this.btnDLCTLHEX_Click);
         // 
         // btnLDHOLHEX
         // 
         this.btnLDHOLHEX.Location = new System.Drawing.Point(330, 99);
         this.btnLDHOLHEX.Name = "btnLDHOLHEX";
         this.btnLDHOLHEX.Size = new System.Drawing.Size(200, 30);
         this.btnLDHOLHEX.TabIndex = 20;
         this.btnLDHOLHEX.Text = "Download Holster Hex File";
         this.btnLDHOLHEX.UseVisualStyleBackColor = true;
         this.btnLDHOLHEX.Click += new System.EventHandler(this.btnLDHOLHEX_Click);
         // 
         // btnDLDCDCHEX
         // 
         this.btnDLDCDCHEX.Location = new System.Drawing.Point(330, 64);
         this.btnDLDCDCHEX.Name = "btnDLDCDCHEX";
         this.btnDLDCDCHEX.Size = new System.Drawing.Size(200, 30);
         this.btnDLDCDCHEX.TabIndex = 19;
         this.btnDLDCDCHEX.Text = "Download DC-DC Hex File";
         this.btnDLDCDCHEX.UseVisualStyleBackColor = true;
         this.btnDLDCDCHEX.Click += new System.EventHandler(this.btnDLDCDCHEX_Click);
         // 
         // btnDLLCDHEX
         // 
         this.btnDLLCDHEX.Location = new System.Drawing.Point(330, 29);
         this.btnDLLCDHEX.Name = "btnDLLCDHEX";
         this.btnDLLCDHEX.Size = new System.Drawing.Size(200, 30);
         this.btnDLLCDHEX.TabIndex = 18;
         this.btnDLLCDHEX.Text = "Download LCD Hex File";
         this.btnDLLCDHEX.UseVisualStyleBackColor = true;
         this.btnDLLCDHEX.Click += new System.EventHandler(this.btnDLLCDHEX_Click);
         // 
         // groupBox21
         // 
         this.groupBox21.Controls.Add(this.btnCodes);
         this.groupBox21.Controls.Add(this.label62);
         this.groupBox21.Controls.Add(this.label61);
         this.groupBox21.Controls.Add(this.txtParameter);
         this.groupBox21.Controls.Add(this.btnSendCommandCode);
         this.groupBox21.Controls.Add(this.txtCommandCode);
         this.groupBox21.Location = new System.Drawing.Point(22, 20);
         this.groupBox21.Name = "groupBox21";
         this.groupBox21.Size = new System.Drawing.Size(252, 106);
         this.groupBox21.TabIndex = 17;
         this.groupBox21.TabStop = false;
         this.groupBox21.Text = "Command Code";
         // 
         // btnCodes
         // 
         this.btnCodes.Location = new System.Drawing.Point(188, 16);
         this.btnCodes.Name = "btnCodes";
         this.btnCodes.Size = new System.Drawing.Size(46, 23);
         this.btnCodes.TabIndex = 19;
         this.btnCodes.Text = "Codes";
         this.btnCodes.UseVisualStyleBackColor = true;
         this.btnCodes.Click += new System.EventHandler(this.btnCodes_Click);
         // 
         // label62
         // 
         this.label62.AutoSize = true;
         this.label62.Location = new System.Drawing.Point(15, 49);
         this.label62.Name = "label62";
         this.label62.Size = new System.Drawing.Size(55, 13);
         this.label62.TabIndex = 18;
         this.label62.Text = "Parameter";
         // 
         // label61
         // 
         this.label61.AutoSize = true;
         this.label61.Location = new System.Drawing.Point(16, 23);
         this.label61.Name = "label61";
         this.label61.Size = new System.Drawing.Size(54, 13);
         this.label61.TabIndex = 17;
         this.label61.Text = "Command";
         // 
         // txtParameter
         // 
         this.txtParameter.Location = new System.Drawing.Point(72, 45);
         this.txtParameter.Name = "txtParameter";
         this.txtParameter.Size = new System.Drawing.Size(162, 20);
         this.txtParameter.TabIndex = 16;
         // 
         // btnSendCommandCode
         // 
         this.btnSendCommandCode.Location = new System.Drawing.Point(72, 73);
         this.btnSendCommandCode.Name = "btnSendCommandCode";
         this.btnSendCommandCode.Size = new System.Drawing.Size(75, 23);
         this.btnSendCommandCode.TabIndex = 14;
         this.btnSendCommandCode.Text = "Send";
         this.btnSendCommandCode.UseVisualStyleBackColor = true;
         this.btnSendCommandCode.Click += new System.EventHandler(this.btnSendCommandCode_Click);
         // 
         // txtCommandCode
         // 
         this.txtCommandCode.Location = new System.Drawing.Point(72, 19);
         this.txtCommandCode.Name = "txtCommandCode";
         this.txtCommandCode.Size = new System.Drawing.Size(43, 20);
         this.txtCommandCode.TabIndex = 15;
         // 
         // btnConnect
         // 
         this.btnConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
         this.btnConnect.Location = new System.Drawing.Point(797, 10);
         this.btnConnect.Name = "btnConnect";
         this.btnConnect.Size = new System.Drawing.Size(75, 23);
         this.btnConnect.TabIndex = 2;
         this.btnConnect.Text = "Connect";
         this.btnConnect.UseVisualStyleBackColor = true;
         this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
         // 
         // txtStatus
         // 
         this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.txtStatus.Location = new System.Drawing.Point(12, 529);
         this.txtStatus.Name = "txtStatus";
         this.txtStatus.ReadOnly = true;
         this.txtStatus.Size = new System.Drawing.Size(292, 20);
         this.txtStatus.TabIndex = 3;
         // 
         // ilsLED
         // 
         this.ilsLED.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilsLED.ImageStream")));
         this.ilsLED.TransparentColor = System.Drawing.Color.Transparent;
         this.ilsLED.Images.SetKeyName(0, "LedWhite.ico");
         this.ilsLED.Images.SetKeyName(1, "LedBlue.ico");
         this.ilsLED.Images.SetKeyName(2, "LedRed.ico");
         this.ilsLED.Images.SetKeyName(3, "LedGreen.ico");
         // 
         // button1
         // 
         this.button1.Location = new System.Drawing.Point(491, 364);
         this.button1.Name = "button1";
         this.button1.Size = new System.Drawing.Size(106, 23);
         this.button1.TabIndex = 95;
         this.button1.Text = "Send AdHoc Data";
         this.button1.UseVisualStyleBackColor = true;
         this.button1.Visible = false;
         // 
         // button2
         // 
         this.button2.Location = new System.Drawing.Point(491, 338);
         this.button2.Name = "button2";
         this.button2.Size = new System.Drawing.Size(106, 23);
         this.button2.TabIndex = 94;
         this.button2.Text = "AdHoc Integer";
         this.button2.UseVisualStyleBackColor = true;
         this.button2.Visible = false;
         // 
         // button3
         // 
         this.button3.Location = new System.Drawing.Point(491, 312);
         this.button3.Name = "button3";
         this.button3.Size = new System.Drawing.Size(106, 23);
         this.button3.TabIndex = 93;
         this.button3.Text = "AdHoc String";
         this.button3.UseVisualStyleBackColor = true;
         this.button3.Visible = false;
         // 
         // groupBox12
         // 
         this.groupBox12.Controls.Add(this.button4);
         this.groupBox12.Controls.Add(this.button5);
         this.groupBox12.Controls.Add(this.button6);
         this.groupBox12.Controls.Add(this.button7);
         this.groupBox12.Controls.Add(this.button8);
         this.groupBox12.Location = new System.Drawing.Point(645, 9);
         this.groupBox12.Name = "groupBox12";
         this.groupBox12.Size = new System.Drawing.Size(134, 158);
         this.groupBox12.TabIndex = 92;
         this.groupBox12.TabStop = false;
         this.groupBox12.Text = "Battery Pack Control";
         // 
         // button4
         // 
         this.button4.Location = new System.Drawing.Point(11, 19);
         this.button4.Name = "button4";
         this.button4.Size = new System.Drawing.Size(87, 23);
         this.button4.TabIndex = 87;
         this.button4.Text = "Unlock Pack";
         this.button4.UseVisualStyleBackColor = true;
         // 
         // button5
         // 
         this.button5.Location = new System.Drawing.Point(11, 124);
         this.button5.Name = "button5";
         this.button5.Size = new System.Drawing.Size(75, 23);
         this.button5.TabIndex = 91;
         this.button5.Text = "Lock Pack";
         this.button5.UseVisualStyleBackColor = true;
         // 
         // button6
         // 
         this.button6.Location = new System.Drawing.Point(11, 45);
         this.button6.Name = "button6";
         this.button6.Size = new System.Drawing.Size(87, 23);
         this.button6.TabIndex = 88;
         this.button6.Text = "Close FETs";
         this.button6.UseVisualStyleBackColor = true;
         // 
         // button7
         // 
         this.button7.Location = new System.Drawing.Point(11, 98);
         this.button7.Name = "button7";
         this.button7.Size = new System.Drawing.Size(75, 23);
         this.button7.TabIndex = 90;
         this.button7.Text = "Reset Pack";
         this.button7.UseVisualStyleBackColor = true;
         // 
         // button8
         // 
         this.button8.Location = new System.Drawing.Point(11, 71);
         this.button8.Name = "button8";
         this.button8.Size = new System.Drawing.Size(105, 23);
         this.button8.TabIndex = 89;
         this.button8.Text = "Reset Perm Failure";
         this.button8.UseVisualStyleBackColor = true;
         // 
         // button9
         // 
         this.button9.Location = new System.Drawing.Point(495, 247);
         this.button9.Name = "button9";
         this.button9.Size = new System.Drawing.Size(100, 23);
         this.button9.TabIndex = 85;
         this.button9.Text = "Main Battery Out";
         this.button9.UseVisualStyleBackColor = true;
         // 
         // groupBox13
         // 
         this.groupBox13.Controls.Add(this.checkBox1);
         this.groupBox13.Controls.Add(this.button10);
         this.groupBox13.Controls.Add(this.button11);
         this.groupBox13.Controls.Add(this.button12);
         this.groupBox13.Controls.Add(this.checkBox2);
         this.groupBox13.Controls.Add(this.button13);
         this.groupBox13.Controls.Add(this.button14);
         this.groupBox13.Location = new System.Drawing.Point(276, 243);
         this.groupBox13.Name = "groupBox13";
         this.groupBox13.Size = new System.Drawing.Size(213, 145);
         this.groupBox13.TabIndex = 82;
         this.groupBox13.TabStop = false;
         this.groupBox13.Text = "Charger";
         // 
         // checkBox1
         // 
         this.checkBox1.AutoSize = true;
         this.checkBox1.Location = new System.Drawing.Point(114, 94);
         this.checkBox1.Name = "checkBox1";
         this.checkBox1.Size = new System.Drawing.Size(95, 17);
         this.checkBox1.TabIndex = 7;
         this.checkBox1.Text = "AC Connected";
         this.checkBox1.UseVisualStyleBackColor = true;
         // 
         // button10
         // 
         this.button10.Location = new System.Drawing.Point(6, 91);
         this.button10.Name = "button10";
         this.button10.Size = new System.Drawing.Size(105, 23);
         this.button10.TabIndex = 3;
         this.button10.Text = "AC Connected";
         this.button10.UseVisualStyleBackColor = true;
         // 
         // button11
         // 
         this.button11.Location = new System.Drawing.Point(7, 67);
         this.button11.Name = "button11";
         this.button11.Size = new System.Drawing.Size(105, 23);
         this.button11.TabIndex = 2;
         this.button11.Text = "Charger Current";
         this.button11.UseVisualStyleBackColor = true;
         // 
         // button12
         // 
         this.button12.Location = new System.Drawing.Point(7, 43);
         this.button12.Name = "button12";
         this.button12.Size = new System.Drawing.Size(105, 23);
         this.button12.TabIndex = 1;
         this.button12.Text = "Charger Voltage";
         this.button12.UseVisualStyleBackColor = true;
         // 
         // checkBox2
         // 
         this.checkBox2.AutoSize = true;
         this.checkBox2.Location = new System.Drawing.Point(114, 118);
         this.checkBox2.Name = "checkBox2";
         this.checkBox2.Size = new System.Drawing.Size(96, 17);
         this.checkBox2.TabIndex = 84;
         this.checkBox2.Text = "Charger is OFF";
         this.checkBox2.UseVisualStyleBackColor = true;
         // 
         // button13
         // 
         this.button13.Location = new System.Drawing.Point(7, 19);
         this.button13.Name = "button13";
         this.button13.Size = new System.Drawing.Size(105, 23);
         this.button13.TabIndex = 0;
         this.button13.Text = "Charger Status";
         this.button13.UseVisualStyleBackColor = true;
         // 
         // button14
         // 
         this.button14.Location = new System.Drawing.Point(6, 115);
         this.button14.Name = "button14";
         this.button14.Size = new System.Drawing.Size(105, 23);
         this.button14.TabIndex = 83;
         this.button14.Text = "Turn Charger Off";
         this.button14.UseVisualStyleBackColor = true;
         // 
         // button15
         // 
         this.button15.Location = new System.Drawing.Point(402, 216);
         this.button15.Name = "button15";
         this.button15.Size = new System.Drawing.Size(105, 23);
         this.button15.TabIndex = 78;
         this.button15.Text = "Battery Mode";
         this.button15.UseVisualStyleBackColor = true;
         // 
         // button16
         // 
         this.button16.Location = new System.Drawing.Point(402, 190);
         this.button16.Name = "button16";
         this.button16.Size = new System.Drawing.Size(105, 23);
         this.button16.TabIndex = 77;
         this.button16.Text = "Charging Status";
         this.button16.UseVisualStyleBackColor = true;
         // 
         // button17
         // 
         this.button17.Location = new System.Drawing.Point(402, 164);
         this.button17.Name = "button17";
         this.button17.Size = new System.Drawing.Size(105, 23);
         this.button17.TabIndex = 76;
         this.button17.Text = "Safety Alert";
         this.button17.UseVisualStyleBackColor = true;
         // 
         // button18
         // 
         this.button18.Location = new System.Drawing.Point(402, 138);
         this.button18.Name = "button18";
         this.button18.Size = new System.Drawing.Size(105, 23);
         this.button18.TabIndex = 74;
         this.button18.Text = "PF Alert";
         this.button18.UseVisualStyleBackColor = true;
         // 
         // button19
         // 
         this.button19.Location = new System.Drawing.Point(402, 112);
         this.button19.Name = "button19";
         this.button19.Size = new System.Drawing.Size(105, 23);
         this.button19.TabIndex = 72;
         this.button19.Text = "PF Status";
         this.button19.UseVisualStyleBackColor = true;
         // 
         // button20
         // 
         this.button20.Location = new System.Drawing.Point(402, 86);
         this.button20.Name = "button20";
         this.button20.Size = new System.Drawing.Size(105, 23);
         this.button20.TabIndex = 70;
         this.button20.Text = "Safety Status";
         this.button20.UseVisualStyleBackColor = true;
         // 
         // button21
         // 
         this.button21.Location = new System.Drawing.Point(402, 60);
         this.button21.Name = "button21";
         this.button21.Size = new System.Drawing.Size(105, 23);
         this.button21.TabIndex = 68;
         this.button21.Text = "Battery Status";
         this.button21.UseVisualStyleBackColor = true;
         // 
         // button22
         // 
         this.button22.Location = new System.Drawing.Point(402, 34);
         this.button22.Name = "button22";
         this.button22.Size = new System.Drawing.Size(105, 23);
         this.button22.TabIndex = 66;
         this.button22.Text = "Operation Status";
         this.button22.UseVisualStyleBackColor = true;
         // 
         // button23
         // 
         this.button23.Location = new System.Drawing.Point(402, 8);
         this.button23.Name = "button23";
         this.button23.Size = new System.Drawing.Size(105, 23);
         this.button23.TabIndex = 61;
         this.button23.Text = "FET Status";
         this.button23.UseVisualStyleBackColor = true;
         // 
         // button24
         // 
         this.button24.Location = new System.Drawing.Point(6, 348);
         this.button24.Name = "button24";
         this.button24.Size = new System.Drawing.Size(100, 23);
         this.button24.TabIndex = 60;
         this.button24.Text = "Cell 3";
         this.button24.UseVisualStyleBackColor = true;
         // 
         // button25
         // 
         this.button25.Location = new System.Drawing.Point(6, 322);
         this.button25.Name = "button25";
         this.button25.Size = new System.Drawing.Size(100, 23);
         this.button25.TabIndex = 59;
         this.button25.Text = "Cell 2";
         this.button25.UseVisualStyleBackColor = true;
         // 
         // button26
         // 
         this.button26.Location = new System.Drawing.Point(6, 296);
         this.button26.Name = "button26";
         this.button26.Size = new System.Drawing.Size(100, 23);
         this.button26.TabIndex = 58;
         this.button26.Text = "Cell 1";
         this.button26.UseVisualStyleBackColor = true;
         // 
         // button27
         // 
         this.button27.Location = new System.Drawing.Point(6, 267);
         this.button27.Name = "button27";
         this.button27.Size = new System.Drawing.Size(100, 23);
         this.button27.TabIndex = 57;
         this.button27.Text = "Charge Level";
         this.button27.UseVisualStyleBackColor = true;
         // 
         // button28
         // 
         this.button28.Location = new System.Drawing.Point(6, 240);
         this.button28.Name = "button28";
         this.button28.Size = new System.Drawing.Size(100, 23);
         this.button28.TabIndex = 56;
         this.button28.Text = "Cycle Count";
         this.button28.UseVisualStyleBackColor = true;
         // 
         // button29
         // 
         this.button29.Location = new System.Drawing.Point(6, 214);
         this.button29.Name = "button29";
         this.button29.Size = new System.Drawing.Size(100, 23);
         this.button29.TabIndex = 55;
         this.button29.Text = "Serial Number";
         this.button29.UseVisualStyleBackColor = true;
         // 
         // button30
         // 
         this.button30.Location = new System.Drawing.Point(6, 188);
         this.button30.Name = "button30";
         this.button30.Size = new System.Drawing.Size(100, 23);
         this.button30.TabIndex = 54;
         this.button30.Text = "Remaining Time";
         this.button30.UseVisualStyleBackColor = true;
         // 
         // button31
         // 
         this.button31.Location = new System.Drawing.Point(7, 162);
         this.button31.Name = "button31";
         this.button31.Size = new System.Drawing.Size(100, 23);
         this.button31.TabIndex = 53;
         this.button31.Text = "Temperature";
         this.button31.UseVisualStyleBackColor = true;
         // 
         // button32
         // 
         this.button32.Location = new System.Drawing.Point(7, 110);
         this.button32.Name = "button32";
         this.button32.Size = new System.Drawing.Size(100, 23);
         this.button32.TabIndex = 52;
         this.button32.Text = "Current";
         this.button32.UseVisualStyleBackColor = true;
         // 
         // button33
         // 
         this.button33.Location = new System.Drawing.Point(7, 84);
         this.button33.Name = "button33";
         this.button33.Size = new System.Drawing.Size(100, 23);
         this.button33.TabIndex = 51;
         this.button33.Text = "Voltage";
         this.button33.UseVisualStyleBackColor = true;
         // 
         // button34
         // 
         this.button34.Location = new System.Drawing.Point(6, 6);
         this.button34.Name = "button34";
         this.button34.Size = new System.Drawing.Size(100, 23);
         this.button34.TabIndex = 50;
         this.button34.Text = "Firmware";
         this.button34.UseVisualStyleBackColor = true;
         // 
         // button35
         // 
         this.button35.Location = new System.Drawing.Point(7, 58);
         this.button35.Name = "button35";
         this.button35.Size = new System.Drawing.Size(100, 23);
         this.button35.TabIndex = 49;
         this.button35.Text = "FCC";
         this.button35.UseVisualStyleBackColor = true;
         // 
         // button36
         // 
         this.button36.Location = new System.Drawing.Point(7, 32);
         this.button36.Name = "button36";
         this.button36.Size = new System.Drawing.Size(100, 23);
         this.button36.TabIndex = 48;
         this.button36.Text = "Man Name";
         this.button36.UseVisualStyleBackColor = true;
         // 
         // groupBox14
         // 
         this.groupBox14.Controls.Add(this.button37);
         this.groupBox14.Controls.Add(this.radioButton1);
         this.groupBox14.Controls.Add(this.radioButton2);
         this.groupBox14.Controls.Add(this.radioButton3);
         this.groupBox14.Controls.Add(this.radioButton4);
         this.groupBox14.Controls.Add(this.radioButton5);
         this.groupBox14.Controls.Add(this.radioButton6);
         this.groupBox14.Location = new System.Drawing.Point(276, 8);
         this.groupBox14.Name = "groupBox14";
         this.groupBox14.Size = new System.Drawing.Size(110, 183);
         this.groupBox14.TabIndex = 45;
         this.groupBox14.TabStop = false;
         this.groupBox14.Text = "Battery Errors";
         // 
         // button37
         // 
         this.button37.Location = new System.Drawing.Point(9, 19);
         this.button37.Name = "button37";
         this.button37.Size = new System.Drawing.Size(92, 23);
         this.button37.TabIndex = 58;
         this.button37.Text = "Errors";
         this.button37.UseVisualStyleBackColor = true;
         // 
         // radioButton1
         // 
         this.radioButton1.AutoSize = true;
         this.radioButton1.Checked = true;
         this.radioButton1.Location = new System.Drawing.Point(10, 46);
         this.radioButton1.Name = "radioButton1";
         this.radioButton1.Size = new System.Drawing.Size(64, 17);
         this.radioButton1.TabIndex = 0;
         this.radioButton1.TabStop = true;
         this.radioButton1.Text = "No Error";
         this.radioButton1.UseVisualStyleBackColor = true;
         // 
         // radioButton2
         // 
         this.radioButton2.AutoSize = true;
         this.radioButton2.Location = new System.Drawing.Point(10, 156);
         this.radioButton2.Name = "radioButton2";
         this.radioButton2.Size = new System.Drawing.Size(78, 17);
         this.radioButton2.TabIndex = 5;
         this.radioButton2.Text = "Over Temp";
         this.radioButton2.UseVisualStyleBackColor = true;
         // 
         // radioButton3
         // 
         this.radioButton3.AutoSize = true;
         this.radioButton3.Location = new System.Drawing.Point(10, 134);
         this.radioButton3.Name = "radioButton3";
         this.radioButton3.Size = new System.Drawing.Size(88, 17);
         this.radioButton3.TabIndex = 4;
         this.radioButton3.Text = "D_FET Open";
         this.radioButton3.UseVisualStyleBackColor = true;
         // 
         // radioButton4
         // 
         this.radioButton4.AutoSize = true;
         this.radioButton4.Location = new System.Drawing.Point(10, 112);
         this.radioButton4.Name = "radioButton4";
         this.radioButton4.Size = new System.Drawing.Size(79, 17);
         this.radioButton4.TabIndex = 3;
         this.radioButton4.Text = "Comm Error";
         this.radioButton4.UseVisualStyleBackColor = true;
         // 
         // radioButton5
         // 
         this.radioButton5.AutoSize = true;
         this.radioButton5.Location = new System.Drawing.Point(10, 90);
         this.radioButton5.Name = "radioButton5";
         this.radioButton5.Size = new System.Drawing.Size(94, 17);
         this.radioButton5.TabIndex = 2;
         this.radioButton5.Text = "Cell Imbalance";
         this.radioButton5.UseVisualStyleBackColor = true;
         // 
         // radioButton6
         // 
         this.radioButton6.AutoSize = true;
         this.radioButton6.Location = new System.Drawing.Point(10, 68);
         this.radioButton6.Name = "radioButton6";
         this.radioButton6.Size = new System.Drawing.Size(68, 17);
         this.radioButton6.TabIndex = 1;
         this.radioButton6.Text = "FCC Low";
         this.radioButton6.UseVisualStyleBackColor = true;
         // 
         // label37
         // 
         this.label37.AutoSize = true;
         this.label37.Location = new System.Drawing.Point(214, 116);
         this.label37.Name = "label37";
         this.label37.Size = new System.Drawing.Size(33, 13);
         this.label37.TabIndex = 43;
         this.label37.Text = "Amps";
         // 
         // label38
         // 
         this.label38.AutoSize = true;
         this.label38.Location = new System.Drawing.Point(214, 194);
         this.label38.Name = "label38";
         this.label38.Size = new System.Drawing.Size(44, 13);
         this.label38.TabIndex = 41;
         this.label38.Text = "Minutes";
         // 
         // label39
         // 
         this.label39.AutoSize = true;
         this.label39.Location = new System.Drawing.Point(214, 168);
         this.label39.Name = "label39";
         this.label39.Size = new System.Drawing.Size(17, 13);
         this.label39.TabIndex = 40;
         this.label39.Text = "°F";
         // 
         // label41
         // 
         this.label41.AutoSize = true;
         this.label41.Location = new System.Drawing.Point(214, 90);
         this.label41.Name = "label41";
         this.label41.Size = new System.Drawing.Size(30, 13);
         this.label41.TabIndex = 39;
         this.label41.Text = "Volts";
         // 
         // label42
         // 
         this.label42.Location = new System.Drawing.Point(215, 273);
         this.label42.Name = "label42";
         this.label42.Size = new System.Drawing.Size(43, 13);
         this.label42.TabIndex = 38;
         this.label42.Text = "%";
         // 
         // btnClearAllValues
         // 
         this.btnClearAllValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
         this.btnClearAllValues.Location = new System.Drawing.Point(683, 527);
         this.btnClearAllValues.Name = "btnClearAllValues";
         this.btnClearAllValues.Size = new System.Drawing.Size(93, 23);
         this.btnClearAllValues.TabIndex = 4;
         this.btnClearAllValues.Text = "Clear All Values";
         this.btnClearAllValues.UseVisualStyleBackColor = true;
         this.btnClearAllValues.Click += new System.EventHandler(this.btnClearAllValues_Click);
         // 
         // txtResponse
         // 
         this.txtResponse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
         this.txtResponse.Location = new System.Drawing.Point(314, 529);
         this.txtResponse.Name = "txtResponse";
         this.txtResponse.ReadOnly = true;
         this.txtResponse.Size = new System.Drawing.Size(292, 20);
         this.txtResponse.TabIndex = 5;
         // 
         // btnIsDrawerOut
         // 
         this.btnIsDrawerOut.Location = new System.Drawing.Point(118, 126);
         this.btnIsDrawerOut.Name = "btnIsDrawerOut";
         this.btnIsDrawerOut.Size = new System.Drawing.Size(75, 23);
         this.btnIsDrawerOut.TabIndex = 11;
         this.btnIsDrawerOut.Text = "Is Out";
         this.btnIsDrawerOut.UseVisualStyleBackColor = true;
         this.btnIsDrawerOut.Click += new System.EventHandler(this.btnIsDrawerOut_Click);
         // 
         // btnIsDrawerIn
         // 
         this.btnIsDrawerIn.Location = new System.Drawing.Point(118, 153);
         this.btnIsDrawerIn.Name = "btnIsDrawerIn";
         this.btnIsDrawerIn.Size = new System.Drawing.Size(75, 23);
         this.btnIsDrawerIn.TabIndex = 12;
         this.btnIsDrawerIn.Text = "Is In";
         this.btnIsDrawerIn.UseVisualStyleBackColor = true;
         this.btnIsDrawerIn.Click += new System.EventHandler(this.btnIsDrawerIn_Click);
         // 
         // txtIsDrawerOut
         // 
         this.txtIsDrawerOut.Location = new System.Drawing.Point(67, 127);
         this.txtIsDrawerOut.Name = "txtIsDrawerOut";
         this.txtIsDrawerOut.Size = new System.Drawing.Size(50, 20);
         this.txtIsDrawerOut.TabIndex = 13;
         // 
         // txtIsDrawerIn
         // 
         this.txtIsDrawerIn.Location = new System.Drawing.Point(67, 154);
         this.txtIsDrawerIn.Name = "txtIsDrawerIn";
         this.txtIsDrawerIn.Size = new System.Drawing.Size(50, 20);
         this.txtIsDrawerIn.TabIndex = 14;
         // 
         // MobiusUSB
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size(884, 562);
         this.Controls.Add(this.txtResponse);
         this.Controls.Add(this.btnClearAllValues);
         this.Controls.Add(this.txtStatus);
         this.Controls.Add(this.btnConnect);
         this.Controls.Add(this.tabControl1);
         this.Controls.Add(this.btnClose);
         this.Name = "MobiusUSB";
         this.Text = "Stinger Medical MOBIUS 4 USB Interface";
         this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MobiusUSB_FormClosing);
         this.tabControl1.ResumeLayout(false);
         this.tabPage1.ResumeLayout(false);
         this.tabPage1.PerformLayout();
         this.groupBox18.ResumeLayout(false);
         this.groupBox18.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
         this.groupBox4.ResumeLayout(false);
         this.groupBox4.PerformLayout();
         this.tabPage2.ResumeLayout(false);
         this.tabPage2.PerformLayout();
         this.groupBox38.ResumeLayout(false);
         this.groupBox38.PerformLayout();
         this.groupBox11.ResumeLayout(false);
         this.groupBox11.PerformLayout();
         this.groupBox10.ResumeLayout(false);
         this.groupBox10.PerformLayout();
         this.tabPage3.ResumeLayout(false);
         this.tabPage3.PerformLayout();
         this.groupBox5.ResumeLayout(false);
         this.groupBox5.PerformLayout();
         this.tabPage4.ResumeLayout(false);
         this.tabPage4.PerformLayout();
         this.groupBox1.ResumeLayout(false);
         this.groupBox1.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh1A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh1A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh1A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh1A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh1A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh1A)).EndInit();
         this.groupBox6.ResumeLayout(false);
         this.groupBox6.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh1B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh1B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh1B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh1B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh1B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh1B)).EndInit();
         this.tabPage5.ResumeLayout(false);
         this.tabPage5.PerformLayout();
         this.groupBox17.ResumeLayout(false);
         this.groupBox17.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh2A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh2A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh2A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh2A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh2A)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh2A)).EndInit();
         this.groupBox16.ResumeLayout(false);
         this.groupBox16.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxVoltageCh2B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanOnCh2B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxOutputCh2B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBusOnCh2B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxFanBlockedCh2B)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxShortCh2B)).EndInit();
         this.tabPage6.ResumeLayout(false);
         this.tabPage6.PerformLayout();
         this.groupBox25.ResumeLayout(false);
         this.groupBox25.PerformLayout();
         this.groupBox8.ResumeLayout(false);
         this.groupBox8.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
         this.groupBox20.ResumeLayout(false);
         this.groupBox20.PerformLayout();
         this.tabPage7.ResumeLayout(false);
         this.tabPage7.PerformLayout();
         this.groupBox35.ResumeLayout(false);
         this.groupBox35.PerformLayout();
         this.groupBox2.ResumeLayout(false);
         this.groupBox7.ResumeLayout(false);
         this.groupBox7.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
         this.groupBox19.ResumeLayout(false);
         this.groupBox19.PerformLayout();
         this.groupBox15.ResumeLayout(false);
         this.groupBox15.PerformLayout();
         this.tabPage13.ResumeLayout(false);
         this.tabPage13.PerformLayout();
         this.tabPage8.ResumeLayout(false);
         this.tabPage8.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxRebootWiFi)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSaveConfiguration)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetExternalAntenna)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetCommTimeout)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetCommSize)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetRemoteResponse)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetRemotePort)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetHostIPAddress)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetSSID)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxBaudRate)).EndInit();
         ((System.ComponentModel.ISupportInitialize)(this.pbxCommandMode)).EndInit();
         this.groupBox9.ResumeLayout(false);
         this.groupBox9.PerformLayout();
         this.tabPage10.ResumeLayout(false);
         this.tabPage10.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetChannel)).EndInit();
         this.groupBox27.ResumeLayout(false);
         this.groupBox27.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetDHCP)).EndInit();
         this.groupBox28.ResumeLayout(false);
         this.groupBox28.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetJoin)).EndInit();
         this.groupBox29.ResumeLayout(false);
         this.groupBox29.PerformLayout();
         this.groupBox34.ResumeLayout(false);
         this.groupBox34.PerformLayout();
         ((System.ComponentModel.ISupportInitialize)(this.pbxSetAuth)).EndInit();
         this.tabPage9.ResumeLayout(false);
         this.groupBox33.ResumeLayout(false);
         this.groupBox33.PerformLayout();
         this.groupBox32.ResumeLayout(false);
         this.groupBox32.PerformLayout();
         this.groupBox26.ResumeLayout(false);
         this.groupBox26.PerformLayout();
         this.groupBox24.ResumeLayout(false);
         this.groupBox24.PerformLayout();
         this.groupBox23.ResumeLayout(false);
         this.groupBox23.PerformLayout();
         this.groupBox22.ResumeLayout(false);
         this.groupBox22.PerformLayout();
         this.groupBox3.ResumeLayout(false);
         this.groupBox3.PerformLayout();
         this.tabPage11.ResumeLayout(false);
         this.tabPage11.PerformLayout();
         this.groupBox37.ResumeLayout(false);
         this.groupBox37.PerformLayout();
         this.groupBox31.ResumeLayout(false);
         this.groupBox31.PerformLayout();
         this.groupBox30.ResumeLayout(false);
         this.groupBox30.PerformLayout();
         this.tabPage12.ResumeLayout(false);
         this.groupBox36.ResumeLayout(false);
         this.groupBox36.PerformLayout();
         this.groupBox21.ResumeLayout(false);
         this.groupBox21.PerformLayout();
         this.groupBox12.ResumeLayout(false);
         this.groupBox13.ResumeLayout(false);
         this.groupBox13.PerformLayout();
         this.groupBox14.ResumeLayout(false);
         this.groupBox14.PerformLayout();
         this.ResumeLayout(false);
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btnClose;
      private System.Windows.Forms.TabControl tabControl1;
      private System.Windows.Forms.TabPage tabPage1;
      private System.Windows.Forms.TabPage tabPage2;
      private System.Windows.Forms.TabPage tabPage3;
      private System.Windows.Forms.TabPage tabPage4;
      private System.Windows.Forms.TabPage tabPage5;
      private System.Windows.Forms.Button btnConnect;
      private System.Windows.Forms.TextBox txtStatus;
      private System.Windows.Forms.TextBox txtCBFirmware;
      private System.Windows.Forms.Button btnCBFirmware;
      private System.Windows.Forms.TextBox txtLCDFirmware;
      private System.Windows.Forms.TextBox txtDC1Firmware;
      private System.Windows.Forms.TextBox txtDC1VoltB;
      private System.Windows.Forms.TextBox txtDC1CurrB;
      private System.Windows.Forms.TextBox txtDC1VoltA;
      private System.Windows.Forms.TextBox txtDC1CurrA;
      private System.Windows.Forms.Button btnHCFirmware;
      private System.Windows.Forms.Button btnBUBMeasuredVoltage;
      private System.Windows.Forms.Button btnBUBChargerStatus;
      private System.Windows.Forms.TabPage tabPage6;
      private System.Windows.Forms.Button btnFlashBL;
      private System.Windows.Forms.Button btnBLOff;
      private System.Windows.Forms.Button btnBLOn;
      private System.Windows.Forms.Button btnTripleBeep;
      private System.Windows.Forms.Button btnDoubleBeep;
      private System.Windows.Forms.Button btnSingleBeep;
      private System.Windows.Forms.Button btnLCDFirmware;
      private System.Windows.Forms.Button btnDC1CurrB;
      private System.Windows.Forms.Button btnDC1VoltB;
      private System.Windows.Forms.Button btnDC1CurrA;
      private System.Windows.Forms.Button btnDC1VoltA;
      private System.Windows.Forms.Button btnDC1Firmware;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Button btnDC2CurrB;
      private System.Windows.Forms.Button btnDC2VoltB;
      private System.Windows.Forms.Button btnDC2CurrA;
      private System.Windows.Forms.Button btnDC2VoltA;
      private System.Windows.Forms.Button btnDC2Firmware;
      private System.Windows.Forms.TextBox txtDC2Firmware;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.TextBox txtDC2VoltB;
      private System.Windows.Forms.TextBox txtDC2CurrB;
      private System.Windows.Forms.TextBox txtDC2VoltA;
      private System.Windows.Forms.TextBox txtDC2CurrA;
      private System.Windows.Forms.TextBox txtDC1Status;
      private System.Windows.Forms.TextBox txtDC1Errors;
      private System.Windows.Forms.Button btnDC1Status;
      private System.Windows.Forms.Button btnDC1Errors;
      private System.Windows.Forms.Button btnWiFiFirmware;
      private System.Windows.Forms.TextBox txtWiFiFirmware;
      private System.Windows.Forms.TextBox txtDC2Status;
      private System.Windows.Forms.TextBox txtDC2Errors;
      private System.Windows.Forms.Button btnDC2Status;
      private System.Windows.Forms.Button btnDC2Errors;
      private System.Windows.Forms.TextBox txtLinkQuality;
      private System.Windows.Forms.TextBox txtMACAddress;
      private System.Windows.Forms.TextBox txtIPAddress;
      private System.Windows.Forms.Button btnLinkQuality;
      private System.Windows.Forms.Button btnMACAddress;
      private System.Windows.Forms.Button btnIPAddress;
      private System.Windows.Forms.Button btnMuteBuzzer;
      private System.Windows.Forms.TabPage tabPage7;
      private System.Windows.Forms.Button btnResetMax;
      private System.Windows.Forms.Button btnMotion;
      private System.Windows.Forms.Button btnZMax;
      private System.Windows.Forms.Button btnYMax;
      private System.Windows.Forms.Button btnXMax;
      private System.Windows.Forms.Button btnZValue;
      private System.Windows.Forms.Button btnYValue;
      private System.Windows.Forms.Button btnXValue;
      private System.Windows.Forms.TextBox txtMotion;
      private System.Windows.Forms.TextBox txtZMax;
      private System.Windows.Forms.TextBox txtYMax;
      private System.Windows.Forms.TextBox txtXMax;
      private System.Windows.Forms.TextBox txtZValue;
      private System.Windows.Forms.TextBox txtYValue;
      private System.Windows.Forms.TextBox txtXValue;
      private System.Windows.Forms.TextBox txtReadSPI;
      private System.Windows.Forms.TextBox txtWriteSPI;
      private System.Windows.Forms.Button btnReadSPI;
      private System.Windows.Forms.Button btnWriteSPI;
      private System.Windows.Forms.TextBox txtIsFlashing;
      private System.Windows.Forms.Button btnFlashLEDs;
      private System.Windows.Forms.Button btnIsFlashing;
      private System.Windows.Forms.TextBox txtBUBChargerStatus;
      private System.Windows.Forms.TextBox txtBUBMeasuredVoltage;
      private System.Windows.Forms.GroupBox groupBox6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.PictureBox pbxFanOnCh1B;
      private System.Windows.Forms.PictureBox pbxOutputCh1B;
      private System.Windows.Forms.PictureBox pbxBusOnCh1B;
      private System.Windows.Forms.PictureBox pbxFanBlockedCh1B;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.PictureBox pbxShortCh1B;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label lblVoltage;
      private System.Windows.Forms.Button btnSetVoltage1B;
      private System.Windows.Forms.Button btnVoltSetting1B;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.TextBox txtVoltageSetting1B;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label19;
      private Meter.Meter VoltMeter1Ch1;
      private System.Windows.Forms.GroupBox groupBox1;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.PictureBox pbxFanOnCh1A;
      private System.Windows.Forms.PictureBox pbxOutputCh1A;
      private System.Windows.Forms.PictureBox pbxBusOnCh1A;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.PictureBox pbxShortCh1A;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.Button btnSetVoltage1A;
      private System.Windows.Forms.PictureBox pbxFanBlockedCh1A;
      private System.Windows.Forms.Label label27;
      private System.Windows.Forms.Label label28;
      private Meter.Meter VoltMeter1Ch2;
      private System.Windows.Forms.TextBox txtVoltageSetting1A;
      private System.Windows.Forms.Button btnVoltSetting1A;
      private System.Windows.Forms.Label label31;
      private System.Windows.Forms.Label label30;
      private System.Windows.Forms.Label label29;
      private System.Windows.Forms.Button btnEnableSet1;
      private System.Windows.Forms.ImageList ilsLED;
      private System.Windows.Forms.Button btnSendData;
      private System.Windows.Forms.GroupBox groupBox4;
      private System.Windows.Forms.TextBox txtPageNo;
      private System.Windows.Forms.Label label32;
      private System.Windows.Forms.Label label33;
      private System.Windows.Forms.TextBox txtTxInterval;
      private System.Windows.Forms.Button btnGetTxInterval;
      private System.Windows.Forms.GroupBox groupBox8;
      private System.Windows.Forms.Button btnSetTxInterval;
      private System.Windows.Forms.Button btnBuzzerState;
      private System.Windows.Forms.TextBox txtBuzzerState;
      private System.Windows.Forms.Button btnBLightstate;
      private System.Windows.Forms.TextBox txtBLightState;
      private System.Windows.Forms.GroupBox groupBox11;
      private System.Windows.Forms.GroupBox groupBox10;
      private System.Windows.Forms.Button btnUnMuteBuzzer;
      private System.Windows.Forms.Label label36;
      private System.Windows.Forms.TextBox txtDC1AWatts;
      private System.Windows.Forms.Label label35;
      private System.Windows.Forms.TextBox txtDC1BWatts;
      private System.Windows.Forms.Button button1;
      private System.Windows.Forms.Button button2;
      private System.Windows.Forms.Button button3;
      private System.Windows.Forms.GroupBox groupBox12;
      private System.Windows.Forms.Button button4;
      private System.Windows.Forms.Button button5;
      private System.Windows.Forms.Button button6;
      private System.Windows.Forms.Button button7;
      private System.Windows.Forms.Button button8;
      private System.Windows.Forms.Button button9;
      private System.Windows.Forms.GroupBox groupBox13;
      private System.Windows.Forms.CheckBox checkBox1;
      private System.Windows.Forms.Button button10;
      private System.Windows.Forms.Button button11;
      private System.Windows.Forms.Button button12;
      private System.Windows.Forms.CheckBox checkBox2;
      private System.Windows.Forms.Button button13;
      private System.Windows.Forms.Button button14;
      private System.Windows.Forms.Button button15;
      private System.Windows.Forms.Button button16;
      private System.Windows.Forms.Button button17;
      private System.Windows.Forms.Button button18;
      private System.Windows.Forms.Button button19;
      private System.Windows.Forms.Button button20;
      private System.Windows.Forms.Button button21;
      private System.Windows.Forms.Button button22;
      private System.Windows.Forms.Button button23;
      private System.Windows.Forms.Button button24;
      private System.Windows.Forms.Button button25;
      private System.Windows.Forms.Button button26;
      private System.Windows.Forms.Button button27;
      private System.Windows.Forms.Button button28;
      private System.Windows.Forms.Button button29;
      private System.Windows.Forms.Button button30;
      private System.Windows.Forms.Button button31;
      private System.Windows.Forms.Button button32;
      private System.Windows.Forms.Button button33;
      private System.Windows.Forms.Button button34;
      private System.Windows.Forms.Button button35;
      private System.Windows.Forms.Button button36;
      private System.Windows.Forms.GroupBox groupBox14;
      private System.Windows.Forms.Button button37;
      private System.Windows.Forms.RadioButton radioButton1;
      private System.Windows.Forms.RadioButton radioButton2;
      private System.Windows.Forms.RadioButton radioButton3;
      private System.Windows.Forms.RadioButton radioButton4;
      private System.Windows.Forms.RadioButton radioButton5;
      private System.Windows.Forms.RadioButton radioButton6;
      private System.Windows.Forms.Label label37;
      private System.Windows.Forms.Label label38;
      private System.Windows.Forms.Label label39;
      private System.Windows.Forms.Label label41;
      private System.Windows.Forms.Label label42;
      private System.Windows.Forms.Label label45;
      private System.Windows.Forms.Label label44;
      private System.Windows.Forms.TextBox txtDC2BWatts;
      private System.Windows.Forms.TextBox txtDC2AWatts;
      private System.Windows.Forms.Button btnLEDsOff;
      private System.Windows.Forms.Button btnRebootMobius;
      private System.Windows.Forms.TextBox txtBootDevice;
      private System.Windows.Forms.TextBox txtFWRev;
      private System.Windows.Forms.TextBox txtPageCount;
      private System.Windows.Forms.Label label51;
      private System.Windows.Forms.Label label50;
      private System.Windows.Forms.Label label49;
      private System.Windows.Forms.GroupBox groupBox15;
      private System.Windows.Forms.Button btnSetMaxCC;
      private System.Windows.Forms.Button btnGetMaxCC;
      private System.Windows.Forms.TextBox txtMaxCC;
      private System.Windows.Forms.TextBox txtSparePins;
      private System.Windows.Forms.Button btnSparePins;
      private System.Windows.Forms.Label label52;
      private System.Windows.Forms.TabPage tabPage8;
      private System.Windows.Forms.Button btnSetAdHocMode;
      private System.Windows.Forms.Button btnFactoryDefault;
      private System.Windows.Forms.GroupBox groupBox9;
      private System.Windows.Forms.Button btnSetBaudHigh;
      private System.Windows.Forms.Button btnSetBaudLow;
      private System.Windows.Forms.Label label34;
      private System.Windows.Forms.TextBox txtWiFiResponse;
      private System.Windows.Forms.Button btnWiFiSend;
      private System.Windows.Forms.TextBox txtWiFiCommand;
      private System.Windows.Forms.TextBox txtReboot;
      private System.Windows.Forms.TextBox txtSave;
      private System.Windows.Forms.TextBox txtExtAnt;
      private System.Windows.Forms.TextBox txtCommTime;
      private System.Windows.Forms.TextBox txtCommSize;
      private System.Windows.Forms.TextBox txtRemoteResponse;
      private System.Windows.Forms.TextBox txtRemotePort;
      private System.Windows.Forms.TextBox txtHostAddress;
      private System.Windows.Forms.TextBox txtSSID;
      private System.Windows.Forms.TextBox txtBaudRate;
      private System.Windows.Forms.TextBox txtEnterCommandMode;
      private System.Windows.Forms.Button btnReboot;
      private System.Windows.Forms.Button btnSave;
      private System.Windows.Forms.Button btnExtAnt;
      private System.Windows.Forms.Button btnCommTime;
      private System.Windows.Forms.Button btnCommSize;
      private System.Windows.Forms.Button btnRemoteResponse;
      private System.Windows.Forms.Button btnRemotePort;
      private System.Windows.Forms.Button btnHostAddress;
      private System.Windows.Forms.Button btnSetSSID;
      private System.Windows.Forms.Button btnSetBaudRate;
      private System.Windows.Forms.Button btnCommandMode;
      private System.Windows.Forms.Button btnSetAllParameters;
      private System.Windows.Forms.Button btnLock1Voltages;
      private System.Windows.Forms.Button btnUnLock1Voltages;
      private System.Windows.Forms.Button btnEnableOutputs1;
      private System.Windows.Forms.Button btnDisableOutputs1;
      private System.Windows.Forms.GroupBox groupBox16;
      private System.Windows.Forms.GroupBox groupBox17;
      private System.Windows.Forms.Label label54;
      private System.Windows.Forms.Label label55;
      private System.Windows.Forms.Label label57;
      private Meter.Meter VoltMeter2Ch2;
      private System.Windows.Forms.Label label56;
      private Meter.Meter VoltMeter2Ch1;
      private System.Windows.Forms.GroupBox groupBox19;
      private System.Windows.Forms.GroupBox groupBox18;
      private System.Windows.Forms.GroupBox groupBox20;
      private System.Windows.Forms.Label label58;
      private System.Windows.Forms.Label label59;
      private System.Windows.Forms.Label label60;
      private System.Windows.Forms.Button btnAreVoltages1Locked;
      private System.Windows.Forms.TextBox txtAreVoltages1Locked;
      private System.Windows.Forms.Button btnClearAllValues;
      private System.Windows.Forms.TextBox txtDeviceMACAddress;
      private System.Windows.Forms.Button btnDeviceMACAddress;
      private System.Windows.Forms.TabPage tabPage9;
      private System.Windows.Forms.GroupBox groupBox26;
      private System.Windows.Forms.TextBox txtDC2Serial;
      private System.Windows.Forms.GroupBox groupBox24;
      private System.Windows.Forms.TextBox txtLCDSerial;
      private System.Windows.Forms.GroupBox groupBox23;
      private System.Windows.Forms.TextBox txtHCSerial;
      private System.Windows.Forms.GroupBox groupBox22;
      private System.Windows.Forms.TextBox txtCBSerial;
      private System.Windows.Forms.GroupBox groupBox3;
      private System.Windows.Forms.Button btnWSSNSet;
      private System.Windows.Forms.Button btnWSSNGet;
      private System.Windows.Forms.TextBox txtWSSerialNumber;
      private System.Windows.Forms.Button btnSetDC2Serial;
      private System.Windows.Forms.Button btnGetDC2Serial;
      private System.Windows.Forms.Button btnSetLCDSerial;
      private System.Windows.Forms.Button btnGetLCDSerial;
      private System.Windows.Forms.Button btnSetHCSerial;
      private System.Windows.Forms.Button btnGetHCSerial;
      private System.Windows.Forms.Button btnSetCBSerial;
      private System.Windows.Forms.Button btnGetCBSerial;
      private System.Windows.Forms.CheckBox ckbNewDevice;
      private System.Windows.Forms.CheckBox ckbActivate;
      private System.Windows.Forms.ToolTip toolTip1;
      private System.Windows.Forms.TabPage tabPage10;
      private System.Windows.Forms.GroupBox groupBox27;
      private System.Windows.Forms.Button btnSetDHCP;
      private System.Windows.Forms.RadioButton rdoDHCPCache;
      private System.Windows.Forms.TextBox txtDHCPIPAddress;
      private System.Windows.Forms.Label label139;
      private System.Windows.Forms.RadioButton rdoAutoIP;
      private System.Windows.Forms.RadioButton rdoDHCPOn;
      private System.Windows.Forms.RadioButton rdoDHCPOff;
      private System.Windows.Forms.Label label138;
      private System.Windows.Forms.TextBox txtChannel;
      private System.Windows.Forms.Button btnSetChannel;
      private System.Windows.Forms.GroupBox groupBox28;
      private System.Windows.Forms.Button btnSetJoin;
      private System.Windows.Forms.RadioButton rdoAdHoc;
      private System.Windows.Forms.RadioButton rdoSecurity;
      private System.Windows.Forms.RadioButton rdoSSID;
      private System.Windows.Forms.RadioButton rdoManual;
      private System.Windows.Forms.GroupBox groupBox29;
      private System.Windows.Forms.Label label134;
      private System.Windows.Forms.Label label135;
      private System.Windows.Forms.Label label137;
      private System.Windows.Forms.TextBox txtPassPhrase;
      private System.Windows.Forms.TextBox txtWEPKey;
      private System.Windows.Forms.Button btnSetAuth;
      private System.Windows.Forms.RadioButton rdoWPA2;
      private System.Windows.Forms.RadioButton rdoMixedWPA;
      private System.Windows.Forms.RadioButton rdoWPA1;
      private System.Windows.Forms.RadioButton rdoWEP128;
      private System.Windows.Forms.RadioButton rdoOpen;
      private System.Windows.Forms.Button btnSetVoltage2B;
      private System.Windows.Forms.Button btnVoltSetting2B;
      private System.Windows.Forms.TextBox txtVoltageSetting2B;
      private System.Windows.Forms.Label label53;
      private System.Windows.Forms.Button btnSetVoltage2A;
      private System.Windows.Forms.Button btnVoltSetting2A;
      private System.Windows.Forms.TextBox txtVoltageSetting2A;
      private System.Windows.Forms.Label label63;
      private System.Windows.Forms.TextBox txtAreVoltages2Locked;
      private System.Windows.Forms.Button btnAreVoltages2Locked;
      private System.Windows.Forms.Button btnEnableOutputs2;
      private System.Windows.Forms.Button btnDisableOutputs2;
      private System.Windows.Forms.Button btnUnLock2Voltages;
      private System.Windows.Forms.Button btnLock2Voltages;
      private System.Windows.Forms.Button btnEnableSet2;
      private System.Windows.Forms.Label label69;
      private System.Windows.Forms.Label label70;
      private System.Windows.Forms.Label label71;
      private System.Windows.Forms.PictureBox pbxFanOnCh2A;
      private System.Windows.Forms.PictureBox pbxOutputCh2A;
      private System.Windows.Forms.PictureBox pbxBusOnCh2A;
      private System.Windows.Forms.Label label72;
      private System.Windows.Forms.PictureBox pbxShortCh2A;
      private System.Windows.Forms.Label label73;
      private System.Windows.Forms.PictureBox pbxFanBlockedCh2A;
      private System.Windows.Forms.Label label64;
      private System.Windows.Forms.Label label65;
      private System.Windows.Forms.Label label66;
      private System.Windows.Forms.PictureBox pbxFanOnCh2B;
      private System.Windows.Forms.PictureBox pbxOutputCh2B;
      private System.Windows.Forms.PictureBox pbxBusOnCh2B;
      private System.Windows.Forms.PictureBox pbxFanBlockedCh2B;
      private System.Windows.Forms.Label label67;
      private System.Windows.Forms.PictureBox pbxShortCh2B;
      private System.Windows.Forms.Label label68;
      private System.Windows.Forms.Label label85;
      private System.Windows.Forms.Label label84;
      private System.Windows.Forms.Label label83;
      private System.Windows.Forms.Label label82;
      private System.Windows.Forms.Label label81;
      private System.Windows.Forms.Label label80;
      private System.Windows.Forms.Label label79;
      private System.Windows.Forms.PictureBox pbxRebootWiFi;
      private System.Windows.Forms.PictureBox pbxSaveConfiguration;
      private System.Windows.Forms.PictureBox pbxSetExternalAntenna;
      private System.Windows.Forms.PictureBox pbxSetCommTimeout;
      private System.Windows.Forms.PictureBox pbxSetCommSize;
      private System.Windows.Forms.PictureBox pbxSetRemoteResponse;
      private System.Windows.Forms.PictureBox pbxSetRemotePort;
      private System.Windows.Forms.Label label78;
      private System.Windows.Forms.PictureBox pbxSetHostIPAddress;
      private System.Windows.Forms.Label label77;
      private System.Windows.Forms.PictureBox pbxSetSSID;
      private System.Windows.Forms.Label label76;
      private System.Windows.Forms.PictureBox pbxBaudRate;
      private System.Windows.Forms.Label label75;
      private System.Windows.Forms.PictureBox pbxCommandMode;
      private System.Windows.Forms.Label label87;
      private System.Windows.Forms.PictureBox pbxSetChannel;
      private System.Windows.Forms.Label label89;
      private System.Windows.Forms.PictureBox pbxSetDHCP;
      private System.Windows.Forms.Label label88;
      private System.Windows.Forms.PictureBox pbxSetJoin;
      private System.Windows.Forms.Label label86;
      private System.Windows.Forms.PictureBox pbxSetAuth;
      private System.Windows.Forms.TextBox txtResponse;
      private System.Windows.Forms.Button btnGetWLAN;
      private System.Windows.Forms.TextBox txtWLAN;
      private System.Windows.Forms.Label lblWLAN;
      private System.Windows.Forms.PictureBox pictureBox1;
      private System.Windows.Forms.PictureBox pictureBox2;
      private System.Windows.Forms.PictureBox pictureBox3;
      private System.Windows.Forms.Button btnGetAllSerialNumbers;
      private System.Windows.Forms.Button btnSendWiFi;
      private System.Windows.Forms.Button btnResetBaudSetFlag;
      private System.Windows.Forms.Button btnSendWLAN;
      private System.Windows.Forms.Button btnSendBoardSerials;
      private System.Windows.Forms.TabPage tabPage11;
      private System.Windows.Forms.TextBox txtKCFirmware;
      private System.Windows.Forms.Button btnKCFirmware;
      private System.Windows.Forms.GroupBox groupBox30;
      private System.Windows.Forms.Button btnSetDrawerOpenTime;
      private System.Windows.Forms.Button btnGetDrawerOpenTime;
      private System.Windows.Forms.Label label74;
      private System.Windows.Forms.TextBox txtDrawerOpenTime;
      private System.Windows.Forms.Button btnLockDrawers;
      private System.Windows.Forms.Button btnUnlockDrawers;
      private System.Windows.Forms.TextBox txtUnlockCount;
      private System.Windows.Forms.Button btnUnlockCount;
      private System.Windows.Forms.TextBox txtNarcPin;
      private System.Windows.Forms.TextBox txtAdminPin;
      private System.Windows.Forms.Button btnGetNarcPin;
      private System.Windows.Forms.Button btnGetAdminPin;
      private System.Windows.Forms.GroupBox groupBox31;
      private System.Windows.Forms.TextBox txtUserPin5;
      private System.Windows.Forms.TextBox txtUserPin4;
      private System.Windows.Forms.TextBox txtUserPin3;
      private System.Windows.Forms.TextBox txtUserPin2;
      private System.Windows.Forms.TextBox txtUserPin1;
      private System.Windows.Forms.Button btnUserPin5;
      private System.Windows.Forms.Button btnUserPin4;
      private System.Windows.Forms.Button btnUserPin3;
      private System.Windows.Forms.Button btnUserPin2;
      private System.Windows.Forms.Button btnUserPin1;
      private System.Windows.Forms.Button btnZeroLockCount;
      private System.Windows.Forms.Button btnSetNarcPin;
      private System.Windows.Forms.Button btnSetAdminPin;
      private System.Windows.Forms.Label label94;
      private System.Windows.Forms.Label label93;
      private System.Windows.Forms.Label label92;
      private System.Windows.Forms.Label label91;
      private System.Windows.Forms.Label label90;
      private System.Windows.Forms.TextBox txtMotorDnCurrent;
      private System.Windows.Forms.TextBox txtMotorUpCurrent;
      private System.Windows.Forms.Button btnMotorDnCurrent;
      private System.Windows.Forms.Button btnMotorUpCurrent;
      private System.Windows.Forms.TextBox txtDrawerCount;
      private System.Windows.Forms.Button btnDrawerCount;
      private System.Windows.Forms.GroupBox groupBox32;
      private System.Windows.Forms.Button btnSetMEDSerial;
      private System.Windows.Forms.Button btnGetMEDSerial;
      private System.Windows.Forms.TextBox txtMEDSerial;
      private System.Windows.Forms.Button btnTempMute;
      private System.Windows.Forms.GroupBox groupBox33;
      private System.Windows.Forms.Button btnSetMOTSerial;
      private System.Windows.Forms.TextBox txtBatterySerial;
      private System.Windows.Forms.GroupBox groupBox34;
      private System.Windows.Forms.RadioButton rdo26HEX;
      private System.Windows.Forms.RadioButton rdo13ASCII;
      private System.Windows.Forms.TextBox txtForcedReboot;
      private System.Windows.Forms.Button btnForcedReboot;
      private System.Windows.Forms.TabPage tabPage12;
      private System.Windows.Forms.Button btnDLCTLHEX;
      private System.Windows.Forms.Button btnLDHOLHEX;
      private System.Windows.Forms.Button btnDLDCDCHEX;
      private System.Windows.Forms.Button btnDLLCDHEX;
      private System.Windows.Forms.GroupBox groupBox21;
      private System.Windows.Forms.Button btnCodes;
      private System.Windows.Forms.Label label62;
      private System.Windows.Forms.Label label61;
      private System.Windows.Forms.TextBox txtParameter;
      private System.Windows.Forms.Button btnSendCommandCode;
      private System.Windows.Forms.TextBox txtCommandCode;
      private System.Windows.Forms.Button btnDLMEDHEX;
      private System.Windows.Forms.Label label95;
      private System.Windows.Forms.TextBox txtDNSServer;
      private System.Windows.Forms.TextBox txtGateway;
      private System.Windows.Forms.TextBox txtSubnetMask;
      private System.Windows.Forms.Label label96;
      private System.Windows.Forms.Label label97;
      private System.Windows.Forms.Button btnBLMedBoard;
      private System.Windows.Forms.Button btnBLControl;
      private System.Windows.Forms.Button btnBLHolster;
      private System.Windows.Forms.Button btnBLDC;
      private System.Windows.Forms.Button btnBLLCD;
      private System.Windows.Forms.Button btnUpdateRoving;
      private System.Windows.Forms.Button btnBUBChrgOff;
      private System.Windows.Forms.Button btnBUBChrgOn;
      private System.Windows.Forms.PictureBox pbxVoltageCh1A;
      private System.Windows.Forms.Label label99;
      private System.Windows.Forms.PictureBox pbxVoltageCh1B;
      private System.Windows.Forms.Label label98;
      private System.Windows.Forms.PictureBox pbxVoltageCh2A;
      private System.Windows.Forms.Label label101;
      private System.Windows.Forms.PictureBox pbxVoltageCh2B;
      private System.Windows.Forms.Label label100;
      private System.Windows.Forms.RadioButton rdoExternal;
      private System.Windows.Forms.RadioButton rdoInternal;
      private System.Windows.Forms.GroupBox groupBox36;
      private System.Windows.Forms.TextBox txtFTPAddress;
      private System.Windows.Forms.Button btnSetFTPAddress;
      private System.Windows.Forms.Button btnGetFTPAddress;
      private System.Windows.Forms.Button btnGetMOTSerial;
      private System.Windows.Forms.TabPage tabPage13;
      private System.Windows.Forms.GroupBox groupBox5;
      private System.Windows.Forms.CheckBox ckbACConnected;
      private System.Windows.Forms.Button btnACConnected;
      private System.Windows.Forms.TextBox txtChargerCurrent;
      private System.Windows.Forms.Button btnChargerCurrent;
      private System.Windows.Forms.TextBox txtChargerVoltage;
      private System.Windows.Forms.Button btnChargerVoltage;
      private System.Windows.Forms.TextBox txtChargerStatus;
      private System.Windows.Forms.CheckBox ckbChargerOFF;
      private System.Windows.Forms.Button btnHChargerStatus;
      private System.Windows.Forms.Button btnChargerOff;
      private System.Windows.Forms.TextBox txtHCFirmware;
      private System.Windows.Forms.RadioButton rdoMedBoardStatsDP;
      private System.Windows.Forms.RadioButton rdoBoardSerialsDP;
      private System.Windows.Forms.RadioButton rdoWLANDP;
      private System.Windows.Forms.RadioButton rdoAlertDP;
      private System.Windows.Forms.RadioButton rdoQCDP;
      private System.Windows.Forms.RadioButton rdoCommCodeAckDP;
      private System.Windows.Forms.RadioButton rdoRebootRequestDP;
      private System.Windows.Forms.RadioButton rdoBatteryDepletedDP;
      private System.Windows.Forms.RadioButton rdoACDisconnectedDP;
      private System.Windows.Forms.RadioButton rdoACConnectedDP;
      private System.Windows.Forms.RadioButton rdoHeartBeatDP;
      private System.Windows.Forms.RadioButton rdoBatteryRemovedDP;
      private System.Windows.Forms.RadioButton rdoBatteryInsertedDP;
      private System.Windows.Forms.RadioButton rdoStandardDP;
      private System.Windows.Forms.TextBox txtMainBatteryOut;
      private System.Windows.Forms.Button btnMainBatteryOut;
      private System.Windows.Forms.Label label48;
      private System.Windows.Forms.TextBox txtThermTemp;
      private System.Windows.Forms.Label label47;
      private System.Windows.Forms.TextBox txtEfficiency;
      private System.Windows.Forms.Label label46;
      private System.Windows.Forms.GroupBox groupBox35;
      private System.Windows.Forms.Button btnHexConvert;
      private System.Windows.Forms.TextBox txtHexValue;
      private System.Windows.Forms.TextBox txtDecimalValue;
      private System.Windows.Forms.Label label103;
      private System.Windows.Forms.Label label102;
      private System.Windows.Forms.GroupBox groupBox2;
      private System.Windows.Forms.Button btnOpenDischargeFET;
      private System.Windows.Forms.Button btnOpenFETs;
      private System.Windows.Forms.Button btnUnlockPack;
      private System.Windows.Forms.Button btnLockPack;
      private System.Windows.Forms.Button btnCloseFETs;
      private System.Windows.Forms.Button btnResetPack;
      private System.Windows.Forms.Button btnResetPermFailure;
      private System.Windows.Forms.Button btnBattModeD;
      private System.Windows.Forms.Button btnChargeStatD;
      private System.Windows.Forms.Button btnSafeAlertD;
      private System.Windows.Forms.Button btnPFAlertD;
      private System.Windows.Forms.Button btnPFStatD;
      private System.Windows.Forms.Button btnSafeStatD;
      private System.Windows.Forms.Button btnBattStatD;
      private System.Windows.Forms.Button btnOpStatD;
      private System.Windows.Forms.TextBox txtBatteryMode;
      private System.Windows.Forms.TextBox txtChargingStatus;
      private System.Windows.Forms.TextBox txtSafetyAlert;
      private System.Windows.Forms.TextBox txtPFAlert;
      private System.Windows.Forms.TextBox txtPFStatus;
      private System.Windows.Forms.TextBox txtSafetyStatus;
      private System.Windows.Forms.TextBox txtBatteryStatus;
      private System.Windows.Forms.TextBox txtOperationStatus;
      private System.Windows.Forms.TextBox txtFETStatus;
      private System.Windows.Forms.GroupBox groupBox7;
      private System.Windows.Forms.Button btnBatteryErrors;
      private System.Windows.Forms.RadioButton rdoError0;
      private System.Windows.Forms.RadioButton rdoError5;
      private System.Windows.Forms.RadioButton rdoError4;
      private System.Windows.Forms.RadioButton rdoError3;
      private System.Windows.Forms.RadioButton rdoError2;
      private System.Windows.Forms.RadioButton rdoError1;
      private System.Windows.Forms.Label label104;
      private System.Windows.Forms.TextBox txtMeasuredVoltage;
      private System.Windows.Forms.Label label43;
      private System.Windows.Forms.TextBox txtBattWatts;
      private System.Windows.Forms.TextBox txtCell3;
      private System.Windows.Forms.TextBox txtCell2;
      private System.Windows.Forms.TextBox txtCell1;
      private System.Windows.Forms.Label label40;
      private System.Windows.Forms.TextBox txtBatCurrent;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.TextBox txtBatteryName;
      private System.Windows.Forms.Label lblPercent;
      private System.Windows.Forms.ProgressBar progressBar1;
      private System.Windows.Forms.TextBox txtBatCycle;
      private System.Windows.Forms.TextBox txtBatSerial;
      private System.Windows.Forms.TextBox txtBatRemain;
      private System.Windows.Forms.TextBox txtBatTemp;
      private System.Windows.Forms.TextBox txtBatVoltage;
      private System.Windows.Forms.TextBox txtBatCapacity;
      private System.Windows.Forms.Button btnPollBattery;
      private System.Windows.Forms.Label label118;
      private System.Windows.Forms.Label label117;
      private System.Windows.Forms.Label label116;
      private System.Windows.Forms.Label label115;
      private System.Windows.Forms.Label label114;
      private System.Windows.Forms.Label label113;
      private System.Windows.Forms.Label label112;
      private System.Windows.Forms.Label label111;
      private System.Windows.Forms.Label label110;
      private System.Windows.Forms.Label label109;
      private System.Windows.Forms.Label label108;
      private System.Windows.Forms.Label label107;
      private System.Windows.Forms.Label label106;
      private System.Windows.Forms.Label label105;
      private System.Windows.Forms.Label label125;
      private System.Windows.Forms.Label label124;
      private System.Windows.Forms.Label label123;
      private System.Windows.Forms.Label label122;
      private System.Windows.Forms.Label label121;
      private System.Windows.Forms.Label label120;
      private System.Windows.Forms.Label label119;
      private System.Windows.Forms.Label label127;
      private System.Windows.Forms.Label label126;
      private System.Windows.Forms.TextBox txtPowerSource;
      private System.Windows.Forms.Button btnPowerSource;
      private System.Windows.Forms.Label label146;
      private System.Windows.Forms.Label label152;
      private System.Windows.Forms.Label label153;
      private System.Windows.Forms.Label label154;
      private System.Windows.Forms.Label label155;
      private System.Windows.Forms.Label label156;
      private System.Windows.Forms.Label label157;
      private System.Windows.Forms.Label label158;
      private System.Windows.Forms.Label label159;
      private System.Windows.Forms.Button btnBUBBattModeD;
      private System.Windows.Forms.Button btnBUBChargeStatD;
      private System.Windows.Forms.Button btnBUBSafeAlertD;
      private System.Windows.Forms.Button btnBUBPFAlertD;
      private System.Windows.Forms.Button btnBUBPFStatD;
      private System.Windows.Forms.Button btnBUBSafeStatD;
      private System.Windows.Forms.Button btnBUBBattStatD;
      private System.Windows.Forms.Button btnBUBOpStatD;
      private System.Windows.Forms.TextBox txtBUBBatteryMode;
      private System.Windows.Forms.TextBox txtBUBChargingStatus;
      private System.Windows.Forms.TextBox txtBUBSafetyAlert;
      private System.Windows.Forms.TextBox txtBUBPFAlert;
      private System.Windows.Forms.TextBox txtBUBPFStatus;
      private System.Windows.Forms.TextBox txtBUBSafetyStatus;
      private System.Windows.Forms.TextBox txtBUBBatteryStatus;
      private System.Windows.Forms.TextBox txtBUBOperationStatus;
      private System.Windows.Forms.TextBox txtBUBFETStatus;
      private System.Windows.Forms.Label label128;
      private System.Windows.Forms.Label label129;
      private System.Windows.Forms.Label label130;
      private System.Windows.Forms.Label label131;
      private System.Windows.Forms.Label label132;
      private System.Windows.Forms.Label label133;
      private System.Windows.Forms.Label label136;
      private System.Windows.Forms.Label label140;
      private System.Windows.Forms.Label label141;
      private System.Windows.Forms.Label label142;
      private System.Windows.Forms.Label label143;
      private System.Windows.Forms.Label label144;
      private System.Windows.Forms.Label label145;
      private System.Windows.Forms.TextBox txtBUBMeasuredVoltage2;
      private System.Windows.Forms.TextBox txtBUBCell2Voltage;
      private System.Windows.Forms.TextBox txtBUBCell1Voltage;
      private System.Windows.Forms.Label label147;
      private System.Windows.Forms.TextBox txtBUBCurrent;
      private System.Windows.Forms.Label label148;
      private System.Windows.Forms.Label label149;
      private System.Windows.Forms.Label label150;
      private System.Windows.Forms.TextBox txtBUBatteryName;
      private System.Windows.Forms.Label lblBUBPercent;
      private System.Windows.Forms.ProgressBar pBarBUBChargeLevel;
      private System.Windows.Forms.TextBox txtBUBCycleCount;
      private System.Windows.Forms.TextBox txtBUBSerialNumber;
      private System.Windows.Forms.TextBox txtBUBRemainingTime;
      private System.Windows.Forms.TextBox txtBUBTemperature;
      private System.Windows.Forms.TextBox txtBUBVoltage;
      private System.Windows.Forms.TextBox txtBUBFCC;
      private System.Windows.Forms.Button btnPollBUB;
      private System.Windows.Forms.Label label151;
      private System.Windows.Forms.TextBox txtUserPin6;
      private System.Windows.Forms.Button btnUserPin6;
      private System.Windows.Forms.GroupBox groupBox25;
      private System.Windows.Forms.Button btnSetCaptureURL;
      private System.Windows.Forms.Button btnGetCaptureURL;
      private System.Windows.Forms.TextBox txtCaptureURL;
      private System.Windows.Forms.Button btnCommFromFlash;
      private System.Windows.Forms.GroupBox groupBox37;
      private System.Windows.Forms.RadioButton rdoMotor8;
      private System.Windows.Forms.RadioButton rdoMotor7;
      private System.Windows.Forms.RadioButton rdoMotor6;
      private System.Windows.Forms.RadioButton rdoMotor5;
      private System.Windows.Forms.RadioButton rdoMotor4;
      private System.Windows.Forms.RadioButton rdoMotor3;
      private System.Windows.Forms.RadioButton rdoMotor2;
      private System.Windows.Forms.RadioButton rdoMotor1;
      private System.Windows.Forms.Button btnMotorIn;
      private System.Windows.Forms.Button btnMotorOut;
      private System.Windows.Forms.Button btnMotorMove;
      private System.Windows.Forms.Button btnReleaseCassette;
      private System.Windows.Forms.GroupBox groupBox38;
      private System.Windows.Forms.Button btnSetBuzzerMuteTime;
      private System.Windows.Forms.Button btnGetBuzzerMuteTime;
      private System.Windows.Forms.Label label160;
      private System.Windows.Forms.TextBox txtBuzzerMuteTime;
      private System.Windows.Forms.TextBox txtServiceRequest;
      private System.Windows.Forms.RadioButton rdoServiceRequestDP;
      private System.Windows.Forms.Label label161;
      private System.Windows.Forms.TextBox txtPIN;
      private System.Windows.Forms.RadioButton rdoDrawersUnlockedDP;
      private System.Windows.Forms.Button btnIsDrawerIn;
      private System.Windows.Forms.Button btnIsDrawerOut;
      private System.Windows.Forms.TextBox txtIsDrawerIn;
      private System.Windows.Forms.TextBox txtIsDrawerOut;
   }
}

