﻿﻿﻿/**********************************************************************************
 Program Name: MobiusUSB.cs
 Author: G.T. Sanford III
 Copyright (c) 2010 by Stinger Medical
 Created: 2/25/2010 at 13:16
-----------------------------------------------------------------------------------
 Description: 



-----------------------------------------------------------------------------------

                     M O D I F I C A T I O N    H I S T O R Y 

  Date    By   Vers   Modification
--------  ---  ----  --------------------------------------------------------------
02/10/11  GTS  3.20  Added DemoBox code.
03/22/11  GTS  3.22  Added check for 14726 BUB voltage Added support for 26 HEX
                     chars in WEP key.
03/29/11  GTS  3.23  Added progressBar1 value limit.
06/03/11  GTS  3.24  Added more Command Codes.
06/05/11  GTS  3.25  Made Command Codes a separate tab.
06/11/11  GTS  3.26  Added WLAN parameters.
06/14/11  GTS  3.27  Added support for 64 byte Passphrase.
06/25/11  GTS  3.28  Added more Command Codes.
06/28/11  GTS  3.29  Added Update Roving Firmware button. 
07/12/11  GTS  3.30  Added Open Discharge FET button. 
07/15/11  GTS  3.31  Added BUB Charger on/off. 
07/27/11  GTS  3.32  Fixed problem in btnAuth_Click where the passcode wasn't
                     handled properly.
08/08/11  GTS  3.33  Added more code to the Roving firmware update to be sure it
                     can do a download. 
09/05/11  GTS  3.34  Added SPI data to existing textboxes. Made channel 0 the default.
11/16/11  GTS  3.35  Added over/under voltage errors to the DC tabs.
01/31/12  GTS  3.36  Added support for the MAX Cycle Count to be stored in the CELL
                     REVISION section of MAN DATA.
04/02/12  GTS  3.37  Added ability to update the Roving from our ftp site.
04/04/12  GTS  3.38  Added a hex converter.
04/17/12  GTS  3.39  Fixed bug in update Roving.
05/29/12  GTS  3.40  Added ability to set FTP Address.
10/23/12  GTS  3.41  Added measured main battery voltage.
12/26/12  GTS  4.00  Mobius 4 Version.
04/29/13  GTS  4.01  Fixed Channel 2 voltmeter reading. Added threading.














------------------------------ ALL RIGHTS RESERVED --------------------------------
**********************************************************************************/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using at_usb_api;


namespace MobiusUSB
{
   public partial class MobiusUSB : Form
   {
      string FVersion = "4.01";
      string VID = "03EB";
      string PID = "4000";
      string sReply = "";
      decimal BattWatts;
      decimal DC1AWatts;
      decimal DC1BWatts;
      decimal DC2AWatts;
      decimal DC2BWatts;
      bool DataReceived = false;
      string WEP_KEY = "";

      delegate void SetTextCallback( string text );

      bool USBConnected = false;
      string  USBData = "";
      static bool _continue;
      System.Threading.Thread readThread;

      at_usb_api.at_usb_api atusb = new at_usb_api.at_usb_api(); 





      #region MobiusUSB Constructor
      /****************************************************************************
           MobiusUSB                                              02/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      public MobiusUSB()
      {
         InitializeComponent();

         pbxFanBlockedCh1B.Image = ilsLED.Images[ 0 ];     // White 
         pbxFanBlockedCh1A.Image = ilsLED.Images[ 0 ];     // White 
         pbxBusOnCh1B.Image = ilsLED.Images[ 0 ];
         pbxBusOnCh1A.Image = ilsLED.Images[ 0 ];
         pbxFanOnCh1B.Image = ilsLED.Images[ 0 ];
         pbxFanOnCh1A.Image = ilsLED.Images[ 0 ];
         pbxOutputCh1B.Image = ilsLED.Images[ 0 ];
         pbxOutputCh1A.Image = ilsLED.Images[ 0 ];
         pbxShortCh1B.Image = ilsLED.Images[ 0 ];
         pbxShortCh1A.Image = ilsLED.Images[ 0 ];
         pbxVoltageCh1B.Image = ilsLED.Images[ 0 ];
         pbxVoltageCh1A.Image = ilsLED.Images[ 0 ];
         //txtSSID.Text = "StingerMedical";

         pbxFanBlockedCh2B.Image = ilsLED.Images[ 0 ];     // White 
         pbxFanBlockedCh2A.Image = ilsLED.Images[ 0 ];     // White 
         pbxBusOnCh2B.Image = ilsLED.Images[ 0 ];
         pbxBusOnCh2A.Image = ilsLED.Images[ 0 ];
         pbxFanOnCh2B.Image = ilsLED.Images[ 0 ];
         pbxFanOnCh2A.Image = ilsLED.Images[ 0 ];
         pbxOutputCh2B.Image = ilsLED.Images[ 0 ];
         pbxOutputCh2A.Image = ilsLED.Images[ 0 ];
         pbxShortCh2B.Image = ilsLED.Images[ 0 ];
         pbxShortCh2A.Image = ilsLED.Images[ 0 ];
         pbxVoltageCh2B.Image = ilsLED.Images[ 0 ];
         pbxVoltageCh2A.Image = ilsLED.Images[ 0 ];

         pbxCommandMode.Image = ilsLED.Images[ 0 ];
         pbxBaudRate.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFi.Image = ilsLED.Images[ 0 ];
         pbxSaveConfiguration.Image = ilsLED.Images[ 0 ];
         pbxSetCommSize.Image = ilsLED.Images[ 0 ];
         pbxSetCommTimeout.Image = ilsLED.Images[ 0 ];
         pbxSetExternalAntenna.Image = ilsLED.Images[ 0 ];
         pbxSetHostIPAddress.Image = ilsLED.Images[ 0 ];
         pbxSetRemotePort.Image = ilsLED.Images[ 0 ];
         pbxSetRemoteResponse.Image = ilsLED.Images[ 0 ];
         pbxSetSSID.Image = ilsLED.Images[ 0 ];
         pbxSetDHCP.Image = ilsLED.Images[ 0 ];
         pbxSetAuth.Image = ilsLED.Images[ 0 ];
         pbxSetChannel.Image = ilsLED.Images[ 0 ];
         pbxSetJoin.Image = ilsLED.Images[ 0 ];

         atusb.PacketSize = 64;

         bool OK = atusb.ConnectDevice( VID, PID );

         USBConnected = false;
         _continue = false;
 
         if ( OK )
         {
            txtStatus.Text = "Connected";
            txtStatus.BackColor = Control.DefaultBackColor;
            USBConnected = true;

            if ( !_continue )
            {
               readThread = new Thread( ReadUSB );
               _continue = true;
               readThread.Start();
            }
         }
         else
         {
            USBConnected = false;
            txtStatus.BackColor = Color.LightPink;
            txtStatus.Text = "NOT Connected: Connect USB and press Connect";
            btnConnect.Enabled = true;
         }

         this.Text = "Stinger Medical MOBIUS 4 USB Interface  " + FVersion;
      }
      /*--- end of MobiusUSB() --------------------------------------------------*/
      #endregion




      #region btnConnect_Click 
      /****************************************************************************
           btnConnect_Click                                       02/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnConnect_Click( object sender, EventArgs e )
      {
         bool OK = atusb.ConnectDevice( VID, PID );

         if ( OK )
         {
            txtStatus.Text = "Connected";
            txtStatus.BackColor = Control.DefaultBackColor;
            USBConnected = true;

            if ( !_continue )
            {
               readThread = new Thread( ReadUSB );
               _continue = true;
               readThread.Start();
            }
         }
         else
         {
            txtStatus.BackColor = Color.LightPink;
            txtStatus.Text = "NOT Connected: Connect USB and press Connect";
            USBConnected = false;
         }
      }
      /*--- end of btnConnect_Click() -------------------------------------------*/
      #endregion




      #region MobiusUSB_FormClosing 
      /****************************************************************************
           MobiusUSB_FormClosing                                  12/29/12
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void MobiusUSB_FormClosing( object sender, FormClosingEventArgs e )
      {
         _continue = false;

         try
         {
            readThread.Abort();
         }
         catch
         {
         }

         atusb.CloseDevice();
      }
      /*--- end of MobiusUSB_FormClosing() --------------------------------------*/
      #endregion




      #region btnClose_Click 
      /****************************************************************************
           btnClose_Click                                         02/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnClose_Click( object sender, EventArgs e )
      {
         _continue = false;

         try
         {
            readThread.Abort();
         }
         catch
         {
         }

         atusb.CloseDevice();
         this.Close();
      }
      /*--- end of btnClose_Click() ---------------------------------------------*/
      #endregion




      #region ReadUSB 
      /****************************************************************************
           ReadUSB                                                05/10/12
        Description: USB thread
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: Runs continuously while program is running
                 1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
       012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
      "FF 0B 20 55 02 04 7E 02 01 7E D1 03 56 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 "
      ****************************************************************************/

      public void ReadUSB()
      {
         while ( _continue )
         {
            Thread.Sleep( 1 );
            try
            {
               sReply = atusb.ReadBytes( false );
               if ( sReply.Length > 2 )
               {
                  SetText( sReply );
               }
            }
            catch
            {
            }
         }
      }
      /*--- end of ReadUSB() ----------------------------------------------------*/
      #endregion




      #region SetText
      /****************************************************************************
           SetText                                                01/27/09
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: This handles displaying the contents of SerialIn as a string
                     if needed.
      ****************************************************************************/

      private void SetText( string text )
      {
         // InvokeRequired compares the thread ID of the
         // calling thread to the thread ID of the creating thread.
         // If these threads are different, it returns true.
         if ( this.InvokeRequired )
         {
            SetTextCallback stc = new SetTextCallback( SetText );
            this.Invoke( stc, new object[] { text } );
         }
         else
         {
            txtResponse.Text = text.Substring( 2 );
            txtResponse.Refresh();
            ProcessCommands( text );
            DataReceived = true;
         }
      }
      /*--- end of SetText() ----------------------------------------------------*/
      #endregion




      #region SendDeviceCommand
      /****************************************************************************
                SendDeviceCommand                                       08/02/10
        Description: Wrapper function for atusb.WriteBytes().
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void SendDeviceCommand( string Command )
      {
         if ( USBConnected )
         {
            txtStatus.Text = Command;
            txtStatus.Refresh();
            txtResponse.Text = "";
            txtResponse.Refresh();

            DataReceived = false;

            atusb.WriteBytes( Command );
         }

         return;
      }
      /*--- end of SendDeviceCommand() ------------------------------------------*/
      #endregion




      #region WaitForData
      /****************************************************************************
           WaitForData                                            09/08/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private bool WaitForData()
      {
         int tries = 0;

         while ( !DataReceived && USBConnected )
         {
            Application.DoEvents();
            System.Threading.Thread.Sleep( 1 );
            tries++;
            if ( tries > 3000 )   // 3 seconds 
               return false;
         }

         return true;
      }
      /*--- end of WaitForData() -----------------------------------------------*/
      #endregion




      #region CalculateWatts 
      /****************************************************************************
           CalculateWatts                                         08/03/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void CalculateWatts()
      {
         decimal E, I, P, Efficiency;

         // Battery 
         try
         {
            E = Convert.ToDecimal( txtBatVoltage.Text );
            E = E / 1000.0m;
         }
         catch
         {
            E = 0.0m;
         }

         try
         {
            I = Convert.ToDecimal( txtBatCurrent.Text );
            if ( I < 0.0m )
               I = I * (-decimal.One);
            I = I / 1000.0m;
         }
         catch
         {
            I = 0.0m;
         }
         try
         {
            P = ( I * E );
         }
         catch
         {
            P = 0.0m;
         }

         BattWatts = P;
         txtBattWatts.Text = String.Format( "{0:N}", P );

         // DC 1 A 
         try
         {
            E = Convert.ToDecimal( txtDC1VoltA.Text );
            E = E / 1000.0m;
         }
         catch
         {
            E = 0.0m;
         }
         try
         {
            I = Convert.ToDecimal( txtDC1CurrA.Text );
            if ( I < 0.0m )
               I = I * ( -decimal.One );
            I = I / 1000.0m;
         }
         catch
         {
            I = 0.0m;
         }
         try
         {
            P = ( I * E );
         }
         catch
         {
            P = 0.0m;
         }

         DC1AWatts = P;
         txtDC1AWatts.Text = String.Format( "{0:N}", P );

         // DC 1 B 

         try
         {
            E = Convert.ToDecimal( txtDC1VoltB.Text );
            E = E / 1000.0m;
         }
         catch
         {
            E = 0.0m;
         }
         try
         {
            I = Convert.ToDecimal( txtDC1CurrB.Text );
            if ( I < 0.0m )
               I = I * ( -decimal.One );
            I = I / 1000.0m;
         }
         catch
         {
            I = 0.0m;
         }
         try
         {
            P = ( I * E );
         }
         catch
         {
            P = 0.0m;
         }

         DC1BWatts = P;
         txtDC1BWatts.Text = String.Format( "{0:N}", P );

         // DC 2 A 
         try
         {
            E = Convert.ToDecimal( txtDC2VoltA.Text );
            E = E / 1000.0m;
         }
         catch
         {
            E = 0.0m;
         }
         try
         {
            I = Convert.ToDecimal( txtDC2CurrA.Text );
            if ( I < 0.0m )
               I = I * ( -decimal.One );
            I = I / 1000.0m;
         }
         catch
         {
            I = 0.0m;
         }
         try
         {
            P = ( I * E );
         }
         catch
         {
            P = 0.0m;
         }

         DC2AWatts = P;
         txtDC2AWatts.Text = String.Format( "{0:N}", P );

         // DC 2 B 
         try
         {
            E = Convert.ToDecimal( txtDC2VoltB.Text );
            E = E / 1000.0m;
         }
         catch
         {
            E = 0.0m;
         }
         try
         {
            I = Convert.ToDecimal( txtDC2CurrB.Text );
            if ( I < 0.0m )
               I = I * ( -decimal.One );
            I = I / 1000.0m;
         }
         catch
         {
            I = 0.0m;
         }
         try
         {
            P = ( I * E );
         }
         catch
         {
            P = 0.0m;
         }

         DC2BWatts = P;
         txtDC2BWatts.Text = String.Format( "{0:N}", P );

         // Efficiency 

         try
         {
            Efficiency = ( DC1AWatts + DC1BWatts + DC2AWatts + DC2BWatts ) / BattWatts;
            Efficiency = Efficiency * 100.0m;
         }
         catch
         {
            Efficiency = 0.0m;
         }
         txtEfficiency.Text = String.Format( "{0:N}", Efficiency );
      }
      /*--- end of CalculateWatts() ---------------------------------------------*/
      #endregion




      #region ProcessCommands 
      /****************************************************************************
           ProcessCommands                                        02/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void ProcessCommands( string sReply )
      {
         char Device = sReply[ 0 ];
         char Command = sReply[ 1 ];
         string Reply = sReply.Substring( 3 );
         Int16 Amt = 0;
         int itmp = 0;
         float ftmp = 0f;

         try
         {
            Amt = Convert.ToInt16( Reply );
         }
         catch
         {
            Amt = 0;
         }

         //txtStatus.Text = sReply;

         switch ( Device )
         {
            #region Control Board
            case '0':                  // Main Control 
               switch ( Command )
               {
                  case 'A':       txtCBFirmware.Text = Reply;           break;

                  case 'B':    // Get board serial number 
                     txtCBSerial.Text = Reply;
                  break;

                  case 'C':
                     txtWSSerialNumber.Text = Reply;
                     break;
 
                  case 'D':            // main battery thermistor 
                  break;

                  case 'E':            // measured main battery voltage 
                     txtMeasuredVoltage.Text = Reply;
                  break;

                  case 'F':            // measured BUB voltage 
                     txtBUBMeasuredVoltage.Text = Reply;
                     txtBUBMeasuredVoltage2.Text = Reply;
                     int irp = 0;
                     try
                     {
                        irp = Int32.Parse( Reply );
                     }
                     catch
                     {
                        irp = 0;
                     }

                     if ( irp >= 12600 )
                        txtBUBMeasuredVoltage.BackColor = Color.Pink;
                     else
                        txtBUBMeasuredVoltage.BackColor = Color.White;
                  break;

                  case 'G':         txtXValue.Text = Reply;           break;
                  case 'H':         txtYValue.Text = Reply;           break;
                  case 'I':         txtZValue.Text = Reply;           break;
                  case 'J':         txtMotion.Text = Reply.Substring( 0, 1 );   break;
                  case 'K':         txtXMax.Text = Reply;             break;
                  case 'L':         txtYMax.Text = Reply;             break;
                  case 'M':         txtZMax.Text = Reply;             break;
                  // 'N' - reset MAX 
                  case 'R':
                     //   0123456789012345678901234567890123456789
                     //0R2__________3_300_____311444400002
                     txtReadSPI.Text = Reply;
                     itmp = Convert.ToInt16( Reply[ 17 ] );
                     itmp += (Convert.ToInt16( Reply[ 18 ] ) * 256);
                     txtPageCount.Text = itmp.ToString();
                     string FWTemp = ( Reply[ 1 ] ).ToString();
                     txtFWRev.Text = FWTemp + "." + Convert.ToInt16( Reply[ 2 ] ).ToString();
                     txtBootDevice.Text = Convert.ToString( Reply[ 19 ] );
                     break;
                  case 'T':       txtTxInterval.Text = Reply;           break;
                  case 'V':            // Spare Pins Status 
                     int SP = 0;
                     try
                     {
                        SP = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        SP = 0;
                     }

                     string B7 = "0";
                     string B6 = "0";
                     string B5 = "0";
                     string B4 = "0";
                     string B3 = "0";
                     string B2 = "0";
                     string B1 = "0";
                     string B0 = "0";

                     if ( ( SP & 0x80 ) == 0x80 )
                        B7 = "1";
                     if ( ( SP & 0x40 ) == 0x40 )
                        B6 = "1";
                     if ( ( SP & 0x20 ) == 0x20 )
                        B5 = "1";
                     if ( ( SP & 0x10 ) == 0x10 )
                        B4 = "1";
                     if ( ( SP & 0x08 ) == 0x08 )
                        B3 = "1";
                     if ( ( SP & 0x03 ) == 0x04 )
                        B2 = "1";
                     if ( ( SP & 0x02 ) == 0x02 )
                        B1 = "1";
                     if ( ( SP & 0x01 ) == 0x01 )
                        B0 = "1";

                     txtSparePins.Text = B7 + B6 + B5 + B4 + " " + B3 + B2 + B1; // + B0; 

                     //txtSparePins.Text = Reply;
                     //txtSparePins.Text = SP.ToString();
                  break;

                  case 'W':
                     if ( Reply[ 0 ] == 'M' )
                        txtPowerSource.Text = "Main Battery";
                     if ( Reply[ 0 ] == 'B' )
                        txtPowerSource.Text = "Backup Battery";
                     if ( Reply[ 0 ] == 'A' )
                        txtPowerSource.Text = "AC Brick";
                  break;

                  case 'X':
                     if ( Reply[ 0 ] == '3' )
                        txtBUBChargerStatus.Text = "Charging";
                     else if ( Reply[ 0 ] == '4' )
                        txtBUBChargerStatus.Text = "Standby";
                     else if ( Reply[ 0 ] == '6' )
                        txtBUBChargerStatus.Text = "Error";
                  break;
               }
            break;
            #endregion




            #region HolsterCharger
            case '1':                  // Holster 
               switch ( Command )
               {
                  case 'A':        txtHCFirmware.Text = Reply;           break;
                  case 'B':        txtChargerStatus.Text = Reply;                 break;
                  case 'C':        ckbACConnected.Checked = (Reply[ 0 ] == '1' ? true : false);   break;
                  
                  case 'D':        //txtBatteryErrors.Text = Reply;        
                  break;

                  case 'E':        ckbChargerOFF.Checked = ( Reply[ 0 ] == '1' ? true : false );  break;
                  case 'F':        txtChargerVoltage.Text = Reply;                 break;
                  case 'G':        txtChargerCurrent.Text = Reply;                 break;
                  case 'H':        txtMainBatteryOut.Text = Reply;                 break;
                  case 'L':        txtIsFlashing.Text = Reply.Substring( 0, 1 );   break;
                  case 'M':        txtIsFlashing.Text = Reply.Substring( 0, 1 );   break;
                  case 'V':        txtChargerStatus.Text = Reply.Substring( 0, 1 );   break;

                  case ')':        txtMaxCC.Text = Reply;                 break;
                  case '<':    // Get board serial number 
                     txtHCSerial.Text = Reply;
                     break;
                  case ']':    // Get forced reboot flag 
                     txtForcedReboot.Text = Reply;
                     break;
            #endregion




            #region Main Battery

                  // Main Battery 
                  case 'a':        txtBatSerial.Text = Reply;            break;
                  case 'b':        txtBatVoltage.Text = Reply;           break;
                  case 'c':
                     txtBatCurrent.Text = Reply;
                     CalculateWatts();
                     break;
                  case 'd':        txtBatTemp.Text = Reply;              break;
                  case 'e':
                     int cl = 0;
                     try
                     {
                        cl = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        cl = 0;
                     }
                     progressBar1.Maximum = 100;
                     progressBar1.Minimum = 0;
                     lblPercent.Text = ( cl.ToString() + "%" );
                     if ( cl > 100 )
                        cl = 100;
                     if ( cl < 0 )
                        cl = 0;
                     progressBar1.Value = cl;
                     break;

                  case 'f':
                     int hr = 0;
                     int min = 0;
                     int mtot = Convert.ToInt32( Reply );
                     hr = mtot / 60;
                     min = mtot - (hr * 60);

                     txtBatRemain.Text = Reply + "    " + hr.ToString() + ":" + min.ToString();
                  break;

                  case 'g':        txtBatCycle.Text = Reply;       break;
                  case 'h':        txtBatCapacity.Text = Reply;    break;
                  case 'i':        txtCell1.Text = Reply;          break;
                  case 'j':        txtCell2.Text = Reply;          break;
                  case 'k':        txtCell3.Text = Reply;          break;
                  case 'l':
                     switch ( Reply[ 0 ] )
                     {
                        case '0':
                           txtFETStatus.Text = "Both FET's Open";
                           break;
                        case '2':
                           txtFETStatus.Text = "Charge FET Open";
                           break;
                        case '4':
                           txtFETStatus.Text = "Discharge FET Open";
                           break;
                        case '6':
                           txtFETStatus.Text = "Both FET's Closed";
                           break;
                     }
                     break;
                  case 'm':        txtBatteryName.Text = Reply;           break;
                  case 'n':        txtThermTemp.Text = Reply;                 break;
                  case 'o':        txtSafetyStatus.Text = String.Format( "{0:X4}", Amt );     break;
                  case 'p':        txtPFStatus.Text = String.Format( "{0:X4}", Amt );         break;
                  case 'q':        txtPFAlert.Text = String.Format( "{0:X4}", Amt );          break;
                  case 'r':        txtBatteryStatus.Text = String.Format( "{0:X4}", Amt );    break;
                  case 's':        txtOperationStatus.Text = String.Format( "{0:X4}", Amt );  break;
                  case 't':        txtBatteryMode.Text = String.Format( "{0:X4}", Amt );      break;
                  case 'u':        txtSafetyAlert.Text = String.Format( "{0:X4}", Amt );      break;
                  case 'v':        txtChargingStatus.Text = String.Format( "{0:X4}", Amt );   break;
                  //case 'w':         = Reply;                 break;
                  //case 'x':         = Reply;                 break;
                  //case 'y':         = Reply;                 break;
                  //case 'z':         = Reply;                 break;

                  //case '{':         = Reply;                 break;

               }
            break;
            #endregion




            #region LCD
            case '2':                  // LCD 
               switch ( Command )
               {
                  case 'K':        txtLCDFirmware.Text = Reply;          break;
                  case 'L':        
                     switch ( Reply[ 0 ] )
                     {
                        case '0':
                           txtBuzzerState.Text = "On";
                        break;
                        case '1':
                           txtBuzzerState.Text = "Perm Muted";
                        break;
                        case '2':
                           txtBuzzerState.Text = "Temp Mute";
                        break;
                        case '3':
                           txtBuzzerState.Text = "Temp Perm Muted";
                        break;
                     }
                     break;
                  case 'S':
                     if ( Reply[ 0 ] == '1' )
                        txtBLightState.Text = "On";
                     else
                        txtBLightState.Text = "Off";
                     break;
                  case '<':    // Get board serial number 
                     txtLCDSerial.Text = Reply;
                     break;
               }
            break;
            #endregion




            #region DC-DC 1
            case '3':                  // DC-DC 1 
               switch ( Command )
               {
                  case 'A':        txtDC1Firmware.Text = Reply;          break;
                  case 'D': 
                     txtDC1VoltA.Text = Reply;
                     try
                     {
                        ftmp = Convert.ToSingle( Reply );
                     }
                     catch
                     {
                        ftmp = 0f;
                     }
                     if ( ftmp > 30000 )
                        VoltMeter1Ch2.Value = ftmp / 10000f;
                     else
                        VoltMeter1Ch2.Value = ftmp / 1000f;
                     break;
                  case 'E':
                     txtDC1CurrA.Text = Reply;
                     // ZZZZZZZZZZZZZZZZ
                     //CalculateWatts();
                     break;
                  case 'K':
                     txtDC1VoltB.Text = Reply;
                     try
                     {
                        ftmp = Convert.ToSingle( Reply );
                     }
                     catch
                     {
                        ftmp = 0f;
                     }
                     VoltMeter1Ch1.Value = ftmp / 1000f;
                     break;
                  case 'L':
                     txtDC1CurrB.Text = Reply;
                     // ZZZZZZZZZZZZZZZZ
                     //CalculateWatts();
                     break;
                  case 'P':
                     txtDC1Errors.Text = Reply;
                     try
                     {
                        itmp = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        itmp = 0;
                     }

                     if ( ( itmp & 0x20 ) == 0x20 )     // Ch B over/under voltage error 
                        pbxVoltageCh1B.Image = ilsLED.Images[ 2 ];     // Red 
                     else
                        pbxVoltageCh1B.Image = ilsLED.Images[ 0 ];     // White 

                     if ( ( itmp & 0x10 ) == 0x10 )     // Ch A over/under voltage error 
                        pbxVoltageCh1A.Image = ilsLED.Images[ 2 ];     // Red 
                     else
                        pbxVoltageCh1A.Image = ilsLED.Images[ 0 ];     // White 

                     if ( ( itmp & 0x08 ) == 0x08 )     // Fan Ch1 error 
                     {
                        pbxFanBlockedCh1B.Image = ilsLED.Images[ 2 ];     // Red 
                     }
                     else
                     {
                        pbxFanBlockedCh1B.Image = ilsLED.Images[ 0 ];     // White 
                     }
                     if ( ( itmp & 0x04 ) == 0x04 )     // Fan Ch2 error 
                     {
                        pbxFanBlockedCh1A.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxFanBlockedCh1A.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x02 ) == 0x02 )     // Ch1 Short Circuit error 
                     {
                        pbxShortCh1B.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxShortCh1B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x01 ) == 0x01 )     // Ch2 Short Circuit error 
                     {
                        pbxShortCh1A.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxShortCh1A.Image = ilsLED.Images[ 0 ];
                     }
                     break;
                  case 'Q':
                     txtDC1Status.Text = Reply;
                     try
                     {
                        itmp = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        itmp = 0;
                     }
                     if ( ( itmp & 0x01 ) == 0x01 )     // Ch 2 A bus power enabled  
                     {
                        pbxBusOnCh1A.Image = ilsLED.Images[ 3 ];          // Green 
                     }
                     else
                     {
                        pbxBusOnCh1A.Image = ilsLED.Images[ 0 ];          // White 
                     }
                     if ( ( itmp & 0x02 ) == 0x02 )     // Ch 1 B bus power enabled  
                     {
                        pbxBusOnCh1B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxBusOnCh1B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x04 ) == 0x04 )     // Ch 2 A output enabled 
                     {
                        pbxOutputCh1A.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxOutputCh1A.Image = ilsLED.Images[ 0 ];
                     }
                     if ( ( itmp & 0x08 ) == 0x08 )     // Ch 1 B output enabled 
                     {
                        pbxOutputCh1B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxOutputCh1B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x10 ) == 0x10 )     // Ch 2 A fan on 
                     {
                        pbxFanOnCh1A.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxFanOnCh1A.Image = ilsLED.Images[ 0 ];
                     }
                     if ( ( itmp & 0x20 ) == 0x20 )     // Ch 1 B fan on 
                     {
                        pbxFanOnCh1B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxFanOnCh1B.Image = ilsLED.Images[ 0 ];
                     }
                     break;
                  case 'M':
                     txtVoltageSetting1B.Text = Reply;
                     break;
                  case 'F':
                     txtVoltageSetting1A.Text = Reply;
                     break;
                  case 'S':
                     txtAreVoltages1Locked.Text = Reply;
                     break;
               }
            break;
            #endregion




            #region DC-DC 2
            case '4':                  // DC-DC 2 
               switch ( Command )
               {
                  case 'A':        txtDC2Firmware.Text = Reply;          break;
                  case 'D':
                     txtDC2VoltA.Text = Reply;
                     try
                     {
                        ftmp = Convert.ToSingle( Reply );
                     }
                     catch
                     {
                        ftmp = 0f;
                     }
                     if ( ftmp > 30000 )
                        VoltMeter2Ch2.Value = ftmp / 10000f;
                     else
                        VoltMeter2Ch2.Value = ftmp / 1000f;
                     break;
                  case 'E':
                     txtDC2CurrA.Text = Reply;
                     // ZZZZZZZZZZZZZZZZ
                     //CalculateWatts();
                     break;
                  case 'K':
                     txtDC2VoltB.Text = Reply;
                     try
                     {
                        ftmp = Convert.ToSingle( Reply );
                     }
                     catch
                     {
                        ftmp = 0f;
                     }
                     VoltMeter2Ch1.Value = ftmp / 1000f;
                     break;
                  case 'L':
                     txtDC2CurrB.Text = Reply;
                     // ZZZZZZZZZZZZZZZZ
                     //CalculateWatts();
                     break;
                  case 'P':
                     txtDC2Errors.Text = Reply;
                     try
                     {
                        itmp = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        itmp = 0;
                     }

                     if ( ( itmp & 0x20 ) == 0x20 )     // Ch B over/under voltage error 
                        pbxVoltageCh2B.Image = ilsLED.Images[ 2 ];     // Red 
                     else
                        pbxVoltageCh2B.Image = ilsLED.Images[ 0 ];     // White 

                     if ( ( itmp & 0x10 ) == 0x10 )     // Ch A over/under voltage error 
                        pbxVoltageCh2A.Image = ilsLED.Images[ 2 ];     // Red 
                     else
                        pbxVoltageCh2A.Image = ilsLED.Images[ 0 ];     // White 

                     if ( ( itmp & 0x08 ) == 0x08 )     // Fan Ch1 error 
                     {
                        pbxFanBlockedCh2B.Image = ilsLED.Images[ 2 ];     // Red 
                     }
                     else
                     {
                        pbxFanBlockedCh2B.Image = ilsLED.Images[ 0 ];     // White 
                     }
                     if ( ( itmp & 0x04 ) == 0x04 )     // Fan Ch2 error 
                     {
                        pbxFanBlockedCh2A.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxFanBlockedCh2A.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x02 ) == 0x02 )     // Ch1 Short Circuit error 
                     {
                        pbxShortCh2B.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxShortCh2B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x01 ) == 0x01 )     // Ch2 Short Circuit error 
                     {
                        pbxShortCh2A.Image = ilsLED.Images[ 2 ];
                     }
                     else
                     {
                        pbxShortCh2A.Image = ilsLED.Images[ 0 ];
                     }
                     break;
                  case 'Q':
                     txtDC2Status.Text = Reply;
                     try
                     {
                        itmp = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        itmp = 0;
                     }
                     if ( ( itmp & 0x01 ) == 0x01 )     // Ch 2 A bus power enabled  
                     {
                        pbxBusOnCh2A.Image = ilsLED.Images[ 3 ];          // Green 
                     }
                     else
                     {
                        pbxBusOnCh2A.Image = ilsLED.Images[ 0 ];          // White 
                     }
                     if ( ( itmp & 0x02 ) == 0x02 )     // Ch 1 B bus power enabled  
                     {
                        pbxBusOnCh2B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxBusOnCh2B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x04 ) == 0x04 )     // Ch 2 A output enabled 
                     {
                        pbxOutputCh2A.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxOutputCh2A.Image = ilsLED.Images[ 0 ];
                     }
                     if ( ( itmp & 0x08 ) == 0x08 )     // Ch 1 B output enabled 
                     {
                        pbxOutputCh2B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxOutputCh2B.Image = ilsLED.Images[ 0 ];
                     }

                     if ( ( itmp & 0x10 ) == 0x10 )     // Ch 2 A fan on 
                     {
                        pbxFanOnCh2A.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxFanOnCh2A.Image = ilsLED.Images[ 0 ];
                     }
                     if ( ( itmp & 0x20 ) == 0x20 )     // Ch 1 B fan on 
                     {
                        pbxFanOnCh2B.Image = ilsLED.Images[ 3 ];
                     }
                     else
                     {
                        pbxFanOnCh2B.Image = ilsLED.Images[ 0 ];
                     }
                     break;
                  case 'M':
                     txtVoltageSetting2B.Text = Reply;
                     break;
                  case 'F':
                     txtVoltageSetting2A.Text = Reply;
                     break;
                  case 'S':
                     txtAreVoltages2Locked.Text = Reply;
                     break;
                  case '<':    // Get board serial number 
                     txtDC2Serial.Text = Reply;
                  break;
               }
            break;
            #endregion




            #region MedBoard
            case '5':                  // MedBoard 
               switch ( Command )
               {
                  case 'A':
                     txtKCFirmware.Text = Reply;
                     break;
                  case 'D':
                     txtDrawerOpenTime.Text = Reply;
                     break;
                  case 'J':
                     txtUnlockCount.Text = Reply;
                     break;
                  case 'L':
                     txtMotorUpCurrent.Text = Reply;
                     break;
                  case 'M':
                     txtMotorDnCurrent.Text = Reply;
                     break;
                  case 'N':
                     txtDrawerCount.Text = Reply;
                     break;

                  case 'Q':
                     txtIsDrawerOut.Text = Reply;
                     break;
                  case 'R':
                     txtIsDrawerIn.Text = Reply;
                     break;

                  case 'f':
                     txtAdminPin.Text = Reply;
                     break;
                  case 'h':
                     txtNarcPin.Text = Reply;
                     break;
                  case 'a':
                     txtUserPin1.Text = Reply;
                     break;
                  case 'b':
                     txtUserPin2.Text = Reply;
                     break;
                  case 'c':
                     txtUserPin3.Text = Reply;
                     break;
                  case 'd':
                     txtUserPin4.Text = Reply;
                     break;
                  case 'e':
                     txtUserPin5.Text = Reply;
                     break;
                  case '<':    // Get board serial number 
                     txtMEDSerial.Text = Reply;
                     break;
                  case 'q':
                     if ( Reply[ 0 ] == '0' )
                        txtBuzzerMuteTime.Text = Reply.Substring( 1 );
                     else
                        txtBuzzerMuteTime.Text = Reply;
                     break;
               }
            break;
            #endregion




            #region WiFi
            case '6':                  // WiFi
               txtWiFiResponse.Text = Reply;
               switch ( Command )
               {
                  case 'A':        txtWiFiFirmware.Text = Reply;         break;
                  case 'B':        txtIPAddress.Text = Reply;            break;
                  case 'C':        txtMACAddress.Text = Reply;           break;
                  case 'D':        txtLinkQuality.Text = Reply;          break;
                  case 'E':        txtWiFiResponse.Text = Reply;         break;
                  case 'F':
                  case 'G':
                  case 'H':
                  case 'I':        txtDeviceMACAddress.Text = Reply;     break;
                  case 'J':        txtWLAN.Text = Decode( Reply );       break;
                  case '|':        txtPassPhrase.Text = Reply;           break;
                  case '~':        txtPassPhrase.Text += Reply;          break;
                  case '<':        txtFTPAddress.Text = Reply;           break;
                  case 'k':        txtCaptureURL.Text = Reply;           break;
                }
            break;
            #endregion




            #region Motor Board
            case '7':                  // 
            break;
            #endregion




            #region BlueTooth Board
            case '8':                  // 
            break;
            #endregion




            #region Backup Battery
            case '9':                  // 
               switch ( Command )
               {
                  case 'a':
                     txtBUBSerialNumber.Text = Reply;
                     break;
                  case 'b':
                     txtBUBVoltage.Text = Reply;
                     break;
                  case 'c':
                     txtBUBCurrent.Text = Reply;
                     break;
                  case 'd':
                     txtBUBTemperature.Text = Reply;
                     break;
                  case 'e':
                     int cl = 0;
                     try
                     {
                        cl = Convert.ToInt32( Reply );
                     }
                     catch
                     {
                        cl = 0;
                     }
                     pBarBUBChargeLevel.Maximum = 100;
                     pBarBUBChargeLevel.Minimum = 0;
                     lblBUBPercent.Text = ( cl.ToString() + "%" );
                     if ( cl > 100 )
                        cl = 100;
                     if ( cl < 0 )
                        cl = 0;
                     pBarBUBChargeLevel.Value = cl;
                     break;

                  case 'f':
                     int hr = 0;
                     int min = 0;
                     int mtot = Convert.ToInt32( Reply );
                     hr = mtot / 60;
                     min = mtot - ( hr * 60 );

                     txtBUBRemainingTime.Text = Reply + "    " + hr.ToString() + ":" + min.ToString();
                     break;

                  case 'g':
                     txtBUBCycleCount.Text = Reply;
                     break;
                  case 'h':
                      txtBUBFCC.Text = Reply;
                     break;
                  case 'i':
                     txtBUBCell1Voltage.Text = Reply;
                     break;
                  case 'j':
                     txtBUBCell2Voltage.Text = Reply;
                     break;
                  case 'k':
                     //txtBUBCell3Voltage.Text = Reply;
                     break;
                  case 'l':
                     switch ( Reply[ 0 ] )
                     {
                        case '0':
                           txtBUBFETStatus.Text = "Both FET's Open";
                           break;
                        case '2':
                           txtBUBFETStatus.Text = "Charge FET Open";
                           break;
                        case '4':
                           txtBUBFETStatus.Text = "Discharge FET Open";
                           break;
                        case '6':
                           txtBUBFETStatus.Text = "Both FET's Closed";
                           break;
                     }
                     break;
                  case 'm':
                     txtBUBatteryName.Text = Reply;
                     break;
                  case 'n':
                     //txtBUBThermTemp.Text = Reply;
                     break;
                  case 'o':
                     txtBUBSafetyStatus.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 'p':
                     txtBUBPFStatus.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 'q':
                     txtBUBPFAlert.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 'r':
                     txtBUBBatteryStatus.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 's':
                     txtBUBOperationStatus.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 't':
                     txtBUBBatteryMode.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 'u':
                     txtBUBSafetyAlert.Text = String.Format( "{0:X4}", Amt );
                     break;
                  case 'v':            // BU_ChargingStatus 
                     txtBUBChargingStatus.Text = String.Format( "{0:X4}", Amt );
                  break;

                  case 'A':            // BU_Timer 
                  break;

                  case 'B':            // BU_Charger 
                  break;

                  case 'C':            // BU_Active 
                  break;

                  case 'D':            // BU_Off 
                  break;
               }
            break;
            #endregion

         }
      }
      /*--- end of ProcessCommands() --------------------------------------------*/
      #endregion




      #region HexToDecimal
      /****************************************************************************
           HexToDecimal                                           04/07/10
        Description: String hex value to integer value
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 8 hex characters max.
      ****************************************************************************/

      private Int32 HexToDecimal( string sHex )
      {
         Int32 value = 0;
         int len = sHex.Length;  // 8 
         Int32 Multiplier = 1;

         // FFFFFFFF 
         // 01234567 

         for ( int i = len - 1; i >= 0; i-- )
         {
            switch ( sHex[ i ] )
            {
               case '0':    value += 0 * Multiplier;     break;
               case '1':    value += 1 * Multiplier;     break;
               case '2':    value += 2 * Multiplier;     break;
               case '3':    value += 3 * Multiplier;     break;
               case '4':    value += 4 * Multiplier;     break;
               case '5':    value += 5 * Multiplier;     break;
               case '6':    value += 6 * Multiplier;     break;
               case '7':    value += 7 * Multiplier;     break;
               case '8':    value += 8 * Multiplier;     break;
               case '9':    value += 9 * Multiplier;     break;
               case 'a':    value += 10 * Multiplier;    break;
               case 'A':    value += 10 * Multiplier;    break;
               case 'b':    value += 11 * Multiplier;    break;
               case 'B':    value += 11 * Multiplier;    break;
               case 'c':    value += 12 * Multiplier;    break;
               case 'C':    value += 12 * Multiplier;    break;
               case 'd':    value += 13 * Multiplier;    break;
               case 'D':    value += 13 * Multiplier;    break;
               case 'e':    value += 14 * Multiplier;    break;
               case 'E':    value += 14 * Multiplier;    break;
               case 'f':    value += 15 * Multiplier;    break;
               case 'F':    value += 15 * Multiplier;    break;
            }
            Multiplier = ( Multiplier * 16 );
         }

         return ( value );
      }
      /*--- end of HexToDecimal() -----------------------------------------------*/
      #endregion




      #region ConvertFromHex
      /**********************************************************************************
         ConvertFromHex                                               04/05/10
      Description: Converts a 2-byte char array representing a hex value into the
                  numeric value. Range is 0 to 255.
      Parameters: 2-byte array
      Return Value: numeric value
      Processing: 
         Called By: ProcessUSBCommands() in the firmware upgrade section
         Comments: "A5" to 165 
      **********************************************************************************/

      private int ConvertFromHex( string sbite )
      {
         int value = 0;

         switch ( sbite[ 0 ] )      // MSB 
         {
            case '0':      value = 0;        break;
            case '1':      value = 16;       break;
            case '2':      value = 32;       break;
            case '3':      value = 48;       break;
            case '4':      value = 64;       break;
            case '5':      value = 80;       break;
            case '6':      value = 96;       break;
            case '7':      value = 112;      break;
            case '8':      value = 128;      break;
            case '9':      value = 144;      break;
            case 'a':
            case 'A':      value = 160;      break;
            case 'b':
            case 'B':      value = 176;      break;
            case 'c':
            case 'C':      value = 192;      break;
            case 'd':
            case 'D':      value = 208;      break;
            case 'e':
            case 'E':      value = 224;      break;
            case 'f':
            case 'F':      value = 240;      break;
         }

         switch ( sbite[ 1 ] )      // LSB 
         {
            case '0':      value += 0;       break;
            case '1':      value += 1;       break;
            case '2':      value += 2;       break;
            case '3':      value += 3;       break;
            case '4':      value += 4;       break;
            case '5':      value += 5;       break;
            case '6':      value += 6;       break;
            case '7':      value += 7;       break;
            case '8':      value += 8;       break;
            case '9':      value += 9;       break;
            case 'a':
            case 'A':      value += 10;      break;
            case 'b':
            case 'B':      value += 11;      break;
            case 'c':
            case 'C':      value += 12;      break;
            case 'd':
            case 'D':      value += 13;      break;
            case 'e':
            case 'E':      value += 14;      break;
            case 'f':
            case 'F':      value += 15;      break;
         }

         return value;
      }
      /*--- end of ConvertFromHex() ---------------------------------------------------*/
      #endregion




      #region Decode 
      /****************************************************************************
           Decode                                                 12/25/12
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private string Decode( string s )
      {
         string uncoded = s;

         //01...|....1....|....2....|....3....|....4....|....5....|....6....|
         //6JWOW|4|1|1|WEP|74657374706f77657268657265|

         int ipos = s.IndexOf( "|" );
         ipos = s.IndexOf( "|", ipos + 1 );
         ipos = s.IndexOf( "|", ipos + 1 );
         ipos = s.IndexOf( "|", ipos + 1 );
         ipos = s.IndexOf( "|", ipos + 1 );
         string skey = s.Substring( ipos + 1 );
         ipos = skey.IndexOf( "|" );
         if ( ipos > 0)
            skey = skey.Remove( ipos );
         string ikey = "";

         if ( skey.Length > 2 )
         {
            for ( int i = 0; i < skey.Length - 1; i += 2 )
            {
               string stmp = skey.Substring( i, 3 );
               if ( stmp[ 2 ] == ' ' )
               {
                  i++;
                  stmp = stmp.Substring( 0, 2 );
               }
               int itmp = ConvertFromHex( stmp );
               ikey = ikey + Convert.ToChar( itmp );
            }
         }

         return uncoded + "  (" + ikey + ")";
      }
      /*--- end of Decode() -----------------------------------------------------*/
      #endregion




      #region txtWEPKey_Leave
      /****************************************************************************
           txtWEPKey_Leave                                        08/03/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtWEPKey_Leave( object sender, EventArgs e )
      {
         if ( txtWEPKey.Text.Trim().Length > 0 )
         {
            if ( rdo13ASCII.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 13 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
                  txtWEPKey.Focus();
                  return;
               }
               WEP_KEY = "";              // Start fresh 
               // Convert ASCII WEP Key to Hex digits: 
               // WirelessLAN4u = 576972656C6573734C414E3475 
               foreach ( char c in txtWEPKey.Text.Trim() )
                  WEP_KEY += String.Format( "{0:X}", (int)c );
            }
            if ( rdo26HEX.Checked )
            {
               if ( txtWEPKey.Text.Trim().Length != 26 )
               {
                  MessageBox.Show( "The WEP Key must be exactly 26 HEX characters long!", "Length Error" );
                  //txtWEPKey.Focus();
                  return;
               }
               WEP_KEY = txtWEPKey.Text.Trim();
            }
         }

         return;
      }
      /*--- end of txtWEPKey_Leave() --------------------------------------------*/
      #endregion



                           


      #region Control Board   (0)

      private void GET_CBFirmware()
      {
         SendDeviceCommand( "0A2" );
      }

      private void btnCBFirmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0A2" );
      }

      private void btnWSSNGet_Click( object sender, EventArgs e )
      {
         string command = "0C2";

         SendDeviceCommand( command );
      }

      private void btnWSSNSet_Click( object sender, EventArgs e )
      {
         string command = "0C1" + txtWSSerialNumber.Text.Trim();

         SendDeviceCommand( command );
      }

      private void btnWriteSPI_Click( object sender, EventArgs e )
      {
         string command = "0R1" + txtWriteSPI.Text.Trim();

         SendDeviceCommand( command );
      }

      private void btnReadSPI_Click( object sender, EventArgs e )
      {
         string command = "0R2" + txtPageNo.Text;
         //if ( txtPageNo.Text == "0" )
         //   ReadPageZero = true;
         //else
         //   ReadPageZero = false;
         SendDeviceCommand( command );
      }

      private void btnPowerSource_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0W2" );
      }



      private void btnSendData_Click( object sender, EventArgs e )
      {
         string command = "0S10";

         // which type do they want to send? 

         // ZZZZZZZZZZZZZZZZZ
         if ( rdoStandardDP.Checked )
            command = "0S10";
         if ( rdoBatteryInsertedDP.Checked )
            command = "0S11";
         if ( rdoBatteryRemovedDP.Checked )
            command = "0S12";
         if ( rdoHeartBeatDP.Checked )
            command = "0S13";
         if ( rdoACConnectedDP.Checked )
            command = "0S14";
         if ( rdoACDisconnectedDP.Checked )
            command = "0S15";
         if ( rdoBatteryDepletedDP.Checked )
            command = "0S16";
         if ( rdoRebootRequestDP.Checked )
            command = "0S17";
         if ( rdoCommCodeAckDP.Checked )
            command = "0S18";
         if ( rdoQCDP.Checked )
            command = "0S19";
         if ( rdoAlertDP.Checked )
            command = "0S110";
         if ( rdoWLANDP.Checked )
            command = "0S111";
         if ( rdoBoardSerialsDP.Checked )
            command = "0S112";
         if ( rdoMedBoardStatsDP.Checked )
            command = "0S113";
         if ( rdoServiceRequestDP.Checked )
            command = "0S118" + txtServiceRequest.Text;
         if ( rdoDrawersUnlockedDP.Checked )
            command = "0S119" + txtPIN.Text;

         SendDeviceCommand( command );
      }




      private void btnTxInterval_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0T2" );   // Get 
      }

      private void btnSetTxInterval_Click( object sender, EventArgs e )
      {
         string stmp = txtTxInterval.Text.Trim();
         int ival = 0;

         try
         {
            ival = Convert.ToInt32( stmp );
         }
         catch
         {
            ival = 300;
         }

         if ( ival < 10 || ival > 9999 )
            ival = 300;

         string command = "0T1" + ival.ToString();

         SendDeviceCommand( command );
      }

      private void btnSparePins_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V2" );
      }

      private void btnGetCBSerial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0B2" );
      }

      private void btnSetCBSerial_Click( object sender, EventArgs e )
      {
         string command = "0B1" + txtCBSerial.Text.Trim();

         SendDeviceCommand( command );
      }

      private void btnSendQCPacket_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0S9" );
      }

      private void btnMeasuredVoltage_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0E2" );
      }


      private void btnXValue_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0G2" );
      }

      private void btnYValue_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0H2" );
      }

      private void btnZValue_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0I2" );
      }

      private void btnXMax_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0K2" );
      }

      private void btnYMax_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0L2" );
      }

      private void btnZMax_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0M2" );
      }

      private void btnMotion_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0J2" );
      }

      private void btnResetMax_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0N1" );
      }

      #endregion     // Control Board 



      
      #region BootLoad Commands
      private void btnDLLCDHEX_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V101[L]" );
      }

      private void btnDLDCDCHEX_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V101[D]" );
      }

      private void btnLDHOLHEX_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V101[H]" );
      }

      private void btnDLCTLHEX_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V101[C]" );
      }

      private void btnDLMEDHEX_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V101[M]" );
      }

      private void btnBLLCD_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V310" );
      }

      private void btnBLDC_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V410" );
      }

      private void btnBLHolster_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V210" );
      }

      private void btnBLControl_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V110" );
      }

      private void btnBLMedBoard_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0V710" );
      }
      #endregion

      


      #region Holster Board   (1)

      private void btnChargerStatus_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1B2" );
      }

      private void btnForcedReboot_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1]2" );
      }

      private void btnHCFirmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1A2" );
      }

      private void btnRebootMobius_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1W0" );     // Open FET 
         // there will be no response 
      }

      private void btnGetHCSerial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1<2" );
      }

      private void btnSetHCSerial_Click( object sender, EventArgs e )
      {
         string command = "1>1" + txtHCSerial.Text.Trim();

         SendDeviceCommand( command );
      }


      private void btnMainBatteryOut_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1H2" );
      }

      private void btnHChargerStatus_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1B2" );
      }

      private void btnChargerVoltage_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1F2" );
      }

      private void btnChargerCurrent_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1G2" );
      }

      private void btnACConnected_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1C2" );
      }

      private void btnChargerOff_Click( object sender, EventArgs e )
      {
         if ( btnChargerOff.Text == "Turn Charger Off" )
         {
            // ChargerState = CHARGER_IDLE_UNTIL_BATTERY_REMOVED; 
            SendDeviceCommand( "1E0" );
            btnChargerOff.Text = "Turn Charger On";
         }
         else
         {
            // ChargerState = CHARGER_IDLE_UNTIL_NEW_BATTERY_INSTALLED; 
            SendDeviceCommand( "1}1" );
            btnChargerOff.Text = "Turn Charger Off";
         }
      }


      #endregion        // Holster Charger 




      #region Main Battery   (1)

      private void btnOpenDischargeFET_Click( object sender, EventArgs e )
      {
         DialogResult dlr = MessageBox.Show( "The system will die if not on AC !!!\nAre you sure you want to open the Discharge FET?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            SendDeviceCommand( "1\"0" );   // Open Battery Discharge FET 
         }
      }

      private void btnOpenFETs_Click( object sender, EventArgs e )
      {
         DialogResult dlr = MessageBox.Show( "The system will die if not on AC !!!\nAre you sure you want to open the FET's?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            SendDeviceCommand( "1[0" );   // Open Battery FET's
         }
      }

      private void btnBatteryErrors_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1D2" );
      }

      private void btnUnlockPack_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1w0" );
      }

      private void btnCloseFETs_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1x1" );
      }

      private void btnResetPermFailure_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1y1" );
      }

      private void btnResetPack_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1z1" );
      }

      private void btnLockPack_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1{1" );
      }

      private void btnFlashLEDs_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1L1" );
      }

      private void btnLEDsOff_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1V0" );
      }

      private void btnIsFlashing_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1M2" );
      }

      private void btnGetMaxCC_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1)2" );
      }

      private void btnSetMaxCC_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "1(1" + txtMaxCC.Text.Trim() );
      }


      // Operation Status Description 
      private void btnOpStatD_Click( object sender, EventArgs e )
      {
         OpStatD osd = new OpStatD();
         osd.BitFields = txtOperationStatus.Text;
         osd.ShowDialog();
      }

      private void btnBattStatD_Click( object sender, EventArgs e )
      {
         BattStatD bsd = new BattStatD();
         bsd.BitFields = txtBatteryStatus.Text;
         bsd.ShowDialog();
      }

      private void btnSafeStatD_Click( object sender, EventArgs e )
      {
         SafeStatD ssd = new SafeStatD();
         ssd.BitFields = txtSafetyStatus.Text;
         ssd.ShowDialog();
      }

      private void btnPFStatD_Click( object sender, EventArgs e )
      {
         PFStatD pfs = new PFStatD();
         pfs.BitFields = txtPFStatus.Text;
         pfs.ShowDialog();
      }

      private void btnPFAlertD_Click( object sender, EventArgs e )
      {
         PFAlertD pfa = new PFAlertD();
         pfa.BitFields = txtPFAlert.Text;
         pfa.ShowDialog();
      }

      private void btnSafeAlertD_Click( object sender, EventArgs e )
      {
         SafeAlertD sad = new SafeAlertD();
         sad.BitFields = txtSafetyAlert.Text;
         sad.ShowDialog();
      }

      private void btnChargeStatD_Click( object sender, EventArgs e )
      {
         ChargeStatD csd = new ChargeStatD();
         csd.BitFields = txtChargingStatus.Text;
         csd.ShowDialog();
      }

      private void btnBattModeD_Click( object sender, EventArgs e )
      {
         BattModeD bmd = new BattModeD();
         bmd.BitFields = txtBatteryMode.Text;
         bmd.ShowDialog();
      }

      #endregion       // Main Battery 
      


       
      #region LCD Board   (2)

      private void btnSingleBeep_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2D1" );
      }

      private void btnDoubleBeep_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2E1" );
      }

      private void btnTripleBeep_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2F1" );
      }

      private void btnBLOn_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2G1" );
      }

      private void btnBLOff_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2G0" );
      }

      private void btnFlashBL_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2I1" );
      }

      private void btnLCDFirmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2A2" );
      }

      private void btnMuteBuzzer_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2L0" );
      }

      private void btnUnMuteBuzzer_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2L1" );
      }

      private void btnTempMute_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2W0" );      // 30 second mute 
      }

      private void btnBuzzerState_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2L2" );
      }

      private void btnBLightstate_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2S2" );
      }

      private void btnGetLCDSerial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "2<2" );
      }

      private void btnSetLCDSerial_Click( object sender, EventArgs e )
      {
         string command = "2>1" + txtLCDSerial.Text.Trim();

         SendDeviceCommand( command );
      }

      #endregion         // LCD 




      #region DCDC 1 Board   (3)

      private void btnDC1Firmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3A2" );
      }

      private void btnDC1VoltA_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3D2" );
      }

      private void btnDC1CurrA_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3E2" );
      }

      private void btnDC1VoltB_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3K2" );
      }

      private void btnDC1CurrB_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3L2" );
      }

      private void btnDC1Errors_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3P2" );
      }

      private void btnDC1Status_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3Q2" );
      }

      private void btnVoltSetting1A_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3F2" );
      }

      private void btnVoltSetting1B_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3M2" );
      }

      private void btnSetVoltage1A_Click( object sender, EventArgs e )
      {
         string stmp = txtVoltageSetting1A.Text.Trim();
         int ival = 0;

         try
         {
            ival = Convert.ToInt32( stmp );
         }
         catch
         {
            ival = 12;
         }

         if ( ival < 5 || ival > 12 )
            ival = 12;

         DialogResult dlr = MessageBox.Show( "Unplug any devices that are voltage sensitive !!!\nAre you sure you want to change voltages?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            VoltMeter1Ch2.Value = ival;
            string command = "3C1" + ival.ToString();

            SendDeviceCommand( command );
         }
      }

      private void btnSetVoltage1B_Click( object sender, EventArgs e )
      {
         string stmp = txtVoltageSetting1B.Text.Trim();
         int ival = 0;

         try
         {
            ival = Convert.ToInt32( stmp );
         }
         catch
         {
            ival = 12;
         }

         if ( ival < 12 || ival > 24 )
            ival = 12;

         DialogResult dlr = MessageBox.Show( "Unplug any devices that are voltage sensitive !!!\nAre you sure you want to change voltages?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            VoltMeter1Ch1.Value = ival;
             string command = "3J1" + ival.ToString();

            SendDeviceCommand( command );
         }
      }


      private void btnEnableSet_Click( object sender, EventArgs e )
      {
         if ( btnEnableSet1.Text == "Enable Set Buttons" )
         {
            DialogResult dlr = MessageBox.Show( "Are you sure you want to enable the SET buttons?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
            if ( dlr == DialogResult.Yes )
            {
               btnSetVoltage1A.Enabled = true;
               btnSetVoltage1B.Enabled = true;
               btnEnableSet1.Text = "Disable Set Buttons";
            }
         }
         else
         {
            btnEnableSet1.Text = "Enable Set Buttons";
            btnSetVoltage1A.Enabled = false;
            btnSetVoltage1B.Enabled = false;
         }
      }

      private void btnLockVoltages_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3U1" );   // Lock Voltages 
      }

      private void btnUnLockVoltages_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3V0" );   // Unlock Voltages 
      }

      private void btnAreVoltages1Locked_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3S2" );   // Are Voltages Locked 
      }

      private void btnDisableOutputs_Click( object sender, EventArgs e )
      {
         DialogResult dlr = MessageBox.Show( "This will shut down all powered devices !!!\nAre you sure you want to disable outputs?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            SendDeviceCommand( "3G0" );   // Disable CH2 Outputs
            while ( !DataReceived )
               Application.DoEvents();
            System.Threading.Thread.Sleep( 500 );
            SendDeviceCommand( "3N0" );   // Disable CH1 Outputs
         }
      }

      private void btnEnableOutputs_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "3H1" );   // Enable CH2 Outputs
         while ( !DataReceived )
            Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );
         SendDeviceCommand( "3O1" );   // Enable CH1 Outputs
      }

      #endregion       // DC 1 




      #region DCDC 2 Board   (4)

      private void btnDC2Firmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4A2" );
      }

      private void btnDC2VoltA_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4D2" );
      }

      private void btnDC2CurrA_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4E2" );
      }

      private void btnDC2VoltB_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4K2" );
      }

      private void btnDC2CurrB_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4L2" );
      }

      private void btnDC2Errors_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4P2" );
      }

      private void btnDC2Status_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4Q2" );
      }

      private void btnGetDC2Serial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4<2" );
      }

      private void btnSetDC2Serial_Click( object sender, EventArgs e )
      {
         string command = "4>1" + txtDC2Serial.Text.Trim();

         SendDeviceCommand( command );
      }

      private void btnVoltSetting2A_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4F2" );
      }

      private void btnVoltSetting2B_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4M2" );
      }

      private void btnSetVoltage2A_Click( object sender, EventArgs e )
      {
         string stmp = txtVoltageSetting2A.Text.Trim();
         int ival = 0;

         try
         {
            ival = Convert.ToInt32( stmp );
         }
         catch
         {
            ival = 12;
         }

         if ( ival < 5 || ival > 12 )
            ival = 12;

         DialogResult dlr = MessageBox.Show( "Unplug any devices that are voltage sensitive !!!\nAre you sure you want to change voltages?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            VoltMeter1Ch2.Value = ival;
            string command = "4C1" + ival.ToString();

            SendDeviceCommand( command );
         }
      }

      private void btnSetVoltage2B_Click( object sender, EventArgs e )
      {
         string stmp = txtVoltageSetting2B.Text.Trim();
         int ival = 0;

         try
         {
            ival = Convert.ToInt32( stmp );
         }
         catch
         {
            ival = 12;
         }

         if ( ival < 12 || ival > 24 )
            ival = 12;

         DialogResult dlr = MessageBox.Show( "Unplug any devices that are voltage sensitive !!!\nAre you sure you want to change voltages?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            VoltMeter1Ch1.Value = ival;
             string command = "4J1" + ival.ToString();

            SendDeviceCommand( command );
         }
      }




      private void btnEnableSet2_Click( object sender, EventArgs e )
      {
         if ( btnEnableSet2.Text == "Enable Set Buttons" )
         {
            DialogResult dlr = MessageBox.Show( "Are you sure you want to enable the SET buttons?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
            if ( dlr == DialogResult.Yes )
            {
               btnSetVoltage2A.Enabled = true;
               btnSetVoltage2B.Enabled = true;
               btnEnableSet2.Text = "Disable Set Buttons";
            }
         }
         else
         {
            btnEnableSet2.Text = "Enable Set Buttons";
            btnSetVoltage2A.Enabled = false;
            btnSetVoltage2B.Enabled = false;
         }
      }

      private void btnLock2Voltages_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4U1" );   // Lock Voltages 
      }

      private void btnUnLock2Voltages_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4V0" );   // Unlock Voltages 
      }

      private void btnAreVoltages2Locked_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4S2" );   // Are Voltages Locked 
      }

      private void btnDisableOutputs2_Click( object sender, EventArgs e )
      {
         DialogResult dlr = MessageBox.Show( "This will shut down all powered devices !!!\nAre you sure you want to disable outputs?", "Mobius USB Message", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dlr == DialogResult.Yes )
         {
            SendDeviceCommand( "4G0" );   // Disable CH2 Outputs
            while ( !DataReceived )
               Application.DoEvents();
            System.Threading.Thread.Sleep( 500 );
            SendDeviceCommand( "4N0" );   // Disable CH1 Outputs
         }
      }

      private void btnEnableOutputs2_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "4H1" );   // Enable CH2 Outputs
         while ( !DataReceived )
            Application.DoEvents();
         System.Threading.Thread.Sleep( 500 );
         SendDeviceCommand( "4O1" );   // Enable CH1 Outputs
      }

      #endregion           // DC 2 




      #region MedBoard Module   (5)

      private void btnKCFirmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5A2" );
      }
      
      private void btnGetDrawerOpenTime_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5D2" );
      }

      private void btnSetDrawerOpenTime_Click( object sender, EventArgs e )
      {
         string dot = txtDrawerOpenTime.Text.Trim();
         int idot = 0;

         try
         {
            idot = int.Parse( dot );
         }
         catch
         {
            idot = 120;
         }

         if ( idot < 10 )
         {
            idot = 10;
            dot = "010";
         }
         else if ( idot > 600 )
         {
            idot = 600;
            dot = "600";
         }
         else if ( idot < 100 )
         {
            dot = "0" + idot.ToString();
         }
         else
         {
            dot = idot.ToString();
         }
         SendDeviceCommand( "5E1" + dot );
         txtDrawerOpenTime.Text = dot;
      }




      private void btnUnlockDrawers_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5F0" );
      }

      private void btnLockDrawers_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5G1" );
      }



      private void btnUnlockCount_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5J2" );
      }


      private void btnZeroLockCount_Click( object sender, EventArgs e )
      {
         DialogResult dr = MessageBox.Show( "Are you sure you want to\nZero out the Drawer unlock count?", "Confirm Action", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2 );
         if ( dr == DialogResult.Yes )
            SendDeviceCommand( "5K0" );
      }


      private void btnMotorUpCurrent_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5L2" );
      }

      private void btnMotorDnCurrent_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5M2" );
      }

      private void btnDrawerCount_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5N2" );
      }


      private void btnGetAdminPin_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5f2" );
      }

      private void btnGetNarcPin_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5h2" );
      }

      private void btnUserPin1_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5a2" );
      }

      private void btnUserPin2_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5b2" );
      }

      private void btnUserPin3_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5c2" );
      }

      private void btnUserPin4_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5d2" );
      }

      private void btnUserPin5_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5e2" );
      }

      private void btnUserPin6_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5l2" );
      }



      private void btnSetAdminPin_Click( object sender, EventArgs e )
      {
         string stmp = txtAdminPin.Text.Trim();

         if ( stmp.Length != 4 )
         {
            MessageBox.Show( "PIN length must be exactly 4 numbers", "Entry Error" );
            txtAdminPin.Focus();
         }
         else
         {
            SendDeviceCommand( "5g1" + stmp );
         }
      }


      private void btnSetNarcPin_Click( object sender, EventArgs e )
      {
         string stmp = txtNarcPin.Text.Trim();
         
         if ( stmp.Length != 2 )
         {
            MessageBox.Show( "PIN length must be exactly 2 numbers", "Entry Error" );
            txtNarcPin.Focus();
         }
         else
         {
            SendDeviceCommand( "5i1" + stmp );
         }
      }

      private void btnGetMEDSerial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5<2" );   // board serial 
      }


      private void btnSetMEDSerial_Click( object sender, EventArgs e )
      {
         string command = "5>1" + txtMEDSerial.Text.Trim();

         SendDeviceCommand( command );
      }


      #endregion           // MedBoard 




      #region WiFi Module   (6)

      private void btnResetBaudSetFlag_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6i0" );
      }

      private void btnSendWLAN_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6W1" );
      }
      
      private void btnSendBoardSerials_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6Y1" );
      }

      private void btnGetWLAN_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6J2" );
      }

      private void txtPassPhrase_Leave( object sender, EventArgs e )
      {
         // replace spaces with dollar signs 
         txtPassPhrase.Text = txtPassPhrase.Text.Replace( ' ', '$' );
      }

      private void btnWiFiFirmware_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6A2" );
      }

      private void btnIPAddress_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6B2" );
      }

      private void btnMACAddress_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6C2" );
      }

      private void btnLinkQuality_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6D2" );
      }

      private void btnDeviceMACAddress_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6I2" );
      }


      private void btnSendWiFi_Click( object sender, EventArgs e )
      {
         string command = txtWiFiCommand.Text.Trim();

         SendDeviceCommand( command );
      }

      private void btnWiFiSend_Click( object sender, EventArgs e )
      {
         string command = "6E1" + txtWiFiCommand.Text.Trim();

         SendDeviceCommand( command );
      }


      private void btnSetBaudLow_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6G1" );   // 9600 
      }

      private void btnSetBaudHigh_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6H1" );   // 38400 
      }

      private void btnCommandMode_Click( object sender, EventArgs e )
      {
         string command = "6E1$$$";
         SendDeviceCommand( command );
         pbxCommandMode.Image = ilsLED.Images[ 3 ];
      }

      private void btnSetBaudRate_Click( object sender, EventArgs e )
      {
         string command = "6E1set u b " + txtBaudRate.Text;
         SendDeviceCommand( command );
         pbxBaudRate.Image = ilsLED.Images[ 3 ];
      }

      private void btnSetSSID1_Click( object sender, EventArgs e )
      {
         string command = "6E1set w s " + txtSSID.Text;
         SendDeviceCommand( command );
         pbxSetSSID.Image = ilsLED.Images[ 3 ];
      }

      private void btnHostAddress_Click( object sender, EventArgs e )
      {
         string command = "6E1set i h " + txtHostAddress.Text;
         SendDeviceCommand( command );
         pbxSetHostIPAddress.Image = ilsLED.Images[ 3 ];
      }

      private void btnRemotePort_Click( object sender, EventArgs e )
      {
         string command = "6E1set i r " + txtRemotePort.Text;
         SendDeviceCommand( command );
         pbxSetRemotePort.Image = ilsLED.Images[ 3 ];
      }

      private void btnRemoteResponse_Click( object sender, EventArgs e )
      {
         string command = "6E1set c r " + txtRemoteResponse.Text;
         SendDeviceCommand( command );
         pbxSetRemoteResponse.Image = ilsLED.Images[ 3 ];
      }

      private void btnCommSize_Click( object sender, EventArgs e )
      {
         string command = "6E1set c s " + txtCommSize.Text;
         SendDeviceCommand( command );
         pbxSetCommSize.Image = ilsLED.Images[ 3 ];
      }

      private void btnCommTime_Click( object sender, EventArgs e )
      {
         string command = "6E1set c t " + txtCommTime.Text;
         SendDeviceCommand( command );
         pbxSetCommTimeout.Image = ilsLED.Images[ 3 ];
      }

      private void btnExtAnt_Click( object sender, EventArgs e )
      {
         string command = "6E1set w e " + txtExtAnt.Text;
         SendDeviceCommand( command );
         pbxSetExternalAntenna.Image = ilsLED.Images[ 3 ];
      }

      private void btnSave_Click( object sender, EventArgs e )
      {
         string command = "6E1save";
         SendDeviceCommand( command );
         pbxSaveConfiguration.Image = ilsLED.Images[ 3 ];
      }

      private void btnReboot_Click( object sender, EventArgs e )
      {
         string command = "6E1reboot";
         SendDeviceCommand( command );
         pbxRebootWiFi.Image = ilsLED.Images[ 3 ];
      }




      #region btnFactoryDefault_Click 
      /****************************************************************************
           btnFactoryDefault_Click                                10/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnFactoryDefault_Click( object sender, EventArgs e )
      {
         string command = "6E1factory RESET";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set u b 38400";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1reboot";
         SendDeviceCommand( command );
      }
      /*--- end of btnFactoryDefault_Click() ------------------------------------*/
      #endregion




      #region btnSetAdHocMode_Click 
      /****************************************************************************
           btnSetAdHocMode_Click                                  10/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetAdHocMode_Click( object sender, EventArgs e )
      {
         string command = "6E1set wlan join 4";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1set wlan ssid adhoc_network";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1set wlan chan 1";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1set ip address 169.254.1.1";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1set ip netmask 255.255.0.0";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1set ip dhcp 0";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 100 );

         command = "6E1save";
         SendDeviceCommand( command );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 500 );

         command = "6E1reboot";
         SendDeviceCommand( command );
      }
      /*--- end of btnSetAdHocMode_Click() --------------------------------------*/
      #endregion




      #region btnSetAllParameters_Click 
      /****************************************************************************
           btnSetAllParameters_Click                              10/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetAllParameters_Click( object sender, EventArgs e )
      {
         if ( ckbNewDevice.Checked )
         {
            SendDeviceCommand( "6G1" );   // 9600 

            while ( !DataReceived )
               Application.DoEvents();
         }

         // tab 1 

         string command = "6E1$$$";
         SendDeviceCommand( command );
         pbxCommandMode.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set u b " + txtBaudRate.Text;
         SendDeviceCommand( command );
         pbxBaudRate.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set w s " + txtSSID.Text;
         SendDeviceCommand( command );
         pbxSetSSID.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 1000 );

         command = "6E1set i h " + txtHostAddress.Text;
         SendDeviceCommand( command );
         pbxSetHostIPAddress.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set i r " + txtRemotePort.Text;
         SendDeviceCommand( command );
         pbxSetRemotePort.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set c r " + txtRemoteResponse.Text;
         SendDeviceCommand( command );
         pbxSetRemoteResponse.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set c s " + txtCommSize.Text;
         SendDeviceCommand( command );
         pbxSetCommSize.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set c t " + txtCommTime.Text;
         SendDeviceCommand( command );
         pbxSetCommTimeout.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1set w e " + txtExtAnt.Text;
         SendDeviceCommand( command );
         pbxSetExternalAntenna.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         // tab 2 

         btnSetChannel_Click( sender, e );

         while ( !DataReceived )
            Application.DoEvents();

         btnSetAuth_Click( sender, e );

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 1000 );

         btnSetDHCP_Click( sender, e );

         while ( !DataReceived )
            Application.DoEvents();

         btnSetJoin_Click( sender, e );

         while ( !DataReceived )
            Application.DoEvents();

         command = "6E1save";
         SendDeviceCommand( command );
         pbxSaveConfiguration.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         System.Threading.Thread.Sleep( 500 );

         command = "6E1reboot";
         SendDeviceCommand( command );
         pbxRebootWiFi.Image = ilsLED.Images[ 3 ];

         while ( !DataReceived )
            Application.DoEvents();

         SendDeviceCommand( "6H1" );   // 38400 
      }
      /*--- end of btnSetAllParameters_Click() ----------------------------------*/
      #endregion




      #region btnSetAuth_Click 
      /****************************************************************************
           btnSetAuth_Click                                       10/25/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSetAuth_Click( object sender, EventArgs e )
      {
         string command = "";

         if ( rdoOpen.Checked )
            command = "6E1set w a 0";   // default 
         if ( rdoWEP128.Checked )
         {
            if ( txtWEPKey.Text.Trim().Length != 13 )
            {
               MessageBox.Show( "The WEP Key must be exactly 13 characters long!", "Length Error" );
               txtWEPKey.Focus();
               return;
            }
            command = "6E1set w a 1";   // WEP-128 
         }
         if ( rdoWPA1.Checked )
            command = "6E1set w a 2";   // 
         if ( rdoMixedWPA.Checked )
            command = "6E1set w a 3";   // 
         if ( rdoWPA2.Checked )
            command = "6E1set w a 4";   // 

         SendDeviceCommand( command );
         pbxSetAuth.Image = ilsLED.Images[ 3 ];

         if ( rdoWEP128.Checked )
         {
            WaitForData();

            if ( WEP_KEY.Length != 26 )
               txtWEPKey_Leave( sender, e );

            command = "6E1set w k " + WEP_KEY;
            SendDeviceCommand( command );
         }

         if ( rdoWPA1.Checked || rdoWPA2.Checked || rdoMixedWPA.Checked )
         {
            WaitForData();

            command = "6E1set w p " + txtPassPhrase.Text;

            if ( command.Length <= 64 )
            {
               SendDeviceCommand( command );
            }
            else      // must split in two 
            {
               int ilen = txtPassPhrase.Text.Length;
               string pc1 = txtPassPhrase.Text.Substring( 0, 32 );   // 1st half
               string pc2 = txtPassPhrase.Text.Substring( 32 ) + Environment.NewLine;      // 2nd half 

               command = "6{1" + pc1;
               SendDeviceCommand( command );

               WaitForData();

               command = "6}1" + pc2;
               SendDeviceCommand( command );
            }
         }
      }
      /*--- end of btnSetAuth_Click() -------------------------------------------*/
      #endregion




      private void btnSetChannel_Click( object sender, EventArgs e )
      {
         string command = "6E1set w c " + txtChannel.Text;
         SendDeviceCommand( command );
         pbxSetChannel.Image = ilsLED.Images[ 3 ];
      }




      private void btnSetJoin_Click( object sender, EventArgs e )
      {
         string command = "";

         if ( rdoManual.Checked )
            command = "6E1set w j 0";
         if ( rdoSSID.Checked )
            command = "6E1set w j 1";    // default 
         if ( rdoSecurity.Checked )
            command = "6E1set w j 2";
         if ( rdoAdHoc.Checked )
            command = "6E1set w j 4";

         SendDeviceCommand( command );
         pbxSetJoin.Image = ilsLED.Images[ 3 ];
      }




      private void btnSetDHCP_Click( object sender, EventArgs e )
      {
         string command = "";

         if ( rdoDHCPOff.Checked )
            command = "6E1set i d 0";
         if ( rdoDHCPOn.Checked )
            command = "6E1set i d 1";   // default 
         if ( rdoAutoIP.Checked )
            command = "6E1set i d 2";
         if ( rdoDHCPCache.Checked )
            command = "6E1set i d 3";

         SendDeviceCommand( command );

         if ( rdoDHCPOff.Checked )
         {
            WaitForData();

            command = "6E1set i a " + txtIPAddress.Text;
            SendDeviceCommand( command );

            WaitForData();

            command = "6E1set i g " + txtGateway.Text;
            SendDeviceCommand( command );

            WaitForData();

            command = "6E1set i n " + txtSubnetMask.Text;
            SendDeviceCommand( command );

            WaitForData();

            command = "6E1set d a " + txtDNSServer.Text;
            SendDeviceCommand( command );
         }

         pbxSetDHCP.Image = ilsLED.Images[ 3 ];
      }

      private void btnGetFTPAddress_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6<2" );   // Get 
      }




      private void btnSetFTPAddress_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6<1" + txtFTPAddress.Text );   // Set 
      }



      private void btnUpdateRoving_Click( object sender, EventArgs e )
      {
         string command = "6E1$$$";
         SendDeviceCommand( command );

         WaitForData();

         command = "6E1set u f 0";                      // hardware flow control off 
         SendDeviceCommand( command );

         WaitForData();


         if ( rdoInternal.Checked )
         {
            command = "6E1set ftp address 192.168.0.165";  // Stinger internal  
            //command = "6E10set ftp address 192.168.0.145";  // Stinger internal  
         }

         if ( rdoExternal.Checked )
         {
            command = "6E1set ftp address 198.175.253.161";  // Roving default 
            //command = "6E1set ftp address 208.62.45.102";  // Stinger external  
         }

         SendDeviceCommand( command );

         WaitForData();

         command = "6E1set ftp dir public";             // ftp directory 
         SendDeviceCommand( command );

         WaitForData();

         command = "6E1set ftp user roving";            // ftp user 
         SendDeviceCommand( command );

         WaitForData();

         command = "6E1set ftp pass Pass123";           // ftp password 
         SendDeviceCommand( command );

         WaitForData();

         command = "6E1ftp u";
         SendDeviceCommand( command );

         MessageBox.Show( "Roving Firmware Update\n\nPlease allow 1 minute for the download,\nThen do a SAVE and REBOOT.", "MobiusUSB Message" );
      }


      #endregion            // WiFi 




      #region Motor Board  (7)
      private void btnGetMOTSerial_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "7<2" );   // board serial 
      }
      #endregion            // MOT 




      #region BlueTooth Module  (8)

      #endregion            // BlueTooth




      #region Backup Battery  (9)

      private void btnBUBMeasuredVoltage_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0F2" );
      }

      private void btnBUBChrgOn_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0l1" );   // turn BUB charger on 
      }

      private void btnBUBChrgOff_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0k0" );   // turn BUB charger off 
      }

      private void btnBUBChargerStatus_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0X2" );   // BUB charger  
      }










      #endregion            // BUB 








      #region txtSSID1_Leave 
      /****************************************************************************
           txtSSID1_Leave                                         08/05/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtSSID1_Leave( object sender, EventArgs e )
      {
         // replace spaces with dollar signs 
         txtSSID.Text = txtSSID.Text.Replace( ' ', '$' );
      }
      /*--- end of txtSSID1_Leave() ---------------------------------------------*/
      #endregion




      #region txtSSID_Leave 
      /****************************************************************************
           txtSSID_Leave                                          08/05/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void txtSSID_Leave( object sender, EventArgs e )
      {
         // replace spaces with dollar signs 
         txtSSID.Text = txtSSID.Text.Replace( ' ', '$' );
      }
      /*--- end of txtSSID_Leave() ----------------------------------------------*/
      #endregion




      #region btnClearAllValues_Click 
      /****************************************************************************
           btnClearAllValues_Click                                08/05/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnClearAllValues_Click( object sender, EventArgs e )
      {
         progressBar1.Value = 0;
         VoltMeter1Ch1.Value = 0f;
         VoltMeter1Ch2.Value = 0f;
         VoltMeter2Ch1.Value = 0f;
         VoltMeter2Ch2.Value = 0f;
         txtStatus.Text = "";
         txtCBFirmware.Text = "";
         txtLCDFirmware.Text = "";
         txtHCFirmware.Text = "";
         txtBatCurrent.Text = "";
         txtBatteryName.Text = "";
         txtBatCycle.Text = "";
         txtBatSerial.Text = "";
         txtBatRemain.Text = "";
         txtBatTemp.Text = "";
         txtBatVoltage.Text = "";
         txtBatCapacity.Text = "";
         txtDC1Firmware.Text = "";
         txtDC1VoltB.Text = "";
         txtDC1CurrB.Text = "";
         txtDC1VoltA.Text = "";
         txtDC1CurrA.Text = "";
         txtDC2Firmware.Text = "";
         txtDC2VoltB.Text = "";
         txtDC2CurrB.Text = "";
         txtDC2VoltA.Text = "";
         txtDC2CurrA.Text = "";
         txtDC1Status.Text = "";
         txtDC1Errors.Text = "";
         txtWiFiFirmware.Text = "";
         txtDC2Status.Text = "";
         txtDC2Errors.Text = "";
         txtLinkQuality.Text = "";
         txtDeviceMACAddress.Text = "";
         txtMACAddress.Text = "";
         txtIPAddress.Text = "";
         txtBatteryMode.Text = "";
         txtChargingStatus.Text = "";
         txtSafetyAlert.Text = "";
         txtPFAlert.Text = "";
         txtPFStatus.Text = "";
         txtSafetyStatus.Text = "";
         txtBatteryStatus.Text = "";
         txtOperationStatus.Text = "";
         txtFETStatus.Text = "";
         txtCell3.Text = "";
         txtCell2.Text = "";
         txtCell1.Text = "";
         txtMainBatteryOut.Text = "";
         txtChargerCurrent.Text = "";
         txtChargerVoltage.Text = "";
         txtChargerStatus.Text = "";
         txtWSSerialNumber.Text = "";
         txtMotion.Text = "";
         txtZMax.Text = "";
         txtYMax.Text = "";
         txtXMax.Text = "";
         txtZValue.Text = "";
         txtYValue.Text = "";
         txtXValue.Text = "";
         txtReadSPI.Text = "";
         txtWriteSPI.Text = "";
         txtIsFlashing.Text = "";
         txtBUBChargerStatus.Text = "";
         txtBUBMeasuredVoltage.Text = "";
         txtVoltageSetting1B.Text = "";
         txtVoltageSetting1A.Text = "";
         //txtPageNo.Text = "";
         txtTxInterval.Text = "";
         txtBuzzerState.Text = "";
         txtBLightState.Text = "";
         txtBattWatts.Text = "";
         txtDC1AWatts.Text = "";
         txtDC1BWatts.Text = "";
         txtDC2BWatts.Text = "";
         txtDC2AWatts.Text = "";
         txtEfficiency.Text = "";
         txtThermTemp.Text = "";
         txtBootDevice.Text = "";
         txtFWRev.Text = "";
         txtPageCount.Text = "";
         txtMaxCC.Text = "";
         txtSparePins.Text = "";
         //txtSSID.Text = "";
         txtWiFiResponse.Text = "";
         txtWiFiCommand.Text = "";
         //txtReboot.Text = "";
         //txtSave.Text = "";
         //txtExtAnt.Text = "";
         //txtCommTime.Text = "";
         //txtCommSize.Text = "";
         //txtRemoteResponse.Text = "";
         //txtRemotePort.Text = "";
         //txtHostAddress.Text = "";
         //txtSSID1.Text = "";
         //txtBaudRate.Text = "";
         //txtEnterCommandMode.Text = "";
         txtAreVoltages1Locked.Text = "";
         lblPercent.Text = "%";
         txtCBSerial.Text = "";
         txtHCSerial.Text = "";
         txtLCDSerial.Text = "";
         txtDC2Serial.Text = "";
         txtMEDSerial.Text = "";
         txtDrawerCount.Text = "";
         txtDrawerOpenTime.Text = "";
         txtMotorDnCurrent.Text = "";
         txtMotorUpCurrent.Text = "";
         txtUnlockCount.Text = "";
         txtAdminPin.Text = "";
         txtNarcPin.Text = "";
         txtUserPin1.Text = "";
         txtUserPin2.Text = "";
         txtUserPin3.Text = "";
         txtUserPin4.Text = "";
         txtUserPin5.Text = "";
         txtKCFirmware.Text = "";

         pbxCommandMode.Image = ilsLED.Images[ 0 ];
         pbxBaudRate.Image = ilsLED.Images[ 0 ];
         pbxRebootWiFi.Image = ilsLED.Images[ 0 ];
         pbxSaveConfiguration.Image = ilsLED.Images[ 0 ];
         pbxSetCommSize.Image = ilsLED.Images[ 0 ];
         pbxSetCommTimeout.Image = ilsLED.Images[ 0 ];
         pbxSetExternalAntenna.Image = ilsLED.Images[ 0 ];
         pbxSetHostIPAddress.Image = ilsLED.Images[ 0 ];
         pbxSetRemotePort.Image = ilsLED.Images[ 0 ];
         pbxSetRemoteResponse.Image = ilsLED.Images[ 0 ];
         pbxSetSSID.Image = ilsLED.Images[ 0 ];
         pbxSetDHCP.Image = ilsLED.Images[ 0 ];
         pbxSetAuth.Image = ilsLED.Images[ 0 ];
         pbxSetChannel.Image = ilsLED.Images[ 0 ];
         pbxSetJoin.Image = ilsLED.Images[ 0 ];
      }
      /*--- end of btnClearAllValues_Click() ------------------------------------*/
      #endregion




      #region btnSendCommandCode_Click 
      /****************************************************************************
           btnSendCommandCode_Click                               08/12/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnSendCommandCode_Click( object sender, EventArgs e )
      {
         string ComCode = txtCommandCode.Text.Trim();
         if ( ComCode.Length > 3 )
            ComCode = ComCode.Substring( 0, 3 );

         string command = "0U1" + ComCode;
         string param = txtParameter.Text.Trim();

         if ( param.Length > 0 )
         {
            switch ( ComCode )
            {
               case "101":      // download boot image 
                  if ( param != "C" && param != "H" && param != "L" && param != "D" && param != "M" &&
                       param != "W" && param != "T" && param != "B" )
                  {
                     MessageBox.Show( "Parameter must be C, H, L, D, M, W, T, B", "Command Code Parameter" );
                     txtParameter.Focus();
                     return;
                  }
                  break;

               default:
                  break;
            }
            
            command = "0U1" + ComCode + "[" + param + "]";
         }
         else      // no parameter 
         {
            if ( ComCode == "601" ||
                 ComCode == "602" ||
                 ComCode == "101" ||
                 ComCode == "102" ||
                 ComCode == "703" ||
                 ComCode == "704" ||
                 ComCode == "808" ||
                 ComCode == "809" ||
                 ComCode == "904" ||
                 ComCode == "905" ||
                 ComCode == "907" )
            {
               MessageBox.Show( "You must include a parameter\nwith this command code.", "Command Error" );
               txtParameter.Focus();
               return;
            }
         }

         SendDeviceCommand( command );
      }
      /*--- end of btnSendCommandCode_Click() -----------------------------------*/
      #endregion




      #region btnCodes_Click 
      /****************************************************************************
           btnCodes_Click                                         08/12/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnCodes_Click( object sender, EventArgs e )
      {
         CCodeList cc = new CCodeList();
         cc.Show();
      }
      /*--- end of btnCodes_Click() ---------------------------------------------*/
      #endregion




      #region ckbActivate_CheckedChanged 
      /****************************************************************************
           ckbActivate_CheckedChanged                             10/19/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void ckbActivate_CheckedChanged( object sender, EventArgs e )
      {
         if ( ckbActivate.Checked )
         {
            btnHostAddress.Enabled = true;
            btnRemotePort.Enabled = true;
            btnRemoteResponse.Enabled = true;
            btnCommSize.Enabled = true;
            btnCommTime.Enabled = true;
            btnExtAnt.Enabled = true;
            btnSetAdHocMode.Enabled = true;
            btnFactoryDefault.Enabled = true;
            btnSetJoin.Enabled = true;
            btnSetDHCP.Enabled = true;
            btnUnlockPack.Enabled = true;
            btnLockPack.Enabled = true;
            btnResetPermFailure.Enabled = true;
            btnResetPack.Enabled = true;
            btnOpenFETs.Enabled = true;
            btnOpenDischargeFET.Enabled = true;
            btnCloseFETs.Enabled = true;
            btnSetMaxCC.Enabled = true;
            btnWSSNSet.Enabled = true;
            btnSetCBSerial.Enabled = true;
            btnSetHCSerial.Enabled = true;
            btnSetLCDSerial.Enabled = true;
            btnSetDC2Serial.Enabled = true;
            btnResetBaudSetFlag.Enabled = true;
            btnWriteSPI.Enabled = true;
            btnSetDrawerOpenTime.Enabled = true;
            btnZeroLockCount.Enabled = true;
            btnSetAdminPin.Enabled = true;
            btnSetNarcPin.Enabled = true;
            btnSetMEDSerial.Enabled = true;
         }
         else
         {
            btnHostAddress.Enabled = false;
            btnRemotePort.Enabled = false;
            btnRemoteResponse.Enabled = false;
            btnCommSize.Enabled = false;
            btnCommTime.Enabled = false;
            btnExtAnt.Enabled = false;
            btnSetAdHocMode.Enabled = false;
            btnFactoryDefault.Enabled = false;
            btnSetJoin.Enabled = false;
            btnSetDHCP.Enabled = false;
            btnUnlockPack.Enabled = false;
            btnLockPack.Enabled = false;
            btnResetPermFailure.Enabled = false;
            btnResetPack.Enabled = false;
            btnOpenFETs.Enabled = false;
            btnOpenDischargeFET.Enabled = false;
            btnCloseFETs.Enabled = false;
            btnSetMaxCC.Enabled = false;
            btnWSSNSet.Enabled = false;
            btnSetCBSerial.Enabled = false;
            btnSetHCSerial.Enabled = false;
            btnSetLCDSerial.Enabled = false;
            btnSetDC2Serial.Enabled = false;
            btnResetBaudSetFlag.Enabled = false;
            btnWriteSPI.Enabled = false;
            btnSetDrawerOpenTime.Enabled = false;
            btnZeroLockCount.Enabled = false;
            btnSetAdminPin.Enabled = false;
            btnSetNarcPin.Enabled = false;
            btnSetMEDSerial.Enabled = false;
         }
      }
      /*--- end of ckbActivate_CheckedChanged() ---------------------------------*/
      #endregion





      private void pictureBox1_DoubleClick( object sender, EventArgs e )
      {
         if ( btnGetWLAN.Visible == false )
         {
            btnGetWLAN.Visible = true;
            txtWLAN.Visible = true;
            lblWLAN.Visible = true;
            //btnSendWLAN.Visible = true;
         }
         else
         {
            btnGetWLAN.Visible = false;
            txtWLAN.Visible = false;
            lblWLAN.Visible = false;
            btnSendWLAN.Visible = false;
         }
      }




      private void btnGetAllSerialNumbers_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "0O2" );     // Workstation 
         WaitForData();
         SendDeviceCommand( "0<2" );     // Main Control 
         WaitForData();
         SendDeviceCommand( "1<2" );     // Holster 
         WaitForData();
         SendDeviceCommand( "2<2" );     // LCD 
         WaitForData();
         SendDeviceCommand( "3<2" );     // DC 1 
         WaitForData();
         SendDeviceCommand( "4<2" );     // DC 2  
         WaitForData();
         SendDeviceCommand( "5<2" );     // Med 
         WaitForData();
         SendDeviceCommand( "6<2" );     // WiFi 
         WaitForData();
         SendDeviceCommand( "7<2" );     // Motor 
         WaitForData();
         SendDeviceCommand( "8<2" );     // BlueTooth 
         WaitForData();
      }




      private void rdoOpen_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoOpen.Checked )
         {
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = false;
         }
         if ( rdoWEP128.Checked )
         {
            txtWEPKey.Enabled = true;
            txtPassPhrase.Enabled = false;
            txtWEPKey.Focus();
         }
         if ( rdoWPA1.Checked )
         {
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhrase.Focus();
         }
         if ( rdoMixedWPA.Checked )
         {
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhrase.Focus();
         }
         if ( rdoWPA2.Checked )
         {
            txtWEPKey.Enabled = false;
            txtPassPhrase.Enabled = true;
            txtPassPhrase.Focus();
         }
      }




      private void rdoDHCPOff_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdoDHCPOff.Checked )
         {
            txtDHCPIPAddress.Enabled = true;
            txtSubnetMask.Enabled = true;
            txtGateway.Enabled = true;
            txtDNSServer.Enabled = true;
            txtDHCPIPAddress.Focus();
         }
         else
         {
            txtDHCPIPAddress.Enabled = false;
            txtSubnetMask.Enabled = false;
            txtGateway.Enabled = false;
            txtDNSServer.Enabled = false;
         }
      }











      private void rdo13ASCII_CheckedChanged( object sender, EventArgs e )
      {
         if ( rdo13ASCII.Checked )
         {
            txtWEPKey.MaxLength = 13;
         }
         if ( rdo26HEX.Checked )
         {
            txtWEPKey.MaxLength = 26;
         }
      }




      private void btnHexConvert_Click( object sender, EventArgs e )
      {
         int ival = 0;

         if ( txtDecimalValue.Text.Length > 0 )
         {
            try
            {
               ival = Convert.ToInt32( txtDecimalValue.Text );
            }
            catch
            {
               ival = 0;
            }

            txtHexValue.Text = String.Format( "{0:X4}", ival );
         }
         else if ( txtHexValue.Text.Length > 0 )
         {
            ival = HexToDecimal( txtHexValue.Text );
            txtDecimalValue.Text = ival.ToString();
         }
      }





      #region btnPollBattery_Click 
      /****************************************************************************
           btnPollBattery_Click                                   12/26/12
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      private void btnPollBattery_Click( object sender, EventArgs e )
      {
         btnPollBattery.Enabled = false;

         // get all main battery parameters 

         SendDeviceCommand( "1a2" );   // Serial Number
         WaitForData();

         SendDeviceCommand( "1b2" );   // Voltage
         WaitForData();

         SendDeviceCommand( "1c2" );   // Current
         WaitForData();

         SendDeviceCommand( "1d2" );   // Temperature
         WaitForData();

         SendDeviceCommand( "1e2" );   // Charge Level
         WaitForData();

         SendDeviceCommand( "1f2" );   // Remaining Time
         WaitForData();

         SendDeviceCommand( "1g2" );   // Cycle Count
         WaitForData();

         SendDeviceCommand( "1h2" );   // FCC
         WaitForData();

         SendDeviceCommand( "1m2" );   // Batt Name
         WaitForData();

         SendDeviceCommand( "1i2" );   // Cell1
         WaitForData();

         SendDeviceCommand( "1j2" );   // Cell2
         WaitForData();

         SendDeviceCommand( "1k2" );   // Cell3
         WaitForData();

         SendDeviceCommand( "1n2" );   // Therm
         WaitForData();

         SendDeviceCommand( "1l2" );   // FET Status
         WaitForData();

         SendDeviceCommand( "1s2" );   // Operation Status
         WaitForData();

         SendDeviceCommand( "1r2" );   // Battery Status
         WaitForData();

         SendDeviceCommand( "1o2" );   // Safety Status
         WaitForData();

         SendDeviceCommand( "1p2" );   // PF Status
         WaitForData();

         SendDeviceCommand( "1q2" );   // PF Alert
         WaitForData();

         SendDeviceCommand( "1u2" );   // Safety Alert
         WaitForData();

         SendDeviceCommand( "1v2" );   // Charging Status
         WaitForData();

         SendDeviceCommand( "1t2" );   // Battery Mode

         btnPollBattery.Enabled = true;

         return;
      }
      /*--- end of btnPollBattery_Click() ---------------------------------------*/
      #endregion




      private void btnPollBUB_Click( object sender, EventArgs e )
      {
         btnPollBUB.Enabled = false;

         // get all main battery parameters 

         SendDeviceCommand( "9a2" );   // Serial Number
         WaitForData();

         SendDeviceCommand( "9b2" );   // Voltage
         WaitForData();

         SendDeviceCommand( "0F2" );   // measured voltage 
         WaitForData();

         SendDeviceCommand( "9c2" );   // Current
         WaitForData();

         SendDeviceCommand( "9d2" );   // Temperature
         WaitForData();

         SendDeviceCommand( "9e2" );   // Charge Level
         WaitForData();

         SendDeviceCommand( "9f2" );   // Remaining Time
         WaitForData();

         SendDeviceCommand( "9g2" );   // Cycle Count
         WaitForData();

         SendDeviceCommand( "9h2" );   // FCC
         WaitForData();

         SendDeviceCommand( "9m2" );   // Batt Name
         WaitForData();

         SendDeviceCommand( "9i2" );   // Cell1
         WaitForData();

         SendDeviceCommand( "9j2" );   // Cell2
         WaitForData();

         //SendDeviceCommand( "9k2" );   // Cell3
         //WaitForData();

         SendDeviceCommand( "9n2" );   // Therm
         WaitForData();

         SendDeviceCommand( "9l2" );   // FET Status
         WaitForData();

         SendDeviceCommand( "9s2" );   // Operation Status
         WaitForData();

         SendDeviceCommand( "9r2" );   // Battery Status
         WaitForData();

         SendDeviceCommand( "9o2" );   // Safety Status
         WaitForData();

         SendDeviceCommand( "9p2" );   // PF Status
         WaitForData();

         SendDeviceCommand( "9q2" );   // PF Alert
         WaitForData();

         SendDeviceCommand( "9u2" );   // Safety Alert
         WaitForData();

         SendDeviceCommand( "9v2" );   // Charging Status
         WaitForData();

         SendDeviceCommand( "9t2" );   // Battery Mode

         btnPollBUB.Enabled = true;

         return;
      }


      
      
      private void btnBUBOpStatD_Click( object sender, EventArgs e )
      {
         OpStatD osd = new OpStatD();
         osd.BitFields = txtBUBOperationStatus.Text;
         osd.ShowDialog();
      }

      private void btnBUBBattStatD_Click( object sender, EventArgs e )
      {
         BattStatD bsd = new BattStatD();
         bsd.BitFields = txtBUBBatteryStatus.Text;
         bsd.ShowDialog();
      }

      private void btnBUBSafeStatD_Click( object sender, EventArgs e )
      {
         SafeStatD ssd = new SafeStatD();
         ssd.BitFields = txtBUBSafetyStatus.Text;
         ssd.ShowDialog();
      }

      private void btnBUBPFStatD_Click( object sender, EventArgs e )
      {
         PFStatD pfs = new PFStatD();
         pfs.BitFields = txtBUBPFStatus.Text;
         pfs.ShowDialog();
      }

      private void btnBUBPFAlertD_Click( object sender, EventArgs e )
      {
         PFAlertD pfa = new PFAlertD();
         pfa.BitFields = txtBUBPFAlert.Text;
         pfa.ShowDialog();
      }

      private void btnBUBSafeAlertD_Click( object sender, EventArgs e )
      {
         SafeAlertD sad = new SafeAlertD();
         sad.BitFields = txtBUBSafetyAlert.Text;
         sad.ShowDialog();
      }

      private void btnBUBChargeStatD_Click( object sender, EventArgs e )
      {
         ChargeStatD csd = new ChargeStatD();
         csd.BitFields = txtBUBChargingStatus.Text;
         csd.ShowDialog();
      }

      private void btnBUBBattModeD_Click( object sender, EventArgs e )
      {
         BattModeD bmd = new BattModeD();
         bmd.BitFields = txtBUBBatteryMode.Text;
         bmd.ShowDialog();
      }

      private void btnGetCaptureURL_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6k2" );
      }

      private void btnSetCaptureURL_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6k1" + txtCaptureURL.Text );
      }

      private void btnCommFromFlash_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "6l1" );
      }




      private void btnMotorOut_Click( object sender, EventArgs e )
      {
         if ( rdoMotor1.Checked )
            SendDeviceCommand( "5H01" );
         else if ( rdoMotor2.Checked )
            SendDeviceCommand( "5H02" );
         else if ( rdoMotor3.Checked )
            SendDeviceCommand( "5H03" );
         else if ( rdoMotor4.Checked )
            SendDeviceCommand( "5H04" );
         else if ( rdoMotor5.Checked )
            SendDeviceCommand( "5H05" );
         else if ( rdoMotor6.Checked )
            SendDeviceCommand( "5H06" );
         else if ( rdoMotor7.Checked )
            SendDeviceCommand( "5H07" );
         else if ( rdoMotor8.Checked )
            SendDeviceCommand( "5H08" );
      }




      private void btnMotorIn_Click( object sender, EventArgs e )
      {
         if ( rdoMotor1.Checked )
            SendDeviceCommand( "5I01" );
         else if ( rdoMotor2.Checked )
            SendDeviceCommand( "5I02" );
         else if ( rdoMotor3.Checked )
            SendDeviceCommand( "5I03" );
         else if ( rdoMotor4.Checked )
            SendDeviceCommand( "5I04" );
         else if ( rdoMotor5.Checked )
            SendDeviceCommand( "5I05" );
         else if ( rdoMotor6.Checked )
            SendDeviceCommand( "5I06" );
         else if ( rdoMotor7.Checked )
            SendDeviceCommand( "5I07" );
         else if ( rdoMotor8.Checked )
            SendDeviceCommand( "5I08" );
      }



      private void btnMotorMove_Click( object sender, EventArgs e )
      {
         if ( rdoMotor1.Checked )
            SendDeviceCommand( "5K01" );
         else if ( rdoMotor2.Checked )
            SendDeviceCommand( "5K02" );
         else if ( rdoMotor3.Checked )
            SendDeviceCommand( "5K03" );
         else if ( rdoMotor4.Checked )
            SendDeviceCommand( "5K04" );
         else if ( rdoMotor5.Checked )
            SendDeviceCommand( "5K05" );
         else if ( rdoMotor6.Checked )
            SendDeviceCommand( "5K06" );
         else if ( rdoMotor7.Checked )
            SendDeviceCommand( "5K07" );
         else if ( rdoMotor8.Checked )
            SendDeviceCommand( "5K08" );
      }




      private void btnReleaseCassette_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5H0m" );
      }




      private void btnGetBuzzerMuteTime_Click( object sender, EventArgs e )
      {
         SendDeviceCommand( "5q2" );
      }




      private void btnSetBuzzerMuteTime_Click( object sender, EventArgs e )
      {
         string scomm = "";
         int mtime = 0;

         if ( txtBuzzerMuteTime.Text.Length < 2 )
         {
            MessageBox.Show( "Buzzer Mute Time must be between 10 and 600 seconds.", "Error" );
            return;
         }

         try
         {
            mtime = Convert.ToInt32( txtBuzzerMuteTime.Text );
         }
         catch
         {
            mtime = 0;
         }

         if ( mtime < 10 || mtime > 600 )
         {
            MessageBox.Show( "Buzzer Mute Time must be between 10 and 600 seconds.", "Error" );
            return;
         }

         if ( txtBuzzerMuteTime.Text.Length == 2 )
            scomm = "0" + txtBuzzerMuteTime.Text;
         else
            scomm = txtBuzzerMuteTime.Text;

         SendDeviceCommand( "5r1" + scomm );
      }




      private void btnIsDrawerOut_Click( object sender, EventArgs e )
      {
         if ( rdoMotor1.Checked )
            SendDeviceCommand( "5Q01" );
         else if ( rdoMotor2.Checked )
            SendDeviceCommand( "5Q02" );
         else if ( rdoMotor3.Checked )
            SendDeviceCommand( "5Q03" );
         else if ( rdoMotor4.Checked )
            SendDeviceCommand( "5Q04" );
         else if ( rdoMotor5.Checked )
            SendDeviceCommand( "5Q05" );
         else if ( rdoMotor6.Checked )
            SendDeviceCommand( "5Q06" );
         else if ( rdoMotor7.Checked )
            SendDeviceCommand( "5Q07" );
         else if ( rdoMotor8.Checked )
            SendDeviceCommand( "5Q08" );
      }


      
      
      private void btnIsDrawerIn_Click( object sender, EventArgs e )
      {
         if ( rdoMotor1.Checked )
            SendDeviceCommand( "5R01" );
         else if ( rdoMotor2.Checked )
            SendDeviceCommand( "5R02" );
         else if ( rdoMotor3.Checked )
            SendDeviceCommand( "5R03" );
         else if ( rdoMotor4.Checked )
            SendDeviceCommand( "5R04" );
         else if ( rdoMotor5.Checked )
            SendDeviceCommand( "5R05" );
         else if ( rdoMotor6.Checked )
            SendDeviceCommand( "5R06" );
         else if ( rdoMotor7.Checked )
            SendDeviceCommand( "5R07" );
         else if ( rdoMotor8.Checked )
            SendDeviceCommand( "5R08" );
      }






   }
}
/*--- end of MobiusUSB.cs -------------------------------------------------------*/

