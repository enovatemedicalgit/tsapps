﻿﻿﻿﻿﻿﻿namespace MobiusUSB
{
   partial class BattModeD
   {
      /// <summary>
      /// Required designer variable.
      /// </summary>
      private System.ComponentModel.IContainer components = null;

      /// <summary>
      /// Clean up any resources being used.
      /// </summary>
      /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
      protected override void Dispose( bool disposing )
      {
         if ( disposing && ( components != null ) )
         {
            components.Dispose();
         }
         base.Dispose( disposing );
      }

      #region Windows Form Designer generated code

      /// <summary>
      /// Required method for Designer support - do not modify
      /// the contents of this method with the code editor.
      /// </summary>
      private void InitializeComponent()
      {
         this.components = new System.ComponentModel.Container();
         this.btnClose = new System.Windows.Forms.Button();
         this.label1 = new System.Windows.Forms.Label();
         this.label2 = new System.Windows.Forms.Label();
         this.label3 = new System.Windows.Forms.Label();
         this.label4 = new System.Windows.Forms.Label();
         this.label5 = new System.Windows.Forms.Label();
         this.label6 = new System.Windows.Forms.Label();
         this.label7 = new System.Windows.Forms.Label();
         this.label8 = new System.Windows.Forms.Label();
         this.label9 = new System.Windows.Forms.Label();
         this.label10 = new System.Windows.Forms.Label();
         this.label11 = new System.Windows.Forms.Label();
         this.label12 = new System.Windows.Forms.Label();
         this.label13 = new System.Windows.Forms.Label();
         this.label14 = new System.Windows.Forms.Label();
         this.label15 = new System.Windows.Forms.Label();
         this.label16 = new System.Windows.Forms.Label();
         this.label17 = new System.Windows.Forms.Label();
         this.label18 = new System.Windows.Forms.Label();
         this.label19 = new System.Windows.Forms.Label();
         this.label20 = new System.Windows.Forms.Label();
         this.label21 = new System.Windows.Forms.Label();
         this.label22 = new System.Windows.Forms.Label();
         this.label23 = new System.Windows.Forms.Label();
         this.label24 = new System.Windows.Forms.Label();
         this.label25 = new System.Windows.Forms.Label();
         this.label26 = new System.Windows.Forms.Label();
         this.toolTip1 = new System.Windows.Forms.ToolTip( this.components );
         this.txtBitField = new System.Windows.Forms.TextBox();
         this.SuspendLayout();
         // 
         // btnClose
         // 
         this.btnClose.Anchor = ( (System.Windows.Forms.AnchorStyles)( ( System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right ) ) );
         this.btnClose.Location = new System.Drawing.Point( 356, 89 );
         this.btnClose.Name = "btnClose";
         this.btnClose.Size = new System.Drawing.Size( 75, 23 );
         this.btnClose.TabIndex = 0;
         this.btnClose.Text = "Close";
         this.btnClose.UseVisualStyleBackColor = true;
         this.btnClose.Click += new System.EventHandler( this.btnClose_Click );
         // 
         // label1
         // 
         this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label1.Location = new System.Drawing.Point( 60, 30 );
         this.label1.Name = "label1";
         this.label1.Size = new System.Drawing.Size( 46, 18 );
         this.label1.TabIndex = 1;
         this.label1.Text = "CapM";
         this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label1, "Units for Capacity Information: mA or mW" );
         // 
         // label2
         // 
         this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label2.Location = new System.Drawing.Point( 106, 30 );
         this.label2.Name = "label2";
         this.label2.Size = new System.Drawing.Size( 46, 18 );
         this.label2.TabIndex = 2;
         this.label2.Text = "ChgM";
         this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label2, "Enables or Disables the transmission of Charging Current and Voltage" );
         // 
         // label3
         // 
         this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label3.Location = new System.Drawing.Point( 152, 30 );
         this.label3.Name = "label3";
         this.label3.Size = new System.Drawing.Size( 46, 18 );
         this.label3.TabIndex = 3;
         this.label3.Text = "AM";
         this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label3, "Alarm Warning Broadcasts" );
         // 
         // label4
         // 
         this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label4.Location = new System.Drawing.Point( 198, 30 );
         this.label4.Name = "label4";
         this.label4.Size = new System.Drawing.Size( 46, 18 );
         this.label4.TabIndex = 4;
         this.label4.Text = "RSVD";
         this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label4, "Reserved" );
         // 
         // label5
         // 
         this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label5.Location = new System.Drawing.Point( 244, 30 );
         this.label5.Name = "label5";
         this.label5.Size = new System.Drawing.Size( 46, 18 );
         this.label5.TabIndex = 5;
         this.label5.Text = "RSVD";
         this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label5, "Reserved" );
         // 
         // label6
         // 
         this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label6.Location = new System.Drawing.Point( 290, 30 );
         this.label6.Name = "label6";
         this.label6.Size = new System.Drawing.Size( 46, 18 );
         this.label6.TabIndex = 6;
         this.label6.Text = "RSVD";
         this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label6, "Reserved" );
         // 
         // label7
         // 
         this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label7.Location = new System.Drawing.Point( 336, 30 );
         this.label7.Name = "label7";
         this.label7.Size = new System.Drawing.Size( 46, 18 );
         this.label7.TabIndex = 7;
         this.label7.Text = "PB";
         this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label7, "Sets the Role of the Battery Pack" );
         // 
         // label8
         // 
         this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label8.Location = new System.Drawing.Point( 382, 30 );
         this.label8.Name = "label8";
         this.label8.Size = new System.Drawing.Size( 46, 18 );
         this.label8.TabIndex = 8;
         this.label8.Text = "CC";
         this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label8, "Internal Charge Controller" );
         // 
         // label9
         // 
         this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label9.Location = new System.Drawing.Point( 60, 48 );
         this.label9.Name = "label9";
         this.label9.Size = new System.Drawing.Size( 46, 18 );
         this.label9.TabIndex = 9;
         this.label9.Text = "CF";
         this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label9, "Condition Cycle Requested" );
         // 
         // label10
         // 
         this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label10.Location = new System.Drawing.Point( 106, 48 );
         this.label10.Name = "label10";
         this.label10.Size = new System.Drawing.Size( 46, 18 );
         this.label10.TabIndex = 10;
         this.label10.Text = "RSVD";
         this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label10, "Reserved" );
         // 
         // label11
         // 
         this.label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label11.Location = new System.Drawing.Point( 152, 48 );
         this.label11.Name = "label11";
         this.label11.Size = new System.Drawing.Size( 46, 18 );
         this.label11.TabIndex = 11;
         this.label11.Text = "RSVD";
         this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label11, "Reserved" );
         // 
         // label12
         // 
         this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label12.Location = new System.Drawing.Point( 198, 48 );
         this.label12.Name = "label12";
         this.label12.Size = new System.Drawing.Size( 46, 18 );
         this.label12.TabIndex = 12;
         this.label12.Text = "RSVD";
         this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label12, "Reserved" );
         // 
         // label13
         // 
         this.label13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label13.Location = new System.Drawing.Point( 244, 48 );
         this.label13.Name = "label13";
         this.label13.Size = new System.Drawing.Size( 46, 18 );
         this.label13.TabIndex = 13;
         this.label13.Text = "RSVD";
         this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label13, "Reserved" );
         // 
         // label14
         // 
         this.label14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label14.Location = new System.Drawing.Point( 290, 48 );
         this.label14.Name = "label14";
         this.label14.Size = new System.Drawing.Size( 46, 18 );
         this.label14.TabIndex = 14;
         this.label14.Text = "RSVD";
         this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label14, "Reserved" );
         // 
         // label15
         // 
         this.label15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label15.Location = new System.Drawing.Point( 336, 48 );
         this.label15.Name = "label15";
         this.label15.Size = new System.Drawing.Size( 46, 18 );
         this.label15.TabIndex = 15;
         this.label15.Text = "PBS";
         this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label15, "Primary Battery Support" );
         // 
         // label16
         // 
         this.label16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
         this.label16.Location = new System.Drawing.Point( 382, 48 );
         this.label16.Name = "label16";
         this.label16.Size = new System.Drawing.Size( 46, 18 );
         this.label16.TabIndex = 16;
         this.label16.Text = "ICC";
         this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
         this.toolTip1.SetToolTip( this.label16, "Supports Internal Charge Controller" );
         // 
         // label17
         // 
         this.label17.AutoSize = true;
         this.label17.Location = new System.Drawing.Point( 76, 15 );
         this.label17.Name = "label17";
         this.label17.Size = new System.Drawing.Size( 13, 13 );
         this.label17.TabIndex = 17;
         this.label17.Text = "7";
         // 
         // label18
         // 
         this.label18.AutoSize = true;
         this.label18.Location = new System.Drawing.Point( 123, 15 );
         this.label18.Name = "label18";
         this.label18.Size = new System.Drawing.Size( 13, 13 );
         this.label18.TabIndex = 18;
         this.label18.Text = "6";
         // 
         // label19
         // 
         this.label19.AutoSize = true;
         this.label19.Location = new System.Drawing.Point( 167, 15 );
         this.label19.Name = "label19";
         this.label19.Size = new System.Drawing.Size( 13, 13 );
         this.label19.TabIndex = 19;
         this.label19.Text = "5";
         // 
         // label20
         // 
         this.label20.AutoSize = true;
         this.label20.Location = new System.Drawing.Point( 214, 15 );
         this.label20.Name = "label20";
         this.label20.Size = new System.Drawing.Size( 13, 13 );
         this.label20.TabIndex = 20;
         this.label20.Text = "4";
         // 
         // label21
         // 
         this.label21.AutoSize = true;
         this.label21.Location = new System.Drawing.Point( 259, 15 );
         this.label21.Name = "label21";
         this.label21.Size = new System.Drawing.Size( 13, 13 );
         this.label21.TabIndex = 21;
         this.label21.Text = "3";
         // 
         // label22
         // 
         this.label22.AutoSize = true;
         this.label22.Location = new System.Drawing.Point( 306, 15 );
         this.label22.Name = "label22";
         this.label22.Size = new System.Drawing.Size( 13, 13 );
         this.label22.TabIndex = 22;
         this.label22.Text = "2";
         // 
         // label23
         // 
         this.label23.AutoSize = true;
         this.label23.Location = new System.Drawing.Point( 352, 15 );
         this.label23.Name = "label23";
         this.label23.Size = new System.Drawing.Size( 13, 13 );
         this.label23.TabIndex = 23;
         this.label23.Text = "1";
         // 
         // label24
         // 
         this.label24.AutoSize = true;
         this.label24.Location = new System.Drawing.Point( 397, 15 );
         this.label24.Name = "label24";
         this.label24.Size = new System.Drawing.Size( 13, 13 );
         this.label24.TabIndex = 24;
         this.label24.Text = "0";
         // 
         // label25
         // 
         this.label25.AutoSize = true;
         this.label25.Location = new System.Drawing.Point( 5, 33 );
         this.label25.Name = "label25";
         this.label25.Size = new System.Drawing.Size( 53, 13 );
         this.label25.TabIndex = 25;
         this.label25.Text = "High Byte";
         // 
         // label26
         // 
         this.label26.AutoSize = true;
         this.label26.Location = new System.Drawing.Point( 7, 50 );
         this.label26.Name = "label26";
         this.label26.Size = new System.Drawing.Size( 51, 13 );
         this.label26.TabIndex = 26;
         this.label26.Text = "Low Byte";
         // 
         // toolTip1
         // 
         this.toolTip1.AutomaticDelay = 100;
         this.toolTip1.AutoPopDelay = 8000;
         this.toolTip1.InitialDelay = 200;
         this.toolTip1.ReshowDelay = 100;
         this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
         // 
         // txtBitField
         // 
         this.txtBitField.Location = new System.Drawing.Point( 60, 83 );
         this.txtBitField.Name = "txtBitField";
         this.txtBitField.ReadOnly = true;
         this.txtBitField.Size = new System.Drawing.Size( 50, 20 );
         this.txtBitField.TabIndex = 27;
         this.txtBitField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
         // 
         // BattModeD
         // 
         this.AutoScaleDimensions = new System.Drawing.SizeF( 6F, 13F );
         this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
         this.ClientSize = new System.Drawing.Size( 443, 124 );
         this.Controls.Add( this.txtBitField );
         this.Controls.Add( this.label26 );
         this.Controls.Add( this.label25 );
         this.Controls.Add( this.label24 );
         this.Controls.Add( this.label23 );
         this.Controls.Add( this.label22 );
         this.Controls.Add( this.label21 );
         this.Controls.Add( this.label20 );
         this.Controls.Add( this.label19 );
         this.Controls.Add( this.label18 );
         this.Controls.Add( this.label17 );
         this.Controls.Add( this.label16 );
         this.Controls.Add( this.label15 );
         this.Controls.Add( this.label14 );
         this.Controls.Add( this.label13 );
         this.Controls.Add( this.label12 );
         this.Controls.Add( this.label11 );
         this.Controls.Add( this.label10 );
         this.Controls.Add( this.label9 );
         this.Controls.Add( this.label8 );
         this.Controls.Add( this.label7 );
         this.Controls.Add( this.label6 );
         this.Controls.Add( this.label5 );
         this.Controls.Add( this.label4 );
         this.Controls.Add( this.label3 );
         this.Controls.Add( this.label2 );
         this.Controls.Add( this.label1 );
         this.Controls.Add( this.btnClose );
         this.Name = "BattModeD";
         this.Text = "Battery Mode";
         this.Load += new System.EventHandler( this.BattModeD_Load );
         this.ResumeLayout( false );
         this.PerformLayout();

      }

      #endregion

      private System.Windows.Forms.Button btnClose;
      private System.Windows.Forms.Label label1;
      private System.Windows.Forms.Label label2;
      private System.Windows.Forms.Label label3;
      private System.Windows.Forms.Label label4;
      private System.Windows.Forms.Label label5;
      private System.Windows.Forms.Label label6;
      private System.Windows.Forms.Label label7;
      private System.Windows.Forms.Label label8;
      private System.Windows.Forms.Label label9;
      private System.Windows.Forms.Label label10;
      private System.Windows.Forms.Label label11;
      private System.Windows.Forms.Label label12;
      private System.Windows.Forms.Label label13;
      private System.Windows.Forms.Label label14;
      private System.Windows.Forms.Label label15;
      private System.Windows.Forms.Label label16;
      private System.Windows.Forms.Label label17;
      private System.Windows.Forms.Label label18;
      private System.Windows.Forms.Label label19;
      private System.Windows.Forms.Label label20;
      private System.Windows.Forms.Label label21;
      private System.Windows.Forms.Label label22;
      private System.Windows.Forms.Label label23;
      private System.Windows.Forms.Label label24;
      private System.Windows.Forms.Label label25;
      private System.Windows.Forms.Label label26;
      private System.Windows.Forms.ToolTip toolTip1;
      private System.Windows.Forms.TextBox txtBitField;
   }
}
