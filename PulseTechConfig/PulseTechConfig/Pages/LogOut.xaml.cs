﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;
using PulseTechConfig.Common;

namespace PulseTechConfig.Pages
{
    /// <summary>
    /// Interaction logic for LogOut.xaml
    /// </summary>
    public partial class LogOut : UserControl, IContent
    {
        public LogOut()
        {
            InitializeComponent();
        }

        private void BtnLogout_OnClick(object sender, RoutedEventArgs e)
        {
            PulseTechGlobals.UserLoggedIn = false;
            string url = "/Pages/Home.xaml";
            BBCodeBlock bbBlock = new BBCodeBlock();
            bbBlock.LinkNavigator.Navigate(new Uri(url, UriKind.Relative), this, NavigationHelper.FrameSelf);
        }

        private void BtnCancel_OnClick(object sender, RoutedEventArgs e)
        {

            var x = Parent;
            
            string url = "/Pages/LoggedInHome.xaml";
            BBCodeBlock bbBlock = new BBCodeBlock();
            var mw = Application.Current.MainWindow;

            //var frame = Application.Current.MainWindow.FindChild<ModernFrame>("ContentFrame");
            //var field = typeof(ModernFrame).GetField("history", BindingFlags.NonPublic | BindingFlags.Instance);
            //var history = (Stack<Uri>)field.GetValue(frame);

            
            bbBlock.LinkNavigator.Navigate(new Uri(url, UriKind.Relative), this, NavigationHelper.FrameSelf);
            

            //var mw = Application.Current.MainWindow;
            //NavigationWindow nav = mw as NavigationWindow;
            //nav.Navigate(new LoggedInHome());

            //NavigationService ns = NavigationService.GetNavigationService(mw);
            //if (ns.CanGoBack)
            //{
            //    ns.GoBack();   
            //}
            //var x = (FirstFloor.ModernUI.Windows.Controls.ModernFrame) this.Parent;
            
            ////var history = (new System.Collections.Generic.System_StackDebugView<System.Uri>(x.history)).Items[0].m_String;
            
            
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            Debug.WriteLine("test");
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("test");
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("test");
            
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            
            Debug.WriteLine("test");
        }
    }
}
