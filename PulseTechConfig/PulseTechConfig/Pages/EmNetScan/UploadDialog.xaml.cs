﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTechConfig.Common;
using Telerik.Windows.Documents.Fixed;
using Application = System.Windows.Application;

namespace PulseTechConfig.Pages.EmNetScan
{
    /// <summary>
    /// Interaction logic for UploadDialog.xaml
    /// </summary>
    public partial class UploadDialog : ModernWindow
    {
        private string _extension = "Ns1";
        private string _selectedFile = "";
        private HttpResponseMessage _response = new HttpResponseMessage();
        private string _siteId = "";

        public UploadDialog()
        {
            InitializeComponent();
            PulseTechGlobals.ExitOperation = false;
        }

        private void CmdUpload_OnClick(object sender, RoutedEventArgs e)
        {
            PulseTechGlobals.SelectedFloor = PulseTechGlobals.SelectedFloor;
            PulseTechGlobals.SelectedWing = PulseTechGlobals.SelectedWing;
            if (PulseTechGlobals.SelectedSite == "")
            {
                PulseTechGlobals.MsgBox("Select Site", "You must select a Site first");
                return;
            }

            OpenFileDialog dialog = new OpenFileDialog()
            {
                DefaultExt = _extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", _extension, "Ns1"),
                FilterIndex = 1
            };
            DialogResult result = dialog.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                _selectedFile = dialog.FileName;
                ProgressRing.IsActive = true;
                var task = Task.Factory.StartNew(UploadFile, TaskCreationOptions.LongRunning).ContinueWith(EndUpload);
            }

        }

        private void UploadFile()
        {
            string filename = _selectedFile;
            string site = Sites.SelectedSite;

            if (string.IsNullOrEmpty(site))
            {
                PulseTechGlobals.MsgBox("Select Site", "You must select a Site first");
                return;
            }
            byte[] bData = System.IO.File.ReadAllBytes(filename);

            
            System.Net.Http.HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://nnt.myenovate.com/");

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var parms = new Dictionary<string, object>();
            string myData = Convert.ToBase64String(bData);
            parms.Add("SiteId", PulseTechGlobals.SelectedSite);
            parms.Add("Data", myData);
            parms.Add("UserId", PulseTechGlobals.UserId.ToString());
            parms.Add("SignalThreshhold", "70");
            //test
            _response = client.PostAsJsonAsync("api/ScanData/", parms).Result;
           

        }

        private void EndUpload(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                if (_response.IsSuccessStatusCode)
                {
                    PulseTechGlobals.MsgBox("Success", "Successfully Added AP Scan to Site " );
                }
                else
                {
                    PulseTechGlobals.MsgBox("Error on Import", " Code" + _response.StatusCode + " : Message - " + _response.ReasonPhrase);
                }
            }));
        }

        private void CmdExit_OnClick(object sender, RoutedEventArgs e)
        {
            PulseTechGlobals.ExitOperation = true;
            this.Close();
        }

        private void Sites_OnSiteChanged(object sender, SiteSelectEventArgs e)
        {
            _siteId = e.SiteId.ToString();
            CbxSiteFloor.LoadFloors(e.SiteId.ToString());
            CbxSiteWing.LoadWings(e.SiteId.ToString());
            
        }
    }
}
