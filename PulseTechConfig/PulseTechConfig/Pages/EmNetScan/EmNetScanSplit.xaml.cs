﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using PulseTech.SMnetScan;
using PulseTechConfig.Common;
using Telerik.Charting;
using Telerik.Windows.Controls.ChartView;
using Application = System.Windows.Application;
using Button = System.Windows.Controls.Button;
using Type = System.Type;
using UserControl = System.Windows.Controls.UserControl;

namespace PulseTechConfig.Pages.EmNetScan
{
    /// <summary>
    /// Interaction logic for EmNetScanSplit.xaml
    /// </summary>
    public partial class EmNetScanSplit : UserControl, IContent
    {
        private readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer =
            new System.Windows.Threading.DispatcherTimer();

        public List<WirelessInfo> ApInfoList = new List<WirelessInfo>();
        private NetworkApInfo _ap = new NetworkApInfo();
        public DataTable ApDataTable = new DataTable();
        private bool _showUnknown = false;
        private Ns1File _exportFile;
        private string _extension = "ns1";
        private bool _scanningActive = false;

        enum Type { Ap, AdHoc, Other }

        public EmNetScanSplit()
        {
            InitializeComponent();
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 3);
            
        }


        private void CmdStart_OnClick(object sender, RoutedEventArgs e)
        {

            StartScanning();
        }

        private void StartScanning()
        {
            if (_scanningActive) return;
            _scanningActive = true;
            CmdStop.Visibility = Visibility.Visible;
            CmdStart.Visibility = Visibility.Hidden;
            lblStatus.Content = "Scanning...";
            _showUnknown = Convert.ToBoolean(ChkShowUnknown.IsChecked);
            CheckNetwork();
            UpdatePolarChart();

        }

        private void UpdatePolarChart()
        {
            ClearPolar();
        }

        private void CheckNetwork()
        {
            _dispatcherTimer.Stop();
            
            var task = Task.Factory.StartNew(StartNetworkScan, TaskCreationOptions.LongRunning).ContinueWith(EndNetworkScan);
        }

        private void CmdStop_OnClick(object sender, RoutedEventArgs e)
        {
            _scanningActive = false;
            CmdStop.Visibility = Visibility.Hidden;
            CmdStart.Visibility = Visibility.Visible;
            lblStatus.Content = "Not Scanning";
            ClearPolar();
            PolarPaused();
            ExportNsFile();
        }

        private void ClearPolar()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                int cnt = PointSeries.DataPoints.Count;
                for (int p = 0; p < cnt; p++)
                {
                    PointSeries.DataPoints.RemoveAt(0);
                    APpolar.Annotations.RemoveAt(0);
                }               
            }));
        }

        private void PolarPaused()
        {
            PolarDataPoint paused = new PolarDataPoint
            {
                Angle = 0,
                Value = 0,
                Label = "Not Scanning"
            };
            PointSeries.DataPoints.Add(paused);
            PolarCustomAnnotation pausedAnnotation = new PolarCustomAnnotation
            {
                PolarValue = 10,
                RadialValue = 10,
                Content = "Paused..."
            };
            APpolar.Annotations.Add(pausedAnnotation);
        }

        private void AddPoint(string label, int linkQuality, int angle)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (linkQuality > 99) linkQuality = 80;
                PolarDataPoint ptDataPoint = new PolarDataPoint
                {
                    Angle = angle,
                    Value = 100 - linkQuality,
                    Label = label
                };
                PointSeries.DataPoints.Add(ptDataPoint);
                PolarCustomAnnotation ptAnnotation = new PolarCustomAnnotation
                {
                    PolarValue = 100 - linkQuality,
                    RadialValue = angle + 10,
                    Content = label
                };
                APpolar.Annotations.Add(ptAnnotation);
            }));
        }

        private void StartNetworkScan()
        {
            _ap = new NetworkApInfo();
            ApInfoList = _ap.GetApList(_showUnknown);
            //LoadPolar();
            //ApDataTable = ApInfoList.ToDataTable();
            //ApDataTable.DefaultView.Sort = "Id";
        }

        private void LoadPolar()
        {
            ClearPolar();
            Dictionary<string,string> aps = new Dictionary<string, string>();
            int angle = 0;
            foreach (WirelessInfo wi in ApInfoList)
            {
                if (wi.SSID != "")
                {
                    if (!aps.ContainsKey(wi.SSID))
                    {
                        if (wi.SSID == "Unknown")
                        {
                            if (Convert.ToBoolean(ChkShowUnknown.IsChecked))
                            {
                                aps.Add(wi.SSID, wi.SSID);
                                AddPoint(wi.SSID, wi.LinkQuality, angle);
                                angle += 60;
                                if (angle > 359) angle = 10;                                
                            }
                        }
                        else                            
                        {
                            aps.Add(wi.SSID, wi.SSID);
                            AddPoint(wi.SSID, wi.LinkQuality, angle);
                            angle += 60;
                            if (angle > 359) angle = 10;
                        }
                    }
                }
            }
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            CheckNetwork();
            //_dispatcherTimer.Stop();
            //DataTable dt = ToDataTable(UsbCommon.AssetStatus);
            // LoadFromUsb();
        }


        private void EndNetworkScan(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                LoadPolar();
                ApDataTable = ApInfoList.ToDataTable();
                ApDataTable.DefaultView.Sort = "Id";

                DataGridNetworkDetails.ItemsSource = null;
                DataGridNetworkDetails.ItemsSource = ApDataTable.DefaultView;

            }));

            _dispatcherTimer.Start();
        }

        private void ExportNsFile()
        {
            _exportFile = new Ns1File(ApDataTable.Rows.Count);

            try
            {
                for (int i = 0; i < ApDataTable.Rows.Count; i++)
                {
                    DataRow dr = ApDataTable.Rows[i];
                    //string[] latlon = dr["locationColumn"].Value.ToString().Split(',');
                    Type apType = Type.Other;
                    double lat = 0.0;
                    double lon = 0.0;

                            bool enc = dr["security"].ToString() != "None";
                            _exportFile.Add(dr["SSID"].ToString(),
                                           dr["apMac"].ToString(),
                                           Convert.ToInt32(dr["LinkQuality"]),
                                           dr["firstSeen"].ToString(),
                                           dr["lastSeen"].ToString(),
                                           Convert.ToUInt32(dr["channel"]),
                                           Convert.ToUInt32(dr["speed"]),
                                           enc,
                                           lat,
                                           lon,
                                           (int)Type.Ap);
                }
                var dlg = new ModernDialog
                {
                    Title = "Export Network Scan",
                    Content = "Export Network Scan"
                };
                dlg.Buttons = new Button[] { dlg.OkButton, dlg.CancelButton };
                dlg.ShowDialog();

                if (Convert.ToBoolean(dlg.DialogResult.HasValue ? dlg.DialogResult.ToString() : "<null>"))
                {
                    ExportScans();
                }
                //this.dialogResult.Text = dlg.DialogResult.HasValue ? dlg.DialogResult.ToString() : "<null>";
                //this.dialogMessageBoxResult.Text = dlg.MessageBoxResult.ToString();

            //    exportFile.WriteOut(sdlgExport.FileName);
            }
            catch
            {
                PulseTechGlobals.MsgBox("NS1 Export Failed","The data could not be exported") ;
            }
        }

        private void ExportScans()
        {
            
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = _extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", _extension, "Ns1"),
                FilterIndex = 1
            };
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                _exportFile.WriteOut(dialog.FileName);
                PulseTechGlobals.MsgBox("Export Complete", "Network Scan file successfully exported");
            }
        }


        private void CmdUpload_OnClick(object sender, RoutedEventArgs e)
        {
            UploadDialog up = new UploadDialog();

            up.ShowDialog();

            if (PulseTechGlobals.ExitOperation) return;
            
            
        }

        private void ChkShowUnknown_OnChecked(object sender, RoutedEventArgs e)
        {
            _showUnknown = Convert.ToBoolean(ChkShowUnknown.IsChecked);
            StartNetworkScan();
        }

        private void ChkShowUnknown_OnUnchecked(object sender, RoutedEventArgs e)
        {
            _showUnknown = Convert.ToBoolean(ChkShowUnknown.IsChecked);
            StartNetworkScan();
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            StartScanning();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            
        }
    }
}