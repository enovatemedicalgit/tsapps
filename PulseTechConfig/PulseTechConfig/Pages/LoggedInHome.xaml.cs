﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;
using PulseTech;
using PulseTechConfig.Common;

namespace PulseTechConfig.Pages
{
    /// <summary>
    /// Interaction logic for LoggedInHome.xaml
    /// </summary>
    public partial class LoggedInHome : UserControl, IContent
    {
        private string _result = "Not Connected";
        private string _lastSerialNo = "";
        private DataTable _usbDataTable;
        readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer = new System.Windows.Threading.DispatcherTimer();

        public LoggedInHome()
        {
            InitializeComponent();
            TextLoggedInAs.Text = PulseTechGlobals.UserName;
        }


        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            Debug.WriteLine("Onfragment");
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("OnNavigatedFrom");
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            if (PulseTechGlobals.UserLoggedIn)
            {
                try
                {
                    string url = "/Pages/LogOut.xaml";
                    NavigationWindow nav = this.Parent as NavigationWindow;
                    nav.Navigate(new System.Uri(url, UriKind.RelativeOrAbsolute));


                    //BBCodeBlock bbBlock = new BBCodeBlock();
                    //IInputElement target = NavigationHelper.FindFrame("_top", this);
                    //bbBlock.LinkNavigator.Navigate(new Uri("/Pages/LogOut.xaml", UriKind.Relative), this, NavigationHelper.FrameSelf);
                    //bbBlock.LinkNavigator.Navigate(new Uri("/Pages/LogOut.xaml", UriKind.Relative), this,
                    //    NavigationHelper.FindFrame("_top", this);
                    //                    IInputElement target = NavigationHelper.FindFrame("_top", this);
                    ////NavigationCommands.GoToPage.Execute("/Pages/LoggedInHome.xaml", target);
                    //NavigationCommands.GoToPage.Execute("/Pages/RhythmField/RhythmMain.xaml", target);


                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
            Debug.WriteLine("OnNavigatedTo");
           //var task = Task.Factory.StartNew(ConnectUsbDevice, TaskCreationOptions.LongRunning).ContinueWith(EndUpdate);
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 2);
            _dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _dispatcherTimer.Stop();
            AssetInfo.StartConnection();
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            Debug.WriteLine("OnNavigatingFrom");
        }

        //private void ConnectUsbDevice()
        //{
        //    _result = UsbCommon.AttemptConnect();
        //}
        //private void LoadGrid()
        //{
        //    Debug.WriteLine("Loading grid...");
        //    UsbCommon.SendCommonCommands();
            
        //}

        //private void EndUpdate(Task tsk)
        //{
        //    SetUsbStatus();

        //    if (UsbCommon.UsbConnected)
        //    {
        //        LoadGrid();
        //    }

        //}

        //private void SetUsbStatus()
        //{
        //    Application.Current.Dispatcher.Invoke(new Action(() =>
        //    {
        //        TxtConnected.Text = _result;
        //        if (UsbCommon.UsbConnected) TxtConnected.Foreground = new SolidColorBrush(Colors.DarkGreen);
        //    }));
        //}

      

        //private void LoadFromUsb(){

        //    _usbDataTable = ObjectToData(UsbCommon.AssetStatus);
        //    DataRow row = _usbDataTable.Rows[0];
        //    //var dictionary = row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row.Field<string>(col.ColumnName));
        //    DGMobuisDetails.ItemsSource = _usbDataTable.DefaultView;
        //    if (UsbCommon.AssetStatus.DeviceSerial != "")
        //    {
        //        if (UsbCommon.AssetStatus.DeviceSerial == null) return;
        //        if (UsbCommon.AssetStatus.DeviceSerial != _lastSerialNo)
        //        {
        //            ApiLayer api = new ApiLayer();
        //            var parms = new Dictionary<string, string>();
        //            parms.Add("SerialNo", UsbCommon.AssetStatus.DeviceSerial);
        //            try
        //            {

        //                DataTable _dt = api.ExecStoredProc("[prcGetAssetType]", false, parms);
        //                if (_usbDataTable.Rows.Count > 0)
        //                {
        //                    UsbCommon.AssetStatus.DeviceType = _dt.Rows[0]["Description"].ToString();
        //                    _usbDataTable = ObjectToData(UsbCommon.AssetStatus);
        //                    DGMobuisDetails.ItemsSource = null;
        //                    DGMobuisDetails.ItemsSource = _usbDataTable.DefaultView;
        //                }
        //            }
        //            catch
        //            {
        //                //PulseTechGlobals.MsgBox("No Workstations Found", "Workstation Lookup") ;
        //            }
        //        }
        //    }
        //}


        //private DataTable ToDataTable(AssetUsbStatus aStatus)
        //{
        //    DataTable dt = new DataTable();
        //    dt.Columns.Add("ItemName");
        //    dt.Columns.Add("ItemValue");
        //    foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(aStatus))
        //    {
        //        AssetKv kv = new AssetKv
        //        {
        //            ItemName = prop.Name,
        //            ItemValue = prop.GetValue(aStatus).ToString() ?? ""
        //        };
                
        //    }
        //    return dt;
        //}


        //public static DataTable ObjectToData(object o)
        //{
        //    DataTable dt = new DataTable("OutputData");

        //    DataRow dr = dt.NewRow();
        //    dt.Columns.Add("ItemName", typeof(string));
        //    dt.Columns.Add("ItemValue", typeof(string));
        //    //dt.Rows.Add(dr);

        //    //o.GetType().GetProperties().ToList().ForEach(f =>
        //    //{
        //    //    try
        //    //    {
        //    //        f.GetValue(o, null);
        //    //        dt.Columns.Add(f.Name, f.PropertyType);
        //    //        dt.Rows[0][f.Name] = f.GetValue(o, null);
        //    //    }
        //    //    catch { }
        //    //});

        //    foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(o))
        //    {

        //        dr = dt.NewRow();
        //        dr[0] = prop.Name;
        //        dr[1] = prop.GetValue(o) == null ? "" : prop.GetValue(o).ToString();
        //        dt.Rows.Add(dr);

        //    }
        //    return dt;
        //}
        //public class AssetKv
        //{
        //    public string ItemName;
        //    public string ItemValue;
        //}

       

        //private void CmdGetInfo_OnClick(object sender, RoutedEventArgs e)
        //{
        //    UsbCommon.AttemptConnect();
        //    SetUsbStatus();
        //    LoadFromUsb();
        //}
    }
}

