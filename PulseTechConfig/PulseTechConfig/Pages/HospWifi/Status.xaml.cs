﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;
using PulseTechConfig.Common;

namespace PulseTechConfig.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for Status.xaml
    /// </summary>
    public partial class Status : UserControl
    {
        private DataTable _dt = new DataTable();

        public Status()
        {
            InitializeComponent();
        }

        private void CmdRefresh_OnClick(object sender, RoutedEventArgs e)
        {
            ProgressRing.IsActive = true;
            var task = Task.Factory.StartNew(UpdatePacketStatus, TaskCreationOptions.LongRunning).ContinueWith(EndUpdate);
        }

        private void UpdatePacketStatus()
        {
            ApiLayer api = new ApiLayer();
            Dictionary<string, string> parms = new Dictionary<string, string>()
                {
                    {
                        "@SerialNo",
                        UsbCommon.AssetStatus.DeviceSerial
                    }
                };
            try
            {
                _dt = api.ExecStoredProc("[prcGetQSPacket]", false, parms);

            }
            catch
            {
                //PulseTechGlobals.MsgBox("No Workstations Found", "Workstation Lookup") ;
            }


        }

        void  EndUpdate(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                if (_dt.Rows.Count > 0)
                {
                    TxtLastPacket.Text = DateTime.Parse(_dt.Rows[0]["CreatedDateUTC"].ToString()).ToLocalTime().ToString() + " Local Time";
                    DateTime dt2;
                    if (DateTime.TryParse(_dt.Rows[0]["CreatedDateUTC"].ToString(), out dt2) &&
                        dt2.Date > DateTime.Today.AddMinutes(-20))
                    {
                        LblPulseComm.Content = "Communicating To Pulse";
                        LblPulseComm.Foreground = new SolidColorBrush(Colors.DarkGreen);
                    }
                    else
                    {
                        LblPulseComm.Content = "Not Communicating To Pulse";
                        LblPulseComm.Foreground = new SolidColorBrush(Colors.DarkRed);
                    }
                }
                
            }));
        }
    }
}
