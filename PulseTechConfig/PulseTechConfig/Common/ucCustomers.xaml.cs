﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;

namespace PulseTechConfig.Common
{
    /// <summary>
    /// Interaction logic for ucCustomers.xaml
    /// </summary>
    public partial class ucCustomers : UserControl
    {
        public event EventHandler<CustomerSelectEventArgs> SiteChanged;

        //public string URL = "http://rhythm.myenovate.com/";
        public string URL = Properties.Settings.Default.apiUrl;
        ApiLayer api = new ApiLayer();

        public string SelectedCustomer
        {
            get { return CbxCustomerList.SelectedValue.ToString(); }
            set
            {
                CbxCustomerList.SelectedValue = value;
                PulseTechGlobals.SelectedCustomer = value;
            }
        }

        public ucCustomers()
        {
            InitializeComponent();
            LoadCustomers();
        }

         private void LoadCustomers()
        {
            try
            {
                if (PulseTechGlobals.CustomersDataTable.Rows.Count < 1) PulseTechGlobals.CustomersDataTable = api.ExecSpApi("prcCustomersSelect", new Dictionary<string, string>() { { "@IDCustomer", "0" } });
                CbxCustomerList.ItemsSource = PulseTechGlobals.CustomersDataTable.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void SetSelectedCustomer(string custId)
        {
            CbxCustomerList.SelectedValue = custId;
        }

        private void CbxSiteList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PulseTechGlobals.SelectedCustomer = CbxCustomerList.SelectedValue.ToString();
            CustomerSelectEventArgs customerInfo = new CustomerSelectEventArgs();
            customerInfo.CustomerId = Convert.ToInt32(CbxCustomerList.SelectedValue.ToString());
            OnSiteChange(customerInfo);
        }

        protected virtual void OnSiteChange(CustomerSelectEventArgs e)
        {
            EventHandler<CustomerSelectEventArgs> handler = SiteChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }

    public class CustomerSelectEventArgs : EventArgs
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public bool IsIdn { get; set; }
    }
 }

