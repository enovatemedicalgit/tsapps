﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PulseTechConfig.Common
{
    public class WirelessInfo
    {
        public int Id { get; set; }
        public string SSID { get; set; }
        public string APmac { get; set; }
        public int Freq { get; set; }
        public int LinkQuality { get; set; }
        public int Rssi { get; set; }
        public int Channel { get; set; }
        public int Security { get; set; }
        public int Cipher { get; set; }
        public string NetworkType { get; set; }
        public int Speed { get; set; }
        public DateTime FirstSeen { get; set; }
        public DateTime LastSeen { get; set; }
    }
}