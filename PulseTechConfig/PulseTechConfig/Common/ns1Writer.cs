﻿////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;

namespace PulseTech.SMnetScan
{
    enum Type { Ap, AdHoc, Other }

    class Ns1File
    {
        internal static int Count;
        internal int Current;

        readonly ApInfo[] _apInfos = new ApInfo[Count];
        public Ns1File(int count) { _apInfos = new ApInfo[count]; Count = count; }

        public ApInfo[] Items
        {
            get
            {
                if (null != _apInfos) { return _apInfos; }
                return new ApInfo[0];
            }
        }

        #region Public Methods

        /// <summary>
        /// Adds an Ap to the Ap list
        /// </summary>
        /// <param name="ssid">Ssid (eg. linksys)</param>
        /// <param name="bssid">Bssid (eg. 11:22:33:44:55)</param>
        /// <param name="signal">Signal strength (eg. -65)</param>
        /// <param name="firstSeen">Time first seen (eg. 4:26:38 PM)</param>
        /// <param name="lastSeen">Time last seen (eg. 4:34:13 PM)</param>
        /// <param name="channel">Channel (eg. 11)</param>
        /// <param name="speed">Speed (eg. 54)</param>
        /// <param name="encryption">Encryption (eg. false)</param>
        /// <param name="latitude">GPS Latitude (eg. 31.41021)</param>
        /// <param name="longitude">GPS Longitude (eg. -83.4302)</param>
        /// <param name="type"></param>
        public void Add(string ssid, string bssid, Int32 signal, string firstSeen, string lastSeen, UInt32 channel, UInt32 speed, bool encryption, double latitude, double longitude, Type type)
        {
            _apInfos[Current] = new ApInfo(ssid, bssid, signal, firstSeen, lastSeen, channel, speed, encryption, latitude, longitude, type);
            Current += 1;
        }

        public void WriteOut(string filename)
        {
            List<byte> bData = new List<byte>();
            
            //NetS
            bData.AddRange(new byte[] { 0x4E, 0x65, 0x74, 0x53 });
            //12
            bData.AddRange(new byte[] { 0x0c, 0x00, 0x00, 0x00 });
            //How many APs
            bData.AddRange(BitConverter.GetBytes(_apInfos.Length));
            //Loop through all APs and add them to the list.
            foreach (ApInfo item in _apInfos)
            {
                //length of ssid
                bData.Add((byte)item.Ssid.Length);
                //Ssid
                bData.AddRange(System.Text.Encoding.ASCII.GetBytes(item.Ssid));
                //Bssid (MAC address)
                string[] mac = item.Bssid.Split(':');
                for (int i = 0; i < mac.Length; i++)
                {
                    bData.Add(Convert.ToByte(mac[i], 16));
                }
                //Signal
                bData.AddRange(BitConverter.GetBytes(item.Signal));
                //Noise
                bData.AddRange(Reverse(BitConverter.GetBytes(0)));
                //Snr
                bData.AddRange(Reverse(BitConverter.GetBytes(0)));

                //802.11 capability flags.
                if (item.Encryption && item.Type == Type.Ap) { bData.AddRange(new byte[] { 0x11, 0x00, 0x00, 0x00 }); }
                else if (item.Encryption && item.Type == Type.AdHoc) { bData.AddRange(new byte[] { 0x12, 0x00, 0x00, 0x00 }); }
                else if (item.Encryption == false && item.Type == Type.Ap) { bData.AddRange(new byte[] { 0x01, 0x00, 0x00, 0x00 }); }
                else if (item.Encryption == false && item.Type == Type.AdHoc) { bData.AddRange(new byte[] { 0x02, 0x00, 0x00, 0x00 }); }

                //Beacon interval
                bData.AddRange(BitConverter.GetBytes((uint)100));

                //First seen time
                bData.AddRange(BitConverter.GetBytes(item.FirstSeen.ToFileTime()));

                //Last seen time
                bData.AddRange(BitConverter.GetBytes(item.FirstSeen.ToFileTime()));

                //Latitude
                bData.AddRange(BitConverter.GetBytes(item.Latitude));
                //Longitude
                bData.AddRange(BitConverter.GetBytes(item.Longitude));

                //APDATA entries (0)
                bData.AddRange(BitConverter.GetBytes(0));

                //Length of name. Not used
                bData.Add(0);

                //No Name bytes

                //Bit field Channel activity. Not Used.
                bData.AddRange(BitConverter.GetBytes((long)0));

                //Channel
                bData.AddRange(BitConverter.GetBytes((int)item.Channel));

                //IP address. Not used.
                bData.AddRange(Reverse(BitConverter.GetBytes(0)));

                //Min. signal,dBm
                bData.AddRange(BitConverter.GetBytes(-100));

                //Max noise.
                bData.AddRange(BitConverter.GetBytes(0));

                //Speed
                bData.AddRange(BitConverter.GetBytes((((int)item.Speed) * 1024) / 100));

                //IP subnet address. Not used.
                bData.AddRange(BitConverter.GetBytes((uint)0));
                //IP netmask. Not used.
                bData.AddRange(BitConverter.GetBytes((uint)0));
                //Misc flags. Not used.
                bData.AddRange(BitConverter.GetBytes((uint)0));
                //IElength. Not used/Not needed.
                bData.AddRange(BitConverter.GetBytes((uint)0));
            }
            System.IO.File.WriteAllBytes(filename, bData.ToArray());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Reverses the elements in an array of bytes.
        /// </summary>
        /// <param name="input">The array to reverse</param>
        /// <returns>A new array with the values of the input in reverse order.</returns>
        private byte[] Reverse(byte[] input)
        {
            byte[] output = new byte[input.Length];
            int outi = 0;
            for (int i = input.Length - 1; i >= 0; i--)
            {
                output[outi] = input[i];
                outi++;
            }
            return output;
        }

        #endregion
    }

    #region ApInfo class

    class ApInfo
    {
        //private enum Security { None, Wep, WPA, Other }
        public ApInfo(string ssid,string bssid,Int32 signal,string firstSeen,string lastSeen,UInt32 channel,UInt32 speed,bool encryption, double latitude, double longitude,Type type) 
        {
            Ssid = ssid;
            Bssid = bssid;
            Signal = signal;
            FirstSeen = DateTime.Parse(firstSeen);
            LastSeen = DateTime.Parse(lastSeen);
            Channel = channel;
            Speed = speed;
            Encryption = encryption;
            Latitude = latitude;
            Longitude = longitude;
            Type = type;
        }

        public string Ssid { get; set; }

        public string Bssid { get; set; }

        public Int32 Signal { get; set; }

        public DateTime FirstSeen { get; set; }

        public DateTime LastSeen { get; set; }

        public UInt32 Channel { get; set; }

        public UInt32 Speed { get; set; }

        public bool Encryption { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public Type Type { get; set; }
    }

    #endregion
}