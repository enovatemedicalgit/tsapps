﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using Connect;

namespace PulseTechConfig.Common
{
    public static class UsbCommon
    {
        public static string VIDmobius = "03EB";
        public static string PIDmobius = "204F";
        public static bool UsbConnected = false;
        private static bool _continue = false;
        public static System.Threading.Thread ReadThread;
        public static string UsbData = "";
        public static string SubstituteChar = "$";
        public static string PassKey = "";
        public static bool ResponseRecv = false;
        public static string UsbResp = "";
        public static bool UpdateInProgress = false;

        #region "Mobius Return vars"
        public static AssetUsbStatus AssetStatus =  new AssetUsbStatus();

        private static readonly ManualResetEvent ResetEvent = new ManualResetEvent(false);

        #endregion

        private static readonly at_usb_api.at_usb_api Atusb = new at_usb_api.at_usb_api();

        public static string AttemptConnect()
        {
            string status = "Not connected";
            try
            {
                bool OK = Atusb.ConnectDevice(VIDmobius, PIDmobius);

                if (OK)
                {
                    status = "USB Connected";

                    UsbConnected = true;

                    if (!_continue)
                    {
                        if (ReadThread != null) ReadThread.Abort();
                        ReadThread = new Thread(ReadUsb);
                        _continue = true;
                        ReadThread.Start();
                    }
                }
                else
                {
                    UsbConnected = false;
                }

                System.Threading.Thread.Sleep(50); /// time to see LED on 
            }
            catch
            {
                UsbConnected = false;
            }
            return status;
        }

        public static void SendCommonCommands()
        {
            GetApMac();
            GetSerialNo();
            GetMobiusMac();
        }

        public static void ReadUsb()
        {
            while (_continue)
            {
                Thread.Sleep(100);
                try
                {
                    UsbData = Atusb.ReadBytes();
                    if (UsbData.Length > 2)
                    {
                        //Debug.WriteLine("Recv usb data");
                        //PacketsReceived++;
                        //SetText(UsbData);
                        ProcessCommands(UsbData);
                        Debug.WriteLine("USb->" + UsbData);
                        UsbResp = UsbData;
                        ResetEvent.Set();
                    }
                }
                catch
                {
                }
            }
        }

        #region "Mobius Send Commands"

        public static void SendCommand(string command)
        {
            Debug.WriteLine("Sending->"+command);
            //File.AppendAllText(@"C:\main\downloadLogNew.txt", "Sending->" + command + Environment.NewLine);
            Atusb.WriteBytes(command);
            Thread.Sleep(100);
        }
        public static void SendCommandResponse(string command)
        {
            Debug.WriteLine("SendResp->" + command);
            //File.AppendAllText(@"C:\main\downloadLogNew.txt", "SendResp->" + command + Environment.NewLine);
            ResponseRecv = false;
            //Task.Factory.StartNew(() =>  AsyncWrite(command),TaskCreationOptions.LongRunning);
            Atusb.WriteBytes(command);
            //int responseCnt = 0;
            ResetEvent.WaitOne(new TimeSpan(0,0,0,10));
            //while (!ResponseRecv)
            //{
            //    Thread.Sleep(100);
            //    responseCnt++;
            //    if (responseCnt > 10) return;
            //}
            Thread.Sleep(100);
            //Debug.WriteLine("Got reponse");
            //? Wait for response
            
        }

        private static void AsyncWrite(string command)
        {
            Atusb.WriteBytes(command);
        }

        public  static void EnterCommandMode(bool isCharger = false)
        {
            string stype = "";
            if (isCharger) stype = "0";
            SendCommandResponse("6E" + stype+ "$$$");
        }
        public static void SetSSID(string ssid, bool isCharger = false)
        {
            string stype = "";
            if (isCharger) stype = "0";
            SendCommandResponse("6E" + stype + "set w s " + ssid.Replace(" ","$"));
            
        }
        public static void SetChannel(string channel, bool isCharger = false)
        {
            string stype = "";
            if (isCharger) stype = "0";
            SendCommandResponse("6Eset w c " + channel);
            
        }
        public static void SetAuth(int authType, string passPhrase, string wepKey, bool isCharger = false)
        {
            string stype = "";
            if (isCharger) stype = "0";
            switch (authType)
            {
                case (int)Wlan.Dot11AuthAlgorithm.IEEE80211_Open:
                    SendCommandResponse("6E" + stype + "set w a 0");
                    break;
                case (int)Wlan.Dot11AuthAlgorithm.IEEE80211_SharedKey:
                    SendCommandResponse("6E" + stype + "set w a 1");
                    SendCommandResponse("6E" + stype + "set w k " + wepKey);
                    break;
                case (int)Wlan.Dot11AuthAlgorithm.WPA:
                    SendCommandResponse("6E" + stype + "set w a 2");
                    SendCommandResponse("6E" + stype + "set w p " + passPhrase);
                    break;
                case (int)Wlan.Dot11AuthAlgorithm.RSNA:
                    SendCommandResponse("6E" + stype + "set w a 3");
                    SendCommandResponse("6E" + stype + "set w p " + passPhrase);
                    break;
                case (int)Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                    SendCommandResponse("6E" + stype + "set w a 4");
                    SendCommandResponse("6E" + stype + "set w p " + passPhrase);
                    break;
                default:
                    break;
            }
            

        }

        private static string parsePassPhrase(bool isHex, string passPhrase)
        {
            string stmp;
            bool FndSpace = false;
            bool FndSub = false;
            bool Conflict = false;
            string sSub = String.Format("{0:X}", Convert.ToInt32(SubstituteChar[0]));

            // check for presence of spaces and substitution character 

            if (!isHex)
            {
                if (passPhrase.Contains(" ") && passPhrase.Contains(SubstituteChar))
                {
                    Conflict = true;
                }
            }
            else
            {
                for (int i = 0; i < passPhrase.Length; i += 2)
                {
                    stmp = passPhrase.Substring(i, 2);
                    if (stmp == "20")
                        FndSpace = true;
                    if (stmp == sSub)
                        FndSub = true;
                }
                if (FndSpace && FndSub)
                    Conflict = true;
            }

            if (Conflict)
            {
                //MessageBox.Show("This passphrase contains both spaces and\n" +
                //                 "your current Substitution Character\n\n" +
                //                 "Please select a new Substitution Character\n\n" +
                //                 "The New Substitution Character selector will\n" +
                //                 "open when you close this window.", "PassPhrase Error");

                return "";
            }

            // replace spaces with dollar signs 
            if (!isHex)
            {
                char nch = SubstituteChar[0];
                passPhrase = passPhrase.Replace(' ', nch);

            }
            else
            {
                try
                {
                    PassKey = "";
                    for (int i = 0; i < passPhrase.Length; i += 2)
                    {
                        stmp = passPhrase.Substring(i, 2);
                        if (stmp == "20")
                            stmp = String.Format("{0:X}", Convert.ToInt32(SubstituteChar[0]));

                        PassKey = PassKey + stmp;
                    }
                    passPhrase = PassKey;
                }
                catch
                {
                }

            }
            return passPhrase;
        }

        public static void SetDhcp(bool useDhcp, 
                                    string ipAddress, 
                                    string defaultGateway, 
                                    string subnetMask,
                                    string dnsServer)
        {
            SetCommandMode();
            if (!useDhcp)
            {
                SendCommand("6Eset i d 0");
                return;
            }
            SendCommand("6Eset i d 1");
            
            SendCommand("6Eset i a " + ipAddress);
            SendCommand("6Eset i g " + defaultGateway);
            SendCommand("6Eset i n " + subnetMask);
            SendCommand("6Eset d a " + dnsServer);
        }


        public static void GetSerialNo()
        {
            SendCommand("0O");
        }

        public static void SetCommandMode()
        {
            SendCommand("<");
        }

        public static void GetApMac()
        {
            SendCommand("6I");

        }

        public static void GetFetStatus()
        {
            SendCommand("1l");
        }

        public static void GetMobiusMac()
        {
            SendCommand("6C");
        }

        public static void ToggleChargerOnOff(string onOff)
        {
            //0 off 1 On
            if (onOff == "0")
            {
                // ChargerState = CHARGER_IDLE_UNTIL_BATTERY_REMOVED; 
                SendCommand("1E");
            }
            else
            {
                // ChargerState = CHARGER_IDLE_UNTIL_NEW_BATTERY_INSTALLED; 
                SendCommand("1}");
            }
        }

        public static void SetSerialNo(string sn)
        {
            SendCommand("0N" + sn) ;            
        }

        public static void GetMobiusMAC()
        {
            SendCommand("6C");
        }

        public static void GetAPMac()
        {
            SendCommand("6I");
        }

        public static void GetMeasuredVoltage()
        {
            SendCommand("0m");
        }

        public static void GetOperationStatus()
        {
            SendCommand("1s");
        }

        public static void GetBatteryStatus()
        {
            SendCommand("1r");
        }

        public static void GetSafetyStatus()
        {
            SendCommand("1o");
        }

        public static void GetPFStatus()
        {
            SendCommand("1p");
        }

        public static void GetLinkQuality()
        {
            SendCommand("6D");
        }

        public static void GetPFAlert()
        {
            SendCommand("1q");
        }

        public static void GetSafetyAlert()
        {
            SendCommand("1u");
        }

        public static void GetChargingStatus()
        {
            SendCommand("1v");
        }

        public static void GetBatteryMode()
        {
            SendCommand("1t");
        }

        public static void UnlockPack()
        {
            SendCommand("1w");
        }

        public static void CloseFETs()
        {
            SendCommand("1x");
        }

        public static void ResetPermFailure()
        {
            SendCommand("1y");
        }

        public static void ResetPack()
        {
            SendCommand("1z");
        }

        public static void LockPack()
        {
            SendCommand("1{");
        }

        public static void GetXValue()
        {
            SendCommand("1N");
        }

        public static void GetYValue()
        {
            SendCommand("1O");
        }

        public static void GetZValue()
        {
            SendCommand("1P");
        }

        public static void GetXMax()
        {
            SendCommand("1R");
        }

        public static void GetYMax()
        {
            SendCommand("1S");
        }

        public static void GetZMax()
        {
            SendCommand("1T");
        }

        public static void GetMotion()
        {
            SendCommand("1Q");
        }

        public static void ResetMax()
        {
            SendCommand("1U");
        }

        public static void FlashLEDs()
        {
            SendCommand("1L");
        }

        public static void LEDsOff()
        {
            SendCommand("1V");
        }

        public static void IsFlashing()
        {
            SendCommand("1M");
        }

        public static void GetMaxCC()
        {
            SendCommand("1)");
        }

        public static void SetMaxCC()
        {
            // SendCommand("1(" + txtMaxCC.Text.Trim());
        }

        public static void GetDC1Firmware()
        {
            SendCommand("3A");
        }

        public static void GetDC2Firmware()
        {
            SendCommand("4A");
        }

        public static void GetDC1VoltA()
        {
            SendCommand("3D");
        }

        public static void GetDC1CurrA()
        {
            SendCommand("3E");
        }

        public static void GetDC1VoltB()
        {
            SendCommand("3K");
        }

        public static void GetDC1CurrB()
        {
            SendCommand("3L");
        }

        public static void GetDC1Errors()
        {
            SendCommand("3P");
        }

        public static void GetDC1Status()
        {
            SendCommand("3Q");
        }


        public static void GetDC2VoltA()
        {
            SendCommand("4D");
        }

        public static void GetDC2CurrA()
        {
            SendCommand("4E");
        }

        public static void GetDC2VoltB()
        {
            SendCommand("4K");
        }

        public static void GetDC2CurrB()
        {
            SendCommand("4L");
        }

        public static void GetDC2Errors()
        {
            SendCommand("4P");
        }

        public static void GetDC2Status()
        {
            SendCommand("4Q");
        }
#endregion

#region "Mobius Return values"

        public static void ProcessCommands(string sReply)
        {
            char Device = sReply[0];
            char Command = sReply[1];
            string Reply = sReply.Substring(2);
            int Amt = 0;
            int itmp = 0;
            float ftmp = 0f;
            string sout = "";
            int Temp = 0;
            int Seconds = 150;


            ResponseRecv = true;
            int.TryParse(Reply, out Amt);
            //try
            //{
            //    Amt = Convert.ToInt16(Reply);
            //}
            //catch
            //{
            //    Amt = 0;
            //}

            // //txtStatus.Text = sReply;
            char chr = Device;
            switch (Device)
            {
                case '0': // Main Control 
                    // Msg("Main Command = " + Command);
                    switch (Command)
                    {
                      
                        case 'A':
                            AssetStatus.CBFirmwareVersion = Reply;
                            Console.WriteLine("Firmware=" + AssetStatus.CBFirmwareVersion);
                            break;
                        case 'B': // Charger Status 
                            Console.WriteLine("BackupBatteryStatus=" + AssetStatus.CBFirmwareVersion);
                            AssetStatus.BackupBatteryStatus = Reply[0].ToString();
                            Console.WriteLine("BackupBatteryStatus=" + AssetStatus.BackupBatteryStatus);
                            switch (Reply[0])
                            {
                                case '0': // BUB_OK,                             // 0
                                    //txtBUBChargerStatus.Text = "OK";
                                    break;
                                case '1': // BUB_BATTERY_ABSENT,                 // 1
                                    //txtBUBChargerStatus.Text = "Absent";
                                    break;
                                case '2': // BUB_PRE_CHARGE_QUALIFICATION,       // 2
                                    //txtBUBChargerStatus.Text = "Pre-charge Qualification";
                                    break;
                                case '3': // BUB_FAST_CHARGE,                    // 3
                                    //txtBUBChargerStatus.Text = "Fast Charge";
                                    break;
                                case '4': // BUB_MAINTENANCE_CHARGING,           // 4
                                    //txtBUBChargerStatus.Text = "Maintenance Charging";
                                    break;
                                case '5': // BUB_CHARGE_PENDING,                 // 5
                                    //txtBUBChargerStatus.Text = "Charge Pending";
                                    break;
                                case '6': // BUB_CHARGE_FAULT,                   // 6
                                    //txtBUBChargerStatus.Text = "Charge Fault";
                                    break;
                                case '7': // BUB_DISCHARGING,                    // 7
                                    //txtBUBChargerStatus.Text = "Discharging";
                                    break;
                                case '8': // BUB_NO_BUB                          // 8
                                    //txtBUBChargerStatus.Text = "No Backup Battery";
                                    break;
                            }
                            break;
                        case 'D':
                            AssetStatus.BUBVoltage = Reply;
                            break;
                        case 'O':
                            AssetStatus.DeviceSerial = Reply.Replace("_","");
                            break;
                        case 'Q':
                            //txtReadSPI.Text = Reply;
                            break;
                        case 'R': //txtSPIStatus.Text = Reply;            break;
                        case 'T': //txtTxInterval.Text = Reply;           break;
                        case 'Y': // Spare Pins Status 
                            int SP = 0;
                            try
                            {
                                SP = Convert.ToInt32(Reply);
                            }
                            catch
                            {
                                SP = 0;
                            }

                            string B7 = "0";
                            string B6 = "0";
                            string B5 = "0";
                            string B4 = "0";
                            string B3 = "0";
                            string B2 = "0";
                            string B1 = "0";
                            string B0 = "0";

                            if ((SP & 0x80) == 0x80)
                                B7 = "1";
                            if ((SP & 0x40) == 0x40)
                                B6 = "1";
                            if ((SP & 0x20) == 0x20)
                                B5 = "1";
                            if ((SP & 0x10) == 0x10)
                                B4 = "1";
                            if ((SP & 0x08) == 0x08)
                                B3 = "1";
                            if ((SP & 0x03) == 0x04)
                                B2 = "1";
                            if ((SP & 0x02) == 0x02)
                                B1 = "1";
                            if ((SP & 0x01) == 0x01)
                                B0 = "1";

                            //txtSparePins.Text = B7 + B6 + B5 + B4 + " " + B3 + B2 + B1; // + B0; 

                            AssetStatus.SparePinStatus = Reply;
                            ////txtSparePins.Text = SP.ToString();
                            break;
                        case 'm':
                            AssetStatus.measuredVoltage = Reply;
                            break;
                        case '<': // Get board serial number 
                            //txtCBSerial.Text = Reply;
                            AssetStatus.MedboardRev = Reply;
                            break;
                        case 'H': // countdown timer 
                            // Msg("CountDownTimer=" + sReply);

                            try
                            {
                                switch (sReply.Length)
                                {
                                    case 3: // 0H9 
                                        sout = sReply.Substring(2, 1);
                                        break;
                                    case 4: // 0H99  or 0H9K 
                                        sout = sReply.Substring(2, 2); // 99 or 9K 
                                        byte sb = (byte) sout[1];
                                        if (sb < 0x30 || sb > 0x39) // not a digit 
                                            sout = sReply.Substring(2, 1); // 9 
                                        break;
                                    case 5: // 0H120 
                                        sout = sReply.Substring(2, 3);
                                        break;
                                    default:
                                        sout = "150";
                                        break;
                                }
                            }
                            catch
                            {
                                sout = "150";
                            }

                            try
                            {
                                Temp = Convert.ToInt32(sout);
                            }
                            catch
                            {
                                Temp = 150;
                            }

                            // txtSeconds.Text = Temp.ToString();
                            Seconds = Temp;


                            break;
                    }
                    break;

                case '1': // Holster 
                    // Msg("Holster Command = " + Command);

                    switch (Command)
                    {
                        case 'A':
                            AssetStatus.HCFirmware = Reply;
                            break;
                        case 'D':
                            AssetStatus.BatteryErrors = Reply;
                            break;
                        case 'a':
                            AssetStatus.BattSerial = Reply;
                            break;
                        case 'b':
                            AssetStatus.BattVoltage = Reply;
                            break;
                        case 'c':
                            AssetStatus.BattCurrent = Reply;
                            CalculateWatts();
                            break;
                        case 'd':
                            AssetStatus.BattTemp = Reply;
                            break;
                        case 'e':
                            try
                            {
                                AssetStatus.BattChargeLevel = Convert.ToInt32(Reply).ToString();
                            }
                            catch
                            {
                                AssetStatus.BattChargeLevel = "0";
                            }
                            // Msg("ChargeLevel=" + BattChargeLevel);
                            //AssetStatus._goodChargeLevel = true;
                            break;

                        case 'f':
                            int hr = 0;
                            int min = 0;
                            int mtot = Convert.ToInt32(Reply);
                            AssetStatus.RemainingBattTimeMin = mtot.ToString();
                            if (mtot > 1440)
                                mtot = 1440;
                            hr = mtot/60;
                            min = mtot - (hr*60);
                            if (min < 10)
                                AssetStatus.RemainingBattTime = hr.ToString() + ":0" + min.ToString();
                            else
                                AssetStatus.RemainingBattTime = hr.ToString() + ":" + min.ToString();
                            // Msg("Remaining Time=" + RemainingBattTime);
                            break;
                        case 'g':
                            AssetStatus.BattCycle = Reply;
                            break;
                        case 'h':
                            AssetStatus.BattCapacityFCC = Reply;
                            break;
                        case 'i':
                            AssetStatus.BattCell1 = Reply;
                            break;
                        case 'j':
                            AssetStatus.BattCell2 = Reply;
                            break;
                        case 'k':
                            AssetStatus.BattCell3 = Reply;
                            break;
                        case 'l':
                            AssetStatus.BattFETStatus = Reply[0].ToString();
                            switch (Reply[0])
                            {
                                case '0':
                                    //FETStatus = "Both FET's Open";
                                    break;
                                case '2':
                                    //txtFETStatus = "Charge FET Open";
                                    break;
                                case '4':
                                    //txtFETStatus = "Discharge FET Open";
                                    break;
                                case '6':
                                    //txtFETStatus = "Both FET's Closed";
                                    break;
                            }
                            break;
                        case 'm':
                            AssetStatus.BattName = Reply;
                            break;
                        case 'n':
                            AssetStatus.ThermTemp = Reply;
                            break;
                        case 'o':
                            AssetStatus.SafetyStatus = String.Format("{0:X4}", Amt);
                            break;
                        case 'p':
                            AssetStatus.PFStatus = String.Format("{0:X4}", Amt);
                            break;
                        case 'q':
                            AssetStatus.PFAlert = String.Format("{0:X4}", Amt);
                            break;
                        case 'r':
                            AssetStatus.BatteryStatus = String.Format("{0:X4}", Amt);
                            AssetStatus.BatteryStatus = Amt.ToString();
                            break;
                        case 's':
                            //OperationStatus = String.Format("{0:X4}", Amt);
                            AssetStatus.OperationStatus = Reply;
                            break;
                        case 't':
                            //BatteryMode = String.Format("{0:X4}", Amt);
                            AssetStatus.BatteryMode = Reply;
                            break;
                        case 'u':
                            AssetStatus.SafetyAlert = String.Format("{0:X4}", Amt);
                            break;
                        case 'v':
                            AssetStatus.ChargingStatus = String.Format("{0:X4}", Amt);
                            break;
                        //case 'w':         = Reply;                
                        //    break;
                        //case 'x':         = Reply;               
                        //    break;
                        //case 'y':         = Reply;              
                        //    break;
                        //case 'z':         = Reply;              
                        //    break;
                        //case '{':         = Reply;             
                            break;
                        case 'B':
                            AssetStatus.ChargerStatus = Reply;
                            break;
                        case 'C':
                            AssetStatus.ACConnected = (Reply[0] == '1' ? true : false);
                            break;
                        case 'E':
                            AssetStatus.ChargerOFF = (Reply[0] == '1' ? true : false);
                            break;
                        case 'F':
                            AssetStatus.ChargerVoltage = Reply;
                            break;
                        case 'G':
                            AssetStatus.ChargerCurrent = Reply;
                            break;
                        case 'H':
                            AssetStatus.MainBatteryOutFlag = Reply;
                            break;
                        case 'I':
                            AssetStatus.AdHocString = Reply;
                            break;
                        case 'J':
                            AssetStatus.AdHocInteger = Reply;
                            break;
                        case 'K':
                            AssetStatus.SendAdHocData = Reply;
                            break;
                        case 'N':
                            AssetStatus.XValue = Reply;
                            break;
                        case 'O':
                            AssetStatus.YValue = Reply;
                            break;
                        case 'P':
                            AssetStatus.ZValue = Reply;
                            break;
                        case 'R':
                            AssetStatus.XMax = Reply;
                            break;
                        case 'S':
                            AssetStatus.YMax = Reply;
                            break;
                        case 'T':
                            AssetStatus.ZMax = Reply;
                            break;
                        case 'Q':
                            AssetStatus.Motion = Reply;
                            break;
                        //case 'L':        IsFlashing = Reply;         
                        //    break;
                        //case 'V':        IsFlashing = Reply;          
                        //    break;
                        //case 'M':        IsFlashing = Reply;         
                            break;
                        case ')':
                            AssetStatus.BattMaxCC = Reply;
                            break;
                        case '<':
                            AssetStatus.BoardSerial = Reply;
                            //txtHCSerial = Reply;
                            break;
                    }
                    break;

                case '2': // LCD 
                    // Msg("LCD = " + Command);
                    switch (Command)
                    {
                        case 'I': //txtLine1 = Reply;                break;
                        case 'J': //txtLine2 = Reply;                break;
                        case 'K':
                            AssetStatus.LCDFirmwareVersion = Reply; //txtLCDFirmware = Reply;         
                            break;
                        case 'N':
                            AssetStatus.BuzzerState = Reply;
                            //   if ( Reply[ 0 ] == '1' )
                            //      //txtBuzzerState = "Muted";
                            //   else
                            //      //txtBuzzerState = "On";
                            break;
                        case 'S':
                            AssetStatus.LCDBackLightStatus = Reply;
                            //   if ( Reply[ 0 ] == '1' )
                            //      //txtBLightState = "On";
                            //   else
                            //      //txtBLightState = "Off";
                            break;
                        case '<': // Get board serial number 
                            AssetStatus.LCDBoardSerialNumber = Reply;
                            break;
                    }
                    break;

                case '3': // DC-DC 1 
                    Console.WriteLine("DC-DC1 = " + Command);
                    switch (Command)
                    {
                        case 'A':
                            AssetStatus.DC1FirmwareVersion = Reply;
                            AssetStatus.DC2FirmwareVersion = Reply;
                            break;
                        case '<': // Get board serial number 
                            AssetStatus.DC2FirmwareVersion = Reply;
                            break;
                        case 'D':
                            AssetStatus.DC1AVoltage = Reply;
                            //txtDC1VoltA.Text = Reply;
                            try
                            {
                                ftmp = Convert.ToSingle(Reply);
                            }
                            catch
                            {
                                ftmp = 0f;
                            }
                            if (ftmp > 30000)
                            {
                            }
                            //VoltMeter1Ch2.Value = ftmp / 10000f;
                            else
                            {
                            }
                            // VoltMeter1Ch2.Value = ftmp / 1000f;
                            break;
                        case 'E':
                            AssetStatus.DC1AWatts = Reply;
                            CalculateWatts();
                            break;
                        case 'K':
                            AssetStatus.DC1BWatts = Reply;
                            AssetStatus.DC1BVoltage = Reply;
                            try
                            {
                                ftmp = Convert.ToSingle(Reply);
                            }
                            catch
                            {
                                ftmp = 0f;
                            }
                            // VoltMeter1Ch1.Value = ftmp / 1000f;
                            break;
                        case 'L':
                            AssetStatus.DC1CurrB = Reply;
                            CalculateWatts();
                            break;
                        case 'P':
                            AssetStatus.DC1Errors = Reply;
                            try
                            {
                                itmp = Convert.ToInt32(Reply);
                            }
                            catch
                            {
                                itmp = 0;
                            }
                            break;
                        case 'Q':

                            try
                            {
                                itmp = Convert.ToInt32(Reply);
                            }
                            catch
                            {
                                itmp = 0;
                            }
                            if ((itmp & 0x01) == 0x01) // Ch 2 A bus power enabled  
                            {
                                //pbxBusOnCh2.Image = ilsLED.Images[ 3 ];          // Green 
                            }
                            else
                            {
                                //pbxBusOnCh2.Image = ilsLED.Images[ 0 ];          // White 
                            }
                            if ((itmp & 0x02) == 0x02) // Ch 1 B bus power enabled  
                            {
                                //pbxBusOnCh1.Image = ilsLED.Images[ 3 ];
                            }
                            else
                            {
                                //pbxBusOnCh1.Image = ilsLED.Images[ 0 ];
                            }

                            if ((itmp & 0x04) == 0x04) // Ch 2 A output enabled 
                            {
                                //pbxOutputCh2.Image = ilsLED.Images[ 3 ];
                            }
                            else
                            {
                                //pbxOutputCh2.Image = ilsLED.Images[ 0 ];
                            }
                            if ((itmp & 0x08) == 0x08) // Ch 1 B output enabled 
                            {
                                //pbxOutputCh1.Image = ilsLED.Images[ 3 ];
                            }
                            else
                            {
                                //pbxOutputCh1.Image = ilsLED.Images[ 0 ];
                            }

                            if ((itmp & 0x10) == 0x10) // Ch 2 A fan on 
                            {
                                //pbxFanOnCh2.Image = ilsLED.Images[ 3 ];
                            }
                            else
                            {
                                //pbxFanOnCh2.Image = ilsLED.Images[ 0 ];
                            }
                            if ((itmp & 0x20) == 0x20) // Ch 1 B fan on 
                            {
                                //pbxFanOnCh1.Image = ilsLED.Images[ 3 ];
                            }
                            else
                            {
                                //pbxFanOnCh1.Image = ilsLED.Images[ 0 ];
                            }
                            AssetStatus.DC1Status = Reply;
                            break;
                        case 'M':
                            //txtVoltageSetting1.Text = Reply;
                            break;
                        case 'F':
                            //txtVoltageSetting2.Text = Reply;
                            break;
                        case 'S':
                            //txtAreVoltages1Locked.Text = Reply;
                            break;
                    }
                    break;

                case '4': // DC-DC 2 
                    // Msg("DC-DC2 = " + Command);
                    switch (Command)
                    {
                        case 'A':
                            AssetStatus.DC2Firmware = Reply;
                            break;
                        case 'D':
                            AssetStatus.DC2VoltA = Reply;
                            try
                            {
                                ftmp = Convert.ToSingle(Reply);
                            }
                            catch
                            {
                                ftmp = 0f;
                            }
                            if (ftmp > 30000)
                            {
                            }
                            // VoltMeter1Ch2.Value = ftmp / 10000f;
                            else
                            {
                            }
                            // VoltMeter1Ch2.Value = ftmp / 1000f;
                            break;
                        case 'E':
                            AssetStatus.DC2CurrA = Reply;
                            CalculateWatts();
                            break;
                        case 'K':
                            AssetStatus.DC2VoltB = Reply;
                            try
                            {
                                ftmp = Convert.ToSingle(Reply);
                            }
                            catch
                            {
                                ftmp = 0f;
                            }
                            // VoltMeter2Ch1 = ftmp / 1000f;
                            break;
                        case 'L':
                            AssetStatus.DC2CurrB = Reply;
                            CalculateWatts();
                            break;
                        case 'P':
                            AssetStatus.DC2Errors = Reply;
                            break;
                        case 'Q':
                            AssetStatus.DC2Status = Reply;
                            break;
                        case '<': // Get board serial number 
                            AssetStatus.DC2Serial = Reply;
                            break;
                    }
                    break;
                case '5':                  // MedBoard 
                    switch (Command)
                    {
                        case 'A':
                            AssetStatus.MedboardRev = sReply.Substring(2);
                            if (AssetStatus.MedboardRev == "0.00") AssetStatus.MedboardRev = "";
                            break;
                    }
                    break;
                case '6': // WiFi 
                    // Msg("WIFI = " + Command);
                    switch (Command)
                    {
                        case 'A':
                            AssetStatus.WirelesssFirmwareVersion = Reply;
                            break;
                        case 'B':
                            AssetStatus.MobiusIPAddr = Reply;
                            break;
                        case 'C':
                            AssetStatus.APMacAddr = Reply;
                            break;
                        case 'D':
                            AssetStatus.LinqQuality = Reply;
                            break;
                        case 'F':
                        case 'G':
                        case 'H':
                        case 'E': //txtWiFiResponse.Text = Reply;         
                            break;
                        case 'I':
                            AssetStatus.MobiusMacAddr = Reply.Replace("_","");
                            break;
                        case 'X':
                            if (Reply.Contains("B")) AssetStatus.DeviceType = "B";
                            if (Reply.Contains("W")) AssetStatus.DeviceType = "W";
                            break;
                    }
                    break;
                case '7':
                    {
                        chr = Command;
                        if (chr == 'A')
                        {
                            AssetStatus.BayWirelessRev = Reply.Substring(3);
                        }
                        char SubCommand = Reply[2];
                        chr = SubCommand;
                        switch (chr)
                        {
                            case 'a':
                                {
                                    goto case 'b';
                                }
                            case 'b':
                                {
                                    break;
                                }
                            case 'c':
                                {
                                    goto case 'b';
                                }
                            case 'd':
                                {
                                    goto case 'b';
                                }
                            case 'e':
                                {
                                    AssetStatus.bootdevice = Reply.Substring(0, 1);
                                    goto case 'b';
                                }
                            case 'f':
                                {
                                    goto case 'b';
                                }
                            case 'g':
                                {
                                    AssetStatus.pagecount = Reply.Substring(0, 2);
                                    goto case 'b';
                                }
                            default:
                                {
                                    if (chr == 'r')
                                    {
                                        goto case 'b';
                                    }
                                    else
                                    {
                                        goto case 'b';
                                    }
                                }
                        }
                        break;
                    }
                case '8':
                    {
                        char Port = sReply[2];
                        chr = Command;
                        if (chr == 'A')
                        {
                            chr = Port;
                            switch (chr)
                            {
                                case '1':
                                    {
                                        AssetStatus.BCrev1 = Reply.Substring(3);
                                        break;
                                    }
                                case '2':
                                    {
                                        AssetStatus.BCrev2 = Reply.Substring(3);
                                        break;
                                    }
                                case '3':
                                    {
                                        AssetStatus.BCrev3 = Reply.Substring(3);
                                        if (AssetStatus.BCrev3 != "0.00")
                                        {
                                            AssetStatus.HasBC3Board = true;
                                        }
                                        if (AssetStatus.BCrev3 == "0.00")
                                        {
                                            AssetStatus.BCrev3 = "";
                                        }
                                        break;
                                    }
                                case '4':
                                    {
                                        AssetStatus.BCrev4 = Reply.Substring(3);
                                        if (AssetStatus.BCrev4 != "0.00")
                                        {
                                            AssetStatus.HasBC4Board = true;
                                        }
                                        if (AssetStatus.BCrev4 == "0.00")
                                        {
                                            AssetStatus.BCrev4 = "";
                                        }
                                        break;
                                    }
                            }
                        }
                        break;
                    }
            }
        }

        public static void CalculateWatts()
        {
            decimal E, I, P, Efficiency;

            // Battery 
            try
            {
                E = Convert.ToDecimal(AssetStatus.BattVoltage);
                E = E/1000.0m;
            }
            catch
            {
                E = 0.0m;
            }

            try
            {
                //I = Convert.ToDecimal(txtBatCurrent.Text);
                //if (I < 0.0m)
                //    I = I * (-decimal.One);
                //I = I / 1000.0m;
            }
            catch
            {
                I = 0.0m;
            }
            try
            {
                //   P = (I * E);
            }
            catch
            {
                P = 0.0m;
            }

            //  BattWatts = P;
            // txtBattWatts.Text = String.Format("{0:N}", P);

            // DC 1 A 
            try
            {
                //      E = Convert.ToDecimal(txtDC1VoltA.Text);
                //      E = E / 1000.0m;
            }
            catch
            {
                E = 0.0m;
            }
            try
            {
                //     I = Convert.ToDecimal(txtDC1CurrA.Text);
                //      if (I < 0.0m)
                //        I = I * (-decimal.One);
                //     I = I / 1000.0m;
            }
            catch
            {
                I = 0.0m;
            }
            try
            {
                //      P = (I * E);
            }
            catch
            {
                P = 0.0m;
            }

            //DC1AWatts = P;
            // txtDC1AWatts.Text = String.Format("{0:N}", P);

            // DC 1 B 

            try
            {
                //   E = Convert.ToDecimal(txtDC1VoltB.Text);
                //   E = E / 1000.0m;
            }
            catch
            {
                E = 0.0m;
            }
            try
            {
                //I = Convert.ToDecimal(txtDC1CurrB.Text);
                //if (I < 0.0m)
                //    I = I * (-decimal.One);
                //I = I / 1000.0m;
            }
            catch
            {
                I = 0.0m;
            }
            try
            {
                //    P = (I * E);
            }
            catch
            {
                P = 0.0m;
            }

            // DC1BWatts = P;
            //  txtDC1BWatts.Text = String.Format("{0:N}", P);

            // DC 2 A 
            try
            {
                //   E = Convert.ToDecimal(txtDC2VoltA.Text);
                //    E = E / 1000.0m;
            }
            catch
            {
                E = 0.0m;
            }
            try
            {
                //I = Convert.ToDecimal(txtDC2CurrA.Text);
                //if (I < 0.0m)
                //    I = I * (-decimal.One);
                //I = I / 1000.0m;
            }
            catch
            {
                I = 0.0m;
            }
            try
            {
                //    P = (I * E);
            }
            catch
            {
                P = 0.0m;
            }

            //DC2AWatts = P;
            //txtDC2AWatts.Text = String.Format("{0:N}", P);

            // DC 2 B 
            try
            {
                //  E = Convert.ToDecimal(txtDC2VoltB.Text);
                // E = E / 1000.0m;
            }
            catch
            {
                E = 0.0m;
            }
            try
            {
                //I = Convert.ToDecimal(txtDC2CurrB.Text);
                //if (I < 0.0m)
                //    I = I * (-decimal.One);
                //I = I / 1000.0m;
            }
            catch
            {
                I = 0.0m;
            }
            try
            {
                //     P = (I * E);
            }
            catch
            {
                P = 0.0m;
            }

            //DC2BWatts = P;
            //txtDC2BWatts.Text = String.Format("{0:N}", P);

            // Efficiency 

            try
            {
                //Efficiency = (DC1AWatts + DC1BWatts + DC2AWatts + DC2BWatts) / BattWatts;
                //Efficiency = Efficiency * 100.0m;
            }
            catch
            {
                Efficiency = 0.0m;
            }
            // txtEfficiency.Text = String.Format("{0:N}", Efficiency);
        }

        #endregion
    }
}