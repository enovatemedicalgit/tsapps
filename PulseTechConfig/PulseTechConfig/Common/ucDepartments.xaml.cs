﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;
using Telerik.Windows.Controls;

namespace PulseTechConfig.Common
{
    /// <summary>
    /// Interaction logic for ucDepartments.xaml
    /// </summary>
    public partial class ucDepartments : UserControl
    {
        public event EventHandler<DeptSelectEventArgs> DeptChanged;
        private DataTable _dt = new DataTable();
        //public string URL = "http://rhythm.myenovate.com/";
        public string URL = Properties.Settings.Default.apiUrl;
        ApiLayer api = new ApiLayer();
        private string _siteId;

        public ucDepartments()
        {
            InitializeComponent();
        }

        public string SelectedDepartment
        {
            get { return PulseTechGlobals.SelectedDepartment; }
            set
            {
                CbxDepartmentList.SelectedIndex = DepartmentIdtoIndex(value);
                PulseTechGlobals.SelectedDepartment = value;
            }
        }

        public int DepartmentIdtoIndex(string deptId)
        {
            for (int i = 0; i < PulseTechGlobals.DepartmentsDataTable.Rows.Count - 1; i++)
            {
                DataRow dr = PulseTechGlobals.DepartmentsDataTable.Rows[i];
                if (dr["IDDepartment"].ToString() == deptId) return i;
            }
            return 0;
        }

        public void LoadDepartments(string idsite)
        {
            _siteId = idsite;
            var parms = new Dictionary<string, string>();
            parms.Add("SiteID", idsite);
            //}

            //this sometimes throws an error so trap and proceed

            try
            {
                _dt = api.ExecStoredProc("[prcDepartmentsSelectBySite]", false, parms);
                PulseTechGlobals.DepartmentsDataTable = _dt;
            }
            catch
            {
            }

            try
            {
                CbxDepartmentList.ItemsSource = _dt.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void CbxDepartmentList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbxDepartmentList.SelectedValue == null)
            {
                PulseTechGlobals.SelectedDepartment = "";
                return;
            }
            DeptSelectEventArgs deptInfo = new DeptSelectEventArgs();
            deptInfo.DeptId = CbxDepartmentList.SelectedValue.ToString();
            deptInfo.SiteId = _siteId;
            OnDeptChange(deptInfo);
        }
        protected virtual void OnDeptChange(DeptSelectEventArgs e)
        {
            EventHandler<DeptSelectEventArgs> handler = DeptChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }
    }

    public class DeptSelectEventArgs : EventArgs
    {
        public string SiteId { get; set; }
        public string DeptId { get; set; }
        public string Description { get; set; }
    }
    }

