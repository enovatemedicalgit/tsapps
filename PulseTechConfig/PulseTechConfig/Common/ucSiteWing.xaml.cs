﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;

namespace PulseTechConfig.Common
{
    /// <summary>
    /// Interaction logic for ucSiteWing.xaml
    /// </summary>
    public partial class ucSiteWing : UserControl
    {
        public event EventHandler<WingSelectEventArgs> WingChanged;
        private DataTable _dt = new DataTable();
        public string URL = PulseTechConfig.Properties.Settings.Default.apiUrl;
        ApiLayer api = new ApiLayer();
        private string _siteId;

        public ucSiteWing()
        {
            InitializeComponent();
        }

        public string SelectedWing
        {
            get { return PulseTechGlobals.SelectedWing; }
            set
            {
                CbxWingList.SelectedIndex = WingIdtoIndex(value);
                PulseTechGlobals.SelectedWing = value;
            }
        }

        public int WingIdtoIndex(string wingId)
        {
            for (int i = 0; i < PulseTechGlobals.WingsDataTable.Rows.Count - 1; i++)
            {
                DataRow dr = PulseTechGlobals.WingsDataTable.Rows[i];
                if (dr["IDSiteWing"].ToString() == wingId) return i;
            }
            return 0;
        }

        public void LoadWings(string idsite)
        {
            _siteId = idsite;
            var parms = new Dictionary<string, string>();
            parms.Add("SiteID", idsite);
            //}

            //this sometimes throws an error so trap and proceed

            try
            {
                _dt = api.ExecStoredProc("[prcSiteWingsSelectBySite]", false, parms);
                PulseTechGlobals.WingsDataTable = _dt;
            }
            catch
            {
            }

            try
            {
                CbxWingList.ItemsSource = _dt.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void CbxWingList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbxWingList.SelectedValue == null)
            {
                PulseTechGlobals.SelectedWing = "";
                return;
            }
            WingSelectEventArgs wingInfo = new WingSelectEventArgs();
            wingInfo.WingId = CbxWingList.SelectedValue.ToString();
            wingInfo.SiteId = _siteId;
            OnwingChange(wingInfo);
        }
        protected virtual void OnwingChange(WingSelectEventArgs e)
        {
            EventHandler<WingSelectEventArgs> handler = WingChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void CbxWingList_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (CbxWingList.Text == "Select Wing") CbxWingList.Text = "";
        }

        private void CbxWingList_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (CbxWingList.Text == string.Empty) CbxWingList.Text = "Select Wing";
            if (CbxWingList.SelectedIndex == -1)
            {
                CbxWingList.Text = "";
                PulseTechGlobals.SelectedWing = "";
            }
        }
    }
    public class WingSelectEventArgs : EventArgs
    {
        public string SiteId { get; set; }
        public string WingId { get; set; }
        public string Description { get; set; }
    }
}
