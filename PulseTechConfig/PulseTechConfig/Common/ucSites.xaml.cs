﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;

namespace PulseTechConfig.Common
{
    /// <summary>
    /// Interaction logic for ucSites.xaml
    /// </summary>
    public partial class ucSites : UserControl
    {
        public event EventHandler<SiteSelectEventArgs> SiteChanged;

        //public string URL = "http://rhythm.myenovate.com/";
        public string URL = "http://nnt.myenovate.com/"; // this is the default - it is set below from the settings file
        ApiLayer api = new ApiLayer();

        public string SelectedSite
        {
            get { return PulseTechGlobals.SelectedSite; }
            set
            {
                try
                {
                    CbxSiteList.SelectedIndex = SiteIDtoIndex(value);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message );
                }
                
                PulseTechGlobals.SelectedCustomer = value;
            }
        }

        public int SiteIDtoIndex(string siteId)
        {
            for (int i = 0; i < PulseTechGlobals.SitesDataTable.Rows.Count - 1; i++)
            {
                DataRow dr = PulseTechGlobals.SitesDataTable.Rows[i];
                if (dr["IDSite"].ToString() == siteId) return i;
            }
            return 0;
        }
        public ucSites()
        {
            InitializeComponent();
            URL = PulseTechConfig.Properties.Settings.Default.apiUrl;
            LoadSites();
        }

        public void LoadSites()
        {
            try
            {
                CbxSiteList.ItemsSource = PulseTechGlobals.SitesDataTable.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        public void SetSelectedSite(string siteId)
        {
            CbxSiteList.SelectedValue = siteId;
        }

        private void CbxSiteList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbxSiteList.SelectedValue == null)
            {
                PulseTechGlobals.SelectedSite = "";
                return;
            }
            PulseTechGlobals.SelectedSite = CbxSiteList.SelectedValue.ToString();
            SiteSelectEventArgs siteInfo = new SiteSelectEventArgs();
            siteInfo.SiteId = Convert.ToInt32(CbxSiteList.SelectedValue.ToString());
            OnSiteChange(siteInfo);
        }

        protected virtual void OnSiteChange(SiteSelectEventArgs e)
        {
            EventHandler<SiteSelectEventArgs> handler = SiteChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void CbxSiteList_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (CbxSiteList.Text == "Select Site") CbxSiteList.Text = "";
        }

        private void CbxSiteList_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (CbxSiteList.Text == string.Empty) CbxSiteList.Text = "Select Site";
            if (CbxSiteList.SelectedIndex == -1)
            {
                CbxSiteList.Text = "";
                PulseTechGlobals.SelectedSite = "";
            }
            Debug.WriteLine("test");
        }
    }

    public class SiteSelectEventArgs : EventArgs
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public bool IsIdn { get; set; }
    }
}
