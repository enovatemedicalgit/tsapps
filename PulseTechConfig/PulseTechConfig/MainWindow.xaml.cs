﻿using FirstFloor.ModernUI.Windows.Controls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Presentation;
using FirstFloor.ModernUI.Windows.Controls;
using PulseTechConfig.Common;

namespace PulseTechConfig
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow 
    {
        public static bool IsLoggedIn = false;

        public MainWindow()
        {
            InitializeComponent();
            AppearanceManager.Current.AccentColor = Colors.DodgerBlue;
            ContentSource = MenuLinkGroups.First().Links.First().Source;

        }

        private void MainWindow_OnClosing(object sender, CancelEventArgs e)
        {
            if (UsbCommon.ReadThread != null) UsbCommon.ReadThread.Abort();
        }
    }
}
