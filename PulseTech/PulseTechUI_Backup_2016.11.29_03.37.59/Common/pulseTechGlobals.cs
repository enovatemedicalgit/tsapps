﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PulseTech;

namespace PulseTechUI.Common
{
    public static class PulseTechGlobals
    {
        private static string mInternalSecret = "C0mp@ssion";
        public static string Fname = "";

        public static string UserId = "";
        public static int UserType = -1;
        public static int UserSiteId = -1;
        public static string UserName = "";
        public static string SiteName = "";
        public static string Password = "";
        public static bool UserLoggedIn = false;
        public static DataTable SitesDataTable;
        public static string SelectedSite = "";

        public static string EncryptString(string sIn)
        {
            return Crypto.EncryptStringAES(sIn, mInternalSecret);
        }
    }
}
