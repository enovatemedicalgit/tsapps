﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Principal;

namespace PulseTech
{
    public class ApiLayer
    {

        public  string URL  { get; set; } 
        

        public  DataTable ExecSpApi(string storedProc, Dictionary<string,string> spParms )
        {
            if (URL == null)
            {
                URL = "https://rhythm.myenovate.com/";
            }
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(URL);

            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            // not using get method - very limited
            // post is requiring multiple parameters to be sent as 1, so add in stored proc name
            spParms.Add("storedProcName", storedProc);

            HttpResponseMessage response = client.PostAsJsonAsync("api/StoredProc/", spParms).Result;
            if (response.IsSuccessStatusCode)
            {
                var dt = response.Content.ReadAsAsync<DataTable>().Result;

                return dt;
            }
            else
            {
                return new DataTable();
            }
        }


        public bool AuthUser(string userName)
        {
            if (string.IsNullOrEmpty(userName))
            {
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                if (identity == null) return false;
                userName = identity.Name;
            }
            HttpClient client = new HttpClient {BaseAddress = new Uri(URL)};

            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/UserAuth/?userId="+userName).Result;
            if (response.IsSuccessStatusCode)
            {
                bool auth = response.Content.ReadAsAsync<bool>().Result;
                return auth;
            }
            else
            {
                //MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                return false;
            }
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }

        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof (T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo colProp in temp.GetProperties())
                {
                    if (colProp.Name == column.ColumnName) colProp.SetValue(obj, dr[column.ColumnName], null);
                }
            }
            return obj;
        }

        public DataTable ExecStoredProc(string procName, bool isEng, Dictionary<string, string> paramsDictionary)
        {          
            return ExecSpApi(procName, paramsDictionary);
        }
    }
}

