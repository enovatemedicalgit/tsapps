﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucApTreeView.xaml
    /// </summary>
    public partial class ucApTreeView : UserControl
    {
        List<WirelessInfo> _apList = new List<WirelessInfo>();

        public ucApTreeView()
        {
            InitializeComponent();
            GetApList();
            LoadTreeView();
        }

        private void LoadTreeView()
        {
            string lastSsid = "";
            MenuItem childItem = new MenuItem();
            MenuItem childItemDetail = new MenuItem();

            MenuItem root = new MenuItem() { TvItemName = "Available APs" };

            root.IsExpanded = true;
            foreach (WirelessInfo wi in _apList)
            {
                if (wi.SSID != lastSsid)
                {
                    childItem = new MenuItem() { TvItemName = wi.SSID };
                    //if (lastSsid != "") root.Items.Add(childItem);
                    root.Items.Add(childItem);
                    //root.Items[root.Items.Count-1].IsExpanded = true;
                }
                else
                {
                    childItem.Items.Add(new MenuItem() { TvItemName = wi.APmac + "\t(" + wi.Rssi + ")" + "\tQuality=" + wi.LinkQuality });
                }
                lastSsid = wi.SSID;
            }
            //root.Items.Add(childItem);
                       
            APtreeView.Items.Add(root);
        }
        private void GetApList()
        {
            string apMac = "";
            try
            {
                var wlanClient = new WlanClient();

                //Collection<String> connectedSsids = new Collection<string>();
                foreach (WlanClient.WlanInterface wlanInterface in wlanClient.Interfaces)
                {
                    //Wlan.Dot11Ssid ssid = wlanInterface.CurrentConnection.wlanAssociationAttributes.dot11Ssid;
                    //connectedSsids.Add(new String(Encoding.ASCII.GetChars(ssid.SSID, 0, (int)ssid.SSIDLength)));
                    Wlan.WlanBssEntry[] wlanBssEntries = wlanInterface.GetNetworkBssList();
                    foreach (Wlan.WlanBssEntry wlanBssEntry in wlanBssEntries)
                    {
                        byte[] macAddr = wlanBssEntry.dot11Bssid;
                        var macAddrLen = (uint)macAddr.Length;
                        var str = new string[(int)macAddrLen];
                        for (int i = 0; i < macAddrLen; i++)
                        {
                            str[i] = macAddr[i].ToString("x2");
                        }
                        string mac = string.Join("", str);

                        apMac = mac.Substring(0, 2) + ":" + mac.Substring(2, 2) + ":" +
                        mac.Substring(4, 2) + ":" + mac.Substring(6, 2) + ":" +
                        mac.Substring(8, 2) + ":" + mac.Substring(10, 2);
                        string ssid = System.Text.Encoding.Default.GetString(wlanBssEntry.dot11Ssid.SSID).Replace("\0", "");
                        Debug.WriteLine(apMac + " " + ssid + wlanBssEntry.chCenterFrequency.ToString() + "ghz - LinkQuality=" + wlanBssEntry.linkQuality.ToString() + " Rssi=" + wlanBssEntry.rssi.ToString() );
                        if (ssid != "")
                        {
                            WirelessInfo w = new WirelessInfo();
                            w.SSID = ssid;
                            w.APmac = apMac;
                            w.Freq = wlanBssEntry.chCenterFrequency;
                            w.LinkQuality = wlanBssEntry.linkQuality;
                            w.Rssi = wlanBssEntry.rssi;
                            _apList.Add(w);
                        }
                    }
                }
                _apList = _apList.OrderBy(o => o.SSID).ToList();

            }
            catch (Exception ex)
            {
                //APMacAddr = "00:00:00:00:00";
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        apMac = nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
            }
            //return apMac;
        }
    }

    public class MenuItem: TreeViewItemBase
    {
        public MenuItem()
        {
            this.Items = new ObservableCollection<MenuItem>();
        }

        public string TvItemName { get; set; }

        public ObservableCollection<MenuItem> Items { get; set; }
    }

    public class TreeViewItemBase : INotifyPropertyChanged
    {
        private bool isSelected;
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value != this.isSelected)
                {
                    this.isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return this._isExpanded; }
            set
            {
                if (value != this._isExpanded)
                {
                    this._isExpanded = value;
                    NotifyPropertyChanged("IsExpanded");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

}
