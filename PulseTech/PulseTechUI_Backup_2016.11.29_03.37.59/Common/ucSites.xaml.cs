﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucSites.xaml
    /// </summary>
    public partial class ucSites : UserControl
    {
        public event EventHandler<SiteSelectEventArgs> SiteChanged;

        public string URL = "http://rhythm.myenovate.com/";
        ApiLayer api = new ApiLayer();

        public ucSites()
        {
            InitializeComponent();
            LoadSites();
        }

        private void LoadSites()
        {
            try
            {
                CbxSiteList.ItemsSource = PulseTechGlobals.SitesDataTable.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void CbxSiteList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PulseTechGlobals.SelectedSite = CbxSiteList.SelectedValue.ToString();
        }
    }

    public class SiteSelectEventArgs : EventArgs
    {
        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public bool IsIdn { get; set; }
    }
}
