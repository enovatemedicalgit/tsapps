﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PulseTechUI.Common
{
    class WirelessInfo
    {
        public string SSID;
        public string APmac;
        public double Freq;
        public uint LinkQuality;
        public int Rssi;
           
    }

    }
