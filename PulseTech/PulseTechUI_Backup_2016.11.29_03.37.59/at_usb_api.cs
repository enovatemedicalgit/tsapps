/**********************************************************************************
 
-----------------------------------------------------------------------------------
 Description: Atmel USB interface.
  
              Your project must allow unsafe code.

              Buffer sizes are for 64 bytes by default.

-----------------------------------------------------------------------------------
*/

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using DWORD = System.UInt32;


namespace at_usb_api
{
   unsafe public class at_usb_api
   {
      #region Definitions of VID, PID
      
      static Int32 DEFAULT_VID = 0x03EB;
      static Int32 DEFAULT_PID = 0x204F;

      #endregion 


      int _PacketSize = 64;
      bool _foundDLL = false;
      
      /// <summary>
      /// Packet size used by the device endpoints. Maximum is 255 
      /// </summary>
      public int PacketSize
      {
         get
         {
            return _PacketSize;
         }
         set
         {
            if ( value <= 255 ) 
               _PacketSize = value;
            else
               _PacketSize = 255;
         }
      }



      public bool FoundDLL
      {
         get
         {
            return _foundDLL;
         }
      }




      #region Imported DLL functions from atusbhid.dll

      /// <summary>
      /// This function allows to find the Generic HID device using the vendor ID (VID)/the
      /// product ID (PID) and open a handle if the device is connected.
      /// </summary>
      /// <param name="VendorID">const UINT VendorID, the Vendor ID</param>
      /// <param name="ProductID">const UINT ProductID, the Product ID</param>
      /// <returns>boolean, FALSE: if the device is not found.</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern bool findHidDevice( Int32 VendorID, Int32 ProductID );

      /// <summary>
      /// Closes the communication with the USB device and all related handles.
      /// </summary>
      [DllImport( "atusbhid.dll" )]
      private static extern void closeDevice();

      /// <summary>
      /// This function sends data to the device (OUT data). The maximum data length supported
      /// by this function must be lower or equal to the value given by the function
      /// getOutputReportLength.
      /// If the data length exceeds the maximum length specified in the firmware, the user has to send
      /// data in several packets.
      /// When data length is lower than the maximum length, this function will complete the remaining
      /// bytes with zero (0x00).
      /// </summary>
      /// <param name="buf">UCHAR * buffer: pointer of the packet to be sent.</param>
      /// <returns>FALSE: if data transmission fails.
      /// TRUE: if the packet was successfully transferred.</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern bool writeData( void * buf );

      /// <summary>
      /// This function reads the data packets sent by the device (IN data). To avoid data loss
      /// this function should be called in continuous mode (using a thread or a timer).
      /// </summary>
      /// <param name="buffer">UCHAR * buffer: Pointer to the buffer which will contain the received packet.
      /// The buffer must have the length of the IN report given by the getInputReportLength function.</param>
      /// <returns>FALSE: if no data is available.
      /// TRUE: if data are received and stored in the buffer.</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern bool readData( void * buffer );

      /// <summary>
      /// Please note that this function can be used only with a VC++ project.
      /// This function notifies the application if a new plug & play device has been plugged or unplugged.
      /// </summary>
      /// <param name="hWnd">HWND hWnd - Handle to a window.</param>
      /// <returns>FALSE: if the function fails.
      /// TRUE: if the function succeeds.</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern int  hidRegisterDeviceNotification( void * hWnd );

      /// <summary>
      /// Please note that this function can be used only with a VC++ project.
      /// This function closes the specified device notification handle.
      /// </summary>
      /// <param name="hWnd">HWND hWnd - Handle to a window.</param>
      [DllImport( "atusbhid.dll" )]
      private static extern void hidUnregisterDeviceNotification( void * hWnd );

      /// <summary>
      /// Please note that this function can be used only with a VC++ project.
      /// This function allows to check if the new device (plugged or unplugged) notified by 'hidRegister-
      /// DeviceNotification' is the used HID device or not.
      /// </summary>
      /// <param name="dwData">DWORD dwData, the value given by OnDeviceChange 2nd parameter</param>
      /// <returns>TRUE: if the device connected/disconnected is the used HID device
      /// FALSE: if this is another device</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern bool isMyDeviceNotification( DWORD dwData );

      /// <summary>
      /// This function allows the user to send a command data to control the HID device
      /// (i.e.: Start the bootloader, start a new task...). Data will be transmitted over the endpoint 0 as a
      /// 'SetReport' request (Refer to the HID Specification for further information). The endpoints IN
      /// and OUT will be used for the applicative raw data transfer only. The data length is fixed by the
      /// firmware and can be obtained using the function getFeatureReportLength. The data length must not
      /// exceed the length returned by getFeatureReportLength function.
      /// </summary>
      /// <param name="buffer">UCHAR * buffer: Pointer to the buffer which contains the received packet.</param>
      /// <returns>FALSE: if data transmission fails.
      /// TRUE: if data are well transferred.</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern bool setFeature( void * buffer );

      /// <summary>
      /// This function allows the user to get the length of the Feature report (Control data packet sent
      /// from the PC to the device). This value is specified in the firmware.
      /// </summary>
      /// <returns>int</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern int  getFeatureReportLength();

      /// <summary>
      /// This function allows the user to get the length of the OUT report (data packet sent from the PC to
      /// the device). This value is specified in the firmware.
      /// </summary>
      /// <returns>int</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern int  getOutputReportLength();

      /// <summary>
      /// This function allows the user to get the length of the IN report (data packet sent from the device
      /// to the PC). This value is specified in the firmware.
      /// </summary>
      /// <returns>int</returns>
      [DllImport( "atusbhid.dll" )]
      private static extern int  getInputReportLength();

      #endregion




      #region at_usb_api constructor
      /****************************************************************************
           at_usb_api                                             08/30/09 
        Description: default constructor
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      /// <summary>
      /// default constructor
      /// </summary>
      
      public at_usb_api()
      {
         try
         {
            findHidDevice( DEFAULT_VID, DEFAULT_PID );
            _foundDLL = true;
         }
         catch ( DllNotFoundException ex )
         {
            _foundDLL = false;
            //System.Windows.Forms.MessageBox.Show( "Cannot Locate atusbapi.dll\r\nPlease locate the dll and place it in the same folder as the EXE", "HospWiFi Error" );
         }
      }
      /*--- end of at_usb_api() -------------------------------------------------*/
      #endregion




      #region ConnectDevice 
      /****************************************************************************
           ConnectDevice                                          08/30/09 
        Description: Attempts to connect to the device identified by VID,PID
         Parameters: none
       Return Value: true  - success
                     false - unable to connect
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/
      
      /// <summary>
      /// Attempts to connect to the device identified by VID,PID.
      /// </summary>
      /// <returns>true: success   false: unable to connect</returns>
      
      public bool ConnectDevice()
      {
         bool result = false;

         try
         {
             CloseDevice();
            result = findHidDevice( DEFAULT_VID, DEFAULT_PID );
            _foundDLL = true;
         }
         catch ( DllNotFoundException ex )
         {
            //? fix this System.Windows.Forms.MessageBox.Show( "Cannot Locate Atusbapi.dll\r\nPlease locate the dll and place it in the same folder as this EXE", "System Error" );
            _foundDLL = false;
         }

         return result;
      }
      /*--- end of ConnectDevice() ----------------------------------------------*/
      #endregion




      #region ConnectDevice 
      /****************************************************************************
           ConnectDevice                                          08/30/09 
        Description: 
         Parameters: string representations of the VID, PID
       Return Value: true  - success
                     false - unable to connect
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/
      
      /// <summary>
      /// Attempts to connect to the device identified by VID,PID.
      /// </summary>
      /// <param name="VID">Vendor ID</param>
      /// <param name="PID">Product ID</param>
      /// <returns>true: success   false: unable to connect</returns>
      
      public bool ConnectDevice( string VID, string PID )
      {
         int ivid = ConvertToHex( VID );
         int ipid = ConvertToHex( PID );
         bool result = false;
         
         try
         {
            result = findHidDevice( ivid, ipid );
            _foundDLL = true;
         }
         catch ( Exception ex )
         {
            // Fix this ? System.Windows.Forms.MessageBox.Show( "Cannot Locate Atusbapi.dll\r\nPlease locate the dll and place it in the same folder as this EXE", "System Error" );
            _foundDLL = false;
            result = false;
         }

         return result;
      }
      /*--- end of ConnectDevice() ----------------------------------------------*/
      #endregion




      #region WriteBytes 
      /****************************************************************************
           WriteBytes                                             08/30/09 
        Description: 
         Parameters: string to send to the device
       Return Value: none
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/
      
      /// <summary>
      /// Write a string of characters to the USB device.
      /// </summary>
      /// <param name="str">string to send to the device.</param>

      public bool WriteBytes( string str )
      {
          try
          {
              bool result = false;
              byte* send_buf = stackalloc byte[_PacketSize];

              for (int i = 0; i < str.Length; i++)
                  send_buf[i] = Convert.ToByte(str[i]);

              result = writeData((void*)send_buf);

              return (result);
          }
          catch
          {
              //fix this ? Mobius4MonitorService.Program.connectUSB();
              //?
              //Connect.ConnectService.connectUSB();
              //Connect.ConnectService.RestartService();
              return false;
          }
      }
      /*--- end of WriteBytes() -------------------------------------------------*/
      #endregion




      #region FormatData 
      /****************************************************************************
           FormatData                                             09/03/10
        Description: Converts the string of integers representing the bytes read
                     into printable characters. Values less than 32 are replaced
                     with an underscore.
         Parameters: string of integer values
       Return Value: converted string
         Processing: 
          Called By: ReadBytes()
           Comments: 
      ****************************************************************************/

      /// <summary>
      /// Converts the string of integers representing the bytes read
      /// into printable characters. Values less than 32 are replaced
      /// with an underscore.
      /// </summary>
      /// <param name="sin">string to convert.</param>
      /// <returns>string of converted characters/returns>

      private string FormatData( string sin )
      {
         string sout = this.FormatData( sin, false );

         return sout;
      }
      /*--- end of FormatData() -------------------------------------------------*/
      #endregion




      #region FormatData 
      /****************************************************************************
           FormatData                                             09/03/10
        Description: Converts the string of integers representing the bytes read
                     into printable characters. Values less than 32 are replaced
                     with an underscore.
         Parameters: string of integer values
       Return Value: converted string
         Processing: 
          Called By: ReadBytes()
           Comments: 
      ****************************************************************************/

      /// <summary>
      /// Converts the string of integers representing the bytes read
      /// into printable characters. Values less than 32 are replaced
      /// with an underscore.
      /// </summary>
      /// <param name="sin">string to convert.</param>
      /// <param name="InHex">true: return a hex string</param>
      /// <returns>string of converted characters/returns>

      private string FormatData( string sin, bool InHex )
      {
         string sout = "";
         string stmp = "";
         int lcp = 0;
         int cp = 0;

         if ( sin.Length < 1 ) 
            return sout;

         //  012345678901234567890123456789012345678901234567890123456789
         // "52,60,78,111,116,89,101,116,83,101,116,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"
         //   4  <  N   o   t  Y   e   t  S   e   t

         for ( int i = 0; i < _PacketSize; i++ )
         {
            lcp = cp;
            cp = sin.IndexOf( ',', cp );
            if ( cp > 0 ) 
               stmp = sin.Substring( lcp, cp - lcp );
   
            int iamt = 0;

            try
            {
               iamt = Convert.ToInt32( stmp );
            }
            catch
            {
               iamt = 0;
            }

            try
            {
               if ( iamt < 32 && !InHex )
                  sout += "_";
               else
                  sout += Convert.ToChar( iamt );
            }
            catch
            {
            }
            cp++;  // skip ',' 
         }

         if ( !InHex )
            sout = sout.Trim( new char[] { '_' } );

          return sout;
      }
      /*--- end of FormatData() -------------------------------------------------*/
      #endregion




      #region ReadBytes 
      /****************************************************************************
           ReadBytes                                              08/30/09 
        Description: Reads all available bytes from the USB
         Parameters: none 
       Return Value: string of characters read
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/
      
      /// <summary>
      /// Reads all available bytes from the USB device.
      /// </summary>
      /// <returns>string of characters read</returns>

      public string ReadBytes()
      {
         return this.ReadBytes( false );
      }
      /*--- end of ReadBytes() --------------------------------------------------*/
      #endregion




      #region ReadBytes 
      /****************************************************************************
           ReadBytes                                              08/30/09 
        Description: Reads all available bytes from the USB
         Parameters: none 
       Return Value: string of characters read
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/
      
      /// <summary>
      /// Reads all available bytes from the USB device.
      /// </summary>
      /// <param name="InHex">true: return a hex string</param>
      /// <returns>string of characters read</returns>

      public string ReadBytes( bool InHex )
      {
         byte * read_buf = stackalloc byte[ _PacketSize ];
         StringBuilder sb = new StringBuilder();

         if ( readData( read_buf ) )
         {
            for ( int i = 0; i < _PacketSize; i++ )
               sb.Append( read_buf[ i ] + "," );
         }

         if ( InHex ) 
         {
            string stmp = this.FormatData( sb.ToString(), true );
            return ASCIIToHex( stmp );
         }
         else
         {
            return this.FormatData( sb.ToString(), false );
         }
      }
      /*--- end of ReadBytes() --------------------------------------------------*/
      #endregion




      #region CloseDevice 
      /****************************************************************************
           CloseDevice                                            08/30/09 
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      /// <summary>
      /// Close the USB device.
      /// </summary>

      public void CloseDevice()
      {
         closeDevice();
      }
      /*--- end of CloseDevice() ------------------------------------------------*/
      #endregion




      #region ConvertToHex
      /****************************************************************************
           ConvertToHex                                           03/11/09
        Description: Convert a string hex value to an integer hex value.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: ConnectDevice( string VID, string PID );
           Comments: "03EB" to 0x03EB
      ****************************************************************************/
      
      /// <summary>
      /// Convert a string hex value to an integer hex value.
      /// </summary>
      /// <param name="asciiString">String to convert</param>
      /// <returns>Int32 value</returns>
      
      private Int32 ConvertToHex( string asciiString )
      {
         Int32 ihex = 0;
         Int32 multiplier = 0;
         int pos = 0;

         for ( int j = (asciiString.Length - 1); j >= 0; j-- )
         {
            switch ( pos )      // establish multiplier by position 
            {
               case 0:      multiplier = 0;             break;
               case 1:      multiplier = 16;            break;
               case 2:      multiplier = 256;           break;
               case 3:      multiplier = 4096;          break;
               case 4:      multiplier = 65536;         break;
               case 5:      multiplier = 1048576;       break;
               case 6:      multiplier = 16777216;      break;
            }

            switch ( asciiString[ j ] )
            {
               case 'f':
               case 'F':
                  if ( pos == 0 )
                     ihex += 15;
                  else
                     ihex += ( 15 * multiplier );
                  break;

               case 'e':
               case 'E':
                  if ( pos == 0 )
                     ihex += 14;
                  else
                     ihex += ( 14 * multiplier );
                  break;
               
               case 'd':
               case 'D':
                  if ( pos == 0 )
                     ihex += 13;
                  else
                     ihex += ( 13 * multiplier );
                  break;
               
               case 'c':
               case 'C':
                  if ( pos == 0 )
                     ihex += 12;
                  else
                     ihex += ( 12 * multiplier );
                  break;
               
               case 'b':
               case 'B':
                  if ( pos == 0 )
                     ihex += 11;
                  else
                     ihex += ( 11 * multiplier );
                  break;
               
               case 'a':
               case 'A':
                  if ( pos == 0 )
                     ihex += 10;
                  else
                     ihex += ( 10 * multiplier );
                  break;
               
               default:
                  int k = asciiString[ j ] - 0x30;
                  if ( pos == 0 )
                     ihex += k;
                  else
                     ihex += ( k * multiplier );
                  break;
            }

            pos++;                     // next position 
         }

         return ( ihex );
      }
      /*--- end of ConvertToHex() -----------------------------------------------*/
      #endregion




      #region ASCIIToHex
      /****************************************************************************
           ASCIIToHex                                           03/11/09
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

      /// <summary>
      /// Convert an ASCII string to a hex string.
      /// </summary>
      /// <param name="asciiString">String to convert</param>
      /// <returns>Converted string</returns>
      
      public static string ASCIIToHex( string asciiString )
      {
         StringBuilder sb = new StringBuilder();
      
         foreach ( char c in asciiString )
         {
            int tmp = c;
            sb.AppendFormat( "{0:X2} ", (uint)System.Convert.ToUInt32( tmp.ToString() ) );
         }
      
         return ( sb.ToString() );
      }
      /*--- end of ASCIIToHex() -------------------------------------------------*/
      #endregion




      #region HexToDecimal 
      /****************************************************************************
           HexToDecimal                                           04/07/10
        Description: String hex value to integer value
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 8 hex characters max: 4,294,967,295 (0xFFFFFFFF) max.
      ****************************************************************************/

      /// <summary>
      /// Convert a hex string to an integer value.
      /// </summary>
      /// <param name="sHex">String to convert</param>
      /// <returns>unsigned integer value</returns>

      private uint HexToDecimal( string sHex )
      {
         uint value = 0;
         uint Multiplier = 1;
         int len = sHex.Length;  // 8 max 

         if ( len > 8 ) 
         {
            // throw new string length exception 
            return( value );
         }

         // FFFFFFFF 
         // 01234567 

         for ( int i = len - 1; i >= 0; i-- )
         {
            switch ( sHex[ i ] )
            {
               case '0':    value +=  0 * Multiplier;    break;
               case '1':    value +=  1 * Multiplier;    break;
               case '2':    value +=  2 * Multiplier;    break;
               case '3':    value +=  3 * Multiplier;    break;
               case '4':    value +=  4 * Multiplier;    break;
               case '5':    value +=  5 * Multiplier;    break;
               case '6':    value +=  6 * Multiplier;    break;
               case '7':    value +=  7 * Multiplier;    break;
               case '8':    value +=  8 * Multiplier;    break;
               case '9':    value +=  9 * Multiplier;    break;
               case 'a':    value += 10 * Multiplier;    break;
               case 'A':    value += 10 * Multiplier;    break;
               case 'b':    value += 11 * Multiplier;    break;
               case 'B':    value += 11 * Multiplier;    break;
               case 'c':    value += 12 * Multiplier;    break;
               case 'C':    value += 12 * Multiplier;    break;
               case 'd':    value += 13 * Multiplier;    break;
               case 'D':    value += 13 * Multiplier;    break;
               case 'e':    value += 14 * Multiplier;    break;
               case 'E':    value += 14 * Multiplier;    break;
               case 'f':    value += 15 * Multiplier;    break;
               case 'F':    value += 15 * Multiplier;    break;
            }
            Multiplier = (Multiplier * 16);
         }

         return( value );
      }
      /*--- end of HexToDecimal() -----------------------------------------------*/
      #endregion


   }
}
/*--- end of at_usb_api.cs ------------------------------------------------------*/

