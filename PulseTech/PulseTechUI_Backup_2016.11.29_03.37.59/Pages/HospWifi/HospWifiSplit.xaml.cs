﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;

namespace PulseTechUI.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for HopsWifiSplit.xaml
    /// </summary>
    public partial class HospWifiSplit : UserControl
    {
        public HospWifiSplit()
        {
            InitializeComponent();
            GetAps();
        }

        public static string GetAps()
        {
            WlanClient client = new WlanClient();
            int i = 0;
            foreach (WlanClient.WlanInterface wlanInterface in client.Interfaces)
            {
                Wlan.WlanBssEntry[] wlanBssEntries = wlanInterface.GetNetworkBssList();
                foreach (Wlan.WlanBssEntry wlanBssEntry in wlanBssEntries)
                {
                    int signalStrength = wlanBssEntry.rssi; // signal strength in dBm
                    signalStrength = signalStrength * -1;
                    //Msg(i + ".Signal strength = " + signalStrength);
                    return signalStrength.ToString();
                }
            }
            return "0";
            //The following only works in certain circumstances depending on the wireless driver
            //double theSignalStrength = 0;
            //ConnectionOptions theConnectionOptions = new ConnectionOptions();
            //ManagementScope theManagementScope = new ManagementScope("root\\wmi");
            //ObjectQuery theObjectQuery = new ObjectQuery("SELECT * FROM MSNdis_80211_ReceivedSignalStrength WHERE active=true");
            //ManagementObjectSearcher theQuery = new ManagementObjectSearcher(theManagementScope, theObjectQuery);

            //try
            //{

            //    //ManagementObjectCollection theResults = theQuery.Get();
            //    foreach (ManagementObject currentObject in theQuery.Get())
            //    {
            //        theSignalStrength = theSignalStrength + Convert.ToDouble(currentObject["Ndis80211ReceivedSignalStrength"]);
            //    }
            //}
            //catch (Exception e)
            //{
            //    //handle
            //}
            //return Convert.ToDouble(theSignalStrength);
        }
    }
}
