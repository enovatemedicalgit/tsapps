﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;
using PulseTechUI.Common;

namespace PulseTechUI.Pages
{
    /// <summary>
    /// Interaction logic for LoggedInHome.xaml
    /// </summary>
    public partial class LoggedInHome : UserControl
    {
        public LoggedInHome()
        {
            InitializeComponent();
            TextLoggedInAs.Text = "Logged in as: " + PulseTechGlobals.UserName;
        }

    }
}
