﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using FirstFloor.ModernUI.Windows;
using Telerik.Windows.Documents.Fixed;

namespace PulseTechUI.Pages.Help
{
    /// <summary>
    /// Interaction logic for HelpPdf.xaml
    /// </summary>
    public partial class HelpPdf : UserControl, IContent
    {
        MemoryStream mStream = new MemoryStream();
        private bool _manualLoaded = false;

        public HelpPdf()
        {
            InitializeComponent();
        }

        private void LoadManual()
        {
            //if (_manualLoaded) return;
            //using (WebClient client = new WebClient())
            //{
            //    using (Stream ms = new MemoryStream(client.DownloadData("http://software.myenovate.com/PulseTech.pdf")))
            //    {
            //        mStream = new MemoryStream();
            //        mStream.SetLength(ms.Length);
            //        ms.Read(mStream.GetBuffer(), 0, (int) ms.Length);
            //    }
            //}
            //_manualLoaded = true;
            //PdfViewer.FitFullPage = true;
            //PdfViewer.FitToWidth = true;
            //PdfViewer.GoToPage(2);
        }

        private void CmdLoad_OnClick(object sender, RoutedEventArgs e)
        {
            ProgressRing.IsActive = true;
            var task = Task.Factory.StartNew(LoadManual, TaskCreationOptions.LongRunning).ContinueWith(EndLoad);
        }

        private void EndLoad(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                //PdfViewer.DocumentSource = new PdfDocumentSource(mStream);
                Pdf.Navigate("http://software.myenovate.com/PulseTech.pdf");
            }));


        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            //ProgressRing.IsActive = true;
            //var task = Task.Factory.StartNew(LoadManual, TaskCreationOptions.LongRunning).ContinueWith(EndLoad);
            Pdf.Navigate("http://software.myenovate.com/PulseTech.pdf");
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            
        }
    }
}
