﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;
using FirstFloor.ModernUI.Windows;
using PulseTech;
using PulseTechUI.Common;

namespace PulseTechUI.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for AssetInfo.xaml
    /// </summary>
    public partial class AssetInfo : UserControl, IContent
    {
        private readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer =
            new System.Windows.Threading.DispatcherTimer();

        private readonly System.Windows.Threading.DispatcherTimer _startupTimer =
            new System.Windows.Threading.DispatcherTimer();

        private readonly System.Windows.Threading.DispatcherTimer _pulseResponseTimer =
            new System.Windows.Threading.DispatcherTimer();


        private string _lastSerialNo = "";
        private string _serialNo = "";
        private DataTable _usbDataTable = new DataTable();
        private DataTable _dt = new DataTable();

        private string _siteId;
        private string _deptId;

        private string _apName = "";
//        private int authType = 0;
        private string _ssid = "";
        private string _channel = "";

        private string _wpaKey = "";
        private string _wepKey = "";
        private int _auth = 0;
        private bool isCharger = false;
        private string _usbResponse = "";
        private bool _connectInProgress = false;
        private DataTable _snDT = new DataTable();
        private DataTable _commDT = new DataTable();
        private int _successfulNetworkCredentials = 0;
        private int _timerCnt = 0;

        public AssetInfo()
        {
            InitializeComponent();
            PulseTechGlobals.ApSelectedEvent.NewApSelected += ApSelectedEvent_NewApSelected;

            if (UsbCommon.UsbConnected)
            {
                TxtConnected.Text = "Usb Connected";
                TxtConnected.Foreground = new SolidColorBrush(Colors.DarkGreen);
                TxtSerialNo.Text = UsbCommon.AssetStatus.DeviceSerial;
                SetDeviceType();
            }
            else
            {
                _connectInProgress = true;
                var task = Task.Factory.StartNew(SetupForUsbComm, TaskCreationOptions.LongRunning)
                    .ContinueWith(EndSetup);
            }
            _pulseResponseTimer.Tick += _pulseResponseTimer_Tick;
            _pulseResponseTimer.Interval = new TimeSpan(0, 0, 0, 2);
        }

        private void _pulseResponseTimer_Tick(object sender, EventArgs e)
        {
            _pulseResponseTimer.Stop();
            _successfulNetworkCredentials = 0;
            _timerCnt += 1;
            if (_timerCnt > 6 || TxtLastPacket.Text.Length > 2)
            {
                if (TxtLastPacket.Text.Length > 2)
                {
                    _successfulNetworkCredentials = 1;
                    var task = Task.Factory.StartNew(SendCommResults, TaskCreationOptions.LongRunning).ContinueWith(EndSendCommResults);
                    return;
                }
                else
                {
                    PulseTechGlobals.MsgBox("No Results", "No Results from device Posted to Pulse");
                }
            }
            _pulseResponseTimer.Start();
        }

        private void SendCommResults()
        {
            try
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>()
                        {
                            {"@SSID", _ssid},
                            {"@Password", _wpaKey},
                            {"@SerialNo", _serialNo},
                            {"@SiteID", _siteId},
                            {"@Channel", _channel},
                            {"@Successful", _successfulNetworkCredentials.ToString()},
                            {"@NetAuthProtocol", ConvertProtocol(_auth)}
                        };
                ApiLayer api = new ApiLayer();
                DataTable dt = new DataTable();

                dt = api.ExecSpApi("prcSaveNetwork", parameters);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        private void EndSendCommResults(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                UsbCommon.UpdateInProgress = false;
                ProgressRing.IsActive = false;
                PulseTechGlobals.MsgBox("Updated Successfull","Sucessfully Updated - Device connected to " + _ssid);
            }));
        }

        private void SetupForUsbComm()
        {
            if (!_connectInProgress)
            {
                UsbCommon.AttemptConnect();
                UsbCommon.SendCommonCommands();
            }
        }

        private void EndSetup(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (UsbCommon.UsbConnected)
                {
                    TxtConnected.Text = "Usb Connected";
                    TxtConnected.Foreground = new SolidColorBrush(Colors.DarkGreen);
                    TxtSerialNo.Text = UsbCommon.AssetStatus.DeviceSerial;
                    SetDeviceType();
                }
                else
                {
                    TxtConnected.Text = "Usb Not Connected";
                    TxtConnected.Foreground = new SolidColorBrush(Colors.DarkRed);
                }
            }));
            _connectInProgress = false;
        }

        private void ApSelectedEvent_NewApSelected(object sender, EventArgs e)
        {
            Debug.WriteLine("testing");
            _apName = ((PulseTechUI.Common.ApSelectEventArgs) (e)).ApName;
            Txtssid.Text = _apName;
            _auth = ((PulseTechUI.Common.ApSelectEventArgs) (e)).Security;
            switch (_auth)
            {
                case (int) Wlan.Dot11CipherAlgorithm.WEP:
                    //OptWep128.IsChecked = true;
                    //TxtWepKey.IsEnabled = true;
                    //optAscii.IsChecked = false;
                    LblSecurity.Content = "WEP 128";
                    OptWep128.IsChecked = true;
                    break;
                case (int) Wlan.Dot11CipherAlgorithm.None:
                    //OptOpen.IsChecked = true;
                    LblSecurity.Content = "None";
                    OptOpen.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.WPA:
                    //OptWpa1.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "WPA1 ";
                    OptWpa1.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA:
                    //OptWpa2.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "WPA2-PSK";
                    OptWpa2.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                    //OptMixed.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "Mixed WPA1 + WPA2-PSK";
                    OptMixed.IsChecked = true;
                    break;
                default:
                    break;
            }
        }

        private void CmdSetSerialNo_OnClick(object sender, RoutedEventArgs e)
        {
            if (TxtSerialNo.Text.Trim() == "")
            {
                PulseTechGlobals.MsgBox("Serial Number", "You must supply a Serial Number first");
                return;
            }
            ProgressRing.IsActive = true;
            var task =
                Task.Factory.StartNew(UpdateSerialNo, TaskCreationOptions.LongRunning).ContinueWith(EndSetSerialNo);
        }

        private void UpdateSerialNo()
        {
            UsbCommon.SetSerialNo(TxtSerialNo.Text.Trim());
        }

        private void EndSetSerialNo(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                PulseTechGlobals.MsgBox("Serial Number Set", "Successfully set serial number");
            }));
        }

        private void CmdConnect_OnClick(object sender, RoutedEventArgs e)
        {
            ProgressRing.IsActive = true;
            var task = Task.Factory.StartNew(ReConnectUsb, TaskCreationOptions.LongRunning).ContinueWith(EndConnect);
        }

        private void ReConnectUsb()
        {
            //Debug.WriteLine(DateTime.Now.ToString("hh:mm:ss t z") + "-> Start connection...");
            if (!UsbCommon.UsbConnected)
            {
                var task = Task.Factory.StartNew(SetupForUsbComm, TaskCreationOptions.LongRunning)
                    .ContinueWith(EndSetup);
                UsbCommon.AttemptConnect();
                UsbCommon.SendCommonCommands();
                //Debug.WriteLine(DateTime.Now.ToString("hh:mm:ss t z") + "->End connection...");                
            }
        }

        private void EndConnect(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                Debug.WriteLine("End connect");
                if (UsbCommon.UsbConnected)
                {
                    TxtConnected.Text = "Usb Connected";
                    TxtConnected.Foreground = new SolidColorBrush(Colors.DarkGreen);
                    _dispatcherTimer.Tick += _dispatcherTimer_Tick;
                    _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
                    _dispatcherTimer.Start();
                }
                else
                {
                    //TxtConnected.Text = "Connect Failed";
                    //TxtConnected.Foreground = new SolidColorBrush(Colors.DarkRed);
                    _startupTimer.Start();
                }
            }));
        }

        private void _dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _dispatcherTimer.Stop();
            TxtSerialNo.Text = UsbCommon.AssetStatus.DeviceSerial;
            if (!string.IsNullOrEmpty(TxtSerialNo.Text))
            {
                ChkCommunicating.Visibility = Visibility.Visible;
                _serialNo = TxtSerialNo.Text;
                if (!UsbCommon.UpdateInProgress) { 
                var pulsetask =
                    Task.Factory.StartNew(CheckPulseComm, TaskCreationOptions.LongRunning)
                        .ContinueWith(EndCheckPulseComm);
                }
            }
            if (!UsbCommon.UpdateInProgress)
            {
                var task = Task.Factory.StartNew(FindDeviceType, TaskCreationOptions.LongRunning)
                    .ContinueWith(EndDeviceType);
            }
        }

        private void CheckPulseComm()
        {
            _snDT = new DataTable();
            ApiLayer api = new ApiLayer();
            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {{"@SerialNo", _serialNo}};
            _snDT = api.ExecSpApi("prcGetQSPacket", parameters);

            parameters = new Dictionary<string, string>()
            {
                {"@UpdateOrSelect", "0"},
                {"@SerialNo", _serialNo},
            };
            _commDT = api.ExecSpApi("prcAssetsUpdateDeploy", parameters);
        }

        private void EndCheckPulseComm(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                if (_snDT.Rows.Count > 0) TxtLastPacket.Text = DateTime.Parse(_snDT.Rows[0]["CreatedDateUTC"].ToString()).ToLocalTime() + " Local Time";
                if (_commDT.Rows.Count > 0)
                {
                    ChkCommunicating.IsChecked = true;
                    if (_commDT.Rows.Count > 0)
                    {
                        TxtLastPost.Text = DateTime.Parse(_commDT.Rows[0]["LastPostDateUTC"].ToString()).ToLocalTime() + " Local Time";
                        TxtInternalIp.Text = _commDT.Rows[0]["IP"].ToString();
                        TxtDmz.Text = _commDT.Rows[0]["SourceIPAddress"].ToString();
                    }
                }
                else
                {
                    ChkCommunicating.IsChecked = false;
                }
                _dispatcherTimer.Start();
            }));
        }


        private void SetDeviceType()
        {
            switch (UsbCommon.AssetStatus.DeviceType)
            {
                case "Charger":
                    RdoCharger.IsChecked = true;
                    break;
                case "Workstation":
                    RdoWorkStation.IsChecked = true;
                    break;
                default:
                    RdoWorkStation.IsChecked = false;
                    RdoCharger.IsChecked = false;
                    break;
            }
        }

        private void FindDeviceType()
        {
            if (UsbCommon.AssetStatus.DeviceSerial != "")
            {
                if (UsbCommon.AssetStatus.DeviceSerial == null) return;
                if (UsbCommon.AssetStatus.DeviceSerial != _lastSerialNo)
                {
                    ApiLayer api = new ApiLayer();
                    var parms = new Dictionary<string, string>();
                    parms.Add("SerialNo", UsbCommon.AssetStatus.DeviceSerial);
                    try
                    {
                        _dt = api.ExecStoredProc("[prcGetAssetType]", false, parms);
                        if (_dt.Rows.Count > 0)
                        {
                            UsbCommon.AssetStatus.DeviceType = _dt.Rows[0]["Description"].ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex.Message);
                    }
                }
            }
        }

        private void EndDeviceType(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                Debug.WriteLine("EndDeviceType");
                SetDeviceType();
                CbxSites.SelectedSite = _dt.Rows[0]["SiteID"].ToString();
                CbxDepartment.SelectedDepartment = _dt.Rows[0]["DepartmentID"].ToString();
                TxtWing.Text = _dt.Rows[0]["Wing"].ToString();
                TxtFloor.Text = _dt.Rows[0]["FloorDesc"].ToString();
                TxtAssetNo.Text = _dt.Rows[0]["AssetNumber"].ToString();
            }));
        }

        private void CbxSites_OnSiteChanged(object sender, SiteSelectEventArgs e)
        {
            CbxDepartment.LoadDepartments(e.SiteId.ToString());
            _siteId = e.SiteId.ToString();
        }

        private void CbxDepartment_OnDeptChanged(object sender, DeptSelectEventArgs e)
        {
            _deptId = e.DeptId;
        }

        private void CmdAddNewDept_OnClick(object sender, RoutedEventArgs e)
        {
        }

        private void CmdUpdateAuth_OnClick(object sender, RoutedEventArgs e)
        {
            //authType = 0;
            
            isCharger = Convert.ToBoolean(RdoCharger.IsChecked);
            switch (_auth)
            {
                case (int) Wlan.Dot11CipherAlgorithm.WEP:
                    //OptWep128.IsChecked = true;
                    //TxtWepKey.IsEnabled = true;
                    //optAscii.IsChecked = false;
                    LblSecurity.Content = "WEP 128";
                    OptWep128.IsChecked = true;
                    break;
                case (int) Wlan.Dot11CipherAlgorithm.None:
                    //OptOpen.IsChecked = true;
                    LblSecurity.Content = "None";
                    OptOpen.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.WPA:
                    //OptWpa1.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "WPA1 ";
                    OptWpa1.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA:
                    //OptWpa2.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "WPA2-PSK";
                    OptWpa2.IsChecked = true;
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                    //OptMixed.IsChecked = true;
                    //optAscii.IsChecked = true;
                    LblSecurity.Content = "Mixed WPA1 + WPA2-PSK";
                    OptMixed.IsChecked = true;
                    break;
                default:
                    break;
            }

            if (OptMixed.IsChecked == true) _auth = (int) Wlan.Dot11AuthAlgorithm.RSNA_PSK;
            if (OptOpen.IsChecked == true) _auth = (int) Wlan.Dot11CipherAlgorithm.None;
            if (OptWep128.IsChecked == true) _auth = (int) Wlan.Dot11CipherAlgorithm.WEP;
            if (OptWpa1.IsChecked == true) _auth = (int) Wlan.Dot11AuthAlgorithm.WPA;
            if (OptWpa2.IsChecked == true) _auth = (int) Wlan.Dot11AuthAlgorithm.RSNA;
            //UsbCommon.SendCommandResponse("06E0get wlan");


            if (_auth == 0)
            {
                PulseTechGlobals.MsgBox("Select Wireless Type", "You must select a Wireless Type first");
                return;
            }

            if (string.IsNullOrEmpty(TxtWpaKey.Text))
            {
                PulseTechGlobals.MsgBox("Passphrase Needed", "You must enter a Passphrase");
                return;
            }
            _ssid = Txtssid.Text.Trim();
            _channel = "0";
            _wpaKey = TxtWpaKey.Text.Trim();
            ProgressRing.IsActive = true;
            UsbCommon.UpdateInProgress = true;
            var task = Task.Factory.StartNew(SendToDevice, TaskCreationOptions.LongRunning).ContinueWith(SendComplete);
          
        }


        private void SendToDevice()
        {
            string stype = "";

            if (isCharger) stype = "0";
            UsbCommon.SendCommandResponse("6E" + stype + "$$$");
            UsbCommon.UsbResp = "";
            _usbResponse = UsbCommon.UsbResp + Environment.NewLine;
            UsbCommon.SetSSID(_ssid, isCharger);
            _usbResponse += UsbCommon.UsbResp + Environment.NewLine;
            UsbCommon.SetChannel(_channel, isCharger);
            _usbResponse += UsbCommon.UsbResp + Environment.NewLine;
            _wpaKey = _wpaKey.Replace(" ", PulseTechGlobals.ReplacementChar);
            UsbCommon.SetAuth(_auth, _wpaKey, _wepKey, isCharger);
            _usbResponse += UsbCommon.UsbResp + Environment.NewLine;

            UsbCommon.SendCommandResponse("6E" + stype + "save");
            _usbResponse += UsbCommon.UsbResp + Environment.NewLine;
            UsbCommon.SendCommandResponse("6E" + stype + "reboot");
            _usbResponse += UsbCommon.UsbResp + Environment.NewLine;

            UsbCommon.SendCommand("0S");    //tell the device to send a packet immediately
            
        }

        private string ConvertProtocol(int auth)
        {
            string returnAuth = "0";
            switch (auth)
            {
                case (int) Wlan.Dot11CipherAlgorithm.WEP:
                    returnAuth = "2";
                    break;
                case (int) Wlan.Dot11CipherAlgorithm.None:
                    returnAuth = "0";
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.WPA:
                    returnAuth = "3";
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA:
                    returnAuth = "4";
                    break;
                case (int) Wlan.Dot11AuthAlgorithm.RSNA_PSK:
                    returnAuth = "2";
                    break;
                default:
                    break;
            }
            return returnAuth;
        }


        private void SendComplete(Task tsk)
        {
            _timerCnt = 0;
            _pulseResponseTimer.Start();
            //Application.Current.Dispatcher.Invoke(new Action(() =>
            //{
            //    ProgressRing.IsActive = false;
            //    PulseTechGlobals.MsgBox("Wireless Authorization", "Wireless Authorization Complete");
            //}));
        }

        

        private void CmdUpdateFacility_OnClick(object sender, RoutedEventArgs e)
        {
            DataTable dt = new DataTable();

            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                {"@SiteID", _siteId},
                {"@UpdateOrSelect", "1"},
                {"@SerialNo", TxtSerialNo.Text},
                {"@AssetNumber", TxtAssetNo.Text},
                {"@Floor", TxtFloor.Text},
                {"@ManualAllocation", "1"},
                {"@Wing", TxtWing.Text},
                {"@DepartmentID", _deptId},
            };

            try
            {
                ApiLayer api = new ApiLayer();
                dt = api.ExecSpApi("prcAssetsUpdateDeploy", parameters);
                PulseTechGlobals.MsgBox("Asset Updated",
                    "Asset Serial " + TxtSerialNo.Text + " successfully updated in Pulse.");
            }
            catch (
                Exception ex)
            {
                PulseTechGlobals.MsgBox("Asset was not updated.", ex.Message);
            }
        }

        private void LblSecurity_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (OptOpen.Visibility == Visibility.Visible)
            {
                Row1Options.Height = new GridLength(30);
                OptOpen.Visibility = Visibility.Hidden;
                OptWep128.Visibility = Visibility.Hidden;
                OptWpa1.Visibility = Visibility.Hidden;
                OptMixed.Visibility = Visibility.Hidden;
                OptWpa2.Visibility = Visibility.Hidden;
            }
            else
            {
                Row1Options.Height = new GridLength(140);
                OptOpen.Visibility = Visibility.Visible;
                OptWep128.Visibility = Visibility.Visible;
                OptWpa1.Visibility = Visibility.Visible;
                OptMixed.Visibility = Visibility.Visible;
                OptWpa2.Visibility = Visibility.Visible;
            }
        }

        private void Opt_OnChecked(object sender, RoutedEventArgs e)
        {
            var cb = sender as RadioButton;
            string cbName = cb.Content.ToString();
            string preFix = "Security Type = ";
            switch (cbName)
            {
                case "Open":
                    LblSecurity.Content = preFix + "None";
                    break;
                case "WEP-128":
                    LblSecurity.Content = preFix + " Wep 128";
                    break;
                case "WPA1":
                    LblSecurity.Content = preFix + "WPA1";
                    break;
                case "Mixed WPA1 + WPA2-PSK":
                    LblSecurity.Content = preFix + "Mixed WPA1 + WPA2-PSK";
                    break;
                case "WPA2-PSK":
                    LblSecurity.Content = preFix + "WPA2-PSK";
                    break;
                default:
                    break;
            }
        }

        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            _startupTimer.Tick += _startupTimer_Tick;
            _startupTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _startupTimer.Start();
        }

        private void _startupTimer_Tick(object sender, EventArgs e)
        {
            _startupTimer.Stop();
            var task = Task.Factory.StartNew(ReConnectUsb, TaskCreationOptions.LongRunning).ContinueWith(EndConnect);
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
        }


       

        private void CmdCheck_OnClick(object sender, RoutedEventArgs e)
        {
            string stype = "";
            isCharger = Convert.ToBoolean(RdoCharger.IsChecked);
            if (isCharger) stype = "0";

            UsbCommon.SendCommandResponse("6E" + stype + "$$$");
            UsbCommon.SendCommandResponse("6E" + stype + "get wlan");
        }

        private void ChkCommunicating_OnUnchecked(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TxtSerialNo.Text))
            {
                var pulsetask =
                    Task.Factory.StartNew(CheckPulseComm, TaskCreationOptions.LongRunning)
                        .ContinueWith(EndCheckPulseComm);
            }
        }

        private void CmdReplacement_OnClick(object sender, RoutedEventArgs e)
        {
            ReplaceChar repl = new ReplaceChar();

            repl.ShowDialog();

            if (PulseTechGlobals.ExitOperation) return;

            TxtReplacementInfo.Text = PulseTechGlobals.ReplacementChar + " will be replaced with space";
        }
    }
}