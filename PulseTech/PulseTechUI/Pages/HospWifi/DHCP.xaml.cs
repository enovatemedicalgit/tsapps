﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTechUI.Common;

namespace PulseTechUI.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for DHCP.xaml
    /// </summary>
    public partial class DHCP : UserControl
    {
        private bool _useDHCP = false;

        public DHCP()
        {
            InitializeComponent();
        }

        private void CmdUpdate_OnClick(object sender, RoutedEventArgs e)
        {
            UsbCommon.SetDhcp(_useDHCP, TxtIpAddress.Text, TxtDefaultGateway.Text, TxtSubnetMask.Text, TxtDnsServer.Text);
        }
    }
}
