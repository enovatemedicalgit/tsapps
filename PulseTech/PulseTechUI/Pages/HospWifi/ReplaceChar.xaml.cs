﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTechUI.Common;

namespace PulseTechUI.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for ReplaceChar.xaml
    /// </summary>
    public partial class ReplaceChar : Window
    {
        public ReplaceChar()
        {
            InitializeComponent();
        }

        private void CmdUpdateReplacement_OnClick(object sender, RoutedEventArgs e)
        {
            PulseTechGlobals.ExitOperation = false;
            PulseTechGlobals.ReplacementChar = TxtReplace.Text;
            this.Close();
        }

        private void CmdCharMap_OnClick(object sender, RoutedEventArgs e)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("charmap.exe");
            Process.Start(startInfo);
        }

        private void CmdCancel_OnClick(object sender, RoutedEventArgs e)
        {
            PulseTechGlobals.ExitOperation = true;
            this.Close();
        }
    }
}
