﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTechUI.Common;

namespace PulseTechUI.Pages.HospWifi
{
    /// <summary>
    /// Interaction logic for Authentication.xaml
    /// </summary>
    public partial class Authentication : UserControl
    {
        private string _apName = "";
        int authType = 0;
        private string _ssid = "";
        private string _channel = "";

        private string _wpaKey = "";
        private string _wepKey = "";

        public Authentication()
        {
            InitializeComponent();
            PulseTechGlobals.ApSelectedEvent.NewApSelected += ApSelectedEvent_NewApSelected;
            
        }

        void ApSelectedEvent_NewApSelected(object sender, EventArgs e)
        {
            Debug.WriteLine("testing");
            _apName = ((PulseTechUI.Common.ApSelectEventArgs)(e)).ApName;
            Txtssid.Text = _apName;
        }

        public void ApSelected(string apName)
        {
            Debug.WriteLine("test");
        }

        private void CmdUpdate_OnClick(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(Txtssid.Text))
            {
                PulseTechGlobals.MsgBox("SSID Required", "You must enter an SSID first");
            }

            authType = 0;

            if (OptMixed.IsChecked == true) authType = (int)PulseTechGlobals.WirelessAuthType.MixedWpa1Wpa2;
            if (OptOpen.IsChecked == true) authType = (int)PulseTechGlobals.WirelessAuthType.Open;
            if (OptWep128.IsChecked == true) authType = (int)PulseTechGlobals.WirelessAuthType.Wep128;
            if (OptWpa1.IsChecked == true) authType = (int)PulseTechGlobals.WirelessAuthType.Wpa1;
            if (OptWpa2.IsChecked == true) authType = (int)PulseTechGlobals.WirelessAuthType.Wpa2Psk;

            if (authType == 0)
            {
                PulseTechGlobals.MsgBox("Select Wireless Type", "You must select a Wireless Type first");
            }

            var task = Task.Factory.StartNew(SendToDevice, TaskCreationOptions.LongRunning).ContinueWith(SendComplete);
            _ssid = Txtssid.Text.Trim();
            _channel = TxtChannel.Text.Trim();
            _wepKey = TxtWepKey.Text.Trim();
            _wpaKey = TxtWpaKey.Text.Trim();
        }

        private void SendToDevice()
        {
            UsbCommon.SendCommandResponse("6E$$$");

            UsbCommon.SetSSID(_ssid);
            UsbCommon.SetChannel(_channel);
            UsbCommon.SetAuth(authType, _wpaKey, _wepKey);
            
            UsbCommon.SendCommandResponse("6Esave");
            UsbCommon.SendCommandResponse("6Ereboot");
        }

        private void SendComplete(Task tsk)
        {
            
        }
    }
}


