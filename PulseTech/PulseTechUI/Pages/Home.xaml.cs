﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using FirstFloor.ModernUI.Windows;
using FirstFloor.ModernUI.Windows.Controls;
using FirstFloor.ModernUI.Windows.Navigation;
using PulseTech;
using PulseTechUI.Common;

namespace PulseTechUI.Pages
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl, IContent
    {
        private ApiLayer api = new ApiLayer();
        private string _user = "";
        private string _pwd = "";
        private bool _rememberMe = false;

        public Home()
        {
            InitializeComponent();

            CbkRemember.IsChecked = Properties.Settings.Default.RememberMe;
            if (CbkRemember.IsChecked == true)
            {
                TextPassword.Password = Properties.Settings.Default.Pwd;
                TextUsername.Text = Properties.Settings.Default.Username;
            }
        }

        public delegate void RedirectLink(string newlink);



        public void OnFragmentNavigation(FirstFloor.ModernUI.Windows.Navigation.FragmentNavigationEventArgs e)
        {
            Debug.WriteLine("fragment");
        }

        public void OnNavigatedFrom(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("from");
        }

        public void OnNavigatedTo(FirstFloor.ModernUI.Windows.Navigation.NavigationEventArgs e)
        {
            Debug.WriteLine("to");
            if (PulseTechGlobals.UserLoggedIn)
            {
                Application.Current.Dispatcher.Invoke(DispatcherPriority.Background, new ThreadStart(delegate
                {
                    BBCodeBlock bbBlock = new BBCodeBlock();
                    //bbBlock.LinkNavigator.Navigate(new Uri("/Pages/LoggedInHome.xaml", UriKind.Relative), this,NavigationHelper.FrameSelf);
                    bbBlock.LinkNavigator.Navigate(new Uri("/Pages/LogOut.xaml", UriKind.Relative), this, NavigationHelper.FrameSelf);
                }));

            }
        }

        public void OnNavigatingFrom(FirstFloor.ModernUI.Windows.Navigation.NavigatingCancelEventArgs e)
        {
            if (!PulseTechGlobals.UserLoggedIn)
            {
                e.Cancel = true;
            }
        }

        private void BtnLogin_OnClick(object sender, RoutedEventArgs e)
        {

            if (TextUsername.Text.Trim() == "") return;
            
            LoginProgress.Visibility = Visibility.Visible;
            _user = TextUsername.Text;
            _pwd = TextPassword.Password;
            _rememberMe = Convert.ToBoolean(CbkRemember.IsChecked);
            var task = Task.Factory.StartNew(LoginUser, TaskCreationOptions.LongRunning).ContinueWith(EndLogin);
        }

        private void LoginUser()
        {
            try
            {

            Dictionary<string, string> parameters = new Dictionary<string, string>()
            {
                {"@username", _user},
                {"@password", PulseTechGlobals.EncryptString(_pwd)}
            };
            DataTable dt = new DataTable();

            dt = api.ExecSpApi("prcGetUser", parameters);
            if (dt.Rows.Count > 0)
            {
               
                PulseTechGlobals.Fname = dt.Rows[0]["FirstName"].ToString();
                PulseTechGlobals.UserId = dt.Rows[0]["IDUser"].ToString();
                PulseTechGlobals.UserType = Convert.ToInt32(dt.Rows[0]["UserTypeID"].ToString());
                //PulseTechGlobals.userId = Convert.ToInt32(dt.Rows[0]["IDUser"].ToString());
                PulseTechGlobals.UserSiteId = Convert.ToInt32(dt.Rows[0]["IDSite"]);
                PulseTechGlobals.UserName = _user;
                PulseTechGlobals.Password = _pwd;
                parameters = new Dictionary<string, string>() {{"@IDSite", PulseTechGlobals.UserSiteId.ToString()}};

                dt = api.ExecSpApi("prcSitesSelect", parameters);
                if (dt.Rows.Count > 1) //Found Site for User
                {
                    PulseTechGlobals.UserSiteId = Convert.ToInt32(dt.Rows[0]["IDSite"]);
                    PulseTechGlobals.SiteName = dt.Rows[0]["SiteName"].ToString();
                    //arrSites = dt;
                    string siteListString = "";

                    if (_rememberMe == true)
                    {
                        SaveParams();
                    }
                    if (dt.Rows != null)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            siteListString += dt.Rows[i]["SiteName"].ToString() + " and ";
                        }
                        siteListString = siteListString.Remove(siteListString.Length - 4, 4);
                        if (dt.Rows.Count > 20)
                        {
                            siteListString = "all Sites; you are an Enovate Employee.";
                        }
                    }
                    //MessageBox.Show("Successfully logged in as " + TextUsername.Text + " with Access To " + siteListString);

                    PulseTechGlobals.UserLoggedIn = true;
                    PulseTechGlobals.SitesDataTable = dt;

                    NavigationWindow nav = this.Parent as NavigationWindow;
                    nav.Navigate(new LoggedInHome());

                }
                else if (dt.Rows.Count > 0) //Found Site For User
                {
                    parameters = new Dictionary<string, string>() {{"@IDSite", "4"}};
                    dt = api.ExecSpApi("prcSitesAndCustomersSelect", parameters);
                    //siteID = dt.Rows[0]["IDSite"].ToString();
                    //SiteName = dt.Rows[0]["SiteName"].ToString();
                    //arrSites = dt;
                    //if (CbkRemember.IsChecked == true)
                    //{
                    //    Username = txtUsername.Text;
                    //    Password = txtPassword.Text;
                    //}
                    //MessageBox.Show("Successfully logged in as " + TextUsername.Text + " at " + dt.Rows[0]["SiteName"].ToString());
                    SaveParams();
                    PulseTechGlobals.UserLoggedIn = true;
                    PulseTechGlobals.SitesDataTable = dt;


                    //NavigationWindow nav = this.Parent as NavigationWindow;
                    //nav.Navigate(new System.Uri(url, UriKind.RelativeOrAbsolute));
                    var taskCustomer = Task.Factory.StartNew(GetCustomers, TaskCreationOptions.LongRunning).ContinueWith(EndGetCustomers);

                 
                    //NavigationWindow nav = this.Parent as NavigationWindow;
                    //nav.Navigate(new LoggedInHome());
                }
            }
            else
            {
                //MessageBox.Show("Login Failed - Username/Password Invalid.");
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    PulseTechGlobals.MsgBox("Login Failed", "Username/Password Invalid");
                }));
                
                
                //Environment.Exit(0);
            }

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    PulseTechGlobals.MsgBox("Error Logging in", ex.Message);
                }));
            }

        }


        void EndLogin(Task tsk)
        {
            LoginProgress.Dispatcher.BeginInvoke((Action)(() => LoginProgress.Visibility=Visibility.Hidden));
            if (PulseTechGlobals.UserLoggedIn)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    if (CbkRemember.IsChecked == true)
                    {
                        Properties.Settings.Default.Pwd = TextPassword.Password;
                        Properties.Settings.Default.Username = TextUsername.Text;
                        Properties.Settings.Default.Save();
                    }
                    IInputElement target = NavigationHelper.FindFrame("_top", this);
                    //NavigationCommands.GoToPage.Execute("/Pages/LoggedInHome.xaml", target);
                    NavigationCommands.GoToPage.Execute("/Pages/RhythmField/RhythmMain.xaml", target);
                }));
            }

        }

        private void GetSite()
        {
            PulseTechGlobals.SitesDataTable = api.ExecSpApi("prcSitesSelect",
                new Dictionary<string, string>() {{"@IDSite", "4"}});
        }

        private void GetCustomers()
        {
            PulseTechGlobals.CustomersDataTable = api.ExecSpApi("prcCustomersSelect",
                new Dictionary<string, string>() {{"@IDCustomer", "0"}});
        }

        private void SaveParams()
        {
            Properties.Settings.Default.Save();
        }

        private void EndGetSites(Task tsk)
        {

        }

        private void EndGetCustomers(Task tsk)
        {
            
        }
    

    private void ImgShowPassword_OnMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            TextShowPassword.Text = TextPassword.Password;
            TextPassword.Visibility = System.Windows.Visibility.Visible;
            TextShowPassword.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void ImgShowPassword_OnMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            TextShowPassword.Text = TextPassword.Password;
            TextPassword.Visibility = System.Windows.Visibility.Collapsed;
            TextShowPassword.Visibility = System.Windows.Visibility.Visible;
        }


        private void TextUsername_OnLostFocus(object sender, RoutedEventArgs e)
        {
            SaveParams();
        }

        private void TextPassword_OnLostFocus(object sender, RoutedEventArgs e)
        {
            SaveParams();
        }

        private void Home_OnLoaded(object sender, RoutedEventArgs e)
        {
            //if (PulseTechGlobals.UserLoggedIn)
            //{
            //    NavigationWindow nav = this.Parent as NavigationWindow;
            //    nav.Navigate(new LoggedInHome());
            //}
        }
    }

    public class PlaceHolderHelper : DependencyObject
    {
        #region PlaceHolderText
        public static bool GetPlaceHolderText(DependencyObject obj)
        {
            return (bool)obj.GetValue(PlaceHolderTextProperty);
        }
        public static void SetPlaceHolderText(DependencyObject obj, string value)
        {
            obj.SetValue(PlaceHolderTextProperty, value);
        }
        public static readonly DependencyProperty PlaceHolderTextProperty =
            DependencyProperty.RegisterAttached("PlaceHolderText", typeof(string),
                typeof(PlaceHolderHelper),
                new UIPropertyMetadata(string.Empty, PlaceHolderTextChanged));
        private static void PlaceHolderTextChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (!(d is PasswordBox)) return;
            ((PasswordBox)d).PasswordChanged +=
                (sender, args) =>
                {
                    var pb = sender as PasswordBox;
                    pb.SetValue(HasPasswordProperty, (pb.Password.Length > 0));
                };
        }
        #endregion
        #region HasPassword
        public bool HasPassword
        {
            get { return (bool)GetValue(HasPasswordProperty); }
            set { SetValue(HasPasswordProperty, value); }
        }
        private static readonly DependencyProperty HasPasswordProperty =
            DependencyProperty.RegisterAttached("HasPassword",
                typeof(bool), typeof(PlaceHolderHelper),
                new FrameworkPropertyMetadata(false));
        #endregion
    }
}
