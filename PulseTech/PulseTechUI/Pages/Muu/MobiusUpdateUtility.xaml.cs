﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using PulseTechUI.Common;
using Application = System.Windows.Application;
using MessageBox = System.Windows.MessageBox;
using UserControl = System.Windows.Controls.UserControl;

namespace PulseTechUI.Pages.Muu
{
    /// <summary>
    /// Interaction logic for MobiusUpdateUtility.xaml
    /// </summary>
    public partial class MobiusUpdateUtility : UserControl
    {
        private string _url = "http://software.MyEnovate.com/files/HexFiles_V3/";
        private string _bayCharger = "6.46";
        private string _bayWireless = "6.34";
        private string _control = "6.78";
        private string _dcDc = "6.26";
        private string _holster = "6.51";
        private string _lcd = "6.19";
        private string _medBoard = "6.15";
        private string _muu = "3.08";

        private string BaudRate = "38400";
        private string SSID = "EnovatePwr";
        private string HostAddress = "208.62.45.106";
        private string RemotePort = "80";
        private string RemoteResponse = "0";
        private string CommSize = "1420";
        private string CommTime = "400";
        private string ExtAnt = "1";


        private string _controlDownload = "";
        private string _holsterDownload = "";
        private string _lcdDownload = "";
        private string _dcDownload = "";
        private string _medboardDownload = "";
        private string _bayWirelessDownload = "";
        private string _bayChargerDownload = "";

        private int _pageCount = 0;
        private int _lineCnt = 0;
        private string DeviceCode = "0";

        private const int ControlBoard = 1;
        private const int DCBoard = 2;
        private const int DC2Board = 3;
        private const int HolsterBoard = 4;
        private const int LCDBoard = 5;
        private const int MedBoard = 6;
        private const int BayWirelessBoard = 7;
        private const int BayChargerBoard = 8;

        private bool _downloadComplete = false;

        private static string _tempFolder = GetTemporaryDirectory();


        private readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer =
            new System.Windows.Threading.DispatcherTimer();

        private readonly System.Windows.Threading.DispatcherTimer _newDeviceTimer =
            new System.Windows.Threading.DispatcherTimer();

        public MobiusUpdateUtility()
        {
            InitializeComponent();
            if (UsbCommon.UsbConnected)
            {
                TxtUsbConnectionStatus.Text = "Connected";
                TxtUsbConnectionStatus.Foreground = new SolidColorBrush(Colors.Green);
            }
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _dispatcherTimer.Stop();
            Debug.WriteLine("Stop timer");
            CheckConnection();
            _dispatcherTimer.Start();
        }

        private void CheckConnection()
        {
            if (!UsbCommon.UsbConnected) UsbCommon.AttemptConnect();

            if (UsbCommon.UsbConnected)
            {
                TxtUsbConnectionStatus.Text = "Connected";
                TxtUsbConnectionStatus.Foreground = new SolidColorBrush(Colors.Green);
                Debug.WriteLine("Get board info");
                var task = Task.Factory.StartNew(askBoardInfo, TaskCreationOptions.LongRunning).ContinueWith(GetBoardInfo);
                //GetBoardInfo();
            }
            else
            {
                TxtUsbConnectionStatus.Text = "Not Connected";
                TxtUsbConnectionStatus.Foreground = new SolidColorBrush(Colors.Red);
            }
            Debug.WriteLine("Start timer");
        }

        private void askBoardInfo()
        {
            UsbCommon.SendCommandResponse("6X");
        }

        private void GetBoardInfo(Task tsk)
        {
            var task = Task.Factory.StartNew(askDeviceType, TaskCreationOptions.LongRunning).ContinueWith(EndAskType);
        }

        private void askDeviceType()
        {
            switch (UsbCommon.AssetStatus.DeviceType)
            {
                case "W":
                    //this.HasMedBoard = false;
                    //this.HasDC2Board = false;
                    UsbCommon.SendCommandResponse("0A");
                    UsbCommon.SendCommandResponse("1A");
                    UsbCommon.SendCommandResponse("2K");
                    UsbCommon.SendCommandResponse("3A");
                    UsbCommon.SendCommandResponse("4A");
                    UsbCommon.SendCommandResponse("5A");
                    break;
                case "B":
                    //this.HasBC3Board = false;
                    //this.HasBC4Board = false;
                    UsbCommon.SendCommandResponse("7A");
                    UsbCommon.SendCommandResponse("8A1");
                    UsbCommon.SendCommandResponse("8A2");
                    UsbCommon.SendCommandResponse("8A3");
                    UsbCommon.SendCommandResponse("8A4");
                    break;
                default:
                    break;
            }
        }

        private void EndAskType(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                switch (UsbCommon.AssetStatus.DeviceType)
                {
                    case "W":
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.LCDFirmwareVersion))
                            TxtLcdCurrent.Text = UsbCommon.AssetStatus.LCDFirmwareVersion;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.CBFirmwareVersion))
                            TxtControlBoardCurrent.Text = UsbCommon.AssetStatus.CBFirmwareVersion;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.HCFirmware))
                            TxtHolsterCurrent.Text = UsbCommon.AssetStatus.HCFirmware;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.DC1FirmwareVersion))
                            TxtDcdc1Current.Text = UsbCommon.AssetStatus.DC1FirmwareVersion;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.DC2FirmwareVersion))
                            TxtDcdc2Current.Text = UsbCommon.AssetStatus.DC2FirmwareVersion;
                        break;
                    case "B":
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.BayWirelessRev))
                            TxtBayWirelessCurrent.Text = "6." + UsbCommon.AssetStatus.BayWirelessRev;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.BCrev1))
                            TxtBayCharger1Current.Text = "6." + UsbCommon.AssetStatus.BCrev1;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.BCrev2))
                            TxtBayCharger2Current.Text = "6." + UsbCommon.AssetStatus.BCrev2;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.BCrev3))
                            TxtBayCharger3Current.Text = "6." + UsbCommon.AssetStatus.BCrev3;
                        if (!string.IsNullOrEmpty(UsbCommon.AssetStatus.BCrev4))
                            TxtBayCharger4Current.Text = "6." + UsbCommon.AssetStatus.BCrev4;
                        break;
                    default:
                        break;
                }

                _controlDownload = "Control_" + TxtControlBoardLatest.Text.Replace(".", "") + ".hex";
                _holsterDownload = "Holster_" + TxtHolsterLatest.Text.Replace(".", "") + ".hex";
                _lcdDownload = "LCD_" + TxtLcdLatest.Text.Replace(".", "") + ".hex";
                _dcDownload = "DC_DC_" + TxtDcdc1Latest.Text.Replace(".", "") + ".hex";
                _medboardDownload = "MedBoard_" + TxtMedBoardLatest.Text.Replace(".", "") + ".hex";
                _bayWirelessDownload = "BayWireless_" + TxtBayWirelessLatest.Text.Replace(".", "") + ".hex";
                _bayChargerDownload = "BayCharger_" + TxtBayCharger1Latest.Text.Replace(".", "") + ".hex";
            }));
        }


        private void CmdCheckRevs_OnClick(object sender, RoutedEventArgs e)
        {
            GetVersions();
            LoadLatestVersions();
            CheckDiffs();
        }

        private void CheckDiffs()
        {
            CbxWorkstationControlBoard.IsChecked = false;
            CbxWorkstationHolsterCharger.IsChecked = false;
            CbxWorkstationLcDdisplay.IsChecked = false;
            CbxWorkstationDcdc1.IsChecked = false;
            CbxWorkstationDcdc2.IsChecked = false;
            CbxWorkstationMedBoard.IsChecked = false;


            CbxChargerBay1.IsChecked = false;
            CbxChargerBay2.IsChecked = false;
            CbxChargerBay3.IsChecked = false;
            CbxChargerBay4.IsChecked = false;
            CbxChargerBayWireless.IsChecked = false;

            if (TxtControlBoardCurrent.Text != TxtControlBoardLatest.Text && TxtControlBoardCurrent.Text != "")
                CbxWorkstationControlBoard.IsChecked = true;
            if (TxtDcdc1Current.Text != TxtDcdc1Latest.Text && TxtDcdc1Current.Text != "")
                CbxWorkstationDcdc1.IsChecked = true;
            if (TxtDcdc2Current.Text != TxtDcdc2Latest.Text && TxtDcdc2Current.Text != "")
                CbxWorkstationDcdc2.IsChecked = true;
            if (TxtHolsterCurrent.Text != TxtHolsterLatest.Text && TxtHolsterCurrent.Text != "")
                CbxWorkstationHolsterCharger.IsChecked = true;
            if (TxtLcdCurrent.Text != TxtLcdLatest.Text && TxtLcdCurrent.Text != "")
                CbxWorkstationLcDdisplay.IsChecked = true;
            if (TxtMedBoardCurrent.Text != TxtMedBoardLatest.Text && TxtMedBoardCurrent.Text != "")
                CbxWorkstationMedBoard.IsChecked = true;

            if (TxtBayCharger1Current.Text != TxtBayCharger1Latest.Text && TxtBayCharger1Current.Text != "")
                CbxChargerBay1.IsChecked = true;
            if (TxtBayCharger2Current.Text != TxtBayCharger2Latest.Text && TxtBayCharger2Current.Text != "")
                CbxChargerBay2.IsChecked = true;
            if (TxtBayCharger3Current.Text != TxtBayCharger3Latest.Text && TxtBayCharger3Current.Text != "")
                CbxChargerBay3.IsChecked = true;
            if (TxtBayCharger4Current.Text != TxtBayCharger4Latest.Text && TxtBayCharger4Current.Text != "")
                CbxChargerBay4.IsChecked = true;
            if (TxtBayWirelessCurrent.Text != TxtBayWirelessLatest.Text && TxtBayWirelessCurrent.Text != "")
                CbxChargerBayWireless.IsChecked = true;
        }


        private void LoadLatestVersions()
        {
            TxtBayCharger1Latest.Text = _bayCharger;
            TxtBayCharger2Latest.Text = _bayCharger;
            TxtBayCharger3Latest.Text = _bayCharger;
            TxtBayCharger4Latest.Text = _bayCharger;
            TxtBayWirelessLatest.Text = _bayWireless;
            TxtControlBoardLatest.Text = _control;
            TxtDcdc1Latest.Text = _dcDc;
            TxtDcdc2Latest.Text = _dcDc;
            TxtHolsterLatest.Text = _holster;
            TxtLcdLatest.Text = _lcd;
            TxtMedBoardLatest.Text = _medBoard;
        }

        public void GetVersions()
        {
            try
            {
                if (FindWebFile(_url + "Versions.txt"))
                {
                    var result = GetFileViaHttp(_url);
                    string str = Encoding.UTF8.GetString(result);
                    string[] strArr = str.Split(new[] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);
                    Version serverVersion = new Version(strArr[0]);
                    for (int i = 0; i < strArr.Length; i++)
                    {
                        string[] keyvalue = strArr[i].Split(' ');
                        switch (keyvalue[0])
                        {
                            case "BayCharger":
                                _bayCharger = keyvalue[1];
                                break;
                            case "BayWireless":
                                _bayWireless = keyvalue[1];
                                break;
                            case "Control":
                                _control = keyvalue[1];
                                break;
                            case "DC-DC":
                                _dcDc = keyvalue[1];
                                break;
                            case "Holster":
                                _holster = keyvalue[1];
                                break;
                            case "LCD":
                                _lcd = keyvalue[1];
                                break;
                            case "MedBoard":
                                _medBoard = keyvalue[1];
                                break;
                            case "MUU":
                                _muu = keyvalue[1];
                                break;
                        }
                    }
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                // Msg("Pulse Connect - Check Version Info Error... from " + url + " ->" + ex.Message);
            }
        }

        public static byte[] GetFileViaHttp(string url)
        {
            using (WebDownload client = new WebDownload())
            {
                client.Timeout = 15*1000;
                return client.DownloadData(url);
            }
        }

        public bool FindWebFile(string url)
        {
            HttpWebRequest myHttpWebRequest = (HttpWebRequest) WebRequest.Create(_url);
            myHttpWebRequest.Timeout = 10*1000;
            HttpWebResponse myHttpWebResponse = (HttpWebResponse) myHttpWebRequest.GetResponse();
            if (myHttpWebResponse.ContentLength > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void CmdFactoryReset_OnClick(object sender, RoutedEventArgs e)
        {
            UsbCommon.SendCommand("6Efactory RESET");
            UsbCommon.SendCommand("6Eset u b 38400");
            UsbCommon.SendCommand("6Ereboot");
        }

        private void CmdSetNew_OnClick(object sender, RoutedEventArgs e)
        {
            ProgressRing.IsActive = true;
            CmdSetNew.IsEnabled = false;
            var task = Task.Factory.StartNew(SetupNewDevice, TaskCreationOptions.LongRunning).ContinueWith(EndSetupNew);
        }

        private void SetupNewDevice()
        {
            UsbCommon.SendCommand("6G"); // set baud 9600 
            UsbCommon.SendCommand("6E$$$"); //command mode
            UsbCommon.SendCommand("6Eset u b " + BaudRate);
            UsbCommon.SendCommand("6Eset w s " + SSID);
            UsbCommon.SendCommand("6Eset i h " + HostAddress);
            UsbCommon.SendCommand("6Eset i r " + RemotePort);
            UsbCommon.SendCommand("6Eset c r " + RemoteResponse);
            UsbCommon.SendCommand("6Eset c s " + CommSize);
            UsbCommon.SendCommand("6Eset c t " + CommTime);
            UsbCommon.SendCommand("6Eset w e " + ExtAnt);

            UsbCommon.SendCommand("6Eset w c " + "0"); // set channel

            //btnSetAuth_Click(sender, e);

            UsbCommon.SendCommand("6Eset i d 0"); // turn dhcp off

            //btnSetJoin_Click(sender, e);

            UsbCommon.SendCommand("6Esave");
            UsbCommon.SendCommand("6Ereboot");

            UsbCommon.SendCommand("6H"); // 38400 
        }

        private void EndSetupNew(Task tsk)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                ProgressRing.IsActive = false;
                PulseTechGlobals.MsgBox("New Device", "Successfully set up new device");
                CmdSetNew.IsEnabled = true;
            }));
        }

        private void CmdStartUpdate_OnClick(object sender, RoutedEventArgs e)
        {
            _dispatcherTimer.Stop();
            CmdStartUpdate.IsEnabled = false;
            TxtUpdate.IsEnabled = false;
            DoEvents();
            try
            {
                if (CbxWorkstationControlBoard.IsChecked == true && !string.IsNullOrEmpty(TxtControlBoardLatest.Text))
                    UpdateRev(_controlDownload);
                if (CbxWorkstationHolsterCharger.IsChecked == true && !string.IsNullOrEmpty(TxtHolsterLatest.Text))
                    UpdateRev(_holsterDownload);
                if (CbxWorkstationLcDdisplay.IsChecked == true && !string.IsNullOrEmpty(TxtLcdLatest.Text))
                    UpdateRev(_lcdDownload);
                if (CbxWorkstationDcdc1.IsChecked == true && !string.IsNullOrEmpty(TxtDcdc1Latest.Text))
                    UpdateRev(_dcDownload);
                if (CbxWorkstationDcdc2.IsChecked == true && !string.IsNullOrEmpty(TxtDcdc2Latest.Text))
                    UpdateRev(_dcDownload);
                if (CbxWorkstationMedBoard.IsChecked == true && !string.IsNullOrEmpty(TxtMedBoardLatest.Text))
                    UpdateRev(_medboardDownload);

                if (CbxChargerBayWireless.IsChecked == true && !string.IsNullOrEmpty(TxtBayWirelessLatest.Text))
                    UpdateRev(_bayWirelessDownload);
                if (CbxChargerBay1.IsChecked == true && !string.IsNullOrEmpty(TxtBayCharger1Latest.Text))
                    UpdateRev(_bayChargerDownload);
                if (CbxChargerBay2.IsChecked == true && !string.IsNullOrEmpty(TxtBayCharger2Latest.Text))
                    UpdateRev(_bayChargerDownload, true);
                if (CbxChargerBay3.IsChecked == true && !string.IsNullOrEmpty(TxtBayCharger3Latest.Text))
                    UpdateRev(_bayChargerDownload, true);
                if (CbxChargerBay4.IsChecked == true && !string.IsNullOrEmpty(TxtBayCharger4Latest.Text))
                    UpdateRev(_bayChargerDownload, true);
            }
            catch (Exception ex)
            {
                PulseTechGlobals.MsgBox("Firmware Update Aborted", "Unable to update firmware");
            }
            CmdStartUpdate.IsEnabled = true;
            TxtUpdate.IsEnabled = true;
            _dispatcherTimer.Start();
        }

        private void StartDownload(string webResource, string fileName)
        {
            Debug.WriteLine("Start Download...");
            WebClient ptWebClient = new WebClient();
            ptWebClient.DownloadFile(webResource, fileName);
            _downloadComplete = true;
            Debug.WriteLine("Download Complete...");
        }

        private void UpdateRev(string fileName, bool haveDownload = false)
        {
            try
            {
                _downloadComplete = false;
                string[] fileTypeArr;
                string fileType = "";
                string WebResource = null;
                fileTypeArr = fileName.Split('_');
                fileType = fileTypeArr[0];
                WebResource = _url + fileName;
                ProgressRing.IsActive = true;
                DoEvents();
                if (!haveDownload)
                {
                    if (FindWebFile(WebResource))
                    {
                        _tempFolder = GetTemporaryDirectory();

                        // Msg("Downloading File " + fileName + " from " + myStringWebResource);
                        fileName = _tempFolder + @"\" + fileName;
                        Task.Factory.StartNew(() => StartDownload(WebResource, fileName),
                            TaskCreationOptions.LongRunning);
                        while (!_downloadComplete)
                        {
                            DoEvents();
                        }
                    }
                }
                ProgressRing.IsActive = false;
                DoEvents();
                DoEvents();
                switch (fileType)
                {
                    case "Control":
                        UsbCommon.SendCommand("0Zj");
                        SendHexToControl(fileName, ControlBoard);
                        JumpToBootLoader(ControlBoard);
                        break;
                    case "Holster":
                        UsbCommon.SendCommand("1Zj");
                        SendHexToControl(fileName, HolsterBoard);
                        JumpToBootLoader(HolsterBoard);
                        break;
                    case "LCD":
                        UsbCommon.SendCommand("2Zj");
                        SendHexToControl(fileName, LCDBoard);
                        JumpToBootLoader(LCDBoard);
                        break;
                    case "DC":
                        UsbCommon.SendCommand("3Zj"); //4Zj
                        SendHexToControl(fileName, DC2Board);
                        JumpToBootLoader(DC2Board);
                        break;
                    case "MedBoard":
                        UsbCommon.SendCommand("5Zj");
                        SendHexToControl(fileName, MedBoard);
                        JumpToBootLoader(MedBoard);
                        break;
                    case "BayWireless":
                        UsbCommon.SendCommand("7Zj");
                        SendHexToControl(fileName, BayWirelessBoard);
                        JumpToBootLoader(BayWirelessBoard);
                        break;
                    case "BayCharger":
                        UsbCommon.SendCommand("8Z1j");
                        SendHexToControl(fileName, BayChargerBoard);
                        JumpToBootLoader(BayChargerBoard);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                PulseTechGlobals.MsgBox("Firmware Update Failed", ex.Message);
                throw;
            }
        }

        private void SendHexToControl(string HexFileName, int whichboard)
        {
            if (UsbCommon.UsbConnected)
            {
                int totalPages = 0;
                int linecnt = 0;
                string tempFileName = System.IO.Path.GetTempFileName();
                StreamWriter sw = new StreamWriter(tempFileName, false);
                int SPICount = 0;
                StreamReader sr = new StreamReader(HexFileName);
                try
                {
                    string tbuffer = "";
                    SPICount = 0;
                    while (true)
                    {
                        string str = sr.ReadLine();
                        tbuffer = str;
                        if (str == null)
                        {
                            break;
                        }
                        int bytecnt = this.HexToDecimal(tbuffer.Substring(1, 2));
                        string.Format("{0:X}", bytecnt);
                        string str1 = tbuffer.Substring(7, 2);
                        if (str1 != null)
                        {
                            if (str1 == "00")
                            {
                                for (int q = 0; q < bytecnt*2; q++)
                                {
                                    sw.Write(tbuffer.Substring(9 + q, 1));
                                    SPICount++;
                                    if (SPICount == 32)
                                    {
                                        sw.WriteLine();
                                        SPICount = 0;
                                        linecnt++;
                                    }
                                }
                            }
                            else if (str1 == "01")
                            {
                            }
                        }
                    }
                }
                finally
                {
                    if (sr != null)
                    {
                        ((IDisposable) sr).Dispose();
                    }
                }
                while (SPICount < 32)
                {
                    sw.Write("F");
                    SPICount++;
                }
                sw.WriteLine();
                for (linecnt++; linecnt%32 != 0; linecnt++)
                {
                    sw.WriteLine("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");
                }
                sw.Close();
                totalPages = linecnt/32;

                if (UsbCommon.UsbConnected)
                {
                    LoadFlash(whichboard, tempFileName);
                }
                File.Delete(tempFileName);
            }
            else
            {
            }
        }

        private void JumpToBootLoader(int whichboard)
        {
            switch (whichboard)
            {
                case ControlBoard: // CONTROL BOARD ---------------------------------
                    //txtStatus.AppendText("BootLoading the CONTROL BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("0Zj"); // jump to bootloader 
                    break;

                case HolsterBoard: // HOLSTER CHARGER -------------------------------
                    //txtStatus.AppendText("BootLoading the HOLSTER BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("1Zj"); // jump to bootloader 
                    break;

                case LCDBoard: // LCD -------------------------------------------
                    //txtStatus.AppendText("BootLoading the LCD BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("2Zj"); // jump to bootloader 
                    break;

                case DCBoard: // DC --------------------------------------------
                    //txtStatus.AppendText("BootLoading the DC-DC 1 BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("3Zj"); // jump to bootloader 
                    break;

                case DC2Board: // DC --------------------------------------------
                    //txtStatus.AppendText("BootLoading the DC-DC 2 BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("4Zj"); // jump to bootloader 
                    break;

                case MedBoard: // MedBoard --------------------------------------------
                    //txtStatus.AppendText("BootLoading the MEDBOARD BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("5Zj"); // jump to bootloader 
                    break;

                case BayWirelessBoard: // BAY WIRELESS ----------------------------------
                    //txtStatus.AppendText("BootLoading the BAY WIRELESS BOARD" + Environment.NewLine);
                    UsbCommon.SendCommand("7Zj"); // jump to bootloader 
                    break;

                case BayChargerBoard: // BAY CHARGER -----------------------------------
                    //txtStatus.AppendText("BootLoading the BAY CHARGER BOARD(s)" + Environment.NewLine);
                    UsbCommon.SendCommand("8Z1j"); // jump to bootloader 
                    break;
            }

            return;
        }

        private void LoadFlash(int whichboard, string tempFileName)
        {
            // send command indicating a Control Board firmware update 
            UsbCommon.SendCommand(DeviceCode + "Za"); // start of reflash 

            int pageCount = 0;
            int lineCnt = 0;

            // send data packets 
            using (StreamReader sr = new StreamReader(tempFileName))
            {
                string tbuffer = "";
                string dcommand = "";
                int SPICount = 0;

                while ((tbuffer = sr.ReadLine()) != null)
                {
                    lineCnt++;
                    if (tbuffer.Length < 32) // end of file 
                    {
                        int bytecnt = (tbuffer.Length/2);
                        string sbc = ""; // string byte count 
                        if (bytecnt < 16)
                            sbc = "0" + String.Format("{0:X}", bytecnt); // string byte count 
                        else
                            sbc = String.Format("{0:X}", bytecnt); // string byte count 

                        dcommand = DeviceCode + "Zc" + sbc + tbuffer;
                        SPICount = 512; // force a page write 
                    }
                    else // normal record 
                    {
                        dcommand = DeviceCode + "Zc10" + tbuffer;
                        SPICount += 16;
                    }

                    UsbCommon.SendCommand(dcommand);


                    if (SPICount >= 512) // write a page 
                    {
                        // send command to write a page 
                        UsbCommon.SendCommand(DeviceCode + "Zw"); // write buffer 
                        SPICount = 0; // start over 
                        pageCount++;
                        //txtPageCounter.Text = pageCount.ToString();
                        //progressBar1.PerformStep();
                    }
                }

                if (SPICount > 0)
                {
                    // send command to write the final page 
                    UsbCommon.SendCommand(DeviceCode + "Zw"); // write buffer 
                    SPICount = 0; // start over 
                    pageCount++;
                    //txtPageCounter.Text = pageCount.ToString();
                    //progressBar1.PerformStep();
                }

                string subcomm = DeviceCode + "ZbC";

                // tell the device who its for 
                switch (whichboard)
                {
                    case ControlBoard:
                        subcomm = DeviceCode + "ZbC";
                        break;
                    case HolsterBoard:
                        subcomm = DeviceCode + "ZbH";
                        break;
                    case LCDBoard:
                        subcomm = DeviceCode + "ZbL";
                        break;
                    case DCBoard:
                        subcomm = DeviceCode + "Zb1";
                        break;
                    case MedBoard:
                        subcomm = DeviceCode + "ZbM";
                        break;
                    case BayChargerBoard:
                        subcomm = DeviceCode + "ZbB";
                        break;
                    case BayWirelessBoard:
                        subcomm = DeviceCode + "ZbW";
                        break;
                }

                UsbCommon.SendCommand(subcomm); // boot device 


                // finish - send the number of pages 
                string fcommand = DeviceCode + "Zf" + pageCount.ToString(); // include last page 
                UsbCommon.SendCommand(fcommand);

                subcomm = DeviceCode + "ZbX"; // invalid boot device 
                UsbCommon.SendCommand(subcomm);
                subcomm = DeviceCode + "Zf0"; // zero pages 
                UsbCommon.SendCommand(subcomm);
                Thread.Sleep(1000);
            }
        }

        public static string GetTemporaryDirectory()
        {
            string tempFolder = System.IO.Path.GetTempFileName();
            File.Delete(tempFolder);
            Directory.CreateDirectory(tempFolder);

            return tempFolder;
        }

        private int HexToDecimal(string sHex)
        {
            int value = 0;
            int len = sHex.Length;
            int Multiplier = 1;
            int i = len - 1;
            while (i >= 0)
            {
                char chr = sHex[i];
                switch (chr)
                {
                    case '0':
                    {
                        value = value;
                        goto case '@';
                    }
                    case '1':
                    {
                        value = value + Multiplier;
                        goto case '@';
                    }
                    case '2':
                    {
                        value = value + 2*Multiplier;
                        goto case '@';
                    }
                    case '3':
                    {
                        value = value + 3*Multiplier;
                        goto case '@';
                    }
                    case '4':
                    {
                        value = value + 4*Multiplier;
                        goto case '@';
                    }
                    case '5':
                    {
                        value = value + 5*Multiplier;
                        goto case '@';
                    }
                    case '6':
                    {
                        value = value + 6*Multiplier;
                        goto case '@';
                    }
                    case '7':
                    {
                        value = value + 7*Multiplier;
                        goto case '@';
                    }
                    case '8':
                    {
                        value = value + 8*Multiplier;
                        goto case '@';
                    }
                    case '9':
                    {
                        value = value + 9*Multiplier;
                        goto case '@';
                    }
                    case ':':
                    case ';':
                    case '<':
                    case '=':
                    case '>':
                    case '?':
                    case '@':
                    {
                        Label0:
                        Multiplier = Multiplier*16;
                        i--;
                        continue;
                    }
                    case 'A':
                    {
                        value = value + 10*Multiplier;
                        goto case '@';
                    }
                    case 'B':
                    {
                        value = value + 11*Multiplier;
                        goto case '@';
                    }
                    case 'C':
                    {
                        value = value + 12*Multiplier;
                        goto case '@';
                    }
                    case 'D':
                    {
                        value = value + 13*Multiplier;
                        goto case '@';
                    }
                    case 'E':
                    {
                        value = value + 14*Multiplier;
                        goto case '@';
                    }
                    case 'F':
                    {
                        value = value + 15*Multiplier;
                        goto case '@';
                    }
                    default:
                    {
                        switch (chr)
                        {
                            case 'a':
                            {
                                value = value + 10*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            case 'b':
                            {
                                value = value + 11*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            case 'c':
                            {
                                value = value + 12*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            case 'd':
                            {
                                value = value + 13*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            case 'e':
                            {
                                value = value + 14*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            case 'f':
                            {
                                value = value + 15*Multiplier;
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                            default:
                            {
                                Multiplier = Multiplier*16;
                                i--;
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            return value;
        }

        public static void DoEvents()
        {
            Application.Current.Dispatcher.Invoke(DispatcherPriority.Background,
                new Action(delegate { }));
        }
    }
}