﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using Connect;

namespace PulseTechUI.Common
{
    public class NetworkApInfo
    {
        public List<WirelessInfo> ApInfoList = new List<WirelessInfo>();

        public List<WirelessInfo> GetApList(bool showUnknown)
        {
            string apMac = "";
            int id = 0;
            int auth = 0;
            int cipher = 0;

            Dictionary<string, Wlan.WlanAvailableNetwork> ssidAuth = new Dictionary<string, Wlan.WlanAvailableNetwork>();
            try
            {
                var wlanClient = new WlanClient();

                foreach (WlanClient.WlanInterface wlanIface in wlanClient.Interfaces)
                {
                    // Lists all networks with WEP security
                    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                    foreach (Wlan.WlanAvailableNetwork network in networks)
                    {
                        Debug.WriteLine("SSID=" + GetStringForSsid(network.dot11Ssid) + "  " +
                                        network.dot11DefaultAuthAlgorithm + "-" + network.dot11DefaultCipherAlgorithm +
                                        " Bssid#=" + network.numberOfBssids.ToString());
                        if (network.dot11DefaultCipherAlgorithm == Wlan.Dot11CipherAlgorithm.WEP)
                        {
                            //Console.WriteLine("Found WEP network with SSID {0}.", GetStringForSsid(network.dot11Ssid));
                        }
                        string ssidName = System.Text.Encoding.Default.GetString(network.dot11Ssid.SSID).Replace("\0", "");
                        if (!ssidAuth.ContainsKey(ssidName))
                        {
                            ssidAuth.Add(ssidName, network);
                        }
                    }

                    // Retrieves XML configurations of existing profiles.
                    // This can assist you in constructing your own XML configuration
                    // (that is, it will give you an example to follow).
                    foreach (Wlan.WlanProfileInfo profileInfo in wlanIface.GetProfiles())
                    {
                        string name = profileInfo.profileName; // this is typically the network's SSID

                        string xml = wlanIface.GetProfileXml(profileInfo.profileName);
                    }
                }


                //Collection<String> connectedSsids = new Collection<string>();
                foreach (WlanClient.WlanInterface wlanInterface in wlanClient.Interfaces)
                {
                    //Wlan.Dot11Ssid ssid = wlanInterface.CurrentConnection.wlanAssociationAttributes.dot11Ssid;
                    //connectedSsids.Add(new String(Encoding.ASCII.GetChars(ssid.SSID, 0, (int)ssid.SSIDLength)));

                    Wlan.WlanBssEntry[] wlanBssEntries = wlanInterface.GetNetworkBssList();
                    foreach (Wlan.WlanBssEntry wlanBssEntry in wlanBssEntries)
                    {
                        byte[] macAddr = wlanBssEntry.dot11Bssid;
                        var macAddrLen = (uint) macAddr.Length;
                        var str = new string[(int) macAddrLen];
                        for (int i = 0; i < macAddrLen; i++)
                        {
                            str[i] = macAddr[i].ToString("x2");
                        }
                        string mac = string.Join("", str);

                        apMac = mac.Substring(0, 2) + ":" + mac.Substring(2, 2) + ":" + mac.Substring(4, 2) + ":" +
                                mac.Substring(6, 2) + ":" + mac.Substring(8, 2) + ":" + mac.Substring(10, 2);
                        string ssid = System.Text.Encoding.Default.GetString(wlanBssEntry.dot11Ssid.SSID)
                            .Replace("\0", "");
                        Debug.WriteLine(apMac + " " + ssid + " " + wlanBssEntry.chCenterFrequency.ToString() +
                                        "ghz - LinkQuality=" + wlanBssEntry.linkQuality.ToString() + " Rssi=" +
                                        wlanBssEntry.rssi.ToString());

                        WirelessInfo w = new WirelessInfo();
                        w.SSID = ssid;
                        if (string.IsNullOrEmpty(w.SSID)) w.SSID = "Unknown";
                        w.APmac = apMac;
                        w.Freq = Convert.ToInt32(wlanBssEntry.chCenterFrequency);
                        w.LinkQuality = Convert.ToInt32(wlanBssEntry.linkQuality);
                        w.Rssi = wlanBssEntry.rssi;
                        w.NetworkType = "AP";
                        Wlan.WlanAvailableNetwork dictItem;
                        ssidAuth.TryGetValue(ssid, out  dictItem);
                        if (ssid == "EvalCart")
                        {
                            Debug.WriteLine("Eval");
                        }
                        cipher = (int)dictItem.dot11DefaultCipherAlgorithm;
                        auth = (int)dictItem.dot11DefaultAuthAlgorithm;
                        w.Security = auth; 
                        w.FirstSeen = DateTime.Now;
                        w.LastSeen = DateTime.Now;
                        w.Id = ++id;
                        switch (w.Freq)
                        {
                            case 2412000:
                                w.Channel = 1;
                                break;
                            case 2437000:
                                w.Channel = 6;
                                break;
                            case 2462000:
                                w.Channel = 11;
                                break;
                            case 2417000:
                                w.Channel = 2;
                                break;
                            default:
                                w.Channel = 0;
                                break;
                        }
                        if (showUnknown)
                        {
                            ApInfoList.Add(w);
                        }
                        else
                        {
                            if (w.SSID != "") ApInfoList.Add(w);
                        }
                    }
                }
                ApInfoList = ApInfoList.OrderBy(o => o.SSID).ToList();
            }
            catch (Exception ex)
            {
                //APMacAddr = "00:00:00:00:00";
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        apMac = nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
            }
            return ApInfoList;
        }

        private static string GetStringForSsid(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int) ssid.SSIDLength);
        }
    }
}