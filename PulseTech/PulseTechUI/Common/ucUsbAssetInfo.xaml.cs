﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;


namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucUsbAssetInfo.xaml
    /// </summary>
    public partial class ucUsbAssetInfo : UserControl
    {
        private string _result = "Not Connected";
        private string _lastSerialNo = "";
        private DataTable _usbDataTable;

        private readonly System.Windows.Threading.DispatcherTimer _dispatcherTimer =
            new System.Windows.Threading.DispatcherTimer();

        public ucUsbAssetInfo()
        {
            InitializeComponent();
        }


        public void StartConnection()
        {
            var task = Task.Factory.StartNew(ConnectUsbDevice, TaskCreationOptions.LongRunning).ContinueWith(EndUpdate);
        }

        private void ConnectUsbDevice()
        {
            Debug.WriteLine(DateTime.Now.ToString("hh:mm:ss t z") + "-> Start connection...");
            _result = UsbCommon.AttemptConnect();
            UsbCommon.SendCommonCommands();
            Debug.WriteLine(DateTime.Now.ToString("hh:mm:ss t z") + "->End connection...");
        }

        private void LoadGrid()
        {
            Debug.WriteLine("Loading grid...");
            _dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            _dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 1);
            _dispatcherTimer.Start();
        }

        private void EndUpdate(Task tsk)
        {
            SetUsbStatus();

            if (UsbCommon.UsbConnected)
            {
                LoadGrid();
            }
        }

        private void SetUsbStatus()
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                TxtConnected.Text = _result;
                if (UsbCommon.UsbConnected) TxtConnected.Foreground = new SolidColorBrush(Colors.DarkGreen);
            }));
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            _dispatcherTimer.Stop();
            //DataTable dt = ToDataTable(UsbCommon.AssetStatus);
            LoadFromUsb();
        }

        private void LoadFromUsb()
        {
            _usbDataTable =PulseTechGlobals.ObjectToData(UsbCommon.AssetStatus);
            DataRow row = _usbDataTable.Rows[0];
            //var dictionary = row.Table.Columns.Cast<DataColumn>().ToDictionary(col => col.ColumnName, col => row.Field<string>(col.ColumnName));
            DGMobuisDetails.ItemsSource = _usbDataTable.DefaultView;
            if (UsbCommon.AssetStatus.DeviceSerial != "")
            {
                if (UsbCommon.AssetStatus.DeviceSerial == null) return;
                if (UsbCommon.AssetStatus.DeviceSerial != _lastSerialNo)
                {
                    ApiLayer api = new ApiLayer();
                    var parms = new Dictionary<string, string>();
                    parms.Add("SerialNo", UsbCommon.AssetStatus.DeviceSerial);
                    try
                    {
                        DataTable _dt = api.ExecStoredProc("[prcGetAssetType]", false, parms);
                        if (_usbDataTable.Rows.Count > 0)
                        {
                            UsbCommon.AssetStatus.DeviceType = _dt.Rows[0]["Description"].ToString();
                            _usbDataTable = PulseTechGlobals.ObjectToData(UsbCommon.AssetStatus);
                            DGMobuisDetails.ItemsSource = null;
                            DGMobuisDetails.ItemsSource = _usbDataTable.DefaultView;
                        }
                    }
                    catch
                    {
                        //PulseTechGlobals.MsgBox("No Workstations Found", "Workstation Lookup") ;
                    }
                }
            }
        }


       

        private void CmdGetInfo_OnClick(object sender, RoutedEventArgs e)
        {
            UsbCommon.AttemptConnect();
            SetUsbStatus();
            LoadFromUsb();
        }
    }
}