﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucNetworkInterfaces.xaml
    /// </summary>
    public partial class ucNetworkInterfaces : UserControl
    {

        NetworkInterface[] _interfaces;
        private string _selectedInterface = "";

        public ucNetworkInterfaces()
        {
            InitializeComponent();
            LoadNetworkList();
        }

        private void LoadNetworkList()
        {
            try
            {
                try
                {
                    _interfaces = NetworkInterface.GetAllNetworkInterfaces();
                }
                catch (Exception ex)
                {

                }
                CbxNetworkList.ItemsSource = _interfaces;
                int item = -1;
                foreach (NetworkInterface ni in _interfaces)
                {
                    item++;
                    if ( ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211)
                    {
                        if (ni.OperationalStatus == OperationalStatus.Up)
                        {
                            NetworkInterface network = ((NetworkInterface)CbxNetworkList.Items[item]);
                            CbxNetworkList.SelectedIndex = item;
                            CbxNetworkList.Text = network.Description;
                            return;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        //private void CbxSiteList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        //{
        //    PulseTechGlobals.SelectedSite = CbxSiteList.SelectedValue.ToString();
        //}

        private void CbxNetworkList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PulseTechGlobals.SelectedInterface = CbxNetworkList.SelectedValue.ToString();
        }
    }
}
