﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using FirstFloor.ModernUI.Windows.Controls;
using PulseTech;
using Telerik.Windows.Controls;

namespace PulseTechUI.Common
{
    public static class PulseTechGlobals
    {
        private static string mInternalSecret = "C0mp@ssion";
        public static string Fname = "";

        public static string UserId = "";
        public static int UserType = 4;
        public static int UserSiteId = -1;
        public static string UserName = "";
        public static string SiteName = "";
        public static string Password = "";
        public static bool UserLoggedIn = false;
        public static DataTable SitesDataTable;
        public static DataTable DepartmentsDataTable;
        public static DataTable CustomersDataTable;
        public static DataTable WingsDataTable;
        public static DataTable FloorsDataTable;
        public static string SelectedSite = "";
        public static string SelectedDepartment = "";
        public static string SelectedInterface = "";
        public static string SelectedCustomer = "";
        public static string SelectedAP = "";
        public static bool ExitOperation = false;
        public static string SelectedFloor = "";
        public static string SelectedWing = "";
        public static string ReplacementChar = "$";

        public enum WirelessAuthType
        {
            Open,
            Wep128,
            Wpa1,
            MixedWpa1Wpa2,
            Wpa2Psk
        };
            


        public static string EncryptString(string sIn)
        {
            return Crypto.EncryptStringAES(sIn, mInternalSecret);
        }

        public static void ExportToExcel(RadGridView GridView)
        {
            string extension = "xlsx";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            DialogResult result = dialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    GridView.ExportToXlsx(stream,
                      new GridViewDocumentExportOptions()
                      {
                          ShowColumnFooters = true,
                          ShowColumnHeaders = true,
                          ShowGroupFooters = true,
                          AutoFitColumnsWidth = true,
                          ExportDefaultStyles = true
                      });
                }
            }
        }

        public static void MsgBox(string header, string msg)
        {

            try
            {
                MessageBoxButton btn = MessageBoxButton.OK;
                btn = MessageBoxButton.OK;
                var result = ModernDialog.ShowMessage(msg, header, btn);

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        public static DataTable ObjectToData(object o)
        {
            DataTable dt = new DataTable("OutputData");

            DataRow dr = dt.NewRow();
            dt.Columns.Add("ItemName", typeof(string));
            dt.Columns.Add("ItemValue", typeof(string));
            //dt.Rows.Add(dr);

            //o.GetType().GetProperties().ToList().ForEach(f =>
            //{
            //    try
            //    {
            //        f.GetValue(o, null);
            //        dt.Columns.Add(f.Name, f.PropertyType);
            //        dt.Rows[0][f.Name] = f.GetValue(o, null);
            //    }
            //    catch { }
            //});

            foreach (PropertyDescriptor prop in TypeDescriptor.GetProperties(o))
            {
                dr = dt.NewRow();
                dr[0] = prop.Name;
                dr[1] = prop.GetValue(o) == null ? "" : prop.GetValue(o).ToString();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static class ApSelectedEvent
        {
            public static event EventHandler NewApSelected;

            public static void FireApSelected(EventArgs args, string apName)
            {
                var evt = NewApSelected;
                if (evt != null) evt( apName, args);
            }
        }
    }
}
