﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Connect;
using PulseTech.SMnetScan;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucApTreeView.xaml
    /// </summary>
    public partial class ucApTreeView : UserControl
    {
        List<WirelessInfo> _apList = new List<WirelessInfo>();
        private string _selectedAp = "";
        private bool _showAllAPs = false;
        private bool _loadInProgress = false;
        public event EventHandler<ApSelectEventArgs> ApChanged;


        public ucApTreeView()
        {
            InitializeComponent();
            ShowAps(Convert.ToBoolean(ChkShowUnknown.IsChecked));
        }

        public  void ShowAps(bool showUnknown)
        {
            _loadInProgress = true;
            _showAllAPs = showUnknown;
            GetApList(showUnknown);
            LoadTreeView(showUnknown);
            _loadInProgress = false;
        }

        private void LoadTreeView(bool showUnknown = false)
        {
            string lastSsid = "";
            MenuItem childItem = new MenuItem();
            MenuItem childItemDetail = new MenuItem();
            APtreeView.Items.Clear();
            MenuItem root = new MenuItem() { TvItemName = "Available APs" };

            root.IsExpanded = true;
            foreach (WirelessInfo wi in _apList)
            {
                if (wi.SSID != lastSsid )
                {
                    if (wi.SSID == "Unknown")
                    {
                        if (showUnknown)
                        {
                            childItem = new MenuItem() {TvItemName = wi.SSID};
                            //if (lastSsid != "") root.Items.Add(childItem);
                            root.Items.Add(childItem);
                            childItem.Items.Add(new MenuItem() { TvItemName = wi.APmac + "\t(" + wi.Rssi + ")" + "\tQuality=" + wi.LinkQuality, ParentName = wi.SSID });
                            //root.Items[root.Items.Count-1].IsExpanded = true;
                        }
                    }
                    else
                    {
                        childItem = new MenuItem() { TvItemName = wi.SSID };
                       
                        root.Items.Add(childItem);
                        childItem.Items.Add(new MenuItem() { TvItemName = wi.APmac + "\t(" + wi.Rssi + ")" + "\tQuality=" + wi.LinkQuality, ParentName = wi.SSID });
                    }
                }
                else
                {
                    if (wi.SSID == "Unknown")
                    {
                        if (showUnknown) childItem.Items.Add(new MenuItem() { TvItemName = wi.APmac + "\t(" + wi.Rssi + ")" + "\tQuality=" + wi.LinkQuality, ParentName = wi.SSID });
                    }
                    else
                    {
                        childItem.Items.Add(new MenuItem() { TvItemName = wi.APmac + "\t(" + wi.Rssi + ")" + "\tQuality=" + wi.LinkQuality, ParentName = wi.SSID });
                    }
                }
                lastSsid = wi.SSID;
            }
            //root.Items.Add(childItem);
                       
            APtreeView.Items.Add(root);
        }
        private void GetApList(bool showUnknown = false)
        {
            string apMac = "";
            _apList = new List<WirelessInfo>();
            Dictionary<string, Wlan.WlanAvailableNetwork> ssidAuth = new Dictionary<string, Wlan.WlanAvailableNetwork>();
            try
            {
                var wlanClient = new WlanClient();

                foreach (WlanClient.WlanInterface wlanIface in wlanClient.Interfaces)
                {
                    // Lists all networks with WEP security
                    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                    foreach (Wlan.WlanAvailableNetwork network in networks)
                    {
                        Debug.WriteLine("SSID=" + GetStringForSsid(network.dot11Ssid) + "  " +  network.dot11DefaultAuthAlgorithm + "-" + network.dot11DefaultCipherAlgorithm + " Bssid#=" + network.numberOfBssids.ToString());
                        if (network.dot11DefaultCipherAlgorithm == Wlan.Dot11CipherAlgorithm.WEP)
                        {
                            //Console.WriteLine("Found WEP network with SSID {0}.", GetStringForSsid(network.dot11Ssid));
                        }
                        string ssidName = System.Text.Encoding.Default.GetString(network.dot11Ssid.SSID).Replace("\0", "");
                        if (!ssidAuth.ContainsKey(ssidName))
                        {
                            ssidAuth.Add(ssidName, network);
                        }
                    }

                    // Retrieves XML configurations of existing profiles.
                    // This can assist you in constructing your own XML configuration
                    // (that is, it will give you an example to follow).
                    foreach (Wlan.WlanProfileInfo profileInfo in wlanIface.GetProfiles())
                    {
                        string name = profileInfo.profileName; // this is typically the network's SSID

                        string xml = wlanIface.GetProfileXml(profileInfo.profileName);
                    }
                }


                //Collection<String> connectedSsids = new Collection<string>();
                foreach (WlanClient.WlanInterface wlanInterface in wlanClient.Interfaces)
                {
                    //Wlan.Dot11Ssid ssid = wlanInterface.CurrentConnection.wlanAssociationAttributes.dot11Ssid;
                    //connectedSsids.Add(new String(Encoding.ASCII.GetChars(ssid.SSID, 0, (int)ssid.SSIDLength)));
                    
                    Wlan.WlanBssEntry[] wlanBssEntries = wlanInterface.GetNetworkBssList();
                    foreach (Wlan.WlanBssEntry wlanBssEntry in wlanBssEntries)
                    {
                        byte[] macAddr = wlanBssEntry.dot11Bssid;
                        var macAddrLen = (uint)macAddr.Length;
                        var str = new string[(int)macAddrLen];
                        for (int i = 0; i < macAddrLen; i++)
                        {
                            str[i] = macAddr[i].ToString("x2");
                        }
                        string mac = string.Join("", str);

                        apMac = mac.Substring(0, 2) + ":" + mac.Substring(2, 2) + ":" + mac.Substring(4, 2) + ":" + mac.Substring(6, 2) + ":" + mac.Substring(8, 2) + ":" + mac.Substring(10, 2);
                        string ssid = System.Text.Encoding.Default.GetString(wlanBssEntry.dot11Ssid.SSID).Replace("\0", "");
                        Debug.WriteLine(apMac + " " + ssid + " " + wlanBssEntry.chCenterFrequency.ToString() + "ghz - LinkQuality=" + wlanBssEntry.linkQuality.ToString() + " Rssi=" + wlanBssEntry.rssi.ToString() );
                        int auth = 0;
                        int cipher = 0;
                        Wlan.WlanAvailableNetwork dictItem;
                        ssidAuth.TryGetValue(ssid, out  dictItem);
                        cipher = (int)dictItem.dot11DefaultCipherAlgorithm;
                        auth = (int)dictItem.dot11DefaultAuthAlgorithm;
                        //if (ssid == "EvalCart")
                        //{
                        //    Debug.WriteLine("Eval");
                        //}
                        if (string.IsNullOrEmpty(ssid)) ssid = "Unknown";
                        if (showUnknown == true)
                        {
                            WirelessInfo w = new WirelessInfo
                            {
                                SSID = ssid,
                                APmac = apMac,
                                Security = auth,
                                Cipher = cipher,
                                Freq = Convert.ToInt32(wlanBssEntry.chCenterFrequency),
                                LinkQuality = Convert.ToInt32(wlanBssEntry.linkQuality),
                                Rssi = wlanBssEntry.rssi
                            };
                            _apList.Add(w);
                        }
                        else
                        {
                            if (ssid != "")
                            {
                                WirelessInfo w = new WirelessInfo
                                {
                                    SSID = ssid,
                                    APmac = apMac,
                                    Security = auth,
                                    Cipher = cipher,
                                    Freq = Convert.ToInt32(wlanBssEntry.chCenterFrequency),
                                    LinkQuality = Convert.ToInt32(wlanBssEntry.linkQuality),
                                    Rssi = wlanBssEntry.rssi
                                };
                                _apList.Add(w); 
                            }
                        }
                    }
                }
                _apList = _apList.OrderBy(o => o.SSID).ToList();

            }
            catch (Exception ex)
            {
                //APMacAddr = "00:00:00:00:00";
                foreach (NetworkInterface nic in NetworkInterface.GetAllNetworkInterfaces())
                {
                    if (nic.OperationalStatus == OperationalStatus.Up)
                    {
                        apMac = nic.GetPhysicalAddress().ToString();
                        break;
                    }
                }
            }
            //return apMac;
        }
        static string GetStringForSsid(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }

        private void APtreeView_OnSelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (_loadInProgress) return;
            var x = APtreeView.SelectedItem;
            if (x == null)
            {
                ShowAps(_showAllAPs);
                return;
            }
            _selectedAp = ((PulseTechUI.Common.MenuItem)(x)).ParentName;
            if (_selectedAp == null) _selectedAp = ((PulseTechUI.Common.MenuItem) (x)).TvItemName;
            PulseTechGlobals.SelectedAP = _selectedAp;
            if (_selectedAp == "Available APs")
            {
                ShowAps(_showAllAPs);
                return;
            }
            ApSelectEventArgs ApInfo = new ApSelectEventArgs();
            ApInfo.ApName = _selectedAp;
            WirelessInfo ap = GetApInfo(ApInfo.ApName);
            ApInfo.Security = ap.Security;
            
            ApInfo.LinkQuality = ap.LinkQuality;
            ApInfo.MacAddress = ap.APmac;
            ApInfo.Cipher = ap.Cipher;
            
            //OnApChange(ApInfo);

            PulseTechGlobals.ApSelectedEvent.FireApSelected(ApInfo, _selectedAp);

        }

        private WirelessInfo GetApInfo(string apName)
        {
            foreach (WirelessInfo wi in _apList)
            {
                if (wi.SSID == apName)
                {
                    return wi;
                }
            }
            return null;
        }

        public ItemsControl GetSelectedTreeViewItemParent(TreeViewItem item)
        {
            DependencyObject parent = VisualTreeHelper.GetParent(item);
            while (!(parent is TreeViewItem || parent is TreeView))
            {
                parent = VisualTreeHelper.GetParent(parent);
            }

            return parent as ItemsControl;
        }

        protected virtual void OnApChange(ApSelectEventArgs e)
        {
            EventHandler<ApSelectEventArgs> handler = ApChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void APtreeView_OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ShowAps(_showAllAPs);
        }

        private void ChkShowUnknown_OnChecked(object sender, RoutedEventArgs e)
        {
            ShowAps(true);
        }

        private void ChkShowUnknown_OnUnchecked(object sender, RoutedEventArgs e)
        {
            ShowAps(false);
        }
    }

    public class ApSelectEventArgs : EventArgs
    {
        public string ApName { get; set; }
        public int LinkQuality { get; set; }
        public string MacAddress { get; set; }
        public int Security { get; set; }
        public int Cipher { get; set; }
    }

    public class MenuItem: TreeViewItemBase
    {
        public MenuItem()
        {
            this.Items = new ObservableCollection<MenuItem>();
        }

        public string TvItemName { get; set; }
        public string ParentName { get; set; }

        public ObservableCollection<MenuItem> Items { get; set; }
    }

    public class TreeViewItemBase : INotifyPropertyChanged
    {
        private bool isSelected;
        public bool IsSelected
        {
            get { return this.isSelected; }
            set
            {
                if (value != this.isSelected)
                {
                    this.isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
        }

        private bool _isExpanded;
        public bool IsExpanded
        {
            get { return this._isExpanded; }
            set
            {
                if (value != this._isExpanded)
                {
                    this._isExpanded = value;
                    NotifyPropertyChanged("IsExpanded");
                }
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }
    }

}
