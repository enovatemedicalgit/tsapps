﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PulseTech;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucSiteFloor.xaml
    /// </summary>
    public partial class ucSiteFloor : UserControl
    {
      public event EventHandler<FloorSelectEventArgs> FloorChanged;
        private DataTable _dt = new DataTable();
        public string URL = Properties.Settings.Default.apiUrl;
        ApiLayer api = new ApiLayer();
        private string _siteId;

        public ucSiteFloor()
        {
            InitializeComponent();
        }

        public string SelectedFloor
        {
            get { return PulseTechGlobals.SelectedFloor; }
            set
            {
                CbxFloorList.SelectedIndex = FloorIdtoIndex(value);
                PulseTechGlobals.SelectedFloor = value;
            }
        }

        public int FloorIdtoIndex(string FloorId)
        {
            for (int i = 0; i < PulseTechGlobals.FloorsDataTable.Rows.Count - 1; i++)
            {
                DataRow dr = PulseTechGlobals.FloorsDataTable.Rows[i];
                if (dr["IDSiteFloor"].ToString() == FloorId) return i;
            }
            return 0;
        }

        public void LoadFloors(string idsite)
        {
            _siteId = idsite;
            var parms = new Dictionary<string, string>();
            parms.Add("SiteID", idsite);
            //}

            //this sometimes throws an error so trap and proceed

            try
            {
                _dt = api.ExecStoredProc("[prcSiteFloorsSelectBySite]", false, parms);
                PulseTechGlobals.FloorsDataTable = _dt;
            }
            catch
            {
            }

            try
            {
                CbxFloorList.ItemsSource = _dt.DefaultView;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private void CbxFloorList_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CbxFloorList.SelectedValue == null)
            {
                PulseTechGlobals.SelectedFloor = "";
                return;
            }
            FloorSelectEventArgs FloorInfo = new FloorSelectEventArgs();
            FloorInfo.FloorId = CbxFloorList.SelectedValue.ToString();
            FloorInfo.SiteId = _siteId;
            OnFloorChange(FloorInfo);
        }
        protected virtual void OnFloorChange(FloorSelectEventArgs e)
        {
            EventHandler<FloorSelectEventArgs> handler = FloorChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void CbxFloorList_OnGotFocus(object sender, RoutedEventArgs e)
        {
            if (CbxFloorList.Text == "Select Floor") CbxFloorList.Text = "";
        }

        private void CbxFloorList_OnLostFocus(object sender, RoutedEventArgs e)
        {
            if (CbxFloorList.Text == string.Empty) CbxFloorList.Text = "Select Floor";
            if (CbxFloorList.SelectedIndex == -1)
            {
                CbxFloorList.Text = "";
                PulseTechGlobals.SelectedFloor = "";
            }
        }
    }
    public class FloorSelectEventArgs : EventArgs
    {
        public string SiteId { get; set; }
        public string FloorId { get; set; }
        public string Description { get; set; }
    }
}

