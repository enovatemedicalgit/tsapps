﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PulseTechUI.Common
{
    /// <summary>
    /// Interaction logic for ucIconButton.xaml
    /// </summary>
    public partial class ucIconButton : UserControl
    {
        public ucIconButton()
        {
            InitializeComponent();
            MainGrid.DataContext = this;
            var x = MainGrid.Resources;
            DrawingImage ic = (DrawingImage)x["Icon"];
            DrawingGroup g = (DrawingGroup)ic.Drawing;
            GeometryDrawing gd = (GeometryDrawing)g.Children[0];
            //.Item[0].Geometry;
        }

        public string Text
        {
            get { return (string) GetValue(TextProperty); }
            set { SetValue( TextProperty, value); }
        }

        public  static readonly DependencyProperty TextProperty = DependencyProperty.Register(
          "Text",
          typeof(string),
          typeof(ucIconButton),
          new FrameworkPropertyMetadata(new PropertyChangedCallback(AdjustControl))
      );

        private static void AdjustControl(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            (source as ucIconButton).UpdateControls((string) e.NewValue);
        }

        private void UpdateControls(string text)
        {
            TextBlock.Text = text;
        }


        public string Icon
        {
            get { return (string)GetValue(IconProperty); }
            set { SetValue(IconProperty, value); }
        }

        public static readonly DependencyProperty IconProperty = DependencyProperty.Register(
          "Icon",
          typeof(string),
          typeof(ucIconButton),
          new FrameworkPropertyMetadata(new PropertyChangedCallback(SetIcon))
      );

        private static void SetIcon(DependencyObject source, DependencyPropertyChangedEventArgs e)
        {
            (source as ucIconButton).UpdateIcon((string)e.NewValue);
        }

        private void UpdateIcon(string text)
        {
            var x = Icon;
        }

        public object Value
        {
            get { return (object)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(object),
              typeof(ucIconButton), new PropertyMetadata(null));

    }
}
