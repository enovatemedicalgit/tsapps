﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PulseTechUI.Common
{
    public  class AssetUsbStatus
    {
        public string DeviceType { get; set; }
        public string MobiusMacAddr { get; set; }
        public string DC1Status { get; set; }
        public string CBFirmwareVersion { get; set; }
        public string BackupBatteryStatus { get; set; }
        public string BUBVoltage { get; set; }
        public string DeviceSerial { get; set; }
        public string SparePinStatus { get; set; }
        public string measuredVoltage { get; set; }
        public string MedboardRev { get; set; }
        public string HCFirmware { get; set; }
        public string BatteryErrors { get; set; }
        public string BattSerial { get; set; }
        public string BattVoltage { get; set; }
        public string BattCurrent { get; set; }
        public string BattTemp { get; set; }
        public string BattChargeLevel { get; set; }
        //public bool _goodChargeLevel { get; set; }
        public string RemainingBattTimeMin { get; set; }
        public string RemainingBattTime { get; set; }
        public string BattCycle { get; set; }
        public string BattCapacityFCC { get; set; }
        public string BattCell1 { get; set; }
        public string BattCell2 { get; set; }
        public string BattCell3 { get; set; }
        public string BattFETStatus { get; set; }
        public string BattName { get; set; }
        public string ThermTemp { get; set; }
        public string SafetyStatus { get; set; }
        public string PFStatus { get; set; }
        public string PFAlert { get; set; }
        public string BatteryStatus { get; set; }

        public string OperationStatus { get; set; }
        public string BatteryMode { get; set; }
        public string SafetyAlert { get; set; }
        public string ChargingStatus { get; set; }
        public string ChargerStatus { get; set; }
        public bool ACConnected { get; set; }
        public bool ChargerOFF { get; set; }
        public string ChargerVoltage { get; set; }
        public string ChargerCurrent { get; set; }
        public string MainBatteryOutFlag { get; set; }
        public string AdHocString { get; set; }
        public string AdHocInteger { get; set; }
        public string SendAdHocData { get; set; }
        public string XValue { get; set; }
        public string YValue { get; set; }
        public string ZValue { get; set; }
        public string XMax { get; set; }
        public string YMax { get; set; }
        public string ZMax { get; set; }
        public string Motion { get; set; }
        public string BattMaxCC { get; set; }
        public string BoardSerial { get; set; }

        public string LCDFirmwareVersion { get; set; }
        public string BuzzerState { get; set; }
        public string LCDBackLightStatus { get; set; }
        public string LCDBoardSerialNumber { get; set; }
        public string DC1FirmwareVersion { get; set; }
        public string DC2FirmwareVersion { get; set; }
        public string DC1AVoltage { get; set; }
        public string DC1AWatts { get; set; }
        public string DC1BWatts { get; set; }
        public string DC1BVoltage { get; set; }
        public string DC1CurrB { get; set; }
        public string DC1Errors { get; set; }

        public string DC2Firmware { get; set; }
        public string DC2VoltA { get; set; }
        public string DC2CurrA { get; set; }
        public string DC2VoltB { get; set; }
        public string DC2CurrB { get; set; }
        public string DC2Errors { get; set; }
        public string DC2Status { get; set; }
        public string DC2Serial { get; set; }

        public string WirelesssFirmwareVersion { get; set; }
        public string MobiusIPAddr { get; set; }
        public string APMacAddr { get; set; }
        public string LinqQuality { get; set; }
        public string bootdevice { get; set; }
        public string pagecount { get; set; }
        public string BCrev1 { get; set; }
        public string BCrev2 { get; set; }
        public string BCrev3 { get; set; }
        public string BCrev4 { get; set; }
        public bool HasBC3Board { get; set; }
        public bool HasBC4Board { get; set; }
        public string BayWirelessRev { get; set; }
    }
}
