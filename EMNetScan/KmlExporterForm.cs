﻿////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.IO;
using System.Threading;
using Inssider.Properties;
using System.Globalization;

namespace Inssider
{
    public partial class KmlExporterForm : Form
    {
        #region Private Data

        /// <summary>
        /// Represents a Waypoint node in a GPX document.
        /// </summary>      
        private class GpxWaypoint
        {
            /*
             * More obscure details such as Magnetic Variation and Geoid height are not implemented.
             * However the NmeaParser does implement them and the data could be in the log file.
             * */
            //Global Coordinates in metric degrees
            public double Latitude;
            public double Longitude;
            //Elevation/Altitude in metres
            public double Elevation;
            //UTC as read from the GPS satellites
            public string Time;
            //Cheekily used at present to record the speed in km/h
            public string Cmt;
            //type of Fix : none, 2d or 3d.
            public string Fix;
            //the current number of satellites used in the position calculation
            public int NumSatellites;
            //Horizontal Dilution of Precision
            public double HorizontalDilution;
            //Vertical Dilution of Precision
            public double VerticalDilution;
            //Position Dilution of Precision
            public double PositionDilution;
            //The <extensions> node of the GPX format is used to store the Access Point information
            public AccessPointMeasurement Extension;

            private string _name = string.Empty;
            public string Name
            {
                get
                {
                    if (null != Extension.Ssid)
                    {
                        return XmlCleanUp.CleanUp(Extension.Ssid);
                    }
                    return _name;
                }

                set { _name = XmlCleanUp.CleanUp(value); }
            }

            private string _description = string.Empty;
            //Description as recorded by the GPX logger. This is discarded and a new more detailed Description is generated.
            public string Description
            {
                get
                {
                    if (_description == string.Empty)
                    {
                        //Generates the Description string
                        string desc = Name + " [" + Extension.Mac + "]" + Environment.NewLine
                         + Extension.Privacy + Environment.NewLine
                         + Extension.Rssi + "dBm" + Environment.NewLine
                         + "Channel " + Extension.ChannelId + Environment.NewLine

                         + "GPS" + Environment.NewLine
                         + "     Lat,Lon,Alt  " + Latitude + "," + Longitude + "," + Elevation + Environment.NewLine
                         + "     Speed (km/h) " + Cmt + Environment.NewLine
                         + "     Time (UTC)   " + Time + Environment.NewLine
                         + "     Precision    " + Environment.NewLine
                         + "          Satellite Count " + NumSatellites + Environment.NewLine
                         + "          Fix Mode        " + Fix + Environment.NewLine
                         + "          VDOP            " + VerticalDilution + Environment.NewLine
                         + "          HDOP            " + HorizontalDilution + Environment.NewLine
                         + "          PDOP            " + PositionDilution + Environment.NewLine;

                        _description = desc;
                    }

                    return _description;
                }
            }
        }

 
        /// <summary>
        /// Represents an Access Point measurement. Contains information such as Ssid, Signal Level, Channel, Encryption etc.
        /// </summary>
        private struct AccessPointMeasurement
        {
            //Machine Address Code of the Access Points
            public string Mac;
            //Name of the Access Point
            public string Ssid;
            //Signal Strength of the measurement in dBm
            public int Rssi;
            //Frequency band the Ap is using.
            public uint ChannelId;
            //Encryption settings.
            public string Privacy;
            //Quality of the signal. Similar to Rssi.
            public uint SignalQuality;
            //Network type : AD-HOC, Infrastructure etc.
            public string NetworkType;
            //Supported data Rates. More accurately this should be an array.
            public string Rates;
        }

       //Contains all waypoints imported from all GPX files.
        GpxWaypoint[] _gpxWaypoints;

        //The length of _gpxWaypoints
        int _dataPoints;

        //The waypoints for each individual file.
        ArrayList[] _gpxWaypointsByFile;

        //Contains the Mac address of _gpxWaypointsByFile. This is used as a key to sort it so that all an access points measurements are adjacent in the array.
        //Beneficial for finding unique APs quickly.
        ArrayList[] _gpxMacAddresses;

        //2D-Array with a list of Access Points for each channel and encryption type.
        //Beneficial for outputting the KML files as raw text rather than having to treat them as XML (faster).
        readonly ArrayList[,] _channelAndEncryptionIdxs = new ArrayList[MaxChannels, Encryption.Length];

        //The filenames of all files selected from the FileDialog.
        readonly string[] _fileNames = new string[1];

        //Indicates whether that file contains valid GPX data.
        bool[] _isFileValid;

        //Encryption/Privacy settings this program recognises as unique types of encryption.
        static readonly string[] Encryption = { "Unknown", "None", "WEP", "WPA", "WPA-TKIP", "WPA2" };

        //Reset events for the threads which load and process the input and output files.
        readonly ManualResetEvent[] _resetEvents = {new ManualResetEvent(false)};

        //The Mac for each unique Access Point.
        //These are uses as keys for sorting.
        string[] _mac;

        //Array of all unique Access Points.
        //It is a GpxWaypoint which contains the values of the strongest signal point.
        GpxWaypoint[] _apList;

        //As _apList is sorted by Mac address. Access Points occupy continious blocks in the _gpxWaypoints array.
        //These two variables specify at what index in that array this Access Points starts and stops.
        int[] _apStart;
        int[] _apStop;

        //Number of individual Ap KML files which remain to be output.
        //When this equals zero the ResetEvent will be triggered & all files will have been output.
        int _numBusy;

        //STATISTICS
        //The maximum number of channels allowed. 14 thanks to Japan.
        const int MaxChannels =165;

        // The number of access points which use a given channel.
        readonly int[] _channelCounts = new int[MaxChannels];

        //The number of access points which use a given type of encryption
        readonly int[] _encryptionCounts = new int[Encryption.Length];

        //EXPORTING

        //The folder to which all files will be exported.
        string _exportFolder = "";

        //According to the user-specified criteria, these items in the _gpxWaypoints array should be discarded.
        bool[] _ignoreWaypoint;

        //According to the user-specified criteria, this Ap has no valid waypoints measurements at all.
        //That is, all the ignoreWaypoints corresponding to this Ap are true.
        bool[] _ignoreAp;

        //The folder to which the individual Ap KML files will be output.
        string _apFolder;

        //Does the user what dBm labels on all the markers?
        bool _showLabels;

        //Global variables of the combo-boxes to allow cross-thread reading of their values.
        int _filesSummaryComboBoxSelectedIndex;

        //Encryption types
        private enum EncryptionType
        {
            Unknown,
            None,
            Wep,
            Wpa,
            WpaTkip,
            Wpa2Rsna
        }

        #endregion

        #region Constructors

        public KmlExporterForm()
        {
            _filesSummaryComboBoxSelectedIndex = 0;
            _showLabels = false;
            InitializeComponent();
        }

        #endregion

        #region File Functions

        /// <summary>
        /// Loads GPX file of Name filenames[e] to _gpxWaypointsByFile[e]
        /// </summary>
        /// <param name="fileName">index to be processed.</param> 
        /// <param name="index"></param>
        private void LoadGpxFile(string fileName, int index)
        {
            //The user may or may not have selected a valid XML file.
            //If the file is invalid, indicate this in the isValidFile array.
            try
            {
                //XmlTextReader is many times faster than XmlDocument.Load()
                XmlTextReader gpxReader = new XmlTextReader(fileName);
                //List of GPX waypoints that have been read in.
                ArrayList gpxPoints = new ArrayList();
                //With their corresponding Mac addresses for use as keys.
                ArrayList gpxMaCs = new ArrayList();

                do
                {
                    string name  = gpxReader.Name;
                    if (name == "wpt")
                    {
                        //The new waypoint which will eventually be added to _gpxWaypoints array.
                        GpxWaypoint newWpt = new GpxWaypoint();

                        //Latitude and Longitude as attributes rather than elements.
                        if (gpxReader.HasAttributes)
                        {
                            newWpt.Latitude = Convert.ToDouble(gpxReader.GetAttribute("lat"),CultureInfo.InvariantCulture);
                            newWpt.Longitude = Convert.ToDouble(gpxReader.GetAttribute("lon"), CultureInfo.InvariantCulture);
                        }

                        gpxReader.Read();

                        string subnodeName;
                        do
                        {
                            //Name of the element currently being read in
                            subnodeName = gpxReader.Name;
                            switch (subnodeName)
                            {
                                case "ele":
                                    newWpt.Elevation = Convert.ToDouble(gpxReader.ReadString(), CultureInfo.InvariantCulture);
                                    gpxReader.Read();
                                    break;
                                case "time":
                                    newWpt.Time = gpxReader.ReadString();
                                    gpxReader.Read();
                                    break;
                                case "name":
                                    newWpt.Name = gpxReader.ReadString();
                                    gpxReader.Read();
                                    break;
                                case "cmt":
                                    newWpt.Cmt = gpxReader.ReadString();
                                    gpxReader.Read();
                                    break;
                                //Description is not read from GPX. It is remade by the LogViewer.
                                case "desc":
                                    gpxReader.Read();
                                    gpxReader.Read();
                                    break;
                                case "fix":
                                    newWpt.Fix = gpxReader.ReadString();
                                    gpxReader.Read();
                                    break;
                                case "sat":
                                    newWpt.NumSatellites = Convert.ToInt32(gpxReader.ReadString(), CultureInfo.InvariantCulture);
                                    gpxReader.Read();
                                    break;
                                case "vdop":
                                    newWpt.VerticalDilution = Convert.ToDouble(gpxReader.ReadString(), CultureInfo.InvariantCulture);
                                    gpxReader.Read();
                                    break;
                                case "hdop":
                                    newWpt.HorizontalDilution = Convert.ToDouble(gpxReader.ReadString(), CultureInfo.InvariantCulture);
                                    gpxReader.Read();
                                    break;
                                case "pdop":
                                    newWpt.PositionDilution = Convert.ToDouble(gpxReader.ReadString(), CultureInfo.InvariantCulture);
                                    gpxReader.Read();
                                    break;
                                //Blank tags should be ignored
                                case "":
                                    break;
                                //Closing tag of this element.
                                case "wpt":
                                    break;
                                case "extensions":
                                    gpxReader.Read();
                                    string subsubnodeName;
                                    //The extensions element specifies information about our access point at this spot.
                                    AccessPointMeasurement newAPmeasurement = new AccessPointMeasurement();
                                    do
                                    {
                                        subsubnodeName = gpxReader.Name;
                                        switch (subsubnodeName)
                                        {
                                            case "MAC":
                                                string macAddress = gpxReader.ReadString();
                                                gpxMaCs.Add(macAddress);
                                                newAPmeasurement.Mac = macAddress;
                                                gpxReader.Read();
                                                break;
                                            case "SSID":
                                                newAPmeasurement.Ssid = (gpxReader.ReadString());
                                                gpxReader.Read();
                                                break;
                                            case "RSSI":
                                                newAPmeasurement.Rssi = Convert.ToInt32(gpxReader.ReadString());
                                                gpxReader.Read();
                                                break;
                                            case "ChannelID":
                                                newAPmeasurement.ChannelId = Convert.ToUInt32(gpxReader.ReadString());
                                                gpxReader.Read();
                                                break;
                                            case "privacy":
                                                newAPmeasurement.Privacy = gpxReader.ReadString();
                                                gpxReader.Read();
                                                break;
                                            case "signalQuality":
                                                newAPmeasurement.SignalQuality = Convert.ToUInt32(gpxReader.ReadString());
                                                gpxReader.Read();
                                                break;
                                            case "networkType":
                                                newAPmeasurement.NetworkType = gpxReader.ReadString();
                                                gpxReader.Read();
                                                break;
                                            case "rates":
                                                newAPmeasurement.Rates = gpxReader.ReadString();
                                                gpxReader.Read();
                                                break;
                                                //Blank tags should be ignored.
                                            case "":
                                                break;
                                                //Closing tag should be skipped over also.
                                            case "extensions":
                                                break;
                                            default:
                                                gpxReader.Read();
                                                gpxReader.Read();
                                                break;
                                        }

                                        gpxReader.Read();
                                    }
                                    while (subsubnodeName != "extensions");
                                    newWpt.Extension = newAPmeasurement;

                                    break;
                                    //Any other elements of GPX not implemented here but possibilty present in the log.
                                    //e.g. magvar,geoidheight,src,link,sym,type,ageofdgpsdata,dgpsid
                                default:
                                    gpxReader.Read();
                                    gpxReader.Read();
                                    break;
                            }

                            gpxReader.Read();
                        }
                        while (subnodeName != "wpt");

                        //check to make sure it is actually a valid waypoint with data
                        if (newWpt.NumSatellites > 0)
                            gpxPoints.Add(newWpt);
                    }
                } while (gpxReader.Read());

                //Save the parsed values
                _gpxWaypointsByFile[index] = gpxPoints;
                _gpxMacAddresses[index] = gpxMaCs;
                //If you made it this far, the file is valid.
                _isFileValid[index] = true;
            }
            //Something happened, probably a non-XML file was selected.
            catch (XmlException)
            {
                _isFileValid[index] = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadInputFiles()
        {
            //Continue no further unless there is at least one file to process.
            if (_fileNames.Length > 0)
            {
                //INITILISATION OF VARIABLES
                //Initialised the _isFileValid array. Files are assumed to be invalid until they are loaded correctly.
                _isFileValid = new bool[_fileNames.Length];

                //These arrays contain the _gpxWaypoints and _mac address so each file.
                _gpxWaypointsByFile = new ArrayList[_fileNames.Length];
                _gpxMacAddresses = new ArrayList[_fileNames.Length];

                //For every file, queue a thread in the pool.
                for (int i = 0; i < _fileNames.Length; i++)
                {
                    LoadGpxFile(_fileNames[i], i);
                }

                //A List of all waypoints and _mac addresses.
                ArrayList gpxList = new ArrayList();
                ArrayList gpxMacList = new ArrayList();
                //How many of the files chosen by the user were actually valid.
                int validFileCount = 0;

                //Keep appending the waypoints from each file to the waypoint and Mac lists if the file is valid.
                for (int i = 0; i < _fileNames.Length; i++)
                {
                    if (_isFileValid[i])
                    {
                        gpxList.AddRange(_gpxWaypointsByFile[i]);
                        gpxMacList.AddRange(_gpxMacAddresses[i]);
                        validFileCount++;
                    }
                }

                //_gpxWaypoints is derived from the gpxList
                _gpxWaypoints = (GpxWaypoint[])gpxList.ToArray(typeof(GpxWaypoint));
                //The total number of datapoints from all the files combined.
                _dataPoints = _gpxWaypoints.Length;

                //If at least one of the files was a valid GPX which contained at least one valid waypoint
                if (_dataPoints > 0 && validFileCount > 0)
                {
                    //key used to sort GpxWaypoint array.
                    _mac = (string[])gpxMacList.ToArray(typeof(string));

                    //some .gpx files strangely don't have an equal number of mac addresses and waypoints
                    //NOTE: this is currently a quick fix solution and should probably be changed, however this error rarely occurs
                    if (_mac.Length != _gpxWaypoints.Length)
                    {
                        MessageBox.Show("Could not export to .kml: an error in one of the .gpx input files.",
                                        "Invalid .GPX file", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        return;
                    }

                    //_gpxWaypoints will now contain the measurements for each access point in contigious blocks.
                    Array.Sort(_mac, _gpxWaypoints);

                    //temporary list of unique access points and their start and stop positions in the _gpxWaypoints array.
                    ArrayList apList = new ArrayList();
                    ArrayList apStart = new ArrayList();
                    ArrayList apStop = new ArrayList();

                    //The first point will always be a new access point
                    apList.Add(_gpxWaypoints[0]);
                    apStart.Add(0);

                    //Lists representing the best Rssi for a unique access point and the index in the gpwWaypoints array at which it is Measured
                    ArrayList apBestSignal = new ArrayList();
                    ArrayList apBestSignalIdx = new ArrayList();

                    //The first point is the strongest (and only) point seen so far.
                    int bestSignal = _gpxWaypoints[0].Extension.Rssi;
                    apBestSignal.Add(bestSignal);
                    apBestSignalIdx.Add(0);

                    for (int j = 1; j < _dataPoints; j++)
                    {
                        //If this waypoint has a different _mac to the previous waypoint, then this is a new access point (access points are stored in order in the array)
                        if (_gpxWaypoints[j].Extension.Mac != _gpxWaypoints[j - 1].Extension.Mac)
                        {
                            //the previous access point's block in _gpxWaypoints finished with the last node
                            apStop.Add(j - 1);
                            //the new access point starts here
                            apStart.Add(j);
                            //Add this new waypoint.
                            apList.Add(_gpxWaypoints[j]);
                            //The first point is the strongest (and only) point seen so far.
                            bestSignal = _gpxWaypoints[j].Extension.Rssi;
                            apBestSignal.Add(bestSignal);
                            apBestSignalIdx.Add(j);
                        }
                        //If the Mac address of this point is the same as the last.
                        else
                        {
                            //If the signal is the strongest we've seen so far.
                            if (_gpxWaypoints[j].Extension.Rssi > bestSignal)
                            {
                                bestSignal = _gpxWaypoints[j].Extension.Rssi;
                                apBestSignalIdx[apList.Count - 1] = j;
                                apBestSignal[apList.Count - 1] = bestSignal;
                            }
                        }
                    }
                    //the last access point will always finish where the array does.
                    apStop.Add((_dataPoints - 1));

                    //From now on arrays are used rather than lists. (Array.sort function is handy)
                    _apList = (GpxWaypoint[])apList.ToArray(typeof(GpxWaypoint));
                    _apStart = (int[])apStart.ToArray(typeof(int));
                    _apStop = (int[])apStop.ToArray(typeof(int));

                    //Initialise array of SSIDs. This will be used to sort the Access Point arrays in alphabetical order.

                    //For every unique access point use the strongest waypoint as the GpxWaypoint stored in the _apList array.
                    for (int i = 0; i < _apList.Length; i++)
                    {
                        int idx = (int)apBestSignalIdx[i];

                        GpxWaypoint tempGpXpoint = _apList[i];

                        // Use the details of the strongest signal point as the information for the access point
                        tempGpXpoint.Latitude = _gpxWaypoints[idx].Latitude;
                        tempGpXpoint.Longitude = _gpxWaypoints[idx].Longitude;
                        tempGpXpoint.Elevation = _gpxWaypoints[idx].Elevation;

                        //tempGPXpoint.desc = _gpxWaypoints[idx].desc;
                        tempGpXpoint.Fix = _gpxWaypoints[idx].Fix;
                        tempGpXpoint.Cmt = _gpxWaypoints[idx].Cmt;
                        tempGpXpoint.HorizontalDilution = _gpxWaypoints[idx].HorizontalDilution;
                        tempGpXpoint.PositionDilution = _gpxWaypoints[idx].PositionDilution;
                        tempGpXpoint.NumSatellites = _gpxWaypoints[idx].NumSatellites;
                        tempGpXpoint.Time = _gpxWaypoints[idx].Time;
                        tempGpXpoint.VerticalDilution = _gpxWaypoints[idx].VerticalDilution;

                        tempGpXpoint.Extension.Rssi = _gpxWaypoints[idx].Extension.Rssi;
                        tempGpXpoint.Extension.SignalQuality = _gpxWaypoints[idx].Extension.SignalQuality;
                        tempGpXpoint.Extension.Privacy = _gpxWaypoints[idx].Extension.Privacy;
                        tempGpXpoint.Extension.ChannelId = _gpxWaypoints[idx].Extension.ChannelId;
                        tempGpXpoint.Extension.NetworkType = _gpxWaypoints[idx].Extension.NetworkType;
                        tempGpXpoint.Extension.Rates = _gpxWaypoints[idx].Extension.Rates;

                        //Store the newly updated point.
                        _apList[i] = tempGpXpoint;

                        //Now extract the values for display in the dataview
                        AccessPointMeasurement tempPoint = tempGpXpoint.Extension;

                        //Keeps a running tally of the number of access points using a given channel.
                        //But only if channel is above 0
                        //NOTE: If channel is 0 an IndexOutOfRangeException is thrown because ChannelId(is a uint) - 1 becomes 4294967295
                        if (tempPoint.ChannelId > 0) { _channelCounts[tempPoint.ChannelId - 1]++; }

                        //Keeps a running tally of the number of access points using a given type of encryption.
                        _encryptionCounts[(int)GetEncryptionIdx(tempPoint.Privacy)]++;

                    }

                }
                else
                {
                    //The file might be an empty GPX, or an XML file that isn't in GPX format.
                    MessageBox.Show("This file doesn't appear to have any GPS data, or the GPS never acquired a fix.");
                }
            }
        }

        #endregion

        #region UI Functions

        private void UpdateComprehensiveFileOptions()
        {
            organizeByLabel.Enabled = filesComprehensiveFileCheckBox.Checked;
            FilesComprehensiveFileComboBox.Enabled = filesComprehensiveFileCheckBox.Checked;
        }

        /// <summary>
        /// Enables/disables the Export button based on status of export options
        /// </summary>
        private void UpdateExportButtonStatus()
        {
            exportButton.Enabled = inputFileTextBox.Text.Length > 0 && exportFolderTextBox.Text.Length > 0 &&
                (filesSummaryCheckBox.Checked || filesIndividualCheckBox.Checked || filesComprehensiveFileCheckBox.Checked);
        }


        private void SaveSettings()
        {
            //Save all the latest chosen options as defaults for next Time around.
            //File options
            Settings.Default.export_inputFile = inputFileTextBox.Text;
            Settings.Default.export_filesFolder = exportFolderTextBox.Text;
            //Settings.Default.export_FilesCombine = filesCombineCheckBox.Checked;
            Settings.Default.export_FilesSummary = filesSummaryCheckBox.Checked;
            Settings.Default.export_FilesIndividualAPs = filesIndividualCheckBox.Checked;
            Settings.Default.export_FilesComprehensive = filesComprehensiveFileCheckBox.Checked;
            Settings.Default.export_FilesOrganiseByIdx = FilesComprehensiveFileComboBox.SelectedIndex;

            //Visibility Options
            //Settings.Default.export_VisAltitudeIdx = visAltComboBox.SelectedIndex;
            //Settings.Default.export_VisMarkerVisibility = visVisibleCheckBox.Checked;
            //Settings.Default.export_VisExtendedToGround = visExtrudeCheckBox.Checked;
            Settings.Default.export_VisShowLabels = visLabelsCheckBox.Checked;
            //Settings.Default.export_VisWPATKIPColorRed = visWPAColorCheckBox.Checked;
            //Settings.Default.export_VisIncludeTimestamp = visTimestampCheckBox.Checked;

            //Data Quality Options
            Settings.Default.export_DQLockedUp = dqGPSLockedUpCheckBox.Checked;
            Settings.Default.export_DQFixLost = dqSatFixLostCheckBox.Checked;
            Settings.Default.export_DQSatLessThan = dqSatCountCheckBox.Checked;
            Settings.Default.export_DQSatLessThanCount = (int)dqSatCountUpDown.Value;
            Settings.Default.export_DQTooFast = dqSpeedCheckBox.Checked;
            Settings.Default.export_DQTooFastSpeed = (int)dqSpeedUpDown.Value;
            Settings.Default.export_DQTooStrong = dqRSSICheckBox.Checked;
            Settings.Default.export_DQTooStrongStrength = (int)dqRSSIUpDown.Value;
        }


        #endregion

        #region Export Functions

        private void ExportKml()
        {
            //If there is at least one piece of data, and there's an output path to put it to
            if (_dataPoints > 0 && exportFolderTextBox.Text.Length > 0)
            {
                //These global variables are used to allow cross-thread reading of the properties of these GUI controls.
                //visAltComboBox_SelectedIndex = visAltComboBox.SelectedIndex;
                _filesSummaryComboBoxSelectedIndex = FilesComprehensiveFileComboBox.SelectedIndex;
                _showLabels = visLabelsCheckBox.Checked;
                //IsTkipRed = visWPAColorCheckBox.Checked;

                //If the user didn't use a slash at the end of the path, then add one
                if (_exportFolder[_exportFolder.Length - 1] != '\\')
                {
                    _exportFolder += @"\";
                }

#if !DEBUG
                try
                {
#endif
                    //Create the export directory.
                    Directory.CreateDirectory(_exportFolder);

                    //This will be the output sub-folder for the individual access point's KMLs if that option is selected.
                    _apFolder = _exportFolder + @"APs\";

                    //The value of _ignoreWaypoint[i] defines whether _gpxWaypoints[i] complies with the data quality rules specified by the user.
                    //For example if the GPS satellite lost it's Fix, then data should be discarded.
                    _ignoreWaypoint = new bool[_dataPoints];

                    //For every access point check the measurements against the rules and set _ignoreWaypoint accordingly.
                    for (int i = 0; i < _apList.Length; i++)
                    {
                        //An access points measurements are in blocks in the _gpxWaypoints array.
                        //Start and stop specifies where the block starts and stops inculsive.
                        int start = _apStart[i];
                        int stop = _apStop[i];

                        //For every measurement for this access point.
                        for (int j = start; j <= stop; j++)
                        {
                            //Assume the measurement is ok (should not be ignored) until some criteria says otherwise.
                            _ignoreWaypoint[j] = false;

                            //The user wants to ignore values if the GPS device seems to have locked up
                            if (dqGPSLockedUpCheckBox.Checked)
                            {
                                //Compare this measurement to all the other measurements for this access point.
                                for (int k = j + 1; k <= stop; k++)
                                {
                                    //If two measurements for the same access point were taken at EXACTLY the same Time then something isn't right.
                                    if (_gpxWaypoints[k].Time.Equals(_gpxWaypoints[j].Time))
                                    {
                                        _ignoreWaypoint[j] = true;
                                        break;
                                    }
                                }
                            }
                            //The user only wants data if the GPS had a Fix
                            if (dqSatFixLostCheckBox.Checked)
                            {
                                //The GPS needs a 2D lock at the very least. Usually 3 satellites.
                                if (_gpxWaypoints[j].Fix != "2d" && _gpxWaypoints[j].Fix != "3d")
                                {
                                    _ignoreWaypoint[j] = true;
                                }
                            }
                            //The user only wants to use data if at least x number of satellites were uses to calculate the position.
                            if (dqSatCountCheckBox.Checked)
                            {
                                if (_gpxWaypoints[j].NumSatellites < dqSatCountUpDown.Value)
                                {
                                    _ignoreWaypoint[j] = true;
                                }
                            }
                            //The user wants to ignore measurements taken when they were travelling too fast.
                            //Perhaps they're guilty about exceeding the speed limit, or perhaps the signal strength measurement losses accuracy.2
                            if (dqSpeedCheckBox.Checked)
                            {
                                //I'm not sure why this uses a try/catch. the Cmt field should always be present.
                                //However if for some reason it isn't, we're covered.
                                try
                                {
                                    //the speed of travel in km/h
                                    double tempSpeed = Convert.ToDouble(_gpxWaypoints[j].Cmt);
                                    //If too fast, ignore the point
                                    if (tempSpeed > Convert.ToDouble(dqSpeedUpDown.Value))
                                    {
                                        _ignoreWaypoint[j] = true;
                                    }
                                }
                                catch (Exception)
                                {
                                    //If no speed data, or speed is not recognised, ignore the point.
                                    _ignoreWaypoint[j] = true;
                                }

                            }
                            //If the user wants to ignore suspiciously high signal strengths.
                            //Very occasionally and transiently the signal strength of an Ap might be report as incorrectly high
                            if (dqRSSICheckBox.Checked)
                            {
                                if (_gpxWaypoints[j].Extension.Rssi > dqRSSIUpDown.Value)
                                {
                                    _ignoreWaypoint[j] = true;
                                }
                            }
                        }
                    }

                    //Find the strongest point for each Ap again, this Time only using points which meet the data quality criteria set by the user.
                    //A similar routine is done when we load the files, however now we know how many APs there are and arrays are used.
                    //It is possible that all of an APs datapoints could be removed which could be confusing for the user, but not incorrect.
                    int[] apBestSignalIdx = new int[_apList.Length];
                    int[] apBestSignal = new int[_apList.Length];
                    //Specifies whether an access point has any valid waypoints left. If it doesn't, it should be ignored.
                    _ignoreAp = new bool[_apList.Length];

                    //Initialise the 2D array of arraylists which will contain all the access points for a given channel and encryption
                    for (int i = 0; i < MaxChannels; i++)
                    {
                        for (int j = 0; j < Encryption.Length; j++)
                        {
                            _channelAndEncryptionIdxs[i, j] = new ArrayList();
                        }
                    }

                    //For every access point
                    for (int i = 0; i < _apList.Length; i++)
                    {
                        //Start off with a signal level that is impossibly low.
                        //If all an access points measurements are removed.
                        apBestSignal[i] = int.MinValue;
                        //If this access point has even a single valid measurement, this should change to a positive number.
                        apBestSignalIdx[i] = -1;
                        int start = _apStart[i];
                        int stop = _apStop[i];

                        GpxWaypoint tempGpXpoint = _apList[i];

                        //for every measurement taken of this access point...
                        for (int j = start; j <= stop; j++)
                        {
                            //that's consistent with the data quality criteria selected by the user
                            if (!_ignoreWaypoint[j])
                            {
                                //Check if this is the strongest signal measurement so far
                                if (_gpxWaypoints[j].Extension.Rssi > apBestSignal[i])
                                {
                                    //if it's stronger than any points so far, save this point.
                                    apBestSignal[i] = _gpxWaypoints[j].Extension.Rssi;
                                    apBestSignalIdx[i] = j;
                                }
                            }
                        }

                        int idx = apBestSignalIdx[i];

                        if (idx > -1)
                        {
                            // Use the details of the strongest signal point as the information for the access point
                            tempGpXpoint.Latitude = _gpxWaypoints[idx].Latitude;
                            tempGpXpoint.Longitude = _gpxWaypoints[idx].Longitude;
                            tempGpXpoint.Elevation = _gpxWaypoints[idx].Elevation;

                            tempGpXpoint.Fix = _gpxWaypoints[idx].Fix;
                            tempGpXpoint.Cmt = _gpxWaypoints[idx].Cmt;
                            tempGpXpoint.HorizontalDilution = _gpxWaypoints[idx].HorizontalDilution;
                            tempGpXpoint.PositionDilution = _gpxWaypoints[idx].PositionDilution;
                            tempGpXpoint.NumSatellites = _gpxWaypoints[idx].NumSatellites;
                            tempGpXpoint.Time = _gpxWaypoints[idx].Time;
                            tempGpXpoint.VerticalDilution = _gpxWaypoints[idx].VerticalDilution;

                            tempGpXpoint.Extension.Rssi = _gpxWaypoints[idx].Extension.Rssi;
                            tempGpXpoint.Extension.SignalQuality = _gpxWaypoints[idx].Extension.SignalQuality;
                            tempGpXpoint.Extension.Privacy = _gpxWaypoints[idx].Extension.Privacy;
                            tempGpXpoint.Extension.ChannelId = _gpxWaypoints[idx].Extension.ChannelId;
                            tempGpXpoint.Extension.NetworkType = _gpxWaypoints[idx].Extension.NetworkType;
                            tempGpXpoint.Extension.Rates = _gpxWaypoints[idx].Extension.Rates;

                            _apList[i] = tempGpXpoint;

                            //This access point was valid so add it to the list of access points for its channel and encryption type.
                            uint channelIdx = _apList[i].Extension.ChannelId - 1;
                            int encryptionIdx = (int)GetEncryptionIdx(_apList[i].Extension.Privacy);

                            // Only add 2.4x GHz channels for now
                            if (channelIdx < MaxChannels)
                            {
                                _channelAndEncryptionIdxs[channelIdx, encryptionIdx].Add(i);
                            }

                            //This access point has at least one measurement left
                            _ignoreAp[i] = false;
                        }
                        else
                        {
                            //This access point has absolutely not points left
                            _ignoreAp[i] = true;
                        }
                    }

                    //If the user wants a file which contains all information in a single document.
                    if (filesComprehensiveFileCheckBox.Checked)
                    {
                        ExportComprehensiveKmlFile();
                    }

                    if (filesIndividualCheckBox.Checked)
                    {
                        //create the sub-folder to which all these files will be written
                        Directory.CreateDirectory(_apFolder);

                        for (int i = 0; i < _apList.Length; i++)
                        {
                            ExportAccessPointKmlFile(i);
                        }
                    }

                    /*If the user has selected that they want a summary KML file.
                      This was one of the first methods written and has not been updated. It is written quite inefficiently.
                      However the total Time is still pretty quick so it hasn't been changed.
                      A faster approach would be to use a streamwriter to output the file on-the-fly as raw text.
                      This method hails from a Time before there was a _channelAndEncryptionIdxs array which enables all nodes to be output sequentially
                      rather than the extra overhead of treating the file as an XML document and appending nodes to the appropriate places.
                     */
                    if (filesSummaryCheckBox.Checked)
                    {
                        ExportSummaryKmlFile();
                    }

                    SaveSettings();

                    MessageBox.Show("Export Completed", "KML Export", MessageBoxButtons.OK, MessageBoxIcon.Information);
#if !DEBUG
                }
                catch (ArgumentException exp)
                {
                    MessageBox.Show("Output folder could not be created.\nYour path appears to contain illegal characters.");
                    System.Diagnostics.Debug.WriteLine(exp.Message);
                }
                catch (DirectoryNotFoundException exp)
                {
                    MessageBox.Show("Output folder could not be created.\nThe directory could not be found.");
                    System.Diagnostics.Debug.WriteLine(exp.Message);
                }
#endif
            }
            else
            {
                if (_dataPoints == 0)
                {
                    MessageBox.Show("There is no data to output.");
                }
                if (exportFolderTextBox.Text.Length==0)
                {
                    MessageBox.Show("You haven't entered a path for the output files.");
                }
            }
        }

        /// <summary>
        /// Creates a comprehensive KML file containing ALL waypoints for ALL access points
        /// </summary>
        private void ExportComprehensiveKmlFile()
        {
            //This file will be written on-the-fly to the output file.
            StreamWriter sw = new StreamWriter(_exportFolder + "Comprehensive.kml");

            sw.WriteLine(@"<?xml version=""1.0"" ?>");
            sw.WriteLine("<kml>");
            sw.WriteLine("<Document>");
            sw.WriteLine("<name>Comprehensive Data</name>");

            //Depending on the whether the user specified to organise the folders by channel then encryption
            //or the other way round will define which is the inner and outer of the for-loops.

            //Assuming the user wants encryption folders with channel sub-folders
            int outer = Encryption.Length;
            int inner = MaxChannels;
            bool isChannelFirst = false;

            //The user has specified the other way round, channel folders with encryption sub-folders.
            if (_filesSummaryComboBoxSelectedIndex == 1)
            {
                outer = MaxChannels;
                inner = Encryption.Length;
                isChannelFirst = true;
            }

            //Specifies whether the higher-level (outer) folder has been created yet.
            //Only the folders which actually contain data will be created. 
            //So when we find the first sub-folder with data we'll have to create its parent folder first.

            //for every potential folder
            for (int i = 0; i < outer; i++)
            {
                //This outer folder has not been created yet.
                bool outerFolderCreated = false;

                //for every potential sub-folder
                for (int j = 0; j < inner; j++)
                {
                    //The list of indicies of the _apList array for this encryption and channel (i.e. this sub-folder)
                    //The user specified hierarchy will specify whether i corresponds to encryption or channel and vice-versa for j
                    ArrayList tempList = isChannelFirst ? _channelAndEncryptionIdxs[i, j] : _channelAndEncryptionIdxs[j, i];

                    //If there is at least one measurement for this encryption and channel (i.e. this sub-folder)
                    if (tempList.Count > 0)
                    {
                        //If the parent folder has not been created yet, do it now.
                        if (!outerFolderCreated)
                        {
                            sw.WriteLine("<Folder>");
                            //The user-specified hierarchy will define whether the parent folder is for encryption or channel
                            if (!isChannelFirst)
                            {
                                sw.WriteLine("<name>" + Encryption[i] + "</name>");
                            }
                            else
                            {
                                sw.WriteLine("<name> Channel " + (i + 1) + "</name>");
                            }
                            //remember that the parent folder has already been made.
                            outerFolderCreated = true;
                        }

                        //now create the sub-folder (inner)
                        sw.WriteLine("<Folder>");

                        //and give it the appropriate Name
                        if (isChannelFirst)
                        {
                            sw.WriteLine("<name>" + Encryption[j] + "</name>");
                        }
                        else
                        {
                            sw.WriteLine("<name> Channel " + (j + 1) + "</name>");
                        }

                        //for every access point that uses this channel and encryption type
                        for (int k = 0; k < tempList.Count; k++)
                        {
                            //select the access point
                            int I = (int)tempList[k];
                            GpxWaypoint currentAp = _apList[I];

                            //use its Name as the Name of the folder. Name is {Ssid} [{Mac ADDRESS}]
                            string tempName = (currentAp.Name);
                            sw.WriteLine("<Folder>");
                            sw.WriteLine("<name>" + XmlCleanUp.CleanUp(tempName) + "</name>");

                            //use the standard shaded dot marker.
                            const string iconLink = "http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png";
                            //Scale the marker depending on the signal strength
                            double iconScale = GetIconScale(currentAp.Extension.Rssi);
                            //run method which which writes a GpxWaypoint to a <Placemark> element.
                            //The strongest point will be in the root of the Ap's folder with all the individual datapoints in sub-folder below
                            WritePlacemark(currentAp, sw, iconLink, iconScale, currentAp.Extension.Rssi.ToString());

                            //output all the individual measurements to a sub-folder of the Ap's folder
                            ExportAccessPointToKml(I, sw);


                            //Close Access Point Folder
                            sw.WriteLine("</Folder>");
                        }
                        //Close inner folder (encryption or channel)
                        sw.WriteLine("</Folder>");
                    }
                    //Close outer folder (encryption or channel)
                    if (j == (inner - 1) && outerFolderCreated)
                    {
                        sw.WriteLine("</Folder>");
                    }
                }
            }

            //Close the KML document and the streamwriter.
            sw.WriteLine("</Document>");
            sw.WriteLine("</kml>");

            sw.Close();
        }

        private void ExportSummaryKmlFile()
        {                        //This document will contain markers at the strongest point for each access point organised by channel and encryption.
            XmlDocument summaryDocument = new XmlDocument();
            //Using LoadXml is relatively slow, but it's not really a problem as there's only one summary document to write.
            summaryDocument.LoadXml(@"<?xml version=""1.0"" encoding=""UTF-8""?><kml xmlns=""http://www.opengis.net/kml/2.2""><Document><Name>Summary</Name></Document></kml>");

            //Whether encryption types are folders within channel folders or the other way round depends on what the user chose.
            //Start by assuming that channels are sub-folders of encryption folders.
            int outer = Encryption.Length;
            int inner = MaxChannels;
            bool isChannelFirst = false;

            //User wants the other way round, encryption is a sub-folder of channel
            if (FilesComprehensiveFileComboBox.SelectedIndex == 1)
            {
                outer = MaxChannels;
                inner = Encryption.Length;
                isChannelFirst = true;
            }

            //FIRST CREATE THE FOLDER STRUCTURE OF THE KML FILE
            //Outer folder
            for (int i = 0; i < outer; i++)
            {
                //Don't create a folder for it, if there's definitely no data.
                //This will still make a folder if the data was completely removed due to data quality settings.
                if ((isChannelFirst && _channelCounts[i] > 0) || (!isChannelFirst && _encryptionCounts[i] > 0))
                {
                    //Create a folder node and give it a Name depending on what the user chose as the outer folder
                    XmlElement folder = summaryDocument.CreateElement("Folder");
                    XmlElement nameElement = summaryDocument.CreateElement("name");
                    if (isChannelFirst)
                    {
                        nameElement.InnerText = "Channel " + (i + 1) + "";
                    }
                    else
                    {
                        nameElement.InnerText = Encryption[i];
                    }
                    //This folder will be closed (not expanded) by default in google earth.
                    XmlElement openElement = summaryDocument.CreateElement("open");
                    openElement.InnerText = "0";
                    //The visibility of this folder will be as per the users selection
                    XmlElement visibilityElement = summaryDocument.CreateElement("visibility");
                    //visibilityElement.InnerText = System.Convert.ToInt32(visVisibleCheckBox.Checked) + "";
                    visibilityElement.InnerText = "1";  // markers are always visible by default

                    //Append the Name, open and visibility elements to the Folder.
                    folder.AppendChild(nameElement);
                    folder.AppendChild(openElement);
                    folder.AppendChild(visibilityElement);

                    //Now start work on the inner folder
                    for (int j = 0; j < inner; j++)
                    {
                        //Don't create a folder for it, if there's definitely no data.
                        //This will still make a folder if the data was completely removed due to data quality settings.
                        if ((!isChannelFirst && _channelCounts[j] > 0) || (isChannelFirst && _encryptionCounts[j] > 0))
                        {
                            //Create the folder element and give it the appropriate Name.
                            XmlElement subFolder = summaryDocument.CreateElement("Folder");
                            XmlElement subNameElement = summaryDocument.CreateElement("name");
                            if (isChannelFirst)
                            {
                                subNameElement.InnerText = Encryption[j];
                                //An id attribute is added to make it easier/quicker to find
                                subFolder.SetAttribute("id", (i + 1) + "," + j);
                            }
                            else
                            {
                                subNameElement.InnerText = "Channel " + (j + 1) + "";
                                //An id attribute is added to make it easier/quicker to find
                                subFolder.SetAttribute("id", (j + 1) + "," + i);
                            }
                            //This folder will be closed (not expanded) by default in google earth.
                            XmlElement subOpenElement = summaryDocument.CreateElement("open");
                            subOpenElement.InnerText = "0";
                            //The visibility of this folder will be as per the users selection
                            XmlElement subVisibilityElement = summaryDocument.CreateElement("visibility");
                            //Sub_VisibilityElement.InnerText = System.Convert.ToInt32(visVisibleCheckBox.Checked) + "";
                            subVisibilityElement.InnerText = "1";  // markers are always visible by default

                            //Append the Name, open and visibility elements to the Folder.
                            subFolder.AppendChild(subNameElement);
                            subFolder.AppendChild(subOpenElement);
                            subFolder.AppendChild(subVisibilityElement);
                            //Append the sub-folder to its parent folder
                            folder.AppendChild(subFolder);
                        }
                    }
                    //Append the Folder to the document.
                    summaryDocument.GetElementsByTagName("Document").Item(0).AppendChild(folder);
                }
            }

            //NOW ADD THE RESPECTIVE ACCESS POINTS TO THE APPROPRIATE FOLDERS

            //All markers will be the default shaded-dot
            const string iconLink = "http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png";

            //for every access point
            for (int i = 0; i < _apList.Length; i++)
            {
                //every access point with at least one measurement
                if (!_ignoreAp[i])
                {
                    //Select the current access point
                    GpxWaypoint currentAp = _apList[i];
                    //get the color which matches it's encryption type
                    string defaultColor = GetEncryptionColor(currentAp.Extension.Privacy);
                    //use this color for the icon and the label
                    string iconColor = defaultColor;
                    string labelColor = defaultColor;
                    string labelScale = _showLabels ? "1" : "0";

                    //Convert the signal strength to a marker size
                    string iconScale = GetIconScale(currentAp.Extension.Rssi) + "";

                    //The main element is the placemark
                    XmlElement placemark = summaryDocument.CreateElement("Placemark");

                    //The visibility of this placemark will be as per the user specified in the appropriate checkbox
                    XmlElement visElement = summaryDocument.CreateElement("visibility");
                    //visElement.InnerText = System.Convert.ToInt32(visVisibleCheckBox.Checked) + "";
                    visElement.InnerText = "1";

                    //The style element is lazily made setting the text in the innerXml field.
                    //The style element defines things like the icon and labels sizes, colors and icon image.
                    XmlElement styleElement = summaryDocument.CreateElement("Style");
                    {
                        styleElement.InnerXml = "<IconStyle><Icon><href>" + iconLink + "</href></Icon><color>" + iconColor + "</color><scale>" + iconScale + "</scale></IconStyle><LabelStyle><color>" + labelColor + "</color><scale>" + labelScale + "</scale></LabelStyle>";
                        styleElement.SetAttribute("id", "sn_shaded_dot");
                    }
                    //The element which defines the Name of the placemark is the Name of the Ap {Ssid} [{Mac ADDRESS}]
                    XmlElement nameElement = summaryDocument.CreateElement("name");
                    nameElement.InnerText = (currentAp.Name);
                    //The Description of the placemark as generated previously in the LogViewer
                    XmlElement descElement = summaryDocument.CreateElement("description");
                    descElement.InnerText = (currentAp.Description);
                    //This element defines the GPS coordinates, altitude and method in which altitude is shown.
                    XmlElement pointElement = summaryDocument.CreateElement("Point");

                    //element with the GPS information
                    XmlElement coordElement = summaryDocument.CreateElement("coordinates");

                    //In the summary document, the Elevation is always shown as the real Elevation rather than a representation of the signal strength for example.
                    coordElement.InnerText = currentAp.Longitude + "," + currentAp.Latitude + "," + currentAp.Elevation;
                    //Add the GPS coordinates to the point element
                    pointElement.AppendChild(coordElement);

                    //Add all the elements to the placemark
                    placemark.AppendChild(visElement);
                    placemark.AppendChild(styleElement);
                    placemark.AppendChild(nameElement);
                    placemark.AppendChild(descElement);
                    placemark.AppendChild(pointElement);

                    int encryptionType = (int)GetEncryptionIdx(currentAp.Extension.Privacy);
                    //the xpath to the appropriate channel/encryption folder.
                    string xpath = "//Folder[@id='" + currentAp.Extension.ChannelId + "," + encryptionType + "']";

                    //Append the placemark to the current folder
                    XmlNode node = summaryDocument.SelectSingleNode(xpath);

                    if (null != node)
                    {
                        node.AppendChild(placemark);
                    }
                    else
                    {
                        Console.WriteLine("ERROR: Node not found: " + xpath);
                    }
                }
            }
            //Save the completed file.
            summaryDocument.Save(_exportFolder + "Summary.kml");
        }

        /// <summary>
        /// Outputs the data of access point _apList[e] to file.
        /// </summary>
        /// <param name="i">index of _apList to be processed.</param> 
        private void ExportAccessPointKmlFile(int i)
        {
            //This will be access point being processed. For now we just need to work out the filename to use.
            GpxWaypoint currentAp = _apList[i];

            // Make the Ap Name a legal file Name
            char[] badChars = Path.GetInvalidFileNameChars();
            String tempName = String.Join ("", currentAp.Name.Split(badChars,StringSplitOptions.RemoveEmptyEntries));
            
            // if the Ap Name will result in a fully qualified filename that is too long...
            if (tempName.Length > 255 - _apFolder.Length)
            {
                tempName = tempName.Remove( 255 - _apFolder.Length );

                int count = 0;
                while (File.Exists(_apFolder + tempName + ".kml"))
                {
                    string countString = count.ToString();
                    tempName = tempName.Remove(tempName.Length - countString.Length) + countString;
                    count++;
                }
            }

            //Our output file will be at this location.
            string filename2 = _apFolder + tempName + ".kml";

            //The file is output on-the-fly by the streamwriter to the file at filename2
            StreamWriter sw = new StreamWriter(filename2);

            //Open the root nodes of the document
            sw.WriteLine(@"<?xml version=""1.0"" ?>");
            sw.WriteLine("<kml>");
            sw.WriteLine("<Document>");

            //given the index of the access point and a streamwriter to use, this will write all the placemarks and sub-folders for the access point as per the options selected by the user.
            ExportAccessPointToKml(i, sw);

            //Close the root nodes of the document and the streamwriter itself.
            sw.WriteLine("</Document>");
            sw.WriteLine("</kml>");
            sw.Close();
            //Every file processed is one less thread that the ThreadPool has to worry about.
            if (Interlocked.Decrement(ref _numBusy) == 0)
            {
                //When no files remain, the thread can stop waiting and will terminate.
                _resetEvents[0].Set();
            }
        }

        /// <summary>
        /// Writes the data for a given access point at the specified index to the specified streamwriter.
        /// </summary>
        /// <param name="i">index of _apList to be processed.</param>
        /// <param name="sw">streamwriter used to write the data.</param> 
        private void ExportAccessPointToKml(int i, TextWriter sw)
        {
            //This is the access point to be processed.
            GpxWaypoint currentAp = _apList[i];

            //the measurements for an access point occupy contigious blocks in the _gpxWaypoints array.
            int startIndex = _apStart[i];
            int stopIndex = _apStop[i];

            //create the folder
            sw.WriteLine("<Folder>");
            //and give it a Name depending on which folder we are currently processing.
            sw.WriteLine("<name>Rssi</name>");

            //for every measurement belonging to this access point.
            for (int j = startIndex; j <= stopIndex; j++)
            {
                GpxWaypoint tempAp = _gpxWaypoints[j];
                /*If this is a measurement that meets the user specified data quality criteria
                 * and double checks that this waypoint/measurement does in fact belong to the right Ap.
                 * That check is redundant but was useful whilst debugging.
                 */
                if ((currentAp.Extension.Mac == tempAp.Extension.Mac) && (_ignoreWaypoint[j] == false))
                {
                    //The strength of the current measurement in dBm.
                    int tempStrength = _gpxWaypoints[j].Extension.Rssi;

                    //Marker Size is related to signal strength, unless signal strength is already related to Elevation
                    double iconScale = GetIconScale(tempStrength);

                    string iconLink, name;

                    //If this point is the strongest or equal strongest, use the paddle icon instead of the shaded dot.
                    if (tempStrength == currentAp.Extension.Rssi)
                    {
                        iconLink = "http://maps.google.com/mapfiles/kml/paddle/wht-blank.png";
                        name = _gpxWaypoints[j].Name + ": " + tempStrength;
                    }
                    else
                    {
                        iconLink = "http://maps.google.com/mapfiles/kml/shapes/shaded_dot.png";
                        name = tempStrength.ToString();
                    }

                    //use the options generated so far and the streamwriter to write the tempAP GpxWaypoint.
                    WritePlacemark(tempAp, sw, iconLink, iconScale, name);
                }
            }

            //Completed all measurements for this access point so close the folder.
            sw.WriteLine("</Folder>");
        }


        /// <summary>
        /// Writes the placemark element for a specified waypoint using the specified streamwriter.
        /// </summary>
        /// <param name="ap">Waypoint to be processed</param>
        /// <param name="sw">streamwriter used to write the data.</param>
        /// <param name="iconLink">Link to the marker to be used.</param> 
        /// <param name="iconScale">Size of the marker.</param> 
        /// <param name="placemarkName"></param>
        private void WritePlacemark(GpxWaypoint ap, TextWriter sw, string iconLink, double iconScale, string placemarkName)
        {
            //The color which corresponds to this points encryption type
            string defaultColor = GetEncryptionColor(ap.Extension.Privacy);
            string iconColor = defaultColor;
            string labelColor = defaultColor;
            
            //If the user wants labels, give them labels.
            string labelScale = _showLabels ? "1" : "0";

            //The size of the icon is passed into this method and converted to a string here.
            string scale = iconScale.ToString();

            //Open the Placemark element
            sw.WriteLine("<Placemark>");
            {
                //define the default visiblity of this placemark
                sw.WriteLine("<visibility>1</visibility>");
                //write the style element as per the specifications input by the user.
                sw.WriteLine(@"<Style id=""sn_shaded_dot"">");
                {
                    sw.WriteLine("<IconStyle>");
                    {
                        sw.WriteLine("<Icon>");
                        {
                            sw.WriteLine("<href>" + iconLink + "</href>");
                        }
                        sw.WriteLine("</Icon>");
                        sw.WriteLine("<color>" + iconColor + "</color>");
                        sw.WriteLine("<scale>" + scale + "</scale>");
                    }
                    sw.WriteLine("</IconStyle>");
                    sw.WriteLine("<LabelStyle>");
                    {
                        sw.WriteLine("<color>" + labelColor + "</color>");
                        sw.WriteLine("<scale>" + labelScale + "</scale>");
                    }
                    sw.WriteLine("</LabelStyle>");
                }
                sw.WriteLine("</Style>");

                //The Name of the placemark is it's signal strength in dBm.
                sw.WriteLine("<name>" + placemarkName + "</name>");

                //This is the Description of the waypoint as created by the LogViewer previously.
                sw.WriteLine("<description>" + ap.Description + "</description>");
                //The Point element contains the GPS information.
                sw.WriteLine("<Point>");
                // The coordinates: Latitude, Longitude, and Elevation
                sw.WriteLine("<coordinates>" + ap.Longitude + "," + ap.Latitude + "," + ap.Elevation + "</coordinates>");

                //Close the Point tag.
                sw.WriteLine("</Point>");
            }
            //Close the Placemark tag.
            sw.WriteLine("</Placemark>");
        }

        /// <summary>
        /// Converts a string Description of the encryption type to an index representing that encryption type.
        /// </summary>
        /// <param name="desc">Encryption type Description.</param> 
        private EncryptionType GetEncryptionIdx(string desc)
        {
            if (null == desc)
                return EncryptionType.Unknown;
            if (desc.Contains("No") || desc.Contains("no"))
                return EncryptionType.Unknown;
            if (desc.Contains("WEP"))
                return EncryptionType.Wep;
            if (desc.Contains("WPA-TKIP"))
                return EncryptionType.WpaTkip;
            if (desc.Contains("WPA2") || desc.Contains("RSNA"))
                return EncryptionType.Wpa2Rsna;
            if (desc.Contains("WPA"))
                return EncryptionType.Wpa;
            //If its made it this far I don't know what it is?
            return EncryptionType.Unknown;
        }

        /// <summary>
        /// Converts a string Description of the encryption type to a color code string for the KML file.
        /// </summary>
        /// <param name="input">Encryption type Description.</param> 
        private string GetEncryptionColor(string input)
        {
            EncryptionType encryptionType = GetEncryptionIdx(input);

            if (encryptionType == EncryptionType.Unknown)
            {
                //GREEN
                return "ff00ff00";
            }

            if (encryptionType == EncryptionType.Wep)
            {
                //YELLOW
                return "ff00ffff";
            }
            //WPA-TKIP sometimes means the Ap is operating in mixed WPA-WPA2 mode. The user can define whether it wants to use the WPA or WPA2 coloring.
            if (encryptionType == EncryptionType.Wpa || encryptionType == EncryptionType.WpaTkip)
            {
                //ORANGE
                return "ff00aaff";
            }

            //WPA-TKIP sometimes means the Ap is operating in mixed WPA-WPA2 mode. The user can define whether it wants to use the WPA or WPA2 coloring.
            if (encryptionType == EncryptionType.Wpa2Rsna)
            {   //RED
                return "ff0000ff";
            }
            //If its made it this far I don't know what it is?
            return "ffffffff";
        }

        /// <summary>
        /// Converts an Rssi dBm value to a marker scale size
        /// </summary>
        /// <param name="rssi">The signal strength in dBm</param> 
        private double GetIconScale(int rssi)
        {
            return ((100 + rssi) / 15.0);
        }

        #endregion

        #region Event Handlers

        /// <summary>
        /// Loads the last-used settings
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void KmlExporterForm_Load(object sender, EventArgs e)
        {
            //File options
            // input GPX file
            inputFileTextBox.Text = Settings.Default.export_inputFile;
            //The output folder for the processed files
            exportFolderTextBox.Text = Settings.Default.export_filesFolder;
            //Does the user want to combine all the valid GPX waypoints to a single GPX file?
            //filesCombineCheckBox.Checked = Settings.Default.export_FilesCombine;
            //Does the user want a summary KML which contains only the strongest points for each Ap
            filesSummaryCheckBox.Checked = Settings.Default.export_FilesSummary;
            //Does the user want individual KML files for every Ap containing all the valid measurements for that Ap.
            filesIndividualCheckBox.Checked = Settings.Default.export_FilesIndividualAPs;
            //Does the user want to output all the data to a single KML file
            filesComprehensiveFileCheckBox.Checked = Settings.Default.export_FilesComprehensive;
            //Defines the hierachy or the KML files. Channel then encryption or the otherway round.
            FilesComprehensiveFileComboBox.SelectedIndex = Settings.Default.export_FilesOrganiseByIdx;

            //Visibility Options
            //How should altitude be output in the KML files? As signal strength, height, speed etc.?
            //visAltComboBox.SelectedIndex = Settings.Default.export_VisAltitudeIdx;
            //Should the markers be visible by default? The main marker for each Ap in the comprehensive KML file will always be visible.
            //visVisibleCheckBox.Checked = Settings.Default.export_VisMarkerVisibility;
            //Should there be a marker that joins the marker to it's position on the ground.
            //Again, the main marker always has this in the comprehensive KML file.
            //visExtrudeCheckBox.Checked = Settings.Default.export_VisExtendedToGround;
            //Should markers have their labels visible?
            visLabelsCheckBox.Checked = Settings.Default.export_VisShowLabels;
            //Should WPA-TKIP points be colored red or orange?
            //visWPAColorCheckBox.Checked = Settings.Default.export_VisWPATKIPColorRed;
            //Should the KML file contain the timestamp node. The Time will be visible in the Description, however inclusion
            //of the timestamp field will allow animation of the data or to compare values taken on two different occasions.
            //It can also make viewing the data in google earth confusing.
            //visTimestampCheckBox.Checked = Settings.Default.export_VisIncludeTimestamp;

            //Data Quality Options
            //Should points be discarded if the GPS has locked up? i.e. if an access point has more than one measurement for exactly the same Time.
            dqGPSLockedUpCheckBox.Checked = Settings.Default.export_DQLockedUp;
            //Should points be discarded if there was no valid GPS Fix when the data was recorded?
            dqSatFixLostCheckBox.Checked = Settings.Default.export_DQFixLost;
            //Should points be discarded if the GPS Fix was using less than or equal to this number of satellites
            dqSatCountCheckBox.Checked = Settings.Default.export_DQSatLessThan;
            dqSatCountUpDown.Value = Settings.Default.export_DQSatLessThanCount;
            //Should points be discarded if you were travelling too fast.
            dqSpeedCheckBox.Checked = Settings.Default.export_DQTooFast;
            dqSpeedUpDown.Value = Settings.Default.export_DQTooFastSpeed;
            //Should points be discarded if they appear to be too strong?
            //Sometimes a very strong value will be read very briefly which is not correct.
            dqRSSICheckBox.Checked = Settings.Default.export_DQTooStrong;
            dqRSSIUpDown.Value = Settings.Default.export_DQTooStrongStrength;
        }

        private void FilesComprehensiveFileCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateComprehensiveFileOptions();

            UpdateExportButtonStatus();
        }

        /// <summary>
        /// Launches a FileDialog to select the input file(s).
        /// </summary>
        private void SelectInputFileButton_Click(object sender, EventArgs e)
        {
            //Launch the dialog box to prompt for files.
            if (selectInputFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Display the input file Name
                inputFileTextBox.Text = selectInputFileDialog.FileName;
            }

            UpdateExportButtonStatus();
        }

        /// <summary>
        /// Launches a FolderBrowserDialog to select the output folder.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectExportFolderButton_Click(object sender, EventArgs e)
        {
            if (selectExportFolderDialog.ShowDialog() == DialogResult.OK)
            {
                exportFolderTextBox.Text = selectExportFolderDialog.SelectedPath;
            }

            UpdateExportButtonStatus();
        }

        private void ExportButton_Click(object sender, EventArgs e)
        {
            _fileNames[0] = inputFileTextBox.Text;
            _exportFolder = exportFolderTextBox.Text;

            LoadInputFiles();
            ExportKml();
        }


        #endregion

        private void FilesSummaryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateExportButtonStatus();
        }

        private void FilesIndividualCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            UpdateExportButtonStatus();
        }
    }
}
