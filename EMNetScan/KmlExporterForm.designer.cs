﻿namespace Inssider
{
    partial class KmlExporterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param Name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( KmlExporterForm ) );
           this.exportButton = new System.Windows.Forms.Button();
           this.selectInputFileDialog = new System.Windows.Forms.OpenFileDialog();
           this.groupBox3 = new System.Windows.Forms.GroupBox();
           this.selectExportFolderButton = new System.Windows.Forms.Button();
           this.selectInputFileButton = new System.Windows.Forms.Button();
           this.inputFileTextBox = new System.Windows.Forms.TextBox();
           this.label1 = new System.Windows.Forms.Label();
           this.filesComprehensiveFileCheckBox = new System.Windows.Forms.CheckBox();
           this.FilesComprehensiveFileComboBox = new System.Windows.Forms.ComboBox();
           this.organizeByLabel = new System.Windows.Forms.Label();
           this.filesIndividualCheckBox = new System.Windows.Forms.CheckBox();
           this.filesSummaryCheckBox = new System.Windows.Forms.CheckBox();
           this.exportFolderTextBox = new System.Windows.Forms.TextBox();
           this.label20 = new System.Windows.Forms.Label();
           this.groupBox2 = new System.Windows.Forms.GroupBox();
           this.visLabelsCheckBox = new System.Windows.Forms.CheckBox();
           this.groupBox1 = new System.Windows.Forms.GroupBox();
           this.label23 = new System.Windows.Forms.Label();
           this.dqRSSIUpDown = new System.Windows.Forms.NumericUpDown();
           this.dqRSSICheckBox = new System.Windows.Forms.CheckBox();
           this.label22 = new System.Windows.Forms.Label();
           this.dqSatCountCheckBox = new System.Windows.Forms.CheckBox();
           this.dqGPSLockedUpCheckBox = new System.Windows.Forms.CheckBox();
           this.label25 = new System.Windows.Forms.Label();
           this.dqSatFixLostCheckBox = new System.Windows.Forms.CheckBox();
           this.dqSpeedCheckBox = new System.Windows.Forms.CheckBox();
           this.dqSatCountUpDown = new System.Windows.Forms.NumericUpDown();
           this.dqSpeedUpDown = new System.Windows.Forms.NumericUpDown();
           this.label24 = new System.Windows.Forms.Label();
           this.cancelButton = new System.Windows.Forms.Button();
           this.selectExportFolderDialog = new System.Windows.Forms.FolderBrowserDialog();
           this.groupBox3.SuspendLayout();
           this.groupBox2.SuspendLayout();
           this.groupBox1.SuspendLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.dqRSSIUpDown ) ).BeginInit();
           ( (System.ComponentModel.ISupportInitialize)( this.dqSatCountUpDown ) ).BeginInit();
           ( (System.ComponentModel.ISupportInitialize)( this.dqSpeedUpDown ) ).BeginInit();
           this.SuspendLayout();
           // 
           // exportButton
           // 
           this.exportButton.DialogResult = System.Windows.Forms.DialogResult.OK;
           resources.ApplyResources( this.exportButton, "exportButton" );
           this.exportButton.ForeColor = System.Drawing.SystemColors.ControlText;
           this.exportButton.Name = "exportButton";
           this.exportButton.UseVisualStyleBackColor = true;
           this.exportButton.Click += new System.EventHandler( this.ExportButton_Click );
           // 
           // selectInputFileDialog
           // 
           resources.ApplyResources( this.selectInputFileDialog, "selectInputFileDialog" );
           // 
           // groupBox3
           // 
           this.groupBox3.Controls.Add( this.selectExportFolderButton );
           this.groupBox3.Controls.Add( this.selectInputFileButton );
           this.groupBox3.Controls.Add( this.inputFileTextBox );
           this.groupBox3.Controls.Add( this.label1 );
           this.groupBox3.Controls.Add( this.filesComprehensiveFileCheckBox );
           this.groupBox3.Controls.Add( this.FilesComprehensiveFileComboBox );
           this.groupBox3.Controls.Add( this.organizeByLabel );
           this.groupBox3.Controls.Add( this.filesIndividualCheckBox );
           this.groupBox3.Controls.Add( this.filesSummaryCheckBox );
           this.groupBox3.Controls.Add( this.exportFolderTextBox );
           this.groupBox3.Controls.Add( this.label20 );
           this.groupBox3.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.groupBox3, "groupBox3" );
           this.groupBox3.Name = "groupBox3";
           this.groupBox3.TabStop = false;
           // 
           // selectExportFolderButton
           // 
           resources.ApplyResources( this.selectExportFolderButton, "selectExportFolderButton" );
           this.selectExportFolderButton.Name = "selectExportFolderButton";
           this.selectExportFolderButton.UseVisualStyleBackColor = true;
           this.selectExportFolderButton.Click += new System.EventHandler( this.SelectExportFolderButton_Click );
           // 
           // selectInputFileButton
           // 
           resources.ApplyResources( this.selectInputFileButton, "selectInputFileButton" );
           this.selectInputFileButton.Name = "selectInputFileButton";
           this.selectInputFileButton.UseVisualStyleBackColor = true;
           this.selectInputFileButton.Click += new System.EventHandler( this.SelectInputFileButton_Click );
           // 
           // inputFileTextBox
           // 
           resources.ApplyResources( this.inputFileTextBox, "inputFileTextBox" );
           this.inputFileTextBox.Name = "inputFileTextBox";
           this.inputFileTextBox.ReadOnly = true;
           // 
           // label1
           // 
           resources.ApplyResources( this.label1, "label1" );
           this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label1.Name = "label1";
           // 
           // filesComprehensiveFileCheckBox
           // 
           resources.ApplyResources( this.filesComprehensiveFileCheckBox, "filesComprehensiveFileCheckBox" );
           this.filesComprehensiveFileCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.filesComprehensiveFileCheckBox.Name = "filesComprehensiveFileCheckBox";
           this.filesComprehensiveFileCheckBox.UseVisualStyleBackColor = true;
           this.filesComprehensiveFileCheckBox.CheckedChanged += new System.EventHandler( this.FilesComprehensiveFileCheckBox_CheckedChanged );
           // 
           // FilesComprehensiveFileComboBox
           // 
           this.FilesComprehensiveFileComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.FilesComprehensiveFileComboBox.FormattingEnabled = true;
           this.FilesComprehensiveFileComboBox.Items.AddRange( new object[] {
            resources.GetString("FilesComprehensiveFileComboBox.Items"),
            resources.GetString("FilesComprehensiveFileComboBox.Items1")} );
           resources.ApplyResources( this.FilesComprehensiveFileComboBox, "FilesComprehensiveFileComboBox" );
           this.FilesComprehensiveFileComboBox.Name = "FilesComprehensiveFileComboBox";
           // 
           // organizeByLabel
           // 
           resources.ApplyResources( this.organizeByLabel, "organizeByLabel" );
           this.organizeByLabel.ForeColor = System.Drawing.SystemColors.ControlText;
           this.organizeByLabel.Name = "organizeByLabel";
           // 
           // filesIndividualCheckBox
           // 
           resources.ApplyResources( this.filesIndividualCheckBox, "filesIndividualCheckBox" );
           this.filesIndividualCheckBox.Checked = true;
           this.filesIndividualCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.filesIndividualCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.filesIndividualCheckBox.Name = "filesIndividualCheckBox";
           this.filesIndividualCheckBox.UseVisualStyleBackColor = true;
           this.filesIndividualCheckBox.CheckedChanged += new System.EventHandler( this.FilesIndividualCheckBox_CheckedChanged );
           // 
           // filesSummaryCheckBox
           // 
           resources.ApplyResources( this.filesSummaryCheckBox, "filesSummaryCheckBox" );
           this.filesSummaryCheckBox.Checked = true;
           this.filesSummaryCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.filesSummaryCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.filesSummaryCheckBox.Name = "filesSummaryCheckBox";
           this.filesSummaryCheckBox.UseVisualStyleBackColor = true;
           this.filesSummaryCheckBox.CheckedChanged += new System.EventHandler( this.FilesSummaryCheckBox_CheckedChanged );
           // 
           // exportFolderTextBox
           // 
           resources.ApplyResources( this.exportFolderTextBox, "exportFolderTextBox" );
           this.exportFolderTextBox.Name = "exportFolderTextBox";
           this.exportFolderTextBox.ReadOnly = true;
           // 
           // label20
           // 
           resources.ApplyResources( this.label20, "label20" );
           this.label20.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label20.Name = "label20";
           // 
           // groupBox2
           // 
           this.groupBox2.Controls.Add( this.visLabelsCheckBox );
           this.groupBox2.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.groupBox2, "groupBox2" );
           this.groupBox2.Name = "groupBox2";
           this.groupBox2.TabStop = false;
           // 
           // visLabelsCheckBox
           // 
           resources.ApplyResources( this.visLabelsCheckBox, "visLabelsCheckBox" );
           this.visLabelsCheckBox.Checked = true;
           this.visLabelsCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.visLabelsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.visLabelsCheckBox.Name = "visLabelsCheckBox";
           this.visLabelsCheckBox.UseVisualStyleBackColor = true;
           // 
           // groupBox1
           // 
           this.groupBox1.Controls.Add( this.label23 );
           this.groupBox1.Controls.Add( this.dqRSSIUpDown );
           this.groupBox1.Controls.Add( this.dqRSSICheckBox );
           this.groupBox1.Controls.Add( this.label22 );
           this.groupBox1.Controls.Add( this.dqSatCountCheckBox );
           this.groupBox1.Controls.Add( this.dqGPSLockedUpCheckBox );
           this.groupBox1.Controls.Add( this.label25 );
           this.groupBox1.Controls.Add( this.dqSatFixLostCheckBox );
           this.groupBox1.Controls.Add( this.dqSpeedCheckBox );
           this.groupBox1.Controls.Add( this.dqSatCountUpDown );
           this.groupBox1.Controls.Add( this.dqSpeedUpDown );
           this.groupBox1.Controls.Add( this.label24 );
           this.groupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.groupBox1, "groupBox1" );
           this.groupBox1.Name = "groupBox1";
           this.groupBox1.TabStop = false;
           // 
           // label23
           // 
           resources.ApplyResources( this.label23, "label23" );
           this.label23.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label23.Name = "label23";
           // 
           // dqRSSIUpDown
           // 
           this.dqRSSIUpDown.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.dqRSSIUpDown, "dqRSSIUpDown" );
           this.dqRSSIUpDown.Maximum = new decimal( new int[] {
            200,
            0,
            0,
            0} );
           this.dqRSSIUpDown.Minimum = new decimal( new int[] {
            200,
            0,
            0,
            -2147483648} );
           this.dqRSSIUpDown.Name = "dqRSSIUpDown";
           this.dqRSSIUpDown.Value = new decimal( new int[] {
            20,
            0,
            0,
            -2147483648} );
           // 
           // dqRSSICheckBox
           // 
           resources.ApplyResources( this.dqRSSICheckBox, "dqRSSICheckBox" );
           this.dqRSSICheckBox.Checked = true;
           this.dqRSSICheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.dqRSSICheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.dqRSSICheckBox.Name = "dqRSSICheckBox";
           this.dqRSSICheckBox.UseVisualStyleBackColor = true;
           // 
           // label22
           // 
           resources.ApplyResources( this.label22, "label22" );
           this.label22.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label22.Name = "label22";
           // 
           // dqSatCountCheckBox
           // 
           resources.ApplyResources( this.dqSatCountCheckBox, "dqSatCountCheckBox" );
           this.dqSatCountCheckBox.Checked = true;
           this.dqSatCountCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.dqSatCountCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.dqSatCountCheckBox.Name = "dqSatCountCheckBox";
           this.dqSatCountCheckBox.UseVisualStyleBackColor = true;
           // 
           // dqGPSLockedUpCheckBox
           // 
           resources.ApplyResources( this.dqGPSLockedUpCheckBox, "dqGPSLockedUpCheckBox" );
           this.dqGPSLockedUpCheckBox.Checked = true;
           this.dqGPSLockedUpCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.dqGPSLockedUpCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.dqGPSLockedUpCheckBox.Name = "dqGPSLockedUpCheckBox";
           this.dqGPSLockedUpCheckBox.UseVisualStyleBackColor = true;
           // 
           // label25
           // 
           resources.ApplyResources( this.label25, "label25" );
           this.label25.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label25.Name = "label25";
           // 
           // dqSatFixLostCheckBox
           // 
           resources.ApplyResources( this.dqSatFixLostCheckBox, "dqSatFixLostCheckBox" );
           this.dqSatFixLostCheckBox.Checked = true;
           this.dqSatFixLostCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.dqSatFixLostCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.dqSatFixLostCheckBox.Name = "dqSatFixLostCheckBox";
           this.dqSatFixLostCheckBox.UseVisualStyleBackColor = true;
           // 
           // dqSpeedCheckBox
           // 
           resources.ApplyResources( this.dqSpeedCheckBox, "dqSpeedCheckBox" );
           this.dqSpeedCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
           this.dqSpeedCheckBox.Name = "dqSpeedCheckBox";
           this.dqSpeedCheckBox.UseVisualStyleBackColor = true;
           // 
           // dqSatCountUpDown
           // 
           this.dqSatCountUpDown.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.dqSatCountUpDown, "dqSatCountUpDown" );
           this.dqSatCountUpDown.Name = "dqSatCountUpDown";
           this.dqSatCountUpDown.Value = new decimal( new int[] {
            4,
            0,
            0,
            0} );
           // 
           // dqSpeedUpDown
           // 
           this.dqSpeedUpDown.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.dqSpeedUpDown, "dqSpeedUpDown" );
           this.dqSpeedUpDown.Name = "dqSpeedUpDown";
           // 
           // label24
           // 
           resources.ApplyResources( this.label24, "label24" );
           this.label24.ForeColor = System.Drawing.SystemColors.ControlText;
           this.label24.Name = "label24";
           // 
           // cancelButton
           // 
           this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
           this.cancelButton.ForeColor = System.Drawing.SystemColors.ControlText;
           resources.ApplyResources( this.cancelButton, "cancelButton" );
           this.cancelButton.Name = "cancelButton";
           this.cancelButton.UseVisualStyleBackColor = true;
           // 
           // KmlExporterForm
           // 
           resources.ApplyResources( this, "$this" );
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.BackColor = System.Drawing.SystemColors.Control;
           this.Controls.Add( this.cancelButton );
           this.Controls.Add( this.groupBox3 );
           this.Controls.Add( this.groupBox2 );
           this.Controls.Add( this.exportButton );
           this.Controls.Add( this.groupBox1 );
           this.ForeColor = System.Drawing.SystemColors.HighlightText;
           this.MaximizeBox = false;
           this.MinimizeBox = false;
           this.Name = "KmlExporterForm";
           this.Load += new System.EventHandler( this.KmlExporterForm_Load );
           this.groupBox3.ResumeLayout( false );
           this.groupBox3.PerformLayout();
           this.groupBox2.ResumeLayout( false );
           this.groupBox2.PerformLayout();
           this.groupBox1.ResumeLayout( false );
           this.groupBox1.PerformLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.dqRSSIUpDown ) ).EndInit();
           ( (System.ComponentModel.ISupportInitialize)( this.dqSatCountUpDown ) ).EndInit();
           ( (System.ComponentModel.ISupportInitialize)( this.dqSpeedUpDown ) ).EndInit();
           this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button exportButton;
        private System.Windows.Forms.OpenFileDialog selectInputFileDialog;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox exportFolderTextBox;
        private System.Windows.Forms.CheckBox dqGPSLockedUpCheckBox;
        private System.Windows.Forms.CheckBox dqSatFixLostCheckBox;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.NumericUpDown dqSatCountUpDown;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.CheckBox dqSpeedCheckBox;
        private System.Windows.Forms.NumericUpDown dqSpeedUpDown;
        private System.Windows.Forms.CheckBox dqSatCountCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox filesSummaryCheckBox;
        private System.Windows.Forms.CheckBox filesIndividualCheckBox;
        private System.Windows.Forms.NumericUpDown dqRSSIUpDown;
        private System.Windows.Forms.CheckBox dqRSSICheckBox;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox FilesComprehensiveFileComboBox;
        private System.Windows.Forms.Label organizeByLabel;
        private System.Windows.Forms.CheckBox visLabelsCheckBox;
        private System.Windows.Forms.CheckBox filesComprehensiveFileCheckBox;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TextBox inputFileTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button selectExportFolderButton;
        private System.Windows.Forms.Button selectInputFileButton;
        private System.Windows.Forms.FolderBrowserDialog selectExportFolderDialog;
    }
}

