using System;
using System.Runtime.InteropServices;
using System.Net.NetworkInformation;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;

namespace NativeWifi
{
	// TODO: Separate the functions and the structs/enums. Many of the structs/enums should remain public
	// (since they're reused in the OOP interfaces) -- the rest (including all P/Invoke function mappings)
	// should become internal.

	// All structures which native methods rely on should be kept in the Wlan class.
	// Only change the layout of those structures if it matches the native API documentation.
	// Some structures might have helper properties but adding or changing fields is prohibited.
	// This class is not documented since all the documentation resides in the MSDN. The code
	// documentation only covers details which concern interop users.
	// Some identifier names were modified to correspond to .NET naming conventions
	// but otherwise retain their native meaning.

	/// <summary>
	/// Defines the Native Wifi API through P/Invoke interop.
	/// </summary>
	/// <remarks>
	/// This class is intended for internal use. Use the <see cref="WlanClient"/> class instead.
	/// </remarks>
	public static class Wlan
	{
		#region P/Invoke API
		/// <summary>
		/// Defines various opcodes used to set and query parameters for an interface.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_INTF_OPCODE</c> type.
		/// </remarks>
		public enum WlanIntfOpcode
		{
			/// <summary>
			/// Opcode used to set or query whether auto config is enabled.
			/// </summary>
			AutoconfEnabled = 1,
			/// <summary>
			/// Opcode used to set or query whether background scan is enabled.
			/// </summary>
			BackgroundScanEnabled,
			/// <summary>
			/// Opcode used to set or query the media streaming mode of the driver.
			/// </summary>
			MediaStreamingMode,
			/// <summary>
			/// Opcode used to set or query the radio state.
			/// </summary>
			RadioState,
			/// <summary>
			/// Opcode used to set or query the BSS type of the interface.
			/// </summary>
			BssType,
			/// <summary>
			/// Opcode used to query the state of the interface.
			/// </summary>
			InterfaceState,
			/// <summary>
			/// Opcode used to query information about the current connection of the interface.
			/// </summary>
			CurrentConnection,
			/// <summary>
			/// Opcose used to query the current channel on which the wireless interface is operating.
			/// </summary>
			ChannelNumber,
			/// <summary>
			/// Opcode used to query the supported auth/cipher pairs for infrastructure mode.
			/// </summary>
			SupportedInfrastructureAuthCipherPairs,
			/// <summary>
			/// Opcode used to query the supported auth/cipher pairs for ad hoc mode.
			/// </summary>
			SupportedAdhocAuthCipherPairs,
			/// <summary>
			/// Opcode used to query the list of supported country or region strings.
			/// </summary>
			SupportedCountryOrRegionStringList,
			/// <summary>
			/// Opcode used to set or query the current operation mode of the wireless interface.
			/// </summary>
			CurrentOperationMode,
			/// <summary>
			/// Opcode used to query driver statistics.
			/// </summary>
			Statistics = 0x10000101,
			/// <summary>
			/// Opcode used to query the received signal strength.
			/// </summary>
			Rssi,
			SecurityStart = 0x20010000,
			SecurityEnd = 0x2fffffff,
			IhvStart = 0x30000000,
			IhvEnd = 0x3fffffff
		}

		/// <summary>
		/// Specifies the origin of automatic configuration (auto config) settings.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_OPCODE_VALUE_TYPE</c> type.
		/// </remarks>
		public enum WlanOpcodeValueType
		{
			/// <summary>
			/// The auto config settings were queried, but the origin of the settings was not determined.
			/// </summary>
			QueryOnly = 0,
			/// <summary>
			/// The auto config settings were set by group policy.
			/// </summary>
			SetByGroupPolicy = 1,
			/// <summary>
			/// The auto config settings were set by the user.
			/// </summary>
			SetByUser = 2,
			/// <summary>
			/// The auto config settings are invalid.
			/// </summary>
			Invalid = 3
		}

		public const uint WlanClientVersionXpSp2 = 1;
		public const uint WlanClientVersionLonghorn = 2;

		[DllImport("wlanapi.dll")]
		public static extern int WlanOpenHandle(
			[In] UInt32 clientVersion,
			[In, Out] IntPtr pReserved,
			[Out] out UInt32 negotiatedVersion,
			[Out] out IntPtr clientHandle);

		[DllImport("wlanapi.dll")]
		public static extern int WlanCloseHandle(
			[In] IntPtr clientHandle,
			[In, Out] IntPtr pReserved);

		[DllImport("wlanapi.dll")]
		public static extern int WlanEnumInterfaces(
			[In] IntPtr clientHandle,
			[In, Out] IntPtr pReserved,
			[Out] out IntPtr ppInterfaceList);

		[DllImport("wlanapi.dll")]
		public static extern int WlanQueryInterface(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] WlanIntfOpcode opCode,
			[In, Out] IntPtr pReserved,
			[Out] out int dataSize,
			[Out] out IntPtr ppData,
			[Out] out WlanOpcodeValueType wlanOpcodeValueType);

		[DllImport("wlanapi.dll")]
		public static extern int WlanSetInterface(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] WlanIntfOpcode opCode,
			[In] uint dataSize,
			[In] IntPtr pData,
			[In, Out] IntPtr pReserved);

		/// <param name="interfaceGuid"></param>
		/// <param name="pDot11Ssid">Not supported on Windows XP SP2: must be a <c>null</c> reference.</param>
		/// <param name="pIeData">Not supported on Windows XP SP2: must be a <c>null</c> reference.</param>
		/// <param name="clientHandle"></param>
		/// <param name="pReserved"></param>
		[DllImport("wlanapi.dll")]
		public static extern int WlanScan(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] IntPtr pDot11Ssid,
			[In] IntPtr pIeData,
			[In, Out] IntPtr pReserved);

		/// <summary>
		/// Defines flags passed to <see cref="WlanGetAvailableNetworkList"/>.
		/// </summary>
		[Flags]
		public enum WlanGetAvailableNetworkFlags
		{
			/// <summary>
			/// Include all ad-hoc network profiles in the available network list, including profiles that are not visible.
			/// </summary>
			IncludeAllAdhocProfiles = 0x00000001,
			/// <summary>
			/// Include all hidden network profiles in the available network list, including profiles that are not visible.
			/// </summary>
			IncludeAllManualHiddenProfiles = 0x00000002
		}

		/// <summary>
		/// The header of an array of information about available networks.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		internal struct WlanAvailableNetworkListHeader
		{
			/// <summary>
			/// Contains the number of items following the header.
			/// </summary>
			public uint NumberOfItems;
			/// <summary>
			/// The index of the current item. The index of the first item is 0.
			/// </summary>
			public uint Index;
		}

		/// <summary>
		/// Contains various flags for the network.
		/// </summary>
		[Flags]
		public enum WlanAvailableNetworkFlags
		{
			/// <summary>
			/// This network is currently connected.
			/// </summary>
			Connected = 0x00000001,
			/// <summary>
			/// There is a profile for this network.
			/// </summary>
			HasProfile = 0x00000002
		}

		/// <summary>
		/// Contains information about an available wireless network.
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet=CharSet.Unicode)]
		public struct WlanAvailableNetwork
		{
			/// <summary>
			/// Contains the profile name associated with the network.
			/// If the network doesn't have a profile, this member will be empty.
			/// If multiple profiles are associated with the network, there will be multiple entries with the same SSID in the visible network list. Profile names are case-sensitive.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string ProfileName;
			/// <summary>
			/// Contains the SSID of the visible wireless network.
			/// </summary>
			public Dot11Ssid Dot11Ssid;
			/// <summary>
			/// Specifies whether the network is infrastructure or ad hoc.
			/// </summary>
			public Dot11BssType Dot11BssType;
			/// <summary>
			/// Indicates the number of BSSIDs in the network.
			/// </summary>
			public uint NumberOfBssids;
			/// <summary>
			/// Indicates whether the network is connectable or not.
			/// </summary>
			public bool NetworkConnectable;
			/// <summary>
			/// Indicates why a network cannot be connected to. This member is only valid when <see cref="NetworkConnectable"/> is <c>false</c>.
			/// </summary>
			public WlanReasonCode WlanNotConnectableReason;
			/// <summary>
			/// The number of PHY types supported on available networks.
			/// The maximum value of this field is 8. If more than 8 PHY types are supported, <see cref="MorePhyTypes"/> must be set to <c>true</c>.
			/// </summary>
			private uint _numberOfPhyTypes;
			/// <summary>
			/// Contains an array of <see cref="Dot11PhyType"/> values that represent the PHY types supported by the available networks.
			/// When <see cref="_numberOfPhyTypes"/> is greater than 8, this array contains only the first 8 PHY types.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			private Dot11PhyType[] _dot11PhyTypes;
			/// <summary>
			/// Gets the <see cref="Dot11PhyType"/> values that represent the PHY types supported by the available networks.
			/// </summary>
			public Dot11PhyType[] Dot11PhyTypes
			{
				get
				{
					Dot11PhyType[] ret = new Dot11PhyType[_numberOfPhyTypes];
					Array.Copy(_dot11PhyTypes, ret, _numberOfPhyTypes);
					return ret;
				}
			}
			/// <summary>
			/// Specifies if there are more than 8 PHY types supported.
			/// When this member is set to <c>true</c>, an application must call <see cref="WlanClient.WlanInterface.GetNetworkBssList()"/> to get the complete list of PHY types.
			/// <see cref="WlanBssEntry.PhyId"/> contains the PHY type for an entry.
			/// </summary>
			public bool MorePhyTypes;
			/// <summary>
			/// A percentage value that represents the signal quality of the network.
			/// This field contains a value between 0 and 100.
			/// A value of 0 implies an actual RSSI signal strength of -100 dbm.
			/// A value of 100 implies an actual RSSI signal strength of -50 dbm.
			/// You can calculate the RSSI signal strength value for values between 1 and 99 using linear interpolation.
			/// </summary>
			public uint WlanSignalQuality;
			/// <summary>
			/// Indicates whether security is enabled on the network.
			/// </summary>
			public bool SecurityEnabled;
			/// <summary>
			/// Indicates the default authentication algorithm used to join this network for the first time.
			/// </summary>
			public Dot11AuthAlgorithm Dot11DefaultAuthAlgorithm;
			/// <summary>
			/// Indicates the default cipher algorithm to be used when joining this network.
			/// </summary>
			public Dot11CipherAlgorithm Dot11DefaultCipherAlgorithm;
			/// <summary>
			/// Contains various flags for the network.
			/// </summary>
			public WlanAvailableNetworkFlags Flags;
			/// <summary>
			/// Reserved for future use. Must be set to NULL.
			/// </summary>
			uint _reserved;
		}

		[DllImport("wlanapi.dll")]
		public static extern int WlanGetAvailableNetworkList(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] WlanGetAvailableNetworkFlags flags,
			[In, Out] IntPtr reservedPtr,
			[Out] out IntPtr availableNetworkListPtr);

		[Flags]
		public enum WlanProfileFlags
		{
			/// <remarks>
			/// The only option available on Windows XP SP2.
			/// </remarks>
			AllUser = 0,
			GroupPolicy = 1,
			User = 2
		}

		[DllImport("wlanapi.dll")]
		public static extern int WlanSetProfile(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] WlanProfileFlags flags,
			[In, MarshalAs(UnmanagedType.LPWStr)] string profileXml,
			[In, Optional, MarshalAs(UnmanagedType.LPWStr)] string allUserProfileSecurity,
			[In] bool overwrite,
			[In] IntPtr pReserved,
			[Out] out WlanReasonCode reasonCode);

		/// <summary>
		/// Defines the access mask of an all-user profile.
		/// </summary>
		[Flags]
		public enum WlanAccess
		{
			/// <summary>
			/// The user can view profile permissions.
			/// </summary>
			ReadAccess = 0x00020000 | 0x0001,
			/// <summary>
			/// The user has read access, and the user can also connect to and disconnect from a network using the profile.
			/// </summary>
			ExecuteAccess = ReadAccess | 0x0020,
			/// <summary>
			/// The user has execute access and the user can also modify and delete permissions associated with a profile.
			/// </summary>
			WriteAccess = ReadAccess | ExecuteAccess | 0x0002 | 0x00010000 | 0x00040000
		}

		/// <param name="profileXml"></param>
		/// <param name="flags">Not supported on Windows XP SP2: must be a <c>null</c> reference.</param>
		/// <param name="clientHandle"></param>
		/// <param name="interfaceGuid"></param>
		/// <param name="profileName"></param>
		/// <param name="pReserved"></param>
		/// <param name="grantedAccess"></param>
		[DllImport("wlanapi.dll")]
		public static extern int WlanGetProfile(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In, MarshalAs(UnmanagedType.LPWStr)] string profileName,
			[In] IntPtr pReserved,
			[Out] out IntPtr profileXml,
			[Out, Optional] out WlanProfileFlags flags,
			[Out, Optional] out WlanAccess grantedAccess);

		[DllImport("wlanapi.dll")]
		public static extern int WlanGetProfileList(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] IntPtr pReserved,
			[Out] out IntPtr profileList
		);

		[DllImport("wlanapi.dll")]
		public static extern void WlanFreeMemory(IntPtr pMemory);

		[DllImport("wlanapi.dll")]
		public static extern int WlanReasonCodeToString(
			[In] WlanReasonCode reasonCode,
			[In] int bufferSize,
			[In, Out] StringBuilder stringBuffer,
			IntPtr pReserved
		);

		/// <summary>
		/// Specifies where the notification comes from.
		/// </summary>
		[Flags]
		public enum WlanNotificationSource
		{
			None = 0,
			/// <summary>
			/// All notifications, including those generated by the 802.1X module.
			/// </summary>
			All = 0X0000FFFF,
			/// <summary>
			/// Notifications generated by the auto configuration module.
			/// </summary>
			Acm = 0X00000008,
			/// <summary>
			/// Notifications generated by MSM.
			/// </summary>
			Msm = 0X00000010,
			/// <summary>
			/// Notifications generated by the security module.
			/// </summary>
			Security = 0X00000020,
			/// <summary>
			/// Notifications generated by independent hardware vendors (IHV).
			/// </summary>
			Ihv = 0X00000040
		}

		/// <summary>
		/// Indicates the type of an ACM (<see cref="WlanNotificationSource.Acm"/>) notification.
		/// </summary>
		/// <remarks>
		/// The enumeration identifiers correspond to the native <c>wlan_notification_acm_</c> identifiers.
		/// On Windows XP SP2, only the <c>ConnectionComplete</c> and <c>Disconnected</c> notifications are available.
		/// </remarks>
		public enum WlanNotificationCodeAcm
		{
			AutoconfEnabled = 1,
			AutoconfDisabled,
			BackgroundScanEnabled,
			BackgroundScanDisabled,
			BssTypeChange,
			PowerSettingChange,
			ScanComplete,
			ScanFail,
			ConnectionStart,
			ConnectionComplete,
			ConnectionAttemptFail,
			FilterListChange,
			InterfaceArrival,
			InterfaceRemoval,
			ProfileChange,
			ProfileNameChange,
			ProfilesExhausted,
			NetworkNotAvailable,
			NetworkAvailable,
			Disconnecting,
			Disconnected,
			AdhocNetworkStateChange
		}

		/// <summary>
		/// Indicates the type of an MSM (<see cref="WlanNotificationSource.Msm"/>) notification.
		/// </summary>
		/// <remarks>
		/// The enumeration identifiers correspond to the native <c>wlan_notification_msm_</c> identifiers.
		/// </remarks>
		public enum WlanNotificationCodeMsm
		{
			Associating = 1,
			Associated,
			Authenticating,
			Connected,
			RoamingStart,
			RoamingEnd,
			RadioStateChange,
			SignalQualityChange,
			Disassociating,
			Disconnected,
			PeerJoin,
			PeerLeave,
			AdapterRemoval,
			AdapterOperationModeChange
		}

		/// <summary>
		/// Contains information provided when registering for notifications.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_NOTIFICATION_DATA</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanNotificationData
		{
			/// <summary>
			/// Specifies where the notification comes from.
			/// </summary>
			/// <remarks>
			/// On Windows XP SP2, this field must be set to <see cref="WlanNotificationSource.None"/>, <see cref="WlanNotificationSource.All"/> or <see cref="WlanNotificationSource.Acm"/>.
			/// </remarks>
			public WlanNotificationSource NotificationSource;
			/// <summary>
			/// Indicates the type of notification. The value of this field indicates what type of associated data will be present in <see cref="DataPtr"/>.
			/// </summary>
			private int _notificationCode;
			/// <summary>
			/// Indicates which interface the notification is for.
			/// </summary>
			public Guid InterfaceGuid;
			/// <summary>
			/// Specifies the size of <see cref="DataPtr"/>, in bytes.
			/// </summary>
			public int DataSize;
			/// <summary>
			/// Pointer to additional data needed for the notification, as indicated by <see cref="_notificationCode"/>.
			/// </summary>
			public IntPtr DataPtr;

			/// <summary>
			/// Gets the notification code (in the correct enumeration type) according to the notification source.
			/// </summary>
			public object NotificationCode
			{
				get
				{
				    if (NotificationSource == WlanNotificationSource.Msm)
						return (WlanNotificationCodeMsm)_notificationCode;
				    if (NotificationSource == WlanNotificationSource.Acm)
				        return (WlanNotificationCodeAcm)_notificationCode;
				    return _notificationCode;
				}
			}
		}

		/// <summary>
		/// Defines the callback function which accepts WLAN notifications.
		/// </summary>
		public delegate void WlanNotificationCallbackDelegate(ref WlanNotificationData notificationData, IntPtr context);

		[DllImport("wlanapi.dll")]
		public static extern int WlanRegisterNotification(
			[In] IntPtr clientHandle,
			[In] WlanNotificationSource notifSource,
			[In] bool ignoreDuplicate,
			[In] WlanNotificationCallbackDelegate funcCallback,
			[In] IntPtr callbackContext,
			[In] IntPtr reserved,
			[Out] out WlanNotificationSource prevNotifSource);

		/// <summary>
		/// Defines connection parameter flags.
		/// </summary>
		[Flags]
		public enum WlanConnectionFlags
		{
			/// <summary>
			/// Connect to the destination network even if the destination is a hidden network. A hidden network does not broadcast its SSID. Do not use this flag if the destination network is an ad-hoc network.
			/// <para>If the profile specified by <see cref="WlanConnectionParameters.Profile"/> is not <c>null</c>, then this flag is ignored and the nonBroadcast profile element determines whether to connect to a hidden network.</para>
			/// </summary>
			HiddenNetwork = 0x00000001,
			/// <summary>
			/// Do not form an ad-hoc network. Only join an ad-hoc network if the network already exists. Do not use this flag if the destination network is an infrastructure network.
			/// </summary>
			AdhocJoinOnly = 0x00000002,
			/// <summary>
			/// Ignore the privacy bit when connecting to the network. Ignoring the privacy bit has the effect of ignoring whether packets are encryption and ignoring the method of encryption used. Only use this flag when connecting to an infrastructure network using a temporary profile.
			/// </summary>
			IgnorePrivacyBit = 0x00000004,
			/// <summary>
			/// Exempt EAPOL traffic from encryption and decryption. This flag is used when an application must send EAPOL traffic over an infrastructure network that uses Open authentication and WEP encryption. This flag must not be used to connect to networks that require 802.1X authentication. This flag is only valid when <see cref="WlanConnectionParameters.WlanConnectionMode"/> is set to <see cref="WlanConnectionMode.TemporaryProfile"/>. Avoid using this flag whenever possible.
			/// </summary>
			EapolPassthrough = 0x00000008
		}

		/// <summary>
		/// Specifies the parameters used when using the <see cref="WlanConnect"/> function.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_CONNECTION_PARAMETERS</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanConnectionParameters
		{
			/// <summary>
			/// Specifies the mode of connection.
			/// </summary>
			public WlanConnectionMode WlanConnectionMode;
			/// <summary>
			/// Specifies the profile being used for the connection.
			/// The contents of the field depend on the <see cref="WlanConnectionMode"/>:
			/// <list type="table">
			/// <listheader>
			/// <term>Value of <see cref="WlanConnectionMode"/></term>
			/// <description>Contents of the profile string</description>
			/// </listheader>
			/// <item>
			/// <term><see cref="Wlan.WlanConnectionMode.Profile"/></term>
			/// <description>The name of the profile used for the connection.</description>
			/// </item>
			/// <item>
			/// <term><see cref="Wlan.WlanConnectionMode.TemporaryProfile"/></term>
			/// <description>The XML representation of the profile used for the connection.</description>
			/// </item>
			/// <item>
			/// <term><see cref="Wlan.WlanConnectionMode.DiscoverySecure"/>, <see cref="Wlan.WlanConnectionMode.DiscoveryUnsecure"/> or <see cref="Wlan.WlanConnectionMode.Auto"/></term>
			/// <description><c>null</c></description>
			/// </item>
			/// </list>
			/// </summary>
			[MarshalAs(UnmanagedType.LPWStr)]
			public string Profile;
			/// <summary>
			/// Pointer to a <see cref="Dot11Ssid"/> structure that specifies the SSID of the network to connect to.
			/// This field is optional. When set to <c>null</c>, all SSIDs in the profile will be tried.
			/// This field must not be <c>null</c> if <see cref="WlanConnectionMode"/> is set to <see cref="Wlan.WlanConnectionMode.DiscoverySecure"/> or <see cref="Wlan.WlanConnectionMode.DiscoveryUnsecure"/>.
			/// </summary>
			public IntPtr Dot11SsidPtr;
			/// <summary>
			/// Pointer to a <see cref="Dot11BssidList"/> structure that contains the list of basic service set (BSS) identifiers desired for the connection.
			/// </summary>
			/// <remarks>
			/// On Windows XP SP2, must be set to <c>null</c>.
			/// </remarks>
			public IntPtr DesiredBssidListPtr;
			/// <summary>
			/// A <see cref="Wlan.Dot11BssType"/> value that indicates the BSS type of the network. If a profile is provided, this BSS type must be the same as the one in the profile.
			/// </summary>
			public Dot11BssType Dot11BssType;
			/// <summary>
			/// Specifies ocnnection parameters.
			/// </summary>
			/// <remarks>
			/// On Windows XP SP2, must be set to 0.
			/// </remarks>
			public WlanConnectionFlags Flags;
		}

		/// <summary>
		/// The connection state of an ad hoc network.
		/// </summary>
		public enum WlanAdhocNetworkState
		{
			/// <summary>
			/// The ad hoc network has been formed, but no client or host is connected to the network.
			/// </summary>
			Formed = 0,
			/// <summary>
			/// A client or host is connected to the ad hoc network.
			/// </summary>
			Connected = 1
		}

		[DllImport("wlanapi.dll")]
		public static extern int WlanConnect(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] ref WlanConnectionParameters connectionParameters,
			IntPtr pReserved);

		[DllImport("wlanapi.dll")]
		public static extern int WlanDeleteProfile(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In, MarshalAs(UnmanagedType.LPWStr)] string profileName,
			IntPtr reservedPtr
		);

		[DllImport("wlanapi.dll")]
		public static extern int WlanGetNetworkBssList(
			[In] IntPtr clientHandle,
			[In, MarshalAs(UnmanagedType.LPStruct)] Guid interfaceGuid,
			[In] IntPtr dot11SsidInt,
			[In] Dot11BssType dot11BssType,
			[In] bool securityEnabled,
			IntPtr reservedPtr,
			[Out] out IntPtr wlanBssList
		);

		[StructLayout(LayoutKind.Sequential)]
		internal struct WlanBssListHeader
		{
			internal uint TotalSize;
			internal uint NumberOfItems;
		}

		/// <summary>
		/// Contains information about a basic service set (BSS).
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanBssEntry
		{
			/// <summary>
			/// Contains the SSID of the access point (AP) associated with the BSS.
			/// </summary>
			public Dot11Ssid Dot11Ssid;
			/// <summary>
			/// The identifier of the PHY on which the AP is operating.
			/// </summary>
			public uint PhyId;
			/// <summary>
			/// Contains the BSS identifier.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public byte[] Dot11Bssid;
			/// <summary>
			/// Specifies whether the network is infrastructure or ad hoc.
			/// </summary>
			public Dot11BssType Dot11BssType;
			public Dot11PhyType Dot11BssPhyType;
			/// <summary>
			/// The received signal strength in dBm.
			/// </summary>
			public int Rssi;
			/// <summary>
			/// The link quality reported by the driver. Ranges from 0-100.
			/// </summary>
			public uint LinkQuality;
			/// <summary>
			/// If 802.11d is not implemented, the network interface card (NIC) must set this field to TRUE. If 802.11d is implemented (but not necessarily enabled), the NIC must set this field to TRUE if the BSS operation complies with the configured regulatory domain.
			/// </summary>
			public bool InRegDomain;
			/// <summary>
			/// Contains the beacon interval value from the beacon packet or probe response.
			/// </summary>
			public ushort BeaconPeriod;
			/// <summary>
			/// The timestamp from the beacon packet or probe response.
			/// </summary>
			public ulong Timestamp;
			/// <summary>
			/// The host timestamp value when the beacon or probe response is received.
			/// </summary>
			public ulong HostTimestamp;
			/// <summary>
			/// The capability value from the beacon packet or probe response.
			/// </summary>
			public ushort CapabilityInformation;
			/// <summary>
			/// The frequency of the center channel, in kHz.
			/// </summary>
			public uint ChCenterFrequency;
			/// <summary>
			/// Contains the set of data transfer rates supported by the BSS.
			/// </summary>
			public WlanRateSet WlanRateSet;
			/// <summary>
			/// Offset of the information element (IE) data blob.
			/// </summary>
			public uint IeOffset;
			/// <summary>
			/// Size of the IE data blob, in bytes.
			/// </summary>
			public uint IeSize;
		}

		/// <summary>
		/// Contains the set of supported data rates.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanRateSet
		{
			/// <summary>
			/// The length, in bytes, of <see cref="_rateSet"/>.
			/// </summary>
			private uint _rateSetLength;
			/// <summary>
			/// An array of supported data transfer rates.
			/// If the rate is a basic rate, the first bit of the rate value is set to 1.
			/// A basic rate is the data transfer rate that all stations in a basic service set (BSS) can use to receive frames from the wireless medium.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 126)]
			private ushort[] _rateSet;

			public ushort[] Rates
			{
				get
				{
					ushort[] rates = new ushort[_rateSetLength];
					Array.Copy(_rateSet, rates, rates.Length);
					return rates;
				}
			}

			/// <summary>
			/// CalculateS the data transfer rate in Mbps for an arbitrary supported rate.
			/// </summary>
			/// <param name="rate"></param>
			/// <returns></returns>
			public double GetRateInMbps(int rate)
			{
				return (_rateSet[rate] & 0x7FFF) * 0.5;
			}
		} 

		/// <summary>
		/// Represents an error occuring during WLAN operations which indicate their failure via a <see cref="WlanReasonCode"/>.
		/// </summary>
		public class WlanException : Exception
		{
			private readonly WlanReasonCode _reasonCode;

			WlanException(WlanReasonCode reasonCode)
			{
				_reasonCode = reasonCode;
			}

			/// <summary>
			/// Gets the WLAN reason code.
			/// </summary>
			/// <value>The WLAN reason code.</value>
			public WlanReasonCode ReasonCode
			{
				get { return _reasonCode; }
			}

			/// <summary>
			/// Gets a message that describes the reason code.
			/// </summary>
			/// <value></value>
			/// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
			public override string Message
			{
				get
				{
					StringBuilder sb = new StringBuilder(1024);
					if (WlanReasonCodeToString(_reasonCode, sb.Capacity, sb, IntPtr.Zero) == 0)
						return sb.ToString();
				    return string.Empty;
				}
			}
		}

		// TODO: .NET-ify the WlanReasonCode enum (naming convention + docs).

		/// <summary>
		/// Specifies reasons for a failure of a WLAN operation.
		/// </summary>
		/// <remarks>
		/// To get the WLAN API native reason code identifiers, prefix the identifiers with <c>WLAN_REASON_CODE_</c>.
		/// </remarks>
		public enum WlanReasonCode
		{
			Success = 0,
			// general codes
			Unknown = 0x10000 + 1,

			RangeSize = 0x10000,
			Base = 0x10000 + RangeSize,

			// range for Auto Config
			//
			AcBase = 0x10000 + RangeSize,
			AcConnectBase = (AcBase + RangeSize / 2),
			AcEnd = (AcBase + RangeSize - 1),

			// range for profile manager
			// it has profile adding failure reason codes, but may not have 
			// connection reason codes
			//
			ProfileBase = 0x10000 + (7 * RangeSize),
			ProfileConnectBase = (ProfileBase + RangeSize / 2),
			ProfileEnd = (ProfileBase + RangeSize - 1),

			// range for MSM
			//
			MsmBase = 0x10000 + (2 * RangeSize),
			MsmConnectBase = (MsmBase + RangeSize / 2),
			MsmEnd = (MsmBase + RangeSize - 1),

			// range for MSMSEC
			//
			MsmsecBase = 0x10000 + (3 * RangeSize),
			MsmsecConnectBase = (MsmsecBase + RangeSize / 2),
			MsmsecEnd = (MsmsecBase + RangeSize - 1),

			// AC network incompatible reason codes
			//
			NetworkNotCompatible = (AcBase + 1),
			ProfileNotCompatible = (AcBase + 2),

			// AC connect reason code
			//
			NoAutoConnection = (AcConnectBase + 1),
			NotVisible = (AcConnectBase + 2),
			GpDenied = (AcConnectBase + 3),
			UserDenied = (AcConnectBase + 4),
			BssTypeNotAllowed = (AcConnectBase + 5),
			InFailedList = (AcConnectBase + 6),
			InBlockedList = (AcConnectBase + 7),
			SsidListTooLong = (AcConnectBase + 8),
			ConnectCallFail = (AcConnectBase + 9),
			ScanCallFail = (AcConnectBase + 10),
			NetworkNotAvailable = (AcConnectBase + 11),
			ProfileChangedOrDeleted = (AcConnectBase + 12),
			KeyMismatch = (AcConnectBase + 13),
			UserNotRespond = (AcConnectBase + 14),

			// Profile validation errors
			//
			InvalidProfileSchema = (ProfileBase + 1),
			ProfileMissing = (ProfileBase + 2),
			InvalidProfileName = (ProfileBase + 3),
			InvalidProfileType = (ProfileBase + 4),
			InvalidPhyType = (ProfileBase + 5),
			MsmSecurityMissing = (ProfileBase + 6),
			IhvSecurityNotSupported = (ProfileBase + 7),
			IhvOuiMismatch = (ProfileBase + 8),
			// IHV OUI not present but there is IHV settings in profile
			IhvOuiMissing = (ProfileBase + 9),
			// IHV OUI is present but there is no IHV settings in profile
			IhvSettingsMissing = (ProfileBase + 10),
			// both/conflict MSMSec and IHV security settings exist in profile 
			ConflictSecurity = (ProfileBase + 11),
			// no IHV or MSMSec security settings in profile
			SecurityMissing = (ProfileBase + 12),
			InvalidBssType = (ProfileBase + 13),
			InvalidAdhocConnectionMode = (ProfileBase + 14),
			NonBroadcastSetForAdhoc = (ProfileBase + 15),
			AutoSwitchSetForAdhoc = (ProfileBase + 16),
			AutoSwitchSetForManualConnection = (ProfileBase + 17),
			IhvSecurityOnexMissing = (ProfileBase + 18),
			ProfileSsidInvalid = (ProfileBase + 19),
			TooManySsid = (ProfileBase + 20),

			// MSM network incompatible reasons
			//
			UnsupportedSecuritySetByOs = (MsmBase + 1),
			UnsupportedSecuritySet = (MsmBase + 2),
			BssTypeUnmatch = (MsmBase + 3),
			PhyTypeUnmatch = (MsmBase + 4),
			DatarateUnmatch = (MsmBase + 5),

			// MSM connection failure reasons, to be defined
			// failure reason codes
			//
			// user called to disconnect
			UserCancelled = (MsmConnectBase + 1),
			// got disconnect while associating
			AssociationFailure = (MsmConnectBase + 2),
			// timeout for association
			AssociationTimeout = (MsmConnectBase + 3),
			// pre-association security completed with failure
			PreSecurityFailure = (MsmConnectBase + 4),
			// fail to start post-association security
			StartSecurityFailure = (MsmConnectBase + 5),
			// post-association security completed with failure
			SecurityFailure = (MsmConnectBase + 6),
			// security watchdog timeout
			SecurityTimeout = (MsmConnectBase + 7),
			// got disconnect from driver when roaming
			RoamingFailure = (MsmConnectBase + 8),
			// failed to start security for roaming
			RoamingSecurityFailure = (MsmConnectBase + 9),
			// failed to start security for adhoc-join
			AdhocSecurityFailure = (MsmConnectBase + 10),
			// got disconnection from driver
			DriverDisconnected = (MsmConnectBase + 11),
			// driver operation failed
			DriverOperationFailure = (MsmConnectBase + 12),
			// Ihv service is not available
			IhvNotAvailable = (MsmConnectBase + 13),
			// Response from ihv timed out
			IhvNotResponding = (MsmConnectBase + 14),
			// Timed out waiting for driver to disconnect
			DisconnectTimeout = (MsmConnectBase + 15),
			// An internal error prevented the operation from being completed.
			InternalFailure = (MsmConnectBase + 16),
			// UI Request timed out.
			UiRequestTimeout = (MsmConnectBase + 17),
			// Roaming too often, post security is not completed after 5 times.
			TooManySecurityAttempts = (MsmConnectBase + 18),

			// MSMSEC reason codes
			//

			MsmsecMin = MsmsecBase,

			// Key index specified is not valid
			MsmsecProfileInvalidKeyIndex = (MsmsecBase + 1),
			// Key required, PSK present
			MsmsecProfilePskPresent = (MsmsecBase + 2),
			// Invalid key length
			MsmsecProfileKeyLength = (MsmsecBase + 3),
			// Invalid PSK length
			MsmsecProfilePskLength = (MsmsecBase + 4),
			// No auth/cipher specified
			MsmsecProfileNoAuthCipherSpecified = (MsmsecBase + 5),
			// Too many auth/cipher specified
			MsmsecProfileTooManyAuthCipherSpecified = (MsmsecBase + 6),
			// Profile contains duplicate auth/cipher
			MsmsecProfileDuplicateAuthCipher = (MsmsecBase + 7),
			// Profile raw data is invalid (1x or key data)
			MsmsecProfileRawdataInvalid = (MsmsecBase + 8),
			// Invalid auth/cipher combination
			MsmsecProfileInvalidAuthCipher = (MsmsecBase + 9),
			// 802.1x disabled when it's required to be enabled
			MsmsecProfileOnexDisabled = (MsmsecBase + 10),
			// 802.1x enabled when it's required to be disabled
			MsmsecProfileOnexEnabled = (MsmsecBase + 11),
			MsmsecProfileInvalidPmkcacheMode = (MsmsecBase + 12),
			MsmsecProfileInvalidPmkcacheSize = (MsmsecBase + 13),
			MsmsecProfileInvalidPmkcacheTtl = (MsmsecBase + 14),
			MsmsecProfileInvalidPreauthMode = (MsmsecBase + 15),
			MsmsecProfileInvalidPreauthThrottle = (MsmsecBase + 16),
			// PreAuth enabled when PMK cache is disabled
			MsmsecProfilePreauthOnlyEnabled = (MsmsecBase + 17),
			// Capability matching failed at network
			MsmsecCapabilityNetwork = (MsmsecBase + 18),
			// Capability matching failed at NIC
			MsmsecCapabilityNic = (MsmsecBase + 19),
			// Capability matching failed at profile
			MsmsecCapabilityProfile = (MsmsecBase + 20),
			// Network does not support specified discovery type
			MsmsecCapabilityDiscovery = (MsmsecBase + 21),
			// Passphrase contains invalid character
			MsmsecProfilePassphraseChar = (MsmsecBase + 22),
			// Key material contains invalid character
			MsmsecProfileKeymaterialChar = (MsmsecBase + 23),
			// Wrong key type specified for the auth/cipher pair
			MsmsecProfileWrongKeytype = (MsmsecBase + 24),
			// "Mixed cell" suspected (AP not beaconing privacy, we have privacy enabled profile)
			MsmsecMixedCell = (MsmsecBase + 25),
			// Auth timers or number of timeouts in profile is incorrect
			MsmsecProfileAuthTimersInvalid = (MsmsecBase + 26),
			// Group key update interval in profile is incorrect
			MsmsecProfileInvalidGkeyIntv = (MsmsecBase + 27),
			// "Transition network" suspected, trying legacy 802.11 security
			MsmsecTransitionNetwork = (MsmsecBase + 28),
			// Key contains characters which do not map to ASCII
			MsmsecProfileKeyUnmappedChar = (MsmsecBase + 29),
			// Capability matching failed at profile (auth not found)
			MsmsecCapabilityProfileAuth = (MsmsecBase + 30),
			// Capability matching failed at profile (cipher not found)
			MsmsecCapabilityProfileCipher = (MsmsecBase + 31),

			// Failed to queue UI request
			MsmsecUiRequestFailure = (MsmsecConnectBase + 1),
			// 802.1x authentication did not start within configured time 
			MsmsecAuthStartTimeout = (MsmsecConnectBase + 2),
			// 802.1x authentication did not complete within configured time
			MsmsecAuthSuccessTimeout = (MsmsecConnectBase + 3),
			// Dynamic key exchange did not start within configured time
			MsmsecKeyStartTimeout = (MsmsecConnectBase + 4),
			// Dynamic key exchange did not succeed within configured time
			MsmsecKeySuccessTimeout = (MsmsecConnectBase + 5),
			// Message 3 of 4 way handshake has no key data (RSN/WPA)
			MsmsecM3MissingKeyData = (MsmsecConnectBase + 6),
			// Message 3 of 4 way handshake has no IE (RSN/WPA)
			MsmsecM3MissingIe = (MsmsecConnectBase + 7),
			// Message 3 of 4 way handshake has no Group Key (RSN)
			MsmsecM3MissingGrpKey = (MsmsecConnectBase + 8),
			// Matching security capabilities of IE in M3 failed (RSN/WPA)
			MsmsecPrIeMatching = (MsmsecConnectBase + 9),
			// Matching security capabilities of Secondary IE in M3 failed (RSN)
			MsmsecSecIeMatching = (MsmsecConnectBase + 10),
			// Required a pairwise key but AP configured only group keys
			MsmsecNoPairwiseKey = (MsmsecConnectBase + 11),
			// Message 1 of group key handshake has no key data (RSN/WPA)
			MsmsecG1MissingKeyData = (MsmsecConnectBase + 12),
			// Message 1 of group key handshake has no group key
			MsmsecG1MissingGrpKey = (MsmsecConnectBase + 13),
			// AP reset secure bit after connection was secured
			MsmsecPeerIndicatedInsecure = (MsmsecConnectBase + 14),
			// 802.1x indicated there is no authenticator but profile requires 802.1x
			MsmsecNoAuthenticator = (MsmsecConnectBase + 15),
			// Plumbing settings to NIC failed
			MsmsecNicFailure = (MsmsecConnectBase + 16),
			// Operation was cancelled by caller
			MsmsecCancelled = (MsmsecConnectBase + 17),
			// Key was in incorrect format 
			MsmsecKeyFormat = (MsmsecConnectBase + 18),
			// Security downgrade detected
			MsmsecDowngradeDetected = (MsmsecConnectBase + 19),
			// PSK mismatch suspected
			MsmsecPskMismatchSuspected = (MsmsecConnectBase + 20),
			// Forced failure because connection method was not secure
			MsmsecForcedFailure = (MsmsecConnectBase + 21),
			// ui request couldn't be queued or user pressed cancel
			MsmsecSecurityUiFailure = (MsmsecConnectBase + 22),

			MsmsecMax = MsmsecEnd
		}

		/// <summary>
		/// Contains information about connection related notifications.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_CONNECTION_NOTIFICATION_DATA</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WlanConnectionNotificationData
		{
			/// <remarks>
			/// On Windows XP SP 2, only <see cref="Wlan.WlanConnectionMode.Profile"/> is supported.
			/// </remarks>
			public WlanConnectionMode WlanConnectionMode;
			/// <summary>
			/// The name of the profile used for the connection. Profile names are case-sensitive.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 32)]
			public string ProfileName;
			/// <summary>
			/// The SSID of the association.
			/// </summary>
			public Dot11Ssid Dot11Ssid;
			/// <summary>
			/// The BSS network type.
			/// </summary>
			public Dot11BssType Dot11BssType;
			/// <summary>
			/// Indicates whether security is enabled for this connection.
			/// </summary>
			public bool SecurityEnabled;
			/// <summary>
			/// Indicates the reason for an operation failure.
			/// This field has a value of <see cref="Wlan.WlanReasonCode.Success"/> for all connection-related notifications except <see cref="WlanNotificationCodeAcm.ConnectionComplete"/>.
			/// If the connection fails, this field indicates the reason for the failure.
			/// </summary>
			public WlanReasonCode WlanReasonCode;
			/// <summary>
			/// This field contains the XML presentation of the profile used for discovery, if the connection succeeds.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1)]
			public string ProfileXml;
		}

		/// <summary>
		/// Indicates the state of an interface.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_INTERFACE_STATE</c> type.
		/// </remarks>
		public enum WlanInterfaceState
		{
			/// <summary>
			/// The interface is not ready to operate.
			/// </summary>
			NotReady = 0,
			/// <summary>
			/// The interface is connected to a network.
			/// </summary>
			Connected = 1,
			/// <summary>
			/// The interface is the first node in an ad hoc network. No peer has connected.
			/// </summary>
			AdHocNetworkFormed = 2,
			/// <summary>
			/// The interface is disconnecting from the current network.
			/// </summary>
			Disconnecting = 3,
			/// <summary>
			/// The interface is not connected to any network.
			/// </summary>
			Disconnected = 4,
			/// <summary>
			/// The interface is attempting to associate with a network.
			/// </summary>
			Associating = 5,
			/// <summary>
			/// Auto configuration is discovering the settings for the network.
			/// </summary>
			Discovering = 6,
			/// <summary>
			/// The interface is in the process of authenticating.
			/// </summary>
			Authenticating = 7
		}

		/// <summary>
		/// Contains the SSID of an interface.
		/// </summary>
		public struct Dot11Ssid
		{
			/// <summary>
			/// The length, in bytes, of the <see cref="Ssid"/> array.
			/// </summary>
			public uint SsidLength;
			/// <summary>
			/// The SSID.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
			public byte[] Ssid;
		}

		/// <summary>
		/// Defines an 802.11 PHY and media type.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>DOT11_PHY_TYPE</c> type.
		/// </remarks>
		public enum Dot11PhyType : uint
		{
			/// <summary>
			/// Specifies an unknown or uninitialized PHY type.
			/// </summary>
			Unknown = 0,
			/// <summary>
			/// Specifies any PHY type.
			/// </summary>
			Any = Unknown,
			/// <summary>
			/// Specifies a frequency-hopping spread-spectrum (FHSS) PHY. Bluetooth devices can use FHSS or an adaptation of FHSS.
			/// </summary>
			Fhss = 1,
			/// <summary>
			/// Specifies a direct sequence spread spectrum (DSSS) PHY.
			/// </summary>
			Dsss = 2,
			/// <summary>
			/// Specifies an infrared (IR) baseband PHY.
			/// </summary>
			IrBaseband = 3,
			/// <summary>
			/// Specifies an orthogonal frequency division multiplexing (OFDM) PHY. 802.11a devices can use OFDM.
			/// </summary>
			Ofdm = 4,
			/// <summary>
			/// Specifies a high-rate DSSS (HRDSSS) PHY.
			/// </summary>
			Hrdsss = 5,
			/// <summary>
			/// Specifies an extended rate PHY (ERP). 802.11g devices can use ERP.
			/// </summary>
			Erp = 6,
            /// <summary>
            /// Specifies a high-throughput (HT) 802.11n PHY. 
            /// Each 802.11n PHY, whether dual-band or not, is specified as this PHY type.
            /// </summary>
            Ht = 7,
			/// <summary>
			/// Specifies the start of the range that is used to define PHY types that are developed by an independent hardware vendor (IHV).
			/// </summary>
			IhvStart = 0x80000000,
			/// <summary>
			/// Specifies the end of the range that is used to define PHY types that are developed by an independent hardware vendor (IHV).
			/// </summary>
			IhvEnd = 0xffffffff
		}

		/// <summary>
		/// Defines a basic service set (BSS) network type.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>DOT11_BSS_TYPE</c> type.
		/// </remarks>
		public enum Dot11BssType
		{
			/// <summary>
			/// Specifies an infrastructure BSS network.
			/// </summary>
			Infrastructure = 1,
			/// <summary>
			/// Specifies an independent BSS (IBSS) network.
			/// </summary>
			Independent = 2,
			/// <summary>
			/// Specifies either infrastructure or IBSS network.
			/// </summary>
			Any = 3
		}

		/// <summary>
		/// Contains association attributes for a connection
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_ASSOCIATION_ATTRIBUTES</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanAssociationAttributes
		{
			/// <summary>
			/// The SSID of the association.
			/// </summary>
			public Dot11Ssid Dot11Ssid;
			/// <summary>
			/// Specifies whether the network is infrastructure or ad hoc.
			/// </summary>
			public Dot11BssType Dot11BssType;
			/// <summary>
			/// The BSSID of the association.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)] private byte[] _dot11Bssid;
			/// <summary>
			/// The physical type of the association.
			/// </summary>
			public Dot11PhyType Dot11PhyType;
			/// <summary>
			/// The position of the <see cref="Wlan.Dot11PhyType"/> value in the structure containing the list of PHY types.
			/// </summary>
			public uint Dot11PhyIndex;
			/// <summary>
			/// A percentage value that represents the signal quality of the network.
			/// This field contains a value between 0 and 100.
			/// A value of 0 implies an actual RSSI signal strength of -100 dbm.
			/// A value of 100 implies an actual RSSI signal strength of -50 dbm.
			/// You can calculate the RSSI signal strength value for values between 1 and 99 using linear interpolation.
			/// </summary>
			public uint WlanSignalQuality;
			/// <summary>
			/// The receiving rate of the association.
			/// </summary>
			public uint RxRate;
			/// <summary>
			/// The transmission rate of the association.
			/// </summary>
			public uint TxRate;

			/// <summary>
			/// Gets the BSSID of the associated access point.
			/// </summary>
			/// <value>The BSSID.</value>
			public PhysicalAddress Dot11Bssid
			{
				get { return new PhysicalAddress(_dot11Bssid); }
			}
		}

		/// <summary>
		/// Defines the mode of connection.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_CONNECTION_MODE</c> type.
		/// </remarks>
		public enum WlanConnectionMode
		{
			/// <summary>
			/// A profile will be used to make the connection.
			/// </summary>
			Profile = 0,
			/// <summary>
			/// A temporary profile will be used to make the connection.
			/// </summary>
			TemporaryProfile,
			/// <summary>
			/// Secure discovery will be used to make the connection.
			/// </summary>
			DiscoverySecure,
			/// <summary>
			/// Unsecure discovery will be used to make the connection.
			/// </summary>
			DiscoveryUnsecure,
			/// <summary>
			/// A connection will be made automatically, generally using a persistent profile.
			/// </summary>
			Auto,
			/// <summary>
			/// Not used.
			/// </summary>
			Invalid
		}

		/// <summary>
		/// Defines a wireless LAN authentication algorithm.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>DOT11_AUTH_ALGORITHM</c> type.
		/// </remarks>
		public enum Dot11AuthAlgorithm : uint
		{
			/// <summary>
			/// Specifies an IEEE 802.11 Open System authentication algorithm.
			/// </summary>
			Ieee80211Open = 1,
			/// <summary>
			/// Specifies an 802.11 Shared Key authentication algorithm that requires the use of a pre-shared Wired Equivalent Privacy (WEP) key for the 802.11 authentication.
			/// </summary>
			Ieee80211SharedKey = 2,
			/// <summary>
			/// Specifies a Wi-Fi Protected Access (WPA) algorithm. IEEE 802.1X port authentication is performed by the supplicant, authenticator, and authentication server. Cipher keys are dynamically derived through the authentication process.
			/// <para>This algorithm is valid only for BSS types of <see cref="Dot11BssType.Infrastructure"/>.</para>
			/// <para>When the WPA algorithm is enabled, the 802.11 station will associate only with an access point whose beacon or probe responses contain the authentication suite of type 1 (802.1X) within the WPA information element (IE).</para>
			/// </summary>
			Wpa = 3,
			/// <summary>
			/// Specifies a WPA algorithm that uses preshared keys (PSK). IEEE 802.1X port authentication is performed by the supplicant and authenticator. Cipher keys are dynamically derived through a preshared key that is used on both the supplicant and authenticator.
			/// <para>This algorithm is valid only for BSS types of <see cref="Dot11BssType.Infrastructure"/>.</para>
			/// <para>When the WPA PSK algorithm is enabled, the 802.11 station will associate only with an access point whose beacon or probe responses contain the authentication suite of type 2 (preshared key) within the WPA IE.</para>
			/// </summary>
			WpaPsk = 4,
			/// <summary>
			/// This value is not supported.
			/// </summary>
			WpaNone = 5,
			/// <summary>
			/// Specifies an 802.11i Robust Security Network Association (RSNA) algorithm. WPA2 is one such algorithm. IEEE 802.1X port authentication is performed by the supplicant, authenticator, and authentication server. Cipher keys are dynamically derived through the authentication process.
			/// <para>This algorithm is valid only for BSS types of <see cref="Dot11BssType.Infrastructure"/>.</para>
			/// <para>When the RSNA algorithm is enabled, the 802.11 station will associate only with an access point whose beacon or probe responses contain the authentication suite of type 1 (802.1X) within the RSN IE.</para>
			/// </summary>
			Rsna = 6,
			/// <summary>
			/// Specifies an 802.11i RSNA algorithm that uses PSK. IEEE 802.1X port authentication is performed by the supplicant and authenticator. Cipher keys are dynamically derived through a preshared key that is used on both the supplicant and authenticator.
			/// <para>This algorithm is valid only for BSS types of <see cref="Dot11BssType.Infrastructure"/>.</para>
			/// <para>When the RSNA PSK algorithm is enabled, the 802.11 station will associate only with an access point whose beacon or probe responses contain the authentication suite of type 2(preshared key) within the RSN IE.</para>
			/// </summary>
			RsnaPsk = 7,
			/// <summary>
			/// Indicates the start of the range that specifies proprietary authentication algorithms that are developed by an IHV.
			/// </summary>
			/// <remarks>
			/// This enumerator is valid only when the miniport driver is operating in Extensible Station (ExtSTA) mode.
			/// </remarks>
			IhvStart = 0x80000000,
			/// <summary>
			/// Indicates the end of the range that specifies proprietary authentication algorithms that are developed by an IHV.
			/// </summary>
			/// <remarks>
			/// This enumerator is valid only when the miniport driver is operating in Extensible Station (ExtSTA) mode.
			/// </remarks>
			IhvEnd = 0xffffffff
		}

		/// <summary>
		/// Defines a cipher algorithm for data encryption and decryption.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>DOT11_CIPHER_ALGORITHM</c> type.
		/// </remarks>
		public enum Dot11CipherAlgorithm : uint
		{
			/// <summary>
			/// Specifies that no cipher algorithm is enabled or supported.
			/// </summary>
			None = 0x00,
			/// <summary>
			/// Specifies a Wired Equivalent Privacy (WEP) algorithm, which is the RC4-based algorithm that is specified in the 802.11-1999 standard. This enumerator specifies the WEP cipher algorithm with a 40-bit cipher key.
			/// </summary>
			Wep40 = 0x01,
			/// <summary>
			/// Specifies a Temporal Key Integrity Protocol (TKIP) algorithm, which is the RC4-based cipher suite that is based on the algorithms that are defined in the WPA specification and IEEE 802.11i-2004 standard. This cipher also uses the Michael Message Integrity Code (MIC) algorithm for forgery protection.
			/// </summary>
			Tkip = 0x02,
			/// <summary>
			/// Specifies an AES-CCMP algorithm, as specified in the IEEE 802.11i-2004 standard and RFC 3610. Advanced Encryption Standard (AES) is the encryption algorithm defined in FIPS PUB 197.
			/// </summary>
			Ccmp = 0x04,
			/// <summary>
			/// Specifies a WEP cipher algorithm with a 104-bit cipher key.
			/// </summary>
			Wep104 = 0x05,
			/// <summary>
			/// Specifies a Robust Security Network (RSN) Use Group Key cipher suite. For more information about the Use Group Key cipher suite, refer to Clause 7.3.2.9.1 of the IEEE 802.11i-2004 standard.
			/// </summary>
			WpaUseGroup = 0x100,
			/// <summary>
			/// Specifies a Wifi Protected Access (WPA) Use Group Key cipher suite. For more information about the Use Group Key cipher suite, refer to Clause 7.3.2.9.1 of the IEEE 802.11i-2004 standard.
			/// </summary>
			RsnUseGroup = 0x100,
			/// <summary>
			/// Specifies a WEP cipher algorithm with a cipher key of any length.
			/// </summary>
			Wep = 0x101,
			/// <summary>
			/// Specifies the start of the range that is used to define proprietary cipher algorithms that are developed by an independent hardware vendor (IHV).
			/// </summary>
			IhvStart = 0x80000000,
			/// <summary>
			/// Specifies the end of the range that is used to define proprietary cipher algorithms that are developed by an IHV.
			/// </summary>
			IhvEnd = 0xffffffff
		}

		/// <summary>
		/// Defines the security attributes for a wireless connection.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_SECURITY_ATTRIBUTES</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential)]
		public struct WlanSecurityAttributes
		{
			/// <summary>
			/// Indicates whether security is enabled for this connection.
			/// </summary>
			[MarshalAs(UnmanagedType.Bool)]
			public bool SecurityEnabled;
			[MarshalAs(UnmanagedType.Bool)]
			public bool OneXEnabled;
			/// <summary>
			/// The authentication algorithm.
			/// </summary>
			public Dot11AuthAlgorithm Dot11AuthAlgorithm;
			/// <summary>
			/// The cipher algorithm.
			/// </summary>
			public Dot11CipherAlgorithm Dot11CipherAlgorithm;
		}

		/// <summary>
		/// Defines the attributes of a wireless connection.
		/// </summary>
		/// <remarks>
		/// Corresponds to the native <c>WLAN_CONNECTION_ATTRIBUTES</c> type.
		/// </remarks>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WlanConnectionAttributes
		{
			/// <summary>
			/// The state of the interface.
			/// </summary>
			public WlanInterfaceState IsState;
			/// <summary>
			/// The mode of the connection.
			/// </summary>
			public WlanConnectionMode WlanConnectionMode;
			/// <summary>
			/// The name of the profile used for the connection. Profile names are case-sensitive.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string ProfileName;
			/// <summary>
			/// The attributes of the association.
			/// </summary>
			public WlanAssociationAttributes WlanAssociationAttributes;
			/// <summary>
			/// The security attributes of the connection.
			/// </summary>
			public WlanSecurityAttributes WlanSecurityAttributes;
		}

		/// <summary>
		/// Contains information about a LAN interface.
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WlanInterfaceInfo
		{
			/// <summary>
			/// The GUID of the interface.
			/// </summary>
			public Guid InterfaceGuid;
			/// <summary>
			/// The description of the interface.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string InterfaceDescription;
			/// <summary>
			/// The current state of the interface.
			/// </summary>
			public WlanInterfaceState IsState;
		}

		/// <summary>
		/// The header of the list returned by <see cref="WlanEnumInterfaces"/>.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		internal struct WlanInterfaceInfoListHeader
		{
			public uint NumberOfItems;
			public uint Index;
		}

		/// <summary>
		/// The header of the list returned by <see cref="WlanGetProfileList"/>.
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		internal struct WlanProfileInfoListHeader
		{
			public uint NumberOfItems;
			public uint Index;
		}

		/// <summary>
		/// Contains basic information about a profile.
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
		public struct WlanProfileInfo
		{
			/// <summary>
			/// The name of the profile. This value may be the name of a domain if the profile is for provisioning. Profile names are case-sensitive.
			/// </summary>
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
			public string ProfileName;
			/// <summary>
			/// Profile flags.
			/// </summary>
			public WlanProfileFlags ProfileFlags;
		}

		/// <summary>
		/// Flags that specifiy the miniport driver's current operation mode.
		/// </summary>
		[Flags]
		public enum Dot11OperationMode : uint
		{
			Unknown = 0x00000000,
			Station = 0x00000001,
			Ap = 0x00000002,
			/// <summary>
			/// Specifies that the miniport driver supports the Extensible Station (ExtSTA) operation mode.
			/// </summary>
			ExtensibleStation = 0x00000004,
			/// <summary>
			/// Specifies that the miniport driver supports the Network Monitor (NetMon) operation mode.
			/// </summary>
			NetworkMonitor = 0x80000000
		}
		#endregion

		/// <summary>
		/// Helper method to wrap calls to Native WiFi API methods.
		/// If the method falls, throws an exception containing the error code.
		/// </summary>
		/// <param name="win32ErrorCode">The error code.</param>
		[DebuggerStepThrough]
		internal static void ThrowIfError(int win32ErrorCode)
		{
			if (win32ErrorCode != 0)
				throw new Win32Exception(win32ErrorCode);
		}
	}
}
