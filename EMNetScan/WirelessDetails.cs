////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//      http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;


namespace Inssider
{
   /// <summary>
   /// This is a simple data class that holds some basic
   /// data retrieved from the wireless interface
   /// </summary>
   public class WirelessDetails: IComparable
   {
      /// <summary>
      /// Creates a new instance.
      /// </summary>
      /// <param name="macAddress">raw mac address byte array</param>
      public WirelessDetails(byte[] macAddress)
      {
         _macAddress = new MacAddress(macAddress);
      }




#region Properties
      /// <summary>
      /// Gets or sets the network privacy information
      /// </summary>
      public string Privacy
      {
         get;
         set;
      }



      /// <summary>
      /// Gets the MAC address associated with this class
      /// </summary>
      public MacAddress MacAddress
      {
         get
         {
            return _macAddress;
         }
      }



      private readonly MacAddress _macAddress;
      /// <summary>
      /// Gets or sets the network Ssid
      /// </summary>
      public string Ssid
      {
         get;
         set;
      }



      /// <summary>
      /// Gets or sets the wifi network channel
      /// </summary>
      public uint Channel
      {
         get
         {
            return _channel;
         }
         set
         {
            _channel = value;
         }
      }



      private uint _channel;



      /// <summary>
      /// Gets or sets the RSSI (receive signal strength indication).
      /// </summary>
      public int Rssi
      {
         get
         {
            return _rssi;
         }
         set
         {
            _rssi = value;
         }
      }


      private int _rssi;



      /// <summary>
      /// Gets or sets indication of whether network is 802.11n
      /// </summary>
      public bool IsTypeN
      {
         get;
         set;
      }



      /// <summary>
      /// Gets or sets indication of whether the network uses channel bonding
      /// </summary>
      public bool IsChannelBonded
      {
         get;
         set;
      }



      /// <summary>
      /// Gets or sets the signal quality value
      /// </summary>
      public uint SignalQuality
      {
         get;
         set;
      }



      /// <summary>
      /// Gets or sets the supported data rates.
      /// </summary>
      public List < double > Rates
      {
         get
         {
            return _rates;
         }
      }



      private readonly List < double > _rates = new List < double > ();



      /// <summary>
      /// Gets or sets the string representation of 
      /// the network type.
      /// </summary>
      public string NetworkType
      {
         get;
         set;
      }



      /// <summary>
      /// Gets or sets the string representation of the
      /// infrastructure mode.
      /// </summary>
      public string InfrastructureMode
      {
         get;
         set;
      }
#endregion 




      /// <summary>
      /// Helper function to build a string representing
      /// the supported data rates. The format is like:
      /// 1/5.5/11/26/54
      /// </summary>
      /// <returns></returns>
      public string BuildRateString()
      {
         StringBuilder   sb = new StringBuilder();

         string separator = "";
         foreach(double rate in Rates)
         {
            sb.Append(separator);
            sb.Append(rate);
            separator = "/";
         }
         return (sb.ToString());
      }
      // End RateString()




#region IComparable Members
      public int CompareTo(object obj)
      {
         WirelessDetails details = (WirelessDetails) obj;

         if (details == this)
         {
            return 0;
         }
         else if (details.Channel == _channel)
         {
            return ((int) details.Rssi - (int) _rssi);
         }
         else 
         {
            return ((int) _channel - (int) details.Channel);
         }
      }
#endregion 
   }
}

