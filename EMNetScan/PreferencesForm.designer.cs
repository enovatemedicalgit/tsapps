namespace Inssider
{
    /// <summary>
    /// 
    /// </summary>
    partial class PreferencesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( PreferencesForm ) );
           this.inactiveNetworkTimeoutComboBox = new System.Windows.Forms.ComboBox();
           this.inactiveNetworkLabel = new System.Windows.Forms.Label();
           this.okButton = new System.Windows.Forms.Button();
           this.cancelButton = new System.Windows.Forms.Button();
           this.tabControl1 = new System.Windows.Forms.TabControl();
           this.tpGeneral = new System.Windows.Forms.TabPage();
           this.label9 = new System.Windows.Forms.Label();
           this.driverModeComboBox = new System.Windows.Forms.ComboBox();
           this.networkScanRateUpDown = new System.Windows.Forms.NumericUpDown();
           this.label8 = new System.Windows.Forms.Label();
           this.tpGPS = new System.Windows.Forms.TabPage();
           this.gpsEnableCheckBox = new System.Windows.Forms.CheckBox();
           this.logFolderBrowserButton = new System.Windows.Forms.Button();
           this.label11 = new System.Windows.Forms.Label();
           this.autoSaveUpDown = new System.Windows.Forms.NumericUpDown();
           this.autoSaveCheckBox = new System.Windows.Forms.CheckBox();
           this.logFolderNameBox = new System.Windows.Forms.TextBox();
           this.label10 = new System.Windows.Forms.Label();
           this.logCheckBox = new System.Windows.Forms.CheckBox();
           this.panel2 = new System.Windows.Forms.Panel();
           this.label2 = new System.Windows.Forms.Label();
           this.label5 = new System.Windows.Forms.Label();
           this.label4 = new System.Windows.Forms.Label();
           this.label7 = new System.Windows.Forms.Label();
           this.label6 = new System.Windows.Forms.Label();
           this.label3 = new System.Windows.Forms.Label();
           this.cbPorts = new System.Windows.Forms.ComboBox();
           this.cbParity = new System.Windows.Forms.ComboBox();
           this.cbData = new System.Windows.Forms.ComboBox();
           this.cbFlow = new System.Windows.Forms.ComboBox();
           this.cbStop = new System.Windows.Forms.ComboBox();
           this.cbBaud = new System.Windows.Forms.ComboBox();
           this.panel1 = new System.Windows.Forms.Panel();
           this.logFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
           this.tabControl1.SuspendLayout();
           this.tpGeneral.SuspendLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.networkScanRateUpDown ) ).BeginInit();
           this.tpGPS.SuspendLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.autoSaveUpDown ) ).BeginInit();
           this.SuspendLayout();
           // 
           // inactiveNetworkTimeoutComboBox
           // 
           this.inactiveNetworkTimeoutComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
           this.inactiveNetworkTimeoutComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
           this.inactiveNetworkTimeoutComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.inactiveNetworkTimeoutComboBox.FormattingEnabled = true;
           this.inactiveNetworkTimeoutComboBox.Items.AddRange( new object[] {
            resources.GetString("inactiveNetworkTimeoutComboBox.Items"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items1"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items2"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items3"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items4"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items5"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items6"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items7"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items8"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items9"),
            resources.GetString("inactiveNetworkTimeoutComboBox.Items10")} );
           resources.ApplyResources( this.inactiveNetworkTimeoutComboBox, "inactiveNetworkTimeoutComboBox" );
           this.inactiveNetworkTimeoutComboBox.Name = "inactiveNetworkTimeoutComboBox";
           // 
           // inactiveNetworkLabel
           // 
           resources.ApplyResources( this.inactiveNetworkLabel, "inactiveNetworkLabel" );
           this.inactiveNetworkLabel.Name = "inactiveNetworkLabel";
           // 
           // okButton
           // 
           resources.ApplyResources( this.okButton, "okButton" );
           this.okButton.Name = "okButton";
           this.okButton.UseVisualStyleBackColor = true;
           this.okButton.Click += new System.EventHandler( this.OkButton_Click );
           // 
           // cancelButton
           // 
           resources.ApplyResources( this.cancelButton, "cancelButton" );
           this.cancelButton.Name = "cancelButton";
           this.cancelButton.UseVisualStyleBackColor = true;
           this.cancelButton.Click += new System.EventHandler( this.CancelButton_Click );
           // 
           // tabControl1
           // 
           this.tabControl1.Controls.Add( this.tpGeneral );
           this.tabControl1.Controls.Add( this.tpGPS );
           resources.ApplyResources( this.tabControl1, "tabControl1" );
           this.tabControl1.Name = "tabControl1";
           this.tabControl1.SelectedIndex = 0;
           // 
           // tpGeneral
           // 
           this.tpGeneral.Controls.Add( this.label9 );
           this.tpGeneral.Controls.Add( this.driverModeComboBox );
           this.tpGeneral.Controls.Add( this.networkScanRateUpDown );
           this.tpGeneral.Controls.Add( this.label8 );
           this.tpGeneral.Controls.Add( this.inactiveNetworkLabel );
           this.tpGeneral.Controls.Add( this.inactiveNetworkTimeoutComboBox );
           resources.ApplyResources( this.tpGeneral, "tpGeneral" );
           this.tpGeneral.Name = "tpGeneral";
           this.tpGeneral.UseVisualStyleBackColor = true;
           // 
           // label9
           // 
           resources.ApplyResources( this.label9, "label9" );
           this.label9.Name = "label9";
           // 
           // driverModeComboBox
           // 
           this.driverModeComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
           this.driverModeComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
           this.driverModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.driverModeComboBox.FormattingEnabled = true;
           this.driverModeComboBox.Items.AddRange( new object[] {
            resources.GetString("driverModeComboBox.Items"),
            resources.GetString("driverModeComboBox.Items1"),
            resources.GetString("driverModeComboBox.Items2")} );
           resources.ApplyResources( this.driverModeComboBox, "driverModeComboBox" );
           this.driverModeComboBox.Name = "driverModeComboBox";
           // 
           // networkScanRateUpDown
           // 
           resources.ApplyResources( this.networkScanRateUpDown, "networkScanRateUpDown" );
           this.networkScanRateUpDown.Maximum = new decimal( new int[] {
            15,
            0,
            0,
            0} );
           this.networkScanRateUpDown.Minimum = new decimal( new int[] {
            1,
            0,
            0,
            0} );
           this.networkScanRateUpDown.Name = "networkScanRateUpDown";
           this.networkScanRateUpDown.Value = new decimal( new int[] {
            2,
            0,
            0,
            0} );
           // 
           // label8
           // 
           resources.ApplyResources( this.label8, "label8" );
           this.label8.Name = "label8";
           // 
           // tpGPS
           // 
           this.tpGPS.Controls.Add( this.gpsEnableCheckBox );
           this.tpGPS.Controls.Add( this.logFolderBrowserButton );
           this.tpGPS.Controls.Add( this.label11 );
           this.tpGPS.Controls.Add( this.autoSaveUpDown );
           this.tpGPS.Controls.Add( this.autoSaveCheckBox );
           this.tpGPS.Controls.Add( this.logFolderNameBox );
           this.tpGPS.Controls.Add( this.label10 );
           this.tpGPS.Controls.Add( this.logCheckBox );
           this.tpGPS.Controls.Add( this.panel2 );
           this.tpGPS.Controls.Add( this.label2 );
           this.tpGPS.Controls.Add( this.label5 );
           this.tpGPS.Controls.Add( this.label4 );
           this.tpGPS.Controls.Add( this.label7 );
           this.tpGPS.Controls.Add( this.label6 );
           this.tpGPS.Controls.Add( this.label3 );
           this.tpGPS.Controls.Add( this.cbPorts );
           this.tpGPS.Controls.Add( this.cbParity );
           this.tpGPS.Controls.Add( this.cbData );
           this.tpGPS.Controls.Add( this.cbFlow );
           this.tpGPS.Controls.Add( this.cbStop );
           this.tpGPS.Controls.Add( this.cbBaud );
           this.tpGPS.Controls.Add( this.panel1 );
           resources.ApplyResources( this.tpGPS, "tpGPS" );
           this.tpGPS.Name = "tpGPS";
           this.tpGPS.UseVisualStyleBackColor = true;
           // 
           // gpsEnableCheckBox
           // 
           resources.ApplyResources( this.gpsEnableCheckBox, "gpsEnableCheckBox" );
           this.gpsEnableCheckBox.Name = "gpsEnableCheckBox";
           this.gpsEnableCheckBox.UseVisualStyleBackColor = true;
           this.gpsEnableCheckBox.CheckedChanged += new System.EventHandler( this.GpsEnableCheckBox_CheckedChanged );
           // 
           // logFolderBrowserButton
           // 
           resources.ApplyResources( this.logFolderBrowserButton, "logFolderBrowserButton" );
           this.logFolderBrowserButton.Name = "logFolderBrowserButton";
           this.logFolderBrowserButton.UseVisualStyleBackColor = true;
           this.logFolderBrowserButton.Click += new System.EventHandler( this.LogFolderBrowserButton_Click );
           // 
           // label11
           // 
           resources.ApplyResources( this.label11, "label11" );
           this.label11.Name = "label11";
           // 
           // autoSaveUpDown
           // 
           resources.ApplyResources( this.autoSaveUpDown, "autoSaveUpDown" );
           this.autoSaveUpDown.Maximum = new decimal( new int[] {
            32000,
            0,
            0,
            0} );
           this.autoSaveUpDown.Minimum = new decimal( new int[] {
            10,
            0,
            0,
            0} );
           this.autoSaveUpDown.Name = "autoSaveUpDown";
           this.autoSaveUpDown.Value = new decimal( new int[] {
            60,
            0,
            0,
            0} );
           // 
           // autoSaveCheckBox
           // 
           resources.ApplyResources( this.autoSaveCheckBox, "autoSaveCheckBox" );
           this.autoSaveCheckBox.Checked = true;
           this.autoSaveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.autoSaveCheckBox.Name = "autoSaveCheckBox";
           this.autoSaveCheckBox.UseVisualStyleBackColor = true;
           // 
           // logFolderNameBox
           // 
           resources.ApplyResources( this.logFolderNameBox, "logFolderNameBox" );
           this.logFolderNameBox.Name = "logFolderNameBox";
           this.logFolderNameBox.ReadOnly = true;
           // 
           // label10
           // 
           resources.ApplyResources( this.label10, "label10" );
           this.label10.Name = "label10";
           // 
           // logCheckBox
           // 
           resources.ApplyResources( this.logCheckBox, "logCheckBox" );
           this.logCheckBox.Checked = true;
           this.logCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
           this.logCheckBox.Name = "logCheckBox";
           this.logCheckBox.UseVisualStyleBackColor = true;
           this.logCheckBox.CheckedChanged += new System.EventHandler( this.LogCheckBox_CheckedChanged );
           // 
           // panel2
           // 
           this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
           resources.ApplyResources( this.panel2, "panel2" );
           this.panel2.MinimumSize = new System.Drawing.Size( 4, 4 );
           this.panel2.Name = "panel2";
           // 
           // label2
           // 
           resources.ApplyResources( this.label2, "label2" );
           this.label2.Name = "label2";
           // 
           // label5
           // 
           resources.ApplyResources( this.label5, "label5" );
           this.label5.Name = "label5";
           // 
           // label4
           // 
           resources.ApplyResources( this.label4, "label4" );
           this.label4.Name = "label4";
           // 
           // label7
           // 
           resources.ApplyResources( this.label7, "label7" );
           this.label7.Name = "label7";
           // 
           // label6
           // 
           resources.ApplyResources( this.label6, "label6" );
           this.label6.Name = "label6";
           // 
           // label3
           // 
           resources.ApplyResources( this.label3, "label3" );
           this.label3.Name = "label3";
           // 
           // cbPorts
           // 
           this.cbPorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbPorts.FormattingEnabled = true;
           resources.ApplyResources( this.cbPorts, "cbPorts" );
           this.cbPorts.Name = "cbPorts";
           // 
           // cbParity
           // 
           this.cbParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbParity.FormattingEnabled = true;
           this.cbParity.Items.AddRange( new object[] {
            resources.GetString("cbParity.Items"),
            resources.GetString("cbParity.Items1"),
            resources.GetString("cbParity.Items2"),
            resources.GetString("cbParity.Items3"),
            resources.GetString("cbParity.Items4")} );
           resources.ApplyResources( this.cbParity, "cbParity" );
           this.cbParity.Name = "cbParity";
           // 
           // cbData
           // 
           this.cbData.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbData.FormattingEnabled = true;
           this.cbData.Items.AddRange( new object[] {
            resources.GetString("cbData.Items"),
            resources.GetString("cbData.Items1"),
            resources.GetString("cbData.Items2"),
            resources.GetString("cbData.Items3")} );
           resources.ApplyResources( this.cbData, "cbData" );
           this.cbData.Name = "cbData";
           // 
           // cbFlow
           // 
           this.cbFlow.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbFlow.FormattingEnabled = true;
           this.cbFlow.Items.AddRange( new object[] {
            resources.GetString("cbFlow.Items"),
            resources.GetString("cbFlow.Items1"),
            resources.GetString("cbFlow.Items2")} );
           resources.ApplyResources( this.cbFlow, "cbFlow" );
           this.cbFlow.Name = "cbFlow";
           // 
           // cbStop
           // 
           this.cbStop.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbStop.FormattingEnabled = true;
           this.cbStop.Items.AddRange( new object[] {
            resources.GetString("cbStop.Items"),
            resources.GetString("cbStop.Items1"),
            resources.GetString("cbStop.Items2"),
            resources.GetString("cbStop.Items3")} );
           resources.ApplyResources( this.cbStop, "cbStop" );
           this.cbStop.Name = "cbStop";
           // 
           // cbBaud
           // 
           this.cbBaud.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
           this.cbBaud.FormattingEnabled = true;
           this.cbBaud.Items.AddRange( new object[] {
            resources.GetString("cbBaud.Items"),
            resources.GetString("cbBaud.Items1"),
            resources.GetString("cbBaud.Items2"),
            resources.GetString("cbBaud.Items3"),
            resources.GetString("cbBaud.Items4"),
            resources.GetString("cbBaud.Items5"),
            resources.GetString("cbBaud.Items6"),
            resources.GetString("cbBaud.Items7"),
            resources.GetString("cbBaud.Items8"),
            resources.GetString("cbBaud.Items9"),
            resources.GetString("cbBaud.Items10"),
            resources.GetString("cbBaud.Items11"),
            resources.GetString("cbBaud.Items12"),
            resources.GetString("cbBaud.Items13"),
            resources.GetString("cbBaud.Items14")} );
           resources.ApplyResources( this.cbBaud, "cbBaud" );
           this.cbBaud.Name = "cbBaud";
           // 
           // panel1
           // 
           this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
           resources.ApplyResources( this.panel1, "panel1" );
           this.panel1.MinimumSize = new System.Drawing.Size( 4, 4 );
           this.panel1.Name = "panel1";
           // 
           // PreferencesForm
           // 
           resources.ApplyResources( this, "$this" );
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.Controls.Add( this.tabControl1 );
           this.Controls.Add( this.cancelButton );
           this.Controls.Add( this.okButton );
           this.MaximizeBox = false;
           this.MinimizeBox = false;
           this.Name = "PreferencesForm";
           this.Load += new System.EventHandler( this.PreferencesForm_Load );
           this.tabControl1.ResumeLayout( false );
           this.tpGeneral.ResumeLayout( false );
           this.tpGeneral.PerformLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.networkScanRateUpDown ) ).EndInit();
           this.tpGPS.ResumeLayout( false );
           this.tpGPS.PerformLayout();
           ( (System.ComponentModel.ISupportInitialize)( this.autoSaveUpDown ) ).EndInit();
           this.ResumeLayout( false );

        }

        #endregion

        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox inactiveNetworkTimeoutComboBox;
        private System.Windows.Forms.Label inactiveNetworkLabel;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpGeneral;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cbBaud;
        public System.Windows.Forms.ComboBox cbPorts;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.ComboBox cbData;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.ComboBox cbParity;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.ComboBox cbStop;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbFlow;
        public System.Windows.Forms.TabPage tpGPS;
        private System.Windows.Forms.NumericUpDown networkScanRateUpDown;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox driverModeComboBox;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox logFolderNameBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox logCheckBox;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown autoSaveUpDown;
        private System.Windows.Forms.CheckBox autoSaveCheckBox;
        private System.Windows.Forms.Button logFolderBrowserButton;
        private System.Windows.Forms.FolderBrowserDialog logFolderBrowserDialog;
        private System.Windows.Forms.CheckBox gpsEnableCheckBox;
    }
}