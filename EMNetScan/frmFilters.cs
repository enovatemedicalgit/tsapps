﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Inssider
{
    public partial class frmFilters : Form
    {
        internal Filter NewFilter;
        private string[] IntList = new string[] { ">", "<", "Equal", "Not Equal" };
        private string[] StringList = new string[] { "Equal", "Contains", "Starts With", "Ends With", "Not Equal", "Not Contains", "Not Starts With", "Not Ends With" };
        private string[] BothList = new string[] { ">", "<", "Equal", "Contains", "Starts With", "Ends With", "Not Equal", "Not Contains", "Not Starts With", "Not Ends With" };
        //Not
        //private string[] NIntList = new string[] { ">", "<", "Not Equal" };
        //private string[] NStringList = new string[] { "Not Equal", "Not Contains", "Not Starts With", "Not Ends With" };
        //private string[] NBothList = new string[] { ">", "<", "Not Equal", "Not Contains", "Not Starts With", "Not Ends With" };
        private int indexOffset = 0;

        public frmFilters()
        {
            InitializeComponent();
        }

        public frmFilters(Filter.CompareUnit Unit, Filter.CompareMethod Method, object value, bool Inverted)
        {
            InitializeComponent();
            cbUnit.SelectedIndex = (int)Unit;
            cbMethod.Items.Clear();

            if (Unit == Filter.CompareUnit.MAC || Unit == Filter.CompareUnit.SSID)
            {
                cbMethod.Items.AddRange(StringList);
                indexOffset = 2;
            }
            else
            {
                cbMethod.Items.AddRange(IntList);
                indexOffset = 0;
            }

            cbMethod.SelectedIndex = (int)Method - indexOffset;
            if (Inverted)
            {
                if (indexOffset == 2) //String list
                {
                    cbMethod.SelectedIndex += 4;
                }
                else if (indexOffset == 0) //Int list
                {
                    cbMethod.SelectedIndex = 3;
                }
            }

            if (Unit == Filter.CompareUnit.Security)
            {
                cbValue.SelectedIndex = (int)value;
            }
            else
            {
                txtValue.Text = value.ToString();
            }
            chbInvert.Checked = Inverted;
            btnAdd.Text = Localizer.GetString("ApplyString");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            //public enum CompareUnit { RSSI, SSID, Channel, Security }
            //public enum CompareMethod { GreaterThan, Equal, LessThan, Contains, StartWith, EndsWith }

            if ((Filter.CompareUnit)cbUnit.SelectedIndex == Filter.CompareUnit.MAC)
            {
                //Condition the MAC Address for compare.
                string valStr = txtValue.Text.ToUpper();
                if (!valStr.Contains(":"))
                {
                    StringBuilder sb = new StringBuilder();
                    string separator = "";
                    for (int i = 0; i < valStr.Length; i += 2)
                    {
                        sb.Append(separator);
                        sb.Append(valStr[i]);
                        if(i+1 < valStr.Length)
                            sb.Append(valStr[i+1]);
                        separator = ":";
                    }
                    txtValue.Text = sb.ToString();
                }
            }

            if (indexOffset == 2 && cbMethod.SelectedIndex > 3) //String list
            {
                chbInvert.Checked = true;
                cbMethod.SelectedIndex -= 4; 
            }
            else if (indexOffset == 0 && cbMethod.SelectedIndex > 2) //Int list
            {
                chbInvert.Checked = true;
                cbMethod.SelectedIndex -= 1;
            }

            if (txtValue.Visible)
            {
                NewFilter = new Filter((Filter.CompareUnit)cbUnit.SelectedIndex, (Filter.CompareMethod)(cbMethod.SelectedIndex + indexOffset), txtValue.Text, chbInvert.Checked);
            }
            else
            {
                //The security dropdown was selected
                NewFilter = new Filter((Filter.CompareUnit)cbUnit.SelectedIndex, (Filter.CompareMethod)(cbMethod.SelectedIndex + indexOffset), (Filter.SecurityTypes)cbValue.SelectedIndex, chbInvert.Checked);
            }
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void cbUnit_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbMethod.Items.Clear();
            if (cbUnit.SelectedIndex == 1 || cbUnit.SelectedIndex == 5)
            {
                cbMethod.Items.AddRange(StringList);
                indexOffset = 2;
            }
            else
            {
                cbMethod.Items.AddRange(IntList);
                indexOffset = 0;
            }

            if (cbUnit.SelectedIndex == 3) //The Security unit
            {
                txtValue.Visible = false;
                cbValue.Location = txtValue.Location;
                cbValue.Visible = true;
            }
            else
            {
                txtValue.Visible = true;
                cbValue.Visible = false;
            }

            cbMethod.Enabled = true;
            txtValue.Enabled = true;
        }
    }
}
