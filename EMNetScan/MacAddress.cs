////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Text;

namespace Inssider
{
    /// <summary>
    /// This class represents a MAC address of a network
    /// device.
    /// </summary>
    public class MacAddress
    {        
        // individual bytes of the mac address
        private readonly byte[] _bytes;

        #region Constructors

        /// <summary>
        /// Hidden constructor. MacAddress objects should be
        /// created using the Create method, or by
        /// </summary>
        private MacAddress() {}

        public MacAddress( byte[] bytes ) {
            if (null == bytes || bytes.Length == 0) {
                throw new ArgumentException("Invalid byte array argument.");
            }
            _bytes = new byte[bytes.Length];
            Array.Copy(bytes, _bytes, bytes.Length);
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets an array of bytes representing the raw MAC
        /// address value.
        /// </summary>
        public byte[] Bytes {
            get {
                return (byte[])_bytes.Clone();
            }
        }

        /// <summary>
        /// Gets an indexed byte of the mac address
        /// </summary>
        /// <param name="index">index of the byte to return</param>
        /// <returns>byte value</returns>
        public byte this[int index] {
            get { return _bytes[index]; }
        }

        /// <summary>
        /// Number of bytes that make up the address.
        /// </summary>
        public int Length {
            get { return _bytes.Length; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Converts the MacAddress object to a string. The format
        /// of the string will be like the following:
        /// A1:2B:3C:4D:5E:6F
        /// </summary>
        /// <returns>human readable form of the mac address</returns>
        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            string separator = "";
            for (int i = 0; i < _bytes.Length; i++) {
                sb.Append(separator);
                sb.AppendFormat("{0:x2}", _bytes[i]);
                separator = ":";
            }
            return (sb.ToString().ToUpper());
        }

        /// <summary>
        /// Determines if this object is equal to another
        /// MacAddress object.
        /// </summary>
        /// <param name="obj">a MacAddress object to compare with the current object</param>
        /// <returns>true if the two objects are equal, false otherwise.</returns>
        public override bool Equals(object obj) {
            bool result = false;
            if (obj is MacAddress) {
                MacAddress mac = (MacAddress)obj;
                result = mac._bytes.Length == _bytes.Length;
                for (int i = 0; (i < _bytes.Length) && result; ++i) {
                    result &= mac._bytes[i] == _bytes[i];
                }            
            }
            return result;
        }

        /// <summary>
        /// Hash function for this type.
        /// </summary>
        /// <returns>integer hash value</returns>
        public override int GetHashCode() {
            return base.GetHashCode();
        }

        #endregion
    }
}
