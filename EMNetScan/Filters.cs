﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Inssider
{
    public class Filter
    {
        public enum CompareUnit { RSSI, SSID, Channel, Security, Speed, MAC }
        public enum CompareMethod { GreaterThan, LessThan, Equal, Contains, StartWith, EndsWith }
        private enum ObjectType { String, Int };
        internal enum SecurityTypes { None, WEP, WPA_TKIP, WPA_CCMP, WPA2_TKIP, WPA2_CCMP };

        private CompareUnit _unit;
        private CompareMethod _comp;
        private object _value;
        private bool _active = true;
        private bool _inverted = false;

        public Filter(CompareUnit CompareUnit, CompareMethod CompareMethod, object Value)
        {
            _unit = CompareUnit;
            _comp = CompareMethod;
            _value = Value;
        }

        public Filter(CompareUnit CompareUnit, CompareMethod CompareMethod, object Value, bool Inverted)
        {
            _unit = CompareUnit;
            _comp = CompareMethod;
            _value = Value;
            _inverted = Inverted;
        }

        public bool FilterOut(ScannerNetwork Ap)
        {
            ObjectType _type;
            Type tp = typeof(ScannerNetwork);
            PropertyInfo fi;
            switch (_unit)
            {
                case CompareUnit.RSSI:
                    fi = tp.GetProperty("Signal");
                    _type = ObjectType.Int;
                    break;
                case CompareUnit.SSID:
                    fi = tp.GetProperty("SSID");
                    _type = ObjectType.String;
                    break;
                case CompareUnit.Channel:
                    fi = tp.GetProperty("Channel");
                    _type = ObjectType.Int;
                    break;
                case CompareUnit.Security:
                    fi = tp.GetProperty("Security");
                    _type = ObjectType.Int;
                    break;
                case CompareUnit.Speed:
                    fi = tp.GetProperty("Speed");
                    _type = ObjectType.Int;
                    break;
                case CompareUnit.MAC:
                    fi = tp.GetProperty("MACAddressStringXX");
                    _type = ObjectType.String;
                    break;
                default:
                    fi = null;
                    _type = ObjectType.String;
                    break;
            }

            switch (_type)
            {
                case ObjectType.String:
                    string strValComp = fi.GetValue(Ap, null).ToString().ToLower();
                    if (_inverted)
                    {
                        if (_comp == CompareMethod.Contains && !strValComp.Contains(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.StartWith && !strValComp.StartsWith(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.EndsWith && !strValComp.EndsWith(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.Equal && strValComp != _value.ToString().ToLower()) return true;
                    }
                    else
                    {
                        if (_comp == CompareMethod.Contains && strValComp.Contains(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.StartWith && strValComp.StartsWith(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.EndsWith && strValComp.EndsWith(_value.ToString().ToLower())) return true;
                        if (_comp == CompareMethod.Equal && strValComp == _value.ToString().ToLower()) return true;
                    }
                    break;
                case ObjectType.Int:
                    _value = Convert.ToInt32(_value);
                    int intValComp;
                    if (_unit != CompareUnit.Security)
                    {
                        intValComp = Convert.ToInt32(fi.GetValue(Ap, null));
                    }
                    else
                    {
                        intValComp = RankSec(fi.GetValue(Ap, null).ToString());
                    }

                    if (_comp == CompareMethod.GreaterThan && intValComp > (int)_value) return true;
                    if (_comp == CompareMethod.LessThan && intValComp < (int)_value) return true;
                    if (_comp == CompareMethod.Equal && _inverted && intValComp != (int)_value) return true;
                    if (_comp == CompareMethod.Equal && !_inverted && intValComp == (int)_value) return true;
                    break;
                default:
                    break;
            }
            //int chan = (int)fi.GetValue(Ap, null);
            //if (chan < -75)
            //{ return true; }

            //foreach (System.Reflection.MemberInfo chan in fis)
            //{
            //Console.WriteLine(chan);
            //}

            return false;
        }

        //None, WEP, WPA_TKIP, WPA_CCMP, WPA2_TKIP, WPA2_CCMP
        private int RankSec(string Security)
        {
            if (Security.ToLower().Contains("tkip"))
            {
                if (Security.ToLower().Contains("wpa2") || Security.ToLower().Contains("rsna"))
                {
                    return 4;
                }
                else if (Security.ToLower().Contains("wpa"))
                {
                    return 2;
                }
            }
            else if (Security.ToLower().Contains("ccmp") || Security.ToLower().Contains("aes"))
            {
                if (Security.ToLower().Contains("wpa2") || Security.ToLower().Contains("rsna"))
                {
                    return 5;
                }
                else if (Security.ToLower().Contains("wpa"))
                {
                    return 3;
                }
            }
            else if (Security.ToLower().StartsWith("wep")) return 1;
            else if (Security.ToLower().StartsWith("none")) return 0;

            return 0;
        }

        public bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public bool Inverted
        {
            get { return _inverted; }
            set { _inverted = value; }
        }

        public CompareUnit Unit
        {
            get { return _unit; }
            set { _unit = value; }
        }

        public CompareMethod Method
        {
            get { return _comp; }
            set { _comp = value; }
        }

        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
