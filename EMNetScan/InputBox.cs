﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;


namespace Inssider
{
    public partial class InputBox : Telerik.WinControls.UI.ShapedForm
    {
        public string selectedFile { get; set; }
        public bool bUseDepartment { get; set; }
             public string SiteID { get; set; }
        public InputBox()
        {
            InitializeComponent();
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
              // Create an instance of the open file dialog box.
            if (ddlSites.SelectedIndex == 0)
            {
                MessageBox.Show("Please Select a Site");
                return;
            }
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
          
            // Set filter options and filter index.
            openFileDialog1.Title = "Select NS1 Scan File For Selected Site/Facility";
            openFileDialog1.Filter = "NS1 Files (.ns1)|*.ns1";
            openFileDialog1.FilterIndex = 1;
            
            // Process input if the user clicked OK.
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                 selectedFile =  openFileDialog1.FileName.ToString();
                // Open the selected file to read.
                //System.IO.Stream fileStream = openFileDialog1.OpenFile();
                //using (System.IO.StreamReader reader = new System.IO.StreamReader(fileStream))
                //{
                //    // Read the first line from the file and write it the textbox.
                //    selectedFile = reader.ReadLine();
                //}
                //fileStream.Close();
            }
              SiteID = ddlSites.SelectedValue.ToString();
        }

        private void InputBox_Load(object sender, EventArgs e)
        {
            try
            {
            
                ApiLayer api = new ApiLayer();
                DataTable dt = new DataTable();

                Dictionary<string, string> parameters = new Dictionary<string, string>() {
                { "@IDSite", "4" } };

                dt = api.ExecSpApi("prcSitesSelect", parameters);
                if (dt.Rows.Count > 1)
                {
                       ddlSites.DataSource = dt;
                    ddlSites.SelectedIndex = 0;
                    ddlSites.DisplayMember =  "SiteDescription";
                    ddlSites.ValueMember = "IDSite";
                  
                }
            }
            catch (Exception ex)
            {

            }


             
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();

        }

        private void ddlSites_SelectedIndexChanged(object sender, EventArgs e)
        {
              ApiLayer api2 = new ApiLayer();
               DataTable dt2 = new DataTable();
                Dictionary<string, string> parameters2 = new Dictionary<string, string>() {
                { "@SiteID", ddlSites.SelectedValue.ToString() } };
               SiteID = ddlSites.SelectedValue.ToString();
                //dt2 = api2.ExecSpApi("prcDepartmentsSelectBySite", parameters2);
                //if (dt2.Rows.Count > 0)
                //{
                //    cbDepartment.DataSource = dt2;
                //    cbDepartment.SelectedValue = null;
                //    cbDepartment.Text = "Select a Department";
                //}
             
        }

        private void cbDepartment_Click(object sender, EventArgs e)
        {
                 bUseDepartment = true;
        }

    }
}
