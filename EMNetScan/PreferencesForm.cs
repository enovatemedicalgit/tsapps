////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;
using Inssider.Properties;
using System.IO;

namespace Inssider
{
    internal partial class PreferencesForm : Form
    {
        /// <summary>
        /// 
        /// </summary>
        internal PreferencesForm()
        {
            InitializeComponent();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            switch (inactiveNetworkTimeoutComboBox.SelectedIndex)
            {
                case 0:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.Zero;
                    break;
                case 1:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromSeconds(5);
                    break;

                case 2:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromSeconds(10);
                    break;

                case 3:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromSeconds(15);
                    break;

                case 4:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromSeconds(30);
                    break;

                case 5:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromSeconds(45);
                    break;

                case 6:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromMinutes(1);
                    break;

                case 7:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromMinutes(5);
                    break;

                case 8:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromMinutes(15);
                    break;

                case 9:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromMinutes(30);
                    break;

                case 10:
                    Settings.Default.InactiveNetworkTimeout = TimeSpan.FromMinutes(60);
                    break;
            }

            // convert from seconds to msec
            Settings.Default.NetworkScanRate = (int) networkScanRateUpDown.Value*1000;

            if (Settings.Default.driverMode != driverModeComboBox.SelectedIndex)
            {
                MessageBox.Show(
                    "The scan must be manually stopped & restarted \nfor the new driver mode to take effect.\n\nIf the new settings fail, the mode will be automatically reset to 'Auto'");
                Settings.Default.driverMode = driverModeComboBox.SelectedIndex;
            }

            Settings.Default.isLogging = logCheckBox.Checked;
            Settings.Default.LogFolder = logFolderNameBox.Text;

            Settings.Default.isAutoSaving = autoSaveCheckBox.Checked;
            Settings.Default.autoSaveRate = (int) Math.Round(autoSaveUpDown.Value);

            //GPS support
            Settings.Default.gpsEnabled = gpsEnableCheckBox.Checked;

            if (gpsEnableCheckBox.Checked)
            {
                try
                {
                    Settings.Default.GPSPort = cbPorts.Text;
                }
                catch
                {
                }

                try
                {
                    Settings.Default.GPSPortBaud = Convert.ToInt32(cbBaud.Text);
                }
                catch
                {
                }

                try
                {
                    Settings.Default.GPSPortDataBits = Convert.ToInt32(cbData.Text);
                }
                catch
                {
                }

                try
                {
                    switch (cbStop.SelectedIndex)
                    {
                        case 0:
                            Settings.Default.GPSPortStopBits = System.IO.Ports.StopBits.None;
                            break;

                        case 1:
                            Settings.Default.GPSPortStopBits = System.IO.Ports.StopBits.One;
                            break;

                        case 2:
                            Settings.Default.GPSPortStopBits = System.IO.Ports.StopBits.OnePointFive;
                            break;

                        case 3:
                            Settings.Default.GPSPortStopBits = System.IO.Ports.StopBits.Two;
                            break;
                    }
                }
                catch
                {
                }

                try
                {
                    //Settings.Default.GPSPortParity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), cbParity.Text, true);

                    switch (cbParity.Text)
                    {
                        case "None":
                            Settings.Default.GPSPortParity = System.IO.Ports.Parity.None;
                            break;

                        case "Odd":
                            Settings.Default.GPSPortParity = System.IO.Ports.Parity.Odd;
                            break;

                        case "Even":
                            Settings.Default.GPSPortParity = System.IO.Ports.Parity.Even;
                            break;

                        case "Mark":
                            Settings.Default.GPSPortParity = System.IO.Ports.Parity.Mark;
                            break;

                        case "Space":
                            Settings.Default.GPSPortParity = System.IO.Ports.Parity.Space;
                            break;
                    }
                }
                catch
                {
                }

                try
                {
                    switch (cbFlow.SelectedIndex)
                    {
                        case 0:
                            Settings.Default.GPSFlowControl = System.IO.Ports.Handshake.None;
                            break;

                        case 1:
                            Settings.Default.GPSFlowControl = System.IO.Ports.Handshake.RequestToSend;
                            break;

                        case 2:
                            Settings.Default.GPSFlowControl = System.IO.Ports.Handshake.XOnXOff;
                            break;
                    }
                }
                catch
                {
                }
            }
            try
            {
                //We need to save the settings before closing the window!
                Settings.Default.Save();
            }
            catch (SystemException)
            {
                MessageBox.Show("A setting could not be saved, please try again", "Error", MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
#if DEBUG
                Console.WriteLine("GPS support is : {0}", Settings.Default.gpsEnabled ? "Enabled" : "Disabled");
#endif

            DialogResult = DialogResult.OK;
            Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            // Return preferences panel to original state 
            Close();
        }

        private void PreferencesForm_Load(object sender, EventArgs e)
        {
#if DEBUG
            Console.WriteLine("Preferences form load - loading settings");
#endif
            try
            {
                switch ((int)Settings.Default.InactiveNetworkTimeout.TotalSeconds)
                {
                    case 0:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 0;
                        break;

                    case 5:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 1;
                        break;

                    case 10:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 2;
                        break;

                    case 15:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 3;
                        break;

                    case 30:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 4;
                        break;

                    case 45:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 5;
                        break;

                    case 60: //1Min
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 6;
                        break;

                    case 300: //5Min
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 7;
                        break;

                    case 900: //15Min
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 8;
                        break;

                    case 1800: //30Min
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 9;
                        break;

                    case 3600: //1Hour
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 10;
                        break;

                    default:
                        inactiveNetworkTimeoutComboBox.SelectedIndex = 0;
                        break;
                }
            }
            catch
            {
                // if anything went wrong accessing the settings, just default to NEVER
                inactiveNetworkTimeoutComboBox.SelectedIndex = 0;
            }

            try
            {
                // convert msec to sec
                networkScanRateUpDown.Value = Settings.Default.NetworkScanRate / 1000;
            }
            catch
            {
                // default to 1 second
                networkScanRateUpDown.Value = 1;
            }

            try
            {
                driverModeComboBox.SelectedIndex = Settings.Default.driverMode;
            }
            catch
            {
                // default to AUTO
                driverModeComboBox.SelectedIndex = 0;
            }

            try
            {
                logCheckBox.Checked = Settings.Default.isLogging;
            }
            catch
            {
                // default to no GPX logging
                logCheckBox.Checked = false;
            }

            try
            {
                // Set Log Folder to last-used folder, else set to My Documents...
                if ((string.Empty != Settings.Default.LogFolder) && (Directory.Exists(Settings.Default.LogFolder)))
                {
                    logFolderNameBox.Text = Settings.Default.LogFolder;
                }
                else
                {
                    logFolderNameBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                }
            }
            catch
            {
                // if the filename is screwed up, don't enable logging
                logCheckBox.Checked = false;
            }
            logFolderNameBox.Enabled = logCheckBox.Checked;

            try
            {
                autoSaveCheckBox.Checked = Settings.Default.isAutoSaving;
            }
            catch
            {
                // default to no autosave
                autoSaveCheckBox.Checked = false;
            }

            try
            {
                autoSaveUpDown.Value = Settings.Default.autoSaveRate;
            }
            catch
            {
                // default to one minute
                autoSaveUpDown.Value = 60;
            }

            try
            {
                gpsEnableCheckBox.Checked = Settings.Default.gpsEnabled;
#if DEBUG
                Console.WriteLine("GPS support is : {0}", Settings.Default.gpsEnabled ? "Enabled" : "Disabled");
                Console.WriteLine("Avalible Com ports:");
#endif
                foreach (string port in System.IO.Ports.SerialPort.GetPortNames())
                {
                    cbPorts.Items.Add(port);
#if DEBUG
                    Console.WriteLine(port);
#endif
                }

                cbPorts.SelectedIndex = cbPorts.Items.IndexOf(Settings.Default.GPSPort);

                cbBaud.SelectedIndex = cbBaud.Items.IndexOf(Settings.Default.GPSPortBaud.ToString());

                cbData.SelectedIndex = cbData.Items.IndexOf(Settings.Default.GPSPortDataBits.ToString());
#if DEBUG
                Console.WriteLine("\r\nSelected params:");
                Console.WriteLine("Selected port : {0}", Settings.Default.GPSPort);
                Console.WriteLine("Selected baud rate : {0}", Settings.Default.GPSPortBaud);
                Console.WriteLine("Selected data bits : {0}", Settings.Default.GPSPortDataBits);
                Console.WriteLine("Selected stop bits : {0}", Settings.Default.GPSPortStopBits);
                Console.WriteLine("Selected flow control : {0}", Settings.Default.GPSFlowControl);
                Console.WriteLine("Selected parity : {0}", Settings.Default.GPSPortParity);
#endif

                switch (Settings.Default.GPSPortStopBits)
                {
                    case System.IO.Ports.StopBits.None:
                        cbStop.SelectedIndex = 0;
                        break;
                    case System.IO.Ports.StopBits.One:
                        cbStop.SelectedIndex = 1;
                        break;
                    case System.IO.Ports.StopBits.OnePointFive:
                        cbStop.SelectedIndex = 2;
                        break;
                    case System.IO.Ports.StopBits.Two:
                        cbStop.SelectedIndex = 3;
                        break;
                    default:
                        cbStop.SelectedIndex = 0;
                        break;
                }

                switch (Settings.Default.GPSFlowControl)
                {
                    case System.IO.Ports.Handshake.None:
                        cbFlow.SelectedIndex = 0;
                        break;
                    case System.IO.Ports.Handshake.RequestToSend:
                        cbFlow.SelectedIndex = 1;
                        break;
                    case System.IO.Ports.Handshake.XOnXOff:
                        cbFlow.SelectedIndex = 2;
                        break;
                    default:
                        cbFlow.SelectedIndex = 0;
                        break;
                }

                cbParity.SelectedIndex = cbParity.Items.IndexOf(Settings.Default.GPSPortParity.ToString());
            }
            catch
            {
                // if there was a problem loading GPS prefs, default to GPS disabled
                gpsEnableCheckBox.Checked = false;
            }

            GpsEnableCheckBox_CheckedChanged(this, null);
        }

        private void LogCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            logFolderNameBox.Enabled = logCheckBox.Checked;
        }

        private void LogFolderBrowserButton_Click(object sender, EventArgs e)
        {
            if (logFolderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                logFolderNameBox.Text = logFolderBrowserDialog.SelectedPath;
            }
        }

        private void GpsEnableCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            cbPorts.Enabled = gpsEnableCheckBox.Checked;
            cbBaud.Enabled = gpsEnableCheckBox.Checked;
            cbData.Enabled = gpsEnableCheckBox.Checked;
            cbStop.Enabled = gpsEnableCheckBox.Checked;
            cbFlow.Enabled = gpsEnableCheckBox.Checked;
            cbParity.Enabled = gpsEnableCheckBox.Checked;
        }
    }
}