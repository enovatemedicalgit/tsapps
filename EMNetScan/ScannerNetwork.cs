////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Drawing;

namespace Inssider
{
    public class ScannerNetwork
    {
        #region Fields

        private static uint _nextId;
        private readonly uint _id;
        private readonly String _ssid;
        private String _vendor;
        private uint _channel;
        private int _signal;
        private String _security;
        private String _networkType;
        private float _speed;
        private DateTime _lastSeenTimeStamp;
        internal Color LineColor = Color.Red;
        private readonly bool _dashedLine;
        private readonly bool _stripedLine;
        private bool _visible = true;
        private readonly MacAddress _macAddress;
        private readonly Image _sigInd;

        private static readonly Color[] NetworkColors = 
        {
            Color.FromArgb(255,0,0),
            Color.FromArgb(46,206,12),
            Color.FromArgb(206,12,130),
            Color.FromArgb(254,231,0),
            Color.FromArgb(21,52,206),
            Color.FromArgb(153,153,153),
            Color.FromArgb(255,153,0),
            Color.FromArgb(12,153,206),
            Color.FromArgb(115,78,0),
            Color.FromArgb(186,232,6),
            Color.FromArgb(127,12,206),
        };

        #endregion

        #region Constructors

        private ScannerNetwork()
        {
            _sigInd = null;
            Selected = false;
            _stripedLine = false;
            _dashedLine = false;
        }

        internal ScannerNetwork(String ssid, MacAddress macAddress, DateTime timeStamp) {
            _sigInd = null;
            Selected = false;
            _stripedLine = false;
            _dashedLine = false;
            _id = _nextId++;
            _ssid = ssid;
            _macAddress = macAddress;
            FirstSeenTimeStamp = timeStamp;
            _lastSeenTimeStamp = timeStamp;

            if (_id >= NetworkColors.Length) {
                _dashedLine = true;
            }
            LineColor = NetworkColors[_id % NetworkColors.Length];
        }

        #endregion

        #region Properties

        internal object[] Data {
            get {
                object[] row = { 
                    _id, 
                    _visible, 
                    _sigInd, 
                    _macAddress.ToString(),
                    _vendor,
                    Ssid, 
                    _channel, 
                    _signal, 
                    _security, 
                    _networkType, 
                    _speed, 
                    FirstSeenTimeStamp.ToLongTimeString(), 
                    _lastSeenTimeStamp.ToLongTimeString() 
                };
                return row;
            }
        }

        internal bool DashedLine {
            get { return _dashedLine; }
        }

        internal bool StripedLine {
            get { return _stripedLine; }
        }

        internal uint Id {
            get { return _id; }
        }

        internal MacAddress MacAddress {
            get { return _macAddress; }
        }

        internal String MacAddressStringXx {
            get {
                return _macAddress.ToString();
            }
        }

        internal String Ssid {
            get
            {
                if ((_ssid.Length > 0) && (_ssid[0] != '\0')) {
                    return _ssid;
                }
                return Localizer.GetString("Unknown");
            }
        }

        internal uint Channel {
            get { return _channel; }
            set { _channel = value; }
        }

        public uint FrequencyMHz
        {
            get
            {
                // 2.4 GHz
                if ((_channel >= 1) && (_channel <= 13))
                {
                    return (_channel * 5) + 2407;
                }
                if (_channel == 14)
                {
                    return 2484;
                }
                    // 5 GHz
                if (_channel >= 36)
                {
                    return (_channel * 5) + 5000;
                }
                // Unknown
                return 0;
            }
        }
        
        internal int Signal
        {
            get { return _signal; }
            set { _signal = value; }
        }


        internal String Security {
            get { return _security; }
            set { _security = value; }
        }

        internal String NetworkType {
            get { return _networkType; }
            set {
                _networkType = value == "Infrastructure" ? Localizer.GetString("AccessPoint") : value;
            }
        }

        internal float Speed {
            get { return _speed; }
            set { _speed = value; }
        }

        internal String SpeedString { get; set; }

        internal DateTime LastSeenTimeStamp {
            get { return _lastSeenTimeStamp; }
            set { _lastSeenTimeStamp = value; }
        }

        internal DateTime FirstSeenTimeStamp { get; private set; }

        internal bool Visible {
            get { return _visible; }
            set { _visible = value; }
        }

        internal bool Selected { get; set; }

        internal String Vendor
        {
            get { return _vendor; }
            set { _vendor = value; }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Sets the nex Id value to zero.
        /// </summary>
        internal static void ResetNextId()
        {
            _nextId = 0;
        } // End ResetNextId()

        public static ScannerNetwork Create(WirelessDetails detailItem, string vendor, DateTime timeStamp, bool visible) {
            ScannerNetwork network = new ScannerNetwork(detailItem.Ssid, detailItem.MacAddress, timeStamp);
            network.Vendor = vendor;
            network.NetworkType = detailItem.InfrastructureMode;
            network.Signal = detailItem.Rssi;
            network.Channel = detailItem.Channel;
            if (detailItem.Rates.Count > 0) {
                network.Speed = (float)detailItem.Rates[detailItem.Rates.Count - 1];
            }
            else {
                network.Speed = 0;
            }
            network.SpeedString = detailItem.BuildRateString();
            network.Security = detailItem.Privacy;
            network.Visible = visible;
            return network;
        }

        #endregion
    }
}
