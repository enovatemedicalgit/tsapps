namespace Inssider
{
    partial class ExceptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if(disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
           System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager( typeof( ExceptionForm ) );
           this.panelTop = new System.Windows.Forms.Panel();
           this.label1 = new System.Windows.Forms.Label();
           this.labelTitle = new System.Windows.Forms.Label();
           this.panelDevider = new System.Windows.Forms.Panel();
           this.labelExceptionDate = new System.Windows.Forms.Label();
           this.labelCaption = new System.Windows.Forms.Label();
           this.labelDescription = new System.Windows.Forms.Label();
           this.buttonNotSend = new System.Windows.Forms.Button();
           this.buttonSend = new System.Windows.Forms.Button();
           this.labelLinkTitle = new System.Windows.Forms.Label();
           this.linkLabelData = new System.Windows.Forms.LinkLabel();
           this.checkBoxRestart = new System.Windows.Forms.CheckBox();
           this.panelTop.SuspendLayout();
           this.SuspendLayout();
           // 
           // panelTop
           // 
           this.panelTop.BackColor = System.Drawing.SystemColors.Window;
           this.panelTop.Controls.Add( this.label1 );
           this.panelTop.Controls.Add( this.labelTitle );
           resources.ApplyResources( this.panelTop, "panelTop" );
           this.panelTop.Name = "panelTop";
           // 
           // label1
           // 
           resources.ApplyResources( this.label1, "label1" );
           this.label1.Name = "label1";
           // 
           // labelTitle
           // 
           resources.ApplyResources( this.labelTitle, "labelTitle" );
           this.labelTitle.Name = "labelTitle";
           this.labelTitle.Click += new System.EventHandler( this.labelTitle_Click );
           // 
           // panelDevider
           // 
           this.panelDevider.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
           resources.ApplyResources( this.panelDevider, "panelDevider" );
           this.panelDevider.Name = "panelDevider";
           // 
           // labelExceptionDate
           // 
           resources.ApplyResources( this.labelExceptionDate, "labelExceptionDate" );
           this.labelExceptionDate.Name = "labelExceptionDate";
           this.labelExceptionDate.Click += new System.EventHandler( this.LabelExceptionDate_Click );
           // 
           // labelCaption
           // 
           resources.ApplyResources( this.labelCaption, "labelCaption" );
           this.labelCaption.Name = "labelCaption";
           // 
           // labelDescription
           // 
           resources.ApplyResources( this.labelDescription, "labelDescription" );
           this.labelDescription.Name = "labelDescription";
           // 
           // buttonNotSend
           // 
           this.buttonNotSend.Cursor = System.Windows.Forms.Cursors.Default;
           this.buttonNotSend.DialogResult = System.Windows.Forms.DialogResult.Cancel;
           resources.ApplyResources( this.buttonNotSend, "buttonNotSend" );
           this.buttonNotSend.Name = "buttonNotSend";
           this.buttonNotSend.UseVisualStyleBackColor = true;
           this.buttonNotSend.Click += new System.EventHandler( this.buttonNotSend_Click );
           // 
           // buttonSend
           // 
           this.buttonSend.DialogResult = System.Windows.Forms.DialogResult.Yes;
           resources.ApplyResources( this.buttonSend, "buttonSend" );
           this.buttonSend.Name = "buttonSend";
           this.buttonSend.UseVisualStyleBackColor = true;
           this.buttonSend.Click += new System.EventHandler( this.ButtonSend_Click );
           // 
           // labelLinkTitle
           // 
           resources.ApplyResources( this.labelLinkTitle, "labelLinkTitle" );
           this.labelLinkTitle.Name = "labelLinkTitle";
           // 
           // linkLabelData
           // 
           resources.ApplyResources( this.linkLabelData, "linkLabelData" );
           this.linkLabelData.Name = "linkLabelData";
           this.linkLabelData.TabStop = true;
           this.linkLabelData.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler( this.LinkLabelData_LinkClicked );
           // 
           // checkBoxRestart
           // 
           resources.ApplyResources( this.checkBoxRestart, "checkBoxRestart" );
           this.checkBoxRestart.Checked = true;
           this.checkBoxRestart.CheckState = System.Windows.Forms.CheckState.Checked;
           this.checkBoxRestart.Name = "checkBoxRestart";
           this.checkBoxRestart.UseVisualStyleBackColor = true;
           // 
           // ExceptionForm
           // 
           this.AcceptButton = this.buttonSend;
           resources.ApplyResources( this, "$this" );
           this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
           this.CancelButton = this.buttonNotSend;
           this.ControlBox = false;
           this.Controls.Add( this.checkBoxRestart );
           this.Controls.Add( this.linkLabelData );
           this.Controls.Add( this.labelLinkTitle );
           this.Controls.Add( this.buttonSend );
           this.Controls.Add( this.buttonNotSend );
           this.Controls.Add( this.labelDescription );
           this.Controls.Add( this.labelCaption );
           this.Controls.Add( this.labelExceptionDate );
           this.Controls.Add( this.panelDevider );
           this.Controls.Add( this.panelTop );
           this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
           this.MaximizeBox = false;
           this.MinimizeBox = false;
           this.Name = "ExceptionForm";
           this.TopMost = true;
           this.Load += new System.EventHandler( this.ExceptionForm_Load );
           this.panelTop.ResumeLayout( false );
           this.ResumeLayout( false );
           this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTop;
        private System.Windows.Forms.Panel panelDevider;
        private System.Windows.Forms.Label labelExceptionDate;
        private System.Windows.Forms.Label labelCaption;
        private System.Windows.Forms.Label labelDescription;
        internal System.Windows.Forms.Label labelTitle;
        internal System.Windows.Forms.Label labelLinkTitle;
        internal System.Windows.Forms.LinkLabel linkLabelData;
        internal System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.Button buttonNotSend;
        private System.Windows.Forms.Label label1;
        internal System.Windows.Forms.CheckBox checkBoxRestart;
    }
}