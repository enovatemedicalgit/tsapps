using System;
using System.Net.NetworkInformation;
using System.ComponentModel;
using System.Collections.Specialized;


namespace Inssider
{
   public class WirelessAdapterProvider
   {
#region Private Data
      private readonly StringCollection _ignoreInterfaces = new StringCollection();
      private readonly BindingList < WirelessInterface > _wirelessAdapterList = new BindingList < WirelessInterface > ();
#endregion 




      public BindingList < WirelessInterface > AdapterList
      {
         get
         {
            return (_wirelessAdapterList);
         }
      }




      public void UpdateAdapterListNative()
      {
         //_wirelessAdapterList = _client.Interfaces;
      }




      public void UpdateAdapterListNdis()
      {
         NetworkInterface[] allInterfaces = NetworkInterface.GetAllNetworkInterfaces();
         foreach(NetworkInterface nif in allInterfaces)
         {
            if (!_ignoreInterfaces.Contains(nif.Name))
            {
               if (nif.NetworkInterfaceType == NetworkInterfaceType.Loopback)
               {
                  _ignoreInterfaces.Add(nif.Name);
               }
               else 
               {
                  bool newNetwork = true;

                  for (int i = 0; i < _wirelessAdapterList.Count; i++)
                  {
                     if (_wirelessAdapterList[i].Name.Equals(nif.Name))
                     {
                        newNetwork = false;
                        break;
                     }
                  }
                  if (newNetwork)
                  {
                     try
                     {
                        _wirelessAdapterList.Add(WirelessInterface.Create(nif));
                     }
                     catch(ApplicationException)
                     {
                        /* Ignore */
                        _ignoreInterfaces.Add(nif.Name);
                     }
                     catch(Win32Exception)
                     {
                        /* Ignore */
                        _ignoreInterfaces.Add(nif.Name);
                     }
                  }
               }
            }
         }

         for (int i = 0; i < _wirelessAdapterList.Count; i++)
         {
            bool found = false;

            foreach(NetworkInterface nif in allInterfaces)
            {
               if (nif.Name == _wirelessAdapterList[i].Name && nif.Description == _wirelessAdapterList[i].Description)
               {
                  found = true;
                  break;
               }
            }
            if (!found)
            {
               _wirelessAdapterList.Remove(_wirelessAdapterList[i]);
               i--;
            }
         }
      }
   }
}

