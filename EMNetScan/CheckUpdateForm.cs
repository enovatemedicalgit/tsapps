////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;
using Inssider.Properties;


namespace Inssider
{
	/// <summary>
	/// Summary description for CheckUpdateForm.
	/// </summary>
	internal class CheckUpdateForm : Form
	{
		private Label _installedVersionNameLabel;
        private Label _latestVersionNameLabel;
		private Button _downloadUpdateButton;
		private Button _remindLaterButton;
        private Label _installedVersionLabel;
        private Label _latestVersionLabel;
        private TextBox _descriptionTextBox;
        private String _downloadUrl = "";
        private Button _ignoreVersionButton;

        /// <summary>
        /// 
        /// </summary>
        internal CheckUpdateForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			_latestVersionLabel.Text = LatestVersion;
		}

	    #region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CheckUpdateForm));
            this._installedVersionNameLabel = new System.Windows.Forms.Label();
            this._latestVersionNameLabel = new System.Windows.Forms.Label();
            this._downloadUpdateButton = new System.Windows.Forms.Button();
            this._remindLaterButton = new System.Windows.Forms.Button();
            this._installedVersionLabel = new System.Windows.Forms.Label();
            this._latestVersionLabel = new System.Windows.Forms.Label();
            this._descriptionTextBox = new System.Windows.Forms.TextBox();
            this._ignoreVersionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _installedVersionNameLabel
            // 
            this._installedVersionNameLabel.AccessibleDescription = null;
            this._installedVersionNameLabel.AccessibleName = null;
            resources.ApplyResources(this._installedVersionNameLabel, "_installedVersionNameLabel");
            this._installedVersionNameLabel.Font = null;
            this._installedVersionNameLabel.Name = "_installedVersionNameLabel";
            // 
            // _latestVersionNameLabel
            // 
            this._latestVersionNameLabel.AccessibleDescription = null;
            this._latestVersionNameLabel.AccessibleName = null;
            resources.ApplyResources(this._latestVersionNameLabel, "_latestVersionNameLabel");
            this._latestVersionNameLabel.Font = null;
            this._latestVersionNameLabel.Name = "_latestVersionNameLabel";
            // 
            // _downloadUpdateButton
            // 
            this._downloadUpdateButton.AccessibleDescription = null;
            this._downloadUpdateButton.AccessibleName = null;
            resources.ApplyResources(this._downloadUpdateButton, "_downloadUpdateButton");
            this._downloadUpdateButton.BackgroundImage = null;
            this._downloadUpdateButton.Font = null;
            this._downloadUpdateButton.Name = "_downloadUpdateButton";
            this._downloadUpdateButton.Click += new System.EventHandler(this.DownloadUpdateButton_Click);
            // 
            // _remindLaterButton
            // 
            this._remindLaterButton.AccessibleDescription = null;
            this._remindLaterButton.AccessibleName = null;
            resources.ApplyResources(this._remindLaterButton, "_remindLaterButton");
            this._remindLaterButton.BackgroundImage = null;
            this._remindLaterButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._remindLaterButton.Font = null;
            this._remindLaterButton.Name = "_remindLaterButton";
            this._remindLaterButton.Click += new System.EventHandler(this.RemindLaterButton_Click);
            // 
            // _installedVersionLabel
            // 
            this._installedVersionLabel.AccessibleDescription = null;
            this._installedVersionLabel.AccessibleName = null;
            resources.ApplyResources(this._installedVersionLabel, "_installedVersionLabel");
            this._installedVersionLabel.Font = null;
            this._installedVersionLabel.Name = "_installedVersionLabel";
            // 
            // _latestVersionLabel
            // 
            this._latestVersionLabel.AccessibleDescription = null;
            this._latestVersionLabel.AccessibleName = null;
            resources.ApplyResources(this._latestVersionLabel, "_latestVersionLabel");
            this._latestVersionLabel.Name = "_latestVersionLabel";
            // 
            // _descriptionTextBox
            // 
            this._descriptionTextBox.AccessibleDescription = null;
            this._descriptionTextBox.AccessibleName = null;
            resources.ApplyResources(this._descriptionTextBox, "_descriptionTextBox");
            this._descriptionTextBox.BackgroundImage = null;
            this._descriptionTextBox.Font = null;
            this._descriptionTextBox.Name = "_descriptionTextBox";
            this._descriptionTextBox.ReadOnly = true;
            // 
            // _ignoreVersionButton
            // 
            this._ignoreVersionButton.AccessibleDescription = null;
            this._ignoreVersionButton.AccessibleName = null;
            resources.ApplyResources(this._ignoreVersionButton, "_ignoreVersionButton");
            this._ignoreVersionButton.BackgroundImage = null;
            this._ignoreVersionButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._ignoreVersionButton.Font = null;
            this._ignoreVersionButton.Name = "_ignoreVersionButton";
            this._ignoreVersionButton.Click += new System.EventHandler(this.IgnoreVersionButton_Click);
            // 
            // CheckUpdateForm
            // 
            this.AccessibleDescription = null;
            this.AccessibleName = null;
            resources.ApplyResources(this, "$this");
            this.BackgroundImage = null;
            this.Controls.Add(this._ignoreVersionButton);
            this.Controls.Add(this._descriptionTextBox);
            this.Controls.Add(this._latestVersionLabel);
            this.Controls.Add(this._installedVersionLabel);
            this.Controls.Add(this._downloadUpdateButton);
            this.Controls.Add(this._remindLaterButton);
            this.Controls.Add(this._latestVersionNameLabel);
            this.Controls.Add(this._installedVersionNameLabel);
            this.Font = null;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "CheckUpdateForm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion

        #region Public Properties

        internal String InstalledVersion
        {
            get { return _installedVersionLabel.Text; }
            set { _installedVersionLabel.Text = value; }
        }

        internal String LatestVersion
        {
            get { return _latestVersionLabel.Text; }
            set { _latestVersionLabel.Text = value; }
        }

        internal String DownloadUrl
        {
            get { return _downloadUrl; }
            set { _downloadUrl = value; }
        }

        internal String VersionDescription
        {
            get { return _descriptionTextBox.Text; }
            set { _descriptionTextBox.Text = value; }
        }

        #endregion

        #region Private Methods

        private void DownloadUpdateButton_Click(object sender, EventArgs e)
		{
            DialogResult = DialogResult.OK;
            Close();
		}

        private void RemindLaterButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void IgnoreVersionButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Update the IgnoreUpdateVersion
                Settings.Default.IgnoreUpdateVersion = LatestVersion;
                Settings.Default.Save();
            }
            catch 
            { // couldn't save prefs...oh well
            }

            // Close Dialog
            DialogResult = DialogResult.Cancel;
            Close();
        }

        #endregion
    }
}
