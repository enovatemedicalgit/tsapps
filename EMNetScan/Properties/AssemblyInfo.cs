﻿﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "EMNetScan" )]
[assembly: AssemblyDescription( "Network Scanner" )]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany( "Enovate Medical" )]
[assembly: AssemblyProduct( "EMNetScan" )]
[assembly: AssemblyCopyright("Copyright © Enovate Medical 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the Id of the typelib if this project is exposed to COM
[assembly: Guid("1b1fb41a-cead-4603-b2f9-bd03e2e09b57")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyFileVersion( "1.0.0.0" )]
[assembly: AssemblyVersion( "2010.07.25.0" )]
