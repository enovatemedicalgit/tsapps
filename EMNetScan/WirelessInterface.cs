////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//      http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Inssider.Properties;


namespace Inssider
{
   public abstract class WirelessInterface
   {
      /// <summary>
      /// Gets the name of the interface
      /// </summary>
      public abstract string Name
      {
         get;
      }



      /// <summary>
      /// Gets the description of the WirelessInterface
      /// </summary>
      public abstract string Description
      {
         get;
      }



      /// <summary>
      /// Instructs the network driver to scan for available
      /// networks.
      /// </summary>
      public abstract void ScanNetworks();



      /// <summary>
      /// Gets detailed information about the available wireless
      /// networks.
      /// </summary>
      /// <returns>an array of WirelessDetails objects, which
      /// contain information about the available wirless networks, such
      /// as MAC address and RSSI.
      /// </returns>
      public          abstract List < WirelessDetails > GetWirelessDetails();



      /// <summary>
      /// Creates an instance of the appropritate WirelessInterface based
      /// on the current operating environment.
      /// </summary>
      /// <returns>a WirelessInterface instance</returns>
      public static WirelessInterface Create(NetworkInterface networkInterface)
      {
         WirelessInterface wlanInterface;

         try
         {
            if ((IsXp() && Settings.Default.driverMode == 0) || Settings.Default.driverMode == 2)
            {
               wlanInterface = new NdisWirelessInterface(networkInterface);
            }
            else 
            {
               wlanInterface = new NativeWirelessInterface(networkInterface);
            }
         }
         catch(ApplicationException ex)
         {
            if (Settings.Default.driverMode != 0)
            {
               Settings.Default.driverMode = 0;
               wlanInterface = Create(networkInterface);
            }
            else 
            {
               throw(ex);
            }
         }
         return (wlanInterface);
      }
      // End Create()




      /// <summary>
      /// Determines if the current operating environment is Windows
      /// XP
      /// </summary>
      /// <returns>true of the underlying operating environment is Windows XP, false otherwise</returns>
      private static bool IsXp()
      {
         return (Environment.OSVersion.Version.Major == 5) && 
         (Environment.OSVersion.Version.Minor >= 1);
      }
      // End IsXp()




      /// <summary>
      /// Converts a list of raw rate values into a list of rates 
      /// in units of Mbits/second.
      /// </summary>
      /// <param name="rates">array of raw rate values</param>
      /// <param name="rateList">output list of rates in Mbs</param>
      protected void ConvertToMbs(ushort[] rates, List < double > rateList)
      {
         ConvertToMbs(rates, rateList, false);
      }




      /// <summary>
      /// Converts a list of raw rate values into a list of rates 
      /// in units of Mbits/second.
      /// </summary>
      /// <param name="rates">array of raw rate values</param>
      /// <param name="rateList">utput list of rates in Mbs</param>
      /// <param name="isTypeN">indicates whether this is a type N network</param>
      protected void ConvertToMbs(ushort[] rates, List < double > rateList, bool isTypeN)
      {
         for (int i = 0; i < rates.Length; ++i)
         {
            if (rates[i] > 0)
            {
               rateList.Add((double)(rates[i]& 0x7FFF) *0.5);
            }
         }
         if (isTypeN)
         {
            rateList.Add(65.0f);
         }
         rateList.Sort(new Comparison < double > (Compare));
      }
      // End ConvertToMbs()




      /// <summary>
      /// Converts a list of raw rate values into a list of rates 
      /// in units of Mbits/second.
      /// </summary>
      /// <param name="rates">array of raw rate values</param>
      /// <param name="rateList">output list of rates in Mbs</param>
      protected void ConvertToMbs(byte[] rates, List < double > rateList)
      {
         ConvertToMbs(rates, rateList, false);
      }




      /// <summary>
      /// Converts a list of raw rate values into a list of rates 
      /// in units of Mbits/second.
      /// </summary>
      /// <param name="rates">array of raw rate values</param>
      /// <param name="rateList">utput list of rates in Mbs</param>
      /// <param name="isTypeN">indicates whether this is a type N network</param>
      /// <param name="isTypeN">indicates whether this is a type N network</param>
      protected void ConvertToMbs(byte[] rates, List < double > rateList, bool isTypeN)
      {
         for (int i = 0; i < rates.Length; ++i)
         {
            if (rates[i] > 0)
            {
               rateList.Add((double)(rates[i]& 0x7F) *0.5);
            }
         }
         if (isTypeN)
         {
            rateList.Add(65.0f);
         }
         rateList.Sort(new Comparison < double > (Compare));
      }
      // End ConvertToMbs()




      /// <summary>
      /// Compares two doubles for equality.
      /// </summary>
      /// <param name="d1"></param>
      /// <param name="d2"></param>
      /// <returns></returns>
      private int Compare(double d1, double d2)
      {
         if (d1 > d2)
         {
            return 1;
         }
         if (d1 < d2)
         {
            return - 1;
         }
         return 0;
      }
      // End Compare()




      /// <summary>
      /// Converts a frequency value to a designated WIFI channel
      /// </summary>
      /// <param name="frequency">frequency to convert to a channel</param>
      /// <returns>the WIFI channel at the given frequency</returns>
      protected uint ConvertToChannel(uint frequency)
      {
         // 2.4 GHz
         if ((frequency > 2400000) && (frequency < 2484000))
         {
            return (frequency - 2407000) / 5000;
         }
         if ((frequency >= 2484000) && (frequency <= 2495000))
         {
            return 14;
         }
         // 5 GHz
         if ((frequency > 5000000) && (frequency < 5900000))
         {
            return (frequency - 5000000) / 5000;
         }
         // Unknown
         return 0;
      }
      // End ConvertToChannel()

   }
}

