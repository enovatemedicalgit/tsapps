////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Net.NetworkInformation;
using System.Collections.Generic;
using System.Text;
using MetaGeek.IoctlNdis;
using System.Management;

namespace Inssider
{
    public class NdisWirelessInterface : WirelessInterface
    {
        #region Text Conversion Strings
        /// <summary>
        /// Structure for associating an integer Value to a string.
        /// </summary>
        struct ValueString
        {
            internal readonly int Value;
            internal readonly string Text;
            internal ValueString(int val, string txt) {
                Value = val;
                Text = txt;
            }
        }

        /// <summary>
        /// Authentication mode strings
        /// </summary>
        static readonly ValueString[] AuthModeText = new ValueString[]{            	
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeOpen, "Open" ),
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeShared, "Shared" ),
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeAutoSwitch, "Auto Switch" ),
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeWpa, "WPA" ),												
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeWpapsk, "WPAPSK" ),				
			new ValueString( (int)AuthenticationMode.Ndis80211AuthModeWpaNone, "None" )       
        };

        /// <summary>
        /// Network type strings.
        /// </summary>
        static readonly ValueString[] NetworkTypeText = new ValueString[]{ 
            new ValueString( (int)NdisNetworkType.Ndis80211Ofdm24, "2.4-GHz OFDM"),				
			new ValueString( (int)NdisNetworkType.Ndis80211Ofdm5, "5-GHz OFDM" ),				
			new ValueString( (int)NdisNetworkType.Ndis80211Ds, "DS (direct-sequence spread-spectrum)")
        };

        /// <summary>
        /// Infrastructure mode strings.
        /// </summary>
        static readonly ValueString[] InfrastructureText = new ValueString[]{
			new ValueString( (int)NetworkInfrastructure.Ndis80211Infrastructure, "Access Point" ),
			new ValueString( (int)NetworkInfrastructure.Ndis80211Ibss, "Ad Hoc" ),							
            new ValueString( (int)NetworkInfrastructure.Ndis80211AutoUnknown, "Auto or unknown" )
         };

        #endregion


        /// <summary>
        /// Gets the name of the interface
        /// </summary>
        public override string Name {
            get {
                return _interface.Name;
            }
        }

        /// <summary>
        /// Gets the description of the underlying 
        /// interface
        /// </summary>
        public override string Description
        {
            get
            {
                return _interface.Description;
            }
        }

        private readonly NetworkInterface _interface;
        // NDIS API wrapper 
        private readonly IoctlNdis _ndisWlan;

        /// <summary>
        /// Helper function used to find a string associated with
        /// a partiular Value.
        /// </summary>
        /// <param name="strings">Value-string array to search</param>
        /// <param name="value">Value to find</param>
        /// <returns>the string associated with the Value</returns>
        private string FindValueString(ValueString[] strings, int value) {
            for (int i = 0; i < strings.Length; ++i) {
                if (strings[i].Value.Equals(value)) {
                    return (strings[i].Text);
                }
            }
            return (null);
        } // End FindValueString        

        /// <summary>
        /// Determines if the interface will support a Bssid 
        /// query.
        /// </summary>
        /// <param name="networkInterface">network interface to test</param>
        /// <returns>true if the network interface supports a Bssid list query, false otherwise</returns>
        private bool SupportsBssidList(NetworkInterface networkInterface) {
            bool result = false;
            uint[] oidList = _ndisWlan.QuerySupportedOids(networkInterface);
            if (null != oidList) {
                
                foreach (uint oid in oidList) {
                    if ((uint)Oid.Oid80211BssidList == oid) {
                        result = true;
                        break;
                    }
                }
            }
            return (result);
        } // End SupportsBssidList()

        /// <summary>
        /// Creates a new instance of the NdisWirelessInterface
        /// class. If an adapter is not found with the proper 
        /// capabilities, an exception is raised.
        /// </summary>
        public NdisWirelessInterface(NetworkInterface networkInterface) {
            // Make sure a valid interface was specified
            _interface = networkInterface;                      
            if (null == _interface) {
                throw new ArgumentNullException(Localizer.GetString("InvalidNetworkInterface"));
            }            
            // Create the ndis wrapper
            _ndisWlan = new IoctlNdis();
            if (!SupportsBssidList(_interface)) {
                throw new ApplicationException(Localizer.GetString("NoBssidQueries"));
            }
        } // End NdisWirelessInterface()

        /// <summary>
        /// Initiates a discovery scan of the available wireless networkds.
        /// </summary>
        public override void ScanNetworks() { 
            // TODO: Verify why this fails. Does it fail on all platforms, or 
            // just some?
            //_ndisWlan.ScanBssidList(_interface);
            try {
                ManagementClass mc = new ManagementClass("root\\WMI", "MSNDis_80211_BssIdListScan", null);
                ManagementObject mo = mc.CreateInstance();
                mo["Active"] = true;
                mo["InstanceName"] = _interface.Description;//.Replace(" - Packet Scheduler Miniport","");
                mo["UnusedParameter"] = 1;
                mo.Put();
            }
            catch {
                // TODO: Verify root cause of exception.
                // Ignore, for now
                // there seems to be some issues with WMI on certain systems. Various exceptions have been
                // reported from this method, which relate to problems with WMI.
            }
        } // End ScanNetworks()

        /// <summary>
        /// Returns a list of wireless details queried from the 
        /// network adapter.
        /// </summary>
        /// <returns>A List of WirelessDetails objects</returns>
        public override List<WirelessDetails> GetWirelessDetails() {
            List<WirelessDetails> detailList = new List<WirelessDetails>();
            NdisWlanBssidEx[] bssList =  _ndisWlan.QueryBssidList(_interface);
            if (null != bssList) 
            {
                foreach (NdisWlanBssidEx bssItem in bssList) 
                {
                    WirelessDetails detailItem = new WirelessDetails(bssItem.MacAddress);
                    detailItem.Channel = ConvertToChannel(bssItem.Configuration.DsConfig);

                    if (bssItem.IeLength <= bssItem.Ies.Length && bssItem.IeLength > IoctlNdis.HtCapabilitiesLength)
                    {

                        for (int i = 0; i < bssItem.IeLength - (IoctlNdis.HtCapabilitiesLength + 1); i++)
                        {
                            // see if we found some 802.11n (HT) Information Elements
                            if (bssItem.Ies[i] == (byte)IeType.HtCapabilities && bssItem.Ies[i + 1] + 2 == IoctlNdis.HtCapabilitiesLength &&
                                bssItem.Ies[i + IoctlNdis.HtCapabilitiesLength] == (byte)IeType.HtExtendedCapabilities &&
                                bssItem.Ies[i + IoctlNdis.HtCapabilitiesLength + 1] + 2 == IoctlNdis.HtExtendedCapabilitiesLength)
                            {
                                detailItem.IsTypeN = true;

                                // see if the channel bonding bit is set
                                if ((bssItem.Ies[i + 2] & 0x02) != 0x00)
                                {
                                    detailItem.IsChannelBonded = true;
                                }
                            }
                        }
                    }

                    ConvertToMbs(bssItem.SupportedRates, detailItem.Rates, detailItem.IsTypeN);
                    detailItem.Rssi = bssItem.Rssi;
                    detailItem.SignalQuality = 0; // not available with NDIS
                    
                    String ssid = Encoding.ASCII.GetString(bssItem.Ssid, 0, (int)bssItem.SsidLength);
                    if (null != ssid) {
                        ssid = ssid.Trim();
                    }

                    detailItem.Ssid = ssid;
                    detailItem.Privacy = _ndisWlan.GetPrivacyString(bssItem);
                    detailItem.NetworkType = FindValueString(NetworkTypeText, (int)bssItem.NetworkTypeInUse);
                    detailItem.InfrastructureMode = FindValueString(InfrastructureText, (int)bssItem.InfrastructureMode);

                    detailList.Add(detailItem);
                }
            }
            return ( detailList );
        } // End GetWirelessDetails()
    }
}
