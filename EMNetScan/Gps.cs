////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Threading;
using Inssider.Properties;
using System.IO;

namespace Inssider
{
    public class Gps
    {
        private int[] _satIDs;
        public int[] SatIDs
        {
            get { return _satIDs; }
        }

        private List<Satellite> _satellites = new List<Satellite>();
        public List<Satellite> Satellites
        {
            get { return _satellites; }
        }

        public event EventHandler GpsUpdated;
        public event EventHandler GpsTimeout;

        #region Private Data
        //GPS Objects
        private readonly NmeaParser _nmea = new NmeaParser();
        private bool _hasGpsFix;
        private double _magVar = double.NaN;
        //private bool gpsConnected = false;

        //GPS variables
        private System.IO.Ports.SerialPort _gpsSerialPort;
        private bool _enable;
        private Thread _gpsThread;
        private readonly WaitHandle[] _waitHandles;
        readonly AutoResetEvent _terminate = new AutoResetEvent(false);

        #endregion

        #region Constructors
        public Gps()
        {
            HasTalked = false;
            _enable = false;
            _hasGpsFix = false;
            _waitHandles = new WaitHandle[] { _terminate, };
        }

        #endregion

        #region Public Properties
        public bool Connected
        {
            get
            {
                return (_gpsSerialPort != null && _gpsSerialPort.IsOpen);
            }
        }

        public bool Enabled
        {
            get
            {
                return (_enable);
            }

            set
            {
                _enable = value;
                if (!Enabled)
                {
                    TerminateThread();
                }
            }
        }

        // Methods for setting GPS variables. They are in methods so the routine can be extended
        // in the future beyond simple setting of the value. Such as updating controls on the forms or logging etc.
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public double Altitude { get; set; }

        public double Speed { get; set; }

        public DateTime Time { get; set; }

        public DateTime SatelliteTime { get; set; }

        public double Course { get; set; }

        public bool HasTalked { get; set; }

        public bool HasFix
        {
            get { return _hasGpsFix; }
            //set { _hasGpsFix = value; }
        }

        public double MagVar
        {
            get { return _magVar; }
            set { _magVar = value; }
        }

        public int SatellitesVisible { get; set; }

        public double Pdop { get; set; }

        public double Vdop { get; set; }

        public double Hdop { get; set; }

        public int SatellitesUsed { get; set; }

        public double GeoidSeperation
        {
            get { return _nmea.GeoIdSeperation; }
        }

        public string FixString
        {
            get { return _nmea.FixString; }
        }

        public double DgpsAge
        {
            get { return _nmea.DGPSAge; }
        }

        public double Dgpsid
        {
            get { return _nmea.DGPSID; }
        }

        public string PortName
        {
            get
            {
                string name = string.Empty;
                if (null != _gpsSerialPort)
                {
                    name = _gpsSerialPort.PortName;
                }
                return name;
            }
        }
        #endregion

        #region Public Methods

        private void ClosePort()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("GPS - ClosePort()");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            if (_gpsSerialPort == null || !_gpsSerialPort.IsOpen) return;
            try
            {
                _gpsSerialPort.Close();
            }
            catch (IOException)
            {
            }
            catch (ObjectDisposedException)
            {
            }

            HasTalked = false;
            OnGpsUpdated();
        }

                /// <summary>
        /// Initalizes the GPS Serial Port
        /// </summary>
        /// <returns>status</returns>
        private bool ResetPort()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("GPS - ResetPort()");
            Console.WriteLine("GPS - {0}", _enable ? "Port is enabled" : "Port is disabled");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            bool status = false;

            if (_enable)
            {
                try
                {
                    if (_gpsSerialPort != null)
                    {
                        try
                        {
                            _gpsSerialPort.Close();
                        }
                        catch (IOException)
                        {
                        }
                        catch (ObjectDisposedException)
                        {
                        }
                        _gpsSerialPort = null;
                    }

                    _gpsSerialPort = new System.IO.Ports.SerialPort(Settings.Default.GPSPort, Settings.Default.GPSPortBaud, Settings.Default.GPSPortParity, Settings.Default.GPSPortDataBits, Settings.Default.GPSPortStopBits);
                    _gpsSerialPort.Handshake = Settings.Default.GPSFlowControl;
                    _gpsSerialPort.ReadTimeout = 500;

                    _gpsSerialPort.Open();

                    HasTalked = false;

                    status = true;
                }
                catch (Exception ex)
                {
                    status = false;
                    _gpsSerialPort = null;
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("GPS - Exception! - {0}", ex.Message);
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                }
            }
            return status;

        }

        /// <summary>
        /// Initalizes the GPS Serial Port
        /// </summary>
        /// <returns>status</returns>
        public bool InitializeThread()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("GPS - InitializeThread()");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            bool status = false;

            if (_enable)
            {
                if (ResetPort())
                {
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("GPS - Initialization of port successful");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                    if (_gpsThread != null)
                    {
                        _terminate.Set();
                        if (!_gpsThread.Join(1000))
                        {
#if DEBUG
                            Console.ForegroundColor = ConsoleColor.White;
                            Console.WriteLine("GPS - abort old thread");
                            Console.ForegroundColor = ConsoleColor.Gray;
#endif
                            _gpsThread.Abort();
                        }
                    }

                    _gpsThread = new Thread(GpsMethod);
                    _gpsThread.Start();
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("GPS - GPS thread start");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif

                    status = true;
                }
                else
                {
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("GPS - an error occourred, disabling port");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                    Settings.Default.gpsEnabled = false;
                    _enable = false;
                }
            }
            return status;
        }

        public void TerminateThread()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("GPS - TerminateThread()");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            ClosePort();

            if (_gpsThread != null)
            {
                _terminate.Set();
                if (!_gpsThread.Join(1000))
                {
                    _gpsThread.Abort();
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("GPS - abort thread");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// GPS Thread, reads data from serial port and updates the GPS information
        /// For more info on GPS information see this page about NMEA:
        /// http://www.gpsinformation.org/dale/nmea.htm
        /// </summary>
        private void GpsMethod()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("GPS - reader method");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            int gpsTimeoutCounter = 0;

            _terminate.Reset();

            while (true)
            {
                try
                {
                    int eventIdx = WaitHandle.WaitAny(_waitHandles, 100, false);
                    if (eventIdx == WaitHandle.WaitTimeout)
                    {
                        if (_gpsSerialPort != null && _gpsSerialPort.IsOpen)
                        {
                            string sentence = string.Empty;
                            try
                            {
                                sentence = _gpsSerialPort.ReadLine();
                                gpsTimeoutCounter = 0;
                                HasTalked = true;
#if DEBUG
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.WriteLine("GPS - recieved data from the GPS!");
                                Console.ForegroundColor = ConsoleColor.Gray;
#endif
                            }
                            catch (ArgumentOutOfRangeException)
                            {
                                // somehow we got out of sync w/ the serial port stream, try to recover
#if DEBUG
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("GPS - Out of sync error! - recovering");
                                Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                int counter = 0;
                                // line feed is last character of an NMEA sentence

                                try
                                {
                                    while (_gpsSerialPort.ReadChar() != '\n')
                                    {
                                        counter++;
                                        if (counter > 82)
                                        {
#if DEBUG
                                            Console.ForegroundColor = ConsoleColor.Red;
                                            Console.WriteLine("GPS - unable to recover!");
                                            Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                            // unable to recover... close port
                                            ClosePort();
                                        }
                                    }
                                }
                                catch (ArgumentException)
                                {
#if DEBUG
                                    Console.ForegroundColor = ConsoleColor.Red;
                                    Console.WriteLine("GPS - unable to recover!");
                                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                    // unable to recover... close port
                                    ClosePort();
                                }
                            }

                            catch (ArithmeticException ex)
                            {
#if DEBUG
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("GPS - Exception! - {0}", ex.Message);
                                Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                // TODO: wtf??
                            }

                            catch (TimeoutException)
                            {
                                // if the ReadLine() call has timed out 4 times in a row (no data for two seconds), give up
                                if (!HasTalked && 4 <= gpsTimeoutCounter)
                                {
#if DEBUG
                                    Console.ForegroundColor = ConsoleColor.Yellow;
                                    Console.WriteLine("GPS - no data was received, give up");
                                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                    ClosePort();
                                    break;
                                }
                                gpsTimeoutCounter++;
                            }

                            catch (IOException)
                            {
#if DEBUG
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("GPS - IO Exception!");
                                Console.ForegroundColor = ConsoleColor.Gray;
#endif
                                ClosePort();
                                break;
                            }

                            NmeaParser.SentenceType type = _nmea.Parse(sentence);
                            //gpsConnected = true;
                            switch (type)
                            {
                                case NmeaParser.SentenceType.Gprmc:
                                    Latitude = _nmea.Latitude;
                                    Longitude = _nmea.Longitude;
                                    Speed = _nmea.Speed;
                                    Time = _nmea.Timestamp;
                                    SatelliteTime = _nmea.SatelliteTime;
                                    Course = _nmea.Course;
                                    _hasGpsFix = _nmea.HasFix;

                                    MagVar = _nmea.MagVar;
                                    break;

                                case NmeaParser.SentenceType.Gpgsv:
                                    _satellites = _nmea.Satellites;
                                    SatellitesVisible = _nmea.SatelliteCount;
                                    break;

                                case NmeaParser.SentenceType.Gpgsa:
                                    _satIDs = _nmea.GetSatIDs();

                                    Pdop = _nmea.PDOP;
                                    Hdop = _nmea.HDOP;
                                    Vdop = _nmea.VDOP;
                                    SatellitesUsed = _nmea.SatellitesUsed;
                                    break;

                                case NmeaParser.SentenceType.Gpgga:
                                    Latitude = _nmea.Latitude;
                                    Longitude = _nmea.Longitude;
                                    Time = _nmea.Timestamp;
                                    Altitude = _nmea.Altitude;
                                    break;

                                case NmeaParser.SentenceType.Gpvtg:
                                    Speed = _nmea.Speed;
                                    break;

                                default:
                                    //gpsConnected = false;
                                    break;
                            }
                        }
                        else
                        {
                            // The port was previously closed due to being out of sync, reopen
                            Thread.Sleep(500);
                            ResetPort();
                        }

                        OnGpsUpdated();
                    }
                    else // The terminate event was signaled
                    {
                        break;
                    }
                }
                catch (ObjectDisposedException)
                {
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("GPS - Object Disposed Exception");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                    break;
                }
            }
        }

        private void OnGpsUpdated()
        {
            if (null != GpsUpdated)
            {
                GpsUpdated(this, EventArgs.Empty);
            }
        }

        private void OnGpsTimeout()
        {
            if (null != GpsTimeout)
            {
                GpsTimeout(this, EventArgs.Empty);
            }
        }

        #endregion
    }
}
