////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Text;
using System.Windows.Forms;

namespace Inssider
{
    /// <summary>
    /// Form for displaying runtime exceptions. This form will 
    /// allow the user to send an error report back to 
    /// Stinger Medical's customer support.    
    /// </summary>
    public partial class ExceptionForm : Form
    {
        #region Private Data
        // internal exception object to display
        private readonly Exception _exception;
        #endregion


        #region Constructors
        /// <summary>
        /// Creates a new instance of an Exception form.
        /// </summary>
        /// <param name="ex"></param>
        protected ExceptionForm(Exception ex)
            : this() {
            _exception = ex;
        }// End ExceptionForm()

        /// <summary>
        /// Creates a new instance of an Exception form.
        /// </summary>
        protected ExceptionForm() {
            InitializeComponent();
        }// End ExceptionForm()

        #endregion

        /// <summary>
        /// This is the entry point for displaying this form.
        /// </summary>
        /// <param name="ex">exception to display</param>
        public static void ShowException(Exception ex) {
            ExceptionForm exceptionForm = new ExceptionForm(ex);
            try {
                exceptionForm.ShowDialog();
            }
            finally {
                exceptionForm.Dispose();
            }
        } // End ShowException()

        /// <summary>
        /// Handler for load event.
        /// </summary>
        /// <param name="sender">source object</param>
        /// <param name="e">event args</param>
        private void ExceptionForm_Load(object sender, EventArgs e) {
            buttonSend.Focus();
            labelExceptionDate.Text = String.Format(labelExceptionDate.Text, DateTime.Now);
            labelExceptionDate.Text = String.Format(labelExceptionDate.Text, DateTime.Now.ToShortDateString());
            string processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
            Text = processName;
            labelTitle.Text = String.Format(labelTitle.Text, processName);
            linkLabelData.Left = labelLinkTitle.Right;
            checkBoxRestart.Text = String.Format(checkBoxRestart.Text, processName);
        }

        /// <summary>
        /// Handler for displaying the report contents.
        /// </summary>
        /// <param name="sender">source object</param>
        /// <param name="e">event arguments</param>
        private void LinkLabelData_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) {
            MessageBox.Show(CreateErrorReport());
        } // End ShowException()

        /// <summary>
        /// Creates an error message string.
        /// </summary>
        /// <returns></returns>
        private string CreateErrorReport() {
            const string separator = "\r\n\r\n";
            StringBuilder text = new StringBuilder();
            text.Append("[Message]\r\n");
            if (null != _exception) {
                text.Append(_exception.Message);
            }
            else {
                text.Append("None");
            }

            text.Append(separator);

            text.Append("[Version]\r\n");
            text.Append(Application.ProductVersion);
            text.Append(separator);

            text.Append("[WinVer]\r\n");
            text.Append(Environment.OSVersion.VersionString);
            text.Append(separator);

            text.Append("[StatckTrace]\r\n");
            if (null != _exception) {
                //text.Append(_exception.StackTrace);
                text.Append(_exception.ToString());
            }
            else {
                text.Append("None.");
            }
            text.Append(separator);

            return (text.ToString());
        }// End CreateErrorReport()

        /// <summary>
        /// Creates an error message string to include in an email message
        /// </summary>
        /// <returns></returns>
        private string CreateEmailReport() {
            StringBuilder msgBody = new StringBuilder();
            msgBody.Append(Localizer.GetString("ErrorDescription") + "\r\n");
            msgBody.Append("[Description]\r\n\r\n\r\n\r\n");
            msgBody.Append(CreateErrorReport());
            return msgBody.ToString();
        } // End CreateEmailReport()

        /// <summary>
        /// Sends an email containing the error report to Stinger Medical customer support.
        /// </summary>
        private void SendErrorReport() {
            //
            // Create an error report.
            //
            string msgBody = CreateEmailReport();
            MapiMailMessage message = new MapiMailMessage("Inssider Error Report", msgBody);
            message.Recipients.Add("error.reports@metageek.net");
            message.ShowDialog(true);

            
        } // End SendErrorReport()

        /// <summary>
        /// Restarts the application if the restart checkbox is checked.
        /// </summary>
        private void CheckRestart() {
            if (checkBoxRestart.Checked) {
                Application.Restart();
            }
        }// End CheckRestart

        private void buttonNotSend_Click(object sender, EventArgs e) {
            //
            // Check to see if the application should be
            // restarted.
            //
            CheckRestart();
        }

        private void ButtonSend_Click(object sender, EventArgs e) {
            // 
            // Send an email to Stinger Medical.
            //
            SendErrorReport();

            //
            // Check to see if the application should be
            // restarted.
            //
            CheckRestart();

        }

        private void LabelExceptionDate_Click(object sender, EventArgs e)
        {

        }

        private void labelTitle_Click( object sender, EventArgs e )
        {

        } // End CheckRestart()
    }
}