////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Inssider
{
    /// <summary>
    /// 
    /// </summary>
    public class ChannelView : UserControl
    {
        #region Private Data

        private int _leftMargin = 55;
        private int _rightMargin = 10;
        private int _topMargin = 20;
        private int _bottomMargin = 15;

        /// <summary> 
		/// Required designer variable.
		/// </summary>
        /// 
        private readonly Container _components;

        private List<ScannerNetwork> _networks;

        private BandType _band = BandType.Band2400MHz;

		private int _graphWidth;
		private int _graphHeight;
        
        private Color _gridColor = Color.FromArgb(100, Color.Gray);
        private Color _highChannelForeColor = Color.Gray;
        private Color _graphBackColor = Color.Black;

        private float _pixelsPerDbm = 1;
        private float _pixelsPerMHz = 1;

        private int _amplitudeLabelSpacing = 10;

        private float _maxAmplitude = -20;
        private float _minAmplitude = -100;

        private float _minFrequency = 2400;
        private float _maxFrequency = 2495;

        private Font _boldFont;

        #endregion

        public enum BandType
        {
            Band2400MHz = 2400,
            Band5000MHz = 5000
        }

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        public ChannelView()
        {
            _components = null;
            // This call is required by the Windows.Forms Form Designer.
            InitializeComponent();

            SetStyle(ControlStyles.UserPaint | ControlStyles.AllPaintingInWmPaint | ControlStyles.OptimizedDoubleBuffer, true);
            // TODO: Add any initialization after the InitializeComponent call

            _boldFont = new Font(Font, FontStyle.Bold);
        }

        #endregion


        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
		{
			if( disposing )
			{
				if(_components != null)
				{
					_components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.SuspendLayout();

            this.BackColor = System.Drawing.SystemColors.Control;
            this.Name = "ChannelView";
            this.Size = new System.Drawing.Size(400, 216);
            this.SizeChanged += new System.EventHandler(this.ChannelView_SizeChanged);

            this.ResumeLayout(false);
		}
		#endregion


        #region Public Properties

        /// <summary>
        /// List of networks
        /// </summary>
        [Category("Data")]
        public List<ScannerNetwork> Networks
        {
            get { return _networks; }
            set
            {
                _networks = value;
                Invalidate();
            }
        }

        public BandType Band
        {
            get { return _band; }
            set {
                _band = value;

                if (_band == BandType.Band5000MHz)
                {
                    _minFrequency = 5150F;
                    _maxFrequency = 5850F;
                }
                else
                {
                    _minFrequency = 2400F;
                    _maxFrequency = 2495F;
                }

                float viewableRange = _maxFrequency - _minFrequency + 1;
                _pixelsPerMHz = (float)_graphWidth / viewableRange;
            }
        }

        public int RightMargin
        {
            get { return _rightMargin; }
            set { _rightMargin = value; }
        }

        public int LeftMargin
        {
            get { return _leftMargin; }
            set { _leftMargin = value; }
        }

        public int TopMargin
        {
            get { return _topMargin; }
            set { _topMargin = value; }
        }

        public int BottomMargin
        {
            get { return _bottomMargin; }
            set { _bottomMargin = value; }
        }

        public float MaxAmplitude
        {
            get { return _maxAmplitude; }
            set { _maxAmplitude = value; }
        }

        public float MinAmplitude
        {
            get { return _minAmplitude; }
            set { _minAmplitude = value; }
        }

        public float MaxFrequency
        {
            get { return _maxFrequency; }
            set { _maxFrequency = value; }
        }

        public float MinFrequency
        {
            get { return _minFrequency; }
            set { _minFrequency = value; }
        }

        /// <summary>
        /// Color of the grid lines
        /// </summary>
        [Category("Appearance")]
        public Color GridColor
		{
			get { return _gridColor; }
			set 
			{ 
				_gridColor = value;  
			}
		}

        /// <summary>
        /// 
        /// </summary>
        [Category("Appearance")]
        public Color HighChannelForeColor
		{
            get { return _highChannelForeColor; }
			set 
			{ 
				_highChannelForeColor = value;
			}
		}

        /// <summary>
        /// 
        /// </summary>
        [Category("Appearance")]
        public Color GraphBackColor
		{
			get { return _graphBackColor; }
			set 
			{
				_graphBackColor = value; 
			}
		}

        public Bitmap GraphBitmap
        {
            get
            {
                Bitmap bitmap = new Bitmap(Width, Height);
                Graphics graphics = Graphics.FromImage(bitmap);
                graphics.Clear(BackColor);

                DrawView(graphics);
                return bitmap;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void ChannelView_SizeChanged(object sender, EventArgs e)
		{
			UpdateGraphDimensions();
			Invalidate();
		}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPaint(PaintEventArgs e)
        {
            DrawView(e.Graphics);
        }

        #endregion

        #region Private Methods

        private void UpdateGraphDimensions()
        {
            if ((Height > 0) && (Width > 0))
            {

                _graphWidth = (Width - _leftMargin - _rightMargin);
                _graphHeight = (Height - _topMargin - _bottomMargin);

                float viewableRange = _maxAmplitude - _minAmplitude + 1;
                _pixelsPerDbm = (float)_graphHeight / viewableRange;

                viewableRange = _maxFrequency - _minFrequency + 1;
                _pixelsPerMHz = (float)_graphWidth / viewableRange;
            }
        }

        private void DrawView(Graphics graphics)
        {
            DrawGrid(graphics);

            DrawLabels(graphics);

            // set cropping region
            graphics.Clip = new Region(new Rectangle(_leftMargin, _topMargin, _graphWidth, _graphHeight));

            DrawNetworks(graphics);

            // reset cropping region
            graphics.ResetClip();
        }

        /// <summary>
        /// Draws Wi-Fi network overlays
        /// </summary>
        /// <param name="graphics"></param>
        private void DrawNetworks(Graphics graphics)
        {
            if (_networks != null)
            {
                SolidBrush brush = new SolidBrush(Color.Red);

                lock (_networks)
                {
                    try
                    {
                        foreach (ScannerNetwork network in _networks)
                        {
                            if (!network.Visible) continue;
                            int x = _leftMargin + (int)((network.FrequencyMHz - _minFrequency) * _pixelsPerMHz);
                            int y = DbmToY(network.Signal);

                            Pen pen = new Pen(network.LineColor, 2);
                            if (network.Selected)
                            {
                                pen.Width = 4;
                            }

                            switch (network.Security)
                            {
                                case "None":
                                    pen.DashStyle = DashStyle.Dot;
                                    break;

                                case "WEP":
                                    pen.DashStyle = DashStyle.Dash;
                                    break;

                                default:
                                    pen.DashStyle = DashStyle.Solid;
                                    break;
                            }

                            int halfChannelWidthMhz;
                            if (network.Channel > 14)
                            {
                                halfChannelWidthMhz = 10;
                                x += (int)(3 * _pixelsPerMHz);
                            }
                            else
                            {
                                halfChannelWidthMhz = 11;
                            }

                            float floorY = (float)(_topMargin + _graphHeight);

                            // 802.11b arch shape
                            if ((network.Speed <= 20) && (network.FrequencyMHz < 2500))
                            {
                                PointF[] points = new PointF[3];
                                points[0] = new PointF(x - (halfChannelWidthMhz * _pixelsPerMHz), floorY);
                                points[1] = new PointF(x, y);
                                points[2] = new PointF(x + (halfChannelWidthMhz * _pixelsPerMHz), floorY);
                                graphics.DrawCurve(pen, points, 1);
                            }
                                // 802.11a/g/n plateau shape
                            else
                            {
                                PointF[] points = new PointF[5];

                                float quarterY = (floorY - y) / 4;

                                points[0] = new PointF(x - (halfChannelWidthMhz * _pixelsPerMHz), floorY);
                                points[1] = new PointF(x - ((halfChannelWidthMhz - 1) * _pixelsPerMHz), floorY - quarterY);
                                points[2] = new PointF(x - ((halfChannelWidthMhz - 1.5f) * _pixelsPerMHz), floorY - (2 * quarterY));
                                points[3] = new PointF(x - ((halfChannelWidthMhz - 1.5f) * _pixelsPerMHz), points[2].Y - 5);
                                points[4] = new PointF(x - ((halfChannelWidthMhz - 1.5f) * _pixelsPerMHz), y);
                                graphics.DrawCurve(pen, points, 0.3f);

                                PointF topleft = points[4];

                                points[0].X = x + halfChannelWidthMhz * _pixelsPerMHz;
                                points[1].X = x + (halfChannelWidthMhz - 1) * _pixelsPerMHz;
                                points[2].X = x + (halfChannelWidthMhz - 1.5f) * _pixelsPerMHz;
                                points[3].X = x + (halfChannelWidthMhz - 1.5f) * _pixelsPerMHz;
                                points[4].X = x + (halfChannelWidthMhz - 1.5f) * _pixelsPerMHz;
                                graphics.DrawCurve(pen, points, 0.2f);

                                graphics.DrawLine(pen, points[4], topleft);
                            }

                            string networkString = network.Ssid;

                            SizeF stringSize = graphics.MeasureString(networkString, Font);
                            x = x - (int)(stringSize.Width / 2f);
                            y -= 15;

                            brush.Color = network.LineColor;
                            if (network.Selected)
                            {
                                graphics.DrawString(networkString, _boldFont, brush, x, y);
                            }
                            else
                            {
                                graphics.DrawString(networkString, Font, brush, x, y);
                            }
                        }
                    }
                    catch (InvalidOperationException)
                    {
                        // occurs when collection is changed...
                    }
                }
            }
        }


        private void DrawGrid(Graphics graphics)
        {
            Pen pen = new Pen(ForeColor);
            SolidBrush brush = new SolidBrush(_graphBackColor);

            graphics.FillRectangle(brush, _leftMargin - 1, _topMargin - 1, _graphWidth + 1, _graphHeight);

            graphics.DrawRectangle(pen, _leftMargin - 1, _topMargin - 1, _graphWidth + 1, _graphHeight + 1);

            brush.Color = ForeColor;
            //graphics.DrawString(Localizer.GetString("ChannelView"), this.Font, brush, _leftMargin, 3);

            //Draw rotated line and text
            float y = (_graphHeight / 2) + graphics.MeasureString(Localizer.GetString("AmplitudedBm"), Font).Width / 2 + _topMargin;
            PointF rotationPoint = new PointF(8, y);
            Matrix matrix = new Matrix();
            matrix.RotateAt(270, rotationPoint);
            graphics.Transform = matrix;
            graphics.DrawString(Localizer.GetString("AmplitudedBm"), Font, brush, 8, y);
            matrix.RotateAt(90, rotationPoint);
            graphics.Transform = matrix;

            // Y axis
            float maxAmpToLabel = _maxAmplitude - (_amplitudeLabelSpacing / 3);
            int labelAmplitude = (int)(_minAmplitude - (_minAmplitude % _amplitudeLabelSpacing) + _amplitudeLabelSpacing);

            while (labelAmplitude < maxAmpToLabel)
            {
                // amplitude label
                y = _topMargin + _graphHeight - ((labelAmplitude - _minAmplitude) * _pixelsPerDbm);
                graphics.DrawString(labelAmplitude.ToString(), Font, brush, _leftMargin - 30, y - 7);

                // draw the horizontal graph lines
                pen.Color = _gridColor;
                pen.DashStyle = DashStyle.Dot;
                graphics.DrawLine(pen, _leftMargin, y, _leftMargin + _graphWidth, y);

                // Tick marks next to amplitude labels
                pen.Color = ForeColor;
                pen.DashStyle = DashStyle.Solid;
                graphics.DrawLine(pen, _leftMargin - 3, y, _leftMargin, y);

                labelAmplitude += _amplitudeLabelSpacing;
            }

            pen.Dispose();
        }

        // Draw the outline, labels, etc.
        private void DrawLabels(Graphics graphics)
        {
            SolidBrush brush = new SolidBrush(ForeColor);

            float x;
            int freq;

            // X axis labels
            int y = Height - _bottomMargin + 5;


            if (_band == BandType.Band2400MHz)
            {
            for (int channel = 0; channel < 14; channel++)
            {
                freq = 2412 + (5 * channel);
                if (channel == 13)
                {
                    freq += 7;
                }

                x = _leftMargin + (int)(_pixelsPerMHz * (freq - _minFrequency) + (_pixelsPerMHz * 0.5f));	// 11 for middle of channel, 1 for 2401 MHz offset
                brush.Color = ForeColor;

                if (channel < 9)
                {
                    graphics.DrawString((channel + 1).ToString(), Font, brush, x - 4, y);
                }
                else
                {
                    graphics.DrawString((channel + 1).ToString(), Font, brush, x - 8, y);
                }
            }
            }
            else
            {
                for (int channel = 36; channel <= 165; )
                {
                    freq = 5000 + (5 * channel);

                    x = _leftMargin + (int)(_pixelsPerMHz * (freq - _minFrequency) + (_pixelsPerMHz * 0.5f));
                    if (channel <= 64 || channel >= 149)
                    {
                        brush.Color = ForeColor;
                        graphics.DrawString((channel).ToString(), Font, brush, x - (channel > 99 ? 8 : 4), y);
                    }
                    else
                    {
                        brush.Color = HighChannelForeColor;
                        graphics.DrawString((channel).ToString(), Font, brush, x - (channel > 99 ? 8 : 4), y);
                    }

                    switch (channel)
                    {
                        case 64:
                            channel = 100;
                            break;
                        case 140:
                            channel = 149;
                            break;
                        default:
                            channel += 4;
                            break;
                    }
                }
            }
            brush.Dispose();
        }

        private int WifiChannelToX(uint channel)
        {
            if (_band == BandType.Band5000MHz) {
                return (_leftMargin + (int)(((5165 - _minFrequency) + (channel * 5)) * _pixelsPerMHz));
            }
            return (_leftMargin + (int)(((2407 - _minFrequency) + (channel * 5)) * _pixelsPerMHz));
        }

        private int DbmToY(int dbm)
        {
            return (int)(_topMargin + _graphHeight - ((dbm - _minAmplitude) * _pixelsPerDbm));
        }

        #endregion
    }
}
