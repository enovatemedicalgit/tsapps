////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

// ===================================================================
// This graph control was originally published by Stuart D. Konen 
// (skonen@gmail.com) as C2DPushGraph on December 2, 2006. 
//
// The control has since been heavily modified, ripped apart, and put
// back together for Inssider by Ryan Woodings at MetaGeek, LLC 
// (ryan@metageek.net).
// ===================================================================

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Diagnostics;


namespace Inssider
{
    public class TimeGraph : UserControl
    {
        #region Private Structs And Classes

        // ===================================================================
        // PRIVATE POINT CLASS (Contains Line Data Members)
        // ===================================================================
        internal struct Point
        {
            internal float Magnitude;
            internal bool Measured;
            internal DateTime TimeStamp;
        }


        // ===================================================================
        // INTERNAL LINE CLASS (Contains Line Data Members)
        // ===================================================================
        internal class Line
        {
            private readonly TimeGraph _owner;
            private Color _color = Color.Green;
            private readonly uint _numId;
            private bool _selected;
            private bool _visible = true;
            internal int DrawOrder;
            internal DashStyle DashStyle = DashStyle.Solid;
            internal bool StripeDashed;

            internal List<Point> PointsList = new List<Point>();


            internal Line(TimeGraph owner, uint id, Color color, bool dashed, bool striped) {
                StripeDashed = false;
                DrawOrder = 0;
                _selected = false;
                _numId = 0;
                _owner = null;
                _owner = owner;
                _numId = id;
                _color = color;
                DrawOrder = (int)_numId;
                if (dashed) {
                    DashStyle = DashStyle.DashDot;
                }
                StripeDashed = striped;
            }

            /// <summary> 
            /// Clears any currently displayed magnitudes.
            /// </summary>
            internal void Clear() {
                PointsList.Clear();
                _owner.Refresh();
            }

            #region Properties

            /// <summary>
            /// Gets the Id of this line
            /// </summary>
            internal uint Id {
                get { return _numId; }
            }

            /// <summary>
            /// Gets the timestamp of the last point in this line
            /// </summary>
            internal DateTime MaxTimeStamp {
                get
                {
                    return PointsList.Count > 0 ? PointsList[PointsList.Count - 1].TimeStamp : DateTime.MinValue;
                }
            }

            /// <summary>
            /// Gets the timestamp of the last point in this line
            /// </summary>
            internal DateTime MaxMeasuredTimeStamp {
                get
                {
                    if (PointsList.Count > 0) {
                        int i = PointsList.Count - 1;
                        while ((i >= 0) && !PointsList[i].Measured) {
                            i--;
                        }

                        if (i >= 0) {
                            return PointsList[i].TimeStamp;
                        }
                        return DateTime.MinValue;
                    }
                    return DateTime.MinValue;
                }
            }

            /// <summary>
            /// Gets the timestamp of the first point in this line
            /// </summary>
            internal DateTime MinTimeStamp {
                get
                {
                    if (PointsList.Count > 0) {
                        return PointsList[0].TimeStamp;
                    }
                    return DateTime.MinValue;
                }
            }

            /// <summary> 
            /// Gets or sets a value indicating whether the line is visible.
            /// </summary>
            internal bool Visible {
                get { return _visible; }
                set {
                    if (_visible != value) {
                        _visible = value;
                        _owner.Invalidate();
                    }
                }
            }

            /// <summary> 
            /// Sets or gets the line's current color.
            /// </summary>
            internal Color Color {
                set {
                    if (_color != value) {
                        _color = value;
                        _owner.Refresh();
                    }
                }
                get { return _color; }
            }

            /// <summary> 
            /// Sets or gets the line's thickness in pixels. NOTE: It is advisable
            /// to set HighQuality to false if using a thickness greater than
            /// 2 pixels as the antialiasing creates imperfections.
            /// </summary>
            internal bool Selected {
                set {
                    if (_selected != value) {
                        _selected = value;
                        _owner.Refresh();
                    }
                }
                get { return _selected; }
            }

            #endregion

            /// <summary> 
            /// Pushes a new Magnitude (point) to the line with the passed 
            /// numerical Id.
            /// </summary>
            /// <param name="magnitude">
            /// The Magnitude of the new point.
            /// </param>  
            /// <param name="time">
            /// The timestamp of the new point.
            /// </param>  
            /// <param name="measured">
            /// Whether this point was Measured, or estimated.
            /// </param>  
            internal void AddPoint(float magnitude, DateTime time, bool measured) {
                Point newPoint = new Point();
                newPoint.Magnitude = magnitude;
                newPoint.Measured = measured;
                newPoint.TimeStamp = time;

                if (FirstTimeStamp == DateTime.MinValue) {
                    FirstTimeStamp = time;
                }

                if (newPoint.TimeStamp > LatestTimeStamp) {
                    LatestTimeStamp = newPoint.TimeStamp;
                }

                PointsList.Add(newPoint);
            }

            internal void CropToTimes(DateTime minTime, DateTime staleTime) {
                // crop all data prior to minTime
                while ((PointsList.Count > 0) && (PointsList[0].TimeStamp < minTime)) {
                    PointsList.RemoveAt(0);
                }
                if (PointsList.Count > 0) {
                    // if this line is stale update it's last point's timestamp to now
                    if (!PointsList[PointsList.Count - 1].Measured) {
                        Point point = PointsList[PointsList.Count - 1];
                        point.TimeStamp = DateTime.Now;
                        PointsList[PointsList.Count - 1] = point;
                    }
                    // if the data just went stale create two points to mark the stale beginning and ending
                    else if (PointsList[PointsList.Count - 1].TimeStamp < staleTime) {
                        AddPoint(-100, PointsList[PointsList.Count - 1].TimeStamp.AddSeconds(1), false);
                        AddPoint(-100, DateTime.Now, false);
                    }
                }
            }
        }

        #endregion

        // ===================================================================
        // MAIN CONTROL CLASS
        // ===================================================================

        #region Private Data

        private Color _textColor = Color.Yellow;
        private Color _gridColor = Color.Green;
        private Color _graphBackColor = Color.Black;
        private bool _highQuality = true;
        private bool _autoScale;
        private bool _showLabels = true;
        private bool _showMinuteLines = true;
        private float _maxMagnitude = 100;
        private float _minMagnitude;
        private int _gridSpacing = 20;
        private int _leftMargin;
        private int _bottomMargin = 30;
        private int _rightMargin;
        private int _topMargin;

        private static TimeSpan _defaultTimeSpan = TimeSpan.FromMinutes(3);
        private TimeSpan _graphTimeSpan = _defaultTimeSpan;
        private float _pixelsPerMagnitude = 5;
        private Rectangle _clipRectangle = Rectangle.Empty;
        private bool _scanning;

        private readonly SortedList<UInt32, Line> _lines = new SortedList<UInt32, Line>();

        private System.ComponentModel.IContainer _components;

        internal static DateTime LatestTimeStamp = DateTime.MinValue;
        internal static DateTime FirstTimeStamp = DateTime.MinValue;
        internal static DateTime MinTimeInView = DateTime.MinValue;

        private static uint _lineWidth = 1;
        private static uint _selectedWidth = 3;

        private const double MinPixelsPerMinute = 35;
        private const int MaxMinutesToStore = 60;
        private const int MaxSecondsBeforeStale = 10;

        #endregion

        #region Constructors

        public TimeGraph() {
            _components = null;
            _scanning = false;
            _topMargin = 0;
            _rightMargin = 0;
            _leftMargin = 0;
            _minMagnitude = 0;
            _autoScale = false;
            InitializeComponent();
            InitializeStyles();
        }

        public TimeGraph(Form parent) {
            _components = null;
            _scanning = false;
            _topMargin = 0;
            _rightMargin = 0;
            _leftMargin = 0;
            _minMagnitude = 0;
            _autoScale = false;
            parent.Controls.Add(this);

            InitializeComponent();
            InitializeStyles();
        }

        public TimeGraph(Form parent, Rectangle rectPos) {
            _components = null;
            _scanning = false;
            _topMargin = 0;
            _rightMargin = 0;
            _leftMargin = 0;
            _minMagnitude = 0;
            _autoScale = false;
            parent.Controls.Add(this);

            Location = rectPos.Location;
            Height = rectPos.Height;
            Width = rectPos.Width;

            InitializeComponent();
            InitializeStyles();
        }

        #endregion

        public void Start()
        {
            _scanning = true;
        }

        public void Stop()
        {
            _scanning = false;
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (_components != null)) {
                _components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        // ===================================================================

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            _components = new System.ComponentModel.Container();
        }

        #endregion

        private void InitializeStyles() {
            BackColor = Color.Black;

            /* Enable double buffering and similiar techniques to 
             * eliminate flicker */

            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            DoubleBuffered = true;

            SetStyle(ControlStyles.ResizeRedraw, true);
        }

        #region Public Properties

        /// <summary> 
        /// Gets or sets the color of any text displayed in the graph (labels).
        /// </summary>
        public Color TextColor {
            get { return _textColor; }
            set {
                if (_textColor != value) {
                    _textColor = value;
                    Refresh();
                }
            }
        }

        /// <summary> 
        /// Gets or sets the graph's grid color.
        /// </summary>
        public Color GridColor {
            get { return _gridColor; }
            set {
                if (_gridColor != value) {
                    _gridColor = value;
                    Refresh();
                }
            }
        }

        /// <summary> 
        /// Gets or sets the graph's grid color.
        /// </summary>
        public Color GraphColor {
            get { return _graphBackColor; }
            set {
                if (_graphBackColor != value) {
                    _graphBackColor = value;
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the top graph margin
        /// </summary>
        public int TopMargin {
            get { return _topMargin; }
            set {
                if (_topMargin != value) {
                    _topMargin = value;
                    AdjustPixelsPerStep();
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the right graph margin
        /// </summary>
        public int RightMargin {
            get { return _rightMargin; }
            set {
                if (_rightMargin != value) {
                    _rightMargin = value;
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the right graph margin
        /// </summary>
        public int LeftMargin {
            get { return _leftMargin; }
            set {
                if (_leftMargin != value) {
                    _leftMargin = value;
                    Refresh();
                }
            }
        }

        /// <summary>
        /// Gets or sets the right graph margin
        /// </summary>
        public int BottomMargin {
            get { return _bottomMargin; }
            set {
                if (_bottomMargin != value) {
                    _bottomMargin = value;
                    AdjustPixelsPerStep();
                    Refresh();
                }
            }
        }

        /// <summary> 
        /// Gets or sets the number of grid lines to display.
        /// </summary>
        public int GridSpacing {
            get { return _gridSpacing; }
            set {
                if (_gridSpacing != value) {
                    _gridSpacing = value;
                    Refresh();

                }
            }
        }

        /// <summary>
        /// Gets or sets the width (in pixels) of each graphed line
        /// </summary>
        public uint LineWidth {
            get { return _lineWidth; }
            set { _lineWidth = value; }
        }

        /// <summary>
        /// Gets or sets the width (in pixels) of each graphed line that is marked as "selected"
        /// </summary>
        public uint SelectedWidth {
            get { return _selectedWidth; }
            set { _selectedWidth = value; }
        }

        /// <summary> 
        /// Gets or sets the maximum peek Magnitude of the graph, which should be
        /// the largest value you could potentially push to the graph. NOTE: If you
        /// have set AutoScale to true, this value will automatically adjust to
        /// the highest Magnitude pushed to the graph.
        /// </summary>
        public float MaxMagnitude {
            set {
                _maxMagnitude = value;
            }
            get { return _maxMagnitude; }
        }

        /// <summary> 
        /// Gets or sets the minimum Magnitude of the graph, which should be
        /// the smallest value you could potentially push to the graph.
        /// NOTE: If you have set AutoScale to true, this value will 
        /// automatically adjust to the lowest Magnitude pushed to the graph.
        /// </summary>
        public float MinMagnitude {
            set {
                _minMagnitude = value;
            }
            get { return _minMagnitude; }
        }

        /// <summary> 
        /// Gets or sets the value indicating whether the graph automatically
        /// adjusts MinMagnitude and MaxMagnitude to the lowest and highest
        /// values pushed to the graph.
        /// </summary>
        public bool AutoScale {
            set {
                if (_autoScale != value) {
                    _autoScale = value;
                    Refresh();
                }
            }
            get { return _autoScale; }
        }

        /// <summary> 
        /// Gets or sets the value indicating whether the graph is rendered in
        /// 'high quality' mode (with antialiasing). It is suggested that this property
        /// be set to false if you intend to display your graph using bar graph 
        /// styles, line thickness greater than two, or if maximum performance 
        /// is absolutely crucial.
        /// </summary>
        public bool HighQuality {
            set {
                if (value != _highQuality) {
                    _highQuality = value;
                    Refresh(); // Force redraw
                }
            }
            get { return _highQuality; }
        }

        /// <summary> 
        /// Gets or sets the value indicating whether the mimimum and maximum labels
        /// are displayed.
        /// </summary>
        public bool ShowLabels {
            set {
                if (_showLabels != value) {
                    _showLabels = value;
                    Refresh();
                }
            }
            get { return _showLabels; }
        }

        /// <summary>
        /// Gets or sets the value indicating whether the minute lines are 
        /// displayed on the graph
        /// </summary>
        public bool ShowMinuteLines {
            set {
                if (_showMinuteLines != value) {
                    _showMinuteLines = value;
                    Refresh();
                }
            }
            get { return _showMinuteLines; }
        }

        /// <summary>
        /// Gets or sests the default amount of time to display on the graph
        /// </summary>
        public TimeSpan GraphTimeSpan {
            set { _defaultTimeSpan = value; }
            get { return _defaultTimeSpan; }
        }

        public Bitmap GraphBitMap
        {
            get
            {
                Bitmap bitmap = new Bitmap(Width, Height);
                Graphics graphics = Graphics.FromImage(bitmap);
                graphics.Clear(BackColor);

                DrawGraph(ref graphics);

                return bitmap;
            }
        }

        #endregion

        #region Drawing Methods

        private void AdjustPixelsPerStep() {
            _pixelsPerMagnitude = (Height - _topMargin - _bottomMargin) / (_maxMagnitude - _minMagnitude);
        }

        protected override void OnSizeChanged(EventArgs e) {
            _clipRectangle = new Rectangle(_leftMargin, _topMargin, Width - _leftMargin - _rightMargin, Height - _topMargin - _bottomMargin);
            AdjustPixelsPerStep();
            Refresh();

            base.OnSizeChanged(e);
        }

        protected override void OnPaint(PaintEventArgs e) {
            Graphics g = e.Graphics;// 

            SmoothingMode prevSmoothingMode = g.SmoothingMode;

            DrawGraph(ref g);


            g.SmoothingMode = prevSmoothingMode;
        }

        private void DrawGraph(ref Graphics g) {
            g.SmoothingMode = (_highQuality ? SmoothingMode.HighQuality
                                              : SmoothingMode.Default);

            DrawGrid(ref g);

            if (LatestTimeStamp <= DateTime.MinValue + _defaultTimeSpan) {
                return;
            }

            MinTimeInView = FirstTimeStamp;
            if (LatestTimeStamp - _defaultTimeSpan < FirstTimeStamp) {
                _graphTimeSpan = _defaultTimeSpan;
            }
            else {
                _graphTimeSpan = LatestTimeStamp - FirstTimeStamp;
            }

            double minutesInView = (double)(Width - _leftMargin - _rightMargin) / MinPixelsPerMinute;
            if (_graphTimeSpan.TotalMinutes > minutesInView) {
                _graphTimeSpan = TimeSpan.FromMinutes(minutesInView);
                MinTimeInView = LatestTimeStamp - _graphTimeSpan;
            }

            if (_showMinuteLines) {
                DrawMinutes(ref g);
            }


            if (_clipRectangle != Rectangle.Empty) {
                g.SetClip(_clipRectangle, CombineMode.Replace);
            }

            DrawLines(ref g, false);
            DrawLines(ref g, true);
            g.ResetClip();
        }

        private void DrawGrid(ref Graphics g) {
            Pen pen = new Pen(ForeColor);
            SolidBrush brush = new SolidBrush(_graphBackColor);

            // fill graph background
            g.FillRectangle(brush, _leftMargin, _topMargin, Width - _leftMargin - _rightMargin, Height - _topMargin - _bottomMargin);

            // draw graph border
            g.DrawRectangle(pen, _leftMargin - 1, _topMargin, Width - _leftMargin - _rightMargin + 2, Height - _topMargin - _bottomMargin + 1);


            brush.Color = TextColor;

            //Draw Y-axis label
            SizeF labelSize = g.MeasureString(Localizer.GetString("AmplitudedBm"), Font);
            int y = (int)(((Height - _topMargin - _bottomMargin) / 2) + (labelSize.Width / 2) + _topMargin);
            PointF rotationPoint = new PointF(8, y);
            Matrix matrix = new Matrix();
            matrix.RotateAt(270, rotationPoint);
            g.Transform = matrix;
            g.DrawString(Localizer.GetString("AmplitudedBm"), Font, brush, 8, y);
            matrix.RotateAt(90, rotationPoint);
            g.Transform = matrix;

            // SJ - Added range to handle negative magnitudes
            float step = _gridSpacing;
            float magnitude = _minMagnitude + _gridSpacing;

            while (magnitude < _maxMagnitude) {

                y = Height - _bottomMargin - (int)Math.Ceiling(step * _pixelsPerMagnitude);

                String label = magnitude.ToString();
                labelSize = g.MeasureString(label, Font);

                g.DrawString(label, Font, brush, _leftMargin - 5 - labelSize.Width, y - labelSize.Height / 2);

                pen.Color = _gridColor;
                pen.DashStyle = DashStyle.Dot;
                g.DrawLine(pen, _leftMargin, y, Width - _rightMargin, y);

                pen.Color = ForeColor;
                pen.DashStyle = DashStyle.Solid;
                g.DrawLine(pen, _leftMargin - 3, y, _leftMargin, y);

                magnitude += _gridSpacing;
                step += _gridSpacing;
            }

            pen.Dispose();
            brush.Dispose();
        }

        private void DrawMinutes(ref Graphics g) {
            Pen pen = new Pen(ForeColor);
            SolidBrush brush = new SolidBrush(TextColor);
            String timeString;
            SizeF stringSize;

            float gridSpacing = (float)(Width - _leftMargin - _rightMargin) / (float)_graphTimeSpan.TotalMinutes;

            float offset = _leftMargin + (60 - MinTimeInView.Second) * gridSpacing / 60;
            float y = Height - _bottomMargin;

            //bool firstDrawn = false;
            for (int n = 0; n < (int)_graphTimeSpan.TotalMinutes + 1; n++) {
                int x = (int)(offset + (n * gridSpacing));
                if ((x > _leftMargin + 10) && (x < Width - _rightMargin - 10)) {
                    g.DrawLine(pen, x, y, x, y + 3);

                    timeString = MinTimeInView.AddMinutes(n + 1).ToShortTimeString();
                    //if (firstDrawn) {
                    timeString = timeString.Replace(Localizer.GetString("PM"), "");
                    timeString = timeString.Replace(Localizer.GetString("AM"), "");
                    //}
                    timeString = timeString.Trim();

                    stringSize = g.MeasureString(timeString, Font);
                    g.DrawString(timeString, Font, brush, x - stringSize.Width / 2, y + ((_bottomMargin - stringSize.Height) / 2));
                    //firstDrawn = true;
                }
            }

            pen.Dispose();
            brush.Dispose();
        }

        private void DrawLines(ref Graphics g, bool selected) {
            Pen pen = new Pen(Color.White);
            Pen borderPen = new Pen(Color.FromArgb(122, _graphBackColor));
            Pen backPen = new Pen(Color.FromArgb(242, 242, 242));

            float yScaler = (float)(Height - _topMargin - _bottomMargin) / (_maxMagnitude - _minMagnitude);
            float xScaler = (float)(Width - _leftMargin - _rightMargin) / (float)_graphTimeSpan.TotalSeconds; // graphTimeSpan.TotalSeconds;

            DateTime minTimeToStore = DateTime.Now.AddMinutes(-MaxMinutesToStore);
            DateTime staleTime = DateTime.Now.AddSeconds(-MaxSecondsBeforeStale);

            TimeSpan offset;
            GraphicsPath linePath;

            Line line;
            for (int i = 0; i < _lines.Count; i++) {
                //line = _lines.; // null;
                uint key = _lines.Keys[i];
                line = _lines[key];

                if ((line == null) || (!line.Visible) || (line.Selected != selected)) {
                    continue;
                }

                if (_scanning) {
                    line.CropToTimes(minTimeToStore, staleTime);
                }

                /* Now prepare to draw the line */
                pen.Color = line.Color;

                pen.Width = line.Selected ? _selectedWidth : _lineWidth;

                backPen.Width = pen.Width;
                borderPen.Width = pen.Width + 2;
                pen.DashStyle = line.DashStyle;

                float x = 0;
                float y = 0;
                linePath = new GraphicsPath();
                bool measuredLine = true;

                try
                {
                    for (int n = 0; n < line.PointsList.Count; n++)
                    {
                        if (line.PointsList[n].TimeStamp >= MinTimeInView)
                        {
                            offset = line.PointsList[n].TimeStamp - MinTimeInView;
                            float prevX = x;
                            float prevY = y;
                            x = _leftMargin + ((float)offset.TotalSeconds * xScaler);
                            y = Height - _bottomMargin - ((line.PointsList[n].Magnitude + Math.Abs(_minMagnitude)) * yScaler);
                            //y = Height - _bottomMargin - (line.PointsList[n].Magnitude  * yScaler);

                            if (line.PointsList[n].Measured == measuredLine)
                            {
                                linePath.AddLine(x, y, x, y);
                            }
                            else
                            {
                                if (!line.PointsList[n].Measured)
                                {
                                    // draw the estimated line
                                    if (linePath.PointCount > 0)
                                    {
                                        g.DrawPath(borderPen, linePath);
                                        if (line.StripeDashed)
                                        {
                                            g.DrawPath(backPen, linePath);
                                        }
                                        g.DrawPath(pen, linePath);
                                        linePath = new GraphicsPath();
                                    }

                                    pen.DashStyle = DashStyle.Dot;

                                    if (n > 0)
                                    {
                                        if (prevX != 0)
                                        {
                                            // if previous Measured point is in view add that point
                                            linePath.AddLine(prevX, prevY, prevX, prevY);
                                        }
                                        else
                                        {
                                            Debug.Assert(true);
                                        }
                                    }

                                    linePath.AddLine(x, y, x, y);
                                }
                                else
                                {
                                    linePath.AddLine(x, y, x, y);
                                    g.DrawPath(borderPen, linePath);
                                    if (line.StripeDashed)
                                    {
                                        g.DrawPath(backPen, linePath);
                                    }
                                    g.DrawPath(pen, linePath);

                                    linePath = new GraphicsPath();
                                    pen.DashStyle = line.DashStyle;

                                    linePath.AddLine(x, y, x, y);
                                }
                                measuredLine = line.PointsList[n].Measured;
                            }
                        }
                    }
                    g.DrawPath(borderPen, linePath);
                    if (line.StripeDashed)
                    {
                        g.DrawPath(backPen, linePath);
                    }
                    g.DrawPath(pen, linePath);
                }
                catch (OverflowException) { /* ignore, it should be OK next time */ }
            }
            pen.Dispose();
            backPen.Dispose();
            borderPen.Dispose();
        }

        #endregion

        /// <summary> 
        /// Adds a new line using the passed numeric Id as an identifier and sets
        /// the line's initial color to the passed color. If successful, returns
        /// a handle to the new line.
        /// </summary>
        /// <param name="numId">A unique numerical for the line you wish to create.</param>
        /// <param name="color">The line's initial color.</param>
        /// <param name="dashed">Whether or not the line is a dashed line.</param>
        /// <param name="striped"></param>
        internal bool AddLine(uint numId, Color color, bool dashed, bool striped) {
            bool status = false;
            // check for conflicting Id
            if (!_lines.ContainsKey(numId)) {
                Line newLine = new Line(this, numId, color, dashed, striped);
                _lines.Add(numId, newLine);
                status = true;
            }
            return status;
        }

        /// <summary>
        /// Clears all data from the graph
        /// </summary>
        internal void Clear() {
            ClearLines();
            ResetTimeValues();
        } // End Clear()

        /// <summary>
        ///  Removes all line datas from the graph
        /// </summary>
        internal void ClearLines() {
            if (null != _lines) {
                _lines.Clear();
                Invalidate();
            }
        }// End ClearLines

        /// <summary>
        /// Resets the internal time values.
        /// </summary>
        internal void ResetTimeValues() {
            _graphTimeSpan = _defaultTimeSpan;
            LatestTimeStamp = DateTime.MinValue;
            FirstTimeStamp = DateTime.MinValue;
            MinTimeInView = DateTime.MinValue;
        } // End ResetTimes()

        /// <summary> 
        /// Removes a line by its numerical Id.
        /// </summary>
        /// <param name="numId">
        /// The line's numerical Id.
        /// </param>  
        internal bool RemoveLine(uint numId) {
            return _lines.Remove(numId);
        }

        
        /// <summary>
        /// Returns a line with the specified id
        /// </summary>
        /// <param name="key">line id value</param>
        /// <returns>a line object or null</returns>
        internal Line this[uint key] {
            get {
                Line line = null;
                if (_lines.ContainsKey(key)) {
                    line = _lines[key];
                }
                return line;

            }
        }
    }
}
