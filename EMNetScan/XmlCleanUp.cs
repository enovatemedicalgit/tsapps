////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//      http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

// ===================================================================
// This code was originally written by:
// Joel Carpenter
// Brisbane, QLD
// AUSTRALIA
// ===================================================================

using System.Text;


namespace Inssider
{
   class XmlCleanUp
   {
      /// <summary>
      /// Replaces non-ASCII or forbidden XML characters with escape sequences or unicode
      /// </summary>
      /// <param name="input">String to be reformatted. Usually an Ssid.</param>
      public static string CleanUp(string input)
      {
         for (int j = 0; j < input.Length; j++)
         {
            byte            charByte = (byte) input[j];

            // If it's not a standard ascii character or if its the & or < symbols
            if (! (charByte >= 32 && charByte <= 126 && charByte != 38 && charByte != 60))
            {
               switch (charByte)
               {
                  case 38:           // no ampersands
                     input = input.Substring(0, j) + "&amp;" + input.Substring(j + 1);
                     j += 4;
                  break;
                  case 60:           // no less thans
                     input = input.Substring(0, j) + "&lt;" + input.Substring(j + 1);
                     j += 3;
                  break;
                  default:           // all other weird characters change directly to unicode
                     byte[] tempByte = Encoding.Unicode.GetBytes(new char [] { input[ j ] } );
                     string unicodeValue = System.Convert.ToString(tempByte[0] + (tempByte[1] << 8));

                     string output = "&amp;#" + unicodeValue + ";";
                     input = input.Substring(0, j) + output + input.Substring(j + 1);
                     j += output.Length - 1;
                  break;
               }
            }
         }
         return input;
      }
   }
}

