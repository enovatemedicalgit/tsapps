using System;
using System.Collections.Generic;
using System.Text;
using System.Management;
using System.Runtime.InteropServices;


namespace Inssider
{
   class WmiWirelessInterface: WirelessInterface
   {
      string _adapterName = String.Empty;
      ManagementObject _adapterObject = null;

      public WmiWirelessInterface()
:
      base()
      {
         _adapterObject = GetAdapter();
         if (null == _adapterObject)
         {
            throw new ApplicationException("Unable to find a suitable wireless interface");
         }
         _adapterName = (string) _adapterObject["InstanceName"];
      }
      // End WmiWirelessInterface()




      public override void ScanNetworks()
      {
         // direct access instead of a WQL query
         ManagementClass mc = new ManagementClass("root\\WMI", "MSNDis_80211_BssIdListScan", null);
         ManagementObject mo = mc.CreateInstance();

         mo["Active"] = true;
         mo["InstanceName"] = _adapterName;
         mo["UnusedParameter"] = 1;
         mo.Put();
      }




      /// <summary>
      /// Returns the first active adapter
      /// </summary>
      /// <returns>WMI ManagementObject for the adapter</returns>
      private ManagementObject GetAdapter()
      {
         ManagementClass mc = new ManagementClass("root\\WMI", "MSNdis_80211_BSSILIST", null);
         ManagementObjectCollection moc = mc.GetInstances();

         foreach(ManagementObject adapterObject in moc)
         {
            bool active = (bool) adapterObject["Active"];
            if (active)
            {
               return (adapterObject);
            }
         }
         return (null);
      }
      // End GetAdapter()




      public override List < WirelessDetails > GetWirelessDetails()
      {
         List < WirelessDetails > detailList = new List < WirelessDetails > ();

         try
         {
            // ManagementObject adapter = GetAdapter();

            //Note that the "root/WMI" namespace must be searched in all cases below.
            //
            // To get the signal strength of the Connected Wireless Access point:
            // Select from "MSNdis_80211_ReceivedSignalStrength" and examine the
            // "Ndis80211ReceivedSignalStrength" property.
            //
            // To get the MAC Address of the Connected Wireless Access point:
            // Select from "MSNdis_80211_BaseServiceSetIdentifier" and examine the
            // "Ndis80211MacAddress" property.
            //
            // To get the SSID (the name) of the Connected Wireless Access point:
            // Select from "MSNdis_80211_ServiceSetIdentifier" and examine the
            // "Ndis80211SsId" property.

            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI", @"SELECT * FROM MSNdis_80211_BSSILIST WHERE Active = 'True'");
            ManagementObjectCollection bssidList = searcher.Get() as ManagementObjectCollection;

            // ssids will be null if the result of the Get() call wasn't convertible to a ManagementObjectCollection
            //if (bssidList != null)  {

            foreach(ManagementBaseObject ssid in bssidList)
            {
               UInt32 numberOfItems = (UInt32) ssid["NumberOfItems"];
               PropertyDataCollection props = ssid.Properties;

               foreach(PropertyData data in props)
               {
                  Console.WriteLine("{0} = {1}, Type = {2}", data.Name, data.Value, data.Type);
               }
               ManagementBaseObject[] bssList = ssid["Ndis80211BSSIList"] as ManagementBaseObject[];
               ManagementBaseObject obj = bssList[0];

               Console.WriteLine(obj);
               IntPtr          ptr = (IntPtr) obj;

               for (int i = 0; i < numberOfItems; ++i)
               {
                  Int64           address = ptr.ToInt64();
                  ManagementObject baseObject = (ManagementObject) Marshal.PtrToStructure(
                                                                                          new IntPtr(address), typeof(ManagementObject));

                  //ManagementBaseObject baseObject = (ManagementBaseObject)Marshal.
                  props = baseObject.Properties;
                  foreach(PropertyData data in props)
                  {
                     Console.WriteLine("{0} = {1}, Type = {2}", data.Name, data.Value, data.Type);
                  }
                  address += (Int32) baseObject["Ndis80211WLanBssIdLength"];
                  ptr = new IntPtr(address);
                  ManagementClass mc = new ManagementClass();
               }
            }
            //}

         }
         catch(Exception ex)
         {
            throw new ApplicationException("WMI Error", ex);
         }
         return (detailList);
      }




      private void ManagementQuery()
      {
         //ManagementClass mc = new ManagementClass("root\\WMI", "MSNdis_80211_ServiceSetIdentifier", null);
         ManagementClass mc = new ManagementClass("root\\WMI", "MSNdis_80211_BSSILIST", null);
         ManagementObjectCollection moc = mc.GetInstances();

         foreach(ManagementObject mo in moc)
         {
            Console.WriteLine("\n\n--------------------- MSNdis_80211_BSSILIST --------------------------");
            foreach(PropertyData data in mo.Properties)
            {
               Console.WriteLine(String.Format("{0} = {1}", data.Name, data.Value));
            }
            Console.WriteLine("\n--------------------- Ndis80211BSSIList --------------------------");
            ManagementBaseObject[] bssiList = (ManagementBaseObject[]) mo["Ndis80211BSSIList"];
            foreach(ManagementBaseObject baseObject in bssiList)
            {
               PropertyDataCollection properties = baseObject.Properties;

               foreach(PropertyData data in properties)
               {
                  Console.WriteLine(String.Format("{0} = {1}", data.Name, data.Value));
               }
               Console.WriteLine("\n--------------------- Ndis80211Configuration --------------------------");
               ManagementBaseObject configuration = (ManagementBaseObject) baseObject["Ndis80211Configuration"];
               properties = configuration.Properties;
               foreach(PropertyData data in properties)
               {
                  Console.WriteLine(String.Format("{0} = {1}", data.Name, data.Value));
               }
               Console.WriteLine("\n--------------------- Ndis80211InfrastructureMode --------------------------");
               ManagementBaseObject infrastructureMode = (ManagementBaseObject) baseObject["Ndis80211InfrastructureMode"];
               properties = infrastructureMode.Properties;
               foreach(PropertyData data in properties)
               {
                  Console.WriteLine(String.Format("{0} = {1}", data.Name, data.Value));
               }
               Console.WriteLine("\n--------------------- Ndis80211NetworkTypeInUse --------------------------");
               ManagementBaseObject networkType = (ManagementBaseObject) baseObject["Ndis80211NetworkTypeInUse"];
               properties = networkType.Properties;
               foreach(PropertyData data in properties)
               {
                  Console.WriteLine(String.Format("{0} = {1}", data.Name, data.Value));
               }
               Console.WriteLine();
            }
         }
      }
   }
}

