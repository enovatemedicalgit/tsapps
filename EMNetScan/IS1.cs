﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Inssider
{
    public class IS1
    {
        private static int FileVersion = 1;
        internal static List<ScannerNetwork> ReadFile(string filename)
        {
            List<ScannerNetwork> outdata = new List<ScannerNetwork>();
            //FileStream indata = new FileStream(filename, FileMode.Open);
            //byte[] buffer;
            byte[] indata = System.IO.File.ReadAllBytes(filename);
            //indata.Read(
            if (System.Text.Encoding.ASCII.GetString(indata,0,8) == "inSSIDer" )
            {
                ScannerNetwork.ResetNextId();
                //File version
                switch (indata[8])
                {
                    case 1:
                        int curEntry = 0;
                        int maxEntry = BitConverter.ToInt32(indata, 9);
                        
                        for (int i = 13; i < indata.Length; curEntry++)
                        {
                            if (curEntry > maxEntry) break;
                            ScannerNetwork newNet = new ScannerNetwork();

                            newNet.MacAddress = new MacAddress(new byte[] { indata[i], indata[i + 1], indata[i + 2], indata[i + 3], indata[i + 4], indata[i + 5] });
                            i+= 6;

                            newNet.SSID = System.Text.Encoding.ASCII.GetString(indata, i + 1, indata[i]);
                            i += (int)indata[i]+1;

                            newNet.Channel = BitConverter.ToUInt32(indata, i);
                            i += 4;

                            newNet.Signal = BitConverter.ToInt32(indata, i);
                            i += 4;

                            newNet.Security = System.Text.Encoding.ASCII.GetString(indata, i + 1, indata[i]);
                            i += (int)indata[i]+1;

                            newNet.NetworkType = System.Text.Encoding.ASCII.GetString(indata, i + 1, indata[i]);
                            i += (int)indata[i] + 1;

                            newNet.Speed = BitConverter.ToSingle(indata, i);
                            i += 4;

                            newNet.FirstSeenTimeStamp = DateTime.FromFileTime(BitConverter.ToInt64(indata, i));
                            i += 8;

                            newNet.LastSeenTimeStamp = DateTime.FromFileTime(BitConverter.ToInt64(indata, i));
                            i += 8;

                            newNet.Latitude = BitConverter.Int64BitsToDouble(BitConverter.ToInt64(indata, i));
                            i += 8;

                            newNet.Longitude = BitConverter.Int64BitsToDouble(BitConverter.ToInt64(indata, i));
                            i += 8;

                            outdata.Add(newNet);
                            
                        }
                        break;
                    default:
                        break;
                }
            }
            return outdata;
        }

        internal static byte[] WriteToByteArray(List<ScannerNetwork> data)
        {
            //Stopwatch sw = new Stopwatch();
            //sw.Start();
            List<byte> outdata = new List<byte>();
            //Header
            outdata.AddRange(System.Text.Encoding.ASCII.GetBytes("inSSIDer"));
            //File Version
            outdata.Add((byte)FileVersion);
            //AP count
            outdata.AddRange(BitConverter.GetBytes(data.Count));
            foreach (ScannerNetwork item in data)
            {
                //BSSID
                outdata.AddRange(item.MacAddress.Bytes);
                //SSID length
                outdata.Add((byte)item.SSID.Length);
                //SSID
                outdata.AddRange(System.Text.Encoding.ASCII.GetBytes(item.SSID));
                //Channel
                outdata.AddRange(BitConverter.GetBytes(item.Channel));
                //Signal
                outdata.AddRange(BitConverter.GetBytes(item.Signal));
                //Security string length
                outdata.Add((byte)item.Security.Length);
                //Security string
                outdata.AddRange(System.Text.Encoding.ASCII.GetBytes(item.Security));
                //Network type string length
                outdata.Add((byte)item.NetworkType.Length);
                //Network type string
                outdata.AddRange(System.Text.Encoding.ASCII.GetBytes(item.NetworkType));
                //Speed
                outdata.AddRange(BitConverter.GetBytes(item.Speed));
                //FirstSeen
                outdata.AddRange(BitConverter.GetBytes(item.FirstSeenTimeStamp.ToFileTimeUtc()));
                //LastSeen
                outdata.AddRange(BitConverter.GetBytes(item.LastSeenTimeStamp.ToFileTimeUtc()));
                //Latitude
                outdata.AddRange(BitConverter.GetBytes(BitConverter.DoubleToInt64Bits(item.Latitude)));
                //Longitude
                outdata.AddRange(BitConverter.GetBytes(BitConverter.DoubleToInt64Bits(item.Longitude)));
            }
            //sw.Stop();
            return outdata.ToArray();
        }

        internal static void WriteFile(List<ScannerNetwork> data,string Filename)
        {
            System.IO.File.WriteAllBytes(Filename, WriteToByteArray(data));
        }
    }
}
