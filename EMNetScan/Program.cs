////////////////////////////////////////////////////////////////
//
// Copyright 2007-2010 MetaGeek LLC 
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Windows.Forms;

namespace Inssider
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("inSSIDer {0} starting up", VersionInfo.VersionDescription);
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            ScannerForm mainForm = null;

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Create new instance of UnhandledExceptionDlg:
            // NOTE: this hooks up the exception handler functions 
            UnhandledExceptionDlg exDlg = new UnhandledExceptionDlg();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Add handling of OnShowErrorReport.
            // If you skip this then link to report details won't be showing.
            exDlg.OnShowErrorReportClick += delegate(object sender, SendExceptionClickEventArgs ar)
            {
                MessageBox.Show("[Message]\r\n" + ar.UnhandledException.Message + "\r\n\r\n"
                     + "[Version]\r\n" + Application.ProductVersion + "\r\n\r\n"
                     + "[WinVer]\r\n" + Environment.OSVersion.VersionString + "\r\n\r\n"
                     + "[Platform]\r\n" + Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") + "\r\n\r\n"
                     + "[StackTrace]\r\n" + ar.UnhandledException.StackTrace + "\r\n\r\n");
            };

            // Implement your sending protocol here. You can use any information from System.Exception
            exDlg.OnSendExceptionClick += delegate(object sender, SendExceptionClickEventArgs ar)
            {
                // User clicked on "Send Error Report" button:
                if (ar.SendExceptionDetails)
                {
                    String body = "[Description]\r\n\r\n" + ar.UserDescription + "\r\n\r\n";
                    body += "[Message]\r\n" + ar.UnhandledException.Message + "\r\n\r\n";
                    body += "[Version]\r\n" + Application.ProductVersion + "\r\n\r\n";
                    body += "[WinVer]\r\n" + Environment.OSVersion.VersionString + "\r\n\r\n";
                    body += "[Platform]\r\n" + Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") + "\r\n\r\n";
                    body += "[StackTrace]\r\n" + ar.UnhandledException + "\r\n\r\n";

                    MapiMailMessage message = new MapiMailMessage("Inssider Error Report", body);
                    message.Recipients.Add("error.reports@metageek.net");                    
                    message.ShowDialog(true);
                }
                Application.Exit();
            };

            exDlg.OnCopyToClipboardClick += delegate(object sender, SendExceptionClickEventArgs ar)
            {
                String body = "[Description]\r\n\r\n" + ar.UserDescription + "\r\n\r\n";
                body += "[Message]\r\n" + ar.UnhandledException.Message + "\r\n\r\n";
                body += "[Version]\r\n" + Application.ProductVersion + "\r\n\r\n";
                body += "[WinVer]\r\n" + Environment.OSVersion.VersionString + "\r\n\r\n";
                body += "[Platform]\r\n" + Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE") + "\r\n\r\n";
                body += "[StackTrace]\r\n" + ar.UnhandledException + "\r\n\r\n";

                if (mainForm != null)
                {
                    mainForm.StringToClipboard(body);
                }
            };

            try
            {
                mainForm = new ScannerForm();
                Application.Run(mainForm);
            }
            catch (ObjectDisposedException) { /* Ignore */  }
            //Console.ReadKey();
        }

    }
}