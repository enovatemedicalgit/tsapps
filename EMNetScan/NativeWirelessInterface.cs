////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using NativeWifi;
using System.Threading;
using System.Diagnostics;
using System.Net.NetworkInformation;

namespace Inssider
{
    /// <summary>
    /// This class is a thin wrapper around the NativeWifi interface.
    /// </summary>
    class NativeWirelessInterface : WirelessInterface
    {
        /// <summary>
        /// Gets the name of the interface
        /// </summary>
        public override string Name
        {
            get
            {
                return _networkInterface.Name;
            }
        }

        /// <summary>
        /// Gets the description of the interface
        /// </summary>
        public override string Description
        {
            get
            {
                return _networkInterface.Description;
            }
        }

        /// <summary>
        /// Native wireless client object
        /// </summary>
        private readonly WlanClient _client;        

        /// <summary>
        /// Network interface
        /// </summary>
        private readonly WlanClient.WlanInterface _interface;

        /// <summary>
        /// Underlying network interface
        /// </summary>
        private readonly NetworkInterface _networkInterface;

        /// <summary>
        /// Notifies the process when a scan is complete
        /// </summary>
        private readonly AutoResetEvent _scanComplete = new AutoResetEvent(true);

        #region Constructors

        /// <summary>
        /// Creates a NativeWirelessInterface object that acts as an adapter
        /// to the Native Wlan API.
        /// </summary>        
        public NativeWirelessInterface(NetworkInterface networkInterface) {
            //
            // Ensure a valid interface has been specified.
            //
            if (null == networkInterface) {
                throw new ArgumentNullException(Localizer.GetString("InvalidNetworkInterface"));
            }

            //
            // Initialize the underlying client interface.
            //
            _client = new WlanClient();
            if ( null != _client.Interfaces ) {
                foreach (WlanClient.WlanInterface netif in _client.Interfaces) {
                    String interfaceName;
                    try {
                        interfaceName = netif.InterfaceName;
                    }
                    catch (Exception) {
                        //
                        // Sometims the native wifi API will throw an exception
                        // here because the InterfaceName accessor does not always
                        // find the corresponding interface. There may be a race condition
                        // here that needs to be investigated. For now, the exception
                        // is being delt with here to see if another underlying problem
                        // will expose itself.
                        //
                        interfaceName = null;
                    }

                    //
                    // If we have found the matching interface, subscribe to its
                    // notification event handler.
                    //
                    if ( null != interfaceName && interfaceName.Equals(networkInterface.Name)) {
                        _interface = netif;
                        _interface.WlanNotification += WlanNotificationHandler;
                        break;
                    }
                }
            }

            if ( null == _interface ) {
                throw new ApplicationException(Localizer.GetString("InvalidNetworkInterface"));
            }

            _networkInterface = networkInterface;

        } // End NativeWirelessInterface() 

        #endregion

        #region Methods

        /// <summary>
        /// Perorms a network scan
        /// </summary>
        public override void ScanNetworks() {
            //
            // Only allow one scan operation to be in
            // progress at any given time.
            //
            if (_scanComplete.WaitOne(0, false)) {                
                // Begin the scan operation
                _interface.Scan();                
            }
        } // End ScanNetworks()

        /// <summary>
        /// Attempts to find an AvailableNetwork with the given ssid.
        /// </summary>
        /// <param name="ssid">Ssid to find</param>
        /// <param name="networks">array of available networks</param>
        /// <param name="foundNetwork">the network that was found</param>
        /// <returns>true if the Ssid was found, false otherwise</returns>
        private bool FindNetwork(string ssid, IEnumerable<Wlan.WlanAvailableNetwork> networks, ref Wlan.WlanAvailableNetwork foundNetwork) {
            if (null != networks) {
                foreach (Wlan.WlanAvailableNetwork network in networks) {
                    string ssidText = Encoding.ASCII.GetString(network.Dot11Ssid.Ssid, 0,
                        (int)network.Dot11Ssid.SsidLength);
                    if (!ssidText.Equals(ssid)) continue;
                    foundNetwork = network;
                    return (true);
                }
            }
            return (false);
        } // End FindNetwork

        /// <summary>
        /// Returns a list of WirelessDetails objects, which contain
        /// information about the wirless networks that were discovered
        /// during the last scan operation.
        /// </summary>
        /// <returns>a list of WirelessDetails objects</returns>
        public override List<WirelessDetails> GetWirelessDetails() {
            List<WirelessDetails> detailList = new List<WirelessDetails>();

            //
            // Get a list of the BSSIDs
            //
            Wlan.WlanBssEntry[] bssList = _interface.GetNetworkBssList();

            //
            // Get a list of the available networks, so we can determine the 
            // authorization algorithm.
            //
            Wlan.WlanAvailableNetwork[] networkList = _interface.GetAvailableNetworkList(
                Wlan.WlanGetAvailableNetworkFlags.IncludeAllManualHiddenProfiles);

            if (null != bssList && null != networkList) {
                Wlan.WlanAvailableNetwork network = new Wlan.WlanAvailableNetwork();

                foreach (Wlan.WlanBssEntry bssEntry in bssList) {
                    string ssid = Encoding.ASCII.GetString(bssEntry.Dot11Ssid.Ssid, 0,
                        (int)bssEntry.Dot11Ssid.SsidLength);
                    //
                    // Attempt to find the network
                    //
                    bool found = FindNetwork(ssid, networkList, ref network);
                    if (found) {
                        WirelessDetails detailItem = new WirelessDetails(bssEntry.Dot11Bssid);
                        ConvertToMbs(bssEntry.WlanRateSet.Rates, detailItem.Rates, bssEntry.Dot11BssPhyType == Wlan.Dot11PhyType.Ht);
                        // 
                        // Compute the RSSI value. On some systems, the bssEntry.RSSI 
                        // value seems to be inaccurate, so we will base the RSSI value
                        // on the link quality.
                        //
                        int rssi = ComputeRssi(bssEntry.LinkQuality);
                        detailItem.Rssi = (bssEntry.Rssi > rssi ? bssEntry.Rssi : rssi);
                        detailItem.Ssid = ssid;
                        detailItem.Channel = ConvertToChannel(bssEntry.ChCenterFrequency);
                        detailItem.NetworkType = bssEntry.Dot11BssType.ToString();
                        detailItem.Privacy = CreatePrivacyString(network.Dot11DefaultAuthAlgorithm,
                            network.Dot11DefaultCipherAlgorithm);
                        detailItem.InfrastructureMode = ConvertToString(bssEntry.Dot11BssType);
                        detailItem.SignalQuality = network.WlanSignalQuality;

                        detailList.Add(detailItem);
                    }
                }
            }
            return (detailList);
        } // End QueryBssList()
        
        /// <summary>
        /// Computes the RSSI dBM value based on the signal quality.
        /// The signal quality is a percentage value between 0 and 100,
        /// where 0 equals -100 dBm and 100 equals -50 dBm
        /// </summary>
        /// <param name="linkQuality"></param>
        /// <returns></returns>
        private int ComputeRssi(uint linkQuality) {
            // 
            // Make sure the link quality is within the range of [0..100]
            //
            if (linkQuality > 100) {
                linkQuality = 100;
            }
            //
            // Compute the RSSI based on the link quality. Link quality
            // is a percentage value between [0..100], where 0 equals -100dBm
            // and 100 equals -50dBm.
            // 
            return (int)(.5 * linkQuality - 100);
        } // End ComputeRssi()

        /// <summary>
        /// Creates a readable string based on the encryption algorithm and
        /// the authentication method
        /// </summary>
        /// <param name="authentication">authentication used</param>
        /// <param name="encryption">encryption used</param>
        /// <returns>a string representing the privacy mode</returns>
        private string CreatePrivacyString(Wlan.Dot11AuthAlgorithm authentication,
            Wlan.Dot11CipherAlgorithm encryption){

            String text = authentication + "-" + encryption;
            text = text.Replace("_PSK", "");
            text = text.Replace("IEEE80211_", "");
            text = text.Replace("Open", "");
            text = text.Trim(new char[]{ '-' });
            if (null == text || text.Equals(String.Empty)) {
                text = "None";
            }
            return text;
        } // End CreatePrivacyString()
           
        /// <summary>
        /// Converts the bssType to a readable string
        /// </summary>
        /// <param name="bssType">type to convert to a string</param>
        /// <returns>string representing the bss type</returns>
        private string ConvertToString(Wlan.Dot11BssType bssType) {
            string bssText;
            switch (bssType) {
                case Wlan.Dot11BssType.Infrastructure:
                    bssText = Localizer.GetString("Infrastructure");
                    break;
                case Wlan.Dot11BssType.Independent:
                    bssText = Localizer.GetString("Adhoc");
                    break;
                case Wlan.Dot11BssType.Any:
                    bssText = Localizer.GetString("AutoUnknown");
                    break;
                default:
                    bssText = Localizer.GetString("Unknown");
                    break;
            }

            return (bssText);
        }// End ConvertToString()

        /// <summary>
        /// Handles notification events generated by the native wlan 
        /// interface.        
        /// </summary>
        /// <param name="notifyData">notification information about the event</param>
        private void WlanNotificationHandler(Wlan.WlanNotificationData notifyData) {
            if (notifyData.NotificationSource == Wlan.WlanNotificationSource.Acm) {
                Wlan.WlanNotificationCodeAcm acmCode = (Wlan.WlanNotificationCodeAcm)notifyData.NotificationCode;
                if (acmCode == Wlan.WlanNotificationCodeAcm.ScanComplete ||
                    acmCode == Wlan.WlanNotificationCodeAcm.ScanFail) {
                    _scanComplete.Set();
                }
            }
            Debug.WriteLine(notifyData.NotificationCode.ToString());
        }// End WlanNotificationHandler

        #endregion
    }
}
