////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//      http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

// ===================================================================
// This code was originally written by:
// Charles Putney
// 18 Quinns Road
// Shankill
// Co. Dublin
// IRELAND
//
// Charles released the code to CodeProject on June 22, 2007
// Norman Rasmussed updated the code to add Native Wi-Fi API support
// (Charles had used the "netscan" command) on August 10, 2007
//
// The code has since been heavily modified and specialized for Inssider
// by Ryan Woodings at MetaGeek, LLC (ryan@metageek.net). Modifications
// include adding the graph, and changing the data table from a ListView
// to a DataGridView to allow better data manipulation (sorting, etc)
// ===================================================================

using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Data;
using System.Drawing;
using System.Text;
using System.Timers;
using System.Net.NetworkInformation;
using Inssider.Properties;
using Microsoft.Win32;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;

namespace Inssider
{
    public delegate void NetworkScanStateHandler(bool enabled);

    public partial class ScannerForm : Form
    {
        #region Constants
        private const int MinNonGridVertical = 262;
        private const int UpdateHighlightInterval = 300;
        private const int UpdateListInterval = 2500;
        #endregion




        #region Private Variables
        // UI network data
        private readonly List<ScannerNetwork> _networks = new List<ScannerNetwork>();
        // network scanning thread
        private Thread _scanThread;
        // timestamp for the last scan
        private DateTime _lastScanTimeStamp = DateTime.MinValue;
        // timestamp for when the last data was updated on the UI
        private DateTime _lastDrawnTimeStamp = DateTime.MinValue;
        private bool bRememberMe;
        private string sUsername = "";
        private string sPassword = "";
        private int iUserID = 0;
        private string sSiteID = "";
        private bool bLoggedIn;
        // timer for updating the UI
        private readonly System.Timers.Timer _viewUpdateTimer;
        // flag for starting a new selection
        private bool _newSelection;
        private bool _ignoreSelection;
        // flag for automatically resizing grid
        private bool _autoResize = true;
        // application abort flag
        private bool _abort;
        // currently selected network interface
        private NetworkInterface _interface;
        // Scanner thread terminate event
        private readonly AutoResetEvent _terminateNetworkScan = new AutoResetEvent(false);
        // flag for catching suspend/resume
        private bool _scanSuspended;
        readonly ElapsedEventHandler _updateViewHandler;
        readonly ElapsedEventHandler _updateListHandler;
        // Checkbox for the "select all networks"
        private CheckBox _selectAllNetworksCheckBox;
        //To delay set window location settings
        private bool _canSet;
        // XML Log File
        private XmlDocument _dataLog;
        private string _logFilename;
        private const string XmlHeader = @"<?xml version=""1.0""?><gpx version=""1.0"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns=""http://www.topografix.com/GPX/1/0"" xmlns:topografix=""http://www.topografix.com/GPX/Private/TopoGrafix/0/1"" xsi:schemaLocation=""http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd http://www.topografix.com/GPX/Private/TopoGrafix/0/1 http://www.topografix.com/GPX/Private/TopoGrafix/0/1/topografix.xsd""></gpx>";
        private DateTime _logTime = DateTime.Now;
        private readonly Gps _gps;
        private Vendors _vendors = new Vendors();
        #endregion




        #region Constructors
        /// <summary>
        /// Creates a new instance of the ScannerForm, which is the main application
        /// form.
        /// </summary>
        public ScannerForm()
        {
            _gps = null;
            _dataLog = null;
            _canSet = false;
            _scanSuspended = false;
            _interface = null;
            _abort = false;
            _ignoreSelection = false;
            _newSelection = false;
            String culture = String.Empty;
            //culture = "es-es";
            //culture = "zh-TW";
            //culture = "ja-JP";
            //culture = "de-DE";
            //culture = "sv-SE";
            //culture = "ru-RU";

            if (!string.IsNullOrEmpty(culture))
            {
                CultureInfo ci = new CultureInfo(culture);

                // set culture for formatting
                Thread.CurrentThread.CurrentCulture = ci;
                // set culture for resources
                Thread.CurrentThread.CurrentUICulture = ci;
            }

            //
            // Check for the correct operating environment
            //
            CheckEnvironment();

            //
            // Initialize components on the form.
            //
            InitializeComponent();
            _gps = new Gps();
            _gps.GpsTimeout += GpsTimeout;
            _gps.GpsUpdated += GpsUpdated;
            //SelectStartImage();
#if CRASH
         crashToolStripMenuItem.Visible = true;
         crashToolStripMenuItem.Enabled = true;
#endif
            UpdateNetworkScanState(false);
            SystemEvents.PowerModeChanged += SystemEvents_PowerModeChanged;
            channelGraph.Networks = _networks;

            //
            // Initialize the UI update thread
            //
            _updateViewHandler = new ElapsedEventHandler(UpdateDataGrid);
            _updateListHandler = new ElapsedEventHandler(UpdateInterfaceList);
            _viewUpdateTimer = new System.Timers.Timer(UpdateListInterval);
            _viewUpdateTimer.AutoReset = true;
            _viewUpdateTimer.SynchronizingObject = this;
            _viewUpdateTimer.Elapsed += _updateListHandler;
        }
        #endregion




        #region Environment and NIC Selection Methods
        /// <summary>
        /// Checks for supported operating systems
        /// </summary>
        private void CheckEnvironment()
        {
            //int wordSize = IntPtr.Size;
            //if ( wordSize > 4 ) {
            //    ShowError( Localizer.GetString( "x64Error" ) );
            //    Application.Exit();
            //}
            int major = Environment.OSVersion.Version.Major;
            int minor = Environment.OSVersion.Version.Minor;

            if (!((major == 5 && minor >= 1) || (major == 6)))
            {
                ShowError(Localizer.GetString("UnsupportedOsError"));
                Application.Exit();
            }
        }




        /// <summary>
        /// Returns a wireless interface class that wraps the 
        /// wlan query functionality.
        /// </summary>
        /// <returns>a WirelessInterface object</returns>
        private WirelessInterface GetInterface()
        {
            WirelessInterface wirelessInterface = null;

            try
            {
                wirelessInterface = WirelessInterface.Create(_interface);
            }
            catch (ApplicationException ex)
            {
                //ExceptionForm.ShowException( ex );
                ShowError(Localizer.GetString("UnsupportedInterfaceError") + " {0}", ex.Message);
            }
            catch (Win32Exception ex)
            {
                ShowError(Localizer.GetString("UnsupportedInterfaceError") + " {0}", ex.Message);
            }
            return (wirelessInterface);
        }
        // End GetInterface()




        /// <summary>
        /// This method is a Timer event handler that updates the data 
        /// grid with the current network information.        
        /// </summary>
        /// <param name="source">not used</param>
        /// <param name="e">not used</param>
        private void UpdateInterfaceList(object source, ElapsedEventArgs e)
        {
            LoadInterfaceList();
        }




        /// <summary>
        /// Presents the user with a dialog box that allows the user to download the 
        /// Native wireless API.
        /// </summary>
        private void ShowDownloadDllMessage()
        {
            DialogResult status = MessageBox.Show(Localizer.GetString("DownloadWirelessLanApi"), Localizer.GetString("MissingDll"), MessageBoxButtons.YesNo,
                                                  MessageBoxIcon.Error);
            if (status == DialogResult.Yes)
            {
                Process downloadProc = Process.Start("http://support.microsoft.com/kb/918997");
                downloadProc.WaitForExit();
            }
        }




        /// <summary>
        /// Presents the user with an error dialog box. 
        /// </summary>
        /// <param name="format">format string</param>
        /// <param name="args">format arguments</param>
        private void ShowError(string format, params object[] args)
        {
            string messageText = String.Format(format, args);
            MessageBox.Show(messageText, Localizer.GetString("ApplicationError"), MessageBoxButtons.OK, MessageBoxIcon.Error);
        }




        /*
        /// <summary>
        /// Presents the user with a dialog box that includes the
        /// error information.
        /// </summary>
        /// <param name="ex">Exception object</param>
        private void ShowException( Exception ex )
        {
            MessageBox.Show( Localizer.GetString( "ExceptionMessage" ) + ex.Message, Localizer.GetString( "ApplicationError" ), MessageBoxButtons.OK, MessageBoxIcon.Warning );
        }
       */




        private void SystemEvents_PowerModeChanged(object sender, PowerModeChangedEventArgs e)
        {
            switch (e.Mode)
            {
                case PowerModes.Suspend:
                    StopScanning();
                    break;
            }
        }




        private void LoadInterfaceList()
        {
            //
            // Get a list of wireless network interfaces.
            //
            NetworkInterface[] interfaces;
#if !DEBUG
         try
         {
#endif
            interfaces = NetworkInterface.GetAllNetworkInterfaces();
#if !DEBUG
         }
         catch
         {
            // hopefully it will work the next time
         }
#endif
#if DEBUG
            //This doesn't do anything.
            //foreach ( NetworkInterface netif in interfaces )
            //{
            //    Console.WriteLine( netif );
            //}
#endif
            NetworkInterface selectedInterface = (NetworkInterface)cbxInterfaceList.SelectedItem;
            string selectedName = selectedInterface != null
                                  ? selectedInterface.Name
                                  : Settings.Default.PreviousInterfaceName;
            //
            // Populate the drop down list
            //
            cbxInterfaceList.DataSource = interfaces;
            cbxInterfaceList.DisplayMember = "Description";
            foreach (NetworkInterface netInterface in cbxInterfaceList.Items)
            {
                if (netInterface.Name == selectedName)
                {
                    cbxInterfaceList.SelectedItem = netInterface;
                    break;
                }
            }
        }
        // End LoadInterfaceList()




        /// <summary>
        /// Checks for update and displays update dialog if there is an update available
        /// </summary>
        /// <param name="silentIfNoUpdate">False to display dialog saying no update...</param>
        //private void CheckForUpdate( bool silentIfNoUpdate )
        //{
        //   if ( VersionInfo.CheckForAvailableUpdate( Localizer.GetString( "VersionURL" ), Settings.Default.IgnoreUpdateVersion ) )
        //   {
        //      DialogResult    result = VersionInfo.ShowUpdateDialog();

        //      switch ( result )
        //      {
        //         case DialogResult.OK:
        //         try
        //         {
        //            Process.Start( VersionInfo.DownloadUrl );
        //            Close();
        //         }
        //         catch( Win32Exception )
        //         {
        //         }
        //         break;
        //         case DialogResult.Cancel:
        //         Settings.Default.NextUpdateCheck = DateTime.Now + VersionInfo.TimeBetweenReminders;
        //         break;
        //         case DialogResult.Ignore:
        //         Settings.Default.IgnoreUpdateVersion = VersionInfo.LatestVersion;
        //         break;
        //      }
        //   }
        //   else 
        //   {
        //      Settings.Default.NextUpdateCheck = DateTime.Now + TimeSpan.FromDays( 1 );
        //      if ( !silentIfNoUpdate )
        //      {
        //         MessageBox.Show( Localizer.GetString( "LatestVersionAlreadyInstalled", VersionInfo.LatestVersion ) );
        //      }
        //   }
        //}
        #endregion




        #region Network Scanning Methods
        /// <summary>
        /// This method is the entry point for the network scanning thread. Its
        /// purpose is to periodically scan the wireless environment and collect
        /// network information.
        /// </summary>
        private void ScanNetworks()
        {
            //
            // Grab a wireless interface.
            //
            WirelessInterface wirelessInterface = GetInterface();

            if (null == wirelessInterface)
            {
                return;
            }
            //
            // Begin the main network scanning loop.            
            //
            do
            {
                try
                {
                    //
                    // Timestamp the last network scan, so we can keep track of 
                    // when networks come and go.
                    //
                    _lastScanTimeStamp = DateTime.Now;
                    //
                    // Get the network information from the interface.
                    //                
                    if (_scanSuspended)
                    {
                        // grab the interface again... it is lost during suspend/resume
                        wirelessInterface = GetInterface();
                        if (null == wirelessInterface)
                        {
                            MessageBox.Show(Localizer.GetString("WirelessCardRemoved"));
                        }
                        _scanSuspended = false;
                    }
                    List<WirelessDetails> detailList = wirelessInterface.GetWirelessDetails();
                    //
                    // Scan for available network information.
                    //                
                    wirelessInterface.ScanNetworks();
                    //
                    // Update the UI network data
                    //
                    UpdateNetworkList(detailList);
                    //
                    // Plot the RSSI values for each of the networks.
                    //
                    UpdateTimeGraph();
                    if (Settings.Default.DisplayChannelGraph)
                    {
                        channelGraph.Invalidate();
                    }
                    //Writes an entry in the log. This is programmed the lazy/slow way by appending to InnerXml
                    if (Settings.Default.isLogging)
                    {
                        WriteLogEntry(detailList);
                    }
                    // If we finish the loop, reset the failed scan counter
                }
                catch (Win32Exception ex)
                {
                    ShowError("Unable to continue scanning with selected interface: {0}", ex.Message);
                    StopScanning(false);
                    break;
                }
                catch (ThreadAbortException)
                {
                    break;
                }
                catch (Exception ex)
                {
                    ExceptionForm.ShowException(ex);
                }
            } while (!_terminateNetworkScan.WaitOne(Settings.Default.NetworkScanRate, false));
        }
        // End ScanNetworks()




        /// <summary>
        /// This method copies the data from the detail list returned by the adapter
        /// to the local UI network list.
        /// </summary>
        /// <param name="detailList">list of WirelessDetails objects read from the
        /// wireles adapter.</param>
        private void UpdateNetworkList(IEnumerable<WirelessDetails> detailList)
        {
            bool visible = (_selectAllNetworksCheckBox.CheckState == CheckState.Checked);

            foreach (WirelessDetails detailItem in detailList)
            {
                //
                // Attempt to find the current network in the cached UI data. If
                // a ScannerNetwork is not found, then a new object must be added
                // to the list, so its data will be presented on the UI.
                //
                ScannerNetwork network = FindScannerNetwork(detailItem.MacAddress);
                if (null == network)
                {
                    string vendor = _vendors.GetVendor(detailItem.MacAddress);

                    network = ScannerNetwork.Create(detailItem, vendor, _lastScanTimeStamp, visible);
                    lock (_networks)
                    {
                        _networks.Add(network);
                    }
                }
                else
                {
                    // 
                    // A ScannerNetwork was found in the UI data, so
                    // update its current signal value. Sometimes it has a rollover issue and
                    // shows low signals as super strong... 
                    //
                    if (detailItem.Rssi < 0)
                    {
                        network.Signal = detailItem.Rssi;
                    }
                    else
                    {
                        network.Signal = detailItem.Rssi - 256;
                    }
                    network.LastSeenTimeStamp = _lastScanTimeStamp;
                    // update Channel too... it's possible that the AP changed channels... ( although unlikely )
                    network.Channel = detailItem.Channel;
                }
            }
        }
        // End UpdateNetworkList()




        /// <summary>
        /// Attempts to find a ScannerNetwork with the given MAC
        /// address string.
        /// </summary>
        /// <param name="macAddress">MAC address of the network to find</param>
        /// <returns>a ScannerNetwork object if found, null otherwise</returns>
        private ScannerNetwork FindScannerNetwork(MacAddress macAddress)
        {
            lock (_networks)
            {
                foreach (ScannerNetwork network in _networks)
                {
                    if (network.MacAddress.Equals(macAddress))
                    {
                        return network;
                    }
                }
            }
            return null;
        }
        // End FindScannerNetwork()




        /// <summary>
        /// Starts the network scanning thread
        /// </summary>
        private void StartScanning()
        {
            startImageLabel.Visible = false;
            //hideStartImageLabel.Visible = false;
            bottomPanel.Dock = DockStyle.Fill;
            bottomPanel.Visible = true;
            _interface = (cbxInterfaceList.SelectedItem as NetworkInterface);

            if (null != _interface)
            {
                try
                {
                    // TUT: check whether we can open the selected interface before we start the scan thread
                    WirelessInterface tmpIf = GetInterface();
                    if (null == tmpIf)
                    {
                        return;
                    }
                }
                catch (DllNotFoundException)
                {
                    ShowDownloadDllMessage();
                    return;
                }
                catch (FileNotFoundException)
                {
                    ShowError(Localizer.GetString("MissingDLL"));
                    return;
                }
                lock (_networks)
                {
                    _networks.Clear();
                }
                ResetData();
                timeGraph.Start();
                _terminateNetworkScan.Reset();
                _scanThread = new Thread(ScanNetworks);
                _scanThread.IsBackground = true;
                _scanThread.Start();
                _viewUpdateTimer.Elapsed -= _updateListHandler;
                _viewUpdateTimer.Elapsed += _updateViewHandler;
                _viewUpdateTimer.Interval = UpdateHighlightInterval;
                UpdateNetworkScanState(true);
                Settings.Default.PreviousInterfaceName = _interface.Name;
            }
        }
        // End StartScanning() 




        private void UpdateNetworkScanState(bool enabled)
        {
            if (InvokeRequired)
            {
                NetworkScanStateHandler handler = UpdateNetworkScanState;
                Invoke(handler, new object[] { enabled });
            }
            else
            {
                btnScanButton.Text = enabled ? Localizer.GetString("StopScanning") : Localizer.GetString("StartScanning");
                btnScanButton.Refresh();
                cbxInterfaceList.Enabled = !enabled;
            }
        }




        /// <summary>
        /// Stops scanning and kills the scan thread
        /// </summary>
        private void StopScanning()
        {
            StopScanning(true);
            ExportNs1();
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="killThread">true to terminate the scanThread</param>
        private void StopScanning(bool killThread)
        {
            if (timeGraph != null)
            {
                timeGraph.Stop();
            }
            if (Settings.Default.isLogging && (null != _dataLog))
            {
                SaveLogFile();
                _dataLog = null;
            }
            if (_viewUpdateTimer != null)
            {
                _viewUpdateTimer.Elapsed -= _updateViewHandler;
                _viewUpdateTimer.Elapsed += _updateListHandler;
                _viewUpdateTimer.Interval = UpdateListInterval;
            }
            if (killThread && _scanThread != null)
            {
                _terminateNetworkScan.Set();
                if (!_scanThread.Join(5000))
                {
                    _scanThread.Abort();
                }
            }
            UpdateNetworkScanState(false);
        }
        // End StopScanning()




        private void CheckScanStatus()
        {
            if (_scanThread.ThreadState == System.Threading.ThreadState.Stopped)
            {
                StopScanning();
            }
        }
        // End CheckScanStatus




        private ScannerNetwork GetNetworkFromId(uint networkId)
        {
            for (int i = 0; i < _networks.Count; i++)
            {
                if (_networks[i].Id == networkId)
                {
                    return _networks[i];
                }
            }
            return null;
        }
        #endregion




        #region Data Grid Methods
        /// <summary>
        /// Attempts to find a row in the data grid with a matching
        /// MAC address string.
        /// </summary>
        /// <param name="macAddress">MAC address to find in the data grid</param>
        /// <returns>A DataGridViewRow if found, null otherwise.</returns>
        private DataGridViewRow FindDataRow(string macAddress)
        {
            DataGridViewRow foundRow = null;

            foreach (DataGridViewRow row in scannerGridView.Rows)
            {
                string cellMacAddress = (string)row.Cells["macColumn"].Value;
                if (null != cellMacAddress && cellMacAddress.Equals(macAddress))
                {
                    foundRow = row;
                    break;
                }
            }
            return (foundRow);
        }
        // End FindDataRow




        /// <summary>
        /// This method is a Timer event handler that updates the data 
        /// grid with the current network information.        
        /// </summary>
        /// <param name="source">not used</param>
        /// <param name="e">not used</param>
        private void UpdateDataGrid(object source, ElapsedEventArgs e)
        {
            //
            // Make sure the scan thread is still running
            //
            CheckScanStatus();
            if (!_abort && (_lastDrawnTimeStamp < _lastScanTimeStamp))
            {
                _lastDrawnTimeStamp = DateTime.Now;
                ImageList imgLst;

                ClearInactiveNetworks();
                lock (_networks)
                {
                    foreach (ScannerNetwork network in _networks)
                    {
                        imgLst = !(network.Security == "None")
                                 ? imageSignalLevelEnc
                                 : imageSignalLevel;
                        DataGridViewRow row = FindDataRow(network.MacAddress.ToString());

                        if (null != row)
                        {
                            row.Cells["channelColumn"].Value = network.Channel;
                            row.Cells["securityColumn"].Value = network.Security;
                            row.Cells["typeColumn"].Value = network.NetworkType;
                            row.Cells["lastSeenColumn"].Value = network.LastSeenTimeStamp.ToLongTimeString();
                            row.Cells["speedColumn"].Value = network.Speed;
                            row.Cells["speedColumn"].ToolTipText = network.SpeedString;
                            //Update Strength indicator
                            if (network.Signal > -60)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[0];   // Green
                            }
                            else if (network.Signal <= -60 && network.Signal > -70)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[1];   //LtGreen
                            }
                            else if (network.Signal <= -70 && network.Signal > -80)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[2];   //Yellow
                            }
                            else if (network.Signal <= -80 && network.Signal > -90)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[3];   //Orange
                            }
                            else if (network.Signal <= -90 && network.Signal > -99)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[4];   //Red
                            }
                            else
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[5];   //Gray
                            }

                            // Check to see how much time has passed since the network was last seen
                            TimeSpan duration = _lastScanTimeStamp - network.LastSeenTimeStamp;
                            if (duration.Seconds <= 30)
                            {
                                row.Cells["signalColumn"].Value = network.Signal;
                            }
                            else
                            {
                                // After 30 seconds, set the RSSI value to unknown
                                row.Cells["signalColumn"].Value = -100;
                            }
                            //Update GPS location
                            if (network.Signal > (int)row.Cells["oldSigColumn"].Value && (bool)row.Cells["oldSig2Column"].Value)
                            {
                                //if ( GPSloc.TypeOfFix != GPSTools.GPSInfo.FixType.Invalid ) 
                                if (_gps.HasFix)
                                {
                                    row.Cells["locationColumn"].Value = Math.Round(_gps.Latitude, 5).ToString(CultureInfo.InvariantCulture) + " , " + Math.Round(_gps.Longitude, 5).ToString(
                                                                        CultureInfo.InvariantCulture);
                                    // row.Cells["locationColumn"].Value = Math.Round( GPSloc.Latitude, 5 ).ToString() + " , " + Math.Round( GPSloc.Longitude, 5 ).ToString(); 
                                    row.Cells["oldSig2Column"].Value = false;
                                    row.Cells["oldSigColumn"].Value = network.Signal;
                                }
                            }
                            if (network.Signal > (int)row.Cells["oldSigColumn"].Value && (bool)row.Cells["oldSig2Column"].Value == false)
                            {
                                row.Cells["oldSig2Column"].Value = true;
                            }
                        }
                        else
                        {
                            // add the new network to the data grid
                            row = new DataGridViewRow();
                            row.CreateCells(scannerGridView, network.Data);
                            // Ryan: The column names aren't working here... ugh@!
                            row.Cells[1].Style.BackColor = network.LineColor;
                            row.Cells[1].Style.SelectionBackColor = network.LineColor;
                            scannerGridView.Rows.Add(row);
                            // [4-11-09] The column names seem to work after the row is added.
                            if (network.Signal > -60)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[0];     //Green
                            }
                            else if (network.Signal <= -60 && network.Signal > -70)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[1];     //LtGreen
                            }
                            else if (network.Signal <= -70 && network.Signal > -80)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[2];     //Yellow
                            }
                            else if (network.Signal <= -80 && network.Signal > -90)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[3];     //Orange
                            }
                            else if (network.Signal <= -90 && network.Signal > -99)
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[4];     //Red
                            }
                            else
                            {
                                row.Cells["sigIndColumn"].Value = imgLst.Images[5];     //Gray
                            }

                            try
                            {
                                //if ( GPSloc.TypeOfFix != GPSTools.GPSInfo.FixType.Invalid ) 
                                if (_gps.HasFix)
                                {
                                    row.Cells["locationColumn"].Value = Math.Round(_gps.Latitude, 5).ToString(CultureInfo.InvariantCulture) + " , " + Math.Round(_gps.Longitude, 5).ToString(
                                                                        CultureInfo.InvariantCulture);
                                    // row.Cells["locationColumn"].Value = Math.Round( GPSloc.Latitude, 5 ).ToString() + " , " + Math.Round( GPSloc.Longitude, 5 ).ToString(); 
                                }
                                else
                                {
                                    row.Cells["locationColumn"].Value = "0.00000 , 0.00000";
                                }
                            }
                            catch (NullReferenceException)
                            {
                                //DoNothing( ex ); 
                                //System.Diagnostics.Debugger.Break(); 
                                return;
                            }
                            row.Cells["oldSigColumn"].Value = network.Signal;
                            row.Cells["oldSig2Column"].Value = false;
                            UpdateVerticalHeights();
                            row.Selected = false;
                            timeGraph.AddLine(network.Id, network.LineColor, network.DashedLine, network.StripedLine);
                            timeGraph[network.Id].Visible = network.Visible;
                        }
                        txtAPstat.Text = scannerGridView.Rows.Count + " AP( s )";
                    }
                }
                if (null != scannerGridView.SortedColumn)
                {
                    // TUT: for some reason a row gets selected as a side effect of sorting
                    if (0 == scannerGridView.SelectedRows.Count)
                    {
                        _ignoreSelection = true;
                    }
                    scannerGridView.Sort(scannerGridView.SortedColumn,
                                         (scannerGridView.SortOrder == SortOrder.Descending
                                              ? ListSortDirection.Descending
                                              : ListSortDirection.Ascending));
                }
                if (Settings.Default.DisplayTimeGraph)
                {
                    timeGraph.Refresh();
                }
            }
        }




        private void ClearHighlights()
        {
            ScannerNetwork network;

            for (int counter = 0; counter < _networks.Count; counter++)
            {
                network = _networks[counter];
                network.Selected = false;
                if (timeGraph[network.Id] != null)
                {
                    timeGraph[network.Id].Selected = false;
                }
            }
            channelGraph.Invalidate();
        }




        private void CopyGridToClipboard()
        {
            StringBuilder text = new StringBuilder();

            text.Append(scannerGridView.Columns["macColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["vendorColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["ssidColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["channelColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["signalColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["securityColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["typeColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["speedColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["firstSeenColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["lastSeenColumn"].HeaderText + "\t");
            text.Append(scannerGridView.Columns["locationColumn"].HeaderText + "\r\n");
            for (int i = 0; i < scannerGridView.Rows.Count; i++)
            {
                text.Append(scannerGridView.Rows[i].Cells["macColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["vendorColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["ssidColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["channelColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["signalColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["securityColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["typeColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["speedColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["firstSeenColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["lastSeenColumn"].Value + "\t");
                text.Append(scannerGridView.Rows[i].Cells["locationColumn"].Value + "\r\n");
            }
            try
            {
                Clipboard.SetDataObject(text.ToString(), true);
            }
            catch (ExternalException)
            {
                MessageBox.Show(Localizer.GetString("FailedToCopyClipboard"),
                                Localizer.GetString("ClipboardError"),
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }




        private void ClearInactiveNetworks()
        {
            if (Settings.Default.InactiveNetworkTimeout == TimeSpan.Zero)
            {
                return;
            }
            DateTime oldestAllowedTime = DateTime.Now - Settings.Default.InactiveNetworkTimeout;
            for (int i = 0; i < _networks.Count; i++)
            {
                // delete old networks
                if (_networks[i].LastSeenTimeStamp < oldestAllowedTime)
                {
                    // remove from graph first.. 
                    timeGraph.RemoveLine(_networks[i].Id);
                    // remove from table
                    for (int j = 0; j < scannerGridView.Rows.Count; j++)
                    {
                        if ((uint)scannerGridView.Rows[j].Cells["idColumn"].Value == _networks[i].Id)
                        {
                            scannerGridView.Rows.RemoveAt(j);
                            UpdateVerticalHeights();
                            break;
                        }
                    }
                    lock (_networks)
                    {
                        _networks.RemoveAt(i);
                    }
                }
            }
        }




        private void ScannerGridView_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column == scannerGridView.Columns["signalColumn"] || e.Column == scannerGridView.Columns["channelColumn"])
            {
                if (Convert.ToInt32(e.CellValue1) > Convert.ToInt32(e.CellValue2))
                {
                    e.SortResult = 1;
                }
                else if (Convert.ToInt32(e.CellValue1) < Convert.ToInt32(e.CellValue2))
                {
                    e.SortResult = -1;
                }
                else
                {
                    e.SortResult = 0;
                }
                e.Handled = true;
            }
        }




        private void ScannerGridView_UpdateHeaderCheckBoxPos()
        {
            if (scannerGridView != null && _selectAllNetworksCheckBox != null)
            {
                Rectangle rect = scannerGridView.GetColumnDisplayRectangle(scannerGridView.Columns["checkColumn"].Index, true);
                _selectAllNetworksCheckBox.Size = new Size(13, 13);
                rect.X += 6;
                rect.Y += 5;
                //Change the location of the CheckBox to make it stay on the header
                _selectAllNetworksCheckBox.Location = rect.Location;
                _selectAllNetworksCheckBox.Invalidate();
            }
        }




        private void ScannerGridView_ColumnWidthChanged(object sender, DataGridViewColumnEventArgs e)
        {
            ScannerGridView_UpdateHeaderCheckBoxPos();
        }
        #endregion




        #region UI Methods
        /// <summary>
        /// Resets the graph and data grids
        /// </summary>
        private void ResetData()
        {
            scannerGridView.Rows.Clear();
            timeGraph.Clear();
            // Start the network IDs over again
            ScannerNetwork.ResetNextId();
        }




        /// <summary>
        /// Adjust the UI to match the saved preferences
        /// </summary>
        private void PrefsToWindow()
        {
            try
            {
                WindowState = Settings.Default.WindowState;
                Rectangle windowArea = new Rectangle(Settings.Default.WindowLocation, Settings.Default.WindowSize);
                Rectangle desktopArea = Screen.GetWorkingArea(windowArea);

                if (!desktopArea.Contains(windowArea))
                {
                    if (windowArea.Height > desktopArea.Height)
                    {
                        windowArea.Height = desktopArea.Height;
                    }
                    if (windowArea.Width > desktopArea.Width)
                    {
                        windowArea.Width = desktopArea.Width;
                    }
                    if (windowArea.Left < desktopArea.Left ||
                        windowArea.Right > desktopArea.Right)
                    {
                        windowArea.X = desktopArea.Left + ((desktopArea.Width - windowArea.Width) / 2);
                    }
                    if (windowArea.Top < desktopArea.Top ||
                        windowArea.Bottom > desktopArea.Bottom)
                    {
                        windowArea.Y = desktopArea.Top + ((desktopArea.Height - windowArea.Height) / 2);
                    }
                }
                DesktopBounds = windowArea;
                scannerGridView.Columns["idColumn"].DisplayIndex = Settings.Default.IndexIdColumn;
                scannerGridView.Columns["checkColumn"].DisplayIndex = Settings.Default.IndexCheckColumn;
                scannerGridView.Columns["sigIndColumn"].DisplayIndex = Settings.Default.IndexSigIndColumn;
                scannerGridView.Columns["macColumn"].DisplayIndex = Settings.Default.IndexMacColumn;
                scannerGridView.Columns["vendorColumn"].DisplayIndex = Settings.Default.IndexVendorColumn;
                scannerGridView.Columns["ssidColumn"].DisplayIndex = Settings.Default.IndexSsidColumn;
                scannerGridView.Columns["channelColumn"].DisplayIndex = Settings.Default.IndexChannelColumn;
                scannerGridView.Columns["signalColumn"].DisplayIndex = Settings.Default.IndexSignalColumn;
                scannerGridView.Columns["securityColumn"].DisplayIndex = Settings.Default.IndexSecurityColumn;
                scannerGridView.Columns["speedColumn"].DisplayIndex = Settings.Default.IndexSpeedColumn;
                scannerGridView.Columns["typeColumn"].DisplayIndex = Settings.Default.IndexTypeColumn;
                scannerGridView.Columns["firstSeenColumn"].DisplayIndex = Settings.Default.IndexFirstSeenColumn;
                scannerGridView.Columns["lastSeenColumn"].DisplayIndex = Settings.Default.IndexLastSeenColumn;
                scannerGridView.Columns["locationColumn"].DisplayIndex = Settings.Default.IndexLocationColumn;
                // Set column visibility
                scannerGridView.Columns["macColumn"].Visible = Settings.Default.DisplayMacColumn;
                scannerGridView.Columns["vendorColumn"].Visible = Settings.Default.DisplayVendorColumn;
                scannerGridView.Columns["ssidColumn"].Visible = Settings.Default.DisplaySsidColumn;
                scannerGridView.Columns["channelColumn"].Visible = Settings.Default.DisplayChannelColumn;
                scannerGridView.Columns["signalColumn"].Visible = Settings.Default.DisplaySignalColumn;
                scannerGridView.Columns["securityColumn"].Visible = Settings.Default.DisplaySecurityColumn;
                scannerGridView.Columns["speedColumn"].Visible = Settings.Default.DisplaySpeedColumn;
                scannerGridView.Columns["typeColumn"].Visible = Settings.Default.DisplayTypeColumn;
                scannerGridView.Columns["firstSeenColumn"].Visible = Settings.Default.DisplayFirstSeenColumn;
                scannerGridView.Columns["lastSeenColumn"].Visible = Settings.Default.DisplayLastSeenColumn;
                scannerGridView.Columns["locationColumn"].Visible = Settings.Default.DisplayLocationColumn;
                macToolStripMenuItem.Checked = Settings.Default.DisplayMacColumn;
                vendorToolStripMenuItem.Checked = Settings.Default.DisplayVendorColumn;
                ssidToolStripMenuItem.Checked = Settings.Default.DisplaySsidColumn;
                channelToolStripMenuItem.Checked = Settings.Default.DisplayChannelColumn;
                signalToolStripMenuItem.Checked = Settings.Default.DisplaySignalColumn;
                securityToolStripMenuItem.Checked = Settings.Default.DisplaySecurityColumn;
                speedToolStripMenuItem.Checked = Settings.Default.DisplaySpeedColumn;
                typeToolStripMenuItem.Checked = Settings.Default.DisplayTypeColumn;
                firstSeenToolStripMenuItem.Checked = Settings.Default.DisplayFirstSeenColumn;
                lastSeenToolStripMenuItem.Checked = Settings.Default.DisplayLastSeenColumn;
                locationToolStripMenuItem.Checked = Settings.Default.DisplayLocationColumn;
                GraphSplitContainer.SplitterDistance = Settings.Default.graphSplitterDistance;
                displayTimeGraphToolStripMenuItem.Checked = Settings.Default.DisplayTimeGraph;
                displayChannelGraphToolStripMenuItem.Checked = Settings.Default.DisplayChannelGraph;
                if (Settings.Default.ChannelView24)
                {
                    channelGraph.Band = ChannelView.BandType.Band2400MHz;
                    rdoBand2400.Checked = true;
                }
                else
                {
                    channelGraph.Band = ChannelView.BandType.Band5000MHz;
                    rdoBand5000.Checked = true;
                }
                _logFilename = Settings.Default.LogFolder;
            }
            catch
            {
                // This is causing a crash due to a corrupt config file...
                Settings.Default.Reset();
            }
            // will update the views based on the above settings
            UpdateGraphsInView();
        }




        /// <summary>
        /// Randomly picks the start image. Since this is not essential to app functionality
        /// we catch ALL exceptions that may be thrown in this function.
        /// </summary>
        //private void SelectStartImage()
        //{
        //   try
        //   {
        //      if ( Application.ProductVersion.Length > 0 && 
        //          Settings.Default.HideStartImageVersion.Length > 0 && 
        //          VersionInfo.CompareVersions( Application.ProductVersion, Settings.Default.HideStartImageVersion ) )
        //      {
        //         int numImages = int.Parse( Localizer.GetString( "NumStartImages" ) );
        //         Random          random = new Random();
        //         int             imageIndex = random.Next( numImages );

        //         Bitmap image = Localizer.GetBitmap( "start" + imageIndex );
        //         startImageLabel.Image = image;
        //         startImageLabel.Name = imageIndex.ToString();
        //      }
        //      else 
        //      {
        //         startImageLabel.Visible = false;
        //         hideStartImageLabel.Visible = false;
        //      }
        //   }
        //   catch( Exception )
        //   {
        //   }
        //}




        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //private void HideStartImageLabel_Click( object sender, EventArgs e )
        //{
        //   try
        //   {
        //      Settings.Default.HideStartImageVersion = Application.ProductVersion;
        //      Settings.Default.Save();
        //      startImageLabel.Visible = false;
        //      hideStartImageLabel.Visible = false;
        //   }
        //   catch( Exception )
        //   {
        //   }
        //}




        private void UpdateSize()
        {
            if (WindowState == FormWindowState.Normal)
            {
                try
                {
                    Settings.Default.WindowSize = Size;
                }
                catch (SystemException)
                {
                    // couldn't update prefs, that's OK
                }
            }
            if (startImageLabel.Visible)
            {
                startImageLabel.Left = (adPanel.Width - startImageLabel.Width) / 2;
                startImageLabel.Top = (adPanel.Height - startImageLabel.Height) / 2;
                //hideStartImageLabel.Left = ( adPanel.Width - hideStartImageLabel.Width ) / 2;
                //hideStartImageLabel.Top = startImageLabel.Bottom;
            }
            else
            {
                UpdateVerticalHeights();
            }
        }




        private delegate void SetStringToClipboard();
        /// <summary>
        /// Used to copy the error info on a crash to teh clipboard
        /// </summary>
        /// <param name="data"></param>




        public void StringToClipboard(string data)
        {
            if (InvokeRequired)
            {
                Invoke(new SetStringToClipboard(() => Clipboard.SetDataObject(data)));
            }
            else
            {
                Clipboard.SetDataObject(data);
            }
        }




        private void UpdateVerticalHeights()
        {
            if (_autoResize)
            {
                scannerGridView.Height = Math.Min((Height - MinNonGridVertical),
                        (scannerGridView.PreferredSize.Height - (scannerGridView.Height > scannerGridView.DisplayRectangle.Height
                        ? 16 : 33)));
            }
        }




        /// <summary>
        /// Updates which graphs are in view. If no graphs in view the entire graphs panel is hidden
        /// </summary>
        private void UpdateGraphsInView()
        {
            GraphSplitContainer.Visible = (displayTimeGraphToolStripMenuItem.Checked || displayChannelGraphToolStripMenuItem.Checked);
            // When the app starts, the GraphSplitContainer is not visible, because the entire bottomPanel is not visible
            // so this needs to be based on the menu itmes, not on GraphSplitContainer.Visible
            if (displayTimeGraphToolStripMenuItem.Checked || displayChannelGraphToolStripMenuItem.Checked)
            {
                GraphSplitContainer.Panel1Collapsed = !displayTimeGraphToolStripMenuItem.Checked;
                GraphSplitContainer.Panel2Collapsed = !displayChannelGraphToolStripMenuItem.Checked;
            }
            channelGraphPrefsPanel.Enabled = displayChannelGraphToolStripMenuItem.Checked;
        }




        /// <summary>
        /// Plots the RSSI values of the available wireless networks.
        /// </summary>
        private void UpdateTimeGraph()
        {
            lock (_networks)
            {
                foreach (ScannerNetwork network in _networks)
                {
                    TimeGraph.Line line = timeGraph[network.Id];
                    if (null != line && network.LastSeenTimeStamp > line.MaxMeasuredTimeStamp)
                    {
                        line.AddPoint(network.Signal, _lastScanTimeStamp, true);
                    }
                }
            }
        }
        // End UpdateSignalGraph()
        #endregion




        #region UI Interaction Methods
        private void ScannerGridView_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            // clicked on row selector.. 
            if (e.RowIndex == -1)
            {
                _ignoreSelection = true;
                return;
            }
            switch (e.ColumnIndex)
            {
                // Checkbox column
                case 1:
                    ScannerNetwork network = GetNetworkFromId((uint)scannerGridView.Rows[e.RowIndex].Cells["idColumn"].Value);
                    if (e.Button == MouseButtons.Left)
                    {
                        if (network != null)
                        {
                            network.Visible = !network.Visible;
                            scannerGridView.Rows[e.RowIndex].Cells["checkColumn"].Value = network.Visible;
                            if (timeGraph[network.Id] != null)
                            {
                                timeGraph[network.Id].Visible = network.Visible;
                                channelGraph.Invalidate();
                            }
                        }
                    }
                    break;

                default:
                    if (e.Button == MouseButtons.Right)
                    {
                        if (scannerGridView.SelectedRows.Count > 0)
                        {
                            hideToolStripMenuItem.Text = scannerGridView.SelectedRows.Count == 1
                                                         ? Localizer.GetString("HideNetwork", scannerGridView.SelectedRows[0].Cells["ssidColumn"].Value)
                                                         : Localizer.GetString("HideSelectedNetworks");
                            rowContextMenuStrip.Show(Cursor.Position);
                        }
                    }
                    else if ((scannerGridView.Rows[e.RowIndex].Selected) && !_newSelection)
                    {
                        scannerGridView.Rows[e.RowIndex].Selected = false;
                    }
                    break;
            }

            // check for indeterminate state of the selectAllCheckBox
            int numVisible = 0;
            int numInvisible = 0;

            lock (_networks)
            {
                foreach (ScannerNetwork network in _networks)
                {
                    if (network.Visible)
                        numVisible++;
                    else
                        numInvisible++;

                }
            }
            if (numVisible == 0)
            {
                _selectAllNetworksCheckBox.CheckState = CheckState.Unchecked;
            }
            else if (numInvisible == 0)
            {
                _selectAllNetworksCheckBox.CheckState = CheckState.Checked;
            }
            else
            {
                _selectAllNetworksCheckBox.CheckState = CheckState.Indeterminate;
            }
            _newSelection = false;
        }




        /// <summary>
        /// Manually positions the _selectAllNetworksCheckBox. This is kind of a hack,
        /// but there isn't a standard way of putting a checkbox into the DataGridView
        /// header
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ScannerGridView_VisibleChanged(object sender, EventArgs e)
        {
            if (scannerGridView.Visible)
            {
                if (_selectAllNetworksCheckBox == null)
                {
                    _selectAllNetworksCheckBox = new CheckBox();
                    _selectAllNetworksCheckBox.ThreeState = true;
                    _selectAllNetworksCheckBox.CheckState = CheckState.Checked;
                    _selectAllNetworksCheckBox.CheckedChanged += SelectAllNetworksCheckBox_CheckedChanged;
                    //Add the CheckBox into the DataGridView
                    scannerGridView.Controls.Add(_selectAllNetworksCheckBox);
                }
                // TUT: this little hack causes the column indices to get updated on XP
                scannerGridView.Columns["idColumn"].Visible = true;
                scannerGridView.Columns["idColumn"].Visible = false;
                ScannerGridView_UpdateHeaderCheckBoxPos();
            }
        }




        /// <summary>
        /// Toggles the Wi-Fi overlays for ALL Wi-Fi networks in the Wi-Fi sidebar
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private void SelectAllNetworksCheckBox_CheckedChanged(object source, EventArgs e)
        {
            if (_selectAllNetworksCheckBox.CheckState == CheckState.Indeterminate)
            {
                return;
            }
            foreach (DataGridViewRow row in scannerGridView.Rows)
            {
                if (row.Visible)
                {
                    row.Cells["checkColumn"].Value = _selectAllNetworksCheckBox.Checked;
                    ScannerNetwork network = GetNetworkFromId((uint)row.Cells["idColumn"].Value);
                    if (network != null)
                    {
                        network.Visible = _selectAllNetworksCheckBox.Checked;
                        if (timeGraph[network.Id] != null)
                        {
                            timeGraph[network.Id].Visible = network.Visible;
                        }
                    }
                }
            }
            if (Settings.Default.DisplayChannelGraph)
            {
                channelGraph.Invalidate();
            }
        }




        private void ScannerGridView_SelectionChanged(object sender, EventArgs e)
        {
            ClearHighlights();
            if (_ignoreSelection)
            {
                foreach (DataGridViewRow row in scannerGridView.Rows)
                {
                    if (null != row)
                    {
                        row.Selected = false;
                    }
                }
                _ignoreSelection = false;
            }
            else
            {
                for (int row = 0; row < scannerGridView.SelectedRows.Count; row++)
                {
                    uint networkId = (uint)scannerGridView.SelectedRows[row].Cells["idColumn"].Value;
                    if (timeGraph[networkId] != null)
                    {
                        timeGraph[networkId].Selected = true;
                    }
                    ScannerNetwork network = GetNetworkFromId(networkId);
                    if (network != null)
                    {
                        network.Selected = true;
                    }
                }
                _newSelection = true;
                if (Settings.Default.DisplayChannelGraph)
                {
                    channelGraph.Invalidate();
                }
            }
        }




        private void CopyDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyGridToClipboard();
        }




        private void MacToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["macColumn"].Visible = macToolStripMenuItem.Checked;
            Settings.Default.DisplayMacColumn = macToolStripMenuItem.Checked;
        }




        private void VendorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["vendorColumn"].Visible = vendorToolStripMenuItem.Checked;
            Settings.Default.DisplayVendorColumn = vendorToolStripMenuItem.Checked;
        }




        private void SsidToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["ssidColumn"].Visible = ssidToolStripMenuItem.Checked;
            Settings.Default.DisplaySsidColumn = ssidToolStripMenuItem.Checked;
        }




        private void ChannelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["channelColumn"].Visible = channelToolStripMenuItem.Checked;
            Settings.Default.DisplayChannelColumn = channelToolStripMenuItem.Checked;
        }




        private void SignalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["signalColumn"].Visible = signalToolStripMenuItem.Checked;
            Settings.Default.DisplaySignalColumn = signalToolStripMenuItem.Checked;
        }




        private void SecurityToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["securityColumn"].Visible = securityToolStripMenuItem.Checked;
            Settings.Default.DisplaySecurityColumn = securityToolStripMenuItem.Checked;
        }




        private void NetworkToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["typeColumn"].Visible = typeToolStripMenuItem.Checked;
            Settings.Default.DisplayTypeColumn = typeToolStripMenuItem.Checked;
        }




        private void SpeedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["speedColumn"].Visible = speedToolStripMenuItem.Checked;
            Settings.Default.DisplaySpeedColumn = speedToolStripMenuItem.Checked;
        }




        private void FirstSeenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["firstSeenColumn"].Visible = firstSeenToolStripMenuItem.Checked;
            Settings.Default.DisplayFirstSeenColumn = firstSeenToolStripMenuItem.Checked;
        }




        private void LastTimeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["lastSeenColumn"].Visible = lastSeenToolStripMenuItem.Checked;
            Settings.Default.DisplayLastSeenColumn = lastSeenToolStripMenuItem.Checked;
        }




        private void LocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            scannerGridView.Columns["locationColumn"].Visible = locationToolStripMenuItem.Checked;
            Settings.Default.DisplayLocationColumn = locationToolStripMenuItem.Checked;
        }




        private void ScannerForm_SizeChanged(object sender, EventArgs e)
        {
            UpdateSize();
        }




        private void ScannerForm_FormClosing(object sender, FormClosingEventArgs args)
        {
            try
            {
                if (Settings.Default.isLogging && (null != _dataLog))
                {
                    SaveLogFile();
                }
            }
            catch (SystemException)
            {
                // couldn't acccess settings to see if logging, save if there is a log file
                if (null != _dataLog)
                {
                    SaveLogFile();
                }
            }
            Settings.Default.WindowState = WindowState;
            if (WindowState == FormWindowState.Normal)
            {
                Settings.Default.WindowLocation = Location;
                Settings.Default.WindowSize = Size;
            }
            // Save column order
            //Settings.Default.DataGridColumns = new System.Collections.Specialized.StringDictionary();
            //foreach ( DataGridViewColumn col in scannerGridView.Columns )
            //{
            //    Settings.Default.DataGridColumns.Add( col.Name, col.DisplayIndex.ToString() );
            //}

            Settings.Default.IndexIdColumn = scannerGridView.Columns["idColumn"].DisplayIndex;
            Settings.Default.IndexCheckColumn = scannerGridView.Columns["checkColumn"].DisplayIndex;
            Settings.Default.IndexSigIndColumn = scannerGridView.Columns["sigIndColumn"].DisplayIndex;
            Settings.Default.IndexMacColumn = scannerGridView.Columns["macColumn"].DisplayIndex;
            Settings.Default.IndexVendorColumn = scannerGridView.Columns["vendorColumn"].DisplayIndex;
            Settings.Default.IndexSsidColumn = scannerGridView.Columns["ssidColumn"].DisplayIndex;
            Settings.Default.IndexChannelColumn = scannerGridView.Columns["channelColumn"].DisplayIndex;
            Settings.Default.IndexSignalColumn = scannerGridView.Columns["signalColumn"].DisplayIndex;
            Settings.Default.IndexSecurityColumn = scannerGridView.Columns["securityColumn"].DisplayIndex;
            Settings.Default.IndexSpeedColumn = scannerGridView.Columns["speedColumn"].DisplayIndex;
            Settings.Default.IndexTypeColumn = scannerGridView.Columns["typeColumn"].DisplayIndex;
            Settings.Default.IndexFirstSeenColumn = scannerGridView.Columns["firstSeenColumn"].DisplayIndex;
            Settings.Default.IndexLastSeenColumn = scannerGridView.Columns["lastSeenColumn"].DisplayIndex;
            Settings.Default.IndexLocationColumn = scannerGridView.Columns["locationColumn"].DisplayIndex;
            Settings.Default.Save();
            _abort = true;
            if (null != _gps)
            {
                _gps.TerminateThread();
            }
            StopScanning();
        }




        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }




        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (AboutForm about = new AboutForm())
            {
                about.ShowDialog();
            }
        }




        //private void CommunityForumToolStripMenuItem_Click( object sender, EventArgs e )
        //{
        //try
        //{
        //   Process.Start( "http://www.metageek.net/forum?utm_campaign=Software&utm_medium=Inssider." + Application.ProductVersion + "&utm_source=HelpMenuForum" );
        //}
        //catch( Win32Exception )
        //{
        //   //
        //   // Ignore exception. 
        //   // 
        //   // This exception will be thrown when Firefox unexpectedly 
        //   // shuts down, and asks the user to restore the session when it is started by 
        //   // Inssider. Apparently, Windoz doesn't like this, because it tosses a 
        //   // file not found exception. Weird!
        //   //
        //   // Anyway, the lesser evil right now is to silently ignore
        //   // this exception.
        //   //                
        //}
        //}




        private void ScannerForm_LocationChanged(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                if (_canSet)
                {
                    Settings.Default.WindowLocation = Location;
                }
            }
        }




        private void CopyTimeGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(timeGraph.GraphBitMap, true);
        }




        private void CopyChannelGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(channelGraph.GraphBitmap, true);
        }




        private void PreferencesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (PreferencesForm prefs = new PreferencesForm())
            {
                if (prefs.ShowDialog() == DialogResult.OK)
                {
                    ClearInactiveNetworks();
                    _gps.Enabled = Settings.Default.gpsEnabled;
                    if (_gps.Enabled)
                    {
                        if (Settings.Default.GPSPort != _gps.PortName || !_gps.Connected)
                        {
                            _gps.InitializeThread();
                        }
                    }
                    GpsUpdated(this, e);
                }
            }
        }




        private void HelpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string pathName = Directory.GetParent(Path.GetDirectoryName(Application.ExecutablePath)) + "\\docs\\inssider.chm";
                Process.Start(pathName);
            }
            catch (Exception)
            {
                MessageBox.Show(Localizer.GetString("HelpErrorMessage"), Localizer.GetString("HelpError"));
            }
        }




        private void ScannerGridView_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                columnsContextMenuStrip.Show(Cursor.Position);
            }
            else
            {
                // data sort just changed....
                int lastRow = scannerGridView.Rows.Count - 1;

                for (int i = 0; i <= lastRow; i++)
                {
                    uint networkId = (uint)scannerGridView.Rows[i].Cells["idColumn"].Value;
                    if (timeGraph[networkId] != null)
                    {
                        timeGraph[networkId].DrawOrder = lastRow - i;
                    }
                }
                timeGraph.Refresh();
            }
        }




        private void CreateVendorLookup()
        {
            if (null == _vendors)
            {
                _vendors = new Vendors();
            }
            try
            {
                _vendors.LoadFromOui();
            }
            catch
            {
                string message = String.Format("Error loading WiFi vendor names, vendor names will not be displayed");
                MessageBox.Show(message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }




        private void ScannerForm_Load(object sender, EventArgs e)
        {
#if DEBUG
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Form load");
            Console.ForegroundColor = ConsoleColor.Gray;
#endif
            try
            {
                if (Settings.Default.NextUpdateCheck < DateTime.Now)
                {
#if DEBUG
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write("Checking for updates...");
#endif
                    //CheckForUpdate( true );
#if DEBUG
                    Console.WriteLine("Complete");
                    Console.ForegroundColor = ConsoleColor.Gray;
#endif
                }
            }
            catch (SystemException)
            {
                // Couldn't access setting to see if due for an update check
                // go ahead and check
                //CheckForUpdate( true );
            }
            try
            {
                LoadParams();
            }
            catch
            {

            }

            using (var form = new frmLogin())
            {
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Username = sUsername;
                form.Password = sPassword;

                if (bRememberMe == true)
                {
                    form.RememberMe = true;

                }
                var result = form.ShowDialog();
                sUsername = form.Username;
                sPassword = form.Password;
                if (sUsername.Trim() == "") Environment.Exit(1);

                if (result == DialogResult.OK && form.siteID != null)
                {
                    string val = form.siteID;
                    bRememberMe = form.RememberMe;
                    sUsername = form.Username;
                    sPassword = form.Password;
                    iUserID = form.iUserID;
                    lblUsername.Text = "Logged in as: " + sUsername;

                    sSiteID = form.siteID;
                    bLoggedIn = true;
                    MessageBox.Show("Successfully logged in as: " + sUsername);
                           SaveParams();
                }
                else
                {
                    Environment.Exit(1);
                }
            }
            Assembly assem = Assembly.GetEntryAssembly();
            AssemblyName assemName = assem.GetName();
            string strVersion;
            if (System.Deployment.Application.ApplicationDeployment.IsNetworkDeployed)
            {
                System.Deployment.Application.ApplicationDeployment ad = System.Deployment.Application.ApplicationDeployment.CurrentDeployment;
                strVersion = ad.CurrentVersion.ToString();
            }
            else
            {
                strVersion = assemName.Version.ToString();
            }
            this.Text = System.String.Format("{0} ({1})", assemName.Name, strVersion);
            PrefsToWindow();
            UpdateSize();
            try
            {
                _gps.Enabled = Settings.Default.gpsEnabled;
            }
            catch (SystemException)
            {
                _gps.Enabled = false;
            }
            _canSet = true;
            bottomPanel.Visible = false;
            // load the vendor information from the oui file.
            CreateVendorLookup();
            LoadInterfaceList();
            _viewUpdateTimer.Start();
            gridSplitter.SplitPosition = 55;
            GraphSplitContainer.SplitterMoved += GraphSplitContainer_SplitterMoved;
            GraphSplitContainer.Panel1MinSize = 250;
            GraphSplitContainer.Panel2MinSize = 250;
            //Init the GPS
            if (_gps.Enabled)
            {
                _gps.InitializeThread();
            }
            GpsUpdated(this, e);


        }



                #region GetParam
        /****************************************************************************
           GetParam                                               08/02/10
        Description: 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        private string GetParam(StreamReader textIn, string key)
        {
            string sline = "";
            string value = "";

            while (true)
            {
                try
                {
                    sline = textIn.ReadLine();
                }
                catch
                {
                    break;
                }

                if (sline == null)
                    break;

                if (sline.Contains(key))
                {
                    int ipos = sline.IndexOf('=');
                    if (ipos > 0)
                    {
                        try
                        {
                            value = sline.Substring(ipos + 1);
                        }
                        catch
                        {
                            value = "";
                        }
                        break;
                    }
                }
            }

            return value;
        }
        /*--- end of GetParam() ---------------------------------------------------*/
        #endregion

        #region LoadParams
        /****************************************************************************
           LoadParams                                             08/02/10
        Description: Read the saved params and load the textboxes. 
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        public void LoadParams()
        {
            StreamReader textIn;
            string sTemp = "";

            try
            {
                textIn = new StreamReader(new FileStream("SMNetScan.cfg", FileMode.Open, FileAccess.Read));
            }
            catch
            {
                return;
            }

            sUsername = GetParam(textIn, "USERNAME");
            sPassword = GetParam(textIn, "PASSWORD");

            if (sUsername.Length > 1)
            {
                bRememberMe = true;

            }

            textIn.Close();

            return;
        }
        /*--- end of LoadParams() -------------------------------------------------*/
        #endregion

        #region SaveParams
        /****************************************************************************
           SaveParams                                             08/02/10
        Description: Save the data in the textboxes.
         Parameters: 
       Return Value: 
         Processing: 
          Called By: 
           Comments: 
      ****************************************************************************/

        public void SaveParams()
        {
            StreamWriter textOut;

            try
            {
                textOut = new StreamWriter(new FileStream("SMNetScan.cfg", FileMode.Create, FileAccess.ReadWrite));
            }
            catch
            {
                MessageBox.Show("Cannot save to SMNetScan.cfg file; please make sure you have permission to write to this file.");
                return;
            }

            if (bRememberMe == true)
            {
                textOut.WriteLine("USERNAME=" + sUsername);
                textOut.WriteLine("PASSWORD=" + sPassword);
            }
            textOut.Close();
            return;
        }
          #endregion
        private void ScanButton_Click(object sender, EventArgs e)
        {
            if (btnScanButton.Text.Equals(Localizer.GetString("StartScanning")))
            {
                StartScanning();
            }
            else
            {
                StopScanning();
            }
        }




        private void CheckToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CheckForUpdate( false );
        }




        private void AutomaticallyResizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            _autoResize = !_autoResize;
            automaticallyResizeToolStripMenuItem.Checked = _autoResize;
            if (_autoResize)
            {
                UpdateVerticalHeights();
            }
        }




        private void GraphSplitter_SplitterMoving(object sender, SplitterEventArgs e)
        {
            // TUT: once the user moves the splitter up, we don't want to autosize it anymore
            if (e.Y > e.SplitY)
            {
                _autoResize = false;
            }
        }




        private void HideToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in scannerGridView.SelectedRows)
            {
                row.Cells["checkColumn"].Value = false;
                uint networkId = (uint)row.Cells["idColumn"].Value;
                if (timeGraph[networkId] != null)
                {
                    timeGraph[networkId].Visible = false;
                }
                ScannerNetwork network = GetNetworkFromId(networkId);
                if (network != null)
                {
                    network.Visible = false;
                }
                row.Visible = false;
            }
            UpdateVerticalHeights();
            if (Settings.Default.DisplayChannelGraph)
            {
                channelGraph.Invalidate();
            }
        }




        /// <summary>
        /// Shows all networks in the table
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowAllNetworksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in scannerGridView.Rows)
            {
                row.Visible = true;
            }
            UpdateVerticalHeights();
        }




        /// <summary>
        /// Adjust horizontal size of the graphs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GraphSplitContainer_SplitterMoved(object sender, SplitterEventArgs e)
        {
            Settings.Default.graphSplitterDistance = e.SplitX;
        }




        /// <summary>
        /// Go to website for the image ad
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartImageLabel_Click(object sender, EventArgs e)
        {
            string page;

            switch (startImageLabel.Name)
            {
                case "0":
                    page = "products/wi-spy-24i";
                    break;
                case "1":
                    page = "products/wirelessmon";
                    break;
                default:
                    page = "products/wi-spy-dbx";
                    break;
            }
            try
            {
                Process.Start("http://www.metageek.net/" + page + "?utm_campaign=Software&utm_medium=Inssider." + Application.ProductVersion +
                              "&utm_source=StartPage" + startImageLabel.Name);
            }
            catch (Win32Exception)
            {
            }
        }




        private void ClickableControl_MouseEnter(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
        }




        private void ClickableControl_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
        }




        private void ScannerForm_VisibleChanged(object sender, EventArgs e)
        {
            UpdateSize();
        }




        #region GPS Methods
        /// <summary>
        /// Attempts to open the log file for GPS logging
        /// </summary>
        private void OpenLogFile()
        {
            _logTime = DateTime.Now;
            try
            {
                _dataLog = new XmlDocument();
                string dateString = _logTime.Year + "-" + _logTime.Month + "-" + _logTime.Day + "_" + _logTime.Hour + "-" + _logTime.Minute + "-" + _logTime.Second;
                _logFilename = Settings.Default.LogFolder + "\\" + dateString + ".gpx";
                _dataLog.LoadXml(XmlHeader);
            }
            catch (Exception)
            {
                _dataLog = null;
            }
        }




        private void SaveLogFile()
        {
            try
            {
                _dataLog.Save(_logFilename);
            }
            catch (XmlException e)
            {
                Debug.WriteLine(e.Message);
            }
            catch (IOException e)
            {
                Debug.WriteLine(e.Message);
                MessageBox.Show(Localizer.GetString("GpxFileInUse"), Localizer.GetString("Error"), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        private void WriteLogEntry(IEnumerable<WirelessDetails> detailList)
        {
            if (null == _dataLog)
            {
                OpenLogFile();
            }
            try
            {
                //WirelessDetails[] wirelessArray = detailList.ToArray();
                //for ( int i = 0; i < wirelessArray.Length; i++ )
                foreach (WirelessDetails tempDetail in detailList)
                {
                    //WirelessDetails tempDetail = wirelessArray[i];
                    uint channelId = tempDetail.Channel;
                    string mac = tempDetail.MacAddress.ToString();
                    int rssi = tempDetail.Rssi;
                    string networkType = tempDetail.NetworkType;
                    string privacy = tempDetail.Privacy;
                    string rates = tempDetail.BuildRateString();

                    string signalQuality = tempDetail.SignalQuality + "";
                    string ssid = tempDetail.Ssid;

                    ssid = XmlCleanUp.CleanUp(ssid);
                    XmlElement dataPoint = _dataLog.CreateElement("wpt");
                    dataPoint.SetAttribute("lat", _gps.Latitude.ToString(CultureInfo.InvariantCulture.NumberFormat));
                    dataPoint.SetAttribute("lon", _gps.Longitude.ToString(CultureInfo.InvariantCulture.NumberFormat));
                    dataPoint.InnerXml = "<ele>" + _gps.Altitude + "</ele>";
                    dataPoint.InnerXml += "<time>" + _gps.SatelliteTime.Year + "-" +
                    _gps.SatelliteTime.Month.ToString("D2") + "-" + _gps.SatelliteTime.Day.ToString("D2") + "T" +
                    _gps.SatelliteTime.Hour.ToString("D2") + ":" + _gps.SatelliteTime.Minute.ToString("D2") + ":" +
                    _gps.SatelliteTime.Second + "." + _gps.SatelliteTime.Millisecond + "Z</time>";
                    string cr = Environment.NewLine;

                    if (!double.IsNaN(_gps.MagVar))
                    {
                        dataPoint.InnerXml += "<magvar>" + _gps.MagVar + "</magvar>";
                    }
                    dataPoint.InnerXml += "<geoidheight>" + _gps.GeoidSeperation + "</geoidheight>";
                    dataPoint.InnerXml += "<name>" + ssid + " [" + mac + "]</name>";
                    //Currently this description is ignored when converted to a KML file. However you'll see it in Google Earth if you import the GPX directly.
                    string desc = ssid + cr + "[" + mac + "]" + cr + privacy + cr + "RSSI  :" + rssi + "dBm" + cr + "Quality  " + signalQuality + "%" + cr + "Channel " + channelId + cr + "Speed ( kph ) " + _gps.Speed + cr + _gps.Time;
                    dataPoint.InnerXml += "<cmt>" + _gps.Speed + "</cmt>";
                    dataPoint.InnerXml += "<desc>" + desc + "</desc>";
                    dataPoint.InnerXml += "<fix>" + _gps.FixString + "</fix>";
                    dataPoint.InnerXml += "<sat>" + _gps.SatellitesUsed + "</sat>";
                    dataPoint.InnerXml += "<hdop>" + _gps.Hdop + "</hdop>";
                    dataPoint.InnerXml += "<vdop>" + _gps.Vdop + "</vdop>";
                    dataPoint.InnerXml += "<pdop>" + _gps.Pdop + "</pdop>";
                    double age = _gps.DgpsAge;

                    if (age != 0)
                    {
                        dataPoint.InnerXml += "<ageofdgpsdata>" + _gps.DgpsAge + "</ageofdgpsdata>";
                        dataPoint.InnerXml += "<dgpsid>" + _gps.Dgpsid + "</dgpsid>";
                    }
                    string extensions = "";
                    extensions += "<extensions>";
                    extensions += "<MAC>" + mac + "</MAC>";
                    extensions += "<SSID>" + ssid + "</SSID>";
                    extensions += "<RSSI>" + rssi + "</RSSI>";
                    extensions += "<ChannelID>" + channelId + "</ChannelID>";
                    extensions += "<privacy>" + privacy + "</privacy>";
                    extensions += "<signalQuality>" + signalQuality + "</signalQuality>";
                    extensions += "<networkType>" + networkType + "</networkType>";
                    extensions += "<rates>" + rates + "</rates>";
                    extensions += "</extensions>";
                    dataPoint.InnerXml += extensions;
                    //Ignore anything less than or equal to -100 dBm
                    if (rssi > -100)
                    {
                        _dataLog.GetElementsByTagName("gpx").Item(0).AppendChild(dataPoint);
                    }
                }
                // Periodically auto save timestamped log if enabled
                TimeSpan dT = DateTime.Now - _logTime;
                if (Settings.Default.isLogging && Settings.Default.isAutoSaving && ((int)(Math.Floor(dT.TotalSeconds))) > Settings.Default.autoSaveRate)
                {
                    _logTime = DateTime.Now;
                    SaveLogFile();
                }
            }
            catch (Exception)
            {
            }
        }




        private void GpsUpdated(object sender, EventArgs e)
        {
            string updateString = String.Empty;

            try
            {
                if (!_gps.Enabled)
                {
                    updateString = "GPS: Disabled";
                }
                else if (_gps.Connected)
                {
                    if (!_gps.HasTalked)
                    {
                        updateString = "GPS: Waiting for data";
                    }
                    else
                    {
                        if (!_gps.HasFix)
                        {
                            updateString = "FIX LOST! Last Values: ";
                        }
                        updateString += "GPS : " + Math.Round(_gps.Latitude, 5).ToString("F5") + "�," + Math.Round(_gps.Longitude, 5).ToString("F5") + "�," + _gps.Altitude.ToString("F1") + "m";
                        updateString += " SPEED : " + _gps.Speed.ToString("F1") + "km/h SAT COUNT : " + _gps.SatellitesUsed;
                    }
                }
                else
                {
                    updateString = "GPS: Not detected on " + _gps.PortName;
                }
            }
            catch (ObjectDisposedException)
            {
            }
            txtGPSstat.Text = updateString;
        }




        private void GpsTimeout(object sender, EventArgs e)
        {
            txtGPSstat.Text = "GPS: No GPS found on " + _gps.PortName;
        }




        private void ExportAsNS1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportNs1();
        }




        /// <summary>
        /// Exports data in the Netstumbler file format
        /// </summary>
        private void ExportNs1()
        {
            Ns1File exportFile = new Ns1File(scannerGridView.Rows.Count);

            try
            {
                for (int i = 0; i < scannerGridView.Rows.Count; i++)
                {
                    string[] latlon = scannerGridView.Rows[i].Cells["locationColumn"].Value.ToString().Split(',');
                    Type apType = Type.Other;

                    switch (scannerGridView.Rows[i].Cells["typeColumn"].Value.ToString())
                    {
                        case "Access Point":
                            apType = Type.Ap;
                            break;
                        case "Ad Hoc":
                            apType = Type.AdHoc;
                            break;
                    }
                    bool enc = scannerGridView.Rows[i].Cells["securityColumn"].Value.ToString() != "None";
                    exportFile.Add(scannerGridView.Rows[i].Cells["ssidColumn"].Value.ToString(),
                                   scannerGridView.Rows[i].Cells["macColumn"].Value.ToString(),
                                   Convert.ToInt32(scannerGridView.Rows[i].Cells["signalColumn"].Value),
                                   scannerGridView.Rows[i].Cells["firstSeenColumn"].Value.ToString(),
                                   scannerGridView.Rows[i].Cells["lastSeenColumn"].Value.ToString(),
                                   Convert.ToUInt32(scannerGridView.Rows[i].Cells["channelColumn"].Value),
                                   Convert.ToUInt32(scannerGridView.Rows[i].Cells["speedColumn"].Value),
                                   enc,
                                   Convert.ToDouble(latlon[0].Trim(), CultureInfo.InvariantCulture),
                                   Convert.ToDouble(latlon[1].Trim(), CultureInfo.InvariantCulture),
                                   apType);
                }
                sdlgExport.ShowDialog(this);
                sdlgExport.Title = "Save this set of AP Scan Results";
                if (sdlgExport.FileName == "")
                {
                    return;
                }
                exportFile.WriteOut(sdlgExport.FileName);
            }
            catch
            {
                MessageBox.Show("The data could not be exported", "NS1 Export Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }




        private void ExportToKmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (KmlExporterForm logViewerForm = new KmlExporterForm())
            {
                logViewerForm.ShowDialog();
            }
        }
        #endregion




        private void Band2400Button_CheckedChanged(object sender, EventArgs e)
        {
            channelGraph.Band = ChannelView.BandType.Band2400MHz;
            Settings.Default.ChannelView24 = true;
            channelGraph.Invalidate();
        }




        private void Band5000Button_CheckedChanged(object sender, EventArgs e)
        {
            channelGraph.Band = ChannelView.BandType.Band5000MHz;
            Settings.Default.ChannelView24 = false;
            channelGraph.Invalidate();
        }




        /// <summary>
        /// Updates setting for displaying time graph and then calls UpdateGraphView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayTimeGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.DisplayTimeGraph = displayTimeGraphToolStripMenuItem.Checked;
            UpdateGraphsInView();
        }




        /// <summary>
        /// Updates setting for displaying channel graph and then calls UpdateGraphView
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DisplayChannelGraphToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings.Default.DisplayChannelGraph = displayChannelGraphToolStripMenuItem.Checked;
            UpdateGraphsInView();
            channelGraph.Invalidate();
        }
        #endregion

        private void ImportScan()
        {
             string sSelectedSite;
           //  int iSelectedDepartment;
            string sSelectedSiteID;
            InputBox testDialog = new InputBox();

            // Show testDialog as a modal dialog and determine if DialogResult = OK.
            testDialog.ShowDialog(this);
           
            sSelectedSite = testDialog.ddlSites.Text.ToString();
            if (testDialog.DialogResult == DialogResult.OK)
            {
                  sSelectedSite = testDialog.ddlSites.Text.ToString();
                //  iSelectedDepartment = Convert.ToInt32(testDialog.cbDepartment.SelectedValue);
                  sSelectedSiteID = testDialog.SiteID;
            }
            else
            {
                sSelectedSite = "Cancelled";
              //  iSelectedDepartment = 0;
                sSelectedSiteID = "0";
            }
            testDialog.Dispose();
            try
            {
                if (sSelectedSite != "Cancelled" && sSelectedSite != "")
                {
                    string filename = testDialog.selectedFile;
                    byte[] bData = System.IO.File.ReadAllBytes(filename);


                    System.Net.Http.HttpClient client = new HttpClient();
                    client.BaseAddress = new Uri("http://rhythm.myenovate.com/");

                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    var parms = new Dictionary<string, object>();
                    string myData = Convert.ToBase64String(bData);
                    parms.Add("SiteId", sSelectedSiteID);
                    parms.Add("Data", myData);
                 //   if (testDialog.bUseDepartment == true)
                //    {
                //        parms.Add("Dept", iSelectedDepartment);
              //      }
                    parms.Add("UserId", iUserID.ToString());
                           parms.Add("SignalThreshhold", "70");
                    //test
                    HttpResponseMessage response = client.PostAsJsonAsync("api/ScanData/", parms).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var dt = response.Content.ReadAsAsync<DataTable>().Result;
                        MessageBox.Show("Successfully Added AP Scan to Site " + sSelectedSite,"Success");
                    }
                    else
                    {
                        MessageBox.Show("Error Importing, Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    }

                }
            }

            catch (Exception Ex)
            {
                MessageBox.Show("An error occured and the AP Scan was not imported.");
            }
        }
        private void btnImport_Click(object sender, EventArgs e)
        {
            ImportScan();
           
        }



    }
}
/*--- end of ScannerForm.cs -----------------------------------------------------*/

