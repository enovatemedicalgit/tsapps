namespace Inssider
{
    partial class ScannerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScannerForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.GraphSplitContainer = new System.Windows.Forms.SplitContainer();
            this.timeGraph = new Inssider.TimeGraph();
            this.channelGraph = new Inssider.ChannelView();
            this.columnsContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.macToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vendorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ssidToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.channelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.signalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.securityToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.typeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.speedToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.firstSeenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lastSeenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.locationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gPXToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asNS1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyChannelGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllNetworksToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.automaticallyResizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayTimeGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayChannelGraphToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.topPanel = new System.Windows.Forms.Panel();
            this.btnImport = new System.Windows.Forms.Button();
            this.channelGraphPrefsPanel = new System.Windows.Forms.Panel();
            this.rdoBand5000 = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.rdoBand2400 = new System.Windows.Forms.RadioButton();
            this.cbxInterfaceList = new System.Windows.Forms.ComboBox();
            this.btnScanButton = new System.Windows.Forms.Button();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.gridSplitter = new System.Windows.Forms.Splitter();
            this.scannerGridView = new System.Windows.Forms.DataGridView();
            this.idColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.sigIndColumn = new System.Windows.Forms.DataGridViewImageColumn();
            this.macColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vendorColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ssidColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.channelColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.signalColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.securityColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.typeColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.speedColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstSeenColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastSeenColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.locationColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldSigColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.oldSig2Column = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rowContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.adPanel = new System.Windows.Forms.Panel();
            this.startImageLabel = new System.Windows.Forms.Label();
            this.sdlgExport = new System.Windows.Forms.SaveFileDialog();
            this.imageSignalLevelEnc = new System.Windows.Forms.ImageList(this.components);
            this.imageSignalLevel = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtAPstat = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.txtGPSstat = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblUsername = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GraphSplitContainer)).BeginInit();
            this.GraphSplitContainer.Panel1.SuspendLayout();
            this.GraphSplitContainer.Panel2.SuspendLayout();
            this.GraphSplitContainer.SuspendLayout();
            this.columnsContextMenuStrip.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.channelGraphPrefsPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scannerGridView)).BeginInit();
            this.rowContextMenuStrip.SuspendLayout();
            this.adPanel.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GraphSplitContainer
            // 
            this.GraphSplitContainer.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.GraphSplitContainer, "GraphSplitContainer");
            this.GraphSplitContainer.Name = "GraphSplitContainer";
            // 
            // GraphSplitContainer.Panel1
            // 
            this.GraphSplitContainer.Panel1.Controls.Add(this.timeGraph);
            // 
            // GraphSplitContainer.Panel2
            // 
            this.GraphSplitContainer.Panel2.Controls.Add(this.channelGraph);
            // 
            // timeGraph
            // 
            this.timeGraph.AutoScale = false;
            this.timeGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.timeGraph.BackgroundImage = global::Inssider.Properties.Resources.numbersign_background;
            this.timeGraph.BottomMargin = 25;
            resources.ApplyResources(this.timeGraph, "timeGraph");
            this.timeGraph.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            this.timeGraph.GraphColor = System.Drawing.Color.Black;
            this.timeGraph.GraphTimeSpan = System.TimeSpan.Parse("00:05:00");
            this.timeGraph.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.timeGraph.GridSpacing = 10;
            this.timeGraph.HighQuality = false;
            this.timeGraph.LeftMargin = 50;
            this.timeGraph.LineWidth = ((uint)(1u));
            this.timeGraph.MaxMagnitude = 0F;
            this.timeGraph.MinMagnitude = -100F;
            this.timeGraph.Name = "timeGraph";
            this.timeGraph.RightMargin = 15;
            this.timeGraph.SelectedWidth = ((uint)(3u));
            this.timeGraph.ShowLabels = true;
            this.timeGraph.ShowMinuteLines = true;
            this.timeGraph.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.timeGraph.TopMargin = 15;
            // 
            // channelGraph
            // 
            this.channelGraph.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.channelGraph.BackgroundImage = global::Inssider.Properties.Resources.numbersign_background;
            this.channelGraph.Band = Inssider.ChannelView.BandType.Band2400MHz;
            this.channelGraph.BottomMargin = 25;
            resources.ApplyResources(this.channelGraph, "channelGraph");
            this.channelGraph.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.channelGraph.GraphBackColor = System.Drawing.Color.Black;
            this.channelGraph.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.channelGraph.HighChannelForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.channelGraph.LeftMargin = 55;
            this.channelGraph.MaxAmplitude = 0F;
            this.channelGraph.MaxFrequency = 2485F;
            this.channelGraph.MinAmplitude = -100F;
            this.channelGraph.MinFrequency = 2400F;
            this.channelGraph.Name = "channelGraph";
            this.channelGraph.Networks = null;
            this.channelGraph.RightMargin = 15;
            this.channelGraph.TopMargin = 15;
            // 
            // columnsContextMenuStrip
            // 
            this.columnsContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.macToolStripMenuItem,
            this.vendorToolStripMenuItem,
            this.ssidToolStripMenuItem,
            this.channelToolStripMenuItem,
            this.signalToolStripMenuItem,
            this.securityToolStripMenuItem,
            this.typeToolStripMenuItem,
            this.speedToolStripMenuItem,
            this.firstSeenToolStripMenuItem,
            this.lastSeenToolStripMenuItem,
            this.locationToolStripMenuItem});
            this.columnsContextMenuStrip.Name = "columnsContextMenuStrip";
            resources.ApplyResources(this.columnsContextMenuStrip, "columnsContextMenuStrip");
            // 
            // macToolStripMenuItem
            // 
            this.macToolStripMenuItem.Checked = true;
            this.macToolStripMenuItem.CheckOnClick = true;
            this.macToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.macToolStripMenuItem.Name = "macToolStripMenuItem";
            resources.ApplyResources(this.macToolStripMenuItem, "macToolStripMenuItem");
            this.macToolStripMenuItem.Click += new System.EventHandler(this.MacToolStripMenuItem_Click);
            // 
            // vendorToolStripMenuItem
            // 
            this.vendorToolStripMenuItem.Checked = true;
            this.vendorToolStripMenuItem.CheckOnClick = true;
            this.vendorToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.vendorToolStripMenuItem.Name = "vendorToolStripMenuItem";
            resources.ApplyResources(this.vendorToolStripMenuItem, "vendorToolStripMenuItem");
            this.vendorToolStripMenuItem.Click += new System.EventHandler(this.VendorToolStripMenuItem_Click);
            // 
            // ssidToolStripMenuItem
            // 
            this.ssidToolStripMenuItem.Checked = true;
            this.ssidToolStripMenuItem.CheckOnClick = true;
            this.ssidToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ssidToolStripMenuItem.Name = "ssidToolStripMenuItem";
            resources.ApplyResources(this.ssidToolStripMenuItem, "ssidToolStripMenuItem");
            this.ssidToolStripMenuItem.Click += new System.EventHandler(this.SsidToolStripMenuItem_Click);
            // 
            // channelToolStripMenuItem
            // 
            this.channelToolStripMenuItem.Checked = true;
            this.channelToolStripMenuItem.CheckOnClick = true;
            this.channelToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.channelToolStripMenuItem.Name = "channelToolStripMenuItem";
            resources.ApplyResources(this.channelToolStripMenuItem, "channelToolStripMenuItem");
            this.channelToolStripMenuItem.Click += new System.EventHandler(this.ChannelToolStripMenuItem_Click);
            // 
            // signalToolStripMenuItem
            // 
            this.signalToolStripMenuItem.Checked = true;
            this.signalToolStripMenuItem.CheckOnClick = true;
            this.signalToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.signalToolStripMenuItem.Name = "signalToolStripMenuItem";
            resources.ApplyResources(this.signalToolStripMenuItem, "signalToolStripMenuItem");
            this.signalToolStripMenuItem.Click += new System.EventHandler(this.SignalToolStripMenuItem_Click);
            // 
            // securityToolStripMenuItem
            // 
            this.securityToolStripMenuItem.Checked = true;
            this.securityToolStripMenuItem.CheckOnClick = true;
            this.securityToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.securityToolStripMenuItem.Name = "securityToolStripMenuItem";
            resources.ApplyResources(this.securityToolStripMenuItem, "securityToolStripMenuItem");
            this.securityToolStripMenuItem.Click += new System.EventHandler(this.SecurityToolStripMenuItem_Click);
            // 
            // typeToolStripMenuItem
            // 
            this.typeToolStripMenuItem.Checked = true;
            this.typeToolStripMenuItem.CheckOnClick = true;
            this.typeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.typeToolStripMenuItem.Name = "typeToolStripMenuItem";
            resources.ApplyResources(this.typeToolStripMenuItem, "typeToolStripMenuItem");
            this.typeToolStripMenuItem.Click += new System.EventHandler(this.NetworkToolStripMenuItem_Click);
            // 
            // speedToolStripMenuItem
            // 
            this.speedToolStripMenuItem.Checked = true;
            this.speedToolStripMenuItem.CheckOnClick = true;
            this.speedToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.speedToolStripMenuItem.Name = "speedToolStripMenuItem";
            resources.ApplyResources(this.speedToolStripMenuItem, "speedToolStripMenuItem");
            this.speedToolStripMenuItem.Click += new System.EventHandler(this.SpeedToolStripMenuItem_Click);
            // 
            // firstSeenToolStripMenuItem
            // 
            this.firstSeenToolStripMenuItem.Checked = true;
            this.firstSeenToolStripMenuItem.CheckOnClick = true;
            this.firstSeenToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.firstSeenToolStripMenuItem.Name = "firstSeenToolStripMenuItem";
            resources.ApplyResources(this.firstSeenToolStripMenuItem, "firstSeenToolStripMenuItem");
            this.firstSeenToolStripMenuItem.Click += new System.EventHandler(this.FirstSeenToolStripMenuItem_Click);
            // 
            // lastSeenToolStripMenuItem
            // 
            this.lastSeenToolStripMenuItem.Checked = true;
            this.lastSeenToolStripMenuItem.CheckOnClick = true;
            this.lastSeenToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.lastSeenToolStripMenuItem.Name = "lastSeenToolStripMenuItem";
            resources.ApplyResources(this.lastSeenToolStripMenuItem, "lastSeenToolStripMenuItem");
            this.lastSeenToolStripMenuItem.Click += new System.EventHandler(this.LastTimeToolStripMenuItem_Click);
            // 
            // locationToolStripMenuItem
            // 
            this.locationToolStripMenuItem.Checked = true;
            this.locationToolStripMenuItem.CheckOnClick = true;
            this.locationToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.locationToolStripMenuItem.Name = "locationToolStripMenuItem";
            resources.ApplyResources(this.locationToolStripMenuItem, "locationToolStripMenuItem");
            this.locationToolStripMenuItem.Click += new System.EventHandler(this.LocationToolStripMenuItem_Click);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.BackColor = System.Drawing.SystemColors.Control;
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem1,
            this.editToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.mainMenuStrip, "mainMenuStrip");
            this.mainMenuStrip.Name = "mainMenuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gPXToolStripMenuItem,
            this.asNS1ToolStripMenuItem,
            this.toolStripSeparator3,
            this.preferencesToolStripMenuItem,
            this.toolStripSeparator4,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // gPXToolStripMenuItem
            // 
            this.gPXToolStripMenuItem.Name = "gPXToolStripMenuItem";
            resources.ApplyResources(this.gPXToolStripMenuItem, "gPXToolStripMenuItem");
            this.gPXToolStripMenuItem.Click += new System.EventHandler(this.ExportToKmlToolStripMenuItem_Click);
            // 
            // asNS1ToolStripMenuItem
            // 
            this.asNS1ToolStripMenuItem.Name = "asNS1ToolStripMenuItem";
            resources.ApplyResources(this.asNS1ToolStripMenuItem, "asNS1ToolStripMenuItem");
            this.asNS1ToolStripMenuItem.Click += new System.EventHandler(this.ExportAsNS1ToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            resources.ApplyResources(this.preferencesToolStripMenuItem, "preferencesToolStripMenuItem");
            this.preferencesToolStripMenuItem.Click += new System.EventHandler(this.PreferencesToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            resources.ApplyResources(this.exitToolStripMenuItem, "exitToolStripMenuItem");
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem1
            // 
            this.editToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.copyImageToolStripMenuItem,
            this.copyChannelGraphToolStripMenuItem});
            this.editToolStripMenuItem1.Name = "editToolStripMenuItem1";
            resources.ApplyResources(this.editToolStripMenuItem1, "editToolStripMenuItem1");
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            resources.ApplyResources(this.copyToolStripMenuItem, "copyToolStripMenuItem");
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.CopyDataToolStripMenuItem_Click);
            // 
            // copyImageToolStripMenuItem
            // 
            this.copyImageToolStripMenuItem.Name = "copyImageToolStripMenuItem";
            resources.ApplyResources(this.copyImageToolStripMenuItem, "copyImageToolStripMenuItem");
            this.copyImageToolStripMenuItem.Click += new System.EventHandler(this.CopyTimeGraphToolStripMenuItem_Click);
            // 
            // copyChannelGraphToolStripMenuItem
            // 
            this.copyChannelGraphToolStripMenuItem.Name = "copyChannelGraphToolStripMenuItem";
            resources.ApplyResources(this.copyChannelGraphToolStripMenuItem, "copyChannelGraphToolStripMenuItem");
            this.copyChannelGraphToolStripMenuItem.Click += new System.EventHandler(this.CopyChannelGraphToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showAllNetworksToolStripMenuItem,
            this.automaticallyResizeToolStripMenuItem,
            this.displayTimeGraphToolStripMenuItem,
            this.displayChannelGraphToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            resources.ApplyResources(this.editToolStripMenuItem, "editToolStripMenuItem");
            // 
            // showAllNetworksToolStripMenuItem
            // 
            this.showAllNetworksToolStripMenuItem.Name = "showAllNetworksToolStripMenuItem";
            resources.ApplyResources(this.showAllNetworksToolStripMenuItem, "showAllNetworksToolStripMenuItem");
            this.showAllNetworksToolStripMenuItem.Click += new System.EventHandler(this.ShowAllNetworksToolStripMenuItem_Click);
            // 
            // automaticallyResizeToolStripMenuItem
            // 
            this.automaticallyResizeToolStripMenuItem.Checked = true;
            this.automaticallyResizeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this.automaticallyResizeToolStripMenuItem, "automaticallyResizeToolStripMenuItem");
            this.automaticallyResizeToolStripMenuItem.Name = "automaticallyResizeToolStripMenuItem";
            this.automaticallyResizeToolStripMenuItem.Click += new System.EventHandler(this.AutomaticallyResizeToolStripMenuItem_Click);
            // 
            // displayTimeGraphToolStripMenuItem
            // 
            this.displayTimeGraphToolStripMenuItem.Checked = true;
            this.displayTimeGraphToolStripMenuItem.CheckOnClick = true;
            this.displayTimeGraphToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displayTimeGraphToolStripMenuItem.Name = "displayTimeGraphToolStripMenuItem";
            resources.ApplyResources(this.displayTimeGraphToolStripMenuItem, "displayTimeGraphToolStripMenuItem");
            this.displayTimeGraphToolStripMenuItem.Click += new System.EventHandler(this.DisplayTimeGraphToolStripMenuItem_Click);
            // 
            // displayChannelGraphToolStripMenuItem
            // 
            this.displayChannelGraphToolStripMenuItem.Checked = true;
            this.displayChannelGraphToolStripMenuItem.CheckOnClick = true;
            this.displayChannelGraphToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.displayChannelGraphToolStripMenuItem.Name = "displayChannelGraphToolStripMenuItem";
            resources.ApplyResources(this.displayChannelGraphToolStripMenuItem, "displayChannelGraphToolStripMenuItem");
            this.displayChannelGraphToolStripMenuItem.Click += new System.EventHandler(this.DisplayChannelGraphToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem,
            this.toolStripSeparator2});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.btnImport);
            this.topPanel.Controls.Add(this.channelGraphPrefsPanel);
            this.topPanel.Controls.Add(this.cbxInterfaceList);
            this.topPanel.Controls.Add(this.btnScanButton);
            resources.ApplyResources(this.topPanel, "topPanel");
            this.topPanel.Name = "topPanel";
            // 
            // btnImport
            // 
            resources.ApplyResources(this.btnImport, "btnImport");
            this.btnImport.Name = "btnImport";
            this.btnImport.UseVisualStyleBackColor = true;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // channelGraphPrefsPanel
            // 
            this.channelGraphPrefsPanel.Controls.Add(this.rdoBand5000);
            this.channelGraphPrefsPanel.Controls.Add(this.label1);
            this.channelGraphPrefsPanel.Controls.Add(this.rdoBand2400);
            resources.ApplyResources(this.channelGraphPrefsPanel, "channelGraphPrefsPanel");
            this.channelGraphPrefsPanel.Name = "channelGraphPrefsPanel";
            // 
            // rdoBand5000
            // 
            resources.ApplyResources(this.rdoBand5000, "rdoBand5000");
            this.rdoBand5000.Name = "rdoBand5000";
            this.rdoBand5000.TabStop = true;
            this.rdoBand5000.UseVisualStyleBackColor = true;
            this.rdoBand5000.CheckedChanged += new System.EventHandler(this.Band5000Button_CheckedChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // rdoBand2400
            // 
            resources.ApplyResources(this.rdoBand2400, "rdoBand2400");
            this.rdoBand2400.Name = "rdoBand2400";
            this.rdoBand2400.TabStop = true;
            this.rdoBand2400.UseVisualStyleBackColor = true;
            this.rdoBand2400.CheckedChanged += new System.EventHandler(this.Band2400Button_CheckedChanged);
            // 
            // cbxInterfaceList
            // 
            resources.ApplyResources(this.cbxInterfaceList, "cbxInterfaceList");
            this.cbxInterfaceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxInterfaceList.Name = "cbxInterfaceList";
            // 
            // btnScanButton
            // 
            resources.ApplyResources(this.btnScanButton, "btnScanButton");
            this.btnScanButton.Name = "btnScanButton";
            this.btnScanButton.UseVisualStyleBackColor = true;
            this.btnScanButton.Click += new System.EventHandler(this.ScanButton_Click);
            // 
            // bottomPanel
            // 
            this.bottomPanel.BackColor = System.Drawing.SystemColors.Control;
            this.bottomPanel.Controls.Add(this.GraphSplitContainer);
            this.bottomPanel.Controls.Add(this.gridSplitter);
            this.bottomPanel.Controls.Add(this.scannerGridView);
            resources.ApplyResources(this.bottomPanel, "bottomPanel");
            this.bottomPanel.Name = "bottomPanel";
            // 
            // gridSplitter
            // 
            this.gridSplitter.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.gridSplitter, "gridSplitter");
            this.gridSplitter.Name = "gridSplitter";
            this.gridSplitter.TabStop = false;
            this.gridSplitter.SplitterMoving += new System.Windows.Forms.SplitterEventHandler(this.GraphSplitter_SplitterMoving);
            // 
            // scannerGridView
            // 
            this.scannerGridView.AllowUserToAddRows = false;
            this.scannerGridView.AllowUserToDeleteRows = false;
            this.scannerGridView.AllowUserToOrderColumns = true;
            this.scannerGridView.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White;
            this.scannerGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.scannerGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.scannerGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.scannerGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.scannerGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.scannerGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idColumn,
            this.checkColumn,
            this.sigIndColumn,
            this.macColumn,
            this.vendorColumn,
            this.ssidColumn,
            this.channelColumn,
            this.signalColumn,
            this.securityColumn,
            this.typeColumn,
            this.speedColumn,
            this.firstSeenColumn,
            this.lastSeenColumn,
            this.locationColumn,
            this.oldSigColumn,
            this.oldSig2Column});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(242)))), ((int)(((byte)(242)))));
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(50)))), ((int)(((byte)(50)))));
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.scannerGridView.DefaultCellStyle = dataGridViewCellStyle4;
            resources.ApplyResources(this.scannerGridView, "scannerGridView");
            this.scannerGridView.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.scannerGridView.Name = "scannerGridView";
            this.scannerGridView.ReadOnly = true;
            this.scannerGridView.RowHeadersVisible = false;
            dataGridViewCellStyle5.Padding = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.scannerGridView.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.scannerGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.scannerGridView.ShowRowErrors = false;
            this.scannerGridView.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ScannerGridView_CellMouseClick);
            this.scannerGridView.ColumnDisplayIndexChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.ScannerGridView_ColumnWidthChanged);
            this.scannerGridView.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.ScannerGridView_ColumnHeaderMouseClick);
            this.scannerGridView.ColumnWidthChanged += new System.Windows.Forms.DataGridViewColumnEventHandler(this.ScannerGridView_ColumnWidthChanged);
            this.scannerGridView.SelectionChanged += new System.EventHandler(this.ScannerGridView_SelectionChanged);
            this.scannerGridView.SortCompare += new System.Windows.Forms.DataGridViewSortCompareEventHandler(this.ScannerGridView_SortCompare);
            this.scannerGridView.VisibleChanged += new System.EventHandler(this.ScannerGridView_VisibleChanged);
            // 
            // idColumn
            // 
            this.idColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.idColumn, "idColumn");
            this.idColumn.Name = "idColumn";
            this.idColumn.ReadOnly = true;
            // 
            // checkColumn
            // 
            this.checkColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.checkColumn, "checkColumn");
            this.checkColumn.Name = "checkColumn";
            this.checkColumn.ReadOnly = true;
            this.checkColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // sigIndColumn
            // 
            this.sigIndColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.sigIndColumn, "sigIndColumn");
            this.sigIndColumn.Name = "sigIndColumn";
            this.sigIndColumn.ReadOnly = true;
            this.sigIndColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // macColumn
            // 
            this.macColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.macColumn.DataPropertyName = "MacAddress";
            this.macColumn.FillWeight = 110F;
            resources.ApplyResources(this.macColumn, "macColumn");
            this.macColumn.Name = "macColumn";
            this.macColumn.ReadOnly = true;
            // 
            // vendorColumn
            // 
            this.vendorColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.vendorColumn.DataPropertyName = "Vendor";
            resources.ApplyResources(this.vendorColumn, "vendorColumn");
            this.vendorColumn.Name = "vendorColumn";
            this.vendorColumn.ReadOnly = true;
            // 
            // ssidColumn
            // 
            this.ssidColumn.DataPropertyName = "Ssid";
            this.ssidColumn.FillWeight = 126.93F;
            resources.ApplyResources(this.ssidColumn, "ssidColumn");
            this.ssidColumn.Name = "ssidColumn";
            this.ssidColumn.ReadOnly = true;
            // 
            // channelColumn
            // 
            this.channelColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.channelColumn.DataPropertyName = "Channel";
            this.channelColumn.FillWeight = 55F;
            resources.ApplyResources(this.channelColumn, "channelColumn");
            this.channelColumn.Name = "channelColumn";
            this.channelColumn.ReadOnly = true;
            // 
            // signalColumn
            // 
            this.signalColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.signalColumn.DataPropertyName = "Rssi";
            dataGridViewCellStyle3.NullValue = null;
            this.signalColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.signalColumn.FillWeight = 55F;
            resources.ApplyResources(this.signalColumn, "signalColumn");
            this.signalColumn.Name = "signalColumn";
            this.signalColumn.ReadOnly = true;
            // 
            // securityColumn
            // 
            this.securityColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.securityColumn.DataPropertyName = "Privacy";
            this.securityColumn.FillWeight = 60F;
            resources.ApplyResources(this.securityColumn, "securityColumn");
            this.securityColumn.Name = "securityColumn";
            this.securityColumn.ReadOnly = true;
            // 
            // typeColumn
            // 
            this.typeColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.typeColumn.DataPropertyName = "NetworkType";
            this.typeColumn.FillWeight = 80F;
            resources.ApplyResources(this.typeColumn, "typeColumn");
            this.typeColumn.Name = "typeColumn";
            this.typeColumn.ReadOnly = true;
            // 
            // speedColumn
            // 
            this.speedColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.speedColumn.DataPropertyName = "Speed";
            this.speedColumn.FillWeight = 58.17627F;
            resources.ApplyResources(this.speedColumn, "speedColumn");
            this.speedColumn.Name = "speedColumn";
            this.speedColumn.ReadOnly = true;
            // 
            // firstSeenColumn
            // 
            this.firstSeenColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.firstSeenColumn.DataPropertyName = "FirstSeen";
            this.firstSeenColumn.FillWeight = 50F;
            resources.ApplyResources(this.firstSeenColumn, "firstSeenColumn");
            this.firstSeenColumn.Name = "firstSeenColumn";
            this.firstSeenColumn.ReadOnly = true;
            // 
            // lastSeenColumn
            // 
            this.lastSeenColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.lastSeenColumn.DataPropertyName = "LastSeen";
            this.lastSeenColumn.FillWeight = 50F;
            resources.ApplyResources(this.lastSeenColumn, "lastSeenColumn");
            this.lastSeenColumn.Name = "lastSeenColumn";
            this.lastSeenColumn.ReadOnly = true;
            // 
            // locationColumn
            // 
            this.locationColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.locationColumn, "locationColumn");
            this.locationColumn.Name = "locationColumn";
            this.locationColumn.ReadOnly = true;
            // 
            // oldSigColumn
            // 
            resources.ApplyResources(this.oldSigColumn, "oldSigColumn");
            this.oldSigColumn.Name = "oldSigColumn";
            this.oldSigColumn.ReadOnly = true;
            // 
            // oldSig2Column
            // 
            resources.ApplyResources(this.oldSig2Column, "oldSig2Column");
            this.oldSig2Column.Name = "oldSig2Column";
            this.oldSig2Column.ReadOnly = true;
            // 
            // rowContextMenuStrip
            // 
            this.rowContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hideToolStripMenuItem,
            this.showAllToolStripMenuItem});
            this.rowContextMenuStrip.Name = "rowContextMenuStrip";
            resources.ApplyResources(this.rowContextMenuStrip, "rowContextMenuStrip");
            // 
            // hideToolStripMenuItem
            // 
            this.hideToolStripMenuItem.Name = "hideToolStripMenuItem";
            resources.ApplyResources(this.hideToolStripMenuItem, "hideToolStripMenuItem");
            this.hideToolStripMenuItem.Click += new System.EventHandler(this.HideToolStripMenuItem_Click);
            // 
            // showAllToolStripMenuItem
            // 
            this.showAllToolStripMenuItem.Name = "showAllToolStripMenuItem";
            resources.ApplyResources(this.showAllToolStripMenuItem, "showAllToolStripMenuItem");
            this.showAllToolStripMenuItem.Click += new System.EventHandler(this.ShowAllNetworksToolStripMenuItem_Click);
            // 
            // adPanel
            // 
            resources.ApplyResources(this.adPanel, "adPanel");
            this.adPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(36)))), ((int)(((byte)(36)))));
            this.adPanel.Controls.Add(this.bottomPanel);
            this.adPanel.Controls.Add(this.startImageLabel);
            this.adPanel.Name = "adPanel";
            // 
            // startImageLabel
            // 
            resources.ApplyResources(this.startImageLabel, "startImageLabel");
            this.startImageLabel.Name = "startImageLabel";
            this.startImageLabel.Click += new System.EventHandler(this.StartImageLabel_Click);
            this.startImageLabel.MouseEnter += new System.EventHandler(this.ClickableControl_MouseEnter);
            this.startImageLabel.MouseLeave += new System.EventHandler(this.ClickableControl_MouseLeave);
            // 
            // sdlgExport
            // 
            this.sdlgExport.DefaultExt = "*.ns1";
            resources.ApplyResources(this.sdlgExport, "sdlgExport");
            this.sdlgExport.SupportMultiDottedExtensions = true;
            // 
            // imageSignalLevelEnc
            // 
            this.imageSignalLevelEnc.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageSignalLevelEnc.ImageStream")));
            this.imageSignalLevelEnc.TransparentColor = System.Drawing.Color.Magenta;
            this.imageSignalLevelEnc.Images.SetKeyName(0, "5E.bmp");
            this.imageSignalLevelEnc.Images.SetKeyName(1, "4E.bmp");
            this.imageSignalLevelEnc.Images.SetKeyName(2, "3E.bmp");
            this.imageSignalLevelEnc.Images.SetKeyName(3, "2E.bmp");
            this.imageSignalLevelEnc.Images.SetKeyName(4, "1E.bmp");
            this.imageSignalLevelEnc.Images.SetKeyName(5, "0E.bmp");
            // 
            // imageSignalLevel
            // 
            this.imageSignalLevel.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageSignalLevel.ImageStream")));
            this.imageSignalLevel.TransparentColor = System.Drawing.Color.Magenta;
            this.imageSignalLevel.Images.SetKeyName(0, "5.bmp");
            this.imageSignalLevel.Images.SetKeyName(1, "4.bmp");
            this.imageSignalLevel.Images.SetKeyName(2, "3.bmp");
            this.imageSignalLevel.Images.SetKeyName(3, "2.bmp");
            this.imageSignalLevel.Images.SetKeyName(4, "1.bmp");
            this.imageSignalLevel.Images.SetKeyName(5, "0.bmp");
            // 
            // statusStrip1
            // 
            this.statusStrip1.AllowMerge = false;
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.txtAPstat,
            this.toolStripStatusLabel1,
            this.txtGPSstat});
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.SizingGrip = false;
            // 
            // txtAPstat
            // 
            this.txtAPstat.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.txtAPstat.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.txtAPstat.Name = "txtAPstat";
            resources.ApplyResources(this.txtAPstat, "txtAPstat");
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripStatusLabel1.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            resources.ApplyResources(this.toolStripStatusLabel1, "toolStripStatusLabel1");
            this.toolStripStatusLabel1.Spring = true;
            // 
            // txtGPSstat
            // 
            this.txtGPSstat.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.txtGPSstat.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken;
            this.txtGPSstat.Name = "txtGPSstat";
            resources.ApplyResources(this.txtGPSstat, "txtGPSstat");
            // 
            // lblUsername
            // 
            resources.ApplyResources(this.lblUsername, "lblUsername");
            this.lblUsername.Name = "lblUsername";
            // 
            // ScannerForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.adPanel);
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.mainMenuStrip);
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "ScannerForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ScannerForm_FormClosing);
            this.Load += new System.EventHandler(this.ScannerForm_Load);
            this.LocationChanged += new System.EventHandler(this.ScannerForm_LocationChanged);
            this.SizeChanged += new System.EventHandler(this.ScannerForm_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.ScannerForm_VisibleChanged);
            this.GraphSplitContainer.Panel1.ResumeLayout(false);
            this.GraphSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GraphSplitContainer)).EndInit();
            this.GraphSplitContainer.ResumeLayout(false);
            this.columnsContextMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.channelGraphPrefsPanel.ResumeLayout(false);
            this.channelGraphPrefsPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.scannerGridView)).EndInit();
            this.rowContextMenuStrip.ResumeLayout(false);
            this.adPanel.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip columnsContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem macToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ssidToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem channelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem signalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem securityToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem typeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem speedToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lastSeenToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem firstSeenToolStripMenuItem;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.ComboBox cbxInterfaceList;
        private System.Windows.Forms.Button btnScanButton;
        private System.Windows.Forms.Panel bottomPanel;
        private TimeGraph timeGraph;
        private System.Windows.Forms.DataGridView scannerGridView;
        private System.Windows.Forms.Splitter gridSplitter;
        private System.Windows.Forms.ToolStripMenuItem automaticallyResizeToolStripMenuItem;
        private System.Windows.Forms.SplitContainer GraphSplitContainer;
        private ChannelView channelGraph;
        private System.Windows.Forms.ContextMenuStrip rowContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem hideToolStripMenuItem;
        private System.Windows.Forms.Panel adPanel;
        private System.Windows.Forms.Label startImageLabel;
        private System.Windows.Forms.ToolStripMenuItem showAllToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog sdlgExport;
        private System.Windows.Forms.ImageList imageSignalLevelEnc;
        private System.Windows.Forms.ImageList imageSignalLevel;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel txtGPSstat;
        private System.Windows.Forms.ToolStripStatusLabel txtAPstat;
        private System.Windows.Forms.ToolStripMenuItem locationToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.RadioButton rdoBand2400;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RadioButton rdoBand5000;
        private System.Windows.Forms.Panel channelGraphPrefsPanel;
        private System.Windows.Forms.ToolStripMenuItem displayTimeGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayChannelGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vendorToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripMenuItem gPXToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyChannelGraphToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showAllNetworksToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asNS1ToolStripMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn checkColumn;
        private System.Windows.Forms.DataGridViewImageColumn sigIndColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn macColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vendorColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn ssidColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn channelColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn signalColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn securityColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn typeColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn speedColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstSeenColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastSeenColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn locationColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldSigColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn oldSig2Column;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.Label lblUsername;
    }
}

