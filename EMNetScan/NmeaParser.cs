////////////////////////////////////////////////////////////////
//
// Copyright (c) 2007-2010 MetaGeek, LLC
//
// Licensed under the Apache License, Version 2.0 (the "License"); 
// you may not use this file except in compliance with the License. 
// You may obtain a copy of the License at 
//
//	http://www.apache.org/licenses/LICENSE-2.0 
//
// Unless required by applicable law or agreed to in writing, software 
// distributed under the License is distributed on an "AS IS" BASIS, 
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
// See the License for the specific language governing permissions and 
// limitations under the License. 
//
////////////////////////////////////////////////////////////////

// ===================================================================
// This code was originally written by:
// Joel Carpenter
// Brisbane, QLD
// AUSTRALIA
// ===================================================================

using System;
using System.Globalization;
using System.Collections.Generic;

namespace Inssider
{
    public class Satellite
    {
        public int Id;
        public double Elevation;
        public double Azimuth;
        public double Snr;
    }

    public class NmeaParser
    {
        /// <summary>
        /// Enumerated type of GPS sentences
        /// </summary>
        public enum SentenceType
        {
            None = 0, Gprmc, Gpgsv, Gpgsa, Gpgga, Gpvtg
        }

        #region Private Data
        // Represents the EN-US culture, used for numbers in NMEA sentences
        private static readonly CultureInfo NmeaCultureInfo = CultureInfo.InvariantCulture;
        // Used to convert knots into km per hour
        private const double KphPerKnot = 1.8519984;

        private readonly char[] _delimiters = { ',', '*' };
        private double _longitude;
        private double _latitude;
        private double _speedKph;
        private DateTime _timestamp = DateTime.Now;
        private DateTime _satelliteTime = DateTime.Now;
        private double _course;
        private bool _hasFix;
        private double _magVar = double.NaN;
        private int _satelliteCount;
        private bool _allSatellitesLoaded;

        //Gpgsa
        private bool _isForced2D3D;
        private int _fixMode;
        private const int MaxChannels = 12;
        private int[] _satIDs;
        private double _pdop;
        private double _hdop;
        private double _vdop;

        //Gpgga
        private int _satellitesUsed;
        private int _positionFix;
        private double _altitude;
        private double _dgpsAge;
        private int _dgpsid;
        private double _geoIdSeperation;

        private readonly List<Satellite> _satellites = new List<Satellite>();

        public NmeaParser()
        {
            _geoIdSeperation = 0;
            _dgpsid = 0;
            _dgpsAge = 0;
            _altitude = 0;
            _positionFix = 0;
            _satellitesUsed = 0;
            _fixMode = 0;
            _allSatellitesLoaded = false;
            _satelliteCount = 0;
            _hasFix = false;
            _course = 0;
            _speedKph = 0;
            _latitude = 0;
            _longitude = 0;
        }

        #endregion

        #region Properties

        public double Longitude
        {
            get { return _longitude; }
        }

        public double Latitude
        {
            get { return _latitude; }
        }

        public double Speed
        {
            get { return _speedKph; }
        }

        public DateTime Timestamp
        {
            get { return _timestamp; }
        }

        public DateTime SatelliteTime
        {
            get { return _satelliteTime; }
        }

        public double Course
        {
            get { return _course; }
        }

        public bool HasFix
        {
            get { return _hasFix; }
        }

        public double MagVar
        {
            get { return _magVar; }
        }

        public List<Satellite> Satellites
        {
            get { return _satellites; }
        }

        public int SatelliteCount
        {
            get { return _satelliteCount; }
        }

        public bool GetAllSatellitesLoaded()
        {
            return _allSatellitesLoaded;
        }

        public int[] GetSatIDs()
        {
            return _satIDs;
        }

        public int FixMode
        {
            get { return _fixMode; }
        }

        public bool IsForced2D3D
        {
            get { return _isForced2D3D; }
        }

        public double PDOP
        {
            get { return _pdop; }

        }

        public double VDOP
        {
            get { return _vdop; }
        }

        public double HDOP
        {
            get { return _hdop; }
        }

        public int SatellitesUsed
        {
            get { return _satellitesUsed; }
        }

        public int PositionFix
        {
            get { return _positionFix; }
        }

        public double Altitude
        {
            get { return _altitude; }
        }

        public double GeoIdSeperation
        {
            get { return _geoIdSeperation; }
        }

        public double DGPSAge
        {
            get { return _dgpsAge; }
        }

        public int DGPSID
        {
            get { return _dgpsid; }
        }

        public string FixString
        {
            get
            {
                string fix;

                if (_positionFix <= 1)
                {
                    switch (_fixMode)
                    {
                        case 1:
                            fix = "none";
                            break;

                        case 2:
                            fix = "2d";
                            break;

                        case 3:
                            fix = "3d";
                            break;

                        default:
                            fix = "none";
                            break;
                    }
                }
                else
                {
                    switch (_positionFix)
                    {
                        case 2:
                            fix = "dgps";
                            break;

                        case 3:
                            fix = "pps";
                            break;

                        default:
                            fix = "none";
                            break;
                    }
                }

                return fix;
            }
        }

        #endregion

        #region Public Methods

        public SentenceType Parse(string sentence)
        {
            SentenceType type = SentenceType.None;

            // Discard the sentence if its checksum does not match our calculated checksum
            if (IsValidSentence(sentence))
            {
                bool result;

                // Look at the first word to decide where to go next
                switch (GetWords(sentence)[0])
                {
                    case "$GPRMC":
                        // A "Recommended Minimum" sentence was found!
                        result = ParseGprmc(sentence);
                        if (result)
                        {
                            type = SentenceType.Gprmc;
                        }
                        break;

                    case "$GPGSV":
                        // A "Satellites in View" sentence was received
                        result = ParseGpgsv(sentence);
                        if (result)
                        {
                            type = SentenceType.Gpgsv;
                        }
                        break;

                    case "$GPGSA":
                        result = ParseGpgsa(sentence);
                        if (result)
                        {
                            type = SentenceType.Gpgsa;
                        }
                        break;

                    // Fix Data
                    case "$GPGGA":
                        result = ParseGpgga(sentence);
                        if (result)
                        {
                            type = SentenceType.Gpgga;
                        }
                        break;

                    case "$GPVTG":
                        result = ParseGpvtg(sentence);
                        if (result)
                        {
                            type = SentenceType.Gpvtg;
                        }
                        break;

                    default:
                        // Indicate that the sentence was not recognized
                        break;
                }
            }

            return type;
        }

        // Divides a sentence into individual words
        public string[] GetWords(string sentence)
        {
            return sentence.Split(_delimiters);
        }
        // Interprets a $Gprmc message
        public bool ParseGprmc(string sentence)
        {
            string[] words = GetWords(sentence);

            string rawUtCtime = words[1];
            string rawStatus = words[2];
            string rawLatitude = words[3];
            string rawNSindicator = words[4];
            string rawLongitude = words[5];
            string rawEWindicator = words[6];
            string rawSpeedinKnots = words[7];
            string rawCourse = words[8];
            string rawUtCdate = words[9];
            string rawMagneticVariationDegrees = words[10];
            string rawMagneticVariationEw = words[11];

            //GET OUR POSITION. LATITUDE & LONGITUDE
            //If we have all the necessary information
            if (rawLatitude != "" && rawNSindicator != "" && rawLongitude != "" && rawEWindicator != "")
            {
                ParseCoordinates(rawLongitude, rawLatitude, rawNSindicator, rawEWindicator);
            }

            //DATE TIME
            //If we have the information
            if (rawUtCtime != "" && rawUtCdate != "")
            {
                ParseTime(rawUtCtime, rawUtCdate);
            }

            // SPEED
            //If we have the information
            if (rawSpeedinKnots != "")
            {
                // Convert to Kilometres per hour
                _speedKph = double.Parse(rawSpeedinKnots, NmeaCultureInfo) * KphPerKnot;
            }

            // BEARING/COURSE
            //If we have the information
            if (rawCourse != "")
            {
                // Indicate that the sentence was recognized
                _course = double.Parse(rawCourse, NmeaCultureInfo);
            }

            // SATELLITE FIX
            //If we have the information
            if (rawStatus != "")
            {
                switch (rawStatus)
                {
                    case "A":
                        _hasFix = true;
                        break;
                    case "V":
                        _hasFix = false;
                        break;
                }
            }

            //MAGNETIC VARIATION
            //if we have the information
            if (rawMagneticVariationDegrees != "" & rawMagneticVariationEw != "")
            {
                _magVar = double.Parse(rawMagneticVariationDegrees, NmeaCultureInfo);

                if (rawMagneticVariationEw == "W")
                {
                    _magVar = -_magVar;
                }
            }

            return true;

        }

        // Interprets a "Satellites in View" NMEA sentence
        public bool ParseGpgsv(string sentence)
        {
            try
            {
            string[] words = GetWords(sentence);

            string rawNumberOfMessages = words[1];
            string rawSequenceNumber = words[2];
            string rawSatellitesInView = words[3];
            _satelliteCount = int.Parse(rawSatellitesInView, NmeaCultureInfo);

            if (rawSequenceNumber == "1")
            {
                _satellites.Clear();
                _allSatellitesLoaded = false;
            }

            if (rawSequenceNumber == rawNumberOfMessages)
            {
                _allSatellitesLoaded = true;
            }

            int index = 4;

            while (index <= 16 && words[index] != "")
            {
                Satellite tempSatellite = new Satellite();
                string id = words[index];
                if (id != "")
                {
                    int.TryParse(id, NumberStyles.Integer, NmeaCultureInfo, out tempSatellite.Id);
                }
                string elevation = words[index + 1];
                if (elevation != "")
                {

                    tempSatellite.Elevation = double.Parse(elevation, NmeaCultureInfo);
                }
                string azimuth = words[index + 2];
                if (azimuth != "")
                {
                    tempSatellite.Azimuth = Convert.ToDouble(azimuth, CultureInfo.InvariantCulture);
                }

                string snr = words[index + 3];
                tempSatellite.Snr = snr == "" ? double.NaN : Convert.ToDouble(snr, CultureInfo.InvariantCulture);

                index = index + 4;

                _satellites.Add(tempSatellite);
            }
            }
            catch (Exception) { }
            // Indicate that the sentence was recognized
            return true;
        }

        // Interprets a "Fixed Satellites and DOP" NMEA sentence
        public bool ParseGpgsa(string sentence)
        {
            string[] words = GetWords(sentence);

            string rawMode1A = words[1];
            string rawMode1B = words[2];

            int[] satIDs = new int[MaxChannels];
            int idx = 3;
            int satCount = 0;

            for (int i = 0; i < MaxChannels; i++)
            {
                try
                {
                    satIDs[i] = Convert.ToInt32(words[idx]);
                    satCount++;
                }
                catch (FormatException)
                {
                    satIDs[i] = int.MaxValue;
                }
                idx++;
            }

            _satIDs = new int[satCount];

            for (int i = 0; i < satCount; i++)
            {
                _satIDs[i] = satIDs[i];
            }

            string rawPdop = words[idx];
            string rawHdop = words[idx + 1];
            string rawVdop = words[idx + 2];

            if (rawMode1A == "M")
            {
                _isForced2D3D = true;
            }
            if (rawMode1A == "A")
            {
                _isForced2D3D = false;
            }
            
            if (rawMode1B != "")
            {
                _fixMode = Convert.ToInt32(rawMode1B);
            }
            if (rawPdop != "")
                _pdop = Convert.ToDouble(rawPdop, CultureInfo.InvariantCulture);
            if (rawHdop != "")
                _hdop = Convert.ToDouble(rawHdop, CultureInfo.InvariantCulture);
            if (rawVdop != "")
                _vdop = Convert.ToDouble(rawVdop, CultureInfo.InvariantCulture);
            return true;
        }

        //Fix Data
        public bool ParseGpgga(string sentence)
        {
            bool result = false;

            try
            {
                string[] words = GetWords(sentence);

                if (words.Length >= 15)
                {
                    string rawUtCtime = words[1];
                    string rawLatitude = words[2];
                    string rawNsIndicator = words[3];
                    string rawLongitude = words[4];
                    string rawEwIndicator = words[5];
                    string rawPositionFix = words[6];
                    string rawSatellitesUsed = words[7];
                    string rawAltitude = words[9];
                    string rawGeoidSeperation = words[11];
                    string rawDgpsAge = words[13];
                    string rawDgpsStationId = words[14];

                    ParseTime(rawUtCtime, "");
                    ParseCoordinates(rawLongitude, rawLatitude, rawNsIndicator, rawEwIndicator);

                    if (rawSatellitesUsed != "")
                    {
                        _satellitesUsed = int.Parse(rawSatellitesUsed, NmeaCultureInfo);
                    }

                    if (rawAltitude != "")
                    {
                        _altitude = double.Parse(rawAltitude, NmeaCultureInfo);
                    }

                    if (rawGeoidSeperation != "")
                    {
                        _geoIdSeperation = double.Parse(rawGeoidSeperation, NmeaCultureInfo);
                    }

                    if (rawDgpsAge != "")
                    {
                        _dgpsAge = double.Parse(rawDgpsAge, NmeaCultureInfo);
                    }

                    if (rawDgpsStationId != "")
                    {
                        _dgpsid = int.Parse(rawDgpsStationId, NmeaCultureInfo);
                    }

                    if (rawPositionFix != "")
                    {
                        _positionFix = int.Parse(rawPositionFix, NmeaCultureInfo);
                    }

                    result = true;
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns>true if the parse was successful</returns>
        public bool ParseGpvtg(string sentence)
        {
            string[] words = GetWords(sentence);

            string rawSpeedKph = words[7];

            if (rawSpeedKph != "")
            {
                _speedKph = double.Parse(rawSpeedKph, NmeaCultureInfo);
            }
            return true;
        }

        /// <summary>
        /// Calculates the checksum for a sentence
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns></returns>
        public string GetChecksum(string sentence)
        {
            int checksum = 0;
            foreach (char character in sentence)
            {
                if (character == '$')
                {
                    // Ignore the dollar sign
                }
                else if (character == '*')
                {
                    // Stop processing before the asterisk
                    break;
                }
                else
                {
                    // Is this the first value for the checksum?
                    if (checksum == 0)
                    {
                        // Yes. Set the checksum to the value
                        checksum = Convert.ToByte(character);
                    }
                    else
                    {
                        // No. XOR the checksum with this character's value
                        checksum = checksum ^ Convert.ToByte(character);
                    }
                }
            }
            // Return the checksum formatted as a two-character hexadecimal
            return checksum.ToString("X2");
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sentence"></param>
        /// <returns>returns true if the calculated checksum matches the received checksum</returns>
        private bool IsValidSentence(string sentence)
        {
            string readChecksum = sentence.Substring(sentence.IndexOf("*") + 1).Trim();
            return readChecksum == GetChecksum(sentence);
        }

        /// <summary>
        /// Tries to parse coordinates. Sets Latitude and Longitude if possible.
        /// </summary>
        /// <param name="rawLongitude"></param>
        /// <param name="rawLatitude"></param>
        /// <param name="rawNSindicator"></param>
        /// <param name="rawEWindicator"></param>
        private void ParseCoordinates(string rawLongitude, string rawLatitude, string rawNSindicator, string rawEWindicator)
        {
            //Latitude
            try
            {
                double latHours = double.Parse(rawLatitude.Substring(0, 2), NmeaCultureInfo);
                double latMinutes = double.Parse(rawLatitude.Substring(2), NmeaCultureInfo);

                _latitude = latHours + latMinutes / 60;

                if (rawNSindicator == "S")
                {
                    _latitude = -_latitude;
                }

                //Longitude
                double lonHours = double.Parse(rawLongitude.Substring(0, 3), NmeaCultureInfo);
                double lonMinutes = double.Parse(rawLongitude.Substring(3), NmeaCultureInfo);

                _longitude = lonHours + lonMinutes / 60;

                if (rawEWindicator == "W")
                {
                    _longitude = -_longitude;
                }
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Tries to parse the time, sets SatelliteTime and Timestamp if possible
        /// </summary>
        /// <param name="rawUtCtime"></param>
        /// <param name="rawUtCdate"></param>
        private void ParseTime(string rawUtCtime, string rawUtCdate)
        {
            //two lines of code to save us from y2.1k
            DateTime todayTime = DateTime.Now;
            int y2K = todayTime.Year / 100;

            int utcHours = int.Parse(rawUtCtime.Substring(0, 2), NmeaCultureInfo);
            int utcMinutes = int.Parse(rawUtCtime.Substring(2, 2), NmeaCultureInfo);
            int utcSeconds = int.Parse(rawUtCtime.Substring(4, 2), NmeaCultureInfo);
            int utcMilliseconds = 0;

            // Extract milliseconds if it is available
            if (rawUtCtime.Length > 7)
            {
                utcMilliseconds = int.Parse(rawUtCtime.Substring(7), NmeaCultureInfo);
            }

            //Read the date from the satellite
            if (rawUtCdate != "")
            {
                int dd = int.Parse(rawUtCdate.Substring(0, 2), NmeaCultureInfo);
                int mm = int.Parse(rawUtCdate.Substring(2, 2), NmeaCultureInfo);
                int yy = int.Parse(rawUtCdate.Substring(4, 2), NmeaCultureInfo);
                _satelliteTime = new DateTime(y2K * 100 + yy, mm, dd, utcHours, utcMinutes, utcSeconds, utcMilliseconds);
            }
            else
            {
                _satelliteTime = new DateTime(_satelliteTime.Year, _satelliteTime.Month, _satelliteTime.Day, utcHours, utcMinutes, utcSeconds, utcMilliseconds);
            }
            TimeSpan deltaTime = TimeZone.CurrentTimeZone.GetUtcOffset(_satelliteTime);
            _timestamp = _satelliteTime + deltaTime;
        }

        #endregion
    }
}
