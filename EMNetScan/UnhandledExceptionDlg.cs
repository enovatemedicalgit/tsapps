/*
 *                  UnhandledExceptionDlg Class v. 1.1
 * 
 *                      Copyright (c)2006 Vitaly Zayko
 * 
 * History:
 * September 26, 2006 - Added "ThreadException" handler, "SetUnhandledExceptionMode", OnShowErrorReport event 
 *                      and updated the Demo and code comments;
 * August 29, 2006 - Updated information about Microsoft Windows Error Reporting service and its link;
 * July 18, 2006 - Initial release.
 * 
 */

/* More info on MSDN: 
 * http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dnbda/html/exceptdotnet.asp
 * http://msdn2.microsoft.com/en-us/library/system.windows.forms.application.threadexception.aspx
 * http://msdn2.microsoft.com/en-us/library/system.appdomain.unhandledexception.aspx
 * http://msdn2.microsoft.com/en-us/library/system.windows.forms.unhandledexceptionmode.aspx
 */

using System;
using System.Windows.Forms;
using System.Threading;

namespace Inssider
{
    internal class SendExceptionClickEventArgs: EventArgs
    {
        public readonly String UserDescription;
        public readonly bool SendExceptionDetails;
        public readonly Exception UnhandledException;
        public bool RestartApp;

        public SendExceptionClickEventArgs(bool sendDetailsArg, String description, Exception exceptionArg, bool restartAppArg)
        {
            SendExceptionDetails = sendDetailsArg;     // TRUE if user clicked on "Send Error Report" button and FALSE if on "Don't Send"
            UserDescription = description;
            UnhandledException = exceptionArg;         // Used to store captured exception
            RestartApp = restartAppArg;                // Contains user's request: should the App to be restarted or not
        }
    }

    /// <summary>
    /// Class for catching unhandled exception with UI dialog.
    /// </summary>
    class UnhandledExceptionDlg
    {
        private bool _doRestart;

        /// <summary>
        /// Set to true if you want to restart your App after falure
        /// </summary>
        public bool RestartApp
        {
            get { return _doRestart; }
            set { _doRestart = value; }
        }

        public delegate void SendExceptionClickHandler(object sender, SendExceptionClickEventArgs args);

        //public delegate void ShowErrorReportHandler(object sender, System.EventArgs args);

        /// <summary>
        /// Occurs when user clicks on "Send Error report" button
        /// </summary>
        public event SendExceptionClickHandler OnSendExceptionClick;

        /// <summary>
        /// Occurs when user clicks on "click here" link lable to get data that will be send
        /// </summary>
        public event SendExceptionClickHandler OnShowErrorReportClick;

        public event SendExceptionClickHandler OnCopyToClipboardClick;

        /// <summary>
        /// Default constructor
        /// </summary>
        public UnhandledExceptionDlg()
        {
            _doRestart = false;
            InitializeApplicationHandlers();
        }

        private void InitializeApplicationHandlers() {
            // Add the event handler for handling UI thread exceptions to the event:
            Application.ThreadException += ThreadExceptionFunction;

            // Set the unhandled exception mode to force all Windows Forms errors to go through our handler:
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);

            // Add the event handler for handling non-UI thread exceptions to the event:
            AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionFunction;
        } // End 

        /// <summary>
        /// Handle the UI exceptions by showing a dialog box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ThreadExceptionFunction(Object sender, ThreadExceptionEventArgs e)
        {
            // Suppress the Dialog in Debug mode:
            #if !DEBUG
            ShowUnhandledExceptionDlg(e.Exception);
            #endif
        }

        /// <summary>
        /// Handle the UI exceptions by showing a dialog box
        /// </summary>
        /// <param name="sender">Sender Object</param>
        /// <param name="args">Passing arguments: original exception etc.</param>
        private void UnhandledExceptionFunction(Object sender, UnhandledExceptionEventArgs args)
        {
            // Suppress the Dialog in Debug mode:
            #if !DEBUG
            ShowUnhandledExceptionDlg((Exception)args.ExceptionObject);
            #endif
        }

        /// <summary>
        /// Raise Exception Dialog box for both UI and non-UI Unhandled Exceptions
        /// </summary>
        /// <param name="e">Catched exception</param>
        public void ShowUnhandledExceptionDlg(Exception e)
        {
            Exception unhandledException = e ?? new Exception("Unknown unhandled Exception was occurred!");

            UnhandledExDlgForm exDlgForm = new UnhandledExDlgForm();
            try
            {

                string appName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                exDlgForm.Text = appName;
                exDlgForm.labelTitle.Text = String.Format(exDlgForm.labelTitle.Text, appName);

                // Do not show link label if OnShowErrorReport is not handled
                exDlgForm.labelLinkTitle.Visible = (OnShowErrorReportClick != null);
                exDlgForm.linkLabelData.Visible = (OnShowErrorReportClick != null);

                // Disable the Button if OnSendExceptionClick event is not handled
                exDlgForm.buttonSend.Enabled = (OnSendExceptionClick != null);

                // Handle clicks on report link label
                exDlgForm.linkLabelData.LinkClicked += delegate
                {
                    if(OnShowErrorReportClick != null)
                    {
                        SendExceptionClickEventArgs ar = new SendExceptionClickEventArgs(true, "", unhandledException, _doRestart);
                        OnShowErrorReportClick(this, ar);
                    }
                };

                exDlgForm.buttonCopy.Click += delegate
                {
                    if (OnCopyToClipboardClick != null)
                    {
                        SendExceptionClickEventArgs ar = new SendExceptionClickEventArgs(true, exDlgForm.UserDescription, unhandledException, _doRestart);
                        OnCopyToClipboardClick(this, ar);
                    }
                    exDlgForm.buttonCopy.Enabled = false;
                };

                // Show the Dialog box:
                bool sendDetails = (exDlgForm.ShowDialog() == DialogResult.Yes);

                if(OnSendExceptionClick != null)
                {
                    SendExceptionClickEventArgs ar = new SendExceptionClickEventArgs(sendDetails, exDlgForm.UserDescription, unhandledException, _doRestart);
                    OnSendExceptionClick(this, ar);
                }
            }
            finally
            {
                exDlgForm.Dispose();
            }
        }
        
    }
}
